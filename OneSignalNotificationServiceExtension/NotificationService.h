//
//  NotificationService.h
//  OneSignalNotificationServiceExtension
//
//  Created by Vamika on 27/05/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import <UserNotifications/UserNotifications.h>

@interface NotificationService : UNNotificationServiceExtension

@end
