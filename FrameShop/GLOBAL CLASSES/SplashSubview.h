//
//  SplashSubview.h
//  FrameShop
//
//  Created by Saurabh anand on 03/12/19.
//  Copyright © 2019 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Global.h"

NS_ASSUME_NONNULL_BEGIN

@interface SplashSubview : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *frame1;
@property (weak, nonatomic) IBOutlet UIImageView *frame3;
@property (weak, nonatomic) IBOutlet UIImageView *frame2;

@property (weak, nonatomic) IBOutlet UILabel *lbl;
@property (weak, nonatomic) IBOutlet UIView *logoView;
@property (weak, nonatomic) IBOutlet UILabel *subTitleLbl;

@end

NS_ASSUME_NONNULL_END
