//
//  Global.h
//  PicMe
//
//  Created by Saurabh Anand Srivastava on 30/05/17.
//  Copyright © 2017 Saurabh Anand Srivastava. All rights reserved.
//f

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "SAMKeychain.h"
//#import "DBManager.h"

#import <AssetsLibrary/AssetsLibrary.h>
//#import "DBManager.h"
#import <CoreLocation/CoreLocation.h>
#import "UIView+Toast.h"
//#import "AppDelegate.h"
#define APP_DELEGATE ((AppDelegate*)[[UIApplication sharedApplication] delegate])
#define APP_BLUE_COLOR [UIColor colorWithRed:57/255.0 green:69/255.0 blue:148/255.0 alpha:1.0]
#define APP_GRAY_COLOR [UIColor colorWithRed:216/255.0 green:216/255.0 blue:218/255.0 alpha:1.0]

#define APP_LIGHT_GRAY_COLOR [UIColor colorWithRed:246/255.0 green:246/255.0 blue:246/255.0 alpha:1.0]

#define shipping_in_process_dark [UIColor colorWithRed:214/255.0 green:141/255.0 blue:0.0 alpha:1.0]
#define shipping_in_process_light [UIColor colorWithRed:255/255.0 green:251/255.0 blue:219/255.0 alpha:1.0]

#define shipping_in_shipped_dark [UIColor colorWithRed:56/255.0 green:68/255.0 blue:151/255.0 alpha:1.0]
#define shipping_in_shipped_light [UIColor colorWithRed:234/255.0 green:236/255.0 blue:255/255.0 alpha:1.0]

#define delivered_in_shipped_dark [UIColor colorWithRed:45/255.0 green:123/255.0 blue:72/255.0 alpha:1.0]
#define delivered_in_shipped_light [UIColor colorWithRed:220/255.0 green:255/255.0 blue:223/255.0 alpha:1.0]

#define in_process_status_string @"Your order is in our production queue, you'll receive an email once it has been shipped."
#define delivered_status_string @"Your order has been delivered, we hope you like it!"
#define shipping_in_status_string @"Your order has been shipped and is on the way to you!"

#define BG_THREAD dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul); dispatch_async(q, ^{
#define MAIN_THREAD dispatch_async(dispatch_get_main_queue(), ^{
#define END_BLOCK });

//896
#define SLIDE_DURATION                               0.5

#define isiPhone4S                  [UIScreen mainScreen].bounds.size.height == 568

#define isiPhone6                   [UIScreen mainScreen].bounds.size.height == 667

#define isiPhone6Plus               [UIScreen mainScreen].bounds.size.height == 736

#define isiPhone5s                  [UIScreen mainScreen].bounds.size.height == 568

#define isiPhone10R_10SMAX          [UIScreen mainScreen].bounds.size.height == 896

#define isiPhone10                  [UIScreen mainScreen].bounds.size.height == 812

#define isiPad                      UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad
#define FAQ_URL                      @"https://infinity.frameshop.com.au/pages/FAQs"

#define STRIPE_API_TEST                 @"pk_test_2vbwFBoHaobBNRuCWPMddQX2"
#define BRAINTREE_API_LIVE              @"sandbox_ktwwn7dv_vnf2487f6m4dx263"
//#define BRAINTREE_API_LIVE                @"production_4xfgqdr4_mk5fpyzndtytvctx"
//#define STRIPE_API_TEST                   @"pk_live_E1I1gjifP1eE1b4XOurFK3ST"// for app  STRIPE  @"pk_test_2vbwFBoHaobBNRuCWPMddQX2"
#define ONESIGNAL_APP_ID                  @"931ed9f0-f2ce-4072-a333-a1267fa33551"

//#define STRIPE_API_LIVE                    @"sk_test_XP0jasYO6oLZXExKIpqeA0Yo"
#define ACCESS_TOKEN                            @"ACCESS_TOKEN"
#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)

#define USERID                                 @"userId"
#define PRODUCTID                              @"PRODUCTID"



#define LOADERON                               @"loaderOn"
#define LOADEROFF                              @"loaderOff"
#define APP_FB_INVITE_LINK                     @"https://fb.me/1614029085286679"
#define ADD_CAPTION_TEXT                       @"Add Your Caption"


#define INSTAGRAM_APP_ID                         @"1aebc237ab124c3badb1ca35f842464e"
#define INSTAGRAM_REDIRECT_URI                   @"http://www.pocketprints.com.au/wp-admin/admin-ajax.php?action=register_instagram"
#define INSTA_TOKEN                              @"INSTA_TOKEN"



#define APP_LINK                                @"https://itunes.apple.com/us/app/pocket-prints/id900757823?ls=1&mt=8"


#define DATABASE_NAME                          @"FS.sqlite"
#define API_ACCESS_TOKEN                       @"API_ACCESS_TOKEN"
#define PRODUCT_ID_NOTIFICATION                 @"PRODUCT_ID_NOTIFICATION"


#define     PREF_PUSH_TOKEN                    @"PREF_PUSH_TOKEN"
#define     GUEST_USER                         @"GUEST_USER"
#define     GUEST_USER_DETAIL                  @"GUEST_USER_DETAIL "

#define SHOW_FULL                                @"showFull"

#define SLIDE_MENU                              @"slideMenu"
#define SLIDE_MENU_OPTION                       @"SLIDE_MENU_OPTION"
#define FULL_IMAGE                       @"FULL_IMAGE"
#define FINISH_UPLOADING                        @"FINISH_UPLOADING"
#define SHOW_TUT_SCREEN                         @"SHOW_TUT_SCREEN"

#define SHOW_WARNING                             @"SHOW_WARNING"
#define EMAIL                                    @"EMAIL"
#define CART_ID                                  @"CART_ID"
#define NO_CONNECTION                            @"NO"
#define CART_COUNT                               @"CART_COUNT"
#define ADDRESS_ID                               @"ADDRESS_ID"
#define CART_REFRESH                               @"CART_REFRESH"
#define SHOW_RATING                               @"SHOW_RATING"

#define CHANGE_CART_COUNT                          @"CHANGE_CART_COUNT"
#define SINGLE_MAT_ID                              @"SINGLE_MAT_ID"
#define DOUBLE_MAT_ID                              @"DOUBLE_MAT_ID"
#define FRAME_CAT_ID                               @"FRAME_CAT_ID"
#define FRAME_SORT_ID                              @"FRAME_SORT_ID"

#define FRAME_SKU                              @"CUSTOM_PICTURE_FRAME"
#define ACRYLIC_SKU                            @"ACRYLIC_STANDBACK"
#define ACRYLIC_PHOTOBOOK                      @"Acrylic_Photo_Block"
#define BLOCK_MOUNT_SKU                        @"BLOCK_MOUNT"
#define HANGER_SAWTOOTH                        @"HANGER_SAWTOOTH"
#define HANGER_SUPPORT_FRAME                   @"HANGER_SUPPORT_FRAME"

#define CANVAS_PRINTING_SKU                              @"CANVAS"




#define RUN_ON_MAIN_QUEUE(BLOCK_CODE)           dispatch_async(dispatch_get_main_queue(),(BLOCK_CODE))
//@"http://172.16.6.14:3000/api"

//#define SERVER_URL  @"https://dev.pocketprints.com.au/"
//#define SERVER_URL  @"https://lab.spocketprints.com.au/"
//#define SERVER_URL  @"https://pocketprints.synchsoftventures.com/"


#define API_TOKEN               @"token.json"
#define API_PRDUCTS             @"products.json"
#define API_UPLOAD_PHOTO        @"photo.json"
#define API_PREFLIGHT           @"preflight.json"
#define API_ORDER               @"order.json"
#define API_STRIPE_PAYMENT      @"stripe_payment.json"
#define API_ADD_CUSTOMER        @"update_customer.json"

#define SERVER_URL_WITH_API(aAPI) ([NSString stringWithFormat:@"%@/%@",SERVER_URL,aAPI])



#define NO_DATA_MSG                  @"There is no data currently. Pull to refresh."
#define WENT_WRONG_MSG               @"Something went wrong. Pull to refresh."
#define SERVER_MAINTENANC_MSG        @"The server is under maintenance, please visit again few moments later."
#define TIME_OUT_MSG                 @"Ewww! The connectivity is really poor , please try again!"
#define NO_INTERNET_MSG              @"Uh Oh! Looks like you have lost connection."
#define NO_ITEM_ORDERHISTORY              @"Looks like you haven't made an order yet"
#define NO_ITEM_ORDERHISTORY_SUB              @"Head to the home screen and start creating beautiful memories"
#define DELETE_MSG                   @"You will not be able to undo this action!"
#define DELETE_TITLE                   @"Are you sure?"

#define FB_URL                              @"https://www.facebook.com/pocketprintsapp/"
#define IG_URL                              @"https://www.instagram.com/pocketprintsapp/"
#define PIN_URL                             @"https://pin.it/y33tg2fsyq5ww4"
#define GOOGLE_URL                          @"https://plus.google.com/118434834804758695553"
#define APP_LINK_URL                        @"https://apple.co/2HElu49"

#define FAQ_URL                             @"https://infinity.frameshop.com.au/pages/FAQs"
#define PRIVACY_URL                         @"https://infinity.frameshop.com.au/privacy"
#define CONTACT_URL                         @"https://infinity.frameshop.com.au/contact-us"
#define TERMS_AND_CONDITION_URL             @"https://infinity.frameshop.com.au/term-conditions"

#define CORE_BLACK                             @"CORE_BLACK"
#define CORE_WHITE                             @"CORE_WHITE"
#define CORE_MIRROR                             @"CORE_MIRROR"

#define MIN_IMAGE_SIZE 600      //384
#define MAX_IMAGE_SIZE 3456

#define HEIGHT  @"height"
#define WIDTH  @"width"
#define ISACTIVE  @"isActive"
#define PORTRAIT  @"Portrait"
#define LANDSCAPE  @"Landscape"
#define SQUARE  @"Square"
#define ZOOMFACTOR  @"factor"
#define FRAMESHOP  @"Frameshop"
#define APPLINK  @"itms://itunes.apple.com/app/apple-store/id1535595350?mt=8"

#define TOGGLE_UNIT @"TOGGLE_UNIT"

#define MAIN_SCREEN  [UIScreen mainScreen].bounds
#define की_साइज़  size
#define क़ी_चौड़ायी   width
#define INSTAGRAM_ACCESS_TOKEN @"access_token"
#define INSTAGRAM_USER_ID @"user_id"


#define ROLLBAR_ACCESS_TOKEN                @"847f9712caa04d29b170cabd81f8158f"
#define PAYPAL_CLIENT_ID                @"ASGC51kdwj0UHNGbKX-zec8oqcWY8VxL13Uldprpa2CgSjaKXDmwSCbKeim1W6zmZduM7pi7rUYdjGEb"
#define LAUNCH_COUNT                @"LAUNCH_COUNT"


extern NSDictionary *globalSizeDictionary;
static NSString* const CreativeSDKClientId = @"4a13644edc5141a79260fa99129261c1"; // Pro mode
static NSString* const CreativeSDKClientSecret = @"8aaa7e3b-b63a-4535-9863-e7a9e6522ef8";
@interface Global : NSObject
+(void)makeSizeDictionary:(NSString*)sku;
+(void)slidLeftWithView:(UIView *)view leftMargin:(float)leftMargin duration:(float)duration delay :(float)delay shouldDown:(BOOL)shouldDown;
+(void)slidRightWithView:(UIView *)view rightMargin:(float)rightMargin duration:(float)duration delay :(float)delay shouldDown:(BOOL)shouldDown;
+(void)setNavigationBar:(UINavigationController*)navigationController;
+(void)roundCorner:(UIView *)view roundValue:(float)round borderWidth:(float)borderWidth borderColor:(UIColor *)color;
+(void)roundCornerWithShadow:(UIView *)view roundValue:(float)round borderWidth:(float)borderWidth borderColor:(UIColor *)color;
+(void)callFullImage:(UIImage *)img;

+(void)slidDownWithView:(UIView *)view downMargin:(float)downMargin duration:(float)duration delay :(float)delay;
+(void)slidUpWithView:(UIView *)view upMargin:(float)upMargin duration:(float)duration delay :(float)delay;
+(void) addshadow:(UIView *)view radious:(float)radious;
+(void)addShadow:(UIView *)view;
+(void)zoomIn:(UIView *)_slideshow;
+(void)zoomOut:(UIView *)_slideshow;
+(NSString *)convertDateIntoUTC:(NSDate *)date;
+(NSString *)convertUTCIntoDate:(NSString *)date;

+(void)setHashTagColor:(UITextView *)text;
+(void)setImageOnImageView:(UIImageView *)imageView withImageUrl:(NSString *)imageUrl andImageId:(NSString *)imageId;


+(UIImage *)getImageFromDoucumentWithName:(NSString *)imageName;
+(UIImage*)getImageFromUrl:(NSString*)imageName;

+(BOOL) saveImagesInDocumentDirectory:(NSData*) pngData ImageId:(NSString*) imageId;
+ (NSString *)documentsPathForFileName:(NSString *)name;
+(NSMutableDictionary*)getGuestUserDetails;
+(void)deleteFilesFromCacheWhichContain:(NSString *)name;

+(void)addShadowandroundCornerToView:(UIView *)view andCornerRadious:(float)radious;


+( NSMutableArray *)getAllFileFromeCacheContain:(NSString *)str;
+(void)showSideMenu;

+ (void)alert: (UIViewController *) view title: (NSString *) title message: (NSString *) message;
+(NSString*) getXibName:(NSString*) xibActualName;
+(void)showWarningScreen:(NSString *)msg;
+(void)showFullImage:(NSMutableArray *)imageArray;
+(UIStoryboard*) getStoryBoardOfTheApp;
+(NSMutableArray*) sortDictArray:(NSMutableArray*) initialArray BasedOnKey:(NSString*) key;
+(void)removeShadow:(UIView *)view;
+(UIImage *)getImageFromDoucumentWithUrl:(NSString *)url;
+ (UIColor *)colorFromHexString:(NSString *)hexString;
+ (void)startShimmeringToView:(UIView *)view;
+(void)stopShimmeringToView:(UIView *)view;
+(NSString *)getDeviceId;
+(UIImage*) getMidiumSizeImage:(UIImage*)image imageSize:(float)size;
+(void) Padding:(UITextField *) textField;
+(void)callMenuViewWithCode:(NSString *)code;
+(NSString *)convertDicToJson:(NSDictionary *)dic;
+(void)drawLineFromPoint:(CGRect)fromPint toPoint:(CGRect)toPoint whereLineWidth:(float)lineWidth andLineColour:(UIColor *)lineColor atBaseView:(UIView *)baseView;
+(void)showSimpleAlert:(UIViewController *)contro andTitle:(NSString *)titleTxt andMessage:(NSString *)msg;
+(NSMutableDictionary *)convertNsstringToDic:(NSString *)code;
+(BOOL)isString:(NSString *)str contains:(NSString *)subStr;
+(NSString *)makeFirstLatterUpperofString:(NSString *)str;
+ (NSString *)removeEndSpaceFrom:(NSString *)strtoremove;
+(UIImage *)getChangedImageColorWithImage:(UIImage *)image andColor:(UIColor *)color;
+(void)showTutScreen:(NSMutableArray *)frameArray;
+(UIImage *)takeSnapshotofView:(UIView *)view;
+(UIImage *)convertImageTo3dWhereBacking:(NSString *)backing andImageView:(UIImageView *)baseIamgeView andImage:(UIImage *)baseImage withGradient:(BOOL)shouldGrd andCoreColor:(UIColor*)coreColor sku:(NSString *)sku andWidth:(float)width;

+(UIImage *)acrylic2DViewWithGradientWhereImage:(UIImage *)baseImage shouldGradient:(BOOL)shouldGrd;
+(UIImage *)getCheckMarkWhereBgColor:(NSString *)hexString;
+(NSString *)hexStringFromColor:(UIColor *)color;
+(BOOL) saveFileInDocumentDirectory:(NSData*) pngData fileName:(NSString*) fileName;
+(BOOL) needsUpdate;
+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize ;
+(UIImage *)get3DAcrylicImageImage:(UIImage *)baseImage andImageView:(UIImageView *)baseIamgeView;
+(void)makePhotoBook:(UIView *)vw1;
+(void)makeAcrylicPhotoBook:(UIImageView *)topImg mainView: (UIView *)mainView bottomImage: (UIImageView *)bottomImgView leftImage: (UIImageView *)leftImgView rightImage: (UIImageView *)rightImgView centerImage: (UIImageView *)centerImgView mainImageHeightConstant:(NSLayoutConstraint *)_mainImageHeightConstant mainImageWidthtConstant:(NSLayoutConstraint *)_mainImageWidthConstant imageToUse:(UIImage *)imageToUse;
+(void)setLeftImage:(UIImageView *)_leftImage;
+ (UIImage *)blurredImageWithImage:(UIImage *)sourceImage;
+(void)setRightImage:(UIImageView *)_rightImage;
+(void)setTopImage:(UIImageView *)_leftImage;
+(void)setBottomImage:(UIImageView *)_bottomImage;
+(void)setMainView:(UIView *)_mainView;
@end
