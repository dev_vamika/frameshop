//
//  MenuOptionView.h
//  FrameShop
//
//  Created by Saurabh Srivastav on 11/07/21.
//  Copyright © 2021 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopApi.h"
#import <AudioToolbox/AudioServices.h>
NS_ASSUME_NONNULL_BEGIN

@interface MenuOptionView : UIViewController<PopApiDelegate>
@property (weak, nonatomic) IBOutlet UIButton *cartBtn;
@property (weak, nonatomic) IBOutlet UIButton *menuBtn;
@property (weak, nonatomic) IBOutlet UILabel *cartCount;
-(void)getCartCount;
- (IBAction)cartBtn:(id)sender;
- (IBAction)menuBtn:(id)sender;
@end

NS_ASSUME_NONNULL_END
