//
//  Global.m
//  PicMe
//
//  Created by Saurabh Anand Srivastava on 30/05/17.
//  Copyright © 2017 Saurabh Anand Srivastava. All rights reserved.
//

#import "Global.h"
#import "AppDelegate.h"
@implementation Global{
    
}
NSDictionary *globalSizeDictionary;
//DbManager *dbObject;
BOOL reachable;

+(void)makeSizeDictionary:(NSString*)sku{
    if ([sku isEqualToString:CANVAS_PRINTING_SKU]) {
        globalSizeDictionary = @{@"Square" :@[@{WIDTH:@8,HEIGHT:@8},@{WIDTH:@10,HEIGHT:@10},@{WIDTH:@12,HEIGHT:@12},@{WIDTH:@14,HEIGHT:@14},@{WIDTH:@16,HEIGHT:@16},@{WIDTH:@20,HEIGHT:@20},@{WIDTH:@24,HEIGHT:@24},@{WIDTH:@30,HEIGHT:@30},@{WIDTH:@36,HEIGHT:@36}],
                                 @"Portrait" :@[@{WIDTH:@8,HEIGHT:@10},
                                                @{WIDTH:@8,HEIGHT:@12},@{WIDTH:@11,HEIGHT:@14},@{WIDTH:@12,HEIGHT:@16},@{WIDTH:@16,HEIGHT:@20},@{WIDTH:@16,HEIGHT:@24},@{WIDTH:@20,HEIGHT:@30},@{WIDTH:@24,HEIGHT:@36}],
                                 @"Landscape": @[@{WIDTH:@10,HEIGHT:@8},
                                                 @{WIDTH:@12,HEIGHT:@8},@{WIDTH:@14,HEIGHT:@11},@{WIDTH:@16,HEIGHT:@12},@{WIDTH:@20,HEIGHT:@16},@{WIDTH:@24,HEIGHT:@16},@{WIDTH:@30,HEIGHT:@20},@{WIDTH:@36,HEIGHT:@24},@{WIDTH:@16,HEIGHT:@8},@{WIDTH:@24,HEIGHT:@12},@{WIDTH:@36,HEIGHT:@18}]};
    }else if ([sku isEqualToString:ACRYLIC_PHOTOBOOK]){
        globalSizeDictionary = @{@"Square" :@[@{WIDTH:@4,HEIGHT:@4},@{WIDTH:@6,HEIGHT:@6},@{WIDTH:@8,HEIGHT:@8},@{WIDTH:@10,HEIGHT:@10}],
                                 @"Portrait" :@[@{WIDTH:@4,HEIGHT:@6},
                                                @{WIDTH:@5,HEIGHT:@7},@{WIDTH:@6,HEIGHT:@8},@{WIDTH:@8,HEIGHT:@10}],
                                 @"Landscape": @[@{WIDTH:@6,HEIGHT:@4},@{WIDTH:@7,HEIGHT:@5},@{WIDTH:@8,HEIGHT:@6},@{WIDTH:@10,HEIGHT:@8}]};
    } else{
        globalSizeDictionary = @{@"Square" :@[@{WIDTH:@4,HEIGHT:@4 },@{WIDTH:@6,HEIGHT:@6},@{WIDTH:@8,HEIGHT:@8},@{WIDTH:@10,HEIGHT:@10},@{WIDTH:@12,HEIGHT:@12},@{WIDTH:@14,HEIGHT:@14},@{WIDTH:@16,HEIGHT:@16},@{WIDTH:@20,HEIGHT:@20},@{WIDTH:@24,HEIGHT:@24},@{WIDTH:@30,HEIGHT:@30},@{WIDTH:@36,HEIGHT:@36}],
                                 @"Portrait" :@[@{WIDTH:@4,HEIGHT:@6},@{WIDTH:@5,HEIGHT:@7},@{WIDTH:@6,HEIGHT:@8},@{WIDTH:@8,HEIGHT:@10},
                                                @{WIDTH:@8,HEIGHT:@12},@{WIDTH:@11,HEIGHT:@14},@{WIDTH:@12,HEIGHT:@16},@{WIDTH:@16,HEIGHT:@20},@{WIDTH:@16,HEIGHT:@24},@{WIDTH:@20,HEIGHT:@30},@{WIDTH:@24,HEIGHT:@36}],
                                 @"Landscape": @[@{WIDTH:@6,HEIGHT:@4},@{WIDTH:@7,HEIGHT:@5},@{WIDTH:@8,HEIGHT:@6},@{WIDTH:@10,HEIGHT:@8},
                                                 @{WIDTH:@12,HEIGHT:@8},@{WIDTH:@14,HEIGHT:@11},@{WIDTH:@16,HEIGHT:@12},@{WIDTH:@20,HEIGHT:@16},@{WIDTH:@24,HEIGHT:@16},@{WIDTH:@30,HEIGHT:@20},@{WIDTH:@36,HEIGHT:@24},@{WIDTH:@16,HEIGHT:@8},@{WIDTH:@24,HEIGHT:@12},@{WIDTH:@36,HEIGHT:@18}]};
    }
    
    
}

+(NSMutableArray*) sortDictArray:(NSMutableArray*) initialArray BasedOnKey:(NSString*) key{
    if(initialArray.count == 0)
        return initialArray;
    for(int i=0; i< [initialArray count]-1; i++) {
        for(int j = 0; j< [initialArray count]-1-i; j++) {
            if([initialArray[j+1][key] floatValue] < [initialArray[j][key] floatValue]) {
                [initialArray exchangeObjectAtIndex:j+1 withObjectAtIndex:j];
            }
        }
    }
    return initialArray;
}
+(void)slidLeftWithView:(UIView *)view leftMargin:(float)leftMargin duration:(float)duration delay :(float)delay shouldDown:(BOOL)shouldDown{
    
    if (shouldDown) {
        [UIView animateWithDuration:duration delay:delay options:UIViewAnimationOptionCurveEaseIn animations:^{
            view.frame  = CGRectMake(view.frame.origin.x-leftMargin, view.frame.origin.y-leftMargin*0.2, view.frame.size.width,view.frame.size.height);
            
        } completion:^(BOOL finished) {
            
        }];
    }
    else{
        [UIView animateWithDuration:duration delay:delay options:UIViewAnimationOptionCurveEaseIn animations:^{
            view.frame  = CGRectMake(view.frame.origin.x-leftMargin, view.frame.origin.y, view.frame.size.width,view.frame.size.height);
            
        } completion:^(BOOL finished) {
            
        }];
    }
}
+(void)slidRightWithView:(UIView *)view rightMargin:(float)rightMargin duration:(float)duration delay :(float)delay shouldDown:(BOOL)shouldDown{
    if (shouldDown) {
        
        [UIView animateWithDuration:duration delay:delay options:UIViewAnimationOptionCurveEaseIn animations:^{
            view.frame  = CGRectMake(view.frame.origin.x+rightMargin, view.frame.origin.y+rightMargin*0.2, view.frame.size.width,view.frame.size.height);
            
        } completion:^(BOOL finished) {
            
        }];
    }
    else{
        [UIView animateWithDuration:duration delay:delay options:UIViewAnimationOptionCurveEaseIn animations:^{
            view.frame  = CGRectMake(view.frame.origin.x+rightMargin, view.frame.origin.y, view.frame.size.width,view.frame.size.height);
            
        } completion:^(BOOL finished) {
            
        }];
        
    }
}

+(void)addShadowandroundCornerToView:(UIView *)view andCornerRadious:(float)radious{
    
    view.layer.cornerRadius = radious;
    view.clipsToBounds = true;
    
    UIView *supreView=[[UIView alloc] init];
    supreView.frame=view.frame;
    [view.superview addSubview:supreView];
    [self addShadow:supreView];
    [view.superview addSubview:view];
    
    
}
+(void)addShadow:(UIView *)demoView{
    demoView.layer.borderWidth = 1.0;
    demoView.layer.cornerRadius = 3.0;
    demoView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    demoView.layer.shadowColor = [UIColor blackColor].CGColor;
    demoView.layer.shadowOffset = CGSizeMake(0, 3.0); //Here your control your spread
    demoView.layer.shadowOpacity = 1;
    demoView.layer.shadowRadius = 6.0 ;//Here your control your blur
    
}


+(void) addshadow:(UIView *)view radious:(float)radious{
    view.layer.masksToBounds = NO;
    view.layer.shadowColor = [[UIColor blackColor] CGColor];
    view.layer.shadowOffset = CGSizeMake(0,0);
    view.layer.shadowOpacity = 0.9;
    view.layer.shadowRadius = radious;
    
}
+(void)removeShadow:(UIView *)view{
    view.layer.masksToBounds = NO;
    view.layer.shadowColor = [[UIColor clearColor] CGColor];
    view.layer.shadowOffset = CGSizeMake(0,0);
    view.layer.shadowOpacity = 0.0;
    view.layer.shadowRadius = 0;
    
    
    
    
}


+(void)zoomIn:(UIView *)_slideshow{
    
    _slideshow.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.2, 0.2);
    
    
    [UIView animateWithDuration:0.3
                     animations:^{
        _slideshow.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1);
    } completion:^(BOOL finished) {
        
        //[self zoomOut];
        
        
    }];
    
    
}

+(void)zoomOut:(UIView *)_slideshow{
    [UIView animateWithDuration:3.5
                     animations:^{
        _slideshow.transform = CGAffineTransformScale(CGAffineTransformMakeTranslation(0,0), 0, 0);
    } completion:^(BOOL finished) {
        _slideshow.hidden = FALSE;
        //[self zoomIn];
    }];
}

+(void)slidDownWithView:(UIView *)view downMargin:(float)downMargin duration:(float)duration delay :(float)delay {
    
    [UIView animateWithDuration:duration delay:delay options:UIViewAnimationOptionCurveEaseIn animations:^{
        view.frame  = CGRectMake(view.frame.origin.x, view.frame.origin.y+downMargin, view.frame.size.width,view.frame.size.height);
        
    } completion:^(BOOL finished) {
        
    }];
    
    
}



+(void)callMenuViewWithCode:(NSString *)code{
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
    dic[@"code"]=code;
    
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:SLIDE_MENU_OPTION object:self userInfo:dic];
    
}
+(void)callFullImage:(UIImage *)img{
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
    dic[@"img"]=img;
    
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:FULL_IMAGE object:self userInfo:dic];
    
}


+(NSMutableDictionary *)convertNsstringToDic:(NSString *)code{
    NSString *jsonString = code;
    NSData *data = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    return [json mutableCopy];
    
}
+(UIImage *)getCheckMarkWhereBgColor:(NSString *)hexString{
    
    
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    int r=((rgbValue & 0xFF0000) >> 16);
    int g=((rgbValue & 0xFF00) >> 8);
    int b=(rgbValue & 0xFF);
    
    int lightness = ((r*299)+(g*587)+(b*114))/1000; //get lightness value
    
    if (lightness < 127) { //127 = middle of possible lightness value
        return [UIImage imageNamed:@"whiteCheck.png"];
    }
    else  return [UIImage imageNamed:@"blackCheck.png"];
    
    
}



+(UIImage *)get3DAcrylicImageImage:(UIImage *)baseImage andImageView:(UIImageView *)baseIamgeView{
    
    UIView *view = [[UIView alloc] init];
    view.frame = CGRectMake(0, 0, 500, 500);
    view.backgroundColor = [UIColor whiteColor];
    UIView * mainView = [[UIView alloc] init];
    UIImageView *imageView = [[UIImageView alloc] init];
    float imageWidth = baseImage.size.width;
    float imageHeight = baseImage.size.height;
    float imageRatio = imageWidth/imageHeight;
    float viewWidth = 600;
    float viewHeight = viewWidth/imageRatio;
    mainView.frame = CGRectMake(0, 0, viewWidth, viewHeight);
    float localImageWidth = viewWidth*0.77;
    float loacaImageHeight = localImageWidth/imageRatio;
    
    mainView.frame = CGRectMake(0, 0, viewWidth*0.82, viewHeight);
    imageView.frame = CGRectMake((viewWidth-localImageWidth)/2, (viewHeight-loacaImageHeight)/2, localImageWidth, loacaImageHeight);
    // mainView.backgroundColor = [UIColor redColor];
    imageView.image = [self getMidiumSizeImage:baseImage imageSize:viewWidth];
    [mainView addSubview:imageView];
    [view addSubview:mainView];
    imageView.image = [self acrylic2DViewWithGradientWhereImage:baseImage shouldGradient:YES];
    [self makeView3D:imageView.layer andangle:25];
    
    
    
  
    
    
   
        
        UIImageView *barImageView = [[UIImageView alloc] init];
        barImageView.frame = CGRectMake(18, viewHeight*0.029, 25, viewHeight*0.94);
        
        
       
        
        
      //  NSLog(@"%@",baseImage.size.width);
        
        UIImage * barI = [self imageByCropping:baseImage toRect:CGRectMake(0, 0, baseImage.size.width*0.10, baseImage.size.height)];
        
        barImageView.image =[self getMidiumSizeImage:barI imageSize:viewHeight]; /*[self imageByCropping:baseImage toRect:CGRectMake(0, 0, 25, imageHeight)];*/
     
        
            imageView.image =[self getMidiumSizeImage:[self imageByCropping:baseImage toRect:CGRectMake(baseImage.size.width*0.10, 0, baseImage.size.width*0.90, baseImage.size.height)] imageSize:1000] ;
            
        
        
        [self makeView3D:barImageView.layer andangle:-45];
       // imageView.image = [self imageByCropping:imageView.image toRect:CGRectMake(25, 0, imageWidth- 25, imageHeight)];
        [mainView addSubview: barImageView];
        
    
    
 
        
        UIView *bar = [[UIView alloc] init];
        
        bar.frame = CGRectMake(23, viewHeight*0.029, 20, viewHeight*0.94);
        [mainView addSubview:bar];
        [self makeView3D:bar.layer andangle:-45];
        
       
            bar.backgroundColor = [UIColor blackColor];
        
        
    
    UIImage * img = [self takeSnapshotofView:mainView];
    [view removeFromSuperview];
    [mainView removeFromSuperview];
    view = nil;
    mainView = nil;

    imageView = nil;
    // [self addshadow:baseIamgeView radious:6];
    return img;
}



+(UIImage *)convertImageTo3dWhereBacking:(NSString *)backing andImageView:(UIImageView *)baseIamgeView andImage:(UIImage *)baseImage withGradient:(BOOL)shouldGrd andCoreColor:(UIColor*)coreColor sku:(NSString *)sku andWidth:(float)width{
    
    
    
    NSData *imgData ;
    imgData = [self removeExifFromImage:baseImage];
    
    baseImage = [UIImage imageWithData:imgData];
    
    UIView *view = [[UIView alloc] init];
    view.frame = CGRectMake(0, 0, 500, 500);
    view.backgroundColor = [UIColor whiteColor];
    UIView * mainView = [[UIView alloc] init];
    UIImageView *imageView = [[UIImageView alloc] init];
    float imageWidth = baseImage.size.width;
    float imageHeight = baseImage.size.height;
    float imageRatio = imageWidth/imageHeight;
    float viewWidth = 600;
    float viewHeight = viewWidth/imageRatio;
    mainView.frame = CGRectMake(0, 0, viewWidth, viewHeight);
    
    float localImageWidth = viewWidth*0.77;
    float loacaImageHeight = localImageWidth/imageRatio;
    mainView.frame = CGRectMake(0, 0, viewWidth, viewHeight);
    imageView.frame = CGRectMake(50+(viewWidth-localImageWidth)/2, (viewHeight-loacaImageHeight)/2, localImageWidth, loacaImageHeight);
   
    if (baseImage.size.width>baseImage.size.height) {
        
        mainView.frame = CGRectMake(0, 0, viewWidth*0.82, viewHeight);
        imageView.frame = CGRectMake((viewWidth-localImageWidth)/2, (viewHeight-loacaImageHeight)/2, localImageWidth, loacaImageHeight);
    }
    
    

    
    
   
    // mainView.backgroundColor = [UIColor redColor];
    imageView.image = [self getMidiumSizeImage:baseImage imageSize:viewWidth];
    [mainView addSubview:imageView];
    [view addSubview:mainView];
    imageView.image = [self acrylic2DViewWithGradientWhereImage:baseImage shouldGradient:shouldGrd];
    [self makeView3D:imageView.layer andangle:25];
    
    float barWidth;
//
    if ([sku isEqualToString:CANVAS_PRINTING_SKU]){
        barWidth = 3;
    }
    else{
        barWidth = 1;
    }
    
    barWidth = barWidth/width;
    
    baseImage=[UIImage imageWithData:[self removeExifFromImage:baseImage]] ;
   
    
   
    
    
        UIImageView *barImageView = [[UIImageView alloc] init];
        barImageView.frame = CGRectMake(18, viewHeight*0.029, barWidth * 500, viewHeight*0.91);
        
        
       
        
        
      //  NSLog(@"%@",baseImage.size.width);
    if (!([sku isEqualToString:CANVAS_PRINTING_SKU] && [backing isEqualToString:CORE_BLACK] )) {
        
    
        
        UIImage * barI = [self imageByCropping:baseImage toRect:CGRectMake(0, 0, baseImage.size.width*0.10, baseImage.size.height)];
        
        barImageView.image =[self getMidiumSizeImage:barI imageSize:viewHeight];
    }/*[self imageByCropping:baseImage toRect:CGRectMake(0, 0, 25, imageHeight)];*/
        if ([backing isEqualToString:CORE_MIRROR] ) {
//
            barImageView.image = [UIImage imageWithCGImage:barImageView.image.CGImage
                                                     scale:barImageView.image.scale
                                               orientation:UIImageOrientationUpMirrored];
           
//
//
        }
        else{
            if ([sku isEqualToString:CANVAS_PRINTING_SKU]) {
                imageView.image =[self getMidiumSizeImage:[self imageByCropping:baseImage toRect:CGRectMake(baseImage.size.width*0.10, 0, baseImage.size.width*0.90, baseImage.size.height)] imageSize:1000] ;
                barWidth = 3;
            }
            else{
                [self addBlurToView:barImageView];
               
            }
            
            
        }
    
    
  
    
        
       // [self makeView3D:barImageView.layer andangle:-45];
       // imageView.image = [self imageByCropping:imageView.image toRect:CGRectMake(25, 0, imageWidth- 25, imageHeight)];
       // [mainView addSubview: barImageView];
        
  
  
  
        
        UIView *bar = [[UIView alloc] init];
    
    if (baseImage.size.width>baseImage.size.height) {
        bar.frame = CGRectMake(-12, viewHeight*0.042, 60, viewHeight*0.915);
        barImageView.frame = CGRectMake(bar.frame.size.width-barImageView.frame.size.width, 0, barImageView.frame.size.width, viewHeight*0.915);
     
    }
    else   if (baseImage.size.width==baseImage.size.height) {
        bar.frame = CGRectMake(40, viewHeight*0.040, 60, viewHeight*0.92);
        barImageView.frame = CGRectMake(bar.frame.size.width-barImageView.frame.size.width, 0, barImageView.frame.size.width, viewHeight*0.92);
       
     
    }
    
    else{
        
        bar.frame = CGRectMake(18, viewHeight*0.045, 84, viewHeight*0.91);
        barImageView.frame = CGRectMake(bar.frame.size.width-barImageView.frame.size.width-2, 5, barImageView.frame.size.width, viewHeight*0.90);
        
    }
    
   
  
        
        [mainView addSubview:bar];
       
    
    [bar addSubview:barImageView];
       
      //  bar.backgroundColor = (coreColor!=nil)?coreColor:[UIColor blackColor];
   // barImageView.image = nil;
    
    if ([sku isEqualToString:CANVAS_PRINTING_SKU] && [backing isEqualToString:CORE_BLACK] ) {
        barImageView.backgroundColor = (coreColor!=nil)?coreColor:[UIColor blackColor];
    }
    else{
    UIView * backBar = [[UIView alloc] init];
    backBar.frame = CGRectMake(barImageView.frame.origin.x-(barImageView.frame.size.width*3/4), barImageView.frame.origin.y, (barImageView.frame.size.width*3/4), barImageView.frame.size.height);
    backBar.backgroundColor = (coreColor!=nil)?coreColor:[UIColor blackColor];
    [bar addSubview:backBar];
    }
    
    [self makeView3D:bar.layer andangle:-45];
    UIImage * img = [self takeSnapshotofView:mainView];
    [view removeFromSuperview];
    [mainView removeFromSuperview];
    view = nil;
    mainView = nil;

    imageView = nil;
    // [self addshadow:baseIamgeView radious:6];
    return img;
}

+ (void)addBlurToView:(UIView *)view {
    UIView *blurView = nil;

    if([UIBlurEffect class]) { // iOS 8
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
        blurView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        blurView.frame = view.frame;

    } else { // workaround for iOS 7
        blurView = [[UIToolbar alloc] initWithFrame:view.bounds];
    }
    blurView.alpha = 0.5;
    [blurView setTranslatesAutoresizingMaskIntoConstraints:NO];

    [view addSubview:blurView];
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[blurView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(blurView)]];
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[blurView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(blurView)]];
}

+(UIImage *) rotateImage:(UIImage *) image {
    
    if (image.imageOrientation == UIImageOrientationUp) return image;
    
    UIGraphicsBeginImageContextWithOptions(image.size, NO, image.scale);
    [image drawInRect:(CGRect){0, 0, image.size}];
    UIImage *normalizedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return normalizedImage;
}
+(NSData *) removeExifFromImage:(UIImage *) image {
   
    NSData *data =UIImagePNGRepresentation([self rotateImage:image]);
    
    CGImageSourceRef source = CGImageSourceCreateWithData((CFDataRef)data, NULL);
    NSMutableData *mutableData = nil;
    
    if (source) {
        CFStringRef types = CGImageSourceGetType(source);
        size_t count = CGImageSourceGetCount(source);
        mutableData = [NSMutableData data];
        
        CGImageDestinationRef destination = CGImageDestinationCreateWithData((CFMutableDataRef)mutableData, types, count, NULL);
        
        NSDictionary *removeExifProperties = @{(id)kCGImagePropertyExifDictionary: (id)kCFNull,
                                               (id)kCGImagePropertyGPSDictionary : (id)kCFNull};
        
        if (destination) {
            for (size_t index = 0; index < count; index++) {
                CGImageDestinationAddImageFromSource(destination, source, index, (__bridge CFDictionaryRef)removeExifProperties);
            }
            
            if (!CGImageDestinationFinalize(destination)) {
                NSLog(@"CGImageDestinationFinalize failed");
            }
            
            CFRelease(destination);
        }
        
        CFRelease(source);
    }
    return mutableData;
    
}

+(UIImage *)acrylic2DViewWithGradientWhereImage:(UIImage *)baseImage shouldGradient:(BOOL)shouldGrd{
    
    if (baseImage == nil) {
        return nil;
    }
    
    UIView * mainView = [[UIView alloc] init];
    UIImageView *imageView = [[UIImageView alloc] init];
    float imageWidth = baseImage.size.width;
    float imageHeight = baseImage.size.height;
    float imageRatio = imageWidth/imageHeight;
    float viewWidth = 600;
    float viewHeight = viewWidth/imageRatio;
    mainView.frame = CGRectMake(0, 0, viewWidth, viewHeight);
    float localImageWidth = viewWidth;
    float loacaImageHeight = localImageWidth/imageRatio;
    
    mainView.backgroundColor = [UIColor clearColor];
    
    imageView.frame = CGRectMake((viewWidth-localImageWidth)/2, (viewHeight-loacaImageHeight)/2, localImageWidth, loacaImageHeight);
    
    imageView.image =[self getMidiumSizeImage:baseImage imageSize:viewWidth];
    [mainView addSubview:imageView];
    
    if (shouldGrd) {
        [self getGradientImageWhereBaseImageView:imageView];
    }
    
   
    
    
    UIImage * img = [self takeSnapshotofView:mainView];
    return img;
    
}


+(void)getGradientImageWhereBaseImageView:(UIImageView *)imgView{
    UIView *gradientView;
    if (gradientView==nil) {
        gradientView=[[UIView alloc] init];
    }
    
    for (CALayer *layer in gradientView.layer.sublayers) {
        
        [layer removeFromSuperlayer];
        
    }
    
    
    float margine=imgView.frame.size.width*0.20;
    
    gradientView.frame=imgView.frame;
    
    
    
    UIBezierPath* trianglePath = [UIBezierPath bezierPath];
    [trianglePath moveToPoint:CGPointMake(margine, 0)];
    
    [trianglePath addLineToPoint:CGPointMake(gradientView.frame.size.width-(margine*1.5), gradientView.frame.size.height)];
    
    [trianglePath addLineToPoint:CGPointMake(gradientView.frame.size.width, gradientView.frame.size.height)];
    
    [trianglePath addLineToPoint:CGPointMake(gradientView.frame.size.width, 0)];
    
    
    [trianglePath closePath];
    
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    shapeLayer.path = trianglePath.CGPath;
    [gradientView.layer setMask:shapeLayer];
    
    
    CALayer *layer = gradientView.layer;
    layer.cornerRadius = 0.0f;
    layer.masksToBounds = YES;
    layer.borderWidth = 0.0f;
    layer.borderColor = [UIColor clearColor].CGColor;
    
    
    
    // Add Shine
    CAGradientLayer *shineLayer = [CAGradientLayer layer];
    shineLayer.frame = layer.bounds;
    shineLayer.colors = [NSArray arrayWithObjects:
                         (id)[UIColor colorWithRed:1 green:1 blue:1 alpha:0.4].CGColor,
                         (id)[UIColor colorWithRed:1 green:1 blue:1 alpha:0.0].CGColor,
                         
                         nil];
    shineLayer.locations = [NSArray arrayWithObjects:
                            [NSNumber numberWithFloat:0.0f],
                            [NSNumber numberWithFloat:1.0f],
                            
                            nil];
    
    [layer addSublayer:shineLayer];
    
    [imgView.superview addSubview:gradientView];
    
    
    
}




+ (UIImage*)imageByCropping:(UIImage *)imageToCrop toRect:(CGRect)rect
{
    CGImageRef imageRef = CGImageCreateWithImageInRect([imageToCrop CGImage], rect);
    UIImage *cropped = [UIImage imageWithCGImage:imageRef];
    return cropped;
    
    
}

+ (UIImage *)takeSnapshotOfView:(UIView *)view {
    UIImage *image;
    for (UIWindow *window in [UIApplication sharedApplication].windows) {
        
        
        if (window.isKeyWindow) {
            
            UIGraphicsBeginImageContextWithOptions(view.bounds.size, NO, [UIScreen mainScreen].scale);
            
            [window.rootViewController.view drawViewHierarchyInRect:view.bounds afterScreenUpdates:YES];
            
            
            image = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            break;
        }
    }
    
    return image;
}




+(void)makeView3D:(CALayer *)layer andangle:(float)angle{
    
    
    CATransform3D rotationAndPerspectiveTransform = CATransform3DIdentity;
    rotationAndPerspectiveTransform.m34 = 1.0 / -500;
    rotationAndPerspectiveTransform = CATransform3DRotate(rotationAndPerspectiveTransform, angle * M_PI / 180.0f, 0.0f, 1.0f, 0.0f);
    layer.transform = rotationAndPerspectiveTransform;
    layer.shouldRasterize = YES;
    layer.rasterizationScale = [[UIScreen mainScreen] scale];
}

+(NSString *)getDeviceId{
    
    
    
    //Use the bundle name as the App identifier. No need to get the localized version.
    
    NSString *Appname = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleName"];
    
    //Check if we have UUID already
    
    NSString *retrieveuuid = [SAMKeychain passwordForService:Appname account:@"user"];
    
    if (retrieveuuid == NULL)
    {
        
        //Create new key for this app/device
        
        CFUUIDRef newUniqueId = CFUUIDCreate(kCFAllocatorDefault);
        
        retrieveuuid = (__bridge_transfer NSString*)CFUUIDCreateString(kCFAllocatorDefault, newUniqueId);
        
        CFRelease(newUniqueId);
        
        //Save key to Keychain
        [SAMKeychain setPassword:retrieveuuid forService:Appname account:@"user"];
    }
    
    return retrieveuuid;
    
    //return  [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    
}

+(void)slidUpWithView:(UIView *)view upMargin:(float)upMargin duration:(float)duration delay :(float)delay {
    
    [UIView animateWithDuration:duration delay:delay options:UIViewAnimationOptionCurveEaseIn animations:^{
        view.frame  = CGRectMake(view.frame.origin.x, view.frame.origin.y-upMargin, view.frame.size.width,view.frame.size.height);
        
    } completion:^(BOOL finished) {
        
    }];
    
    
}

+(void)showFullImage:(NSMutableArray *)imageArray{
    
    
    NSMutableDictionary * userInfo = [[NSMutableDictionary alloc] init];
    
    userInfo[@"imageArr"]=[imageArray mutableCopy];
    
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:SHOW_FULL object:self userInfo:userInfo];
    
    
    
    
    
}

+(UIStoryboard*) getStoryBoardOfTheApp{
    UIStoryboard *storyboard;
    if(isiPhone4S){
        storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    }
    else if(isiPhone6){
        storyboard = [UIStoryboard storyboardWithName:@"Main4.7" bundle:nil];
    }
    
    else if(isiPhone6Plus){
        storyboard = [UIStoryboard storyboardWithName:@"Main5.5" bundle:nil];
    }
    else if(isiPhone10){
        storyboard = [UIStoryboard storyboardWithName:@"Main10" bundle:nil];
    }
    else if(isiPhone10R_10SMAX){
        storyboard = [UIStoryboard storyboardWithName:@"Main10R" bundle:nil];
    }
    else{
        storyboard = [UIStoryboard storyboardWithName:@"Main3.7" bundle:nil];
    }
    return storyboard;
}

+(void)showSideMenu{
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:SLIDE_MENU object:self userInfo:nil];
}


+(void)showWarningScreen:(NSString *)msg{
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:SHOW_FULL object:self userInfo:nil];
}

+(void)showTutScreen:(NSMutableArray *)frameArray{
    NSMutableDictionary * userInfo = [[NSMutableDictionary alloc] init];
    
    userInfo[@"frameArray"]=[frameArray mutableCopy];
    
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:SHOW_TUT_SCREEN object:self userInfo:userInfo];
}

+ (UIImage *)takeSnapshotofView:(UIView *)view {
    UIImage *image;
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, NO, [UIScreen mainScreen].scale);
    [view drawViewHierarchyInRect:view.bounds afterScreenUpdates:YES];
    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
    
}

+(void)setNavigationBar:(UINavigationController*)navigationController{
    [navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    if(UIUserInterfaceIdiomPad == [[UIDevice currentDevice] userInterfaceIdiom])
        [navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"topBar-ipad.png"] forBarMetrics: UIBarMetricsDefault];
    else
        [navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navigationBar-iPhon.png"] forBarMetrics: UIBarMetricsDefault];
    navigationController.navigationBar.tintColor = [UIColor whiteColor];
    // self.navigationController.navigationBar.translucent = NO;
    navigationController.interactivePopGestureRecognizer.enabled = NO;
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60)
                                                         forBarMetrics:UIBarMetricsDefault];
}


+(void) Padding:(UITextField *) textField{
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
}
+(void)roundCorner:(UIView *)view roundValue:(float)round borderWidth:(float)borderWidth borderColor:(UIColor *)color{
    
    view.layer.cornerRadius = round;
    view.layer.masksToBounds = YES;
    view.layer.borderColor = color.CGColor;
    view.layer.borderWidth = borderWidth;
    
}
+(void)roundCornerWithShadow:(UIView *)view roundValue:(float)round borderWidth:(float)borderWidth borderColor:(UIColor *)color{
    view.layer.cornerRadius = round;
    view.layer.masksToBounds = YES;
    // border
    view.layer.borderWidth = borderWidth;
    view.layer.borderColor = color.CGColor;
    
    // shadow
    view.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    view.layer.shadowOffset = CGSizeMake(3, 3);
    view.layer.shadowOpacity = 0.7;
    view.layer.shadowRadius = 2.0;
    
}


+(NSString *)convertDateIntoUTC:(NSDate *)date{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:@"dd/MM/yyyy HH:mm:ss"];
    NSString *dateString = [dateFormatter stringFromDate:date];
    return dateString;
}








+(NSString *)convertUTCIntoDate:(NSString *)date{
    
    //UTC time
    NSDateFormatter *utcDateFormatter = [[NSDateFormatter alloc] init];
    [utcDateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [utcDateFormatter setTimeZone :[NSTimeZone timeZoneForSecondsFromGMT: 0]];
    
    // utc format
    NSDate *dateInUTC = [utcDateFormatter dateFromString: date];
    
    // offset second
    NSInteger seconds = [[NSTimeZone systemTimeZone] secondsFromGMT];
    
    // format it and send
    NSDateFormatter *localDateFormatter = [[NSDateFormatter alloc] init];
    [localDateFormatter setDateFormat:@"dd-MMM-yyy"];
    [localDateFormatter setTimeZone :[NSTimeZone timeZoneForSecondsFromGMT: seconds]];
    
    // formatted string
    NSString *localDate = [localDateFormatter stringFromDate: dateInUTC];
    return localDate;
    
    
    
}

+(void)setImageOnImageView:(UIImageView *)imageView withImageUrl:(NSString *)imageUrl andImageId:(NSString *)imageId {
    UIActivityIndicatorView *loader;
    
    NSData *pngData = [NSData dataWithContentsOfFile:[self documentsPathForFileName:[NSString stringWithFormat:@"%@.png",imageId]]];
    
    UIImage *img = [[UIImage alloc] initWithData:pngData];
    if (img!=nil) {
        imageView.image = img;
        
    }
    else{
        loader =[[UIActivityIndicatorView alloc] initWithFrame:CGRectMake((imageView.frame.size.width/2)-25, (imageView.frame.size.height/2)-25, 50, 50)];
        dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(q, ^{
            
            NSData *datas =[NSData dataWithContentsOfURL:[NSURL URLWithString:imageUrl]] ;
            
            UIImage *img = [[UIImage alloc] initWithData:datas];
            dispatch_async(dispatch_get_main_queue(), ^{
                if(img!=nil)
                    imageView.image=img;
                [loader stopAnimating];
                [loader removeFromSuperview];
                
                
                [self saveImagesInDocumentDirectory:datas ImageId:imageId];
                
                
            });
        });
        
    }
    
    
    
    
    
}

+(NSString *)convertDicToJson:(NSDictionary *)dic{
    //sas dic is empty
    if (dic==nil) {
        return @"NA";
    }
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    
    if (! jsonData) {
        return @"NA";
    } else {
        return  [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
    }
}

+(UIImage *)getImageFromDoucumentWithName:(NSString *)imageName{
    if (([imageName isKindOfClass:[NSNull class]]) )
        return nil;
    
    imageName = [imageName stringByReplacingOccurrencesOfString: @".png" withString:@""];
    
    
    
    NSData *pngData = [NSData dataWithContentsOfFile:[self documentsPathForFileName:[NSString stringWithFormat:@"%@.png",imageName]]];
    UIImage *img = [[UIImage alloc] initWithData:pngData];
    img =  [UIImage imageWithData:[self removeExifFromImage:img]] ;
    if (img != nil) {
        return img;
    }
    else{
        return nil;
    }
}
+(UIImage*) getMidiumSizeImage:(UIImage*)image imageSize:(float)size{
    CGSize originalSize = image.size;
    CGSize newSize = CGSizeMake(size, (originalSize.height/originalSize.width)*size);
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage* compressedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return compressedImage;
}
+(UIImage *)getImageFromDoucumentWithUrl:(NSString *)url{
    
    NSLog(@"url from url==> %@",url);
    NSData *pngData = [NSData dataWithContentsOfFile:[NSString stringWithFormat:@"%@.png",url]];
    UIImage *img = [[UIImage alloc] initWithData:pngData];
    if (img != nil) {
        return img;
    }
    else{
        return nil;
    }
}


+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    //UIGraphicsBeginImageContext(newSize);
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}


+(UIImage*)getImageFromUrl:(NSString*)imageName{
    NSData *pngData = [NSData dataWithContentsOfFile:[NSString stringWithFormat:@"%@.png",imageName]];
    UIImage *img = [[UIImage alloc] initWithData:pngData];
    if (img != nil) {
        return img;
    }
    else{
        return nil;
    }
}
















+(NSMutableDictionary*)getGuestUserDetails{
    
    
    return [[NSUserDefaults standardUserDefaults] objectForKey:GUEST_USER_DETAIL];
    
}


+(BOOL) saveImagesInDocumentDirectory:(NSData*) pngData ImageId:(NSString*) imageId{
    
    NSString *filePath = [self documentsPathForFileName:[NSString stringWithFormat:@"%@.png",imageId]];
    
    if([[NSFileManager defaultManager] fileExistsAtPath:filePath]){
        [self deleteItemFromPath:filePath];
    }
 
    [pngData writeToFile:filePath atomically:YES];
    
    return YES;
}



+(BOOL) saveFileInDocumentDirectory:(NSData*) pngData fileName:(NSString*) fileName{
    
    NSString *filePath = [self documentsPathForFileName:[NSString stringWithFormat:@"%@",fileName]];
    
    if([[NSFileManager defaultManager] fileExistsAtPath:filePath]){
        [self deleteItemFromPath:filePath];
    }
    
    [pngData writeToFile:filePath atomically:YES];
    
    return YES;
}



+(void)deleteFileFromDocumentWithName:(NSString *)name{
    NSString *filePath = [self documentsPathForFileName:[NSString stringWithFormat:@"%@",name]];
    
    if([[NSFileManager defaultManager] fileExistsAtPath:filePath]){
        [self deleteItemFromPath:filePath];
    }
    
}


+( NSMutableArray *)getAllFileFromeCacheContain:(NSString *)str{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    
    NSError *error;
    NSArray *directoryContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsPath error:&error];
    
    NSMutableArray *arr=[[NSMutableArray alloc] init];
    
    for (int i=0; i<[directoryContents count]; i++)
    {
        NSString* fileName = [directoryContents objectAtIndex:i];
        if ([fileName rangeOfString:str].location != NSNotFound){
            arr[arr.count]=fileName;
            
            
        }
    }
    
    
    
    return arr;
    
}





+(void) deleteItemFromPath:(NSString*) imagePath{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    [fileManager removeItemAtPath:imagePath error:NULL];
}


+(BOOL) needsUpdate{
    NSDictionary* infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString* appID = infoDictionary[@"CFBundleIdentifier"];
    NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:@"http://itunes.apple.com/lookup?bundleId=%@", appID]];
    NSData* data = [NSData dataWithContentsOfURL:url];
    NSDictionary* lookup = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];

    if ([lookup[@"resultCount"] integerValue] == 1){
        NSString* appStoreVersion = lookup[@"results"][0][@"version"];
        NSString* currentVersion = infoDictionary[@"CFBundleShortVersionString"];
        if (![appStoreVersion isEqualToString:currentVersion]){
            NSLog(@"Need to update [%@ != %@]", appStoreVersion, currentVersion);
            return YES;
        }
    }
    return NO;
}


+ (NSString *)documentsPathForFileName:(NSString *)name
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    paths = nil;
    return [documentsPath stringByAppendingPathComponent:name];
}




+(void)setHashTagColor:(UITextView *)text{
    NSMutableAttributedString * string = [[NSMutableAttributedString alloc]initWithString:text.text];
    
    NSArray *words=[text.text componentsSeparatedByString:@" "];
    
    for (NSString *word in words) {
        if ([word hasPrefix:@"#"]) {
            NSRange range=[text.text rangeOfString:word];
            [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithPatternImage:[UIImage imageNamed:@"navigationBar-iPhon.png"]] range:range];
            
        }
    }
    [text setAttributedText:string];
}

+(void)deleteFilesFromCacheWhichContain:(NSString *)name{
    
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    
    NSError *error;
    NSArray *directoryContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsPath error:&error];
    

    for (int i=0; i<[directoryContents count]; i++)
    {
        NSString* fileName = [directoryContents objectAtIndex:i];
        if ([fileName rangeOfString:name].location != NSNotFound){
            [self deleteFileFromDocumentWithName:fileName ];
            
        }
    }
    
}


+ (void)alert: (UIViewController *) view title: (NSString *) title message: (NSString *) message {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
        [view dismissViewControllerAnimated:YES completion:nil];
        [view.navigationController popViewControllerAnimated:YES];
    }];
    
    [alert addAction:defaultAction];
    [view presentViewController:alert animated:YES completion:nil];
}
+(NSString*) getXibName:(NSString*) xibActualName{
    if(isiPhone6)
    {
        if([[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@4.7",xibActualName] ofType:@"nib"] != nil)
        {
            return [NSString stringWithFormat:@"%@4.7",xibActualName];
        }
        else{
            return [NSString stringWithFormat:@"%@",xibActualName];
        }
    }
    else if(isiPhone10)
    {
        if([[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@10",xibActualName] ofType:@"nib"] != nil)
        {
            return [NSString stringWithFormat:@"%@10",xibActualName];
        }
        else
        {
            if([[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@4.7",xibActualName] ofType:@"nib"] != nil)
            {
                return [NSString stringWithFormat:@"%@4.7",xibActualName];
            }
            else{
                return [NSString stringWithFormat:@"%@",xibActualName];
            }
        }
    }
    else if(isiPhone6Plus)
    {
        if([[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@5.5",xibActualName] ofType:@"nib"] != nil)
        {
            return [NSString stringWithFormat:@"%@5.5",xibActualName];
        }
        else{
            return [NSString stringWithFormat:@"%@",xibActualName];
        }
        
    }
    else if(isiPhone10R_10SMAX)
    {
        if([[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@10R",xibActualName] ofType:@"nib"] != nil)
        {
            return [NSString stringWithFormat:@"%@10R",xibActualName];
        }
        else{
            return [NSString stringWithFormat:@"%@5.5",xibActualName];
        }
        
    }
    else
    {
        return [NSString stringWithFormat:@"%@",xibActualName];
    }
}

+ (UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    
    
    
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

+ (void)startShimmeringToView:(UIView *)view
{
    id light = (id)[UIColor colorWithWhite:0 alpha:0.1].CGColor;
    id dark  = (id)[UIColor blackColor].CGColor;
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.colors = @[dark, light, dark];
    gradient.frame = CGRectMake(-view.bounds.size.width, 0, 3*view.bounds.size.width, view.bounds.size.height);
    gradient.startPoint = CGPointMake(0.0, 0.5);
    gradient.endPoint   = CGPointMake(0.8, 0.5); // slightly slanted forward
    gradient.locations  = @[@0.4, @0.5, @0.6];
    view.layer.mask = gradient;
    
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"locations"];
    animation.fromValue = @[@0.0, @0.1, @0.2];
    animation.toValue   = @[@0.8, @0.9, @1.0];
    
    animation.duration = 1.5;
    animation.repeatCount = HUGE_VALF;
    [gradient addAnimation:animation forKey:@"shimmer"];
}
+(void)stopShimmeringToView:(UIView *)view
{
    view.layer.mask = nil;
}
+(void)drawLineFromPoint:(CGRect)fromPint toPoint:(CGRect)toPoint whereLineWidth:(float)lineWidth andLineColour:(UIColor *)lineColor atBaseView:(UIView *)baseView{
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:CGPointMake(fromPint.origin.x, fromPint.origin.y)];
    [path addLineToPoint:CGPointMake(toPoint.origin.x, toPoint.origin.y)];
    
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    shapeLayer.path = [path CGPath];
    shapeLayer.strokeColor = [lineColor CGColor];
    shapeLayer.lineWidth = lineWidth;
    shapeLayer.fillColor = [[UIColor clearColor] CGColor];
    [baseView.layer addSublayer:shapeLayer];
}



+(void)showSimpleAlert:(UIViewController *)contro andTitle:(NSString *)titleTxt andMessage:(NSString *)msg {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:titleTxt message:msg preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //button click event
        
    }];
    [alert addAction:ok];
    [contro presentViewController:alert animated:YES completion:nil];
}

+(BOOL)isString:(NSString *)str contains:(NSString *)subStr{
    
    if ([str rangeOfString:subStr].location == NSNotFound) {
        return false;
    } else {
        return true;
    }
}

+(NSString *)makeFirstLatterUpperofString:(NSString *)str{
    if ([str isEqualToString:@" "] || [str isEqualToString:@"  "]) {
        return @"";
    }
    if (str.length==0 || str==nil) {
        return @"";
    }
    NSString *name=[str lowercaseString];
    name=[self removeEndSpaceFrom:name];
    NSArray *listItems = [name componentsSeparatedByString:@" "];
    
    name=@"";
    for (int i=0; i<listItems.count; i++) {
        
        
        
        NSString * strs=[NSString stringWithFormat:@"%@%@",[[listItems[i] substringToIndex:1] uppercaseString],[listItems[i] substringFromIndex:1] ];
        if (i==0) {
            name =[NSString stringWithFormat:@"%@",strs];
        }
        else{
            name =[NSString stringWithFormat:@"%@ %@",name,strs];
        }
        
        
    }
    return name;
}

+ (NSString *)removeEndSpaceFrom:(NSString *)strtoremove{
    NSUInteger location = 0;
    unichar charBuffer[[strtoremove length]];
    [strtoremove getCharacters:charBuffer];
    int i = 0;
    for(i = (int)[strtoremove length]; i >0; i--) {
        NSCharacterSet* charSet = [NSCharacterSet whitespaceCharacterSet];
        if(![charSet characterIsMember:charBuffer[i - 1]]) {
            break;
        }
    }
    return [strtoremove substringWithRange:NSMakeRange(location, i  - location)];
}



+(UIImage *)getChangedImageColorWithImage:(UIImage *)image andColor:(UIColor *)color{
    
    
    
    CGRect rect = CGRectMake(0, 0, image.size.width, image.size.height);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextClipToMask(context, rect, image.CGImage);
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    
    
    return img;
    
}
+(NSString *)hexStringFromColor:(UIColor *)color {
    const CGFloat *components = CGColorGetComponents(color.CGColor);

    CGFloat r = components[0];
    CGFloat g = components[1];
    CGFloat b = components[2];

    return [NSString stringWithFormat:@"#%02lX%02lX%02lX",
            lroundf(r * 255),
            lroundf(g * 255),
            lroundf(b * 255)];
}

+(void)makePhotoBook:(UIView *)vw1 {
    UILabel *fromLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, 15, 20, 200)];
    fromLabel.text = @"hello World";
    //fromLabel.font = customFont;
    fromLabel.numberOfLines = 1;
    fromLabel.baselineAdjustment = UIBaselineAdjustmentAlignBaselines; // or UIBaselineAdjustmentAlignCenters, or UIBaselineAdjustmentNone
    fromLabel.adjustsFontSizeToFitWidth = YES;
    fromLabel.minimumScaleFactor = 10.0f/12.0f;
    fromLabel.clipsToBounds = YES;
    fromLabel.backgroundColor = [UIColor clearColor];
    fromLabel.textColor = [UIColor blackColor];
    fromLabel.textAlignment = NSTextAlignmentCenter;
    [vw1 addSubview:fromLabel];
    
}
+ (void)makeAcrylicPhotoBook:(UIImageView *)topImg mainView:(UIView *)mainView bottomImage:(UIImageView *)bottomImgView leftImage:(UIImageView *)leftImgView rightImage:(UIImageView *)rightImgView centerImage:(UIImageView *)centerImgView mainImageHeightConstant:(NSLayoutConstraint *)_mainImageHeightConstant mainImageWidthtConstant:(NSLayoutConstraint *)_mainImageWidthConstant imageToUse:(UIImage *)imageToUse{
    
    float maxWidth = 540;
    
    float maxHeight = 585;
    UIImage  *img = imageToUse;
    
    if (img.size.height > img.size.width){
        maxHeight = maxHeight - 50;
        _mainImageHeightConstant.constant = maxHeight;
        maxWidth = (img.size.width/img.size.height) * (maxHeight);
        _mainImageWidthConstant.constant = (img.size.width/img.size.height) * (maxHeight);
//        maxWidth = mainView.frame.size.width - 100;
//        _mainImageWidthConstant.constant = maxWidth;
//        _mainImageHeightConstant.constant = (maxWidth/maxHeight) * img.size.height;
        if (((maxWidth/maxHeight) * img.size.width) > maxWidth){
//            _mainImageWidthConstant.constant = maxWidth;
//            _mainImageHeightConstant.constant = (img.size.width/img.size.height) * maxHeight;
            //
        }
    }else if (img.size.height < img.size.width){
        maxWidth = maxWidth - 20;
        _mainImageWidthConstant.constant = maxWidth;
        maxHeight = (img.size.height/img.size.width) * maxWidth;
        _mainImageHeightConstant.constant = (img.size.height/img.size.width) * maxWidth;
    }else{
        if (maxHeight > maxWidth){
            maxWidth = maxWidth - 50;
            maxHeight = maxWidth;
          //  maxWidth = maxHeight;
        }else{
            maxWidth = maxHeight - 70;
            maxHeight = maxWidth;
        }
//        maxHeight = mainView.frame.size.height - 100;
//        maxWidth = mainView.frame.size.width - 100;
        _mainImageWidthConstant.constant = maxWidth;
        _mainImageHeightConstant.constant = maxWidth;
    }
    CGRect clippedRect  = CGRectMake(0 ,0,(maxWidth/5)/maxWidth*imageToUse.size.width ,imageToUse.size.height);
    CGImageRef imageRef = CGImageCreateWithImageInRect(imageToUse.CGImage, clippedRect);
    UIImage *newImage   = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    leftImgView.image=[self blurredImageWithImage:newImage];
    leftImgView.alpha = 0.98;
    newImage = nil;
     
//    leftImgView.backgroundColor = UIColor.redColor;
//    rightImgView.backgroundColor = UIColor.blueColor;
//    topImg.backgroundColor = UIColor.greenColor;
//    bottomImgView.backgroundColor = UIColor.orangeColor;
    
    CGRect clippedRect2  = CGRectMake(imageToUse.size.width - ((maxWidth/5)/maxWidth*imageToUse.size.width) ,0,(maxWidth/5)/maxWidth*imageToUse.size.width ,imageToUse.size.height);
    CGImageRef imageRef2 = CGImageCreateWithImageInRect(imageToUse.CGImage, clippedRect2);
    UIImage *newImage2   = [UIImage imageWithCGImage:imageRef2];
    CGImageRelease(imageRef2);
    UIImage* flippedImage1 = [self blurredImageWithImage:newImage2];
    UIImage* flippedImage = [UIImage imageWithCGImage:flippedImage1.CGImage
                                                scale:flippedImage1.scale
                                          orientation:UIImageOrientationUpMirrored];
        rightImgView.image= flippedImage;
    rightImgView.alpha = 0.98;
    flippedImage = nil;
    flippedImage1 = nil;
    newImage2 = nil;
    
    CGRect clippedRect3  = CGRectMake(0 ,0,imageToUse.size.width ,((maxHeight/5)/maxHeight*imageToUse.size.height));
    CGImageRef imageRef3 = CGImageCreateWithImageInRect(imageToUse.CGImage, clippedRect3);
    UIImage *newImage3   = [UIImage imageWithCGImage:imageRef3];
    CGImageRelease(imageRef3);
    topImg.image=[self blurredImageWithImage:newImage3];
    topImg.alpha = 0.98;
    newImage3 =  nil;
    
    CGRect clippedRect4  = CGRectMake(0 ,imageToUse.size.height - ((maxHeight/5)/maxHeight*imageToUse.size.height),imageToUse.size.width ,((maxHeight/5)/maxHeight*imageToUse.size.height));
    CGImageRef imageRef4 = CGImageCreateWithImageInRect(imageToUse.CGImage, clippedRect4);
    UIImage *newImage4   = [UIImage imageWithCGImage:imageRef4];
    CGImageRelease(imageRef4);
    UIImage* flippedImageBottom =  [self blurredImageWithImage:newImage4];
    bottomImgView.image= [UIImage imageWithCGImage:flippedImageBottom.CGImage
                                             scale:flippedImageBottom.scale
                                       orientation:UIImageOrientationDownMirrored];
    bottomImgView.alpha = 1.0;
    newImage4 = nil;
    //[self blurredImageWithImage:flippedImageBottom];
   
    
    
    //
    
    //[self acrylic2DViewWithGradientWhereImage:imageToUse shouldGradient:NO];
//    UIView *centeredView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, maxHeight, maxHeight)];
//    centeredView.backgroundColor = UIColor.whiteColor;
//    centeredView.alpha  = 0.2;
//    [centerImgView addSubview:centeredView];
    UIImageView *centeredView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, maxWidth       , maxHeight)];
    centeredView.image = [UIImage imageNamed:@"glass4"];
    centerImgView.image = imageToUse;
    [centerImgView addSubview:centeredView];
    centerImgView.layer.zPosition = 0.1;
   
    
    [self setLeftImage:leftImgView];
    [self setTopImage:topImg];
    [self setRightImage:rightImgView];
    [self setBottomImage:bottomImgView];
    [self setMainView:mainView];
   
  //  [self getGradientImageWhereBaseImageView:centerImgView];
  //  [self acrylic2DViewWithGradientWhereImage:baseImage shouldGradient:YES];
   // [self addBlurToView:leftImgView];
//    [self addBlurToView:topImg];
//    [self addBlurToView:rightImgView];
//    [self addBlurToView:bottomImgView];
   
//    leftImgView.translatesAutoresizingMaskIntoConstraints = false;
//    topImg.translatesAutoresizingMaskIntoConstraints = false;
//    [leftImgView.topAnchor constraintEqualToAnchor:topImg.firstBaselineAnchor constant:2].active = YES;
}
+ (UIImage *)blurredImageWithImage:(UIImage *)sourceImage{

    //  Create our blurred image
    CIContext *context = [CIContext contextWithOptions:nil];
    CIImage *inputImage = [CIImage imageWithCGImage:sourceImage.CGImage];

    //  Setting up Gaussian Blur
    CIFilter *filter = [CIFilter filterWithName:@"CIGaussianBlur"];
    [filter setValue:inputImage forKey:kCIInputImageKey];
    [filter setValue:[NSNumber numberWithFloat:7.0f] forKey:@"inputRadius"];
    CIImage *result = [filter valueForKey:kCIOutputImageKey];

    /*  CIGaussianBlur has a tendency to shrink the image a little, this ensures it matches
     *  up exactly to the bounds of our original image */
    CGImageRef cgImage = [context createCGImage:result fromRect:[inputImage extent]];

    UIImage *retVal = [UIImage imageWithCGImage:cgImage];

    if (cgImage) {
        CGImageRelease(cgImage);
    }

    return retVal;
}
+(void)setLeftImage:(UIImageView *)_leftImage{

    float pNumber = 30*cos(2 * M_PI * (15746/23));
   // _leftImage.transform = CGAffineTransformMakeScale(0.5, 1);
//    CGAffineTransform transform = CGAffineTransformRotate(CGAffineTransformIdentity, pNumber);
//  //  transform = CGAffineTransformScale(transform, scale, scale);
//    _leftImage.transform = transform;
    CGPoint  anchorPoint = CGPointMake(1, 0);
    CGPoint newPoint = CGPointMake(_leftImage.bounds.size.width * anchorPoint.x,
                                   _leftImage.bounds.size.height * anchorPoint.y);
       CGPoint oldPoint = CGPointMake(_leftImage.bounds.size.width * _leftImage.layer.anchorPoint.x,
                                      _leftImage.bounds.size.height * _leftImage.layer.anchorPoint.y);

       newPoint = CGPointApplyAffineTransform(newPoint, _leftImage.transform);
       oldPoint = CGPointApplyAffineTransform(oldPoint, _leftImage.transform);

       CGPoint position = _leftImage.layer.position;

       position.x -= oldPoint.x;
       position.x += newPoint.x;

       position.y -= oldPoint.y;
       position.y += newPoint.y;

    _leftImage.layer.position = position;
    //_leftImage.layer.anchorPoint = anchorPoint;
//
//        CGAffineTransform transform = CGAffineTransformRotate(CGAffineTransformIdentity, pNumber);
//      //  transform = CGAffineTransformScale(transform, scale, scale);
//    float angleX = -0 * M_PI / 180;
//    CATransform3D t1 = CATransform3DMakeRotation(angleX, 0, 0, 0);
//    float angleY = 50 * M_PI / 180;
//    CATransform3D t2 = CATransform3DMakeRotation(angleY, 0, 1, 0);
//    CATransform3D combo1 = CATransform3DConcat(t1, t2);
//  //  self.labelPlay.layer.transform = combo1;
    CALayer *layer = _leftImage.layer;
    CATransform3D rotationAndPerspectiveTransform = CATransform3DIdentity;
    rotationAndPerspectiveTransform.m11 = .7;
    rotationAndPerspectiveTransform.m12 = .5;
    rotationAndPerspectiveTransform = CATransform3DRotate(rotationAndPerspectiveTransform, -1.0f * M_PI / 180.0f, 0.0f, 0.0f, 0.0f);
//    [UIView animateWithDuration:0
//                     animations:^{
           // animations go here
        layer.transform = rotationAndPerspectiveTransform;
    layer.zPosition = 2.0;
      //  self->_leftImage.layer.transform = combo1;
//    }];
   // UIView *myView = [[self leftImage] objectAtIndex:0];
}

+(void)setTopImage:(UIImageView *)_topimage{
//    UIImage *img = [UIImage imageNamed:@"deskTop"];
//    CGRect clippedRect  = CGRectMake(0 ,10,100 ,_leftImage.frame.size.height);
//     CGImageRef imageRef = CGImageCreateWithImageInRect(img.CGImage, clippedRect);
//     UIImage *newImage   = [UIImage imageWithCGImage:imageRef];
//     CGImageRelease(imageRef);
//     _leftImage.image=newImage;
    float pNumber = 30*cos(2 * M_PI * (15746/23));
   // _leftImage.transform = CGAffineTransformMakeScale(0.5, 1);
//    CGAffineTransform transform = CGAffineTransformRotate(CGAffineTransformIdentity, pNumber);
//  //  transform = CGAffineTransformScale(transform, scale, scale);
//    _leftImage.transform = transform;
    CGPoint  anchorPoint = CGPointMake(-1, 1);
    CGPoint newPoint = CGPointMake(_topimage.bounds.size.width * anchorPoint.x,
                                   _topimage.bounds.size.height * anchorPoint.y);
       CGPoint oldPoint = CGPointMake(_topimage.bounds.size.width * _topimage.layer.anchorPoint.x,
                                      _topimage.bounds.size.height * _topimage.layer.anchorPoint.y);

       newPoint = CGPointApplyAffineTransform(newPoint, _topimage.transform);
       oldPoint = CGPointApplyAffineTransform(oldPoint, _topimage.transform);

       CGPoint position = _topimage.layer.position;

       position.x -= oldPoint.x;
       position.x += newPoint.x;

       position.y -= oldPoint.y;
       position.y += newPoint.y;

    _topimage.layer.position = position;
    //_leftImage.layer.anchorPoint = anchorPoint;
//
//        CGAffineTransform transform = CGAffineTransformRotate(CGAffineTransformIdentity, pNumber);
//      //  transform = CGAffineTransformScale(transform, scale, scale);
//    float angleX = -0 * M_PI / 180;
//    CATransform3D t1 = CATransform3DMakeRotation(angleX, 0, 0, 0);
//    float angleY = 50 * M_PI / 180;
//    CATransform3D t2 = CATransform3DMakeRotation(angleY, 0, 1, 0);
//    CATransform3D combo1 = CATransform3DConcat(t1, t2);
//  //  self.labelPlay.layer.transform = combo1;
    CALayer *layer = _topimage.layer;
    CATransform3D rotationAndPerspectiveTransform = CATransform3DIdentity;
    rotationAndPerspectiveTransform.m14 = 0;
    rotationAndPerspectiveTransform.m21 = 1.5;
    rotationAndPerspectiveTransform = CATransform3DRotate(rotationAndPerspectiveTransform, 0.5f * M_PI / 180.0f, 0.0f, 0.0f, 0.0f);
//    [UIView animateWithDuration:0
//                     animations:^{
           // animations go here
        layer.transform = rotationAndPerspectiveTransform;
    layer.zPosition = 1.8;
      //  self->_leftImage.layer.transform = combo1;
//    }];
   // UIView *myView = [[self leftImage] objectAtIndex:0];
    
     
}
+(void)setRightImage:(UIImageView *)_rightImage{
//    UIImage *img = [UIImage imageNamed:@"deskTop"];
//    CGRect clippedRect  = CGRectMake(0 ,10,100 ,_leftImage.frame.size.height);
//     CGImageRef imageRef = CGImageCreateWithImageInRect(img.CGImage, clippedRect);
//     UIImage *newImage   = [UIImage imageWithCGImage:imageRef];
//     CGImageRelease(imageRef);
//     _leftImage.image=newImage;
    float pNumber = 30*cos(2 * M_PI * (15746/23));
   // _leftImage.transform = CGAffineTransformMakeScale(0.5, 1);
//    CGAffineTransform transform = CGAffineTransformRotate(CGAffineTransformIdentity, pNumber);
//  //  transform = CGAffineTransformScale(transform, scale, scale);
//    _leftImage.transform = transform;
    CGPoint  anchorPoint = CGPointMake(0.5, 0);
    CGPoint newPoint = CGPointMake(_rightImage.bounds.size.width * anchorPoint.x,
                                   _rightImage.bounds.size.height * anchorPoint.y);
       CGPoint oldPoint = CGPointMake(_rightImage.bounds.size.width * _rightImage.layer.anchorPoint.x,
                                      _rightImage.bounds.size.height * _rightImage.layer.anchorPoint.y);

       newPoint = CGPointApplyAffineTransform(newPoint, _rightImage.transform);
       oldPoint = CGPointApplyAffineTransform(oldPoint, _rightImage.transform);

       CGPoint position = _rightImage.layer.position;

       position.x -= oldPoint.x;
       position.x += newPoint.x;

       position.y -= oldPoint.y;
       position.y += newPoint.y;

    _rightImage.layer.position = position;
    //_leftImage.layer.anchorPoint = anchorPoint;
//
//        CGAffineTransform transform = CGAffineTransformRotate(CGAffineTransformIdentity, pNumber);
//      //  transform = CGAffineTransformScale(transform, scale, scale);
//    float angleX = -0 * M_PI / 180;
//    CATransform3D t1 = CATransform3DMakeRotation(angleX, 0, 0, 0);
//    float angleY = 50 * M_PI / 180;
//    CATransform3D t2 = CATransform3DMakeRotation(angleY, 0, 1, 0);
//    CATransform3D combo1 = CATransform3DConcat(t1, t2);
//  //  self.labelPlay.layer.transform = combo1;
    CALayer *layer = _rightImage.layer;
    CATransform3D rotationAndPerspectiveTransform = CATransform3DIdentity;
    rotationAndPerspectiveTransform.m11 = .7;
    rotationAndPerspectiveTransform.m12 = .5;
    rotationAndPerspectiveTransform = CATransform3DRotate(rotationAndPerspectiveTransform, -1.0f * M_PI / 180.0f, 0.0f, 0.0f, 0.0f);
//    [UIView animateWithDuration:0
//                     animations:^{
           // animations go here
        layer.transform = rotationAndPerspectiveTransform;
    layer.zPosition = 0.6;
      //  self->_leftImage.layer.transform = combo1;
//    }];
   // UIView *myView = [[self leftImage] objectAtIndex:0];
    
     
}

+(void)setBottomImage:(UIImageView *)_bottomImage{
//    UIImage *img = [UIImage imageNamed:@"deskTop"];
//    CGRect clippedRect  = CGRectMake(0 ,10,100 ,_leftImage.frame.size.height);
//     CGImageRef imageRef = CGImageCreateWithImageInRect(img.CGImage, clippedRect);
//     UIImage *newImage   = [UIImage imageWithCGImage:imageRef];
//     CGImageRelease(imageRef);
//     _leftImage.image=newImage;
    float pNumber = 30*cos(2 * M_PI * (15746/23));
   // _leftImage.transform = CGAffineTransformMakeScale(0.5, 1);
//    CGAffineTransform transform = CGAffineTransformRotate(CGAffineTransformIdentity, pNumber);
//  //  transform = CGAffineTransformScale(transform, scale, scale);
//    _leftImage.transform = transform;
    CGPoint  anchorPoint = CGPointMake(-1, 1);
    CGPoint newPoint = CGPointMake(_bottomImage.bounds.size.width * anchorPoint.x,
                                   _bottomImage.bounds.size.height * anchorPoint.y);
       CGPoint oldPoint = CGPointMake(_bottomImage.bounds.size.width * _bottomImage.layer.anchorPoint.x,
                                      _bottomImage.bounds.size.height * _bottomImage.layer.anchorPoint.y);

       newPoint = CGPointApplyAffineTransform(newPoint, _bottomImage.transform);
       oldPoint = CGPointApplyAffineTransform(oldPoint, _bottomImage.transform);

       CGPoint position = _bottomImage.layer.position;

       position.x -= oldPoint.x;
       position.x += newPoint.x;

       position.y -= oldPoint.y;
       position.y += newPoint.y;

    _bottomImage.layer.position = position;
    //_leftImage.layer.anchorPoint = anchorPoint;
//
//        CGAffineTransform transform = CGAffineTransformRotate(CGAffineTransformIdentity, pNumber);
//      //  transform = CGAffineTransformScale(transform, scale, scale);
//    float angleX = -0 * M_PI / 180;
//    CATransform3D t1 = CATransform3DMakeRotation(angleX, 0, 0, 0);
//    float angleY = 50 * M_PI / 180;
//    CATransform3D t2 = CATransform3DMakeRotation(angleY, 0, 1, 0);
//    CATransform3D combo1 = CATransform3DConcat(t1, t2);
//  //  self.labelPlay.layer.transform = combo1;
    CALayer *layer = _bottomImage.layer;
    CATransform3D rotationAndPerspectiveTransform = CATransform3DIdentity;
    rotationAndPerspectiveTransform.m14 = 0;
    rotationAndPerspectiveTransform.m21 = 1.5;
    rotationAndPerspectiveTransform = CATransform3DRotate(rotationAndPerspectiveTransform, 0.20f * M_PI / 180.0f, 0.0f, 0.24f, 0.0f);
//    [UIView animateWithDuration:0
//                     animations:^{
           // animations go here
        layer.transform = rotationAndPerspectiveTransform;
    layer.zPosition = 1;
      //  self->_leftImage.layer.transform = combo1;
//    }];
//    [_bottomImage bringSubviewToFront:_leftImage];
//    [_leftImage sendSubviewToBack:_bottomImage];
   // UIView *myView = [[self leftImage] objectAtIndex:0];
    
     
}

+(void)setMainView:(UIView *)_mainView{
//    UIImage *img = [UIImage imageNamed:@"deskTop"];
//    CGRect clippedRect  = CGRectMake(0 ,10,100 ,_leftImage.frame.size.height);
//     CGImageRef imageRef = CGImageCreateWithImageInRect(img.CGImage, clippedRect);
//     UIImage *newImage   = [UIImage imageWithCGImage:imageRef];
//     CGImageRelease(imageRef);
//     _leftImage.image=newImage;
    float pNumber = 30*cos(2 * M_PI * (15746/23));
   // _leftImage.transform = CGAffineTransformMakeScale(0.5, 1);
//    CGAffineTransform transform = CGAffineTransformRotate(CGAffineTransformIdentity, pNumber);
//  //  transform = CGAffineTransformScale(transform, scale, scale);
//    _leftImage.transform = transform;
    CGPoint  anchorPoint = CGPointMake(1, 0);
    CGPoint newPoint = CGPointMake(_mainView.bounds.size.width * anchorPoint.x,
                                   _mainView.bounds.size.height * anchorPoint.y);
       CGPoint oldPoint = CGPointMake(_mainView.bounds.size.width * _mainView.layer.anchorPoint.x,
                                      _mainView.bounds.size.height * _mainView.layer.anchorPoint.y);

       newPoint = CGPointApplyAffineTransform(newPoint, _mainView.transform);
       oldPoint = CGPointApplyAffineTransform(oldPoint, _mainView.transform);

       CGPoint position = _mainView.layer.position;

       position.x -= oldPoint.x;
       position.x += newPoint.x;

       position.y -= oldPoint.y;
       position.y += newPoint.y;

    _mainView.layer.position = position;
    //_leftImage.layer.anchorPoint = anchorPoint;
//
//        CGAffineTransform transform = CGAffineTransformRotate(CGAffineTransformIdentity, pNumber);
//      //  transform = CGAffineTransformScale(transform, scale, scale);
//    float angleX = -0 * M_PI / 180;
//    CATransform3D t1 = CATransform3DMakeRotation(angleX, 0, 0, 0);
//    float angleY = 50 * M_PI / 180;
//    CATransform3D t2 = CATransform3DMakeRotation(angleY, 0, 1, 0);
//    CATransform3D combo1 = CATransform3DConcat(t1, t2);
//  //  self.labelPlay.layer.transform = combo1;
    CALayer *layer = _mainView.layer;
    CATransform3D rotationAndPerspectiveTransform = CATransform3DIdentity;
    rotationAndPerspectiveTransform.m34 = 1.0 / -2000;
    rotationAndPerspectiveTransform.m12 = -.3;
    rotationAndPerspectiveTransform = CATransform3DRotate(rotationAndPerspectiveTransform, 30.0f * M_PI / 180.0f, 0.0f, 1.0f, 0.0f);
    
//    [UIView animateWithDuration:0
//                     animations:^{
           // animations go here
        layer.transform = rotationAndPerspectiveTransform;

      //  self->_leftImage.layer.transform = combo1;
//    }];
   // UIView *myView = [[self leftImage] objectAtIndex:0];
    
     
}
@end
