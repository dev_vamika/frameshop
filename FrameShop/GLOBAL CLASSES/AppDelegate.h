//
//  AppDelegate.h
//  FrameShop
//
//  Created by Mayank Barnwal on 02/12/19.
//  Copyright © 2019 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DBManager.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property (strong, nonatomic) UIWindow *window;
@property   (nonatomic,strong) DbManager    *db;

@end

