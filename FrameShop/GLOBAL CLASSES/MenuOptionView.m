//
//  MenuOptionView.m
//  FrameShop
//
//  Created by Saurabh Srivastav on 11/07/21.
//  Copyright © 2021 Mayank Barnwal. All rights reserved.
//

#import "MenuOptionView.h"
#import "Global.h"
#import "AppDelegate.h"
@interface MenuOptionView ()

@end

@implementation MenuOptionView

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIImage *btnImage = [UIImage imageNamed:@"cart.png"];
    btnImage = [Global getMidiumSizeImage:btnImage imageSize:35];
    [_cartBtn setImage:btnImage forState:UIControlStateNormal];
    
    [Global roundCorner:_cartCount roundValue:_cartCount.frame.size.height/2 borderWidth:0.5 borderColor:[UIColor whiteColor]];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishCart:) name:FINISH_UPLOADING object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishCarts) name:CART_REFRESH object:nil];
    // Do any additional setup after loading the view from its nib.
}


-(void)getCartCount{
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    NSString *urlString=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,GET_CART_ITEMS];
    
    NSMutableArray *arr=[[NSMutableArray alloc] init];
    arr=[APP_DELEGATE.db getDataForField:USERTABLE where:nil];
    
    if (arr.count!=0) {
        
        
        [api sendGetRequst:urlString andToken:arr[0][@"token"]];
    }
   
}

-(void)didFinishCarts{
    
    int count=[[[NSUserDefaults standardUserDefaults] valueForKey:CART_COUNT] intValue];
    if (count>9) {
        _cartCount.text=@"9+";
    }
    else{
        _cartCount.text=[NSString stringWithFormat:@"%d",[[[NSUserDefaults standardUserDefaults] valueForKey:CART_COUNT] intValue]];
    }
    _cartCount.hidden=[_cartCount.text isEqualToString:@"0"];
    [self getCartCount];
}

-(void)didFinishCart:(NSNotification*)notification {
    NSDictionary *dic=notification.userInfo;
    
    
    
    AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
    
    [self.navigationController popToRootViewControllerAnimated:YES];
    
    
    
    if ([dic[@"code"] isEqualToString:@"2"]) {
        [self performSelector:@selector(callOrderHistory) withObject:self afterDelay:0.3 ];
    }
    else{
        [self performSelector:@selector(callCartView) withObject:self afterDelay:0.3 ];
    }
    
    int count=[[[NSUserDefaults standardUserDefaults] valueForKey:CART_COUNT] intValue];
    
    if (count>9) {
        _cartCount.text=@"9+";
    }
    else{
        _cartCount.text=[NSString stringWithFormat:@"%d",[[[NSUserDefaults standardUserDefaults] valueForKey:CART_COUNT] intValue]];
    }
    _cartCount.hidden=[_cartCount.text isEqualToString:@"0"];
    
    [self getCartCount];
}

- (IBAction)cartBtn:(id)sender{
    [Global callMenuViewWithCode:@"0"];
}
- (IBAction)menuBtn:(id)sender{
    [Global showSideMenu];
}

#pragma mark - Call Menu
-(void)callCartView{
    [Global callMenuViewWithCode:@"0"];
}

-(void)callOrderHistory{
    [Global callMenuViewWithCode:@"2"];
}
#pragma mark - POP API DELEGATES
    -(void)connectionError{
        

    }
    -(void)requestFailedSessionExpired:(NSDictionary *)dataPack{
        
    }
    -(void)requestFailedServerError:(NSDictionary *)dataPack{
        
    }
    -(void)requestFailedServiceUnavailableDueToMaintenance:(NSDictionary *)dataPack{
      
        // [Global showSimpleAlert:self andTitle:FRAMESHOP andMessage:dataPack[@"message"]];
    }
    -(void)requestFailedMethodNotFound:(NSDictionary *)dataPack{
     
    }
    -(void)requestFailedNotFound:(NSDictionary *)dataPack{
        int count=[[[NSUserDefaults standardUserDefaults] valueForKey:CART_COUNT] intValue];
        if (count>9) {
            _cartCount.text=@"9+";
        }
        else{
            _cartCount.text=[NSString stringWithFormat:@"%d",[[[NSUserDefaults standardUserDefaults] valueForKey:CART_COUNT] intValue]];
        }
        _cartCount.hidden=[_cartCount.text isEqualToString:@"0"];
    }
    -(void)requestFailedUnauthorised:(NSDictionary *)dataPack{
        
    }
    -(void)requestFailedBadRequest:(NSDictionary *)dataPack{
        
    }
    -(void)requestSucceededWithData:(NSDictionary *)dataPack{
       
       
      if ([dataPack[@"url"] isEqualToString:GET_CART_ITEMS]){
            NSArray *arr=[dataPack[@"data"] mutableCopy];
            _cartCount.text=[NSString stringWithFormat:@"%lu",(unsigned long)arr.count];
            [[NSUserDefaults standardUserDefaults] setValue:_cartCount.text forKey:CART_COUNT];
            
            int count=[[[NSUserDefaults standardUserDefaults] valueForKey:CART_COUNT] intValue];
            if (count>9) {
                _cartCount.text=@"9+";
            }
            else{
                _cartCount.text=[NSString stringWithFormat:@"%d",[[[NSUserDefaults standardUserDefaults] valueForKey:CART_COUNT] intValue]];
            }
            _cartCount.hidden=[_cartCount.text isEqualToString:@"0"];
            
            if ([_cartCount.text intValue] == 0){
                [Global deleteFilesFromCacheWhichContain:@"cart_product"];
                [Global deleteFilesFromCacheWhichContain:@"frame"];
                [Global deleteFilesFromCacheWhichContain:@"preview"];
            }
            
        }
        
        
        
    }
    -(void)requestSucceededButActionFailed:(NSDictionary *)dataPack{
        
        
    }
    -(void)uploadedData:(NSString *)pecentage andByte:(float)bytes{
        
    }
    -(void)requestSucceededButNoDataFound:(NSDictionary *)dataPack{
        
    }
    -(void)requestFailedDueToServerIsuueWithError:(NSString*)errorDes{
        
        
        
    }

@end
