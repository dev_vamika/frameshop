//
//  SceneDelegate.h
//  FrameShop
//
//  Created by Mayank Barnwal on 02/12/19.
//  Copyright © 2019 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SplashSubview.h"
#import "Global.h"
#import "FrameViewController.h"
#import "MenuViewController.h"
#import "TutorialViewController.h"
#import "ProductDetailViewController.h"
#import "OrderHistoryViewController.h"
#import "CartPageViewController.h"
@interface SceneDelegate : UIResponder <UIWindowSceneDelegate,MenuViewControllerDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

