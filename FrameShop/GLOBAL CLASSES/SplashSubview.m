//
//  SplashSubview.m
//  FrameShop
//
//  Created by Saurabh anand on 03/12/19.
//  Copyright © 2019 Mayank Barnwal. All rights reserved.
//

#import "SplashSubview.h"

@interface SplashSubview (){
    
}

@end

@implementation SplashSubview

- (void)viewDidLoad {
    [super viewDidLoad];
  //  self.view.backgroundColor=APP_BLUE_COLOR;
    
   
    
    
    [self performSelector:@selector(onTick:) withObject:nil afterDelay:1.0];
}

-(void)dealloc {
    _frame1.image = nil;
    _frame2.image = nil;
    _frame3.image = nil;
}

-(void)onTick:(NSTimer *)timer {
    //do smth
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}
@end
