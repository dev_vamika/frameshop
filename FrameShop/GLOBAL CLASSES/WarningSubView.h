//
//  WarningSubView.h
//  FrameShop
//
//  Created by Saurabh Srivastav on 19/03/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface WarningSubView : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *iconImage;
@property (weak, nonatomic) IBOutlet UILabel *titleText;
@property (weak, nonatomic) IBOutlet UILabel *subTitleTxt;
@property (strong, nonatomic) IBOutlet UIView *noInterNetView;
@property (weak, nonatomic) IBOutlet UIImageView *noNetIcon;
@property (weak, nonatomic) IBOutlet UILabel *noNetTitle;
@property (weak, nonatomic) IBOutlet UILabel *noNetSubTitle;
@property (strong, nonatomic) IBOutlet UIView *noCartView;
@property (weak, nonatomic) IBOutlet UIImageView *noCartIcon;
@property (weak, nonatomic) IBOutlet UILabel *noCartTitle;
@property (weak, nonatomic) IBOutlet UILabel *noCartSubTiTle;
@property (weak, nonatomic) IBOutlet UIView *msgView;

@property NSString *msgType;
@end

NS_ASSUME_NONNULL_END
