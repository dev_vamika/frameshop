//
//  AppDelegate.m
//  FrameShop
//
//  Created by Mayank Barnwal on 02/12/19.
//  Copyright © 2019 Mayank Barnwal. All rights reserved.
//

#import "AppDelegate.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>

#import <Rollbar/Rollbar.h>
#import "BraintreeCore.h"

#import <OneSignal/OneSignal.h>

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    //[FBSDKSettings setAutoLogAppEventsEnabled:NO];
  
    _db = [[DbManager alloc] init];
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    [[NSUserDefaults standardUserDefaults] setInteger:[[NSUserDefaults standardUserDefaults] integerForKey:LAUNCH_COUNT]+1 forKey:LAUNCH_COUNT];
    [Rollbar initWithAccessToken:ROLLBAR_ACCESS_TOKEN];
    [BTAppSwitch setReturnURLScheme:@"frameshop.app.payments"];
    [Rollbar infoWithMessage:@"iOS"];
    //Remove this method to stop OneSignal Debugging
    [OneSignal setLogLevel:ONE_S_LL_VERBOSE visualLevel:ONE_S_LL_NONE];
    
    //START OneSignal initialization code
    [OneSignal initWithLaunchOptions:launchOptions
                               appId:ONESIGNAL_APP_ID
            handleNotificationAction:nil
                            settings:@{kOSSettingsKeyAutoPrompt: @false, kOSSettingsKeyInAppLaunchURL: @false}];
    OneSignal.inFocusDisplayType = OSNotificationDisplayTypeNotification;
    
    // The promptForPushNotificationsWithUserResponse function will show the iOS push notification prompt. We recommend removing the following code and instead using an In-App Message to prompt for notification permission (See step 6)
    [OneSignal promptForPushNotificationsWithUserResponse:^(BOOL accepted) {
        NSLog(@"User accepted notifications: %d", accepted);
    }];
   
    [_db createDatabase:[NSFileManager defaultManager]];
    [Global deleteFilesFromCacheWhichContain:@"border"];
    [Global deleteFilesFromCacheWhichContain:@"frame"];
    [Global deleteFilesFromCacheWhichContain:@"preview"];
   
    [Global deleteFilesFromCacheWhichContain:@"product"];
    [Global deleteFilesFromCacheWhichContain:@"fb"];
    
    [_db alterTable:USER_PRODUCT Colomn:@"core" DefaultValue:@""];
    [_db alterTable:FRAME_TABLE Colomn:@"location" DefaultValue:@""];
    [_db alterTable:USER_PRODUCT Colomn:@"color" DefaultValue:@""];
    
    [FBSDKAppEvents
         logEvent:@"Sas Event"];
    
  
    
    //END OneSignal initializataion code
    return YES;
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
            options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options {
    if ([url.scheme localizedCaseInsensitiveCompare:@"frameshop.app.payments"] == NSOrderedSame) {
        [BTAppSwitch handleOpenURL:url options:options];
    }
    return NO;
}


#pragma mark - UISceneSession lifecycle


- (UISceneConfiguration *)application:(UIApplication *)application configurationForConnectingSceneSession:(UISceneSession *)connectingSceneSession options:(UISceneConnectionOptions *)options API_AVAILABLE(ios(13.0)) {
    // Called when a new scene session is being created.
    // Use this method to select a configuration to create the new scene with.
    return [[UISceneConfiguration alloc] initWithName:@"Default Configuration" sessionRole:connectingSceneSession.role];
}


- (void)application:(UIApplication *)application didDiscardSceneSessions:(NSSet<UISceneSession *> *)sceneSessions API_AVAILABLE(ios(13.0)) {
    
}


@end
