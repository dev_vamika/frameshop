#import "SceneDelegate.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "GiftCardViewController.h"
#import "BraintreeCore.h"
#import "WebViewController.h"
#import "MenuOptionView.h"
@interface SceneDelegate (){
    SplashSubview *splash;
    MenuSubView *sideMenu;
    MenuOptionView *menu;
}

@end

@implementation SceneDelegate


- (void)scene:(UIScene *)scene openURLContexts:(NSSet<UIOpenURLContext *> *)URLContexts  API_AVAILABLE(ios(13.0)){
    UIOpenURLContext *openURLContext = URLContexts.allObjects.firstObject;
    if (openURLContext) {
        [[FBSDKApplicationDelegate sharedInstance] application:UIApplication.sharedApplication
                                                       openURL:openURLContext.URL
                                             sourceApplication:openURLContext.options.sourceApplication
                                                    annotation:openURLContext.options.annotation];
    }
    for (UIOpenURLContext *urlContext in URLContexts) {
        NSURL *url = [urlContext URL];
        if ([url.scheme localizedCaseInsensitiveCompare:@"frameshop.app"] == NSOrderedSame) {
            [BTAppSwitch handleOpenURLContext:urlContext];
        }
    }
    
}

- (void)scene:(UIScene *)scene willConnectToSession:(UISceneSession *)session options:(UISceneConnectionOptions *)connectionOptions  API_AVAILABLE(ios(13.0)){
    
    DbManager *db=[[DbManager alloc] init];
    [db createDatabase:[NSFileManager defaultManager]];
    
    
    
    
    splash = [[SplashSubview alloc] initWithNibName:@"SplashSubview" bundle:nil];
    splash.view.frame = CGRectMake(0, 0, self.window.rootViewController.view.frame.size.width, self.window.rootViewController.view.frame.size.height);
   
    
    
    [self.window.rootViewController.view addSubview:splash.view];
   
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showSideMenu:) name:SLIDE_MENU object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showFullScreen:) name:FULL_IMAGE object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChooseSideMenuOption:) name:SLIDE_MENU_OPTION object:nil];
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showTutScreen:) name:SHOW_TUT_SCREEN object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishCart:) name:FINISH_UPLOADING object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishCarts) name:CART_REFRESH object:nil];
    [self needsUpdate];
   
    
    float y = 0.0f;
    
    if (@available(iOS 11.0, *)) {
        UIWindow *window = UIApplication.sharedApplication.windows.firstObject;
        y = window.safeAreaInsets.top;
        //CGFloat bottomPadding = window.safeAreaInsets.bottom;
    }
    menu = [[MenuOptionView alloc] init];
    menu.view.frame = CGRectMake(MAIN_SCREEN.size.width*2/3 , y, MAIN_SCREEN.size.width/3, 44);
    //[self .window.rootViewController addChildViewController:<#(nonnull UIViewController *)#>];
    [self.window.rootViewController.view addSubview:menu.view];
    
    [menu.cartBtn addTarget:self action:@selector(cartBtn:) forControlEvents:UIControlEventTouchUpInside];
    [menu.menuBtn addTarget:self action:@selector(menuBtn:) forControlEvents:UIControlEventTouchUpInside];
    [menu getCartCount];
    
   // [menu didMoveToParentViewController:self.window.rootViewController];
    
    
}
-(void)didFinishCarts{
    [menu getCartCount];
//    int count=[[[NSUserDefaults standardUserDefaults] valueForKey:CART_COUNT] intValue];
//    if (count>9) {
//        _cartCount.text=@"9+";
//    }
//    else{
//        _cartCount.text=[NSString stringWithFormat:@"%d",[[[NSUserDefaults standardUserDefaults] valueForKey:CART_COUNT] intValue]];
//    }
//    _cartCount.hidden=[_cartCount.text isEqualToString:@"0"];
//    [self getCartCount];
}

-(void)didFinishCart:(NSNotification*)notification {
    [menu getCartCount];
}
- (void) cartBtn:(UIButton *) sender {

    [Global callMenuViewWithCode:@"0"];

}
- (void) menuBtn:(UIButton *) sender {

    [Global showSideMenu];

}

-(void)showFullScreen:(NSNotification*)notification{
    NSDictionary *dics=notification.userInfo;
   
    
    
    NSString * storyboardName = @"Main";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
    ProductImagePreviewVC * vc = (ProductImagePreviewVC *)[storyboard instantiateViewControllerWithIdentifier:@"ProductImagePreviewVC"];
    vc.modalPresentationStyle= UIModalPresentationOverFullScreen;
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
        dic[@"image"] = dics[@"img"];
    vc.previewImageDic = dic;
    
    [self.window.rootViewController presentViewController:vc animated:NO completion:nil];
    
    
    
//    ProductImagePreviewVC *productImagePreviewVC = [segue destinationViewController];
//    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
//    dic[@"image"] = sender;
//    productImagePreviewVC.previewImageDic = dic;
}

-(void)showSideMenu:(NSNotification*)notification{
    
    
    NSString * storyboardName = @"Main";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
    MenuViewController * vc = (MenuViewController *)[storyboard instantiateViewControllerWithIdentifier:@"MenuViewController"];
    vc.modalPresentationStyle=UIModalPresentationFullScreen;
    vc.delegate=self;
    
    [self.window.rootViewController presentViewController:vc animated:NO completion:nil];
    
    
}


-(void)showTutScreen:(NSNotification*)notification{
    
    
    NSString * storyboardName = @"Main";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
    TutorialViewController * vc = (TutorialViewController *)[storyboard instantiateViewControllerWithIdentifier:@"TutorialViewController"];
    vc.frameArray=notification.userInfo[@"frameArray"];
    vc.modalPresentationStyle= UIModalPresentationOverFullScreen;
  
    
    [self.window.rootViewController presentViewController:vc animated:NO completion:nil];
    
    
}



-(void)didChooseSideMenuOption:(NSNotification*)notification{
    
    NSDictionary *dic=notification.userInfo;
    NSString * storyboardName = @"Main";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
    UINavigationController *navController;
    
    switch ([dic[@"code"] intValue]) {
        case 2:{
            OrderHistoryViewController *loginVC = (OrderHistoryViewController *) [storyboard instantiateViewControllerWithIdentifier:@"OrderHistoryViewController"];
            navController = [[UINavigationController alloc] initWithRootViewController:loginVC];
            
        }
            break;
        case 3:{
            
            GiftCardViewController *loginVC = (GiftCardViewController *) [storyboard instantiateViewControllerWithIdentifier:@"GiftCardViewController"];
            navController = [[UINavigationController alloc] initWithRootViewController:loginVC];
            
        }
            break;
        case 0:{
            CartPageViewController *loginVC = (CartPageViewController *) [storyboard instantiateViewControllerWithIdentifier:@"CartPageViewController"];
            navController = [[UINavigationController alloc] initWithRootViewController:loginVC];
            
        }
            break;
        case 4:{
            
            WebViewController *loginVC = (WebViewController *) [storyboard instantiateViewControllerWithIdentifier:@"WebViewController"];
            loginVC.url = FAQ_URL;
            loginVC.type = @"FAQ";
            navController = [[UINavigationController alloc] initWithRootViewController:loginVC];
            
            //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:FAQ_URL] options:@{} completionHandler:nil];

            
        }
            break;
//        case 1:{
//            WebViewController *webVC = (WebViewController *) [storyboard instantiateViewControllerWithIdentifier:@"WebViewController"];
//            webVC.url = FAQ_URL;
//            webVC.type = @"FAQ";
//            navController = [[UINavigationController alloc] initWithRootViewController:webVC];
//
//        }
//            break;
        case 1:{
            WebViewController *webVC = (WebViewController *) [storyboard instantiateViewControllerWithIdentifier:@"WebViewController"];
            webVC.url = CONTACT_URL;
            webVC.type = @"Contact-Us";
            navController = [[UINavigationController alloc] initWithRootViewController:webVC];
            
        }
            break;
//        case 5:{
//            WebViewController *webVC = (WebViewController *) [storyboard instantiateViewControllerWithIdentifier:@"WebViewController"];
//            webVC.url = PRIVACY_URL;
//            webVC.type = @"Privacy Policy";
//            navController = [[UINavigationController alloc] initWithRootViewController:webVC];
//
//        }
//            break;
//        case 6:{
//            WebViewController *webVC = (WebViewController *) [storyboard instantiateViewControllerWithIdentifier:@"WebViewController"];
//            webVC.url = TERMS_AND_CONDITION_URL;
//            webVC.type = @"Terms & Conditions";
//            navController = [[UINavigationController alloc] initWithRootViewController:webVC];
//
//        }
            break;
        default:
            
            
            break;
    }
    if (navController != nil) {
        navController.modalPresentationStyle=UIModalPresentationPopover;
        [self.window.rootViewController presentViewController:navController animated:YES completion:nil];
    }
    
    
    
    
    
}

- (void)sceneDidDisconnect:(UIScene *)scene  API_AVAILABLE(ios(13.0)){
    
}


- (void)sceneDidBecomeActive:(UIScene *)scene  API_AVAILABLE(ios(13.0)){
    
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
//    NSString *documentsPath = [paths objectAtIndex:0];
//    NSLog(@"Document Path: %@",documentsPath);
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"acess" object:nil];//pushyes
    
   
  
    
}

-(void) needsUpdate{
    
    
    dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(q, ^{
        
       
        NSDictionary* infoDictionary = [[NSBundle mainBundle] infoDictionary];
        NSString* appID = infoDictionary[@"CFBundleIdentifier"];
        NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:@"http://itunes.apple.com/lookup?bundleId=%@", appID]];
        NSData* data = [NSData dataWithContentsOfURL:url];
       
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (data!=nil) {
             
            NSDictionary* lookup = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];

            if ([lookup[@"resultCount"] integerValue] == 1){
                NSString* appStoreVersion = lookup[@"results"][0][@"version"];
                NSString* currentVersion = infoDictionary[@"CFBundleShortVersionString"];
                if (![appStoreVersion isEqualToString:currentVersion]){
                    
                    NSString *  msg = [NSString stringWithFormat:@"A new version %@ of %@ is available for download. Would you like to download it now ?",appStoreVersion,infoDictionary[@"CFBundleName"]];
                    
                    //[Global alert:self.window.rootViewController title:@"Frameshop" message:msg];
                    
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"UPDATE" message:msg preferredStyle:UIAlertControllerStyleAlert];

                    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                            //button click event
                        UIApplication *application = [UIApplication sharedApplication];
                        NSURL *URL = [NSURL URLWithString:APPLINK];
                        [application openURL:URL options:@{} completionHandler:^(BOOL success) {
                            if (success) {
                                 NSLog(@"Opened url");
                            }
                        }];
                        
                        
                                        }];
                    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
                    [alert addAction:cancel];
                    [alert addAction:ok];
                    [self.window.rootViewController presentViewController:alert animated:YES completion:nil];
                    
                    
                    NSLog(@"Need to update [%@ != %@]", appStoreVersion, currentVersion);
                    
                }
            }
            }
        });
    });
    
    
    
    
    
    
   
  
  
}





- (UIImage *)pb_takeSnapshot {
    UIGraphicsBeginImageContextWithOptions(self.window.rootViewController.view.bounds.size, NO, [UIScreen mainScreen].scale);
    
    [self.window.rootViewController.view drawViewHierarchyInRect:self.window.rootViewController.view.bounds afterScreenUpdates:YES];
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}


- (void)sceneWillResignActive:(UIScene *)scene  API_AVAILABLE(ios(13.0)){
    // Called when the scene will move from an active state to an inactive state.
    // This may occur due to temporary interruptions (ex. an incoming phone call).
}


- (void)sceneWillEnterForeground:(UIScene *)scene  API_AVAILABLE(ios(13.0)){
    // Called as the scene transitions from the background to the foreground.
    // Use this method to undo the changes made on entering the background.
}


- (void)sceneDidEnterBackground:(UIScene *)scene  API_AVAILABLE(ios(13.0)){
    // Called as the scene transitions from the foreground to the background.
    // Use this method to save data, release shared resources, and store enough scene-specific state information
    // to restore the scene back to its current state.
    [splash.view removeFromSuperview];
    
    
}

@end
