//
//  UploadImageViewController.h
//  FrameShop
//
//  Created by Saurabh Srivastav on 24/03/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopApi.h"
#import "Global.h"
#import "DBManager.h"
NS_ASSUME_NONNULL_BEGIN
@protocol UploadImageViewControllerDelegate <NSObject>
@optional
-(void)didFinishUploading;
-(void)didFailUploading;


@end
@interface UploadImageViewController : UIViewController<PopApiDelegate>
@property(strong,nonatomic)id<UploadImageViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIImageView *displayImage;
@property (weak, nonatomic) IBOutlet UILabel *messageLbl;
@property (weak, nonatomic) IBOutlet UIProgressView *progressBar;
@property (weak, nonatomic) IBOutlet UILabel *pecentageLbl;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityLoader;
@property (weak, nonatomic) IBOutlet UIButton *refreshAction;

@end

NS_ASSUME_NONNULL_END
