//
//  UploadImageViewController.m
//  FrameShop
//
//  Created by Saurabh Srivastav on 24/03/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import "UploadImageViewController.h"
#import "Reachability.h"
@interface UploadImageViewController (){
    DbManager *db;
    UIImage * frameImage, *preview,*main;
    NSTimer *imageTimer,*msgTimer,*uploaderTimer;
    int imageCount,msgCount,perc,serverCount;
    NSMutableArray *imgArray,*msgArray;
    float totalLength,uploadedByte;
}

@end

@implementation UploadImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    db=[[DbManager alloc] init];
    imageCount=0;
    msgCount=0;
    perc=0;
    serverCount=0;
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    frameImage=[Global getImageFromDoucumentWithName:@"frame_preview"];
    preview=[Global getImageFromDoucumentWithName:@"preview"];
    main=[Global getImageFromDoucumentWithName:@"frame"];
    
    _displayImage.image=[Global getMidiumSizeImage:preview imageSize:[UIScreen mainScreen].bounds.size.height];
    
    imgArray=[[NSMutableArray alloc] init];
    imgArray[imgArray.count]=frameImage;//[Global getMidiumSizeImage:frameImage imageSize:500];
    imgArray[imgArray.count]=preview;//[Global getMidiumSizeImage:preview imageSize:500];
    imgArray[imgArray.count]=main;//[Global getMidiumSizeImage:main imageSize:500];
    
    msgArray=[[NSMutableArray alloc] init];
    msgArray[msgArray.count]=@"your image is being uploaded";
    msgArray[msgArray.count]=@"Please do not close app while uploading";
    msgArray[msgArray.count]=@"Almost there...... ";
    _pecentageLbl.text=[NSString stringWithFormat:@"%@%%",@"0"];
    [self startProcess];
    
}

-(void)startProcess{
    imageTimer = [NSTimer scheduledTimerWithTimeInterval: 3.0
                                                  target: self
                                                selector:@selector(onImage)
                                                userInfo: nil repeats:YES];
    
    msgTimer = [NSTimer scheduledTimerWithTimeInterval: 2.5
                                                target: self
                                              selector:@selector(onMessage)
                                              userInfo: nil repeats:YES];
    
    uploaderTimer = [NSTimer scheduledTimerWithTimeInterval: 2
                                                     target: self
                                                   selector:@selector(onUpload)
                                                   userInfo: nil repeats:YES];
    
    [_activityLoader startAnimating];
    
    _progressBar.progress=0;
    [self sendDataToServer];
}


-(void)sendDataToServer{
    
    
    NSData *imgData = UIImagePNGRepresentation(preview);
    totalLength=(float)[imgData length];
     NSLog(@"File size is : %.2f KB ",(float)imgData.length/1024.0f);
    imgData =UIImagePNGRepresentation(main);
  totalLength=totalLength+ (float)[imgData length];
     NSLog(@"File size is : %.2f KB ",(float)imgData.length/1024.0f);
    
     NSLog(@"Total File size is : %.2f KB ",(float)totalLength/1024.0f);
    
    [self saveImageToserverWhereImage:frameImage andId:[[NSUserDefaults standardUserDefaults] valueForKey:CART_ID] andType:@"thumb"];
    [self saveImageToserverWhereImage:main andId:[[NSUserDefaults standardUserDefaults] valueForKey:CART_ID] andType:@"original"];
    
}

-(void)signUpUser{
    
    
    
    
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
   dic[@"name"]=[[UIDevice currentDevice] name];
    dic[@"email"]=[NSString stringWithFormat:@"%@@frameshop.com.au",[Global getDeviceId]];
    dic[@"password"]=@"qwxcjeub32ne";
    dic[@"c_password"]=@"qwxcjeub32ne";
    dic[@"url"]=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,REGISTER_USER];
    [api sendPostRequst:dic];
    
}
-(void)logInUser{
    
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
    
   dic[@"email"]=[NSString stringWithFormat:@"%@@frameshop.com.au",[Global getDeviceId]];
    dic[@"password"]=@"qwxcjeub32ne";
    
    dic[@"url"]=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,LOGIN_USER];
    [api sendPostRequst:dic];
}



-(void)saveImageToserverWhereImage:(UIImage *)img andId:(NSString *)orderId andType:(NSString *)type{
    PopApi * api=[[PopApi alloc] init];
    NSString *urlString=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,UPLOAD_IMAGE];
    api.delegate=self;
    NSMutableArray *arr=[[NSMutableArray alloc] initWithArray:[db getDataForField:USERTABLE where:nil]];
    //arr=[db getDataForField:USERTABLE where:nil];
    
    
    
    if (arr.count!=0) {
        
        [api uploadImage:img orderId:orderId url:urlString accessToken:arr[0][@"token"] type:type];
    }
    
    
}


-(void)onImage {
    imageCount++;
    if (imageCount==imgArray.count) {
        imageCount=0;
    }
    
    
    [UIView transitionWithView:_displayImage
                      duration:1.0
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
        self->_displayImage.image=   [Global getMidiumSizeImage:self->imgArray[self->imageCount] imageSize:[UIScreen mainScreen].bounds.size.height];
    } completion:^(BOOL finished) {
        
    }];
    
    
    //    [UIView animateWithDuration:1.0 delay:0.0 options:UIViewAnimationOptionTransitionFlipFromLeft animations:^{
    //        self->_displayImage.image= self->imgArray[self->imageCount];
    //
    //           } completion:^(BOOL finished) {
    //
    //           }];
    
}
-(void)onMessage {
    msgCount++;
    if (msgCount==msgArray.count) {
        msgCount=0;
    }
    
    
    [UIView transitionWithView:_messageLbl
                      duration:1.0
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
        self->_messageLbl.text= self->msgArray[self->msgCount];
    } completion:^(BOOL finished) {
        
    }];
    
    
    
    
}
-(void)onUpload {
    if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus] == 0)
    {
        [self connectionError];
        
    }
   
    
    
    
}

-(void)disMissScreen{
    [Global deleteFilesFromCacheWhichContain:@"frame"];
    [Global deleteFilesFromCacheWhichContain:@"preview"];
    int count=[[[NSUserDefaults standardUserDefaults] valueForKey:CART_COUNT] intValue]+1;
    [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%d",count] forKey:CART_COUNT];
    [self dismissViewControllerAnimated:NO completion:^{
        [self.delegate didFinishUploading];
    }];
    
}

#pragma mark -Pop api Delegates

-(void)connectionError{
    [msgTimer invalidate];
    [imageTimer invalidate];
    [uploaderTimer invalidate];
    [_activityLoader stopAnimating];
    _pecentageLbl.text=@"";
    _progressBar.progress=0;
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:FRAMESHOP message:NO_INTERNET_MSG preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Reload" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self startProcess];
        
    }];
    
    UIAlertAction *no = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:NO completion:nil];
        
    }];
    
    
    [alert addAction:ok];
    [alert addAction:no];
    [self presentViewController:alert animated:YES completion:nil];
    
    
}
-(void)requestFailedSessionExpired:(NSDictionary *)dataPack{
    
}
-(void)requestFailedServerError:(NSDictionary *)dataPack{
    
}
-(void)requestFailedServiceUnavailableDueToMaintenance:(NSDictionary *)dataPack{
   [Global showSimpleAlert:self andTitle:FRAMESHOP andMessage:dataPack[@"message"]];
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)requestFailedMethodNotFound:(NSDictionary *)dataPack{
    
}
-(void)requestFailedNotFound:(NSDictionary *)dataPack{
    
}
-(void)requestFailedUnauthorised:(NSDictionary *)dataPack{
     [self logInUser];
}
-(void)requestFailedBadRequest:(NSDictionary *)dataPack{
    
}
-(void)requestSucceededWithData:(NSDictionary *)dataPack{
    
    if ([dataPack[@"url"] isEqualToString:LOGIN_USER]) {
        
        [db delTableWithTableName:USERTABLE];
        NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
        
        dic[@"id"]              =[NSString stringWithFormat:@"%@",dataPack[@"data"][@"id"]];
        dic[@"name"]            =dataPack[@"data"][@"name"];
        dic[@"email"]           =dataPack[@"data"][@"email"];
        dic[@"created_at"]      =dataPack[@"data"][@"created_at"];
        dic[@"updated_at"]      =dataPack[@"data"][@"updated_at"];
        dic[@"token"]           =dataPack[@"data"][@"access_token"];
        
        [db insertIntoTable:dic table_name:USERTABLE];
        [self sendDataToServer];
    }
    
    else if ([dataPack[@"url"] isEqualToString:REGISTER_USER]){
        
        NSMutableDictionary *dic =[[NSMutableDictionary alloc] init];
        dic[@"token"]            =dataPack[@"data"][@"token"];
        dic[@"id"]               =[NSString stringWithFormat:@"%@",dic[@"id"]];
        
        [db insertIntoTable:dic table_name:USERTABLE];
        [self sendDataToServer];
    }
    else{
        
        serverCount++;
        if (serverCount==2) {
            [msgTimer invalidate];
            [imageTimer invalidate];
            [uploaderTimer invalidate];
            _progressBar.progress=1;
            _pecentageLbl.text=@"100%";
            [_activityLoader stopAnimating];
            [self performSelector:@selector(disMissScreen) withObject:nil afterDelay:1.5];
            
        }
        
    }
    
    
    
    
    
}
-(void)requestSucceededButActionFailed:(NSDictionary *)dataPack{
    
}
-(void)uploadedData:(NSString *)pecentage andByte:(float)bytes{
     
    uploadedByte=uploadedByte+bytes;
   
      NSLog(@"Upload data is : %.2f %% ",(uploadedByte/1024.0f)/((float)totalLength/1024.0f)*100);
     NSLog(@"Total Uploaded data is : %.2f KB ",(uploadedByte/1024.0f));
    
    
    float value=(uploadedByte/1024.0f)/((float)totalLength/1024.0f)*100;
    if (value>100) {
        
    }
    else{
        _pecentageLbl.text=[NSString stringWithFormat:@"%.0f%%",value];
                  _progressBar.progress= (float)value/100;
    }
        //   NSLog(@"======> %d %d %f",value,perc,(float)perc/100);
          
      
       
    
    
    
    
}
-(void)requestSucceededButNoDataFound:(NSDictionary *)dataPack{
    
}
-(void)requestFailedDueToServerIsuueWithError:(NSString*)errorDes{
    
}


@end
