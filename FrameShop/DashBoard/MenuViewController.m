//
//  MenuViewController.m
//  FrameShop
//
//  Created by Saurabh anand on 04/12/19.
//  Copyright © 2019 Mayank Barnwal. All rights reserved.
//

#import "MenuViewController.h"
#import "Global.h"
@interface MenuViewController (){
    MenuSubView *menuView;
    UIView *restView;
    UIScrollView *scroll;
}
@end

@implementation MenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIImage *img=[self pb_takeSnapshot];
    restView=[[UIView alloc] init];
    restView.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    
    UIImageView *imgeView=[[UIImageView alloc] init];
    imgeView.frame=CGRectMake(0, 0, restView.frame.size.width, restView.frame.size.height);
    imgeView.image=img;
    [restView addSubview:imgeView];
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [restView addGestureRecognizer:singleFingerTap];
    [Global addshadow:restView radious:5];
    self->restView.alpha=1.0f;
    UISwipeGestureRecognizer * swiperight=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swiperight:)];
    swiperight.direction=UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swiperight];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    scroll=[[UIScrollView alloc] init];
    scroll.frame=CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height);
    
    [scroll addSubview:restView];
    
    _sideView.frame=CGRectMake(restView.frame.size.width+20, 0, self.view.bounds.size.width*0.80, self.view.bounds.size.height);
    [scroll addSubview:_sideView];
    scroll.pagingEnabled=YES;
    scroll.bounces=NO;
    scroll.showsHorizontalScrollIndicator=false;
    scroll.contentSize=CGSizeMake(restView.frame.size.width+_sideView.frame.size.width+20, 0);
    [self.view addSubview:scroll];
    scroll.delegate=self;
    
    
    
    menuView=[[MenuSubView alloc] init];
    menuView.view.frame=CGRectMake(0, 0, self.sideView.frame.size.width, self.sideView.frame.size.height);
    
    [self.sideView addSubview:menuView.view];
    menuView.delegate=self;
    
    [self moveSlide:YES];
    [self addChildViewController:menuView];
    
    
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    [self moveSlide:NO];
    
    
}

-(void)moveSlide:(BOOL)shouldVisible{
    if (shouldVisible) {
        CGPoint bottomOffset = CGPointMake(scroll.contentSize.width - scroll.bounds.size.width + scroll.contentInset.right, 0);
        
        
        [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
            [self->scroll setContentOffset:bottomOffset animated:YES];
            
        } completion:^(BOOL finished) {
            
        }];
        
    }
    else{
        CGPoint bottomOffset = CGPointMake(0, 0);
        
        [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            
            [self->scroll setContentOffset:bottomOffset animated:NO];
        } completion:^(BOOL finished) {
            [self dismissViewControllerAnimated:NO completion:nil];
        }];
        
        
        
    }
}

-(void)swiperight:(UISwipeGestureRecognizer*)gestureRecognizer
{
    //Do what you want here
    [self moveSlide:NO];
}


- (UIImage *)pb_takeSnapshot {
    UIImage *image;
    for (UIWindow *window in [UIApplication sharedApplication].windows) {
        
        
        if (window.isKeyWindow) {
            
            UIGraphicsBeginImageContextWithOptions(window.rootViewController.view.bounds.size, NO, [UIScreen mainScreen].scale);
            
            [window.rootViewController.view drawViewHierarchyInRect:window.rootViewController.view.bounds afterScreenUpdates:YES];
            
            
            image = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            break;
        }
    }
    
    return image;
}

#pragma mark - MenuSubview Delegate
-(void)didSelectMenuOption:(NSDictionary *)infoDic{
    
    
    CGPoint bottomOffset = CGPointMake(0, 0);
    
    [UIView animateWithDuration:0.1 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        
        [self->scroll setContentOffset:bottomOffset animated:NO];
    } completion:^(BOOL finished) {
        [self dismissViewControllerAnimated:NO completion:nil];
      
            NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
            [nc postNotificationName:SLIDE_MENU_OPTION object:self userInfo:infoDic];
        
       
        
    }];
      
}

#pragma mark - ScrollView Delegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    if (scrollView.contentOffset.x<=0) {
        [self dismissViewControllerAnimated:NO completion:nil];
    }
}
@end
