//
//  ProductDetailViewController.h
//  FrameShop
//
//  Created by Saurabh anand on 05/12/19.
//  Copyright © 2019 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <WebKit/WebKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ProductDetailViewController : UIViewController<UIScrollViewDelegate,WKNavigationDelegate,WKUIDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *productScroll;
@property (weak, nonatomic) IBOutlet UILabel *mainTitleLbl;

@property (weak, nonatomic) IBOutlet UILabel *productName;
@property (weak, nonatomic) IBOutlet UILabel *productDetail;
@property (nonatomic, weak) IBOutlet UIImageView *mainImageView;

@property (weak, nonatomic) IBOutlet UIButton *closeBtn;
@property (weak, nonatomic) IBOutlet WKWebView *webViewDis;
- (IBAction)closeBtn:(id)sender;




@end

NS_ASSUME_NONNULL_END
