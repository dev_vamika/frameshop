//
//  ProductDetailViewController.m
//  FrameShop
//
//  Created by Saurabh anand on 05/12/19.
//  Copyright © 2019 Mayank Barnwal. All rights reserved.
//

#import "ProductDetailViewController.h"
#import "Global.h"
#import "DBManager.h"
@interface ProductDetailViewController (){
    UIScrollView *scrollView;
    NSString *productId;

    DbManager *db;
    UIImage *imageFromBack;
    NSMutableDictionary *productInfo;
}

@end

@implementation ProductDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    db=[[DbManager alloc] init];
    productInfo=[[NSMutableDictionary alloc] init];
    
    
    
    _productScroll.frame = CGRectMake(10, 80, [UIScreen mainScreen].bounds.size.width-20, [UIScreen mainScreen].bounds.size.height-90);
    
   
    

    

    
    productId=[[NSUserDefaults standardUserDefaults] valueForKey:PRODUCTID];
    
    NSMutableArray *productArray=[db getDataForField:PRODUCT_TABLE where:[NSString stringWithFormat:@"id ='%@'",productId]];
    
    if (productArray.count!=0) {
        productInfo=productArray[0];
    }
    self.productName.text=productInfo[@"showcase_title"] ;
    [self.productName sizeToFit];
    _productName.frame = CGRectMake(0, 5, _productScroll.frame.size.width, _productName.frame.size.height+10);
    float height=[productInfo[@"image_height"] floatValue];
    float width=[productInfo[@"image_width"] floatValue];
    float ratio=height/width;
    height=ratio*(_productScroll.frame.size.width-20);
    
    self.mainImageView.frame=CGRectMake(10, _productName.frame.origin.y+_productName.frame.size.height, (_productScroll.frame.size.width-20),height);
    
    self.mainImageView.image=imageFromBack;
    
    [_productScroll addSubview:_productName];
    [_productScroll addSubview:_mainImageView];
    //_mainImageView.backgroundColor = APP_GRAY_COLOR;
    
    
    self.mainTitleLbl.textColor = APP_BLUE_COLOR;
   
    
    
    self.mainImageView.image=[Global getImageFromDoucumentWithName:[NSString stringWithFormat:@"product_%@",productInfo[@"id"]]];
    
    
    _productDetail.text = productInfo[@"description"];
    [_productDetail sizeToFit];
    _productDetail.frame = CGRectMake(10, _mainImageView.frame.size.height+_mainImageView.frame.origin.y+10, _productScroll.frame.size.width-20, _productDetail.frame.size.height+30);
    [_productScroll addSubview:_productDetail];
    
    
    
    if (_mainImageView.image==nil) {

        dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(q, ^{
            //[Global startShimmeringToView:cell.contentView];
            NSData *datas;

            datas =[NSData dataWithContentsOfURL:[NSURL URLWithString:self->productInfo[@"main_image"]]] ;
            [Global saveImagesInDocumentDirectory:datas ImageId:[NSString stringWithFormat:@"product_%@",self->productInfo[@"id"]]];


            UIImage *img = [[UIImage alloc] initWithData:datas];
            dispatch_async(dispatch_get_main_queue(), ^{
                if(img!=nil)
                    self.mainImageView.image=img;

            });
        });
    }
    
    
    _webViewDis.navigationDelegate = self;
    
    
    _webViewDis.hidden = YES;
    _webViewDis.frame = _productDetail.frame;
    [_productScroll addSubview:_webViewDis];
    _productScroll.contentSize = CGSizeMake(0, _productDetail.frame.origin.y+_productDetail.frame.size.height+30);
    
}


-(void)viewWillAppear:(BOOL)animated{
   
//    dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
//    dispatch_async(q, ^{
//
//        NSData *datas,*data;
//
//            datas =[NSData dataWithContentsOfURL:[NSURL URLWithString:@"https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"]] ;
//        data =[NSData dataWithContentsOfURL:[NSURL URLWithString:@"https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"]] ;
//
//        [Global saveFileInDocumentDirectory:datas fileName:@"styleCss.css"];
//        [Global saveFileInDocumentDirectory:data fileName:@"javaScript.js"];
//       dispatch_async(dispatch_get_main_queue(), ^{
//           [self addCustomCssAndJs];
//        });
//    });
    
    
}

-(void)addCustomCssAndJs {
    NSString *contentCss = [self readTxtFromFileWhereFileName:@"styleCss" andFileType:@"css"];
    
  //  NSString *pathCss = [[NSBundle mainBundle] pathForResource:@"bootstrap-fa" ofType:@"css"];
   // NSString *contentCss = [NSString stringWithContentsOfFile:pathCss encoding:NSUTF8StringEncoding error:nil];
    
    NSString *css = @"javascript:(function() {var parent = document.getElementsByTagName('head').item(0); var style = document.createElement('style'); style.type = 'text/css'; style.innerHTML = window.atob('%@'); parent.appendChild(style)})()";
    
    NSString *cssBase64 = [self encodeStringTo64:contentCss];
    WKUserScript *cssScript = [[WKUserScript alloc] initWithSource:[NSString stringWithFormat:css, cssBase64] injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:YES];
    
    [_webViewDis.configuration.userContentController addUserScript:cssScript];
    
    NSString *js = @"javascript:(function() { var parent = document.getElementsByTagName('head').item(0); var script = document.createElement('script'); script.type = 'text/javascript'; script.innerHTML = window.atob('%@'); parent.appendChild(script)})()";
    
   // NSString *pathJs = [[NSBundle mainBundle] pathForResource:@"bootstrap" ofType:@"js"];
    NSString *contentJs = [self readTxtFromFileWhereFileName:@"javaScript" andFileType:@"js"];
    
    NSString *jsBase64 = [self encodeStringTo64:contentJs];
    WKUserScript *jsScript = [[WKUserScript alloc] initWithSource:[NSString stringWithFormat:js, jsBase64] injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:YES];
    
    [_webViewDis.configuration.userContentController addUserScript:jsScript];
    
    NSString *htmlString = [NSString stringWithFormat:@"%@",productInfo[@"description"]];
    [_webViewDis loadHTMLString:htmlString baseURL:nil];
   
   
  
}

-(NSString*)encodeStringTo64:(NSString*)fromString {
    NSData *plainData = [fromString dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64String;
    
    if ([plainData respondsToSelector:@selector(base64EncodedStringWithOptions:)]) {
        base64String = [plainData base64EncodedStringWithOptions:kNilOptions];
    }

    return base64String;
}

-(NSString *)readTxtFromFileWhereFileName:(NSString *)fileName andFileType:(NSString *)fileType{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];

        NSString *myPathDocs =  [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.%@",fileName,fileType]];
    

        if (![[NSFileManager defaultManager] fileExistsAtPath:myPathDocs])
        {
            NSString *myPathInfo = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@",fileName] ofType:[NSString stringWithFormat:@"%@",fileType]];
            NSFileManager *fileManager = [NSFileManager defaultManager];
            [fileManager copyItemAtPath:myPathInfo toPath:myPathDocs error:NULL];
        }

        //Load from File
    NSString *myString = [[NSString alloc] initWithContentsOfFile:myPathDocs encoding:NSUTF8StringEncoding error:NULL];
    return myString;
}

//- (void)webView:(WKWebView *)webView didFinishNavigation:(null_unspecified WKNavigation *)navigation{
//
//    NSString *cssUrlSrting = @"https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css";
//    NSString *jsUrlSrting = @"https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js";
//    NSString *source = [NSString stringWithFormat:@"var link = document.createElement('link');link.href = '\(%@)';link.rel= 'stylesheet'document.head.appendChild(link);",cssUrlSrting];
//    WKUserScript *cssScript = [[WKUserScript alloc] initWithSource:source injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:true];
//    WKUserContentController *userContentController = [[WKUserContentController alloc] init];
//    [userContentController addUserScript:cssScript];
//    WKWebViewConfiguration * configuration = [[WKWebViewConfiguration alloc] init];
//    [configuration setUserContentController:userContentController];
//    [self.webViewDis.configuration.userContentController addUserScript:cssScript];
//
//
//
//
//
//
//}


- (IBAction)closeBtn:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
