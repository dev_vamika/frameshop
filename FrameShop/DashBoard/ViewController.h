//
//  ViewController.h
//  FrameShop
//
//  Created by Mayank Barnwal on 02/12/19.
//  Copyright © 2019 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <StoreKit/SKStoreReviewController.h>
#import "Global.h"
#import "ProductTableViewCell.h"
#import "LoaderTableViewCell.h"


#import "PopApi.h"

#import "AlbumViewController.h"
#import <AudioToolbox/AudioServices.h>
#import "WarningSubView.h"
#define MENU_ANIMATION_TIME 0.2f
@interface ViewController : UIViewController<ProductTableViewCellDelegate,PopApiDelegate>


@property (weak, nonatomic) IBOutlet UITableView *tableView;




@end

