//
//  TutorialViewController.h
//  FrameShop
//
//  Created by Saurabh Srivastav on 24/06/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TutorialViewController : UIViewController
@property NSMutableArray *frameArray;
@property (weak, nonatomic) IBOutlet UILabel *demoLbl;
- (IBAction)nextBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *nxtBtn;
- (IBAction)skipAction:(id)sender;
@end

NS_ASSUME_NONNULL_END
