//
//  TutorialViewController.m
//  FrameShop
//
//  Created by Saurabh Srivastav on 24/06/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import "TutorialViewController.h"
#import "Global.h"
#define degreesToRadians(x) (M_PI * x / 180.0)
@interface TutorialViewController (){
    int count;
    UIView *view;
    UIBezierPath *path;
    CAShapeLayer *shape,*shape1;
}
@property (nonatomic, strong) NSMutableArray<CAShapeLayer *> *shapeLayers;
@end

@implementation TutorialViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
 
   view=[[UIView alloc] init];
    
    count=0;
    
    [Global roundCorner:_nxtBtn roundValue:3 borderWidth:0 borderColor:nil];
    
    
  
    
    
   
    
    
    // Do any additional setup after loading the view.
}


-(void)viewWillAppear:(BOOL)animated{
   
   [self showTut];
    
}

- (void)createOpenArrowPathFrom:(CGPoint)point1 to:(CGPoint)point3  conftrolPoint:(CGPoint)point2 {
    // create new curve shape layer

    [path removeAllPoints];
    [shape removeFromSuperlayer];
    [shape1 removeFromSuperlayer];
    
    
    
 path = [UIBezierPath bezierPath];

  [path moveToPoint:point1];
  [path addQuadCurveToPoint:point3 controlPoint:point2];

    
    
  shape = [CAShapeLayer layer];
  shape.path = path.CGPath;
  shape.lineWidth = 2;
  shape.strokeColor = [UIColor whiteColor].CGColor;
  shape.fillColor = [UIColor clearColor].CGColor;
  shape.frame = self.view.bounds;
  [self.view.layer addSublayer:shape];
    
    CGFloat angle = atan2f(point3.y - point2.y, point3.x - point2.x);
    
    
    CGFloat distance = 5.0;
    path = [UIBezierPath bezierPath];
    [path moveToPoint:point3];
    [path addLineToPoint:[self calculatePointFromPoint:point3 angle:angle + M_PI_2 distance:distance]]; // to the right
    [path addLineToPoint:[self calculatePointFromPoint:point3 angle:angle          distance:distance]]; // straight ahead
    [path addLineToPoint:[self calculatePointFromPoint:point3 angle:angle - M_PI_2 distance:distance]]; // to the left
    [path closePath];

    
    CABasicAnimation *pathAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    pathAnimation.duration = 0.2;
    pathAnimation.fromValue = @(0.0f);
    pathAnimation.toValue = @(1.0f);
    [shape addAnimation:pathAnimation forKey:@"strokeEnd"];
    
    [self performSelector:@selector(addArrow) withObject:self afterDelay:0.3 ];

    
    
}

-(void)addArrow{
    shape1 = [CAShapeLayer layer];
    shape1.path = path.CGPath;
    shape1.lineWidth = 0.5;
    shape1.strokeColor = [UIColor whiteColor].CGColor;
    shape1.fillColor = [UIColor whiteColor].CGColor;
    shape1.frame = self.view.bounds;
    [self.view.layer addSublayer:shape1];
}

- (CGPoint)calculatePointFromPoint:(CGPoint)point angle:(CGFloat)angle distance:(CGFloat)distance {
    return CGPointMake(point.x + cosf(angle) * distance, point.y + sinf(angle) * distance);
}

-(void)showTut{
    if (count==_frameArray.count) {
        [self dismissViewControllerAnimated:NO completion:nil];
        return;
    }
    
    if (count==_frameArray.count-1) {
        [_nxtBtn setTitle:@"Got It!" forState:UIControlStateNormal];
    }
    
    NSArray *arr = [view subviews];
     
      for (UIView* b in arr)
            {
               
                  
                    [b removeFromSuperview];
                   
               
              
            }
    
    
    
    
    UIImageView *img=(UIImageView *)_frameArray[count][@"view"];
        
          view.frame=CGRectMake(CGRectFromString( _frameArray[count][@"frame"] ).origin.x, CGRectFromString( _frameArray[count][@"frame"] ).origin.y, CGRectFromString( _frameArray[count][@"frame"] ).size.width+10, CGRectFromString( _frameArray[count][@"frame"] ).size.height+10)   ;
             view.backgroundColor=[UIColor whiteColor];
             [self.view addSubview:view];
          img.frame=CGRectMake(5, 5, CGRectFromString( _frameArray[count][@"frame"] ).size.width, CGRectFromString( _frameArray[count][@"frame"] ).size.height);
          [view addSubview:img];
          
        
          _demoLbl.frame=CGRectMake(view.frame.origin.x, view.frame.origin.y+view.frame.size.height, 100, 50);
      
           _demoLbl.text=_frameArray[count][@"dis"];
         
         
          
          [_demoLbl sizeToFit];
          
          _demoLbl.frame=CGRectMake(view.frame.origin.x+view.frame.size.width-_demoLbl.frame.size.width, _demoLbl.frame.origin.y, _demoLbl.frame.size.width, _demoLbl.frame.size.height);
          
         
          [Global roundCorner:view roundValue:view.frame.size.height/2 borderWidth:0 borderColor:nil];
          
          _nxtBtn.frame=CGRectMake(_demoLbl.frame.origin.x+(_demoLbl.frame.size.width-_nxtBtn.frame.size.width)/2, _demoLbl.frame.origin.y+_demoLbl.frame.size.height+10, _nxtBtn.frame.size.width, _nxtBtn.frame.size.height);
    
    
    CGPoint point1=CGPointMake(_nxtBtn.frame.origin.x-10, _nxtBtn.frame.origin.y-10);
    
    CGPoint point2=CGPointMake(_demoLbl.frame.origin.x-100, _demoLbl.frame.origin.y);
    CGPoint point3=CGPointMake(view.frame.origin.x-10, view.frame.origin.y
                               +20);
     [self createOpenArrowPathFrom:point1 to:point3 conftrolPoint:point2];
    
    count++;
}

- (IBAction)nextBtnAction:(id)sender {
    
    [self showTut];
    
    
}
- (IBAction)skipAction:(id)sender {
     [self dismissViewControllerAnimated:NO completion:nil];
}
@end
