//
//  MenuViewController.h
//  FrameShop
//
//  Created by Saurabh anand on 04/12/19.
//  Copyright © 2019 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MenuSubView.h"

NS_ASSUME_NONNULL_BEGIN
@class MenuViewController;
@protocol MenuViewControllerDelegate <NSObject>
@optional
-(void)didSelectMenuOptions:(NSDictionary *)infoDic;

@end

@interface MenuViewController : UIViewController<MenuSubViewDelegate,UIScrollViewDelegate>
@property(strong,nonatomic)id<MenuViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIView *sideView;
@property (weak, nonatomic) IBOutlet UIImageView *mainImage;

@end

NS_ASSUME_NONNULL_END
