//
//  ViewController.m
//  FrameShop
//
//  Created by Mayank Barnwal on 02/12/19.
//  Copyright © 2019 Mayank Barnwal. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"
#import "Validation.h"
@interface ViewController ()<UITableViewDelegate,UITableViewDataSource>{
    BOOL isLoaded;
    
    NSMutableArray *dataArray;
    UIRefreshControl *refreshControl;
    WarningSubView *warningView;
    //NSMutableArray *frameArray;
    NSMutableDictionary *cellDic;
}
-(void)dealloc;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
   // NSArray *arr = [[NSArray alloc] init];
    
      //  NSLog(@"row===%ld",(long)row);
    
    dataArray=[[NSMutableArray alloc] init];

    cellDic = [[NSMutableDictionary alloc] init];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    // frameArray=[[NSMutableArray alloc] init];
    
    UISwipeGestureRecognizer * swipeleft=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeleft:)];
    swipeleft.direction=UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeleft];
    
    
   // [Global roundCorner:_cartCount roundValue:_cartCount.frame.size.height/2 borderWidth:2 borderColor:[UIColor whiteColor]];
    [self.tableView registerNib:[UINib nibWithNibName:@"ProductTableViewCell" bundle:nil]
         forCellReuseIdentifier:@"ProductTableViewCell"];
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [_tableView addSubview:refreshControl];
    
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishCart:) name:FINISH_UPLOADING object:nil];
  //  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishCarts) name:CART_REFRESH object:nil];
    
   // _cartCount.text=@"0";
    
    
    
    
    NSMutableArray *userInfo=[APP_DELEGATE.db getDataForField:USERTABLE where:nil];
    
    
    if (userInfo.count!=0) {
        [self loadProductData];
    }
    else
    {
        [self signUpUser];
    }
    
    
}



-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [Global deleteFilesFromCacheWhichContain:@"ig_"];
    [Global deleteFilesFromCacheWhichContain:@"fb_"];
    [Global deleteFilesFromCacheWhichContain:@"frame"];
    [Global deleteFilesFromCacheWhichContain:@"preview"];
    
    [self.navigationItem setBackBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil]];
    _tableView.hidden=false;
    
//    if ([[NSUserDefaults standardUserDefaults] valueForKey:CART_COUNT]==nil || [_cartCount.text isEqualToString:@"0"]) {
//        _cartCount.hidden=YES;
//    }
//
//    int count=[[[NSUserDefaults standardUserDefaults] valueForKey:CART_COUNT] intValue];
//    if (count>9) {
//        _cartCount.text=@"9+";
//    }
//    else{
//        _cartCount.text=[NSString stringWithFormat:@"%d",[[[NSUserDefaults standardUserDefaults] valueForKey:CART_COUNT] intValue]];
//    }
//    _cartCount.hidden=[_cartCount.text isEqualToString:@"0"];
    
    
    //     [self showTutScreen];
    if ([[NSUserDefaults standardUserDefaults] integerForKey:LAUNCH_COUNT]%5 ==0) {
        //        [self performSegueWithIdentifier:@"ratepopup" sender:nil];
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:LAUNCH_COUNT];
        [SKStoreReviewController requestReview];
    }
    // [Global deleteFilesFromCacheWhichContain:@"frame"];
}

-(void)dealloc {
    _tableView.delegate = nil;
    _tableView.dataSource = nil;
}
//-(void)showTutScreen{
//
//    CGFloat bottomPadding = 0;
//
//    if (@available(iOS 11.0, *)) {
//
//        for (UIWindow *window in [UIApplication sharedApplication].windows) {
//
//
//            if (window.isKeyWindow) {
//
//                bottomPadding = window.safeAreaInsets.top;
//
//                NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
//                UIImageView *img=[[UIImageView alloc] init];
//                img.frame=CGRectMake(self.view.frame.size.width-100, bottomPadding, 25, 25);
//                img.image=[UIImage imageNamed:@"cart.png"];
//                dic[@"view"]=img;
//                dic[@"frame"]=NSStringFromCGRect( img.frame );
//                dic[@"dis"]=@"Press here to open Cart";
//                frameArray[frameArray.count]=dic;
//
//                NSMutableDictionary *dic2=[[NSMutableDictionary alloc] init];
//
//                UIImageView *img2=[[UIImageView alloc] init];
//                img2.frame=CGRectMake(self.view.frame.size.width-60, bottomPadding, 25, 25);
//                img2.image=[UIImage imageNamed:@"menu.png"];
//                dic2[@"view"]=img2;
//                dic2[@"frame"]=NSStringFromCGRect( img2.frame );
//                dic2[@"dis"]=@"Press here to open Menu";
//                frameArray[frameArray.count]=dic2;
//
//
//                [Global showTutScreen:frameArray];
//
//
//                break;
//            }
//        }
//
//
//    }
//}


//-(void)startTute{
//
//    NSIndexPath *indexPath=[NSIndexPath indexPathForRow:0 inSection:0];
//    ProductTableViewCell *cell = (ProductTableViewCell *)[_tableView cellForRowAtIndexPath:indexPath];
//
//
//
//    CGFloat bottomPadding = 0;
//    if (@available(iOS 11.0, *)) {
//
//        for (UIWindow *window in [UIApplication sharedApplication].windows) {
//
//
//            if (window.isKeyWindow) {
//
//                bottomPadding = window.safeAreaInsets.top;
//
//                {
//                    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
//                    UIImageView *img=[[UIImageView alloc] init];
//                    img.frame=CGRectMake(self.view.frame.size.width-100, bottomPadding, 25, 25);
//                    img.image=[UIImage imageNamed:@"cart.png"];
//                    dic[@"view"]=img;
//                    dic[@"frame"]=NSStringFromCGRect( img.frame );
//                    dic[@"dis"]=@"Press here to open Cart";
//                    frameArray[frameArray.count]=dic;
//                }
//
//
//                {
//                    NSMutableDictionary *dic2=[[NSMutableDictionary alloc] init];
//
//                    UIImageView *img2=[[UIImageView alloc] init];
//                    img2.frame=CGRectMake(self.view.frame.size.width-60, bottomPadding, 25, 25);
//                    img2.image=[UIImage imageNamed:@"menu.png"];
//                    dic2[@"view"]=img2;
//                    dic2[@"frame"]=NSStringFromCGRect( img2.frame );
//                    dic2[@"dis"]=@"Press here to open Menu";
//                    frameArray[frameArray.count]=dic2;
//                }
//
//                {
//                    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
//                    UIImageView *img=[[UIImageView alloc] init];
//                    img.frame= [cell.contentView convertRect:cell.infoBtn.frame toView:_tableView];
//
//                    CGRect tem=img.frame;
//                    tem.origin.y=bottomPadding+self.navigationController.navigationBar.frame.size.height+img.frame.origin.y+5;
//                    tem.origin.x=img.frame.origin.x+5;
//
//                    img.frame=tem;
//
//                    img.image=[UIImage imageNamed:@"infoProduct.png"];
//                    dic[@"view"]=img;
//                    dic[@"frame"]=NSStringFromCGRect( img.frame );
//                    dic[@"dis"]=@"Press here for product description";
//                    frameArray[frameArray.count]=dic;
//                }
//
//                {
//                    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
//                    UIImageView *img=[[UIImageView alloc] init];
//                    img.frame= [cell.contentView convertRect:cell.frameBtn.frame toView:_tableView];
//
//                    CGRect tem=img.frame;
//                    tem.origin.y=bottomPadding+self.navigationController.navigationBar.frame.size.height+img.frame.origin.y+5;
//                    tem.origin.x=img.frame.origin.x+5;
//
//                    img.frame=tem;
//
//                    img.image=[Global takeSnapshotofView:cell.frameBtn];
//                    dic[@"view"]=img;
//                    dic[@"frame"]=NSStringFromCGRect( img.frame );
//                    dic[@"dis"]=@"Press here start framing";
//                    frameArray[frameArray.count]=dic;
//
//
//                }
//
//
//
//
//                //                           NSMutableDictionary *dic2=[[NSMutableDictionary alloc] init];
//                //
//                //                          UIImageView *img2=[[UIImageView alloc] init];
//                //                                         img2.frame=CGRectMake(self.view.frame.size.width-60, bottomPadding, 25, 25);
//                //                                         img2.image=[UIImage imageNamed:@"menu.png"];
//                //                                         dic2[@"view"]=img2;
//                //                                         dic2[@"frame"]=NSStringFromCGRect( img2.frame );
//                //                          dic2[@"dis"]=@"Press here to open Menu";
//                //                                         frameArray[frameArray.count]=dic2;
//
//
//
//
//
//                break;
//            }
//        }
//
//
//    }
//
//    [Global showTutScreen:frameArray];
//
//}



#pragma mark - Notifications

-(void)didFinishCarts{

//    int count=[[[NSUserDefaults standardUserDefaults] valueForKey:CART_COUNT] intValue];
//    if (count>9) {
//        _cartCount.text=@"9+";
//    }
//    else{
//        _cartCount.text=[NSString stringWithFormat:@"%d",[[[NSUserDefaults standardUserDefaults] valueForKey:CART_COUNT] intValue]];
//    }
//    _cartCount.hidden=[_cartCount.text isEqualToString:@"0"];
}


-(void)didFinishCart:(NSNotification*)notification {
    NSDictionary *dic=notification.userInfo;



    AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);

    [self.navigationController popToRootViewControllerAnimated:YES];



    if ([dic[@"code"] isEqualToString:@"2"]) {
        [self performSelector:@selector(callOrderHistory) withObject:self afterDelay:0.3 ];
    }
    else{
        [self performSelector:@selector(callCartView) withObject:self afterDelay:0.3 ];
    }

    


}

#pragma mark - Call Menu
-(void)callCartView{
    [Global callMenuViewWithCode:@"0"];
}

-(void)callOrderHistory{
    [Global callMenuViewWithCode:@"2"];
}


#pragma mark - Refresh View Method

- (void)refresh:(UIRefreshControl *)refreshControl
{
    
    [warningView.view removeFromSuperview];
    
    [self loadProductData];
}



#pragma mark - API Methods

//-(void)getCartCount{
//    PopApi *api=[[PopApi alloc] init];
//    api.delegate=self;
//    NSString *urlString=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,GET_CART_ITEMS];
//
//    NSMutableArray *arr=[[NSMutableArray alloc] init];
//    arr=[APP_DELEGATE.db getDataForField:USERTABLE where:nil];
//
//    if (arr.count!=0) {
//
//
//        [api sendGetRequst:urlString andToken:arr[0][@"token"]];
//    }
//    else
//        [self logInUser];
//}


-(void)loadProductData{
    [Global deleteFilesFromCacheWhichContain:@"pro"];
    [APP_DELEGATE.db delTableWithTableName:PRODUCT_TABLE];
    [self->refreshControl endRefreshing];
    [self shouldLoaderOn:YES];
    self->dataArray=[[APP_DELEGATE.db getDataForField:PRODUCT_TABLE where:nil] mutableCopy];
    
    if (self->dataArray.count!=0) {
        [self shouldLoaderOn:NO];
        [self->_tableView reloadData];
        
        
    }
    
    
    
    
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    NSString *urlString=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,GET_PRODUCTS];
    
    NSMutableArray *arr=[[NSMutableArray alloc] init];
    arr=[APP_DELEGATE.db getDataForField:USERTABLE where:nil];
    
    if (arr.count!=0) {
        [api sendGetRequst:urlString andToken:arr[0][@"token"]];
    }
    else{
        [self logInUser];
    }
    
}


-(void)signUpUser{
    [self shouldLoaderOn:YES];
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
    dic[@"name"]=[[UIDevice currentDevice] name];;
    
    dic[@"email"]=[NSString stringWithFormat:@"%@@frameshop.com.au",[Global getDeviceId]];
    dic[@"password"]=@"qwxcjeub32ne";
    dic[@"c_password"]=@"qwxcjeub32ne";
    dic[@"url"]=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,REGISTER_USER];
    [api sendPostRequst:dic];
}
-(void)logInUser{
    [self shouldLoaderOn:YES];
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
    
    dic[@"email"]=[NSString stringWithFormat:@"%@@frameshop.com.au",[Global getDeviceId]];
    dic[@"password"]=@"qwxcjeub32ne";
    dic[@"url"]=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,LOGIN_USER];
    [api sendPostRequst:dic];
}

-(void)shouldLoaderOn:(BOOL)shouldOn{
    isLoaded=!shouldOn;
    dispatch_async(dispatch_get_main_queue(), ^{
        [self->_tableView reloadData];
    });
    
}


#pragma mark - Gesture
-(void)swipeleft:(UISwipeGestureRecognizer*)gestureRecognizer
{
    if ([gestureRecognizer locationInView:self.view].x>self.view.bounds.size.width*0.80) {
        [Global showSideMenu];
    }
}


#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return (isLoaded)?dataArray.count:4;
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if (isLoaded)
        if (cellDic[dataArray[indexPath.row][@"id"]]!=nil) {
            return (ProductTableViewCell *) cellDic[dataArray[indexPath.row][@"id"]];
        }
    
    static NSString *simpleTableIdentifier = @"ProductTableViewCell";
    ProductTableViewCell *cell = (ProductTableViewCell *)[_tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ProductTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    cell.isLoaded=isLoaded;
    
    
    if (isLoaded) {
        //        if (cellDic[dataArray[indexPath.row][@"id"]]!=nil) {
        //            return (ProductTableViewCell *) cellDic[dataArray[indexPath.row][@"id"]];
        //        }
        
        cell.productBox.hidden=NO;
        cell.productName.hidden = NO;
        cell.infoBtn.hidden = NO;
        cell.frameBtn.hidden = NO;
        
        cell.indexPathOfCell=indexPath;
        cell.delegate=self;
        
        cell.productName.text=dataArray[indexPath.row][@"name"];
        
        
        
        
        
        
        // NSString *str = dataArray[indexPath.row][@"id"];
        
        dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(q, ^{
            
            NSData *datas;
            if (self->isLoaded) {
                datas =[NSData dataWithContentsOfURL:[NSURL URLWithString:self->dataArray[indexPath.row][@"main_image"]]] ;
                
                //[Global saveImagesInDocumentDirectory:datas ImageId:[NSString stringWithFormat:@"product_%@",str]];
            }
            
            
            UIImage *img = [[UIImage alloc] initWithData:datas];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if(img!=nil)
                    cell.productImage.image = [Global getMidiumSizeImage:img imageSize:[UIScreen mainScreen].bounds.size.width];
                
                
                });
            });
            
            cellDic[dataArray[indexPath.row][@"id"]] = cell;
            
            
        }
                       else{
            cell.productBox.hidden=YES;
            cell.productName.hidden=YES;
            cell.infoBtn.hidden=YES;
            cell.frameBtn.hidden=YES;
            cell.productImage.image = nil;
        }
                       cell.selectionStyle = UITableViewCellSelectionStyleNone;
                       return cell;
    }
    
    
    
    - (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
    {
        //can be use in future
    }
    
    - (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
        
        if (isLoaded) {
            float height=[dataArray[indexPath.row][@"image_height"] floatValue];
            float width=[dataArray[indexPath.row][@"image_width"] floatValue];
            float ratio=height/width;
            height=ratio*self.tableView.bounds.size.width;
            return height;
        }
        else{
            return self.tableView.bounds.size.width;
        }
        
        
    }
    
#pragma mark - ProductCell delegate
    - (void)didStartFraming:(NSIndexPath *)indexPath{
        
        [APP_DELEGATE.db delTableWithTableName:USER_PRODUCT];
        NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
        dic[@"id"]=[NSString stringWithFormat:@"%@",dataArray[indexPath.row][@"id"]];
        [APP_DELEGATE.db insertIntoTable:dic table_name:USER_PRODUCT];
        [self performSegueWithIdentifier:@"select" sender:dataArray[indexPath.row][@"id"]];
    }
    
    -(void)didSelectInfoOption:(NSIndexPath *)indexPath{
        
        [[NSUserDefaults standardUserDefaults] setValue:dataArray[indexPath.row][@"id"] forKey:PRODUCTID];
        [self performSegueWithIdentifier:@"detail" sender:nil];
        
    }
    
    
//#pragma mark -Action Methods
//    - (IBAction)cartBtn:(id)sender {
//
//
//        [self callCartView];
//
//
//    }
//
//    - (IBAction)menuBtn:(id)sender {
//        //  UIImage *img=[self pb_takeSnapshot];
//
//        [Global showSideMenu];
//
//
//    }
    
#pragma mark - Segue support
    
    - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
        
        if ([[segue identifier] isEqualToString:@"select"]) {
            AlbumViewController *select=[segue destinationViewController];
            select.productId=(NSString *)sender;
        }
        
    }
    
    
    
    
    
    
#pragma mark - POP API DELEGATES
    -(void)connectionError{
        
        if (dataArray.count!=0) {
            return;
        }
        isLoaded=YES;
        [_tableView reloadData];
        _tableView.backgroundColor=[UIColor clearColor];
        if (warningView ==nil) {
            warningView =[[WarningSubView alloc] init];
        }
        warningView.view.frame=self.tableView.frame;
        [self.view insertSubview:warningView.view belowSubview:_tableView];
        
        warningView.titleText.text=NO_INTERNET_MSG;
        warningView.subTitleTxt.text=@"Pull to try again";
        warningView.noCartIcon.image=[UIImage imageNamed:@"noInternet.png"];
        
        
        
    }
    -(void)requestFailedSessionExpired:(NSDictionary *)dataPack{
        
    }
    -(void)requestFailedServerError:(NSDictionary *)dataPack{
        
    }
    -(void)requestFailedServiceUnavailableDueToMaintenance:(NSDictionary *)dataPack{
        [self shouldLoaderOn:NO];
        if (dataArray.count!=0) {
            return;
        }
        isLoaded=YES;
        [_tableView reloadData];
        _tableView.backgroundColor=[UIColor clearColor];
        if (warningView ==nil) {
            warningView =[[WarningSubView alloc] init];
        }
        warningView.view.frame=self.tableView.frame;
        [self.view insertSubview:warningView.view belowSubview:_tableView];
        
        warningView.titleText.text=dataPack[@"message"];
        warningView.subTitleTxt.text=@"Pull to try again";
        warningView.noCartIcon.image=[UIImage imageNamed:@"maintenance.png"];
        // [Global showSimpleAlert:self andTitle:FRAMESHOP andMessage:dataPack[@"message"]];
    }
    -(void)requestFailedMethodNotFound:(NSDictionary *)dataPack{
        [self logInUser];
    }
    -(void)requestFailedNotFound:(NSDictionary *)dataPack{
        
    }
    -(void)requestFailedUnauthorised:(NSDictionary *)dataPack{
        [self logInUser];
    }
    -(void)requestFailedBadRequest:(NSDictionary *)dataPack{
        
    }
    -(void)requestSucceededWithData:(NSDictionary *)dataPack{
        [self shouldLoaderOn:NO];
        if ([dataPack[@"url"] isEqualToString:LOGIN_USER]) {
            
            [APP_DELEGATE.db delTableWithTableName:USERTABLE];
            NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
            
            dic[@"id"]              =[NSString stringWithFormat:@"%@",dataPack[@"data"][@"id"]];
            dic[@"name"]            =dataPack[@"data"][@"name"];
            dic[@"email"]           =dataPack[@"data"][@"email"];
            dic[@"created_at"]      =dataPack[@"data"][@"created_at"];
            dic[@"updated_at"]      =dataPack[@"data"][@"updated_at"];
            dic[@"token"]           =dataPack[@"data"][@"access_token"];
            
            [APP_DELEGATE.db insertIntoTable:dic table_name:USERTABLE];
            [self loadProductData];
            NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
            [nc postNotificationName:CART_REFRESH object:self userInfo:nil];
        }
        
        else if ([dataPack[@"url"] isEqualToString:REGISTER_USER]){
            
            NSMutableDictionary *dic =[[NSMutableDictionary alloc] init];
            dic[@"token"]            =dataPack[@"data"][@"token"];
            dic[@"id"]               =[NSString stringWithFormat:@"%@",dic[@"id"]];
            
            [APP_DELEGATE.db insertIntoTable:dic table_name:USERTABLE];
            [self loadProductData];
            NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
            [nc postNotificationName:CART_REFRESH object:self userInfo:nil];
        }
       
        
        else{
            [FBSDKAppEvents
             logEvent:@"Product Recivied"];
            
            // reload table with product data
            [dataArray removeAllObjects];
            [cellDic removeAllObjects];
            NSArray *arr=[dataPack[@"data"][@"products"] mutableCopy];
            [APP_DELEGATE.db delTableWithTableName:PRODUCT_TABLE];
            [Global deleteFilesFromCacheWhichContain:@"product_"];
            for (int i=0; i<arr.count; i++) {
                NSMutableDictionary *temDic=[[NSMutableDictionary alloc] init];
                
                temDic[@"id"]          =[NSString stringWithFormat:@"%@",arr[i][@"id"]];
                temDic[@"name"]        =arr[i][@"name"];
                temDic[@"sku"]         =arr[i][@"SKU"];
                temDic[@"type"]         =arr[i][@"type"];
                temDic[@"design"]      =arr[i][@"design"];
                temDic[@"base_price"]  =[NSString stringWithFormat:@"%@",arr[i][@"base_price"]];
                temDic[@"image_count"] =[NSString stringWithFormat:@"%@",arr[i][@"image_count"]];
                temDic[@"description"] =arr[i][@"description"];
                
                temDic[@"showcase_title"]     =arr[i][@"showcase_title"];
                temDic[@"showcase_subtitle"]  =arr[i][@"showcase_subtitle"];
                temDic[@"main_image"]         =arr[i][@"main_image"];
                temDic[@"carousel_images"]    =arr[i][@"carousel_images"];
                temDic[@"attached_properties"] =arr[i][@"attached_properties"];
                
                temDic[@"limitations_on_properties"] =arr[i][@"attached_properties"];
                temDic[@"parent_category_id"]        =[NSString stringWithFormat:@"%@",arr[i][@"parent_category_id"]];
                temDic[@"parent_subcategory_id"]     =[NSString stringWithFormat:@"%@",arr[i][@"parent_subcategory_id"]];
                
                temDic[@"is_deleted"]   =[NSString stringWithFormat:@"%@",arr[i][@"is_deleted"]];
                temDic[@"image_width"]  =[NSString stringWithFormat:@"%@",arr[i][@"image_width"]];
                temDic[@"image_height"] =[NSString stringWithFormat:@"%@",arr[i][@"image_height"]];
                
                dataArray[dataArray.count]=temDic;
                [APP_DELEGATE.db insertIntoTable:temDic table_name:PRODUCT_TABLE];
                //fs block mount demo
                
                
            }
            
            //fs for demo
            
            
            
            
            
            isLoaded=YES;
            dispatch_async(dispatch_get_main_queue(), ^{
                [self->_tableView reloadData];
            });
            // [self performSelector:@selector(startTute) withObject:self afterDelay:1.0 ];
            
            //[self getCartCount];
        }
        
        
    }
    -(void)requestSucceededButActionFailed:(NSDictionary *)dataPack{
        [self shouldLoaderOn:NO];
        if ([dataPack[@"url"] isEqualToString:REGISTER_USER] || [dataPack[@"message"] isEqualToString:@"Unauthenticated."]) {
            //go To login
            
            [self logInUser];
        }
        else if ([dataPack[@"url"] isEqualToString:LOGIN_USER]) {
            //something bad happned with server
            [self signUpUser];
            
        }
        
        else{
            NSLog(@"there is some problme with product api");
            //there is some problme with product api
        }
        
        
    }
    -(void)uploadedData:(NSString *)pecentage andByte:(float)bytes{
        
    }
    -(void)requestSucceededButNoDataFound:(NSDictionary *)dataPack{
        
    }
    -(void)requestFailedDueToServerIsuueWithError:(NSString*)errorDes{
        
        
        
    }
    
    
    
    @end
