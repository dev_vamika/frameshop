//
//  MenuSubView.h
//  FrameShop
//
//  Created by Saurabh anand on 04/12/19.
//  Copyright © 2019 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MenuTableViewCell.h"
NS_ASSUME_NONNULL_BEGIN
@class MenuSubView;
@protocol MenuSubViewDelegate <NSObject>
@optional
-(void)didSelectMenuOption:(NSDictionary *)infoDic;

@end
@interface MenuSubView : UIViewController
@property(strong,nonatomic)id<MenuSubViewDelegate> delegate;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *vesrionText;

@end

NS_ASSUME_NONNULL_END
