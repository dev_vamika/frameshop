//
//  LoaderTableViewCell.m
//  FrameShop
//
//  Created by Saurabh anand on 20/01/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import "LoaderTableViewCell.h"
#import "Global.h"
@implementation LoaderTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)viewWillAppear:(BOOL)animated{
    [self layoutIfNeeded];
}
-(void)layoutSubviews{
    [Global startShimmeringToView:self.contentView];
}
@end
