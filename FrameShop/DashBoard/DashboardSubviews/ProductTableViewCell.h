//
//  ProductTableViewCell.h
//  FrameShop
//
//  Created by Saurabh anand on 04/12/19.
//  Copyright © 2019 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class ProductTableViewCell;
@protocol ProductTableViewCellDelegate <NSObject>
@optional
-(void)didSelectInfoOption:(NSIndexPath *)indexPath;
-(void)didStartFraming:(NSIndexPath *)indexPath;
@end
@interface ProductTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *infoBtn;
@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UIView *loaderView;
@property(strong,nonatomic)id<ProductTableViewCellDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIImageView *productImage;
@property (weak, nonatomic) IBOutlet UILabel *productName;
@property (weak, nonatomic) IBOutlet UIImageView *productBox;
@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UILabel *frameBtn;
@property NSIndexPath *indexPathOfCell;
- (IBAction)infoBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *startFramingButton;
- (IBAction)startFramingClick:(id)sender;
@property bool isLoaded;

@end

NS_ASSUME_NONNULL_END
