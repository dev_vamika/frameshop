//
//  MenuTableViewCell.h
//  FrameShop
//
//  Created by Saurabh anand on 04/12/19.
//  Copyright © 2019 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MenuTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *optionIcon;
@property (weak, nonatomic) IBOutlet UILabel *option;

@end

NS_ASSUME_NONNULL_END
