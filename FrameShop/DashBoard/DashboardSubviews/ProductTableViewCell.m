//
//  ProductTableViewCell.m
//  FrameShop
//
//  Created by Saurabh anand on 04/12/19.
//  Copyright © 2019 Mayank Barnwal. All rights reserved.
//

#import "ProductTableViewCell.h"
#import "Global.h"
@implementation ProductTableViewCell{
    // BOOL isShadow;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [Global roundCorner:self.backView roundValue:20 borderWidth:0.0 borderColor:[UIColor lightGrayColor]];
}

-(void)viewWillAppear:(BOOL)animated{
    [self layoutIfNeeded];
    
    
}
-(void)layoutSubviews{
    
    if (!_isLoaded) {
      
        [Global startShimmeringToView:self.contentView];
        return;
    }
    [Global stopShimmeringToView:self.contentView];
    [_productName sizeToFit];
    
    
    float height=_productBox.frame.size.height;
    
   // if (height<_productName.frame.size.height) {
        height=_productName.frame.size.height+25;
   // }
    
    _productBox.frame=CGRectMake(_productImage.frame.origin.x, _productImage.frame.origin.y, _productName.frame.size.width+70, height);
    _productName.frame=CGRectMake(10, (_productBox.frame.size.height/2)-
                                  (_productName.frame.size.height/2), _productName.frame.size.width, _productName.frame.size.height);
    
    [Global roundCorner:self.backView roundValue:20 borderWidth:0.5 borderColor:[UIColor lightGrayColor]];
    [Global roundCorner:_frameBtn roundValue:_frameBtn.frame.size.height/2 borderWidth:2 borderColor:APP_BLUE_COLOR];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    
}

- (IBAction)infoBtnAction:(id)sender {
    [self.delegate didSelectInfoOption:_indexPathOfCell];
}

- (IBAction)startFramingClick:(id)sender {
    [self.delegate didStartFraming:_indexPathOfCell];
}
@end
