//
//  MenuSubView.m
//  FrameShop
//
//  Created by Saurabh anand on 04/12/19.
//  Copyright © 2019 Mayank Barnwal. All rights reserved.
//

#import "MenuSubView.h"
#import "Global.h"
@interface MenuSubView (){
    NSMutableArray *menuArray;
}

@end

@implementation MenuSubView

- (void)viewDidLoad {
    [super viewDidLoad];
    
    menuArray=[[NSMutableArray alloc] init];
    
    menuArray[menuArray.count]=[self getDicWithMenuOption:@"Cart" andImageName:@"blueCart.png"];
 //   menuArray[menuArray.count]=[self getDicWithMenuOption:@"FAQ" andImageName:@"faq.png"];
    menuArray[menuArray.count]=[self getDicWithMenuOption:@"Contact" andImageName:@"contact.png"];
    menuArray[menuArray.count]=[self getDicWithMenuOption:@"Order History" andImageName:@"orderHistory.png"];
    menuArray[menuArray.count]=[self getDicWithMenuOption:@"Gift Card" andImageName:@"giftCard.png"];
    menuArray[menuArray.count]=[self getDicWithMenuOption:@"FAQ" andImageName:@"faq.png"];
//    menuArray[menuArray.count]=[self getDicWithMenuOption:@"Privacy Policy" andImageName:@"privacy_policy.png"];
//    menuArray[menuArray.count]=[self getDicWithMenuOption:@"Terms & Conditions" andImageName:@"t&d_icon.png"];
    _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    
    [_tableView reloadData];
    
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    _vesrionText.frame=CGRectMake((self.view.frame.size.width-_vesrionText.frame.size.width)/2, self.view.bounds.size.height-_vesrionText.frame.size.height, _vesrionText.frame.size.width, _vesrionText.frame.size.height);
}

-(NSMutableDictionary *)getDicWithMenuOption:(NSString *)option andImageName:(NSString *)imageName{
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
    dic[@"option"]=option;
    dic[@"image"]=imageName;
    dic[@"code"]=[NSString stringWithFormat:@"%lu",(unsigned long)menuArray.count];
    return dic;
}

#pragma mark - Table View Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return menuArray.count;
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"MenuTableViewCell";
    MenuTableViewCell *cell = (MenuTableViewCell *)[_tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MenuTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.option.text=menuArray[indexPath.row][@"option"];
    cell.option.textColor =[UIColor blackColor];
    cell.optionIcon.image=[Global getMidiumSizeImage:[UIImage imageNamed:menuArray[indexPath.row][@"image"]] imageSize:cell.optionIcon.frame.size.width] ;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [self.delegate didSelectMenuOption:menuArray[indexPath.row]];
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *nibViews = [[NSBundle mainBundle] loadNibNamed:@"MenuTableViewCell" owner:self options:nil];
    MenuTableViewCell *sqr = [nibViews objectAtIndex:0];
    return sqr.frame.size.height;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    return _headerView.frame.size.height;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return _headerView;
}

@end
