//
//  CameraViewController.h
//  FrameShop
//
//  Created by Saurabh anand on 10/12/19.
//  Copyright © 2019 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlbumCollectionViewCell.h"
#import "AlbumLoginViewController.h"
#import <Photos/Photos.h>
#import "PhotoViewController.h"
#import "SasFacebookCentralScript.h"
#define CELL_ROUND_CORNER_VALUE 20.0f
NS_ASSUME_NONNULL_BEGIN

@interface CameraViewController : UIViewController
@property (nonatomic) NSInteger index;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic, copy) NSArray *assetCollectionSubtypes;

@property NSString *productId;
@end

NS_ASSUME_NONNULL_END
