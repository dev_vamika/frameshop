//
//  AlbumLoginViewController.m
//  FrameShop
//
//  Created by Saurabh anand on 11/12/19.
//  Copyright © 2019 Mayank Barnwal. All rights reserved.
//

#import "AlbumLoginViewController.h"
#import "Global.h"
@interface AlbumLoginViewController ()

@end

@implementation AlbumLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [Global roundCorner:_fbBackView roundValue:5 borderWidth:0.0 borderColor:nil];
    if ([_type isEqualToString:@"fb"]) {
        _igBackView.hidden = true;
        _fbBackView.hidden = false;
        _textimage.image=nil;//@"fbText.png"
//        [_loginBtn setImage:[UIImage imageNamed:@"fbLoginBtn.png"] forState:UIControlStateNormal];
        _subTitle.text=@"We need your permission to access Facebook Albums.";
    }
    else if ([_type isEqualToString:@"insta"]){
        _igBackView.hidden = false;
        _fbBackView.hidden = true;
             _textimage.image=[UIImage imageNamed:@"instaText.png"];
             [_loginBtn setImage:nil forState:UIControlStateNormal];
            _loginBtn.backgroundColor = UIColor.clearColor;
             _subTitle.text=@"Want to access your instagram photos?\n Log in and choose your favourite photos!";
    }
}




@end
