//
//  IGLoginViewController.h
//  InstagramGrapgApiDemo
//
//  Created by Vamika on 25/02/20.
//  Copyright © 2020 Vamika. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>
NS_ASSUME_NONNULL_BEGIN
@protocol IGLoginViewControllerDelegate <NSObject>
//-(void)onCancelIGLogin;
//-(void)getIgMediaJson:(id)json withPaging:(NSDictionary*)pagingDic;
//-(void)AccessTokenExpired;
//-(void)fetchingMoreData:(id)result withPaging:(NSDictionary*)pagingDic;
-(void)IgAuthenticatedSuccessfully;
@end
@interface IGLoginViewController : UIViewController
@property(strong,nonatomic)id<IGLoginViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet WKWebView *webView;
- (IBAction)onCancel:(id)sender;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loader;
@property (weak, nonatomic) IBOutlet UILabel *warningLbl;
-(void)loadMoreIGMediaWithNextUrl:(NSString*)nextUrl;
@end

NS_ASSUME_NONNULL_END
