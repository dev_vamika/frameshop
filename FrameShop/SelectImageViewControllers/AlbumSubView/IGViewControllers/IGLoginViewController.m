//
//  IGLoginViewController.m
//  InstagramGrapgApiDemo
//
//  Created by Vamika on 25/02/20.
//  Copyright © 2020 Vamika. All rights reserved.
//
//https://www.frameshop.com.au/
#import "IGLoginViewController.h"
#import <WebKit/WebKit.h>
#import "Global.h"
#define INSTAGRAM_BASE_URL @"https://api.instagram.com/oauth/authorize"
#define INSTAGRAM_APPID @"?client_id=509472543333532"
#define INSTAGRAM_REDIRECTURI @"&redirect_uri=https://www.frameshop.com.au/"
#define INSTAGRAM_SCOPE @"&scope=user_profile,user_media"
#define INSTAGRAM_RESPONSE @"&response_type="
#define INSTAGRAM_GET_MEDIA_URL @"https://graph.instagram.com/%@?fields=id,username&access_token=%@"


@interface IGLoginViewController ()<WKUIDelegate,WKNavigationDelegate>

@end

@implementation IGLoginViewController
 
- (void)viewDidLoad {
    [super viewDidLoad];
    [self removeCookies];
    self.warningLbl.hidden = YES;
    self.loader.hidden = YES;
    self.title =  @"INSTAGRAM";
    [self.loader stopAnimating];
    _webView.UIDelegate = self;
    _webView.navigationDelegate = self;
    //NSLog(@"%@",[NSString stringWithFormat:@"%@%@%@%@%@%@",INSTAGRAM_BASE_URL,INSTAGRAM_APPID,INSTAGRAM_REDIRECTURI,INSTAGRAM_SCOPE,INSTAGRAM_RESPONSE,@"code"]);
    
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [self iGAuthentication:[NSString stringWithFormat:@"%@%@%@%@%@%@",INSTAGRAM_BASE_URL,INSTAGRAM_APPID,INSTAGRAM_REDIRECTURI,INSTAGRAM_SCOPE,INSTAGRAM_RESPONSE,@"code"]];
}
-(void) removeCookies{
    NSHTTPCookie *cookie;
    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (cookie in [storage cookies])
    {
        NSString* domainName = [cookie domain];
        NSRange domainRange = [domainName rangeOfString:@"instagram"];
        if(domainRange.length > 0)
            [storage deleteCookie:cookie];
        domainRange = [domainName rangeOfString:@"facebook"];
        if(domainRange.length > 0)
            [storage deleteCookie:cookie];
    }
}
-(void)iGAuthentication:(NSString*)url{
    [self removeCookies];
    NSURL *targetURL = [NSURL URLWithString:url];
    NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
    [_webView loadRequest:request];
}

-(void)getIGAccessToken:(NSString*)code
{
    
    NSString *urlString=@"https://api.instagram.com/oauth/access_token";
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    
    NSMutableData *body = [NSMutableData data];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    //@"client_id"
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"client_id\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"509472543333532" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"client_secret\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"6d905a0625d3234205f93e3d03004843" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"grant_type\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"authorization_code" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"redirect_uri\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"https://www.frameshop.com.au/" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"code\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[code dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    // close form
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    // set request body
    [request setHTTPBody:body];
    NSError* error = nil;
    //return and test
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    id jsonObject = [NSJSONSerialization JSONObjectWithData:returnData options:NSJSONReadingAllowFragments error:&error];
    [[NSUserDefaults standardUserDefaults] setObject:jsonObject[INSTAGRAM_ACCESS_TOKEN]  forKey:INSTAGRAM_ACCESS_TOKEN];
    [[NSUserDefaults standardUserDefaults] setObject:jsonObject[INSTAGRAM_USER_ID] forKey:INSTAGRAM_USER_ID];
//    [self getIGMedia:jsonObject];
    [self onCancel:[UIButton new]];;
    
   
}
-(void)getIGMedia:(id)json{
   dispatch_async(dispatch_get_main_queue(), ^{
     self.webView.hidden = YES;
     self.warningLbl.hidden = NO;
     self.loader.hidden = NO;
     [self.loader startAnimating];
   });
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://graph.instagram.com/me/media?fields=id,caption,media_url,media_type&access_token=%@",json[INSTAGRAM_ACCESS_TOKEN]]]];
    [request setHTTPMethod:@"GET"];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (error != nil) {
               // [self.delegate AccessTokenExpired];
               // [self.navigationController popViewControllerAnimated:YES];
            }else{
                NSDictionary *dic = [[NSDictionary alloc] init];
                dic = jsonObject[@"paging"];
                NSLog(@"paging==%@",dic);
                self.warningLbl.hidden = YES;
                self.loader.hidden = YES;
                [self.loader stopAnimating];
              //  [self.delegate getIgMediaJson:jsonObject withPaging:dic];
                [self.navigationController popViewControllerAnimated:YES];
            }
        });
    }] resume];
}

-(void)loadMoreIGMediaWithNextUrl:(NSString*)nextUrl{
    if (nextUrl != nil) {
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:nextUrl]];
        [request setHTTPMethod:@"GET"];
        NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            //sas 10
          //  NSString *requestReply = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
         //   NSLog(@"IG MEDIA reply: %@", requestReply);
            id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
            NSDictionary *dic = [[NSDictionary alloc] init];
            dic = jsonObject[@"paging"];
            NSLog(@"paging==%@",dic);
            dispatch_async(dispatch_get_main_queue(), ^{
           //  [self.delegate fetchingMoreData:jsonObject withPaging:dic];
                [self dismissViewControllerAnimated:YES completion:nil];
            });
        }] resume];
    }
    
}
- (void)webView:(WKWebView *)webView didFinishNavigation:(null_unspecified WKNavigation *)navigation{
   //  NSLog(@"%@",webView.URL);
     if (tempUrl !=nil) {
         
         NSString *decodedUrl = [tempUrl stringByRemovingPercentEncoding];
       //  NSLog (@"%@", decodedUrl);
         tempUrl = nil;
         NSString* code = [[decodedUrl componentsSeparatedByString:@"code="].lastObject stringByReplacingOccurrencesOfString:@"#_" withString:@""];
        
         [self getIGAccessToken:code];
     }
     
 }
NSString *tempUrl;
 - (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler{
   
     if (![navigationAction.request.URL.absoluteString containsString:@"https://www.facebook.com/"]) {
         if ([navigationAction.request.URL.absoluteString containsString:@"#_"] && tempUrl == nil) {
             dispatch_async(dispatch_get_main_queue(), ^{
               self.webView.hidden = YES;
               self.warningLbl.hidden = NO;
               self.loader.hidden = NO;
               [self.loader startAnimating];
             });
             tempUrl =navigationAction.request.URL.absoluteString;
         }
     }else{
         [self iGAuthentication:[NSString stringWithFormat:@"%@%@%@%@%@%@",INSTAGRAM_BASE_URL,INSTAGRAM_APPID,INSTAGRAM_REDIRECTURI,INSTAGRAM_SCOPE,INSTAGRAM_RESPONSE,@"code"]];
     }
     decisionHandler(WKNavigationActionPolicyAllow);
     
 }

 - (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error{
     
 }
- (IBAction)onCancel:(id)sender {
    [self.delegate IgAuthenticatedSuccessfully];
    [self.navigationController popViewControllerAnimated:YES];
}
@end
