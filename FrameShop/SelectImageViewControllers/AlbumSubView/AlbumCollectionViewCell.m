//
//  AlbumCollectionViewCell.m
//  FrameShop
//
//  Created by Saurabh anand on 11/12/19.
//  Copyright © 2019 Mayank Barnwal. All rights reserved.
//

#import "AlbumCollectionViewCell.h"
#import "Global.h"
#import "YYWebImage.h"
@implementation AlbumCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    //_iconImage.image=[UIImage imageNamed:@"p2.png"];
    
    
    
    _photoCount.frame=CGRectMake(0, self.bounds.size.height-_photoCount.frame.size.height, self.bounds.size.width, _photoCount.frame.size.height);
    
    // _albumName.frame=CGRectMake(0, _photoCount.frame.origin.y-_albumName.frame.size.height, self.bounds.size.width, _albumName.frame.size.height);
    
}

-(void)viewWillAppear:(BOOL)animated{
    [self layoutIfNeeded];
}
-(void)layoutSubviews{
    _photoCount.frame=CGRectMake(13, self.bounds.size.height-_photoCount.frame.size.height, self.bounds.size.width, _photoCount.frame.size.height);
    _albumName.frame=CGRectMake(13, _photoCount.frame.origin.y-_albumName.frame.size.height, self.bounds.size.width, _albumName.frame.size.height);
    
    //    _albumName.hidden=YES;
    //    _photoCount.hidden=YES;
    if (_iconImage.image==nil) {
        [Global startShimmeringToView:_iconImage];
    }
    
    
}
-(void)refreshCell{
    
    
    _iconImage.clipsToBounds = YES;
    self.iconImage.contentMode= UIViewContentModeScaleAspectFill;
    if ([_infoDic[@"type"] isEqualToString:@"fb"]) {
        self.iconImage.image=nil;
        
        [self.iconImage yy_setImageWithURL:[NSURL URLWithString:self.infoDic[@"fullImage"]]
                               placeholder:nil
                                   options:YYWebImageOptionProgressiveBlur|YYWebImageOptionSetImageWithFadeAnimation
                                  progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        }
                                 transform:^UIImage *(UIImage *image, NSURL *url) {
            return image;
        }
                                completion:^(UIImage *image, NSURL *url, YYWebImageFromType from, YYWebImageStage stage, NSError *error) {
            if(stage == YYWebImageStageFinished){
                [Global stopShimmeringToView:self->_iconImage];
                self.iconImage.clipsToBounds = YES;
                self.iconImage.contentMode= UIViewContentModeScaleAspectFill;
            }
        }];
        [Global stopShimmeringToView:_iconImage];
    }else if ([_infoDic[@"type"] isEqualToString:@"insta"]){
        self.iconImage.image=nil;
        [self.iconImage yy_setImageWithURL:[NSURL URLWithString:self.infoDic[@"url"]]
                               placeholder:nil
                                   options:YYWebImageOptionProgressiveBlur|YYWebImageOptionSetImageWithFadeAnimation
                                  progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        }
                                 transform:^UIImage *(UIImage *image, NSURL *url) {
            return image;
        }
                                completion:^(UIImage *image, NSURL *url, YYWebImageFromType from, YYWebImageStage stage, NSError *error) {
            if(stage == YYWebImageStageFinished){
                [Global stopShimmeringToView:self->_iconImage];
                self.iconImage.clipsToBounds = YES;
                self.iconImage.contentMode= UIViewContentModeScaleAspectFill;
            }
        }];
    }
}
-(void)refreshView{
    
    _iconImage.clipsToBounds = YES;
    
    if ([_infoDic[@"type"] isEqualToString:@"cam"]) {
        NSPredicate *predicate=[NSPredicate predicateWithFormat:@"mediaType !=%i",PHAssetMediaTypeVideo];
        PHAssetCollection *groupForCell = _infoDic[@"assets"];
        PHFetchOptions *options = [PHFetchOptions new];
        options.predicate = predicate;
        PHFetchResult *fetchResult = [PHAsset fetchAssetsInAssetCollection:groupForCell options:options];
        //        if(fetchResult.count != 0){
        PHImageRequestOptions *imageoptions = [[PHImageRequestOptions alloc] init];
        imageoptions.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat; //I only want the highest possible quality
        imageoptions.synchronous = NO;
        imageoptions.networkAccessAllowed = YES;
        imageoptions.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
        imageoptions.progressHandler = ^(double progress, NSError *error, BOOL *stop, NSDictionary *info) {
        };
        PHImageManager *imageManager = [PHImageManager defaultManager];
        
        [imageManager requestImageForAsset:fetchResult.lastObject
                                targetSize:CGSizeMake([UIScreen mainScreen].bounds.size.width/2, [UIScreen mainScreen].bounds.size.width/2)
                               contentMode:PHImageContentModeAspectFill
                                   options:imageoptions
                             resultHandler:^(UIImage *result, NSDictionary *info) {
            self.iconImage.image = result;
            [Global stopShimmeringToView:self->_iconImage];
            //                self->_albumName.hidden=false;
            //                self->_photoCount.hidden=false;
        }];
        
        self.albumName.text = groupForCell.localizedTitle;
        
        self.photoCount.text = [NSString stringWithFormat:@"%lu", (long)fetchResult.count];
    }else if ([_infoDic[@"type"] isEqualToString:@"fb"]){
        //        self.albumName.hidden=NO;
        //        self.photoCount.hidden=NO;
        self.albumName.text=_infoDic[@"name"];
        self.photoCount.text=  [NSString stringWithFormat:@"%d",[_infoDic[@"count"] intValue]];
        [self getFbAlbumPic];
        
        
    }
    
}
-(void)getFbAlbumPic{
    
    
    
    
    UIImage *temp=[Global getImageFromDoucumentWithName:[NSString stringWithFormat:@"fb_%@",_infoDic[@"fbAlbumId"]]];
    if (temp!=nil) {
        _iconImage.image=temp;
    }
    SasFacebookCentralScript *fb=[[SasFacebookCentralScript alloc] init];
    fb.delegate                 =self;
    
    
    
    dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(q, ^{
        
        [fb getAlbumCoverPicWhereAlbumId:self.infoDic[@"fbAlbumId"]];
    });
    
    
    
    
    
}

#pragma mark -FB delegate
-(void)fbRequestSuccessWithData:(NSMutableArray *)dataArray{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.iconImage.image=[Global getMidiumSizeImage:[UIImage imageWithData:dataArray[0][@"imageData"]] imageSize:[UIScreen mainScreen].bounds.size.width/2]  ;
        [Global saveImagesInDocumentDirectory:UIImagePNGRepresentation(self.iconImage.image) ImageId:[NSString stringWithFormat:@"fb_%@",self.infoDic[@"fbAlbumId"]]];
    });
    [Global stopShimmeringToView:_iconImage];
}
-(void)fbRequestFailsWithError:(NSString *)error {
    dispatch_async(dispatch_get_main_queue(), ^{
    });
}





@end
