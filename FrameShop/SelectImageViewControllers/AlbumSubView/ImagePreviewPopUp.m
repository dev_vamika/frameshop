//
//  ImagePreviewPopUp.m
//  FrameShop
//
//  Created by Saurabh anand on 07/01/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import "ImagePreviewPopUp.h"
#import "YYWebImage.h"
@interface ImagePreviewPopUp (){
    BOOL isShown;
    PHAsset *asset;
    PHImageManager *imageManager ;
    PHImageRequestOptions *options;
}

@end

@implementation ImagePreviewPopUp

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
   
   // [self setUpPopUpView];
    UITapGestureRecognizer *singleFingerTap =
         [[UITapGestureRecognizer alloc] initWithTarget:self
                                                 action:@selector(handleSingleTap:)];
       [self.view addGestureRecognizer:singleFingerTap];
    
    
    
}
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    [self dropUpPopUp];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (!isShown) {
        [self setUpPopUpView];
    }
    
    
}

-(void)setUpPopUpView{
   

    
    
    float previewImageWidth,previewImageHeight, ratio;
    ratio=_imageFromSelection.size.width/_imageFromSelection.size.height;
    
    if (_imageFromSelection.size.width>=_imageFromSelection.size.height) {
       
        previewImageWidth=self.view.bounds.size.width*0.85;
        previewImageHeight=previewImageWidth/ratio;
    }
    else {
        previewImageHeight=self.view.bounds.size.height*0.65;
        previewImageWidth=ratio*previewImageHeight;
       
       
    }
  
    
     
    float imageY=0;
   
    _imageView.image=_imageFromSelection;
    
    _backView.frame=CGRectMake(0, 0, self.view.bounds.size.width, previewImageHeight+_btnView.frame.size.height+20);
    
    
    
    float imageX=(self.backView.bounds.size.width/2)-(previewImageWidth/2);
     _imageView.frame=CGRectMake(imageX, imageY, previewImageWidth, previewImageHeight);
    
    _btnView.frame=CGRectMake(0, previewImageHeight+10,self.view.bounds.size.width , _btnView.frame.size.height);
    
    

    
    _dismisBtn.frame=CGRectMake(5, 0, (_btnView.bounds.size.width/2)-10, _btnView.frame.size.height);
    _selectBtn.frame=CGRectMake((_btnView.bounds.size.width/2)+5, 0, (_btnView.bounds.size.width/2)-10, _btnView.frame.size.height);
    
    _activityLoader.frame=CGRectMake(_selectBtn.frame.origin.x+_selectBtn.frame.size.width-_activityLoader.frame.size.width*2,(_btnView.frame.size.height-_activityLoader.frame.size.height)/2 , _activityLoader.frame.size.width, _activityLoader.frame.size.height);
    _activityLoader.hidden=YES;
    
    [Global roundCorner:_dismisBtn roundValue:_dismisBtn.frame.size.height/2 borderWidth:1.0 borderColor:APP_BLUE_COLOR];
    [Global roundCorner:_selectBtn roundValue:_selectBtn.frame.size.height/2 borderWidth:1.0 borderColor:APP_BLUE_COLOR];
    
   
    [self dropDownPopUp];
  
    if ([_infoDic[@"type"] isEqualToString:@"cam"]) {
        [self getImageFromCam];
    }
   
    isShown=YES;
    
}
-(void)getImageFromCam{
   
  
    [self shouldLoader:YES];
    
        asset=(PHAsset *)_infoDic[@"assets"];
        NSArray *arr;
        arr=[[NSArray alloc] initWithObjects:[NSURL URLWithString:_infoDic[@"fullImage"]], nil];
    
       // asset = [[PHAsset fetchAssetsWithALAssetURLs:arr options:nil] lastObject];
        imageManager = [PHImageManager defaultManager];
       options = [[PHImageRequestOptions alloc] init];
                               options.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat; //I only want the highest possible quality
                               options.synchronous = NO;
                               options.networkAccessAllowed = YES;
                               options.progressHandler = ^(double progress, NSError *error, BOOL *stop, NSDictionary *info) {
                               };
        [imageManager requestImageForAsset:asset
                                targetSize:CGSizeMake(asset.pixelWidth, asset.pixelHeight)
                               contentMode:PHImageContentModeAspectFill
                                   options:options
                             resultHandler:^(UIImage *result, NSDictionary *info) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                
                
              
                        if(result!=nil)
                            self.imageView.image=[Global getMidiumSizeImage:result imageSize:self.imageView.frame.size.width];
                        
                        [Global saveImagesInDocumentDirectory:UIImagePNGRepresentation(result) ImageId:@"frame"];
                self->imageManager = nil;
                self->asset = nil;
                        [self shouldLoader:NO];
              
            });
        }];
    
}


-(void)shouldLoader:(BOOL)shouldLoad{
    if (shouldLoad) {
        _activityLoader.hidden=false;
        [_activityLoader startAnimating];
        _selectBtn.selected=false;
    }
    else{
        _activityLoader.hidden=true;
               [_activityLoader stopAnimating];
               _selectBtn.selected=true;
    }
}


-(void)dropDownPopUp{
    CGRect previousFrame=_backView.frame;
       _backView.frame=CGRectMake(0, -_backView.frame.size.height, _backView.frame.size.width, _backView.frame.size.height);
       
       [UIView animateWithDuration:DROP_ANIMATION_TIME
                              animations:^{
                 self.backView.frame=previousFrame;
                
                              } completion:^(BOOL finished) {
                                 
                                
                                  
    }];
}
-(void)dropUpPopUp{
   
       
       [UIView animateWithDuration:DROP_ANIMATION_TIME
                              animations:^{
           self.backView.frame=CGRectMake(0, -self->_backView.frame.size.height, self->_backView.frame.size.width, self->_backView.frame.size.height);
                
                              } completion:^(BOOL finished) {
                                  [self.view removeFromSuperview];
                                  [self removeFromParentViewController];
                                  self->_activityLoader.hidden = true;
                                  [self->_activityLoader stopAnimating];
                                  
                              }];
}


- (IBAction)dismissAction:(id)sender {
    [Global deleteFilesFromCacheWhichContain:@"frame"];
    [self dropUpPopUp];
    
}

- (IBAction)selectAction:(id)sender {
    _activityLoader.hidden = false;
    [_activityLoader startAnimating];
    if ([_infoDic[@"type"] isEqualToString:@"cam"]) {
    if (!_selectBtn.isSelected) {
        return;
    }else{
        [self shouldLoader:YES];
        
        dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(q, ^{
            
            //NSData *datas =[self dataByRemovingExif :UIImagePNGRepresentation(result)];
            UIImage * img = [Global getImageFromDoucumentWithName:@"frame"];
           
            dispatch_async(dispatch_get_main_queue(), ^{
                if(img!=nil)
                    [self.delegate didSelectImage:self->_indexPath andImage:img];
                    [self dropUpPopUp];
                
                [self shouldLoader:NO];
                
                
            });
        });
        
        
        
        
    }
    }else if ([_infoDic[@"type"] isEqualToString:@"fb"]) {
        
        
        [self shouldLoader:YES];
        
        dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(q, ^{
            
            NSData *datas =[NSData dataWithContentsOfURL:[NSURL URLWithString:self->_infoDic[@"fullImage"]]] ;
            
            UIImage *img = [[UIImage alloc] initWithData:datas];
            dispatch_async(dispatch_get_main_queue(), ^{
                if(img!=nil){
                    
                    [Global saveImagesInDocumentDirectory:datas ImageId:@"frame"];
                [self.delegate didSelectImage:self->_indexPath andImage:img];
                [self dropUpPopUp];
                   
                  
                }
                [self shouldLoader:NO];
                
            });
        });
        
        
       
        
        
    }
    else if ([_infoDic[@"type"] isEqualToString:@"insta"]) {
        _imageView.image = [UIImage imageWithData:[self dataByRemovingExif:(UIImagePNGRepresentation(_imageView.image))]];
        [Global saveImagesInDocumentDirectory:UIImagePNGRepresentation(_imageView.image) ImageId:@"frame"];
        [self.delegate didSelectImage:self->_indexPath andImage:_imageView.image];
        [self dropUpPopUp];
    }
    
   // [Global saveImagesInDocumentDirectory:UIImageJPEGRepresentation(self.imageView.image, 1.0) ImageId:@"frame"];
    
    
    
}

- (NSData *)dataByRemovingExif:(NSData *)data
{
    CGImageSourceRef source = CGImageSourceCreateWithData((CFDataRef)data, NULL);
    NSMutableData *mutableData = nil;

    if (source) {
        CFStringRef type = CGImageSourceGetType(source);
        size_t count = CGImageSourceGetCount(source);
        mutableData = [NSMutableData data];

        CGImageDestinationRef destination = CGImageDestinationCreateWithData((CFMutableDataRef)mutableData, type, count, NULL);

        NSDictionary *removeExifProperties = @{(id)kCGImagePropertyExifDictionary: (id)kCFNull,
                                               (id)kCGImagePropertyGPSDictionary : (id)kCFNull};

        if (destination) {
            for (size_t index = 0; index < count; index++) {
                CGImageDestinationAddImageFromSource(destination, source, index, (__bridge CFDictionaryRef)removeExifProperties);
            }

            if (!CGImageDestinationFinalize(destination)) {
                NSLog(@"CGImageDestinationFinalize failed");
            }

            CFRelease(destination);
        }

        CFRelease(source);
    }

    return mutableData;
}

@end
