//
//  AlbumCollectionViewCell.h
//  FrameShop
//
//  Created by Saurabh anand on 11/12/19.
//  Copyright © 2019 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Photos/Photos.h>
#import "SasFacebookCentralScript.h"

NS_ASSUME_NONNULL_BEGIN

@interface AlbumCollectionViewCell : UICollectionViewCell<SasFacebookCentralScriptDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *iconImage;
@property (weak, nonatomic) IBOutlet UILabel *albumName;
@property (weak, nonatomic) IBOutlet UILabel *photoCount;
@property int index;
@property NSMutableDictionary *infoDic;
-(void)refreshView;
-(void)refreshCell;
@property PHAsset *asset;

@end

NS_ASSUME_NONNULL_END
