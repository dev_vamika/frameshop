//
//  ImagePreviewPopUp.h
//  FrameShop
//
//  Created by Saurabh anand on 07/01/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import "ViewController.h"
#import <Photos/Photos.h>
NS_ASSUME_NONNULL_BEGIN
#define DROP_ANIMATION_TIME 0.2f
@class ImagePreviewPopUp;
@protocol ImagePreviewDelegate <NSObject>
@optional
-(void)didSelectImage:(NSIndexPath *)indexPath andImage:(UIImage *)selectImage;
@end

@interface ImagePreviewPopUp : UIViewController
@property(strong,nonatomic)id<ImagePreviewDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIView *btnView;
@property (weak, nonatomic) IBOutlet UIButton *dismisBtn;
@property (weak, nonatomic) IBOutlet UIButton *selectBtn;
@property UIImage *imageFromSelection;
@property NSMutableDictionary *infoDic;

@property PHAsset *asset;
@property NSIndexPath *indexPath;
- (IBAction)dismissAction:(id)sender;
- (IBAction)selectAction:(id)sender;
-(void)shouldLoader:(BOOL)shouldLoad;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityLoader;
@property (weak, nonatomic) IBOutlet UIButton *goBtn;

@end

NS_ASSUME_NONNULL_END
