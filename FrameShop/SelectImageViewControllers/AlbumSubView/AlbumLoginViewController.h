//
//  AlbumLoginViewController.h
//  FrameShop
//
//  Created by Saurabh anand on 11/12/19.
//  Copyright © 2019 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AlbumLoginViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *textimage;
@property (weak, nonatomic) IBOutlet UIButton *loginBtn;
@property (weak, nonatomic) IBOutlet UILabel *subTitle;
@property NSString *type;
@property (weak, nonatomic) IBOutlet UIView *igBackView;
@property (weak, nonatomic) IBOutlet UIView *fbBackView;


@end

NS_ASSUME_NONNULL_END
