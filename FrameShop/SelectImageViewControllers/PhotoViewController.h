//
//  PhotoViewController.h
//  FrameShop
//
//  Created by Saurabh anand on 12/12/19.
//  Copyright © 2019 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlbumCollectionViewCell.h"

#import <AudioToolbox/AudioServices.h>
#import "FilterViewController.h"
#import <Photos/Photos.h>
#import "ImageCropperViewController.h"
#import "ImagePreviewPopUp.h"
NS_ASSUME_NONNULL_BEGIN

@interface PhotoViewController : UIViewController
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong) NSMutableArray *assets;
@property (nonatomic, strong) PHAssetCollection *assetsGroup;
@property (nonatomic, strong) PHFetchResult *fetchResult;
@property (nonatomic, strong) NSMutableDictionary *igJson;
@property (nonatomic, strong) NSString *igNextPageUrl;
@property NSMutableDictionary *infoDic;
@property (weak, nonatomic) IBOutlet UILabel *cartCount;
- (IBAction)cartBtn:(id)sender;
- (IBAction)menuBtn:(id)sender;
@property NSString *productId;
@end

NS_ASSUME_NONNULL_END
