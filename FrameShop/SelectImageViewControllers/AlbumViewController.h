//
//  AlbumViewController.h
//  FrameShop
//
//  Created by Saurabh anand on 10/12/19.
//  Copyright © 2019 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Global.h"
#import "CameraViewController.h"
#import "MenuSubView.h"
#define MENU_ANIMATION_TIME 0.2f
NS_ASSUME_NONNULL_BEGIN

@interface AlbumViewController : UIViewController<UIPageViewControllerDataSource,UIPageViewControllerDelegate,UIScrollViewDelegate,MenuSubViewDelegate>
@property (weak, nonatomic) IBOutlet UIView *tabBar;
@property (weak, nonatomic) IBOutlet UIView *deviceView;

@property (weak, nonatomic) IBOutlet UIButton *deviceBtn;
- (IBAction)deviceBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *fbView;
@property (weak, nonatomic) IBOutlet UIButton *fbBTn;

- (IBAction)fbBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *instaView;
@property (weak, nonatomic) IBOutlet UIButton *instaBtn;


- (IBAction)instaBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *indiCatorBar;
@property (weak, nonatomic) IBOutlet UILabel *cartCount;
- (IBAction)cartBtn:(id)sender;
- (IBAction)menuBtn:(id)sender;

@property NSString *productId;


@end

NS_ASSUME_NONNULL_END
