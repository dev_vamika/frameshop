//
//  PhotoViewController.m
//  FrameShop
//
//  Created by Saurabh anand on 12/12/19.
//  Copyright © 2019 Mayank Barnwal. All rights reserved.
//

#import "PhotoViewController.h"
#import "AppDelegate.h"
@interface PhotoViewController ()<SasFacebookCentralScriptDelegate,ImagePreviewDelegate>{
    UIImage *sendImage;
    NSIndexPath *selectedIndexPath;
    CGRect sendFrame;
    NSMutableArray *dataArr,*imageArray,*cellArr;
    NSMutableDictionary *imageDic;
    BOOL isMediaLoading;
    NSString *sku;
}

@end

@implementation PhotoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UINib * nib = [UINib nibWithNibName:@"AlbumCollectionViewCell" bundle:nil];
    [_collectionView registerNib:nib forCellWithReuseIdentifier:@"AlbumCollectionViewCell"];
    
    imageDic=[[NSMutableDictionary alloc] init];
    
    if (!self.assets) {
        _assets = [[NSMutableArray alloc] init];
        
    } else {
        [self.assets removeAllObjects];
    }
    
    if ([_infoDic[@"type"] isEqualToString:@"cam"]) {
        
        [self showCamPics];
        
        //self.title = self.assetsGroup.localizedTitle;
        self.title=@"Select Image";
        
    } else if ([_infoDic[@"type"] isEqualToString:@"fb"]) {
        self.title = _infoDic[@"name"];
        [self showFbPics];
    }
    else if ([_infoDic[@"type"] isEqualToString:@"insta"]) {
        self.title = @"INSTAGRAM";
        [self requestdidLoad:self.igJson];
    }
    [Global roundCorner:_cartCount roundValue:_cartCount.frame.size.height/2 borderWidth:2 borderColor:[UIColor whiteColor]];
    
    UISwipeGestureRecognizer * swipeleft=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeleft:)];
    swipeleft.direction=UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeleft];
    
    [self.navigationItem setBackBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil]];
   // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishCart) name:CART_REFRESH object:nil];
    // NSLog(@"%@",_productId);
    
    int count=[[[NSUserDefaults standardUserDefaults] valueForKey:CART_COUNT] intValue];
    if (count>9) {
        _cartCount.text=@"9+";
    }
    else{
        _cartCount.text=[NSString stringWithFormat:@"%d",[[[NSUserDefaults standardUserDefaults] valueForKey:CART_COUNT] intValue]];
    }
    _cartCount.hidden=[_cartCount.text isEqualToString:@"0"];
    
   
           [APP_DELEGATE.db delTableWithTableName:USER_PRODUCT];
           NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
           dic[@"id"]=[NSString stringWithFormat:@"%@",self.productId];
           [APP_DELEGATE.db insertIntoTable:dic table_name:USER_PRODUCT];
    
    NSArray * arrD = [APP_DELEGATE.db getDataForField:PRODUCT_TABLE where:[NSString stringWithFormat:@"id='%@'",_productId] ];
    
    if (arrD.count != 0) {
        sku = arrD[0][@"sku"];
    }
}
-(void)didFinishCart{
    int count=[[[NSUserDefaults standardUserDefaults] valueForKey:CART_COUNT] intValue];
    if (count>9) {
        _cartCount.text=@"9+";
    }
    else{
        _cartCount.text=[NSString stringWithFormat:@"%d",[[[NSUserDefaults standardUserDefaults] valueForKey:CART_COUNT] intValue]];
    }
    _cartCount.hidden=[_cartCount.text isEqualToString:@"0"];
   //[self getCartCount];
}


-(void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    [imageDic removeAllObjects];
}


-(void)showFbPics{
    
    SasFacebookCentralScript *fb=[[SasFacebookCentralScript alloc] init];
    fb.delegate=self;
    [fb getFbPhotosOfAlbumWithAlbumId:_infoDic[@"fbAlbmId"]];
    
    
}
-(void)swipeleft:(UISwipeGestureRecognizer*)gestureRecognizer
{
    
    if ([gestureRecognizer locationInView:self.view].x>self.view.bounds.size.width*0.80) {
        [Global showSideMenu];
    }
}
-(void)showCamPics{
    
    NSPredicate *predicate=[NSPredicate predicateWithFormat:@"mediaType !=%i",PHAssetMediaTypeVideo];
    
    
    PHFetchOptions *options = [PHFetchOptions new];
    options.predicate=predicate;
    self.fetchResult = [PHAsset fetchAssetsInAssetCollection:self.assetsGroup options:options];
    
    self.assets = (NSMutableArray *)self.fetchResult;
    [self.collectionView reloadData];
    
}

- (IBAction)cartBtn:(id)sender{
    [Global callMenuViewWithCode:@"0"];
}
- (IBAction)menuBtn:(id)sender{
    [Global showSideMenu];
}
#pragma mark - UICollectionViewDelegate

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    
    
    return (_assets.count==1)?2:_assets.count;
}

#define kImageViewTag 1 // the image view inside the collection view cell prototype is tagged with "1"

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
    static NSString *identifier = @"AlbumCollectionViewCell";
    
    AlbumCollectionViewCell *cells =[_collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    if (indexPath.row>=_assets.count) {
        cells.contentView.hidden=true;
        return cells;
    }
    
    cells.albumName.hidden=YES;
    cells.photoCount.hidden=YES;
    float cellWidth=((_collectionView.bounds.size.width-1)/3);
    float cellHeight=cellWidth;
    
    cells.iconImage.frame=CGRectMake(cells.iconImage.frame.origin.x, cells.iconImage.frame.origin.y, cellWidth, cellHeight);
    
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
    dic[@"type"]=_infoDic[@"type"];
    if([_infoDic[@"type"] isEqualToString:@"fb"]){
        dic[@"url"] =self.assets[indexPath.row][@"thumb"];
        dic[@"fullImage"] =self.assets[indexPath.row][@"image"];
        dic[@"id"]  =self.assets[indexPath.row][@"id"];
        dic[@"date"]=self.assets[indexPath.row][@"date"];
        dic[@"lat"]= self.assets[indexPath.row][@"lat"];
        dic[@"longi"]=self.assets[indexPath.row][@"longi"];
        dic[@"place"]=self.assets[indexPath.row][@"place"];
        //  NSLog(@"%@",self.assets[indexPath.row]);
        cells.infoDic=dic;
        [cells refreshCell];
    }else if([_infoDic[@"type"] isEqualToString:@"insta"]){
        dic[@"url"] =self.assets[indexPath.row][@"image"];
        dic[@"fullImage"] =self.assets[indexPath.row][@"image"];
        dic[@"id"]  =self.assets[indexPath.row][@"id"];
        dic[@"date"]=self.assets[indexPath.row][@"date"];
        dic[@"lat"]=self.assets[indexPath.row][@"lat"];
        dic[@"longi"]=self.assets[indexPath.row][@"longi"];
        dic[@"place"]=self.assets[indexPath.row][@"place"];
        //  NSLog(@"%@",self.assets[indexPath.row]);
        cells.infoDic=dic;
        [cells refreshCell];
    }
    else if([_infoDic[@"type"] isEqualToString:@"cam"])
    {
        NSInteger reverseIndex = ((self.fetchResult.count)-1)-indexPath.row;
        //  NSLog(@"%ld",reverseIndex);
        cells.iconImage.image=nil;
        
        PHAsset *asset = self.fetchResult[reverseIndex];
        if (imageDic[[NSString stringWithFormat:@"%ld",(long)indexPath.row]]!=nil) {
            cells.iconImage.image=(UIImage *)imageDic[[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
            [Global stopShimmeringToView:cells.iconImage];
        }
        
        else{
            PHImageRequestOptions *options = [[PHImageRequestOptions alloc] init];
            options.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat; //I only want the highest possible quality
            options.synchronous = NO;
            options.networkAccessAllowed = YES;
            options.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
            options.progressHandler = ^(double progress, NSError *error, BOOL *stop, NSDictionary *info) {
            };
            PHImageManager *imageManager = [PHImageManager defaultManager];
            [imageManager requestImageForAsset:asset
                                    targetSize:CGSizeMake([UIScreen mainScreen].bounds.size.width/3 + 20, [UIScreen mainScreen].bounds.size.width/3 + 20)
                                   contentMode:PHImageContentModeAspectFill
                                       options:options
                                 resultHandler:^(UIImage *result, NSDictionary *info) {
                cells.iconImage.image = result;
                self->imageDic[[NSString stringWithFormat:@"%ld",(long)indexPath.row]]=result;
                [Global stopShimmeringToView:cells.iconImage];
            }];
            
        }
        // NSLog(@"this is local assets =%@",asset.localIdentifier);
        NSString *localStr=asset.localIdentifier;
        NSRange range = [localStr rangeOfString:@"/"];
        NSString *newString = [localStr substringToIndex:range.location];
        NSString *appendedString=[NSString stringWithFormat:@"%@%@%@",@"assets-library://asset/asset.png?id=",newString,@"&ext=PNG"];
        NSURL *name=[NSURL URLWithString:appendedString];
        dic[@"fullImage"]=  [NSString stringWithFormat:@"%@",name] ;
        dic[@"type"]=_infoDic[@"type"];
        dic[@"assets"]=asset;
        cells.infoDic=dic;
        
    }
    
    
    return cells;
    
    
}

- (BOOL)collectionView:(UICollectionView *)collectionView
shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (BOOL)collectionView:(UICollectionView *)collectionView
shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    // AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
    AlbumCollectionViewCell *cell = (AlbumCollectionViewCell *)[_collectionView cellForItemAtIndexPath:indexPath];
    
    
    sendImage= cell.iconImage.image;
    if (sendImage == nil) {
        return;
    }
    selectedIndexPath=indexPath;
    
    
    ImagePreviewPopUp *desView = [[ImagePreviewPopUp alloc] init];
    
    desView.delegate=self;
    desView.infoDic=cell.infoDic;
    desView.imageFromSelection=sendImage;
    desView.indexPath=selectedIndexPath;
    
    desView.view.frame=CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height);
    [self.view addSubview:desView.view];
    [self addChildViewController:desView];
    
    
}

-(void)shakeView:(UIView *)lockView{
    CABasicAnimation *animation =
    [CABasicAnimation animationWithKeyPath:@"position"];
    [animation setDuration:0.05];
    [animation setRepeatCount:1];
    [animation setAutoreverses:YES];
    [animation setFromValue:[NSValue valueWithCGPoint:
                             CGPointMake([lockView center].x - 20.0f, [lockView center].y- 20.0f)]];
    [animation setToValue:[NSValue valueWithCGPoint:
                           CGPointMake([lockView center].x + 20.0f, [lockView center].y+ 20.0f)]];
    [[lockView layer] addAnimation:animation forKey:@"position"];
}

#pragma mark collection view cell layout / size
- (CGSize)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    
    
    float cellWidth=((_collectionView.bounds.size.width-1)/3);
    float cellHeight=cellWidth;
    
    CGSize size=CGSizeMake(cellWidth, cellHeight);
    //sqr.bounds.size.width=(_collectionView.bounds.size.width/2)-4;
    return size;
    
    
}

#pragma mark collection view cell paddings
- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0); // top, left, bottom, right
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}
- (void)requestdidLoad:(id)result{
    [self.assets removeAllObjects];
    NSArray *tmpArr = [result objectForKey:@"data"];
    for (NSDictionary   *tmpDict in tmpArr) {
        if ([tmpDict[@"media_type"] isEqualToString:@"CAROUSEL_ALBUM"]) {
            for (int i=0; i<[tmpDict[@"CAROUSEL_ALBUM"] count]; i++) {
                if ([tmpDict[@"CAROUSEL_ALBUM"][i][@"type"] isEqualToString:@"VIDEO"]) {
                }else{
                    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
                    dic[@"thumb"]=tmpDict[@"carousel_media"][i][@"images"][@"thumbnail"][@"url"];
                    dic[@"image"]=tmpDict[@"carousel_media"][i][@"images"][@"standard_resolution"][@"url"];
                    dic[@"id"]   =[NSString stringWithFormat:@"%@_%d",tmpDict[@"id"],i];
                    if (tmpDict[@"location"] == nil || tmpDict[@"location"] == (id)[NSNull null]) {
                        dic[@"place"]= @"";
                        dic[@"lat"]  = @"";
                        dic[@"longi"]= @"";
                    } else {
                        dic[@"place"]= (tmpDict[@"location"][@"name"]!=nil)?tmpDict[@"location"][@"name"]:@"";
                        dic[@"lat"]  =  (tmpDict[@"location"][@"latitude"]!=nil)?tmpDict[@"location"][@"latitude"]:@"";
                        dic[@"longi"]= (tmpDict[@"location"][@"longitude"]!=nil)?tmpDict[@"location"][@"longitude"]:@"";
                    }
                    dic[@"date"] =tmpDict[@"created_time"];
                    if (dic[@"date"]!=nil) {
                        NSDate *aDate = [NSDate dateWithTimeIntervalSince1970: [dic[@"date"]doubleValue]];
                        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                        [dateFormat setDateFormat:@"dd-MMM-YYYY  HH:mm:aa"];
                        
                        dic[@"date"]=[NSString stringWithFormat:@"%@",[dateFormat stringFromDate:aDate]];
                    }
                    self.assets[self.assets.count]=dic;
                }
            }
        }else if ([tmpDict[@"media_type"] isEqualToString:@"VIDEO"]) {
            
        }
        else{
            NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
            dic[@"thumb"]=tmpDict[@"media_url"];
            dic[@"image"]=tmpDict[@"media_url"];
            dic[@"id"]   =[NSString stringWithFormat:@"%@",tmpDict[@"id"]];
            
            if (tmpDict[@"location"] == nil || tmpDict[@"location"] == (id)[NSNull null]) {
                dic[@"place"]= @"";
                dic[@"lat"]  = @"";
                dic[@"longi"]= @"";
            } else {
                // category name is set
                dic[@"place"]= (tmpDict[@"location"][@"name"]!=nil)?tmpDict[@"location"][@"name"]:@"";
                dic[@"lat"]  =  (tmpDict[@"location"][@"latitude"]!=nil)?tmpDict[@"location"][@"latitude"]:@"";
                dic[@"longi"]= (tmpDict[@"location"][@"longitude"]!=nil)?tmpDict[@"location"][@"longitude"]:@"";
            }
            dic[@"date"] =tmpDict[@"created_time"];
            if (dic[@"date"]!=nil) {
                NSDate *aDate = [NSDate dateWithTimeIntervalSince1970: [dic[@"date"]doubleValue]];
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                [dateFormat setDateFormat:@"dd-MMM-YYYY  HH:mm:aa"];
                dic[@"date"]=[NSString stringWithFormat:@"%@",[dateFormat stringFromDate:aDate]];
            }
            self.assets[self.assets.count]=dic;
        }
    }
    if (self.assets.count < 15) {
        [self loadMoreIGMediaWithNextUrl:self.igNextPageUrl];
    }
}
-(void)fetchingMoreData:(id)result withPaging:(NSDictionary*)pagingDic{
    NSArray *tmpArr = [result objectForKey:@"data"];
    for (NSDictionary   *tmpDict in tmpArr) {
        if ([[tmpDict allKeys] containsObject:@"CAROUSEL_ALBUM"]) {
            for (int i=0; i<[tmpDict[@"CAROUSEL_ALBUM"] count]; i++) {
                if ([tmpDict[@"CAROUSEL_ALBUM"][i][@"type"] isEqualToString:@"VIDEO"]) {
                }else{
                    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
                    dic[@"thumb"]=tmpDict[@"carousel_media"][i][@"images"][@"thumbnail"][@"url"];
                    dic[@"image"]=tmpDict[@"carousel_media"][i][@"images"][@"standard_resolution"][@"url"];
                    dic[@"id"]   =[NSString stringWithFormat:@"%@_%d",tmpDict[@"id"],i];
                    self.assets[self.assets.count]=dic;
                }
            }
        }else if ([tmpDict[@"media_type"] isEqualToString:@"VIDEO"]) {
        }
        else{
            NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
            dic[@"thumb"]=tmpDict[@"media_url"];
            dic[@"image"]=tmpDict[@"media_url"];
            dic[@"id"]   =[NSString stringWithFormat:@"%@",tmpDict[@"id"]];
            self.assets[self.assets.count]=dic;
        }
    }
    isMediaLoading = NO;
    [self.collectionView reloadData];
    
    if (self.assets.count < 15) {
        [self loadMoreIGMediaWithNextUrl:self.igNextPageUrl];
    }
}
-(void)loadMoreIGMediaWithNextUrl:(NSString*)nextUrl{
    if (nextUrl != nil) {
        isMediaLoading = YES;
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:nextUrl]];
        [request setHTTPMethod:@"GET"];
        NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            //sas 10
           // NSString *requestReply = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
            NSDictionary *dic = [[NSDictionary alloc] init];
            dic = jsonObject[@"paging"];
            NSLog(@"paging %@",dic);
            dispatch_async(dispatch_get_main_queue(), ^{
                self.igNextPageUrl = dic[@"next"];
                [self fetchingMoreData:jsonObject withPaging:dic];
                [self dismissViewControllerAnimated:YES completion:nil];
            });
        }] resume];
    }
    
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView.contentOffset.y >= scrollView.contentSize.height - scrollView.frame.size.height)
    {
        //LOAD MORE
        if (!isMediaLoading) {
            [self loadMoreIGMediaWithNextUrl:self.igNextPageUrl];
        }
        
    }
}
#pragma mark - Segue support

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    
    
    if ([[segue identifier] isEqualToString:@"colorEffect"]) {
        
        FilterViewController *desView = [segue destinationViewController];
        //[Global saveImagesInDocumentDirectory:UIImageJPEGRepresentation(sendImage, 1.0) ImageId:@"thumb"];
        desView.inputImage=sendImage;
        
    }
    
    if ([[segue identifier] isEqualToString:@"cropimage"]) {
        
        ImageCropperViewController *desView = [segue destinationViewController];
        desView.isFromPreview = YES;
        desView.productId=_productId;
       // [Global saveImagesInDocumentDirectory:UIImageJPEGRepresentation(sendImage, 1.0) ImageId:@"thumb"];
        desView.imageToEdit=sendImage;
        desView.activeDictionary = sender;
    }
    
    
    
}
#pragma mark - Facebook Delegate

-(void)fbRequestSuccessWithData:(NSMutableArray *)dataArray {
    
    [_assets removeAllObjects];
    _assets=[dataArray mutableCopy];
    [_collectionView reloadData];
    
    
}
//[Global saveImagesInDocumentDirectory:UIImageJPEGRepresentation(result, 1.0) ImageId:@"frame"];
#pragma mark - ImagePopUpDelegate
-(void)didSelectImage:(NSIndexPath *)indexPath andImage:(UIImage *)selectImage{
    [Global makeSizeDictionary:sku];
    NSMutableDictionary *activeSizeDic = [[NSMutableDictionary alloc] init];
    //    AlbumCollectionViewCell *cell = (AlbumCollectionViewCell *)[_collectionView cellForItemAtIndexPath:indexPath];
    //    if (![_infoDic[@"type"] isEqualToString:@"fb"] && ![_infoDic[@"type"] isEqualToString:@"insta"]) {
    sendImage= selectImage;
    BOOL ispara = [self isParanoma:selectImage];
    if (ispara) {
        [Global saveImagesInDocumentDirectory:UIImagePNGRepresentation(selectImage) ImageId:@"frame"];
        sendImage= [UIImage imageWithData:UIImagePNGRepresentation(selectImage)];
    }
    
    //    }

    if ([self checkImageResolution:sendImage]) {
        for (NSString *key in [globalSizeDictionary allKeys]) {
            NSMutableArray *sizeToSaveArr = [[NSMutableArray alloc] init];
            NSArray *sizeArr =  globalSizeDictionary[key];
            for (NSDictionary *sizeDic in sizeArr) {
                NSString *selected = [self checkImageSize:sendImage :CGSizeMake([sizeDic[WIDTH] floatValue], [sizeDic[HEIGHT] floatValue]) type:key];
                NSMutableDictionary *objDic = [[NSMutableDictionary alloc] init];
                objDic[ISACTIVE] = ![selected isEqualToString:@""] ? @"YES" : @"NO";
                objDic[WIDTH] = sizeDic[WIDTH];
                objDic[HEIGHT] = sizeDic[HEIGHT];
                objDic[ZOOMFACTOR] = selected;
                [sizeToSaveArr addObject:objDic];
            }
            activeSizeDic[key] = [sizeToSaveArr mutableCopy];
        }
        
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"FULL_IMAGE"];
        
        [self performSegueWithIdentifier:@"cropimage" sender:activeSizeDic];
        
    }
}
-(BOOL)isParanoma:(UIImage*)selectImage{
    float smallest = MIN(selectImage.size.width, selectImage.size.height);
    float largest = MAX(selectImage.size.width, selectImage.size.height);

    float ratio = largest/smallest;

    float maximumRatioForNonePanorama = 4 / 3; // check with your ratio

    if (ratio > maximumRatioForNonePanorama) {
        // it is a panorama
        return YES;
    }
    return  NO;
}
-(BOOL)checkImageResolution:(UIImage*)image{
    if ((image.size.width * image.scale)  < MIN_IMAGE_SIZE && ( image.size.height * image.scale )< MIN_IMAGE_SIZE) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"FrameShop" message:@"The image selected is not high enough quality, please choose another image for better priniting." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }else{
        return YES;
    }
}
-(NSString*)checkImageSize:(UIImage*)selectedImage :(CGSize)ratioSelected type:(NSString *)key{
//    if ([sku isEqualToString:CANVAS_PRINTING_SKU]) {
//        if (ratioSelected.width < 8  || ratioSelected.height < 8) {
//            return @"";
//        }
//    }
    float requiredSize,actualImageSize;
    if ([key isEqualToString:@"Landscape"]) {
        requiredSize = ratioSelected.width*300;
        actualImageSize = selectedImage.size.width;
    }else if ([key isEqualToString:@"Portrait"]){
        requiredSize = ratioSelected.height*300;//1800
        actualImageSize = selectedImage.size.height;//2160
    }
    else{
        requiredSize = ratioSelected.height*300;//1800
        actualImageSize = MIN(selectedImage.size.width, selectedImage.size.height);//2160
    }
    float scaleFactor = requiredSize/actualImageSize;//1800/2160
    if (scaleFactor>4) {
        //disableSize
        return @"";
    }else{
        //enableSize
        float zoomFactorPercentage = (4/scaleFactor);//*100
        return [NSString stringWithFormat:@"%f", zoomFactorPercentage];
    }
}

@end
