//
//  AlbumViewController.m
//  FrameShop
//
//  Created by Saurabh anand on 10/12/19.
//  Copyright © 2019 Mayank Barnwal. All rights reserved.
//

#import "AlbumViewController.h"
#import "Global.h"
@interface AlbumViewController (){
    UIPageViewController *pageViewController;
    NSArray *pages;
    NSMutableArray *viewControllers;
    NSInteger index,pageNumber;
   
}

@end

@implementation AlbumViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
   
    
  
    self.title=@"Select Image";
    [Global addshadow:_tabBar radious:5.0f];
    _deviceBtn.selected=YES;
   
    
    pages = @[@"Camera", @"Facebook", @"Instagram"];
    [self setUpTabBar];
    [self setUpPageViewController];
  


    [Global roundCorner:_cartCount roundValue:_cartCount.frame.size.height/2 borderWidth:2 borderColor:[UIColor whiteColor]];
    
    UISwipeGestureRecognizer * swipeleft=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeleft:)];
       swipeleft.direction=UISwipeGestureRecognizerDirectionLeft;
       [self.view addGestureRecognizer:swipeleft];
    
     [self.navigationItem setBackBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil]];
    
   // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishCart) name:CART_REFRESH object:nil];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    int count=[[[NSUserDefaults standardUserDefaults] valueForKey:CART_COUNT] intValue];
       if (count>9) {
           _cartCount.text=@"9+";
       }
       else{
       _cartCount.text=[NSString stringWithFormat:@"%d",[[[NSUserDefaults standardUserDefaults] valueForKey:CART_COUNT] intValue]];
       }
           _cartCount.hidden=[_cartCount.text isEqualToString:@"0"];
}
-(void)didFinishCart{
     int count=[[[NSUserDefaults standardUserDefaults] valueForKey:CART_COUNT] intValue];
       if (count>9) {
           _cartCount.text=@"9+";
       }
       else{
       _cartCount.text=[NSString stringWithFormat:@"%d",[[[NSUserDefaults standardUserDefaults] valueForKey:CART_COUNT] intValue]];
       }
           _cartCount.hidden=[_cartCount.text isEqualToString:@"0"];
}
-(void)setUpTabBar{
  
    float indicatorBarHeight=3.0f;
    
    [self setUpItemsToTabBarWithView:_deviceView andX:0 andIncludedBtn:_deviceBtn];
    [self setUpItemsToTabBarWithView:_fbView andX:_deviceView.frame.size.width andIncludedBtn:_fbBTn];
    [self setUpItemsToTabBarWithView:_instaView andX:_deviceView.frame.size.width*2 andIncludedBtn:_instaBtn];
    
    
    
    _indiCatorBar.frame=CGRectMake(0, _deviceView.frame.size.height-indicatorBarHeight, _deviceView.frame.size.width, indicatorBarHeight);
    [_tabBar addSubview:_indiCatorBar];
    [Global roundCorner:_indiCatorBar roundValue:_indiCatorBar.frame.size.height/2 borderWidth:0 borderColor:nil];
    
    
}

-(void)setUpItemsToTabBarWithView:(UIView *)view andX:(float )itemX andIncludedBtn:(UIButton *)btn{
    float tabBarHeight=self.tabBar.frame.size.height;
       float tabBarWidth=self.view.frame.size.width;
       float tabItemsWidth=tabBarWidth/3;
    
    view.frame=CGRectMake(itemX,0 , tabItemsWidth, tabBarHeight);
    btn.frame=CGRectMake((view.frame.size.width/2)-(btn.frame.size.width/2), (view.frame.size.height/2)-(btn.frame.size.height/2), btn.frame.size.width, btn.frame.size.height);
   // btn.contentEdgeInsets = UIEdgeInsetsMake(5,10,5,10);
       [_tabBar addSubview:view];
    
}

-(void)slideBarToView:(UIView *)view{
  
   [UIView animateWithDuration:0.3f delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
       self.indiCatorBar.frame  = CGRectMake(view.frame.origin.x, self.indiCatorBar.frame.origin.y, self.indiCatorBar.frame.size.width,self.indiCatorBar.frame.size.height);
       
   } completion:^(BOOL finished) {
       
   }];
}

-(void)setUpPageViewController{
     pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
     pageViewController.dataSource = self;
    pageViewController.delegate=self;
    for (UIView *v in pageViewController.view.subviews) {
        if ([v isKindOfClass:[UIScrollView class]]) {
          //  ((UIScrollView *)v).delegate = self;
        }
    }
    viewControllers = [NSMutableArray array];

    
    for (int i = 0; i < pages.count; i++) {
           CameraViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"Camera"];
           vc.index = i;
        vc.productId=_productId;
           [viewControllers addObject:vc];
       }
       
       // Set the default page to the first in the array and send it to the page view controller
       NSArray *defaultViewController = [NSArray arrayWithObject:viewControllers[0]];
       
       [pageViewController setViewControllers:defaultViewController direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
       
       [self addChildViewController:pageViewController];
       [self.view addSubview:pageViewController.view];
       [pageViewController didMoveToParentViewController:self];
    index=0;
    [pageViewController.view addSubview:_tabBar];
    [Global addshadow:_tabBar radious:1.0];
        
}

-(void)swipeleft:(UISwipeGestureRecognizer*)gestureRecognizer
{
     if ([gestureRecognizer locationInView:self.view].x>self.view.bounds.size.width*0.80) {
            [Global showSideMenu];
       }
}

- (IBAction)cartBtn:(id)sender{
    [Global callMenuViewWithCode:@"0"];
}
- (IBAction)menuBtn:(id)sender{
    [Global showSideMenu];
}


-(UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    
    NSInteger index = ((CameraViewController*) viewController).index;
    
    if (index == 0) {
        return nil;
    }
    
    index--;
    //NSLog(@"we are %ld",(long)index);
    return viewControllers[index];
}

-(UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    
    NSInteger index = ((CameraViewController*) viewController).index;
    
    if (index == viewControllers.count - 1) {
        return nil;
    }
    
    index++;
   // NSLog(@"we are at%ld",(long)index);
    return viewControllers[index];
}

- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray<UIViewController *> *)previousViewControllers transitionCompleted:(BOOL)completed{
    
    if (completed) {
        
 NSInteger currentIndex = ((CameraViewController *)pageViewController.viewControllers.firstObject).index;
        NSLog(@"we are ats%ld",(long)currentIndex);
        pageNumber=(long)currentIndex;
        switch (currentIndex) {
            case 0:
                [self slideBarToView:_deviceView];
                _deviceBtn.selected=YES;
                   _fbBTn.selected=NO;
                   _instaBtn.selected=NO;
                break;
                case 1:
                [self slideBarToView:_fbView];
                _deviceBtn.selected=NO;
                   _fbBTn.selected=YES;
                   _instaBtn.selected=NO;
                break;
                case 2:
                [self slideBarToView:_instaView];
                _deviceBtn.selected=NO;
                   _fbBTn.selected=NO;
                   _instaBtn.selected=YES;
                break;
                
            default:
                break;
        }
        
//        NSArray *defaultViewController = [NSArray arrayWithObject:viewControllers[currentIndex]];
//
//              [pageViewController setViewControllers:defaultViewController direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
        

    }
    
    
}

- (void)pageViewController:(UIPageViewController *)pageViewController willTransitionToViewControllers:(NSArray<UIViewController *> *)pendingViewControllers API_AVAILABLE(ios(6.0)){
    //NSInteger index = ((CameraViewController*) pendingViewControllers[0]).index;
          
         
        
}

-(void)slidePageOfIndex:(int)index andDirection:(UIPageViewControllerNavigationDirection) direction{
    
            
              
              NSArray *defaultViewController = [NSArray arrayWithObject:viewControllers[index]];
                    
                    [pageViewController setViewControllers:defaultViewController direction:direction animated:YES completion:nil];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
  
    float x=0;
    
   CGPoint point = scrollView.contentOffset;

    float percentComplete;
    percentComplete = fabs(point.x - scrollView.frame.size.width);
 
    float per=percentComplete*100/scrollView.frame.size.width;
   
    CGFloat yVelocity = [scrollView.panGestureRecognizer velocityInView:scrollView].x;
    NSLog(@"====> %f",yVelocity);
      if (yVelocity < 0) {
          NSLog(@"Left");
            x=((pageNumber)*self.indiCatorBar.frame.size.width)+(self.indiCatorBar.frame.size.width*per/100);

      } else if (yVelocity > 0) {
          NSLog(@"Right");
          per=100-per;
            x=((pageNumber-1)*self.indiCatorBar.frame.size.width)+(self.indiCatorBar.frame.size.width*per/100);

      } else {
          NSLog(@"Cant predict");
          return;
      }
    
  // x=((pageNumber)*self.indiCatorBar.frame.size.width)+(self.indiCatorBar.frame.size.width*per/100);

////
    [UIView animateWithDuration:0.2f delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        self.indiCatorBar.frame  = CGRectMake(x, self.indiCatorBar.frame.origin.y, self.indiCatorBar.frame.size.width,self.indiCatorBar.frame.size.height);

    } completion:^(BOOL finished) {

    }];
}


- (IBAction)deviceBtnAction:(id)sender {
    
    if (_fbBTn.selected) {
           [self slidePageOfIndex:0 andDirection:UIPageViewControllerNavigationDirectionReverse];
       }
      else if (_instaBtn.selected){
            [self slidePageOfIndex:0 andDirection:UIPageViewControllerNavigationDirectionReverse];
      }
    
    
   _deviceBtn.selected=YES;
      _fbBTn.selected=NO;
      _instaBtn.selected=NO;
    [self slideBarToView:_deviceView];
   
}

- (IBAction)fbBtnAction:(id)sender {
    if (_deviceBtn.selected) {
        [self slidePageOfIndex:1 andDirection:UIPageViewControllerNavigationDirectionForward];
    }
    else if (_instaBtn.selected){
         [self slidePageOfIndex:1 andDirection:UIPageViewControllerNavigationDirectionReverse];
    }
    
    
     _deviceBtn.selected=NO;
     _fbBTn.selected=YES;
     _instaBtn.selected=NO;
    
    
    [self slideBarToView:_fbView];
    
}
- (IBAction)instaBtnAction:(id)sender {
    
    if (_fbBTn.selected || _deviceBtn.selected) {
        [self slidePageOfIndex:2 andDirection:UIPageViewControllerNavigationDirectionForward];
    }
    else if (_instaBtn.selected){
         [self slidePageOfIndex:2 andDirection:UIPageViewControllerNavigationDirectionForward];
    }
    
    
    _instaBtn.selected=YES;
    _deviceBtn.selected=NO;
    _fbBTn.selected=NO;
          
    [self slideBarToView:_instaView];
    // [self slidePageOfIndex:2 andDirection:UIPageViewControllerNavigationDirectionForward];
}
@end
