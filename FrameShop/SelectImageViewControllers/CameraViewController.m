//
//  CameraViewController.m
//  FrameShop
//
//  Created by Saurabh anand on 10/12/19.
//  Copyright © 2019 Mayank Barnwal. All rights reserved.
//

#import "CameraViewController.h"
#import "Global.h"
#import "IGLoginViewController.h"
@interface CameraViewController ()<SasFacebookCentralScriptDelegate,IGLoginViewControllerDelegate>{
    AlbumLoginViewController *loginView;
    NSString* currentType;
    IGLoginViewController *iGLoginViewController;
}
@property (nonatomic, strong) PHPhotoLibrary *assetsLibrary;
@property (nonatomic, strong) NSMutableArray *groups;
@property (nonatomic, copy) NSArray *fetchResults;
@property (nonatomic, copy) NSMutableArray *fbAssets;
@end

@implementation CameraViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    loginView=[[AlbumLoginViewController alloc] init];
    
    UINib * nib = [UINib nibWithNibName:@"AlbumCollectionViewCell" bundle:nil];
    [_collectionView registerNib:nib forCellWithReuseIdentifier:@"AlbumCollectionViewCell"];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshCamView) name:@"acess" object:nil];
    
    if (self.groups == nil) {
        _groups = [[NSMutableArray alloc] init];
    }
    else {
        [self.groups removeAllObjects];
    }
    
  
    
    
    
    // [self.navigationItem.backBarButtonItem setTitle:@""];
    
    
  
    
    UISwipeGestureRecognizer * swipeleft=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeleft:)];
    swipeleft.direction=UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeleft];
    
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setUpCollectionView];
    [self.navigationItem setBackBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil]];
    self.navigationController.navigationBar.tintColor=[UIColor blackColor];
}

-(void)swipeleft:(UISwipeGestureRecognizer*)gestureRecognizer
{
    if ([gestureRecognizer locationInView:self.view].x>self.view.bounds.size.width*0.80) {
        [Global showSideMenu];
    }
}

-(void)setUpCollectionView{
    if (_index==0) {
        
        currentType = @"Cam";
        [self callCam];
    }
    else if (_index==1){
        currentType = @"fb";
        loginView.type=@"fb";
        [self isFacebookAlreadyLoggedIn];
//        [self showLoginOption];
        //fb
    }
    else if (_index==2){
        currentType = @"insta";
        loginView.type=@"insta";
        // [self ValidateIgToken];
        [self showLoginOption];
        //insta
    }
}
-(void)isFacebookAlreadyLoggedIn{
    SasFacebookCentralScript *fb=[[SasFacebookCentralScript alloc] init];
    fb.delegate = self;
    if ([fb isLogin]) {
        [_groups removeAllObjects];
        [_groups addObjectsFromArray:_fbAssets];
        [_collectionView reloadData];
        if (_groups.count == 0) {
            [fb getFBUserAlbums];
        }
       
    }else{
        [self showLoginOption];
    }
}
-(void)showLoginOption{
    
    loginView.view.frame=CGRectMake(_collectionView.frame.origin.x, 0, self.view.bounds.size.width, self.view.bounds.size.height);
    if ([currentType isEqualToString:@"fb"]) {
        [Global deleteFilesFromCacheWhichContain:@"ig_"];
        [loginView.loginBtn addTarget:self action:@selector(signInWithFacebook:) forControlEvents:UIControlEventTouchUpInside];
    }else{
        [Global deleteFilesFromCacheWhichContain:@"fb_"];
        [loginView.loginBtn addTarget:self action:@selector(signInWithIG:) forControlEvents:UIControlEventTouchUpInside];
    }
    [self.view addSubview:loginView.view];
    [self addChildViewController:loginView];
}
- (IBAction)signInWithIG:(id)sender {
    [self getIGMedia];
}
- (void)IgAuthenticatedSuccessfully{
    [self getIGMedia];
}
-(void)ValidateIgToken{
    NSString *instaToken = [[NSUserDefaults standardUserDefaults] valueForKey:INSTAGRAM_ACCESS_TOKEN];
    if (instaToken == nil) {
        self->iGLoginViewController = [[IGLoginViewController alloc] initWithNibName:@"IGLoginViewController" bundle:[NSBundle mainBundle]];
        self->iGLoginViewController.delegate = self;
        [self.navigationController pushViewController:self->iGLoginViewController animated:YES];
        return;
    }
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://graph.instagram.com/me/media?fields=id,caption,media_url,media_type&access_token=%@",instaToken]]];
    [request setHTTPMethod:@"GET"];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error != nil) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self showLoginOption];
            });
        }else{
            [self getIGMedia];
        }
    }] resume];
}
-(void)getIGMedia{
    NSString *instaToken = [[NSUserDefaults standardUserDefaults] valueForKey:INSTAGRAM_ACCESS_TOKEN];
    if (instaToken == nil) {
        self->iGLoginViewController = [[IGLoginViewController alloc] initWithNibName:@"IGLoginViewController" bundle:[NSBundle mainBundle]];
        self->iGLoginViewController.delegate = self;
        [self.navigationController pushViewController:self->iGLoginViewController animated:YES];
        return;
    }
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://graph.instagram.com/me/media?fields=id,caption,media_url,media_type&access_token=%@",instaToken]]];
    [request setHTTPMethod:@"GET"];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error != nil) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[NSUserDefaults standardUserDefaults] setObject:nil forKey:INSTAGRAM_ACCESS_TOKEN];
                self->iGLoginViewController = [[IGLoginViewController alloc] initWithNibName:@"IGLoginViewController" bundle:[NSBundle mainBundle]];
                self->iGLoginViewController.delegate = self;
                [self.navigationController pushViewController:self->iGLoginViewController animated:YES];
            });
        }else{
            id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
            if (jsonObject[@"error"] != nil) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:INSTAGRAM_ACCESS_TOKEN];
                    self->iGLoginViewController = [[IGLoginViewController alloc] initWithNibName:@"IGLoginViewController" bundle:[NSBundle mainBundle]];
                    self->iGLoginViewController.delegate = self;
                    [self.navigationController pushViewController:self->iGLoginViewController animated:YES];
                });
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSDictionary *pagingDic = [[NSDictionary alloc] init];
                    pagingDic = jsonObject[@"paging"];
                    NSLog(@"paging==%@",pagingDic);
                    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
                    dic[@"json"] = jsonObject;
                    dic[@"next"] = [NSString stringWithFormat:@"%@",pagingDic[@"next"]];
                    [self performSegueWithIdentifier:@"photo" sender:dic];
                });
            }
            
        }
    }] resume];
}




- (IBAction)signInWithFacebook:(id)sender {
    SasFacebookCentralScript *fb=[[SasFacebookCentralScript alloc] init];
    fb.delegate = self;
    [fb loginWithFacebook];
}
- (IBAction)signOutFacebook:(id)sender {
    SasFacebookCentralScript *fb=[[SasFacebookCentralScript alloc] init];
    [fb logOutFromFb];
}
-(void)getFbAlbums{
    if (_index == 1) {
        currentType = @"fb";
        SasFacebookCentralScript *fb=[[SasFacebookCentralScript alloc] init];
        fb.delegate = self;
        if ([fb isLogin]) {
            if (_fbAssets.count == 0) {
            }else{
                [_groups removeAllObjects];
                _groups = _fbAssets;
                [_collectionView reloadData];
            }
            [fb getFBUserAlbums];
        }else{
            
            [fb loginWithFacebook];
        }
    }else{
        
    }
}

-(void)callCam{
    currentType=@"cam";
    PHAuthorizationStatus prevStatus = [PHPhotoLibrary authorizationStatus];
    if (@available(iOS 14, *)) {
        prevStatus = [PHPhotoLibrary authorizationStatusForAccessLevel:PHAccessLevelReadWrite];
        if (prevStatus == PHAuthorizationStatusNotDetermined) {
            [PHPhotoLibrary requestAuthorizationForAccessLevel:(PHAccessLevelReadWrite) handler:^(PHAuthorizationStatus status) {
                if (status == PHAuthorizationStatusAuthorized) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self prepareForCamera];
                    });
                }
                else if (status == PHAuthorizationStatusLimited){
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self performSelector:@selector(prepareForCamera) withObject:nil afterDelay:0.2];
                    });
                    
                }
            }];
        }else if (prevStatus == PHAuthorizationStatusDenied){
            [self performSelector:@selector(goTOAppSettings) withObject:nil afterDelay:0.2];
        }
        else{
            [self prepareForCamera];
        }
    }else{
        [self prepareForCamera];
    }
}
-(void) goTOAppSettings{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"We were unable to load your album groups. Sorry!"
                                  message:@"You can enable access in Privacy Settings"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* OK = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
        
    }];
    UIAlertAction* Settings = [UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString] options:@{} completionHandler:nil];
    }];
    [alert addAction:OK];
    [alert addAction:Settings];
    [self presentViewController:alert animated:YES completion:nil];
}
-(void)prepareForCamera{
    currentType=@"cam";
    
    
    self.assetCollectionSubtypes = @[
        @(PHAssetCollectionSubtypeSmartAlbumUserLibrary),
        @(PHAssetCollectionSubtypeAlbumMyPhotoStream),
        @(PHAssetCollectionSubtypeSmartAlbumPanoramas),
        @(PHAssetCollectionSubtypeSmartAlbumScreenshots),
        @(PHAssetCollectionSubtypeSmartAlbumFavorites),
        /*@(PHAssetCollectionSubtypeSmartAlbumAllHidden),*/
        @(PHAssetCollectionSubtypeSmartAlbumRecentlyAdded),
       
        @(PHAssetCollectionSubtypeSmartAlbumSelfPortraits),
        
        @(PHAssetCollectionSubtypeSmartAlbumBursts),
        @(PHAssetCollectionSubtypeAlbumCloudShared),
        @(PHAssetCollectionSubtypeAlbumMyPhotoStream)
    ];
    
    
    
    PHFetchResult *smartAlbums1 = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeSmartAlbum subtype:PHAssetCollectionSubtypeAny options:nil];
    PHFetchResult *userAlbums1 = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeAlbum subtype:PHAssetCollectionSubtypeAny options:nil];
    self.fetchResults = @[smartAlbums1, userAlbums1];
    
    
    // [self getAlbum];
    [self.groups removeAllObjects];
    
    NSArray *assetCollectionSubtypes = self.assetCollectionSubtypes;
    NSMutableDictionary *smartAlbums = [NSMutableDictionary dictionaryWithCapacity:assetCollectionSubtypes.count];
    NSMutableArray *userAlbums = [NSMutableArray array];
    
    for (PHFetchResult *fetchResult in self.fetchResults) {
        
        
        [fetchResult enumerateObjectsUsingBlock:^(PHAssetCollection *assetCollection, NSUInteger index, BOOL *stop) {
            
            PHAssetCollectionSubtype subtype = assetCollection.assetCollectionSubtype;
    
                if (subtype == PHAssetCollectionSubtypeAlbumRegular) {
                    [userAlbums addObject:assetCollection];
                } else if ([assetCollectionSubtypes containsObject:@(subtype)]) {
                    if (!smartAlbums[@(subtype)]) {
                        smartAlbums[@(subtype)] = [NSMutableArray array];
                    }
                    [smartAlbums[@(subtype)] addObject:assetCollection];
                }
            
        }];
    }
    
    NSMutableArray *assetCollections = [NSMutableArray array];
    
    // Fetch smart albums
    for (NSNumber *assetCollectionSubtype in assetCollectionSubtypes) {
        NSArray *collections = smartAlbums[assetCollectionSubtype];
        
        
        if (collections) {
            
            [assetCollections addObjectsFromArray:collections];
            
        }
    }
    
    // Fetch user albums
    [userAlbums enumerateObjectsUsingBlock:^(PHAssetCollection *assetCollection, NSUInteger index, BOOL *stop) {
        [assetCollections addObject:assetCollection];
    }];
    
    
    
    
    
    
    //self.groups = assetCollections;
    
    for (int i=0; i<assetCollections.count; i++) {
        
        NSPredicate *predicate=[NSPredicate predicateWithFormat:@"mediaType !=%i",PHAssetMediaTypeVideo];
        
        
        PHAssetCollection *groupForCell = assetCollections[i];
        PHFetchOptions *options = [PHFetchOptions new];
        options.predicate=predicate;
        PHFetchResult *fetchResult = [PHAsset fetchAssetsInAssetCollection:groupForCell options:options];
        if (fetchResult.count!=0) {
            self.groups[_groups.count]=assetCollections[i];
        }
        
    }
    
    if (assetCollections.count!=0) {
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    }
    
    [self.collectionView reloadData];
}

-(void)refreshCamView{
    if ([currentType isEqualToString:@"cam"]) {
        //[self performSelector:@selector(callCam) withObject:self afterDelay:3.0 ];
        [self callCam];
    }
}

#pragma mark - UICollectionViewDelegate

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    
    [_collectionView.collectionViewLayout invalidateLayout];
    
    return (_groups.count==1)?2:_groups.count;
}

#define kImageViewTag 1 // the image view inside the collection view cell prototype is tagged with "1"

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
    static NSString *identifier = @"AlbumCollectionViewCell";
    
    AlbumCollectionViewCell *cells =[_collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    if (indexPath.row>=_groups.count) {
        cells.contentView.hidden=true;
        return cells;
    }
    
    
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
    if ([currentType isEqualToString:@"cam"]) {
        dic[@"assets"] = self.groups[indexPath.row];
    }
    else if ([currentType isEqualToString:@"fb"]){
        dic[@"fbAlbumId"] = self.groups[indexPath.row][@"id"];
        dic[@"name"]     = self.groups[indexPath.row][@"name"];
        dic[@"count"]    = self.groups[indexPath.row][@"count"];
    }
    dic[@"type"]= currentType;
    cells.index=(int)indexPath.row;
    cells.infoDic=dic;
    
    
    
    float cellWidth=((_collectionView.bounds.size.width-1)/2)-30;
    
    
    cells.iconImage.frame=CGRectMake(15, 15, cellWidth, cellWidth);
    
    // [Global addShadowandroundCornerToView:cells.iconImage andCornerRadious:20];
    [Global roundCorner:cells.iconImage roundValue:20 borderWidth:0 borderColor:nil];
    [cells refreshView];
    return cells;
    
    
}

- (BOOL)collectionView:(UICollectionView *)collectionView
shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (BOOL)collectionView:(UICollectionView *)collectionView
shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath;
{
    return YES;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row>=_groups.count) {
        
        return ;
    }
    
    [self performSegueWithIdentifier:@"photo" sender:[NSString stringWithFormat:@"%ld",indexPath.row]];
    
    
}
#pragma mark collection view cell layout / size
- (CGSize)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
    NSArray *nibViews = [[NSBundle mainBundle] loadNibNamed:@"AlbumCollectionViewCell" owner:self options:nil];
    AlbumCollectionViewCell *sqr = [nibViews objectAtIndex:0];
    
    
    float cellWidth=((_collectionView.bounds.size.width-1)/2);
    float cellHeight=cellWidth+ sqr.albumName.frame.size.height+sqr.photoCount.frame.size.height;
    
    CGSize size=CGSizeMake(cellWidth, cellHeight);
    //sqr.bounds.size.width=(_collectionView.bounds.size.width/2)-4;
    return size;
    
    
}

#pragma mark collection view cell paddings
- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0); // top, left, bottom, right
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}


#pragma mark - Segue support

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"photo"]) {
        
        NSString *indexNo=(NSString *)sender;
        // hand off the asset group (i.e. album) to the next view controller
        if ([currentType isEqualToString:@"insta"]) {
            PhotoViewController *albumContentsViewController = [segue destinationViewController];
            albumContentsViewController.igJson = sender[@"json"];
            albumContentsViewController.igNextPageUrl = sender[@"next"];
            albumContentsViewController.productId=_productId;
            NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
            dic[@"type"]=currentType;
            
            albumContentsViewController.assets=[self.groups mutableCopy];
            albumContentsViewController.infoDic=dic;
        }else{
            PhotoViewController *albumContentsViewController = [segue destinationViewController];
            albumContentsViewController.assetsGroup = self.groups[[indexNo intValue]];
            NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
            dic[@"type"]=currentType;
            if ([currentType isEqualToString:@"fb"]) {
                dic[@"fbAlbmId"]=_fbAssets[[indexNo intValue]][@"id"];
                dic[@"name"]=_fbAssets[[indexNo intValue]][@"name"];
            }
            albumContentsViewController.assets=[self.groups mutableCopy];
            albumContentsViewController.infoDic=dic;
          
            albumContentsViewController.productId=_productId;
        }
        
        
        
        
    }
}

#pragma mark -SasFacebookCentralScriptDelegate
-(void)fbLogInSuccessFulWithInfo:(NSMutableDictionary *)infoDic {
    [self getFbAlbums];
}
-(void)fbLogInFailsFulWithError:(NSString *)error {
    
}
-(void)fbLogInCancel {
    
}
-(void)fbRequestSuccessWithData:(NSMutableArray *)dataArray {
    [_fbAssets removeAllObjects];
    _fbAssets = [dataArray mutableCopy];
    [_groups removeAllObjects];
    _groups = [_fbAssets mutableCopy];
    [loginView.view removeFromSuperview];
    [_collectionView reloadData];
}
-(void)fbRequestFailsWithError:(NSString *)error {
    
}



@end
