//
//  CanvasCoreViewController.m
//  FrameShop
//
//  Created by Saurabh Srivastav on 04/12/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//
#import "Global.h"
#import "CanvasCoreViewController.h"
#define TOP_IMAGEVIEW_MARGINE    20
#define TOP_IMAGE_CUT_MARGINE    4
//#import "Frameshop-Swift.h"

@interface CanvasCoreViewController (){
    NSMutableArray * dataArray;
    UIImage *mainImage;
    float widthMargin,heightMargine;
    float leftRightImageWidth,upBottomImageHeight;
    UIView *leftUpCorner,*leftDownCorner,*rightUpCorner,*rightDownCorner,*temView;
    UIBezierPath *path;
    CAShapeLayer *shapeLayer;
    CGRect superCropRect;
    CGRect cropRect;
    UIImage * croppedImage;
    DbManager *db;
    NSMutableDictionary *productDic;
    NSString *colorStr;
    NSTimer *timer;
    NSTimeInterval timeStamp;
    BOOL isFirstPrice,isLoaded,isMenu;
    UIColor *selectColor,*unselectColor;
    UIImageView * productPreviewImage;
}

@end

@implementation CanvasCoreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UINib * nib = [UINib nibWithNibName:@"CoreCollectionViewCell" bundle:nil];
    [_coreCollection registerNib:nib forCellWithReuseIdentifier:@"CoreCollectionViewCell"];
    nib = [UINib nibWithNibName:@"GalleryWrapCollectionViewCell" bundle:nil];
    [_coreCollection registerNib:nib forCellWithReuseIdentifier:@"GalleryWrapCollectionViewCell"];
    nib = [UINib nibWithNibName:@"ColorWrapCollectionViewCell" bundle:nil];
    [_coreCollection registerNib:nib forCellWithReuseIdentifier:@"ColorWrapCollectionViewCell"];
   
    db=[[DbManager alloc] init];
    dataArray = [[NSMutableArray alloc] init];
    dataArray[dataArray.count] = [[NSDictionary alloc] initWithObjectsAndKeys:@"Black",@"name",@"blackCore.png",@"imageName" , nil];
    dataArray[dataArray.count] = [[NSDictionary alloc] initWithObjectsAndKeys:@"White",@"name",@"blackCore.png",@"imageName" , nil];
    dataArray[dataArray.count] = [[NSDictionary alloc] initWithObjectsAndKeys:@"Mirror",@"name",@"blackCore.png",@"imageName" , nil];
    temView = [[UIView alloc] init];
    
    float zoomF =( (100 - [[[NSUserDefaults standardUserDefaults] valueForKey:@"zoomF"] floatValue]) * [[[NSUserDefaults standardUserDefaults] valueForKey:@"maxZoomF"] floatValue])/100;
    if (zoomF == 0) {
        zoomF = 1;
    }
   else if (zoomF<1) {
        zoomF = ([[[NSUserDefaults standardUserDefaults] valueForKey:@"maxZoomF"] floatValue]) - zoomF;
        //zoomF += 1;
    }
    
    
    _nextBtnLoader.hidden = YES;
    _scrollView.minimumZoomScale = 1;
    _scrollView.maximumZoomScale = zoomF;
    _scrollView.delegate = self;
    path = [UIBezierPath bezierPath];
    shapeLayer = [CAShapeLayer layer];
    
    timeStamp=0;
    isFirstPrice=true;
    selectColor = _nxtBtn.backgroundColor;
    
    unselectColor = _colorBtn.backgroundColor;
    
    _nxtBtn.backgroundColor=[UIColor lightGrayColor];
    _nxtBtn.enabled=NO;
    
    [self deselectAllBtn];
    _mirrorBtn.backgroundColor = selectColor;
    _mirrorBtn.selected = YES;
    
    
    if (_previewImageView.image == nil) {
        
        
        
        [_loaderActivity startAnimating];
        
        [self performSelector:@selector(genrateSlideViewImage) withObject:self afterDelay:1.0 ];
    }
    [self.navigationItem setBackBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil]];
    
    leftUpCorner = [[UIView alloc] init];
    leftDownCorner = [[UIView alloc] init];
    rightUpCorner = [[UIView alloc] init];
    rightDownCorner = [[UIView alloc] init];
    
    NSMutableArray *arr=[[NSMutableArray alloc] init];
    arr=[db getDataForField:USER_PRODUCT where:nil];
    if (arr.count!=0) {
        productDic=arr[0];
        
        NSArray * arrD = [db getDataForField:PRODUCT_TABLE where:[NSString stringWithFormat:@"id='%@'",arr[0][@"id"]] ];
        
        if (arrD.count != 0) {
            productDic[@"sku"]= arrD[0][@"sku"];
        }
        
    }
    [Global roundCorner:_cartCount roundValue:_cartCount.frame.size.height/2 borderWidth:2 borderColor:[UIColor whiteColor]];
    _priceView.frame = CGRectMake(self.view.frame.size.width -97, 10, 97, 43);
    CGRect bounds = _priceView.bounds;
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:bounds
                                                   byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerBottomLeft)
                                                         cornerRadii:CGSizeMake(_priceView.frame.size.height/2, _priceView.frame.size.height/2)];
    
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = bounds;
    maskLayer.path = maskPath.CGPath;
    
    _priceView.layer.mask = maskLayer;
    _activity.hidden = false;
    [_activity startAnimating];
    
    UITapGestureRecognizer *swipeLeft = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTaponPriceLabel:)];
     [self.priceView addGestureRecognizer:swipeLeft];
    
    _priceLbl.frame=CGRectMake(20, 0, _priceView.frame.size.width-40, _priceView.frame.size.height);
    _dotImage.frame=CGRectMake(8, (_priceView.frame.size.height-_dotImage.frame.size.height)/2, _dotImage.frame.size.width, _dotImage.frame.size.height);
    _activity.frame=CGRectMake((_priceView.frame.size.width- _activity.frame.size.width)/2, (_priceView.frame.size.height- _activity.frame.size.height)/2,  _activity.frame.size.width,  _activity.frame.size.height);
    _activity.hidden=YES;
    _priceLbl.text=@"";
    _dotImage.hidden = YES;
    
   // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishCart) name:CART_REFRESH object:nil];
    UISwipeGestureRecognizer * swipeleft=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeleft:)];
    swipeleft.direction=UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeleft];
    
}

- (void)handleSingleTaponPriceLabel:(UITapGestureRecognizer *)recognizer
{
    
    if (_priceView.frame.origin.x == self.view.frame.size.width -30) {
        _dotImage.hidden = YES;
        _priceView.frame = CGRectMake(self.view.frame.size.width -97, 10, 97, 43);
       // _priceLbl.frame=CGRectMake(10, 0, _priceView.frame.size.width-10, _priceView.frame.size.height);
        //_priceLbl.textAlignment = NSTextAlignmentCenter;
       // [self getItemPrice];
        
        [UIView animateWithDuration:1
                         animations:^{
            [self.view layoutIfNeeded];
        }];
    }else{
        _dotImage.hidden = NO;
        _priceView.frame = CGRectMake(self.view.frame.size.width -30, 10, 97, 43);
        _priceLbl.textAlignment = NSTextAlignmentCenter;
        
        
        [UIView animateWithDuration:1
                         animations:^{
            [self.view layoutIfNeeded];
        }];
    }
    
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if (!isMenu) {
        
        
    }
    else{
        isMenu=NO;
    }
    int count=[[[NSUserDefaults standardUserDefaults] valueForKey:CART_COUNT] intValue];
    if (count>9) {
        _cartCount.text=@"9+";
    }
    else{
        _cartCount.text=[NSString stringWithFormat:@"%d",[[[NSUserDefaults standardUserDefaults] valueForKey:CART_COUNT] intValue]];
    }
    _cartCount.hidden=[_cartCount.text isEqualToString:@"0"];
    
}

-(void)didReceiveMemoryWarning{
    
}

-(void)swipeleft:(UISwipeGestureRecognizer*)gestureRecognizer
{
    if ([gestureRecognizer locationInView:self.view].x>self.view.bounds.size.width*0.80) {
       
        isMenu=YES;
        [Global showSideMenu];
    }
}
-(void)didFinishCart{
    int count=[[[NSUserDefaults standardUserDefaults] valueForKey:CART_COUNT] intValue];
    if (count>9) {
        _cartCount.text=@"9+";
    }
    else{
        _cartCount.text=[NSString stringWithFormat:@"%d",[[[NSUserDefaults standardUserDefaults] valueForKey:CART_COUNT] intValue]];
    }
    _cartCount.hidden=[_cartCount.text isEqualToString:@"0"];
}

-(void)getItemPrice{
    
    _priceLbl.text=@"";
    [_activity startAnimating];
    _activity.hidden=false;
    _nxtBtn.backgroundColor=[UIColor lightGrayColor];
    _nxtBtn.enabled=NO;
    
    if (isFirstPrice) {
        [self callPriceApi];
        isFirstPrice=false;
        return;
    }
    
    timeStamp = [[NSDate date] timeIntervalSince1970];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:self->timeStamp];
        
        
        NSTimeInterval difference = [[NSDate date] timeIntervalSinceDate:date];
        
        
        if (difference<1 ){
            NSLog(@"Halt %f",difference);
            return;
        }
        else{
            [self callPriceApi];
        }
    });
    
    
    
    
    
    
}

-(void)callPriceApi{
    _priceLbl.text=@"";
    [_activity startAnimating];
    _activity.hidden=false;
    [self getPrice];
}

-(void)getPrice{
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    NSString *urlString=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,GET_PRICE];
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
    dic[@"url"]=urlString;
    
    NSMutableArray *arr=[[NSMutableArray alloc] init];
    arr=[db getDataForField:USERTABLE where:nil];
    
    
    
    if (arr.count!=0) {
        dic[@"token"]=arr[0][@"token"];
        
       
        
       
            dic[@"type"]=@"product";
            dic[@"product_type"]=productDic[@"sku"];
            dic[@"quantity"]=@"1";
        
        NSMutableDictionary * dataDic = [[NSMutableDictionary alloc] init];
        dataDic [@"width"] =@([productDic[@"width_tf"] floatValue]) ;
        dataDic [@"height"] =@([productDic[@"height_tf"] floatValue]) ;
        dataDic[@"printing"] = @YES;
        if (_galaryBtn.isSelected) {
            dataDic[@"stretching"] = @YES;
            dataDic[@"edge"] = @"WHITE";
            dataDic[@"color"] = @"";
            }
        else  if (_mirrorBtn.isSelected) {
            dataDic[@"stretching"] = @YES;
            dataDic[@"edge"] = @"MIRROR";
            dataDic[@"color"] = @"";
            }
        else{
            dataDic[@"stretching"] = @YES;
            dataDic[@"edge"] = @"BLACK";
            dataDic[@"color"] = colorStr;
        }
            
        dic[@"data"] = dataDic;
        
  
        [api sendPostRequst:dic];
    }
    
}

-(void)genrateSlideViewImage{
    
    dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(q, ^{
        
        UIImage *img =  [Global getImageFromDoucumentWithName:@"frame"];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if(img!=nil){
                self->mainImage = img;
                self.previewImageView.image =[Global getMidiumSizeImage:img imageSize:[UIScreen mainScreen].bounds.size.height*self->_scrollView.maximumZoomScale];
                self.previewImageView.backgroundColor = [UIColor clearColor];
                [self->_loaderActivity stopAnimating];
                self->_loaderActivity.hidden = YES;
                [Global stopShimmeringToView:self.previewImageView];
                self->isLoaded = true;
                [self.coreCollection reloadData];
                [self setUpView];
            }
            
            
            // [Global addshadow:self->_previewImageView radious:6];
            
            
        });
    });
}



-(void)reloadPreviewWherePreviewImage:(UIImage *)image {
    
}



-(void)setUpView{
    
    // _scrollView.frame = CGRectMake(30, 30, _topView.frame.size.width-60, _topView.frame.size.height-60);
    
    DbManager *db = [[DbManager alloc] init];
    NSMutableArray *arr=[db getDataForField:USER_PRODUCT where:nil];
    float imageWidth=0;
    float imageHeight =0;
    
    
    
    
    if (arr.count!=0) {
        widthMargin = [arr[0][@"width_tf"] floatValue ];
        heightMargine =[arr[0][@"height_tf"] floatValue ];
        widthMargin = (TOP_IMAGE_CUT_MARGINE/widthMargin)*100;
        heightMargine = (TOP_IMAGE_CUT_MARGINE/heightMargine)*100;
        imageWidth = mainImage.size.width /*-(leftRightImageWidth*2)*/;
        imageHeight = mainImage.size.height /*- (upBottomImageHeight*2)*/;
    }
    
    float ratio = imageWidth/imageHeight;
    float scrollViewHeight,scrollViewWidth;
    
    _previewImageView.image = [Global getMidiumSizeImage:mainImage imageSize:[UIScreen mainScreen].bounds.size.height*self->_scrollView.maximumZoomScale];
    
    if ([arr[0][@"width_tf"] floatValue ]>[arr[0][@"height_tf"] floatValue ]) {
        
        // scrollViewWidth = (leftImageWidthPer+rightImageWidthPer)*_topView.frame.size.width/100;
        scrollViewWidth = _topView.frame.size.width -TOP_IMAGEVIEW_MARGINE;
        
        
        scrollViewHeight = scrollViewWidth/ratio;
        
        
        
        
    }
    else{
        // scrollViewHeight = (upImageHeightPer+bottomImageHeightPer)*_topView.frame.size.height/100;
        scrollViewHeight = _topView.frame.size.height - TOP_IMAGEVIEW_MARGINE;
        scrollViewWidth = scrollViewHeight*ratio;
    }
    
    
    
    _scrollView.frame = CGRectMake((self.topView.frame.size.width - scrollViewWidth)/2,(self.topView.frame.size.height - scrollViewHeight)/2, scrollViewWidth, scrollViewHeight);
    _previewImageView.frame = CGRectMake(0, 0, scrollViewWidth, scrollViewHeight);
    
    leftRightImageWidth =_previewImageView.frame.size.width *widthMargin/100;
    
    upBottomImageHeight =_previewImageView.frame.size.height *heightMargine/100;
    
   
   
    
    
    _cropView.frame = _scrollView.frame;
    _cropView.userInteractionEnabled = false;
    
    
    
    if (_galaryBtn.isSelected)
        
    {
        [_scrollView setZoomScale:1];
        
        
        
        _previewImageView.frame = CGRectMake(0, 0, _scrollView.frame.size.width, _scrollView.frame.size.height);
        
        CGRect lineFrom;
        lineFrom.origin.x=_scrollView.frame.origin.x+ leftRightImageWidth;
        lineFrom.origin.y= _scrollView.frame.origin.y+upBottomImageHeight;
        
        CGRect lineTo;
        lineTo.origin.x= _scrollView.frame.origin.x+scrollViewWidth-leftRightImageWidth;
        lineTo.origin.y=_scrollView.frame.origin.y+upBottomImageHeight;
        //
        [self drawLineFromPoint:lineFrom toPoint:lineTo whereLineWidth:2 andLineColour:[UIColor whiteColor] atBaseView:_topView];
        
        
        lineFrom.origin.x=_scrollView.frame.origin.x+ leftRightImageWidth;
        lineFrom.origin.y= _scrollView.frame.origin.y+upBottomImageHeight;;
        
        
        lineTo.origin.x =_scrollView.frame.origin.x+ leftRightImageWidth;
        lineTo.origin.y=_scrollView.frame.origin.y+_scrollView.frame.size.height -upBottomImageHeight;
        //
        [self drawLineFromPoint:lineFrom toPoint:lineTo whereLineWidth:2 andLineColour:[UIColor whiteColor] atBaseView:_topView];
        
        
        lineFrom.origin.x=_scrollView.frame.origin.x+ leftRightImageWidth;
        lineFrom.origin.y= _scrollView.frame.origin.y+_scrollView.frame.size.height -upBottomImageHeight;
        
        
        lineTo.origin.x =_scrollView.frame.origin.x+scrollViewWidth - leftRightImageWidth;
        lineTo.origin.y=_scrollView.frame.origin.y+_scrollView.frame.size.height -upBottomImageHeight;
        //
        [self drawLineFromPoint:lineFrom toPoint:lineTo whereLineWidth:2 andLineColour:[UIColor whiteColor] atBaseView:_topView];
        
        
        lineFrom.origin.x=_scrollView.frame.origin.x+scrollViewWidth-leftRightImageWidth;
        lineFrom.origin.y= _scrollView.frame.origin.y+_scrollView.frame.size.height -upBottomImageHeight;
        
        
        lineTo.origin.x =_scrollView.frame.origin.x+scrollViewWidth-leftRightImageWidth;
        lineTo.origin.y=_scrollView.frame.origin.y+upBottomImageHeight;
        
        [self drawLineFromPoint:lineFrom toPoint:lineTo whereLineWidth:2 andLineColour:[UIColor whiteColor] atBaseView:_topView];
        
        leftUpCorner.hidden = NO;
        leftDownCorner.hidden = NO;
        rightUpCorner.hidden = NO;
        rightDownCorner.hidden = NO;
        _upImage.hidden = true;
        _bottomImage.hidden = true;
        _leftImageView.hidden = true;
        _rightImage.hidden = true;
        
        
        leftUpCorner.frame = CGRectMake(_scrollView.frame.origin.x, _scrollView.frame.origin.y, leftRightImageWidth, leftRightImageWidth);
        leftUpCorner.backgroundColor = _topView.backgroundColor;
        [_topView addSubview:leftUpCorner];
        
        
        leftDownCorner.frame = CGRectMake(_scrollView.frame.origin.x, _scrollView.frame.origin.y+scrollViewHeight-upBottomImageHeight, leftRightImageWidth, leftRightImageWidth);
        leftDownCorner.backgroundColor = _topView.backgroundColor;
        [_topView addSubview:leftDownCorner];
        
        
        rightUpCorner.frame = CGRectMake(_scrollView.frame.origin.x+scrollViewWidth-leftRightImageWidth, _scrollView.frame.origin.y, leftRightImageWidth, leftRightImageWidth);
        rightUpCorner.backgroundColor = _topView.backgroundColor;
        [_topView addSubview:rightUpCorner];
        
        
        rightDownCorner.frame = CGRectMake(_scrollView.frame.origin.x+scrollViewWidth-leftRightImageWidth, _scrollView.frame.origin.y+scrollViewHeight-upBottomImageHeight, leftRightImageWidth, leftRightImageWidth);
        rightDownCorner.backgroundColor = _topView.backgroundColor;
        [_topView addSubview:rightDownCorner];
        
       // temView.frame = _cropView.frame;
       
        temView.frame = CGRectMake(_cropView.frame.origin.x + leftUpCorner.frame.size.width, _cropView.frame.origin.y + leftUpCorner.frame.size.height, _cropView.frame.size.width - (leftUpCorner.frame.size.width*2), _cropView.frame.size.height - (leftUpCorner.frame.size.height*2));
        
        
       
       float temWidth = temView.frame.size.width;
        temWidth = temWidth+20;
        
        temView.frame = CGRectMake(temView.frame.origin.x-20, temView.frame.origin.y, temWidth, temView.frame.size.height);
       // temView.center = _topView.center;
        
       // temView.backgroundColor = [UIColor redColor];
        
        
        
        
       // temView.backgroundColor = [UIColor redColor];
        temView.userInteractionEnabled = false;
        [_topView addSubview:temView] ;
      
        
        
        //[_coreCollection reloadData];
        [self getItemPrice];
       // [self performSelector:@selector(showPreview) withObject:self afterDelay:0.1 ];
        [_coreCollection reloadData];
        return;
    }
    
    
    
    
        _scrollView.zoomScale = 1;
        [path removeAllPoints];
        [shapeLayer removeFromSuperlayer];
        scrollViewWidth = scrollViewWidth - (leftRightImageWidth*2);
        scrollViewHeight = scrollViewWidth/ratio;
        
        _scrollView.frame = CGRectMake((self.topView.frame.size.width - scrollViewWidth)/2,(self.topView.frame.size.height - scrollViewHeight)/2, scrollViewWidth, scrollViewHeight);
        _scrollView.contentSize = CGSizeMake(0, 0);
        _previewImageView.frame = CGRectMake(0, 0, scrollViewWidth, scrollViewHeight);
        
        
        _upImage.image = nil;
        _bottomImage.image = nil;
        _leftImageView.image = nil;
        _rightImage.image = nil;
        
        
        _upImage.hidden = false;
        _bottomImage.hidden = NO;
        _leftImageView.hidden = NO;
        _rightImage.hidden = NO;
        _upImage.transform = CGAffineTransformMakeScale(1, -1);
        _bottomImage.transform = CGAffineTransformMakeScale(1, -1);
        _leftImageView.transform = CGAffineTransformMakeScale(-1, 1);
        _rightImage.transform = CGAffineTransformMakeScale(-1, 1);
        _cropView.frame = _scrollView.frame;
        leftUpCorner.hidden = YES;
        leftDownCorner.hidden = YES;
        rightUpCorner.hidden = YES;
        rightDownCorner.hidden = YES;
    temView.frame = _cropView.frame;
   // temView.backgroundColor = [UIColor redColor];
    temView.userInteractionEnabled = false;
    [_topView addSubview:temView] ;
        
    if (_mirrorBtn.isSelected) {
            [self setMirrorViews];
        [self getItemPrice];
        }
        else{
            [self setUpColorWrap];
        }
        
        
   
    
    
    
    
    //NSLog(@" %.2f Mb",(float)[UIImageJPEGRepresentation(previewIcon, 1) length]/1024/1024);
    [_coreCollection reloadData];
    
  //  [self performSelector:@selector(showPreview) withObject:self afterDelay:0.1 ];
    
    
    
    
    
}

-(void)setUpColorWrap{
    float tempWidth = _scrollView.frame.size.width*widthMargin/100;
    float tempHeight = _scrollView.frame.size.height;
    
    _leftImageView.frame = CGRectMake(_scrollView.frame.origin.x-tempWidth, _scrollView.frame.origin.y, tempWidth, tempHeight);
   
    
    _rightImage.frame = CGRectMake(_scrollView.frame.size.width+_scrollView.frame.origin.x, _scrollView.frame.origin.y, tempWidth, tempHeight);
    
    
    tempWidth = _scrollView.frame.size.width;
    tempHeight = _scrollView.frame.size.height *heightMargine/100;
    
    _upImage.frame = CGRectMake(_scrollView.frame.origin.x, _scrollView.frame.origin.y-tempHeight, tempWidth, tempHeight);
   
    
    _bottomImage.frame = CGRectMake(_scrollView.frame.origin.x, _scrollView.frame.size.height+_scrollView.frame.origin.y, tempWidth, tempHeight);
    if (colorStr !=nil) {
        [self setColorToImageViews:[Global colorFromHexString:colorStr]];
    }
   
    
}


-(void)setColorToImageViews:(UIColor *)bgColor{
    _leftImageView.backgroundColor = bgColor;
    _rightImage.backgroundColor =  bgColor;
    _upImage.backgroundColor =  bgColor;
    _bottomImage.backgroundColor = bgColor;
}





- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
    
    
   productPreviewImage.image =[Global getMidiumSizeImage:[self getPreviewImage] imageSize:[UIScreen mainScreen].bounds.size.width];
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
   productPreviewImage.image =[Global getMidiumSizeImage:[self getPreviewImage] imageSize:[UIScreen mainScreen].bounds.size.width];
}
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    //[self showPreview];
    
    if (_mirrorBtn.selected) {
        [self setMirrorViews];
    }
  //  productPreviewImage.image =[self getPreviewImage];
  //  if (_priceView.frame.origin.x == self.view.frame.size.width -97)

   // [self handleSingleTaponPriceLabel:nil];
    
}

- (void)scrollViewWillBeginZooming:(UIScrollView *)scrollView withView:(nullable UIView *)view{
    if (_priceView.frame.origin.x == self.view.frame.size.width -97)
    //
        [self handleSingleTaponPriceLabel:nil];
}

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(nullable UIView *)view atScale:(CGFloat)scale{
    
    if (_priceView.frame.origin.x == self.view.frame.size.width -30)
    
    [self handleSingleTaponPriceLabel:nil];
    
    
    
    
  
    
    
    
 
    
    
//    superCropRect = [_cropView convertRect:_cropView.bounds toView:_scrollView];
//    cropRect = CGRectMake( superCropRect.origin.x - [self realImageRect].origin.x,
//                          superCropRect.origin.y - [self realImageRect].origin.y,
//                          superCropRect.size.width ,
//                          superCropRect.size.height );
    
  //  productPreviewImage.image =[Global getMidiumSizeImage:[self getPreviewImage] imageSize:[UIScreen mainScreen].bounds.size.width/2];
    
    
    
}


-(UIImage *)getPreviewImage{
 CGRect rect2 = [temView convertRect:temView.bounds toView:_scrollView];
    CGRect cropRectT =  CGRectMake( rect2.origin.x - [self realImageRect].origin.x,
                                   rect2.origin.y - [self realImageRect].origin.y,
                                   rect2.size.width ,
                                   rect2.size.height );
  
    if (_colorBtn.isSelected) {
        return  [Global convertImageTo3dWhereBacking:CORE_BLACK andImageView:productPreviewImage andImage:[self cropImage:mainImage toRect:cropRectT imageViewWidth:_previewImageView.frame.size.width imageViewHeight:_previewImageView.frame.size.height] withGradient:NO andCoreColor:[Global colorFromHexString:colorStr] sku:productDic[@"sku"]  andWidth:[self->productDic[@"width_tf"] floatValue]];
    }
 
    return  [Global convertImageTo3dWhereBacking:(_galaryBtn.selected)?CORE_WHITE:CORE_MIRROR andImageView:productPreviewImage andImage:[self cropImage:mainImage toRect:cropRectT imageViewWidth:_previewImageView.frame.size.width imageViewHeight:_previewImageView.frame.size.height] withGradient:NO andCoreColor:[UIColor clearColor] sku:productDic[@"sku"] andWidth:[self->productDic[@"width_tf"] floatValue]];;
}

-(UIImage *)takeScreenShotOfRect:(CGRect)rect andView: (UIView *)captureView{
    
    UIGraphicsBeginImageContextWithOptions(rect.size,YES,0.0f);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [captureView.layer renderInContext:context];
    UIImage *capturedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return capturedImage;
}

- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}



- (UIImage *)imageByScalingProportionallyToSize:(CGSize)targetSize withOriginalImage:(UIImage *)origin {
    
    UIImage *sourceImage = origin;
    UIImage *newImage = nil;
    
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    
    CGFloat targetWidth = targetSize.width;
    CGFloat targetHeight = targetSize.height;
    
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    
    CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
    
    if (CGSizeEqualToSize(imageSize, targetSize) == NO) {
        
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        
        if (widthFactor < heightFactor)
            scaleFactor = widthFactor;
        else
            scaleFactor = heightFactor;
        
        scaledWidth  = width * scaleFactor;
        scaledHeight = height * scaleFactor;
        
        // center the image
        
        if (widthFactor < heightFactor) {
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
        } else if (widthFactor > heightFactor) {
            thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
        }
    }
    
    
    // this is actually the interesting part:
    
    UIGraphicsBeginImageContext(targetSize);
    
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
    thumbnailRect.size.width  = scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    
    [sourceImage drawInRect:thumbnailRect];
    
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    if(newImage == nil) NSLog(@"could not scale image");
    
    return newImage ;
}


-(UIImage *)getCroopedImage{
    croppedImage = nil;
    
    
    
    
    superCropRect = [_cropView convertRect:_cropView.bounds toView:_scrollView];
    cropRect = CGRectMake( superCropRect.origin.x - [self realImageRect].origin.x,
                          superCropRect.origin.y - [self realImageRect].origin.y,
                          superCropRect.size.width,
                          superCropRect.size.height);
    
    
    return  [self cropImage:mainImage toRect:cropRect imageViewWidth:_previewImageView.frame.size.width imageViewHeight:_previewImageView.frame.size.height];
    
}


-(void)setMirrorViews{
    
    
    croppedImage=  [self getCroopedImage];
    
    self->_upImage.image =   [UIImage imageWithData:UIImageJPEGRepresentation([self imageByCropping:self->croppedImage toRect:CGRectMake(0, 0, self->croppedImage.size.width, self->croppedImage.size.height*self->heightMargine/100)], 0.1)]  ;
    
    
    
    
    float temWidth = self->_scrollView.frame.size.width;
    float temRatio = self->_upImage.image.size.width/self->_upImage.image.size.height;
    float temHeight = temWidth/temRatio;
    self->_upImage.frame = CGRectMake(self->_scrollView.frame.origin.x, self->_scrollView.frame.origin.y-temHeight-1, temWidth, temHeight);
    
    
    self->_bottomImage.image =   [UIImage imageWithData:UIImageJPEGRepresentation([self imageByCropping:self->croppedImage toRect:CGRectMake(0, self->croppedImage.size.height - self->croppedImage.size.height*self->heightMargine/100, self->croppedImage.size.width, self->croppedImage.size.height*self->heightMargine/100)], 0.1)]  ;
    
    temWidth = self->_scrollView.frame.size.width;
    temRatio = self->_bottomImage.image.size.width/self->_bottomImage.image.size.height;
    temHeight = temWidth/temRatio;
    self->_bottomImage.frame = CGRectMake(self->_scrollView.frame.origin.x, self->_scrollView.frame.origin.y+self->_scrollView.frame.size.height+1, temWidth, temHeight);
    
    _leftImageView.image =  [UIImage imageWithData:UIImageJPEGRepresentation( [self imageByCropping:croppedImage toRect:CGRectMake(0, 0 , croppedImage.size.width*widthMargin/100, croppedImage.size.height)], 0.1)]  ;
    
    temHeight = _scrollView.frame.size.height;
    temRatio = _leftImageView.image.size.width/_leftImageView.image.size.height;
    
    
    temWidth = temHeight *temRatio;
    
    _leftImageView.frame = CGRectMake(_scrollView.frame.origin.x-temWidth-1, _scrollView.frame.origin.y, temWidth, temHeight);
    [_topView addSubview:_leftImageView];
    
    _rightImage.image = [UIImage imageWithData:UIImageJPEGRepresentation( [self imageByCropping:croppedImage toRect:CGRectMake(croppedImage.size.width - croppedImage.size.width*widthMargin/100, 0 , croppedImage.size.width*widthMargin/100, croppedImage.size.height)], 0.1)] ;
    
    
    temHeight = _scrollView.frame.size.height;
    temRatio = _rightImage.image.size.width/_rightImage.image.size.height;
    temWidth = temHeight *temRatio;
    
    _rightImage.frame = CGRectMake(_scrollView.frame.origin.x+_scrollView.frame.size.width+1, _scrollView.frame.origin.y, temWidth, temHeight);
    
    
    
    
}



-(void)drawLineFromPoint:(CGRect)fromPint toPoint:(CGRect)toPoint whereLineWidth:(float)lineWidth andLineColour:(UIColor *)lineColor atBaseView:(UIView *)baseView{
    
    
    [path moveToPoint:CGPointMake(fromPint.origin.x, fromPint.origin.y)];
    [path addLineToPoint:CGPointMake(toPoint.origin.x, toPoint.origin.y)];
    
    
    shapeLayer.path = [path CGPath];
    shapeLayer.strokeColor = [lineColor CGColor];
    shapeLayer.lineWidth = lineWidth;
    shapeLayer.fillColor = [[UIColor clearColor] CGColor];
    [baseView.layer addSublayer:shapeLayer];
}



- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    
    return _previewImageView;
}

-(UIImage *)cropImage:(UIImage *)inputImage toRect:(CGRect) cropRect imageViewWidth:(float)imageViewWidth imageViewHeight:(float)imageViewHeight {
    
    float imageViewScale = MAX(inputImage.size.width / imageViewWidth,
                               inputImage.size.height / imageViewHeight);
    CGRect cropZone = CGRectMake(cropRect.origin.x * imageViewScale, cropRect.origin.y * imageViewScale, cropRect.size.width * imageViewScale, cropRect.size.height * imageViewScale);
    
    UIImage *img = [self imageByCropping:mainImage toRect:cropZone];
    
    
    
    return  img;
}
-(CGRect)realImageRect{
    CGSize imageViewSize = _previewImageView.frame.size;
    CGSize  imageSize = mainImage.size;
    float scaleWidth = imageViewSize.width / imageSize.width;
    float scaleHeight = imageViewSize.height / imageSize.height;
    float aspect = MIN(scaleWidth, scaleHeight);
    
    CGRect imageRect = CGRectMake(0, 0, imageSize.width * aspect, imageSize.height * aspect);
    
    imageRect.origin.x = (imageViewSize.width - imageRect.size.width) / 2 ;
    imageRect.origin.y = (imageViewSize.height - imageRect.size.height) / 2 ;
    
    // Add imageView offset
    imageRect.origin.x += _previewImageView.frame.origin.x;
    imageRect.origin.y += _previewImageView.frame.origin.y;
    
    return imageRect;
    
}

- (UIImage*)imageByCropping:(UIImage *)imageToCrop toRect:(CGRect)rect
{
    CGImageRef imageRef = CGImageCreateWithImageInRect([imageToCrop CGImage], rect);
    UIImage *cropped = [UIImage imageWithCGImage:imageRef];
    return cropped;
    
    
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return isLoaded? 1:0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (_colorBtn.selected) {
       
        static NSString *identifier = @"ColorWrapCollectionViewCell";
        
        ColorWrapCollectionViewCell *cells =[_coreCollection dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        
        cells.delegate = self;
        
        
        
        return cells;
    }
    else{
        static NSString *identifier = @"GalleryWrapCollectionViewCell";
        
        NSString * temStr = CORE_MIRROR;
        if (_galaryBtn.selected) {
           
            temStr = CORE_WHITE;
        }
       
      
        
        GalleryWrapCollectionViewCell *cells =[_coreCollection dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        
        
        cells.previewIcon.hidden=YES;
        cells.activityLoader.hidden = YES;
        ;
        cells.detail.hidden = YES;
        productPreviewImage = cells.previewIcon;
        cells.previewIcon.image = [UIImage imageWithData:  UIImageJPEGRepresentation( [self imageWithImage:[self getCroopedImage] convertToSize:CGSizeMake(_previewImageView.frame.size.width*2, _previewImageView.frame.size.height*2)], 0.1)];
        // cells.previewIcon.image = [Global convertImageTo3dWhereBacking:CORE_MIRROR andImageView:cells.previewIcon andImage:cells.previewIcon.image]  ;
        
       
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            cells.previewIcon.image =[Global convertImageTo3dWhereBacking:temStr andImageView:cells.previewIcon andImage:cells.previewIcon.image withGradient:NO andCoreColor:[UIColor clearColor] sku:self->productDic[@"sku"] andWidth:[self->productDic[@"width_tf"] floatValue]];
            
            cells.previewIcon.hidden = NO;
          
           
            if (!self->_galaryBtn.isSelected) {
                cells.detail.text = @"Your image will be cloned/mirrored along the edges of the canvas, this is great for landscapes or scenery shots to have it look like it continues over the edge.";
            }
            else{
                cells.detail.text = @"Your image will continue over the edge of the canvas, some of the content will be lost to the edge of the canvas but will create a flowing continuation of your image.";
            }
      
        
            [cells setUpView];
           
            // [Global addshadow:self->_previewImageView radious:6];
            
            
        });
     
        
     
        
    
        
        return cells;
    }
}

- (CGSize)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(_coreCollection.frame.size.width, _coreCollection.frame.size.height);
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
}



#pragma mark - Segue support

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    
    
    if ([[segue identifier] isEqualToString:@"frame"]) {
        
        FrameSettingViewController *albumContentsViewController = [segue destinationViewController];
        albumContentsViewController.cameFrom = @"canvas";
        
    }
}

//- (IBAction)frameAction:(id)sender {
//    superCropRect = [_cropView convertRect:_cropView.bounds toView:_scrollView];
//    cropRect = CGRectMake( superCropRect.origin.x - [self realImageRect].origin.x,
//                          superCropRect.origin.y - [self realImageRect].origin.y,
//                          superCropRect.size.width,
//                          superCropRect.size.height);
//
//
//    croppedImage=  [self cropImage:_previewImageView.image toRect:cropRect imageViewWidth:_previewImageView.frame.size.width imageViewHeight:_previewImageView.frame.size.height];
//
//
//}

-(void)deselectAllBtn{
    _colorBtn.backgroundColor = unselectColor;
    
    _galaryBtn.backgroundColor = unselectColor;
    _mirrorBtn.backgroundColor = unselectColor;
    _colorBtn.selected = false;
    _galaryBtn.selected = false;
    _mirrorBtn.selected = false;
    
}

#pragma mark - ColorWrap Delegate
-(void)didChooseColor:(NSMutableDictionary *)colorDic{
    colorStr =colorDic[@"hex"];
    [self setColorToImageViews:[Global colorFromHexString:colorDic[@"hex"]]];
   // [self getItemPrice];
}


- (IBAction)colorAction:(id)sender {
    [self deselectAllBtn];
    _colorBtn.selected = true;
    _colorBtn.backgroundColor = selectColor;
    [self setUpView];
}

- (IBAction)mirrorAction:(id)sender {
    [self deselectAllBtn];
    _mirrorBtn.selected = true;
    _mirrorBtn.backgroundColor = selectColor;
    [self setUpView];
}

- (IBAction)galaryAction:(id)sender {
    [self deselectAllBtn];
    _galaryBtn.selected = true;
    _galaryBtn.backgroundColor = selectColor;
    [self setUpView];
    
}

- (IBAction)nextAction:(id)sender {
    
    superCropRect = [_cropView convertRect:_cropView.bounds toView:_scrollView];
    cropRect = CGRectMake( superCropRect.origin.x - [self realImageRect].origin.x,
                          superCropRect.origin.y - [self realImageRect].origin.y,
                          superCropRect.size.width,
                          superCropRect.size.height);
    
    
    croppedImage=  [self cropImage:mainImage toRect:cropRect imageViewWidth:_previewImageView.frame.size.width imageViewHeight:_previewImageView.frame.size.height];
    
    _nextBtnLoader.hidden = false;
    [_nextBtnLoader startAnimating];
    
    
    _nxtBtn.enabled=NO;
    [_nxtBtn setTitle:@"" forState:UIControlStateNormal];
    
    DbManager *db = [[DbManager alloc] init];
    NSMutableArray *arr=[db getDataForField:USER_PRODUCT where:nil];
    
    if (arr.count!=0) {
        
        
        if (_galaryBtn.isSelected) {
            arr[0][@"core"]=@"WHITE";
        }
        else if (_mirrorBtn.isSelected){
            arr[0][@"core"]=@"MIRROR";
        }
        else{
            arr[0][@"core"]=@"BLACK";
        }
        arr[0][@"color"]= colorStr;

        
        [db delTableWithTableName:USER_PRODUCT];
        [db insertIntoTable:arr[0] table_name:USER_PRODUCT];
        
    }
    productDic[@"color"] = colorStr;
    UIImage * temImage = [self getPreviewImage];
    
    dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(q, ^{
        //[Global startShimmeringToView:cell.contentView];
     

        [Global saveImagesInDocumentDirectory:UIImageJPEGRepresentation(self->croppedImage, 1.0) ImageId:@"frame"];
        
       
        
        [Global saveImagesInDocumentDirectory:UIImagePNGRepresentation(temImage) ImageId:@"preview"];

     
        dispatch_async(dispatch_get_main_queue(), ^{
            self->_nextBtnLoader.hidden = true;
            [self->_nextBtnLoader stopAnimating];
//            NSLog(@"%@",[arr[0][@"sku"]);
           //{
            [self performSegueWithIdentifier:@"summary" sender:nil];
           // }
            [self->_nxtBtn setTitle:@"Next: Summary" forState:UIControlStateNormal];
            self->_nxtBtn.enabled=YES;

        });
    });
    
   
    
}


- (IBAction)cartBtn:(id)sender{
   
    [Global callMenuViewWithCode:@"0"];
}
- (IBAction)menuBtn:(id)sender{
   
    isMenu=YES;
    [Global showSideMenu];
}

#pragma mark -Pop api Delegates

-(void)connectionError{
   
   
}
-(void)requestFailedSessionExpired:(NSDictionary *)dataPack{
   
}
-(void)requestFailedServerError:(NSDictionary *)dataPack{
   
}
-(void)requestFailedServiceUnavailableDueToMaintenance:(NSDictionary *)dataPack{
    
}
-(void)requestFailedMethodNotFound:(NSDictionary *)dataPack{
   
}
-(void)requestFailedNotFound:(NSDictionary *)dataPack{
    
}
-(void)requestFailedUnauthorised:(NSDictionary *)dataPack{
    
}
-(void)requestFailedBadRequest:(NSDictionary *)dataPack{
  
}
-(void)requestSucceededWithData:(NSDictionary *)dataPack{
    
    
    if ([dataPack[@"url"] isEqualToString:LOGIN_USER]) {
        
        [db delTableWithTableName:USERTABLE];
        NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
        
        dic[@"id"]              =[NSString stringWithFormat:@"%@",dataPack[@"data"][@"id"]];
        dic[@"name"]            =dataPack[@"data"][@"name"];
        dic[@"email"]           =dataPack[@"data"][@"email"];
        dic[@"created_at"]      =dataPack[@"data"][@"created_at"];
        dic[@"updated_at"]      =dataPack[@"data"][@"updated_at"];
        dic[@"token"]           =dataPack[@"data"][@"access_token"];
        
        [db insertIntoTable:dic table_name:USERTABLE];
        [self getItemPrice];
    }
    
    else if ([dataPack[@"url"] isEqualToString:REGISTER_USER]){
        
        NSMutableDictionary *dic =[[NSMutableDictionary alloc] init];
        dic[@"token"]            =dataPack[@"data"][@"token"];
        dic[@"id"]               =[NSString stringWithFormat:@"%@",dic[@"id"]];
        
        [db insertIntoTable:dic table_name:USERTABLE];
        [self getItemPrice];
    }
    else  if ([dataPack[@"url"] isEqualToString:GET_PRICE]){
     
        [_activity stopAnimating];
        _activity.hidden = YES;
        _nxtBtn.backgroundColor=APP_BLUE_COLOR;
        _nxtBtn.enabled=true;
       
        _priceLbl.text=[NSString stringWithFormat:@"$%0.2f",[[NSString stringWithFormat:@"%@",dataPack[@"data"][@"price"]] floatValue]] ;
        NSMutableArray*    tempArr=[db getDataForField:USER_PRODUCT where:nil];
        
        tempArr[0][@"shipping_price"] =[NSString stringWithFormat:@"%.2f",[dataPack[@"data"][@"shipping_charges"] floatValue]];
        tempArr[0][@"price"] =[NSString stringWithFormat:@"%.2f",[dataPack[@"data"][@"price"] floatValue]];
        [db update:USER_PRODUCT :tempArr[0] :[NSString stringWithFormat:@"id='%@'",tempArr[0][@"id"]]];
        
        
       
//        productPreviewImage.image =  [self cropImage:_previewImageView.image toRect:cropRect imageViewWidth:_previewImageView.frame.size.width imageViewHeight:_previewImageView.frame.size.height];
        
        
       
        
        
  
        
        
        
       
        
    }
    
    
    
    
    
    
}
-(void)requestSucceededButActionFailed:(NSDictionary *)dataPack{
   
}
-(void)uploadedData:(NSString *)pecentage andByte:(float)bytes{
    
}
-(void)requestSucceededButNoDataFound:(NSDictionary *)dataPack{
   
   
}
-(void)requestFailedDueToServerIsuueWithError:(NSString*)errorDes{
    
}
@end

