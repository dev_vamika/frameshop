//
//  GalleryWrapCollectionViewCell.m
//  FrameShop
//
//  Created by Saurabh Srivastav on 22/12/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import "GalleryWrapCollectionViewCell.h"

@implementation GalleryWrapCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _previewIcon.hidden = YES;
    _detail.hidden = YES;
    _activityLoader.hidden = YES;
    _detail.text = @"";
    // Initialization code
}
-(void)setUpView{
    
    float width = self.contentView.frame.size.width*0.50;
    float ratio = _previewIcon.image.size.height/_previewIcon.image.size.width;
    float height = width *ratio;
    
    if (_previewIcon.image.size.width<_previewIcon.image.size.height) {
        height = self.contentView.frame.size.height;
        width = height/ratio;
    }
   
    _previewIcon.frame = CGRectMake(0, (self.contentView.frame.size.height-height)/2, width, height);
    
    _detail.frame = CGRectMake(width+10, 10, self.contentView.frame.size.width-width-10, self.contentView.frame.size.height-20);
    _activityLoader.center = _previewIcon.center;
    _detail.hidden = NO;
    
    // _activityLoader.hidden = false;
}
@end
