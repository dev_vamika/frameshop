//
//  GalleryWrapCollectionViewCell.h
//  FrameShop
//
//  Created by Saurabh Srivastav on 22/12/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GalleryWrapCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *previewIcon;

@property (weak, nonatomic) IBOutlet UILabel *detail;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityLoader;
-(void)setUpView;
@end

NS_ASSUME_NONNULL_END
