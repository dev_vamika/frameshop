//
//  ColorWrapCollectionViewCell.m
//  FrameShop
//
//  Created by Saurabh Srivastav on 22/12/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import "ColorWrapCollectionViewCell.h"
#import "Global.h"
@implementation ColorWrapCollectionViewCell{
    NSIndexPath *selectIndexPath;
    NSMutableArray *dataArray;
    bool isUsingWheel;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [Global roundCorner:_colorView roundValue:5 borderWidth:1 borderColor:APP_BLUE_COLOR];
    [Global roundCorner:_pickerView roundValue:5 borderWidth:1 borderColor:APP_BLUE_COLOR];
    [Global roundCorner:_colorViewColor roundValue:_colorViewColor.frame.size.width/2 borderWidth:0.5 borderColor:APP_BLUE_COLOR];
    [Global roundCorner:_colorPickerColor roundValue:_colorViewColor.frame.size.width/2 borderWidth:0 borderColor:nil];
    [self.colorCollection registerNib:[UINib nibWithNibName:@"AlbumCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"AlbumCollectionViewCell"];
    _colorCollection.delegate = self;
    _colorCollection.dataSource = self;
    _colorWheel.continuous = true;
    _colorWheel.delegate = self;
    _colorBar.backgroundColor = APP_BLUE_COLOR;
   
    dataArray=[[NSMutableArray alloc] init];
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    dic[@"name"]= @"Lime";
    dic[@"hex"] = @"#000000";
    dataArray[dataArray.count] = [dic mutableCopy];
    dic[@"name"]= @"Green";
    dic[@"hex"] = @"#808080";
    dataArray[dataArray.count] = [dic mutableCopy];
    dic[@"name"]= @"Aqua";
    dic[@"hex"] = @"#C0C0C0";
    dataArray[dataArray.count] = [dic mutableCopy];
    dic[@"name"]= @"Teal";
    dic[@"hex"] = @"#FFFFFF";
    dataArray[dataArray.count] = [dic mutableCopy];
    dic[@"name"]= @"Orange";
    dic[@"hex"] = @"#FF0000";
    dataArray[dataArray.count] = [dic mutableCopy];
    dic[@"name"]= @"Gold";
    
    dic[@"name"]= @"0000FF";
    dic[@"hex"] = @"#663399";
    dataArray[dataArray.count] = [dic mutableCopy];
    dic[@"name"]= @"Chocolate";
    dic[@"hex"] = @"#008000";
    dataArray[dataArray.count] = [dic mutableCopy];
    dic[@"name"]= @"Lime";
    dic[@"hex"] = @"#FFFF00";
    dataArray[dataArray.count] = [dic mutableCopy];
    dic[@"name"]= @"Green";
    dic[@"hex"] = @"#FF6347";
    dataArray[dataArray.count] = [dic mutableCopy];
    dic[@"name"]= @"Aqua";
    dic[@"hex"] = @"#87CEEB";
    dataArray[dataArray.count] = [dic mutableCopy];
    dic[@"name"]= @"Teal";
    dic[@"hex"] = @"#7CFC00";
    dataArray[dataArray.count] = [dic mutableCopy];
    dic[@"name"]= @"Orange";
    dic[@"hex"] = @"#FFD700";
    dataArray[dataArray.count] = [dic mutableCopy];
    dic[@"name"]= @"Gold";
    dic[@"hex"] = @"#800080";
    dataArray[dataArray.count] = [dic mutableCopy];
    dic[@"name"]= @"RebeccaPurple";
    dic[@"hex"] = @"#00FFFF";
    dataArray[dataArray.count] = [dic mutableCopy];
    dic[@"name"]= @"Chocolate";
    dic[@"hex"] = @"#996600";
    dataArray[dataArray.count] = [dic mutableCopy];
    dic[@"name"]= @"Lime";
    dic[@"hex"] = @"#FF00FF";
    dataArray[dataArray.count] = [dic mutableCopy];
    dic[@"name"]= @"Green";
    dic[@"hex"] = @"#DA70D6";
    dataArray[dataArray.count] = [dic mutableCopy];
    dic[@"name"]= @"Aqua";
    dic[@"hex"] = @"#7FFFD4";
    dataArray[dataArray.count] = [dic mutableCopy];
    dic[@"name"]= @"Teal";
    dic[@"hex"] = @"#FA500";
    dataArray[dataArray.count] = [dic mutableCopy];
    dic[@"name"]= @"Orange";
    dic[@"hex"] = @"#808000";
    dataArray[dataArray.count] = [dic mutableCopy];
    _stackView.backgroundColor = APP_BLUE_COLOR;
   
    [Global roundCorner:_backView roundValue:10 borderWidth:1.0 borderColor:APP_BLUE_COLOR];
    [_colorCollection reloadData];
    
    // Initialization code
}

#pragma mark - UICollectionViewDataSource and Delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return dataArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"AlbumCollectionViewCell";
    
    AlbumCollectionViewCell *cells; //=[_colorCollectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
   
        cells = [_colorCollection dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        
  
    
    float cellWidth=(self.colorCollection.bounds.size.width/5)-5;
    float cellHeight=cellWidth;
    
     cells.frame=CGRectMake(cells.frame.origin.x, cells.frame.origin.y, cellWidth, cellHeight);
    
    cells.albumName.hidden=YES;
    cells.photoCount.hidden=YES;
    cells.iconImage.image=nil;
    cells.iconImage.hidden=YES;
    cells.contentView.frame=CGRectMake(0, 0, cellWidth, cellHeight);
    cells.contentView.backgroundColor=[UIColor groupTableViewBackgroundColor];
    cells.contentView.frame = CGRectMake(1, 1, cells.contentView.frame.size.width - 2, cells.contentView.frame.size.height - 2);
    
    
    [Global roundCorner:cells.contentView roundValue:cells.contentView.frame.size.height/2 borderWidth:0.5 borderColor:[UIColor lightGrayColor]];
    cells.contentView.backgroundColor = [Global colorFromHexString:dataArray[indexPath.row][@"hex"]];
    cells.iconImage.frame = CGRectMake(0, 0, cells.contentView.frame.size.width-10, cells.contentView.frame.size.height-10);
    cells.iconImage.center = cells.contentView.center;
    cells.iconImage.backgroundColor = [UIColor clearColor];
  
 
    [Global stopShimmeringToView:cells.iconImage];
    
    if (selectIndexPath == nil) {
        selectIndexPath = indexPath;
        cells.iconImage.hidden = false;
        cells.iconImage.image = [Global getCheckMarkWhereBgColor:dataArray[indexPath.row][@"hex"]];
        _colorViewColor.backgroundColor = [Global colorFromHexString:dataArray[indexPath.row][@"hex"]];
        _colorViewColorName.text =dataArray[indexPath.row][@"hex"];
        _colorPickerColor.backgroundColor =[Global colorFromHexString:dataArray[indexPath.row][@"hex"]];
        _colorPickerColorName.text =dataArray[indexPath.row][@"hex"];
        [self.delegate didChooseColor:dataArray[indexPath.row]];
    }
    else if(selectIndexPath == indexPath){
        if (isUsingWheel == false){
        cells.iconImage.hidden = false;
        cells.iconImage.image = [Global getCheckMarkWhereBgColor:dataArray[indexPath.row][@"hex"]];
        }
        [Global stopShimmeringToView:cells.iconImage];
    }
    return cells;
    
}


- (CGSize)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(self.colorCollection.bounds.size.width/5 - 5, self.colorCollection.bounds.size.width/5 - 5);
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    isUsingWheel = false;
    AlbumCollectionViewCell *cells = (AlbumCollectionViewCell*)[collectionView cellForItemAtIndexPath:selectIndexPath];
    cells.iconImage.image = nil;
    cells.iconImage.hidden = YES;
    [Global stopShimmeringToView:cells.iconImage];
    
 
    AlbumCollectionViewCell *cell = (AlbumCollectionViewCell*)[collectionView cellForItemAtIndexPath:indexPath];
    cell.iconImage.hidden = false;
    cell.iconImage.image = [Global getCheckMarkWhereBgColor:dataArray[indexPath.row][@"hex"]];
    _colorViewColor.backgroundColor = [Global colorFromHexString:dataArray[indexPath.row][@"hex"]];
    _colorViewColorName.text =dataArray[indexPath.row][@"hex"];
    selectIndexPath = indexPath;
    _colorPickerColorName . text =dataArray[indexPath.row][@"hex"];
    _colorPickerColor.backgroundColor =[Global colorFromHexString:dataArray[indexPath.row][@"hex"]];
    [Global stopShimmeringToView:cell.iconImage];
    [self.delegate didChooseColor:dataArray[indexPath.row]];
    [_colorCollection reloadData];
    
}

#pragma mark - ColorWheel Delegate
- (void)colorWheelDidChangeColor:(ISColorWheel *)colorWheel
{
    isUsingWheel = true;
    _colorPickerColor.backgroundColor = colorWheel.currentColor;
   //
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    dic[@"hex"] = [Global hexStringFromColor:colorWheel.currentColor];
    _colorPickerColorName.text =dic[@"hex"];
    [self.delegate didChooseColor:dic];
    _colorViewColor.backgroundColor = colorWheel.currentColor;
    _colorViewColorName . text =dic[@"hex"];
    
    AlbumCollectionViewCell *cells = (AlbumCollectionViewCell*)[_colorCollection cellForItemAtIndexPath:selectIndexPath];
  //  [ _colorCollection scrollToItemAtIndexPath:selectIndexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:true ];
    [_colorCollection reloadItemsAtIndexPaths:@[selectIndexPath]];
    cells.iconImage.image = nil;
    cells.iconImage.hidden = YES;
    [Global stopShimmeringToView:cells.iconImage];
    
    
}


@end
