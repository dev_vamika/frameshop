//
//  CoreCollectionViewCell.h
//  FrameShop
//
//  Created by Saurabh Srivastav on 04/12/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CoreCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *coreName;
@property (weak, nonatomic) IBOutlet UIImageView *coreImage;
@property (weak, nonatomic) IBOutlet UIView *borderView;

@end

NS_ASSUME_NONNULL_END
