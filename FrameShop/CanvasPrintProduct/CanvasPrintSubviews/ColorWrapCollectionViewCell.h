//
//  ColorWrapCollectionViewCell.h
//  FrameShop
//
//  Created by Saurabh Srivastav on 22/12/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlbumViewController.h"
#import "ISColorWheel.h"
NS_ASSUME_NONNULL_BEGIN
@class ColorWrapCollectionViewCell;
@protocol ColorWrapCollectionViewCellrDelegate <NSObject>
@optional
-(void)didChooseColor:(NSMutableDictionary *)colorDic;

@end
@interface ColorWrapCollectionViewCell : UICollectionViewCell<UICollectionViewDataSource,UICollectionViewDelegate,ISColorWheelDelegate>
@property (weak, nonatomic) IBOutlet UICollectionView *colorCollection;
@property (weak, nonatomic) IBOutlet UIStackView *stackView;
@property (weak, nonatomic) IBOutlet UIView *pickerView;
@property (weak, nonatomic) IBOutlet UIView *colorView;
@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UIView *colorBar;
@property (weak, nonatomic) IBOutlet ISColorWheel *colorWheel;
@property (weak, nonatomic) IBOutlet UILabel *colorViewColorName;
@property (weak, nonatomic) IBOutlet UIView *colorViewColor;
@property (weak, nonatomic) IBOutlet UILabel *colorPickerColorName;
@property (weak, nonatomic) IBOutlet UIView *colorPickerColor;
@property(strong,nonatomic)id<ColorWrapCollectionViewCellrDelegate> delegate;
@end

NS_ASSUME_NONNULL_END
