//
//  CanvasCoreViewController.h
//  FrameShop
//
//  Created by Saurabh Srivastav on 04/12/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreCollectionViewCell.h"
#import "FrameSettingViewController.h"
#import "GalleryWrapCollectionViewCell.h"
#import "ColorWrapCollectionViewCell.h"
#import "Global.h"
#import "DBManager.h"
#import "PopApi.h"
NS_ASSUME_NONNULL_BEGIN

@interface CanvasCoreViewController : UIViewController<UIScrollViewDelegate,ColorWrapCollectionViewCellrDelegate,PopApiDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *previewImageView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *nextBtnLoader;
@property (weak, nonatomic) IBOutlet UIImageView *dotImage;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIStackView *optionStack;
@property (weak, nonatomic) IBOutlet UICollectionView *coreCollection;
@property (weak, nonatomic) IBOutlet UIButton *nxtBtn;
@property (weak, nonatomic) IBOutlet UIView *priceView;
@property (weak, nonatomic) IBOutlet UILabel *priceLbl;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activity;

@property (weak, nonatomic) IBOutlet UIButton *galaryBtn;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loaderActivity;
@property (weak, nonatomic) IBOutlet UIButton *mirrorBtn;
@property (weak, nonatomic) IBOutlet UIButton *colorBtn;

@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIImageView *leftImageView;
@property (weak, nonatomic) IBOutlet UIImageView *bottomImage;
@property (weak, nonatomic) IBOutlet UIImageView *rightImage;
@property (weak, nonatomic) IBOutlet UIImageView *upImage;
@property (weak, nonatomic) IBOutlet UIView *cropView;

@property (weak, nonatomic) IBOutlet UILabel *cartCount;

- (IBAction)cartBtn:(id)sender;
- (IBAction)menuBtn:(id)sender;

- (IBAction)nextAction:(id)sender;
- (IBAction)galaryAction:(id)sender;
- (IBAction)mirrorAction:(id)sender;
- (IBAction)colorAction:(id)sender;


@end

NS_ASSUME_NONNULL_END
