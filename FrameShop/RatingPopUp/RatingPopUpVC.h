//
//  RatingPopUpVC.h
//  FrameShop
//
//  Created by vamika on 13/07/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RatingPopUpVC : UIViewController
@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UIButton *buttonOne;
@property (weak, nonatomic) IBOutlet UIButton *buttonTwo;
@property (weak, nonatomic) IBOutlet UIButton *buttonThree;
@property (weak, nonatomic) IBOutlet UIButton *buttonFour;
@property (weak, nonatomic) IBOutlet UIButton *buttonFive;
- (IBAction)submit:(id)sender;
- (IBAction)notYetClick:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;
- (IBAction)onClickStar:(id)sender;
@end

NS_ASSUME_NONNULL_END
