//
//  RatingPopUpVC.m
//  FrameShop
//
//  Created by vamika on 13/07/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import "RatingPopUpVC.h"
#import "Global.h"
@interface RatingPopUpVC ()
{
    NSArray *_starButtons;
}
@end

@implementation RatingPopUpVC

- (void)viewDidLoad {
    [super viewDidLoad];
    _starButtons = @[self.buttonOne, self.buttonTwo, self.buttonThree, self.buttonFour, self.buttonFive];
    
    
}
- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    [Global roundCorner:_backView roundValue:15.0 borderWidth:0.5 borderColor:APP_GRAY_COLOR];
    
    [Global roundCorner:_submitButton roundValue:_submitButton.frame.size.height/2 borderWidth:0.0 borderColor:nil];
}
- (IBAction)notYetClick:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)submit:(id)sender {
    
    
}
- (IBAction)onClickStar:(id)sender{
    UIButton *button = (UIButton *)sender;
    [self set_stars:(int)[button tag]];
}
-(void)set_stars:(int)num{
   for (int loop = 0; loop < 5; loop++) {
       if ((loop + 1) <= num) {
           [((UIButton*)_starButtons[loop]) setSelected:YES];
       }
       else {
           [((UIButton*)_starButtons[loop]) setSelected:NO];
       }
   }
}
@end
