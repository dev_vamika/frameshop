//
//  SummaryPopUpView.m
//  FrameShop
//
//  Created by Vamika on 09/04/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import "SummaryPopUpView.h"
#import "Global.h"
#import "SummaryPopUpCell.h"
#import "CanvasTableViewCell.h"
#import "BlockMountTableViewCell.h"
#import "AcrylicDetailTableViewCell.h"
@interface SummaryPopUpView (){
    DbManager *db;
    
    
    NSMutableArray *dataArray;
    NSString *type;
    ImageViewTableViewCell *imageCell;
    
}

@end

@implementation SummaryPopUpView

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView registerNib:[UINib nibWithNibName:@"SummaryPopUpCell" bundle:nil] forCellReuseIdentifier:@"SummaryPopUpCell"];
    
    
    
    
    
    dataArray=[[NSMutableArray alloc] init];
    
    db=[[DbManager alloc] init];
    
    NSMutableArray *arr=[[NSMutableArray alloc] init];
    NSMutableArray *frmArr=[[NSMutableArray alloc] init];
    NSMutableArray *matArr=[[NSMutableArray alloc] init];
    NSMutableArray *arr1=[[NSMutableArray alloc] init];
    NSMutableDictionary * temDic=[[NSMutableDictionary alloc] init];
    NSMutableDictionary *outDic=[[NSMutableDictionary alloc] init];
    type=@"frame";
    
    
    if (_infoDic==nil) {
        
        arr=[db getDataForField:USER_PRODUCT where:nil];
            frmArr=[db getDataForField:FRAME_TABLE where:[NSString stringWithFormat:@"id='%@'",arr[0][@"frame"]]];
        matArr=[db getDataForField:METABOARD_TABLE where:[NSString stringWithFormat:@"id='%@'",arr[0][@"topMatBoard"]]];
        arr1=[[NSMutableArray alloc] init];
        temDic=[self getGlassWidthWhereInfoDic:arr[0]];
        float margine  = (([frmArr[0][@"width"] floatValue])*2)+(([frmArr[0][@"rebate"] floatValue])*2);
        outDic=[[NSMutableDictionary alloc] init];
        outDic[@"width"]       =[NSString stringWithFormat:@"%.1f",[temDic[@"width"]floatValue]+margine];
        outDic[@"height"]       =[NSString stringWithFormat:@"%.1f",[temDic[@"height"]floatValue]+margine];
        
        
        
    }
    
    
    
    
    else {
        
        
        if (_infoDic[@"frame"]==nil) {
            
            if ([_infoDic[@"type"] isEqualToString:ACRYLIC_SKU]) {
                type=@"Acrylic";
            }
          else  if ([_infoDic[@"type"] isEqualToString:CANVAS_PRINTING_SKU] ) {
                type = @"canvas";
            }
          else  if ([_infoDic[@"type"] isEqualToString:BLOCK_MOUNT_SKU] ) {
                type = @"block";
            }
            
            
            
        }
        
        else{
            
            
            
            NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
            
            
            
            dic[@"width_tf"]=[NSString stringWithFormat:@"%.2f",[_infoDic[@"width"]floatValue]] ;
            dic[@"height_tf"]=[NSString stringWithFormat:@"%.2f",[_infoDic[@"height"]floatValue]] ;
            arr[0]=dic;
            arr[0][@"style"]=@"none";
            temDic[@"width"]=[NSString stringWithFormat:@"%.2f",[_infoDic[@"size"][@"glass"][@"width"] floatValue]] ;
            temDic[@"height"]=[NSString stringWithFormat:@"%.2f",[_infoDic[@"size"][@"glass"][@"height"] floatValue]] ;
            
            
            outDic[@"width"]=[NSString stringWithFormat:@"%.2f",[_infoDic[@"size"][@"outside"][@"width"] floatValue]] ;
            outDic[@"height"]=[NSString stringWithFormat:@"%.2f",[_infoDic[@"size"][@"outside"][@"height"] floatValue]] ;
            NSMutableDictionary *frmDic=[[NSMutableDictionary alloc] init];
            frmDic[@"sku"]=_infoDic[@"frame"];
            frmArr[frmArr.count]=[frmDic mutableCopy];
            
            
            
            id obj=_infoDic[@"matboards"];
            if ([obj isKindOfClass:[NSDictionary class]]) {
                if (_infoDic[@"matboards"][@"top_mat"]!=nil) {
                    arr[0][@"style"]=@"Single";
                    [frmDic removeAllObjects];
                    frmDic=_infoDic[@"matboards"][@"top_mat"];
                    matArr[matArr.count]=frmDic;
                }
                if (_infoDic[@"matboards"][@"bottom_mat"]!=nil ) {
                    id obj=_infoDic[@"matboards"][@"bottom_mat"];
                    if ([obj isKindOfClass:[NSDictionary class]])
                        arr[0][@"style"]=@"Double";
                }
            }
            
            
            
            
            
            
            
            
            
            
            
            
        }
        
    }
    
    
    
    
    if ([type isEqualToString:@"frame"]) {
        
        
        
        for (int i=0; i<4; i++) {
            NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
            
            
            switch (i) {
                case 0:
                    dic[@"Dimensions"]=@"";
                    break;
                case 1:
                    
                  
                    dic[@"Image"]=[NSString stringWithFormat:@"%.1f x %.1f cm",[arr[0][@"width_tf"]floatValue],[arr[0][@"height_tf"]floatValue]];
                    
                    if ([arr[0][@"unit"] isEqualToString:@"inch"]){
                        dic[@"Image"]=[NSString stringWithFormat:@"%.1f x %.1f inch",floor([arr[0][@"width_tf"] floatValue]/2.5) ,floor([arr[0][@"height_tf"] floatValue]/2.5) ];
                    }
                    break;
                case 2:
                {
                    
                    
                    dic[@"Glass"]=[NSString stringWithFormat:@"%.1f x %.1f cm",[temDic[@"width"]floatValue],[temDic[@"height"]floatValue]];
                   
                    if ([arr[0][@"unit"] isEqualToString:@"inch"]){
                        dic[@"Glass"]=[NSString stringWithFormat:@"%.1f x %.1f inch",floor([temDic[@"width"] floatValue]/2.5) ,floor([temDic[@"height"] floatValue]/2.5) ];
                    }
                    
                }
                    break;
                case 3:{
                    dic[@"Outer(approx)"]=[NSString stringWithFormat:@"%.1f x %.1f cm",[outDic[@"width"]floatValue],[outDic[@"height"]floatValue]];
                    if ([arr[0][@"unit"] isEqualToString:@"inch"]){
                                           dic[@"Outer(approx)"]=[NSString stringWithFormat:@"%.1f x %.1f inch",[outDic[@"width"] floatValue]/2.5 ,[outDic[@"height"] floatValue]/2.5 ];
                                       }
                }
                    break;
                    
                default:
                    break;
            }
            
            arr1[arr1.count]=dic;
        }
        dataArray[dataArray.count]=[arr1 mutableCopy];
        
        [arr1 removeAllObjects];
        
        
        
        
        {
            
            
            NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
            dic[@"Frame Code"]=frmArr[0][@"sku"];
            arr1[arr1.count]=dic;
            dataArray[dataArray.count]=[arr1 mutableCopy];
            [arr1 removeAllObjects];
        }
        
        
        
        
        if (![arr[0][@"style"] isEqualToString:@"none"]) {
            for (int i=0; i<3; i++) {
                NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
                
                
                switch (i) {
                    case 0:
                        dic[@"Top Mat Dimensions"]=@"";
                        break;
                    case 1:
                        dic[@"Code"]= matArr[0][@"code"];
                        break;
                    case 2:
                        dic[@"Name"]=matArr[0][@"name"];
                        break;
                        
                        
                    default:
                        break;
                }
                
                arr1[arr1.count]=dic;
            }
            dataArray[dataArray.count]=[arr1 mutableCopy];
            
            [arr1 removeAllObjects];
            
            
            if ([arr[0][@"style"] isEqualToString:@"Double"]) {
                
                [matArr removeAllObjects];
                
                if (_infoDic!=nil) {
                    
                    matArr[matArr.count]=[_infoDic[@"matboards"][@"bottom_mat"] mutableCopy];
                }
                
                else
                    matArr=[db getDataForField:METABOARD_TABLE where:[NSString stringWithFormat:@"id='%@'",arr[0][@"bottomMat"]]];
                
                
                for (int i=0; i<3; i++) {
                    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
                    
                    
                    switch (i) {
                        case 0:
                            dic[@"Bottom Mat Dimensions"]=@"";
                            break;
                        case 1:
                            dic[@"Code"]=matArr[0][@"code"];
                            break;
                        case 2:
                            dic[@"Name"]=matArr[0][@"name"];
                            break;
                            
                            
                        default:
                            break;
                    }
                    
                    arr1[arr1.count]=dic;
                }
                dataArray[dataArray.count]=[arr1 mutableCopy];
                
                [arr1 removeAllObjects];
            }
            
            
            
        }
        
        
        
        
        for (int i=0; i<2; i++) {
            NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
            
            
            switch (i) {
                case 0:
                    dic[@"Printing"]=@"";
                    break;
                case 1:
                    dic[@"Paper Type"]= @"LUSTER";
                    break;
                    
                    
                    
                default:
                    break;
            }
            
            arr1[arr1.count]=dic;
        }
        dataArray[dataArray.count]=[arr1 mutableCopy];
        
        [arr1 removeAllObjects];
        
        
        for (int i=0; i<4; i++) {
            NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
            
            
            switch (i) {
                case 0:
                    dic[@"Other"]=@"";
                    break;
                case 1:
                    dic[@"Glass"]=@"Clear Perspex";
                    break;
                case 2:
                    
                    dic[@"Backing"]=@"Adhesive Foamcore";
                    break;
                case 3:
                    
                    if ([temDic[@"width"] floatValue]<21 && [temDic[@"height"] floatValue]<21 ) {
                        dic[@"Hanging"]=@"Hanger + Standback";
                    }
                    
                   else if ([temDic[@"width"] floatValue]<21 && [temDic[@"height"] floatValue]<29.7 ) {
                                   
                        dic[@"Hanging"]=@"Hanger + Standback";
                   }else{
                       dic[@"hanger"]=@"D-rings + String";
                   }
                    
                   
                    break;
                    
                default:
                    break;
            }
            
            arr1[arr1.count]=dic;
        }
        dataArray[dataArray.count]=[arr1 mutableCopy];
        
        [arr1 removeAllObjects];
        
    }
    [_tableView reloadData];
    
    
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [self.view addGestureRecognizer:singleFingerTap];
    
    
    
    
    
    
    
    [Global roundCorner:_backView roundValue:10.0 borderWidth:0.5 borderColor:APP_GRAY_COLOR];
    
    [Global roundCorner:_okButton roundValue:_okButton.frame.size.height/2 borderWidth:0.0 borderColor:APP_GRAY_COLOR];
    
    
   // [self performSelector:@selector(delRow) withObject:nil afterDelay:3.0];
  
    
    
 
    
    
    
    
}



- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    //CGPoint location = [recognizer locationInView:[recognizer.view superview]];
    [self dismissViewControllerAnimated:YES completion:nil];
    //Do stuff here...
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (isiPhone4S) {
        self.backViewHeight.constant = self.view.frame.size.height*0.8;
    }else if (isiPhone10) {
        self.backViewHeight.constant = self.view.frame.size.height*0.45;
    }
    else{
        self.backViewHeight.constant = self.view.frame.size.height*0.6;
    }
}



- (IBAction)onOk:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(NSMutableDictionary *)getGlassWidthWhereInfoDic:(NSMutableDictionary *)infoDic{
    NSMutableDictionary * price=[[NSMutableDictionary alloc] init];
    float width =[infoDic[@"width_tf"] floatValue];
    float height=[infoDic[@"height_tf"] floatValue];
    if (!([infoDic[@"style"] isEqualToString:@"none"])) {
        width=width+[infoDic[@"right"]floatValue ]+[infoDic[@"left"]floatValue ];
        height=height+[infoDic[@"top"]floatValue ]+[infoDic[@"bottom"]floatValue ];
        if ([infoDic[@"style"] isEqualToString:@"Double"]){
            width=width+([infoDic[@"bottomUniformWidth"] floatValue]*2);
            height=height+([infoDic[@"bottomUniformWidth"] floatValue]*2);
        }
        
    }
    price[@"width"] =[NSString stringWithFormat:@"%.01f",width];
    price[@"height"]=[NSString stringWithFormat:@"%.01f",height];
    return price;
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([type isEqualToString:@"frame"]) {
        if (_infoDic==nil) {
               return dataArray.count;
           }
        return dataArray.count+1;
    }
    else{
        return 2;
    }
   
    
    
  
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if ([type isEqualToString:@"frame"]) {
        
        
        
        if (_infoDic==nil) {
            static NSString *simpleTableIdentifier = @"ProductDetailTableViewCell";
                   ProductDetailTableViewCell *cell = (ProductDetailTableViewCell *)[_tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
                   
                   if (cell == nil)
                   {
                       NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ProductDetailTableViewCell" owner:self options:nil];
                       cell = [nib objectAtIndex:0];
                   }
                   
                   
                   
                   
                   switch ([dataArray[indexPath.row] count]) {
                       case 1:
                           [cell.view1 removeFromSuperview];
                           [cell.view2 removeFromSuperview];
                           [cell.view3 removeFromSuperview];
                           
                           break;
                       case 2:
                           
                           [cell.view2 removeFromSuperview];
                           [cell.view3 removeFromSuperview];
                           
                           break;
                       case 3:
                           
                           [cell.view3 removeFromSuperview];
                           
                           break;
                           
                       default:
                           break;
                   }
                   
                   for (int i=0; i<[dataArray[indexPath.row] count]; i++) {
                       
                       NSDictionary *dic=dataArray[indexPath.row][i];
                       
                       
                       
                       switch (i) {
                           case 0:
                               for (NSString* key in dic.allKeys)
                               {
                                   cell.heading.text=[NSString stringWithFormat:@"%@",key];
                                   cell.upLbl.text=@"";
                                   if (indexPath.row==1) {
                                       cell.upLbl.text=[NSString stringWithFormat:@": %@",dic[key]];
                                   }
                               }
                               break;
                           case 1:
                               for (NSString* key in dic.allKeys)
                               {
                                   cell.lbl1L.text=[NSString stringWithFormat:@"  %@",key];
                                   cell.lbl1R.text=[NSString stringWithFormat:@": %@",dic[key]] ;
                               }
                               break;
                           case 2:
                               for (NSString* key in dic.allKeys)
                               {
                                   cell.lbl2L.text=[NSString stringWithFormat:@"  %@",key];;
                                   cell.lbl2R.text=[NSString stringWithFormat:@": %@",dic[key]] ;;
                               }
                               break;
                           case 3:
                               for (NSString* key in dic.allKeys)
                               {
                                   cell.lbl3L.text=[NSString stringWithFormat:@"  %@",key];;
                                   cell.lbl3R.text=[NSString stringWithFormat:@": %@",dic[key]] ;;
                               }
                               break;
                               
                           default:
                               break;
                       }
                   }
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                   
                   return cell;
        }
        
        
        
        if (indexPath.row!=0) {
            
   
        
        
        
        static NSString *simpleTableIdentifier = @"ProductDetailTableViewCell";
        ProductDetailTableViewCell *cell = (ProductDetailTableViewCell *)[_tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ProductDetailTableViewCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        
        
        
        switch ([dataArray[indexPath.row-1] count]) {
            case 1:
                [cell.view1 removeFromSuperview];
                [cell.view2 removeFromSuperview];
                [cell.view3 removeFromSuperview];
                
                break;
            case 2:
                
                [cell.view2 removeFromSuperview];
                [cell.view3 removeFromSuperview];
                
                break;
            case 3:
                
                [cell.view3 removeFromSuperview];
                
                break;
                
            default:
                break;
        }
        
        for (int i=0; i<[dataArray[indexPath.row-1] count]; i++) {
            
            NSDictionary *dic=dataArray[indexPath.row-1][i];
            
            
            
            switch (i) {
                case 0:
                    for (NSString* key in dic.allKeys)
                    {
                        cell.heading.text=[NSString stringWithFormat:@"%@",key];
                        cell.upLbl.text=@"";
                        if (indexPath.row==2) {
                            cell.upLbl.text=[NSString stringWithFormat:@": %@",dic[key]];
                        }
                    }
                    break;
                case 1:
                    for (NSString* key in dic.allKeys)
                    {
                        cell.lbl1L.text=[NSString stringWithFormat:@"  %@",key];
                        cell.lbl1R.text=[NSString stringWithFormat:@": %@",dic[key]] ;
                    }
                    break;
                case 2:
                    for (NSString* key in dic.allKeys)
                    {
                        cell.lbl2L.text=[NSString stringWithFormat:@"  %@",key];;
                        cell.lbl2R.text=[NSString stringWithFormat:@": %@",dic[key]] ;;
                    }
                    break;
                case 3:
                    for (NSString* key in dic.allKeys)
                    {
                        cell.lbl3L.text=[NSString stringWithFormat:@"  %@",key];;
                        cell.lbl3R.text=[NSString stringWithFormat:@": %@",dic[key]] ;;
                    }
                    break;
                    
                default:
                    break;
            }
        }
         cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
        }
        else{
            if (imageCell!=nil) {
                return imageCell;
            }
            static NSString *simpleTableIdentifier = @"ImageViewTableViewCell";
                  imageCell  = (ImageViewTableViewCell *)[_tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
                   
                   if (imageCell == nil)
                   {
                       NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ImageViewTableViewCell" owner:self options:nil];
                       imageCell = [nib objectAtIndex:0];
                   }
            imageCell.userInteractionEnabled = YES;
                       UITapGestureRecognizer *singleFingerTap =
                         [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                 action:@selector(handleSingleTapOnImage:)];
                       [imageCell addGestureRecognizer:singleFingerTap];
            imageCell.imageUrl=_infoDic[@"thumb_url"];
            imageCell.thumbImage=self.thumbImage;
            [imageCell setImage];
             imageCell.selectionStyle = UITableViewCellSelectionStyleNone;
            return imageCell;
        }
        
        
    }
    else{
        
        
        if (indexPath.row==1) {
         
            if ([_infoDic[@"type"] isEqualToString:CANVAS_PRINTING_SKU]) {
                static NSString *simpleTableIdentifier = @"CanvasTableViewCell";
                CanvasTableViewCell *cell = (CanvasTableViewCell *)[_tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
                
                if (cell == nil)
                {
                    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CanvasTableViewCell" owner:self options:nil];
                    cell = [nib objectAtIndex:0];
                }
                cell.productDic = (NSDictionary *)_infoDic;
                
                cell.productImage.image = self.thumbImage;
                [cell setUpHistoryView];
                     cell.selectionStyle = UITableViewCellSelectionStyleNone;
                return cell;
            }
            else if ([_infoDic[@"type"] isEqualToString:BLOCK_MOUNT_SKU]){
                
                static NSString *simpleTableIdentifier = @"BlockMountTableViewCell";
                BlockMountTableViewCell *cell = (BlockMountTableViewCell *)[_tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
                
                if (cell == nil)
                {
                    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"BlockMountTableViewCell" owner:self options:nil];
                    cell = [nib objectAtIndex:0];
                }
                cell.productDic = (NSDictionary *)_infoDic;
                [cell setUpHistoryView];
                     cell.selectionStyle = UITableViewCellSelectionStyleNone;
                return cell;
                
            }
            
            
        
        static NSString *simpleTableIdentifier = @"AcrylicDetailTableViewCell";
            AcrylicDetailTableViewCell *cell = (AcrylicDetailTableViewCell *)[_tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"AcrylicDetailTableViewCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
            cell.productDic = _infoDic;
        [cell setUpHistoryView];
             cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
        }
        else{
       
            if (imageCell!=nil) {
                return imageCell;
            }
                
                static NSString *simpleTableIdentifier = @"ImageViewTableViewCell";
                       imageCell = (ImageViewTableViewCell *)[_tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
                       
                       if (imageCell == nil)
                       {
                           NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ImageViewTableViewCell" owner:self options:nil];
                           imageCell = [nib objectAtIndex:0];
                       }
            imageCell.userInteractionEnabled = YES;
            UITapGestureRecognizer *singleFingerTap =
              [[UITapGestureRecognizer alloc] initWithTarget:self
                                                      action:@selector(handleSingleTapOnImage:)];
            [imageCell addGestureRecognizer:singleFingerTap];
                imageCell.imageUrl=_infoDic[@"thumb_url"];
             imageCell.thumbImage=self.thumbImage;
                           [imageCell setImage];
             imageCell.selectionStyle = UITableViewCellSelectionStyleNone;
                return imageCell;
            
        }
    }
    
}
//cartzoom
- (void)handleSingleTapOnImage:(UITapGestureRecognizer *)recognizer
{
    if (imageCell.previewImage.image != nil) {
        [self performSegueWithIdentifier:@"cartzoom" sender:imageCell.previewImage.image];
    }
    
}

#pragma mark - Segue support

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
  if([[segue identifier] isEqualToString:@"cartzoom"]){
        ProductImagePreviewVC *productImagePreviewVC = [segue destinationViewController];
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
        dic[@"image"] = sender;
        productImagePreviewVC.previewImageDic = dic;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([type isEqualToString:@"frame"]) {
       
        
        if (_infoDic==nil) {
             if (indexPath.row==1) {
                       return 45;
                   }
                  
            return 30*[dataArray[indexPath.row] count];
        }
        
        
        
        if (indexPath.row==2) {
            return 45;
        }
       else if (indexPath.row==0) {
            return 350;
        }
        return 30*[dataArray[indexPath.row-1] count];
    }
    else{
         if (indexPath.row==0) {
            return 300;
        }
       else if ([_infoDic[@"type"] isEqualToString:BLOCK_MOUNT_SKU]){
           NSArray *nibViews = [[NSBundle mainBundle] loadNibNamed:@"BlockMountTableViewCell" owner:self options:nil];
           BlockMountTableViewCell *sqr = [nibViews objectAtIndex:0];
           return sqr.frame.size.height;
        }
       else if ([_infoDic[@"type"] isEqualToString:CANVAS_PRINTING_SKU]){
           NSArray *nibViews = [[NSBundle mainBundle] loadNibNamed:@"CanvasTableViewCell" owner:self options:nil];
           CanvasTableViewCell *sqr = [nibViews objectAtIndex:0];
           return sqr.frame.size.height;
        }
        
       
        
        NSArray *nibViews = [[NSBundle mainBundle] loadNibNamed:@"AcrylicDetailTableViewCell" owner:self options:nil];
        AcrylicDetailTableViewCell *sqr = [nibViews objectAtIndex:0];
        return sqr.frame.size.height;
    }
    
    
    
    
    
}


@end

