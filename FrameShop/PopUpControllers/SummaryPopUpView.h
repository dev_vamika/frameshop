//
//  SummaryPopUpView.h
//  FrameShop
//
//  Created by Vamika on 09/04/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBManager.h"
#import "summarySectionView.h"
#import "ProductDetailTableViewCell.h"

#import "ImageViewTableViewCell.h"
#import "ProductImagePreviewVC.h"
NS_ASSUME_NONNULL_BEGIN

@interface SummaryPopUpView : UIViewController
//@property (weak, nonatomic) IBOutlet UIView *secondView;
//@property (weak, nonatomic) IBOutlet UIView *thirdView;
//@property (weak, nonatomic) IBOutlet UILabel *imageDimnetionLbl;
//@property (weak, nonatomic) IBOutlet UILabel *glassLbl;
//@property (weak, nonatomic) IBOutlet UILabel *outerLabel;
//@property (weak, nonatomic) IBOutlet UILabel *paperTypelbl;
//@property (weak, nonatomic) IBOutlet UILabel *codeLbl;
//@property (weak, nonatomic) IBOutlet UILabel *widthLbl;
//@property (weak, nonatomic) IBOutlet UIButton *vGrooveButton;
- (IBAction)onOk:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *okButton;
//@property (weak, nonatomic) IBOutlet UILabel *glassNameLbl;
//@property (weak, nonatomic) IBOutlet UILabel *backingLbl;
//@property (weak, nonatomic) IBOutlet UILabel *hangingLbl;
//@property (weak, nonatomic) IBOutlet UILabel *frameCodeLbl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *backViewHeight;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *backView;
@property NSMutableDictionary *infoDic;
@property UIImage *thumbImage;
//@property (weak, nonatomic) IBOutlet UIView *firstView;
@end

NS_ASSUME_NONNULL_END
