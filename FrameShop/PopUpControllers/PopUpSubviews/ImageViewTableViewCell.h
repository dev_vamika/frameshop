//
//  ImageViewTableViewCell.h
//  FrameShop
//
//  Created by Saurabh Srivastav on 18/05/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ImageViewTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *previewImage;
@property NSString *imageUrl;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activity;
@property UIImage *thumbImage;
-(void)setImage;
@end

NS_ASSUME_NONNULL_END
