//
//  summarySectionView.h
//  FrameShop
//
//  Created by Vamika on 10/04/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface summarySectionView : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UIImageView *CheckMArkImage;;

@end

NS_ASSUME_NONNULL_END
