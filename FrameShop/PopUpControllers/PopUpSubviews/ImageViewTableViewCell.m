//
//  ImageViewTableViewCell.m
//  FrameShop
//
//  Created by Saurabh Srivastav on 18/05/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import "ImageViewTableViewCell.h"

@implementation ImageViewTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _activity.hidden=YES;
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setImage{
    _activity.hidden=false;
    [_activity startAnimating];
    
    if (_thumbImage!=nil) {
        self->_previewImage.image=_thumbImage;
                              self->_previewImage.backgroundColor=[UIColor clearColor];
                              [self->_activity stopAnimating];
                              [self->_activity removeFromSuperview];
    }
    
    else{
           dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
           dispatch_async(q, ^{
               //[Global startShimmeringToView:cell.contentView];
               NSData *datas;
         
               datas =[NSData dataWithContentsOfURL:[NSURL URLWithString:self->_imageUrl]] ;
                   
               
               
               UIImage *img = [[UIImage alloc] initWithData:datas];
               dispatch_async(dispatch_get_main_queue(), ^{
                   if(img!=nil){
                       self->_previewImage.image=img;
                       self->_previewImage.backgroundColor=[UIColor clearColor];
                       [self->_activity stopAnimating];
                       [self->_activity removeFromSuperview];
                   }
                   
                   
               });
           });
    }
    
}
@end
