//
//  ProductDetailTableViewCell.m
//  FrameShop
//
//  Created by Saurabh Srivastav on 30/04/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import "ProductDetailTableViewCell.h"

@implementation ProductDetailTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [Global roundCorner:_backView roundValue:1.5 borderWidth:0.5 borderColor:[UIColor lightGrayColor]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
