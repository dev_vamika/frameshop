//
//  SummaryPopUpCell.m
//  FrameShop
//
//  Created by Vamika on 10/04/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import "SummaryPopUpCell.h"
#import "Global.h"
#import "DBManager.h"
@implementation SummaryPopUpCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [Global roundCorner:_backView roundValue:0.0 borderWidth:0.5 borderColor:APP_GRAY_COLOR];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setValues:(NSString*)sectionTitle{
    DbManager *db = [[DbManager alloc] init];
     NSMutableArray *arr=[db getDataForField:USER_PRODUCT where:nil];
     NSMutableArray * frmArr=[db getDataForField:FRAME_TABLE where:[NSString stringWithFormat:@"id='%@'",arr[0][@"frame"]]];
     NSMutableArray *matArr=[db getDataForField:METABOARD_TABLE where:[NSString stringWithFormat:@"id='%@'",arr[0][@"topMatBoard"]]];
    
    if ([sectionTitle isEqualToString:@"Dimensions"]) {
        
        
        NSDictionary * temDic=[self getGlassWidthWhereInfoDic:arr[0]];
        _titleOne.text = [NSString stringWithFormat:@"Image: %.1f x %.1f cm",[arr[0][@"width_tf"]floatValue],[arr[0][@"height_tf"]floatValue]];
        _titleTwo.text = [NSString stringWithFormat:@"Glass: %.1f x %.1f cm",[temDic[@"width"]floatValue],[temDic[@"height"]floatValue]];
        
        float margine  = (([frmArr[0][@"width"] floatValue])*2)+(([frmArr[0][@"rebate"] floatValue])*2);
        NSMutableDictionary *outDic=[[NSMutableDictionary alloc] init];
        outDic[@"width"]       =[NSString stringWithFormat:@"%.1f",[temDic[@"width"]floatValue]+margine];
        outDic[@"height"]       =[NSString stringWithFormat:@"%.1f",[temDic[@"height"]floatValue]+margine];
        _titleThree.text = [NSString stringWithFormat:@"Outer(approx): %.1f x %.1f cm",[outDic[@"width"]floatValue],[outDic[@"height"]floatValue]];
    }
    
    
    
    else if ([sectionTitle isEqualToString:@"Top Mat Dimensions"]) {
        _titleOne.text = [NSString stringWithFormat:@"Code: %@",matArr[0][@"code"]];
        _titleTwo.text = [NSString stringWithFormat:@"Name: %@ cm",matArr[0][@"name"]];
//        NSLog(@"Top Frame Code:%@ and name %@", matArr[0][@"code"],matArr[0][@"name"]);
//                   NSLog(@"Bottom mat code %@ Bottom Mat name %@",matArr[0][@"code"],matArr[0][@"name"]);
    }else if ([sectionTitle isEqualToString:@"Bottom Mat Dimensions"]) {
       _titleOne.text = [NSString stringWithFormat:@"Code: %@",matArr[0][@"code"]];
        _titleTwo.text = [NSString stringWithFormat:@"Name: %@ cm",matArr[0][@"name"]];
    }
//
//
//
//        NSMutableDictionary *outDic=[[NSMutableDictionary alloc] init];
//
//
//    if ([arr[0][@"topMat"] isEqualToString:@"V Groove"]) {
//        _vGrooveButton.selected=YES;
//    }
//
//     _imageDimnetionLbl.text=[NSString stringWithFormat:@"Image: %@ x %@ cm",arr[0][@"width_tf"],arr[0][@"height_tf"]];
//
//        NSDictionary * temDic=[self getGlassWidthWhereInfoDic:arr[0]];
//
//
//     _glassLbl.text=[NSString stringWithFormat:@"Glass: %@ x %@ cm",temDic[@"width"],temDic[@"height"]];
//
//
//        float margine  = (([frmArr[0][@"width"] floatValue])*2)-(([frmArr[0][@"rebate"] floatValue])*2);
//        outDic[@"width"]       =[NSString stringWithFormat:@"%.1f",[temDic[@"width"]floatValue]+margine];
//        outDic[@"heigh"]       =[NSString stringWithFormat:@"%.1f",[temDic[@"height"]floatValue]+margine];
//
//      _outerLabel.text=[NSString stringWithFormat:@"Outer(approx): %@ x %@ cm",outDic[@"width"],outDic[@"heigh"]];
//
//    _frameCodeLbl.text=frmArr[0][@"sku"];
//
//
//
//    _paperTypelbl.text= @"Paper Type: Luster";
//    _glassNameLbl.text=@"Clear";
//    _backingLbl.text=@"MDF";
//    _hangingLbl.text=@"Standback + Hanger";
}
-(NSMutableDictionary *)getGlassWidthWhereInfoDic:(NSMutableDictionary *)infoDic{
    NSMutableDictionary * price=[[NSMutableDictionary alloc] init];
    float width =[infoDic[@"width_tf"] floatValue];
    float height=[infoDic[@"height_tf"] floatValue];
    if (!([infoDic[@"style"] isEqualToString:@"none"])) {
        width=width+[infoDic[@"right"]floatValue ]+[infoDic[@"left"]floatValue ];
        height=height+[infoDic[@"top"]floatValue ]+[infoDic[@"bottom"]floatValue ];
        if ([infoDic[@"style"] isEqualToString:@"Double"]){
            width=width+([infoDic[@"bottomUniformWidth"] floatValue]*2);
            height=height+([infoDic[@"bottomUniformWidth"] floatValue]*2);
        }
        
    }
    price[@"width"] =[NSString stringWithFormat:@"%.01f",width];
    price[@"height"]=[NSString stringWithFormat:@"%.01f",height];
    return price;
}
@end
