//
//  ProductDetailTableViewCell.h
//  FrameShop
//
//  Created by Saurabh Srivastav on 30/04/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Global.h"
NS_ASSUME_NONNULL_BEGIN

@interface ProductDetailTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UIView *headingView;
@property (weak, nonatomic) IBOutlet UIView *view1;
@property (weak, nonatomic) IBOutlet UIView *view2;
@property (weak, nonatomic) IBOutlet UIView *view3;
@property (weak, nonatomic) IBOutlet UILabel *heading;
@property (weak, nonatomic) IBOutlet UILabel *upLbl;
@property (weak, nonatomic) IBOutlet UILabel *lbl1L;
@property (weak, nonatomic) IBOutlet UILabel *lbl1R;
@property (weak, nonatomic) IBOutlet UILabel *lbl2L;
@property (weak, nonatomic) IBOutlet UILabel *lbl2R;
@property (weak, nonatomic) IBOutlet UILabel *lbl3L;
@property (weak, nonatomic) IBOutlet UILabel *lbl3R;

@end

NS_ASSUME_NONNULL_END
