//
//  SummaryPopUpCell.h
//  FrameShop
//
//  Created by Vamika on 10/04/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SummaryPopUpCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleOne;
@property (weak, nonatomic) IBOutlet UILabel *valueOne;
@property (weak, nonatomic) IBOutlet UILabel *titleTwo;
@property (weak, nonatomic) IBOutlet UILabel *valueTwo;
@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UILabel *titleThree;
@property (weak, nonatomic) IBOutlet UILabel *valueThree;
-(void)setValues:(NSString*)sectionTitle;
@end

NS_ASSUME_NONNULL_END
