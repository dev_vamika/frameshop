//
//  ImageCropperViewController.h
//  imageEditor
//
//  Created by Vamika on 17/12/19.
//  Copyright © 2019 Vamika. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IQKeyboardManager.h"
#import "FilterViewController.h"
#import "TOCropViewController.h"
#import "UIImage+CropRotate.h"



NS_ASSUME_NONNULL_BEGIN

@interface ImageCropperViewController : UIViewController<TOCropViewControllerDelegate>
@property (strong, nonatomic) IBOutlet UIView *editViewBg;

@property (weak, nonatomic) IBOutlet UIImageView *lockImageView;
@property (weak, nonatomic) IBOutlet UIImageView *unlockImageView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activity;

@property UIImage *imageToEdit;
@property NSMutableDictionary *activeDictionary;
@property BOOL isFromPreview;
- (IBAction)onNextStep:(id)sender;
- (IBAction)onStandardClick:(id)sender;
- (IBAction)onSelectCustom:(id)sender;
- (IBAction)onSelectPortrait:(id)sender;
- (IBAction)onSelectLandscape:(id)sender;
- (IBAction)onSelectSquare:(id)sender;

@property (weak, nonatomic) IBOutlet UICollectionView *collectioView;
@property (weak, nonatomic) IBOutlet UIButton *nextStep;
@property (weak, nonatomic) IBOutlet UIButton *customBtn;
@property (weak, nonatomic) IBOutlet UIStackView *standardOptionStackView;
@property (weak, nonatomic) IBOutlet UIButton *standardBtn;
@property (weak, nonatomic) IBOutlet UIButton *portraitBtn;
@property (weak, nonatomic) IBOutlet UIButton *landscapeBtn;
@property (weak, nonatomic) IBOutlet UIButton *squareBTn;
- (IBAction)onClickEffects:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *effectBtn;
@property (weak, nonatomic) IBOutlet UIButton *fullImageChoiceBtn;
//- (IBAction)onClickUseFullImage:(id)sender;
//- (IBAction)onClickCm:(id)sender;
//- (IBAction)onClickInch:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *widthTF;
@property (weak, nonatomic) IBOutlet UITextField *heightTF;
//@property (weak, nonatomic) IBOutlet UILabel *unitLbl;
//@property (weak, nonatomic) IBOutlet UIButton *ratioInfoBtn;
//- (IBAction)onClickRatioLocked:(id)sender;
//@property (weak, nonatomic) IBOutlet UIImageView *ratioImageView;
//- (IBAction)onClickInfo:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *customViewContainer;
//@property (weak, nonatomic) IBOutlet UIButton *cmButton;
//@property (weak, nonatomic) IBOutlet UIButton *inchButton;
@property (weak, nonatomic) IBOutlet UILabel *cartCount;
- (IBAction)cartBtn:(id)sender;
//@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *editLoader;
- (IBAction)menuBtn:(id)sender;
//@property (weak, nonatomic) IBOutlet UIView *topCroppperView;
//@property (weak, nonatomic) IBOutlet UIView *unitStackView;
//@property (weak, nonatomic) IBOutlet UIView *fullImgStackView;
//@property (weak, nonatomic) IBOutlet UIButton *yesBtn;
//@property (weak, nonatomic) IBOutlet UIButton *noBtn;
//@property (weak, nonatomic) IBOutlet UIView *unitView;
//@property (weak, nonatomic) IBOutlet UIButton *stdInchBtn;
@property NSString * productId;
- (IBAction)useFullImageSeg:(id)sender;
//@property (weak, nonatomic) IBOutlet UIButton *stdCmBtn;
- (IBAction)ratioLockedSeg:(id)sender;
- (IBAction)unitSelectSeg:(id)sender;
- (IBAction)standardUnitSeg:(id)sender;
@property (weak, nonatomic) IBOutlet UISegmentedControl *standardUnitSegement;
@property (weak, nonatomic) IBOutlet UISegmentedControl *useFullImageSegment;
@property (weak, nonatomic) IBOutlet UISegmentedControl *ratioLockSegment;
@property (weak, nonatomic) IBOutlet UISegmentedControl *customUnitSegment;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *editViewHeight;

@end

NS_ASSUME_NONNULL_END
