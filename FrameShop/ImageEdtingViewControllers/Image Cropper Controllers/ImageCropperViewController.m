//
//  ImageCropperViewController.m
//  imageEditor
//
//  Created by Vamika on 17/12/19.
//  Copyright © 2019 Vamika. All rights reserved.
//

#import "ImageCropperViewController.h"
#import "Global.h"
#import "AppDelegate.h"

#ifndef CGWidth
#define CGWidth(rect)                   rect.size.width
#endif

#ifndef CGHeight
#define CGHeight(rect)                  rect.size.height
#endif

#ifndef CGOriginX
#define CGOriginX(rect)                 rect.origin.x
#endif

#ifndef CGOriginY
#define CGOriginY(rect)                 rect.origin.y
#endif

@interface ImageCropperViewController ()<UIScrollViewDelegate,TOCropViewControllerDelegate,UITextFieldDelegate>
{
    CGRect cropArea;
    NSMutableDictionary *imageProccessingfInfoDic;
    NSArray *selectedOptionArr;
    TOCropViewController *cropController;
    CGSize selectedSize,defaulImageSize;
    NSIndexPath *selectedIndex,*highLightedIndex;
    UIImage *originalImage;
    BOOL onSelectEffect,onSelectNextButton,menuSelected;
    
    BOOL selectMinDefaultStandardSize;
    NSString * sku;
    CGSize maxLimitTobeSelect;
}
@end

@implementation ImageCropperViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _cartCount.hidden= YES;
    selectMinDefaultStandardSize = NO;
   // _topView.hidden = NO;
   
    menuSelected = NO;
    selectedOptionArr = [[NSArray alloc] init];
    imageProccessingfInfoDic = [[NSMutableDictionary alloc] init];
    
    [self setBordersToViews];
    
    self.title = @"Choose a Size";
    
    [self.widthTF.keyboardToolbar.doneBarButton setTarget:self action:@selector(nextAction)];
    [self.heightTF.keyboardToolbar.doneBarButton setTarget:self action:@selector(doneAction)];
    
    _activity.hidden=true;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showEditImageImage:) name:@"show_effect" object:nil];
    
    self.widthTF.delegate = self;
    self.heightTF.delegate = self;
    
    UISwipeGestureRecognizer * swipeleft=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeleft:)];
    swipeleft.direction=UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeleft];
   // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishCart) name:CART_REFRESH object:nil];
    [self.navigationItem setBackBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil]];
    self.landscapeBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
//    _loaderActivity.hidden = false;
//    [_loaderActivity startAnimating];
   // [Global startShimmeringToView:_topViewUpLoaderView];
    //[Global startShimmeringToView:_topViewBottomLoaderView];
    
//    if (isiPhone4S) {
//        _editViewHeight.constant =  self.view.frame.size.height*0.45;
//    }else{
//        _editViewHeight.constant = self.view.frame.size.height*0.55;
//    }
        
        NSArray * arrD = [APP_DELEGATE.db getDataForField:PRODUCT_TABLE where:[NSString stringWithFormat:@"id='%@'",_productId] ];
        
        if (arrD.count != 0) {
            sku = arrD[0][@"sku"];
            if ([sku isEqualToString:FRAME_SKU]) {
                [self.nextStep setTitle:@"Next: Framing" forState:UIControlStateNormal];
               
            }
           else if ([sku isEqualToString:CANVAS_PRINTING_SKU]) {
                [self.nextStep setTitle:@"Next: Wrap Style" forState:UIControlStateNormal];
               
            }else{
                [self.nextStep setTitle:@"Next: Summary" forState:UIControlStateNormal];
            }
        }
   
    
  
//        self->_topView.hidden = YES;
//        self->_loaderActivity.hidden = true;
//        [self->_loaderActivity stopAnimating];
//        self.nextBtn.enabled = true;
    

        [self setRestViews];
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    self.view.userInteractionEnabled = YES;
    if (!menuSelected){
        if (isiPhone4S) {
            _editViewHeight.constant =  self.view.frame.size.height*0.45;
        }else{
            _editViewHeight.constant = self.view.frame.size.height*0.55;
        }
    }
    
      
 
    
}



-(void)setRestViews{
//    if (!menuSelected && _isFromPreview) {
//        self.finalImage.image = _imageToEdit;
//
//    }
   // _finalImage.image = nil;
   // _finalImage.hidden = true;
    if (!menuSelected) {
    NSMutableArray *arr=[APP_DELEGATE.db getDataForField:USER_PRODUCT where:nil];
    if (arr == nil) {
       
        NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
        dic[@"id"]=[NSString stringWithFormat:@"%@",self.productId];
        [APP_DELEGATE.db insertIntoTable:dic table_name:USER_PRODUCT];
    }
    NSString *str = arr[0][CROPPING_TYPE];
    if (![str isEqualToString:@""]) {
        if ([str isEqualToString:@"STANDARD"]) {
            self.activity.hidden = NO;
           [self.activity startAnimating];
            [self onRefreshStandardWithDBWithInputImage:_imageToEdit];
        }else{
            self.activity.hidden = NO;
           [self.activity startAnimating];
            [self onRefreshCustomWithDBWithInputImage:_imageToEdit];
        }
    }
    else{
       
            [self refreshLoadedViewWithInputImage:_imageToEdit];
        
    }
    
     }
    int count=[[[NSUserDefaults standardUserDefaults] valueForKey:CART_COUNT] intValue];
      if (count>9) {
          _cartCount.text=@"9+";
      }
      else{
      _cartCount.text=[NSString stringWithFormat:@"%d",[[[NSUserDefaults standardUserDefaults] valueForKey:CART_COUNT] intValue]];
      }
          _cartCount.hidden=[_cartCount.text isEqualToString:@"0"];
 
}

-(void)didFinishCart{
    
    int count=[[[NSUserDefaults standardUserDefaults] valueForKey:CART_COUNT] intValue];
    if (count>9) {
        _cartCount.text=@"9+";
    }
    else{
    _cartCount.text=[NSString stringWithFormat:@"%d",[[[NSUserDefaults standardUserDefaults] valueForKey:CART_COUNT] intValue]];
    }
        _cartCount.hidden=[_cartCount.text isEqualToString:@"0"];
}

-(void)refreshLoadedViewWithInputImage:(UIImage *)img{
    
    if (_isFromPreview) {
        self.activity.hidden = NO;
       [self.activity startAnimating];
        _imageToEdit = img;
        [self onStandardClick:[UIButton new]];
    }else{
        self.customBtn.selected = NO;
        self.standardBtn.selected = YES;
        self.standardBtn.backgroundColor =[UIColor colorWithRed:60/255.0 green:69/255.0 blue:143/255.0 alpha:1.0];
        self.customBtn.backgroundColor =[UIColor colorWithRed:239/255.0 green:239/255.0 blue:239/255.0 alpha:1.0];
        self.customViewContainer.hidden = YES;
        [self openCropeerWithCropRectSize:CGSizeMake(0, 0) aspectRatio:CGSizeMake(selectedSize.width, selectedSize.height) isCustom:NO image:img zoomFactor:0.0];
    }
}
-(void)setBordersToViews{
    [Global roundCorner:self.portraitBtn roundValue:5 borderWidth:0.0 borderColor:[UIColor whiteColor]];
    [Global roundCorner:self.landscapeBtn roundValue:5 borderWidth:0.0 borderColor:[UIColor whiteColor]];
    [Global roundCorner:self.heightTF roundValue:0.0 borderWidth:1.0 borderColor:[UIColor colorWithRed:239/255.0 green:239/255.0 blue:239/255.0 alpha:1.0]];
    [Global roundCorner:self.widthTF roundValue:0.0 borderWidth:1.0 borderColor:[UIColor colorWithRed:239/255.0 green:239/255.0 blue:239/255.0 alpha:1.0]];
    [Global roundCorner:self.editViewBg roundValue:0.0 borderWidth:1.5 borderColor:[UIColor colorWithRed:239/255.0 green:239/255.0 blue:239/255.0 alpha:1.0]];
    [Global roundCorner:self.squareBTn roundValue:5 borderWidth:0.0 borderColor:[UIColor whiteColor]];
    [Global roundCorner:self.effectBtn roundValue:self.effectBtn.frame.size.height/2 borderWidth:0.1 borderColor:[UIColor colorWithRed:60/255.0 green:69/255.0 blue:143/255.0 alpha:1.0]];
    [Global roundCorner:self.cartCount roundValue:self.cartCount.frame.size.height/2 borderWidth:2.0 borderColor:[UIColor whiteColor]];
    [Global roundCorner:self.customBtn roundValue:self.customBtn.frame.size.height/2 borderWidth:0.0 borderColor:[UIColor clearColor]];
    [Global roundCorner:self.standardBtn roundValue:self.standardBtn.frame.size.height/2 borderWidth:0.0 borderColor:[UIColor clearColor]];
    
    [self.standardUnitSegement setTitleTextAttributes:@{NSFontAttributeName:self.landscapeBtn.titleLabel.font,
     NSForegroundColorAttributeName:APP_GRAY_COLOR}
    forState:UIControlStateNormal];
    [self.standardUnitSegement setTitleTextAttributes:@{NSFontAttributeName:self.landscapeBtn.titleLabel.font,
     NSForegroundColorAttributeName:APP_BLUE_COLOR}
    forState:UIControlStateSelected];
    [self.customUnitSegment setTitleTextAttributes:@{NSFontAttributeName:self.widthTF.font,
        NSForegroundColorAttributeName:APP_GRAY_COLOR}
       forState:UIControlStateNormal];
    [self.customUnitSegment setTitleTextAttributes:@{NSFontAttributeName:self.widthTF.font,
     NSForegroundColorAttributeName:APP_BLUE_COLOR}
    forState:UIControlStateSelected];
    [self.useFullImageSegment setTitleTextAttributes:@{NSFontAttributeName:self.widthTF.font,
        NSForegroundColorAttributeName:APP_GRAY_COLOR}
       forState:UIControlStateNormal];
    [self.useFullImageSegment setTitleTextAttributes:@{NSFontAttributeName:self.widthTF.font,
     NSForegroundColorAttributeName:APP_BLUE_COLOR}
    forState:UIControlStateSelected];
}

-(void)swipeleft:(UISwipeGestureRecognizer*)gestureRecognizer
{
       //Do what you want here
   
    menuSelected = YES;
  if ([gestureRecognizer locationInView:self.view].x>self.view.bounds.size.width*0.80){
         [Global showSideMenu];
    }
}

#pragma mark - showEditImageFromEffect
-(void)showEditImageImage:(NSNotification*)notification{
    //return;
    NSDictionary* info = [notification userInfo];
    self.widthTF.text = @"";
    self.heightTF.text = @"";
    //UIImage *oldImage =_imageToEdit;
    imageProccessingfInfoDic[WIDTH] = info[WIDTH];
    imageProccessingfInfoDic[HEIGHT] = info[HEIGHT];
    imageProccessingfInfoDic[ZOOMFACTOR] = info[ZOOMFACTOR];
    imageProccessingfInfoDic[@"effectName"] = info[@"effectName"];
    imageProccessingfInfoDic[FILTER_APPLIED] = @"YES";
    imageProccessingfInfoDic[@"index"] = info[@"index"];
    _imageToEdit = (UIImage *)info[@"image"];
//    UIImage *newImage = info[@"image"];
//    if (oldImage.size.width != newImage.size.width) {
//        self.finalImage.image = [self imageWithImage:info[@"image"] scaledToSize:oldImage.size];
//    }else  if (oldImage.size.height != newImage.size.height) {
//        self.finalImage.image = [self imageWithImage:info[@"image"] scaledToSize:oldImage.size];
//    }else{
//
//    }
   // self.finalImage.image = [self imageWithImage:info[@"image"] scaledToSize:oldImage.size];

    //[self.view addSubview:self.activity];
    
//    [cropController setImage:_imageToEdit];
//    [_collectioView reloadData];
////    return;
//
    if ([imageProccessingfInfoDic[@"select_sizes"] isEqualToString:@"STANDARD"]) {
    self.activity.hidden = NO;
   [self.activity startAnimating];
    [self onRefreshStandardWithDBWithInputImage:_imageToEdit];
    }else{
    self.activity.hidden = NO;
   [self.activity startAnimating];
    [self onRefreshCustomWithDBWithInputImage:_imageToEdit];
    }
}


#pragma mark - onClickCart
- (IBAction)cartBtn:(id)sender{
    [Global callMenuViewWithCode:@"0"];
}
#pragma mark - onClickMenu
- (IBAction)menuBtn:(id)sender{
    menuSelected = YES;
    [Global showSideMenu];
}
#pragma mark - onClickEffects
- (IBAction)onClickEffects:(id)sender {
    self.activity.hidden = YES;
    [self.activity stopAnimating];
    
    onSelectEffect = !onSelectEffect;
    [cropController doneButtonTapped];
}

#pragma mark - onNextStep
- (IBAction)onNextStep:(id)sender {

    self.activity.hidden = NO;
    [self.activity startAnimating];
    if (![imageProccessingfInfoDic[@"select_sizes"] isEqualToString:@"STANDARD"]) {
        if ([self.heightTF.text isEqualToString:@""] && [self.widthTF.text isEqualToString:@""]) {
            [self.view makeToast:@"please enter width or height" duration:2.0 position:CSToastPositionCenter];
            return;
        }
        if (([self.heightTF.text floatValue] > maxLimitTobeSelect.height) && ([self.widthTF.text floatValue] > maxLimitTobeSelect.width)) {
            [self.view makeToast:@"please enter correct size" duration:2.0 position:CSToastPositionCenter];
            return;
        }
        if ([imageProccessingfInfoDic[UNIT] isEqualToString:@"cm"]) {
            if ([self.heightTF.text floatValue] > 152.5 || [self.widthTF.text floatValue] > 101.5) {
                [self showAlertPopUpWithMessage:[NSString stringWithFormat:@"Min: 10 x 10 cm, Max: %.1f x %.1f cm",maxLimitTobeSelect.width*2.54,maxLimitTobeSelect.height*2.54]];
                self.widthTF.text = [NSString stringWithFormat:@"%.1f",defaulImageSize.width];
                self.heightTF.text = [NSString stringWithFormat:@"%.1f",defaulImageSize.height];
                return;
            }
        }
        else if([imageProccessingfInfoDic[UNIT] isEqualToString:@"inch"]){
            if ([self.heightTF.text floatValue] > 60.0 || [self.widthTF.text floatValue] > 40.0) {
                [self showAlertPopUpWithMessage:[NSString stringWithFormat:@"Min: 4 x 4 Inch, Max: %.1f x %.1f Inch",maxLimitTobeSelect.width,maxLimitTobeSelect.height]];
                self.widthTF.text = [NSString stringWithFormat:@"%.1f",defaulImageSize.width];
                self.heightTF.text = [NSString stringWithFormat:@"%.1f",defaulImageSize.height];
                return;
            }
            
        }
        if (self.heightTF.text.length == 0 || self.widthTF.text.length == 0) {
            [self showAlertPopUpWithMessage:@"Please enter your image size before processing"];
            return;
        }
        //self.heightTF != nil || self.widthTF.text != nil
        if ([self.heightTF.text isEqualToString:@""] || self.heightTF.text== nil ) {
            [self showAlertPopUpWithMessage:@"Please enter your image size before processing"];
            
            return;
        }
        if ([self.widthTF.text isEqualToString:@""] || self.widthTF.text== nil ) {
            [self showAlertPopUpWithMessage:@"Please enter your image size before processing"];
            
            return;
        }
        self.view.userInteractionEnabled = NO;
            onSelectNextButton = !onSelectNextButton;
//        [cropController doneButtonTapped:CGSizeMake([[NSString stringWithFormat:@"%.1f",[self->selectedOptionArr[selectedIndex.row][@"width"] floatValue]*2.54] floatValue], [[NSString stringWithFormat:@"%.1f",[self->selectedOptionArr[selectedIndex.row][@"height"] floatValue]*2.54] floatValue])type:sku];
        [cropController doneButtonTapped];
        
        
    }else{
       
        self.heightTF.text = @"";
        self.widthTF.text = @"";
        self.view.userInteractionEnabled = NO;
        onSelectNextButton = !onSelectNextButton;
        [cropController doneButtonTapped];

    }
     
}
#pragma mark - onStandardClick
- (IBAction)onStandardClick:(id)sender {
    [self.widthTF resignFirstResponder];
    [self.heightTF resignFirstResponder];
    self.activity.hidden = NO;
   [self.activity startAnimating];
    [self deSelectAllOptions];
    if (imageProccessingfInfoDic.count == 0) {
        if (_imageToEdit.size.width > _imageToEdit.size.height)
        {
            //landscape
            imageProccessingfInfoDic[TYPE_CHOOSED] = @"Landscape";
            selectedOptionArr = _activeDictionary[@"Landscape"];
            self.landscapeBtn.selected = YES;
            self.landscapeBtn.backgroundColor = [UIColor colorWithRed:60/255.0 green:69/255.0 blue:143/255.0 alpha:1.0];
        }
        else if (_imageToEdit.size.height > _imageToEdit.size.width)
        {
            //portrait
            imageProccessingfInfoDic[TYPE_CHOOSED] = @"Portrait";
            selectedOptionArr = _activeDictionary[@"Portrait"];
            self.portraitBtn.selected = YES;
            self.portraitBtn.backgroundColor = [UIColor colorWithRed:60/255.0 green:69/255.0 blue:143/255.0 alpha:1.0];
        }else{
            //square
            imageProccessingfInfoDic[TYPE_CHOOSED] = @"Square";
            selectedOptionArr = _activeDictionary[@"Square"];
               self.squareBTn.selected = YES;
               self.squareBTn.backgroundColor = [UIColor colorWithRed:60/255.0 green:69/255.0 blue:143/255.0 alpha:1.0];
        }
    }else{
    if ([imageProccessingfInfoDic[TYPE_CHOOSED] isEqualToString:@"Square"]) {
        imageProccessingfInfoDic[TYPE_CHOOSED] = @"Square";
        selectedOptionArr = _activeDictionary[@"Square"];
           self.squareBTn.selected = YES;
           self.squareBTn.backgroundColor = [UIColor colorWithRed:60/255.0 green:69/255.0 blue:143/255.0 alpha:1.0];
       }else if ([imageProccessingfInfoDic[TYPE_CHOOSED] isEqualToString:@"Landscape"]) {
           imageProccessingfInfoDic[TYPE_CHOOSED] = @"Landscape";
           selectedOptionArr = _activeDictionary[@"Landscape"];
           self.landscapeBtn.selected = YES;
           self.landscapeBtn.backgroundColor = [UIColor colorWithRed:60/255.0 green:69/255.0 blue:143/255.0 alpha:1.0];
       }else{
           imageProccessingfInfoDic[TYPE_CHOOSED] = @"Portrait";
           selectedOptionArr = _activeDictionary[@"Portrait"];
           self.portraitBtn.selected = YES;
           self.portraitBtn.backgroundColor = [UIColor colorWithRed:60/255.0 green:69/255.0 blue:143/255.0 alpha:1.0];
       }
    }
    imageProccessingfInfoDic[@"select_sizes"] = @"STANDARD";
    
    //[_collectioView reloadData];
    //self.finalImage.hidden = YES;
    self.customBtn.selected = NO;
    self.standardBtn.selected = YES;
    self.standardBtn.backgroundColor =[UIColor colorWithRed:60/255.0 green:69/255.0 blue:143/255.0 alpha:1.0];
    self.customBtn.backgroundColor =[UIColor colorWithRed:239/255.0 green:239/255.0 blue:239/255.0 alpha:1.0];
    self.customViewContainer.hidden = YES;
    if ([imageProccessingfInfoDic[@"unit"] isEqualToString:@"cm"]) {
        [self selectCmWithInputImage:_imageToEdit];
    }else{
        [self SelectInchWithInputImage:_imageToEdit];
    }
    if ([sku isEqualToString:CANVAS_PRINTING_SKU]) {
        [self openCropeerWithCropRectSize:CGSizeMake(0, 0) aspectRatio:CGSizeMake(8.0, 10.0) isCustom:NO image:_imageToEdit zoomFactor:0.0];
    }else{
    float w = 4.0;
    float h = 6.0;
    [self openCropeerWithCropRectSize:CGSizeMake(0, 0) aspectRatio:CGSizeMake(w, h) isCustom:NO image:_imageToEdit zoomFactor:3.0];
    }
}

#pragma mark - onSelectCustom
- (IBAction)onSelectCustom:(id)sender {
    if ([sku isEqualToString:ACRYLIC_PHOTOBOOK]){
        [ self showAlertPopUpWithMessage:@"Sorry custom sizes are not available for this product"];
        return;
    }
    [self setImageMaxLimitForCutomOptionWithInputImage:_imageToEdit];
    self.activity.hidden = NO;
   [self.activity startAnimating];
    imageProccessingfInfoDic[@"select_sizes"] = @"CUSTOM";
    self.useFullImageSegment.selected = YES;
    self.customUnitSegment.selected = YES;

    //self.finalImage.hidden = YES;
    self.customBtn.selected = YES;
    self.standardBtn.selected = NO;
    self.standardBtn.backgroundColor = [UIColor colorWithRed:239/255.0 green:239/255.0 blue:239/255.0 alpha:1.0];
    self.customBtn.backgroundColor = [UIColor colorWithRed:60/255.0 green:69/255.0 blue:143/255.0 alpha:1.0];
    self.customViewContainer.hidden = NO;
    //    BOOL userFullImageOption = imageProccessingfInfoDic[@"use_full_image"];
    if ([imageProccessingfInfoDic[@"unit"] isEqualToString:@"cm"]) {
        [self selectCmWithInputImage:_imageToEdit];
    }else{
        [self SelectInchWithInputImage:_imageToEdit];
    }
    if ([imageProccessingfInfoDic[@"use_full_image"] isEqualToString:@"YES"] || [[NSUserDefaults standardUserDefaults] boolForKey:@"FULL_IMAGE"]) {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"FULL_IMAGE"];
        [self onClickUseFullImage:[UIButton new]];
    }else{
        [self onNoUseFullImage:[UIButton new]];
    }
   if (self.widthTF.text.length == 0 || self.heightTF.text.length == 0) {
        defaulImageSize = [self calculateCustomDefaultSize:_imageToEdit];
        self.widthTF.text = [NSString stringWithFormat:@"%.1f",defaulImageSize.width];
        self.heightTF.text = [NSString stringWithFormat:@"%.1f",defaulImageSize.height];
        imageProccessingfInfoDic[WIDTH] = [NSString stringWithFormat:@"%.1f",[self.widthTF.text floatValue]*2.54] ;
        imageProccessingfInfoDic[HEIGHT] = [NSString stringWithFormat:@"%.1f",[self.heightTF.text floatValue]*2.54];
    }else{
        imageProccessingfInfoDic[WIDTH] = [imageProccessingfInfoDic[UNIT] isEqualToString:@"cm"]?self.widthTF.text:[NSString stringWithFormat:@"%.1f",[self.widthTF.text floatValue]*2.54] ;
        imageProccessingfInfoDic[HEIGHT] = [imageProccessingfInfoDic[UNIT] isEqualToString:@"cm"]?self.heightTF.text:[NSString stringWithFormat:@"%.1f",[self.heightTF.text floatValue]*2.54] ;
    }
    
}
- (void) setImageMaxLimitForCutomOptionWithInputImage:(UIImage *)img{
    if (img.size.width > img.size.height)
    {
        //landscape
        maxLimitTobeSelect = CGSizeMake(36.0,24.0);
    }
    else if (img.size.height > img.size.width)
    {
        //portrait
        maxLimitTobeSelect =  CGSizeMake(24.0,36.0);
    }else{
        //square
        maxLimitTobeSelect = CGSizeMake(36.0,36.0);
    }
}
- (void)selectCmWithInputImage:(UIImage *)img {
    imageProccessingfInfoDic[@"unit"] = @"cm";
    if ([imageProccessingfInfoDic[@"select_sizes"] isEqualToString:@"STANDARD"]) {
        [self.standardUnitSegement setSelectedSegmentIndex:0];
        [self standardUnitSeg:_standardUnitSegement];
       // [self.collectioView reloadData];
    }else{
        if (self.customUnitSegment.selectedSegmentIndex == 1) {
            [self.customUnitSegment setSelectedSegmentIndex:0];
            [self unitSelectSeg:_customUnitSegment];
        }
        
    }
    defaulImageSize = [self calculateCustomDefaultSize:img];
}
- (void)SelectInchWithInputImage:(UIImage *)img{
    imageProccessingfInfoDic[@"unit"] = @"inch";
    if ([imageProccessingfInfoDic[@"select_sizes"] isEqualToString:@"STANDARD"]) {
         [self.standardUnitSegement setSelectedSegmentIndex:1];
        [self standardUnitSeg:_standardUnitSegement];
     //   [self.collectioView reloadData];
    }else{
        if (self.customUnitSegment.selectedSegmentIndex == 0 ) {
            [self.customUnitSegment setSelectedSegmentIndex:1];
                   [self unitSelectSeg:_customUnitSegment];
        }
        

    }
    defaulImageSize = [self calculateCustomDefaultSize:img];
    
}
#pragma mark - refreshCustomWithDB
- (void)onRefreshCustomWithDBWithInputImage:(UIImage *)img{
    [self setImageMaxLimitForCutomOptionWithInputImage:img];
    NSMutableArray *dbData = [APP_DELEGATE.db getDataForField:USER_PRODUCT where:nil];
    if ([dbData[0][USE_FULL_IMAGE] isEqualToString:@"YES"]) {
        [self.useFullImageSegment setSelectedSegmentIndex:0];

    }else{
        [self.useFullImageSegment setSelectedSegmentIndex:1];
    }
   // self.finalImage.hidden = YES;
    self.customBtn.selected = YES;
    self.standardBtn.selected = NO;
    self.standardBtn.backgroundColor = [UIColor colorWithRed:239/255.0 green:239/255.0 blue:239/255.0 alpha:1.0];
    self.customBtn.backgroundColor = [UIColor colorWithRed:60/255.0 green:69/255.0 blue:143/255.0 alpha:1.0];
    self.customViewContainer.hidden = NO;
    //    if ([dbData[0][USE_FULL_IMAGE] isEqualToString:@"YES"]) {
    //        [self onClickUseFullImage:[UIButton new]];
    //    }else{
    //        [self onNoUseFullImage:[UIButton new]];
    //    }
    NSString *unitType = dbData[0][UNIT];
    if ([unitType isEqualToString:@"cm"]) {
        [self onClickCm:[UIButton new] andInputImage:img];
    }else{
        [self onClickInch:[UIButton new] andInputImage:img];
    }
    
    self.widthTF.text = [unitType isEqualToString:@"cm"]?dbData[0][WIDTH_TF]:[NSString stringWithFormat:@"%.1f",[dbData[0][WIDTH_TF] floatValue]*0.393701];//1 cm = 0.393701 inch
    self.heightTF.text = [unitType isEqualToString:@"cm"]?dbData[0][HEIGHT_TF]:[NSString stringWithFormat:@"%.1f",[dbData[0][HEIGHT_TF] floatValue]*0.393701];
   // [self doneAction];
     if ([dbData[0][USE_FULL_IMAGE] isEqualToString:@"YES"]) {
    [self openCropeerWithCropRectSize:CGSizeMake(0, 0) aspectRatio:CGSizeMake([self.widthTF.text intValue],[self.heightTF.text intValue]) isCustom:NO image:img zoomFactor:4.0];
    cropController.cropView.userInteractionEnabled = NO;
     }else{
     NSString *zoomFactor = [self checkImageSize:img :CGSizeMake([self.widthTF.text floatValue], [self.heightTF.text floatValue]) type:@""];
    [self openCropeerWithCropRectSize:CGSizeMake(0, 0) aspectRatio:CGSizeMake([self.widthTF.text intValue] , [self.heightTF.text intValue]) isCustom:YES image:img zoomFactor:[zoomFactor floatValue]];
     }
}
#pragma mark - refreshStandardWithDB
- (void)onRefreshStandardWithDBWithInputImage:(UIImage *)img{
    [self deSelectAllOptions];
    NSMutableArray *dbData = [APP_DELEGATE.db getDataForField:USER_PRODUCT where:nil];
    selectedOptionArr = _activeDictionary[dbData[0][TYPE_CHOOSED]];
    selectedIndex = [NSIndexPath indexPathForRow:[dbData[0][SELECTED_INDEX] floatValue] inSection:0];
    imageProccessingfInfoDic[@"select_sizes"] = @"STANDARD";
    if ([dbData[0][TYPE_CHOOSED] isEqualToString:@"Portrait"]) {
        self.portraitBtn.selected = YES;
        self.portraitBtn.backgroundColor = [UIColor colorWithRed:60/255.0 green:69/255.0 blue:143/255.0 alpha:1.0];
    }else if ([dbData[0][TYPE_CHOOSED] isEqualToString:@"Landscape"]) {
        self.landscapeBtn.selected = YES;
        self.landscapeBtn.backgroundColor = [UIColor colorWithRed:60/255.0 green:69/255.0 blue:143/255.0 alpha:1.0];
    }else{
        self.squareBTn.selected = YES;
        self.squareBTn.backgroundColor = [UIColor colorWithRed:60/255.0 green:69/255.0 blue:143/255.0 alpha:1.0];
    }
   // self.finalImage.hidden = YES;
    self.customBtn.selected = NO;
    self.standardBtn.selected = YES;
    self.standardBtn.backgroundColor =[UIColor colorWithRed:60/255.0 green:69/255.0 blue:143/255.0 alpha:1.0];
    self.customBtn.backgroundColor =[UIColor colorWithRed:239/255.0 green:239/255.0 blue:239/255.0 alpha:1.0];
    self.customViewContainer.hidden = YES;
     NSString *unitType = dbData[0][UNIT];
    if ([unitType isEqualToString:@"cm"]) {
        [self onClickCm:[UIButton new] andInputImage:img];
    }else{
        [self onClickInch:[UIButton new] andInputImage:img];
    }
    float w = [unitType isEqualToString:@"cm"]?[dbData[0][WIDTH_TF] floatValue]:[[NSString stringWithFormat:@"%.1f",[dbData[0][WIDTH_TF] floatValue]*0.393701] floatValue];
    float h = [unitType isEqualToString:@"cm"]?[dbData[0][HEIGHT_TF] floatValue]:[[NSString stringWithFormat:@"%.1f",[dbData[0][HEIGHT_TF] floatValue]*0.393701] floatValue];
    [self openCropeerWithCropRectSize:CGSizeMake(0, 0) aspectRatio:CGSizeMake(w, h) isCustom:NO image:img zoomFactor:3.0];
}
#pragma mark - onSelectPortrait
- (IBAction)onSelectPortrait:(id)sender {
    [self deSelectAllOptions];
    imageProccessingfInfoDic[TYPE_CHOOSED] = @"Portrait";
    self.portraitBtn.selected = YES;
    self.portraitBtn.backgroundColor = [UIColor colorWithRed:60/255.0 green:69/255.0 blue:143/255.0 alpha:1.0];
    selectedOptionArr = [[NSArray alloc] init];
    selectedOptionArr = _activeDictionary[@"Portrait"];
    [_collectioView reloadData];
    
}
#pragma mark - onSelectLandscape
- (IBAction)onSelectLandscape:(id)sender {
    [self deSelectAllOptions];
    imageProccessingfInfoDic[TYPE_CHOOSED] = @"Landscape";
    self.landscapeBtn.selected = YES;
    self.landscapeBtn.backgroundColor = [UIColor colorWithRed:60/255.0 green:69/255.0 blue:143/255.0 alpha:1.0];
    selectedOptionArr = [[NSArray alloc] init];
    selectedOptionArr = _activeDictionary[@"Landscape"];
    [_collectioView reloadData];
    
}
#pragma mark - onSelectSquare
- (IBAction)onSelectSquare:(id)sender {
    [self deSelectAllOptions];
    imageProccessingfInfoDic[TYPE_CHOOSED] = @"Square";
    self.squareBTn.selected = YES;
    self.squareBTn.backgroundColor = [UIColor colorWithRed:60/255.0 green:69/255.0 blue:143/255.0 alpha:1.0];
    selectedOptionArr = [[NSArray alloc] init];
    selectedOptionArr = _activeDictionary[@"Square"];
    [_collectioView reloadData];
    
}

-(void)deSelectAllOptions{
    self.portraitBtn.selected = NO;
    self.landscapeBtn.selected = NO;
    self.squareBTn.selected = NO;
    self.portraitBtn.backgroundColor = [UIColor colorWithRed:239/255.0 green:239/255.0 blue:239/255.0 alpha:1.0];
    self.landscapeBtn.backgroundColor = [UIColor colorWithRed:239/255.0 green:239/255.0 blue:239/255.0 alpha:1.0];
    self.squareBTn.backgroundColor = [UIColor colorWithRed:239/255.0 green:239/255.0 blue:239/255.0 alpha:1.0];
}
#pragma mark - onClickUseFullImage

- (IBAction)onClickUseFullImage:(id)sender {
    self.fullImageChoiceBtn.selected = YES;
    imageProccessingfInfoDic[@"use_full_image"] = @"YES";
    //NSLog(@"%@",imageProccessingfInfoDic[@"use_full_image"]);
    [self.useFullImageSegment setSelectedSegmentIndex:0];
    defaulImageSize = [self calculateCustomDefaultSize:_imageToEdit];
    self.widthTF.text = [NSString stringWithFormat:@"%.1f",defaulImageSize.width];
    self.heightTF.text = [NSString stringWithFormat:@"%.1f",defaulImageSize.height];
    
    CGSize imageDefaultWOrH = [self convertInchOrCmToPixel:[NSString stringWithFormat:@"%.1f",defaulImageSize.width] height:[NSString stringWithFormat:@"%.1f",defaulImageSize.height]];
    [self openCropeerWithCropRectSize:CGSizeMake(0, 0) aspectRatio:CGSizeMake(imageDefaultWOrH.width,imageDefaultWOrH.height) isCustom:NO image:_imageToEdit zoomFactor:1.0];
    cropController.cropView.userInteractionEnabled = NO;
    
}
- (IBAction)onNoUseFullImage:(id)sender{
     self.fullImageChoiceBtn.selected = NO;
    defaulImageSize = [self calculateCustomDefaultSize:_imageToEdit];
    self.widthTF.text = [NSString stringWithFormat:@"%.1f",defaulImageSize.width];
    self.heightTF.text = [NSString stringWithFormat:@"%.1f",defaulImageSize.height];
    if (![imageProccessingfInfoDic[@"use_full_image"] isEqualToString:@""]) {
         NSString *zoomFactor = [self checkImageSize:_imageToEdit :CGSizeMake([self.widthTF.text floatValue], [self.heightTF.text floatValue]) type:@""];
               [self openCropeerWithCropRectSize:CGSizeMake(_imageToEdit.size.width, _imageToEdit.size.height) aspectRatio:CGSizeMake(0.0, 0.0) isCustom:YES image:_imageToEdit zoomFactor:[zoomFactor floatValue]];
    }else{
//        NSString *zoomFactor = [self checkImageSize:self.finalImage.image :CGSizeMake([imageProccessingfInfoDic[WIDTH] floatValue]*0.393701, [imageProccessingfInfoDic[HEIGHT] floatValue]*0.393701) type:@""];
//        [self openCropeerWithCropRectSize:CGSizeMake([imageProccessingfInfoDic[WIDTH] floatValue]*0.393701, [imageProccessingfInfoDic[WIDTH] floatValue]*0.393701) aspectRatio:CGSizeMake(0.0, 0.0) isCustom:YES image:self.finalImage.image zoomFactor:0.0];
        [self openCropeerWithCropRectSize:CGSizeMake(_imageToEdit.size.width, _imageToEdit.size.height) aspectRatio:CGSizeMake(0.0, 0.0) isCustom:YES image:_imageToEdit zoomFactor:0.0];
    }
   
    imageProccessingfInfoDic[@"use_full_image"] = @"";
    [self.useFullImageSegment setSelectedSegmentIndex:1];
    cropController.cropView.userInteractionEnabled = YES;
    
}
//-(CGSize)calculateDefaultSizeInInch:(UIImage*)image{
//    //    •    Width (inch) = Image Width in Pixels / 150
//    //    •    Width (cm) = Image Width in Pixels / 150 * 2.54
//    float ratio = image.size.width/image.size.height;
//    if ([imageProccessingfInfoDic[@"unit"] isEqualToString:@"cm"]) {
//        float cmW = image.size.width/150*2.54;
//        float cmH = cmW/ratio;
//        return CGSizeMake(cmW, cmH);
//    }else{
//        float inchW = image.size.width/150;
//        float inchH = inchW/ratio;
//        return CGSizeMake(inchW, inchH);
//    }
//}

-(CGSize)calculateCustomDefaultSize:(UIImage*)image{
    float ratio = image.size.width/image.size.height;
    int MaximumSizeLimit = 36;
    float imageW = 0.0,imageH = 0.0;
    if (ratio>1) {
        //LANDSCAPE
            imageW = MIN(image.size.width/150, MaximumSizeLimit);
            imageH = imageW/ratio;
    }else if(ratio <= 1){
            //PORTRAIT OR SQUARE
            imageH = MIN(image.size.height/150, MaximumSizeLimit);
            imageW = imageH*ratio;
    }
    if ([imageProccessingfInfoDic[@"unit"] isEqualToString:@"cm"]) {
        return CGSizeMake(imageW*2.54, imageH*2.54);
    }
    return CGSizeMake(imageW, imageH);
}


#pragma mark - onClickCm
- (IBAction)onClickCm:(id)sender andInputImage:(UIImage *)img{
    
//    if ([imageProccessingfInfoDic[@"unit"] isEqualToString:@"inch"]) {
         imageProccessingfInfoDic[@"unit"] = @"cm";
           if ([imageProccessingfInfoDic[@"select_sizes"] isEqualToString:@"STANDARD"]) {

               [self.standardUnitSegement setSelectedSegmentIndex:0];
               [self.collectioView reloadData];
           }else{

               [self.customUnitSegment setSelectedSegmentIndex:0];
               if (![self.widthTF.text isEqualToString:@""] || ![self.heightTF.text isEqualToString:@""]) {
                   self.widthTF.text = [self returnRightMetric:self.widthTF.text typeOfMetricUsed:@"inches"];
                   self.heightTF.text = [self returnRightMetric:self.heightTF.text typeOfMetricUsed:@"inches"];
               }
           }
//    }
   defaulImageSize = [self calculateCustomDefaultSize:img];
    
    // NSLog(@"%@",imageProccessingfInfoDic[@"unit"]);
}
#pragma mark - onClickInch
- (IBAction)onClickInch:(id)sender andInputImage:(UIImage *)img{
//     if ([imageProccessingfInfoDic[@"unit"] isEqualToString:@"cm"]) {
    imageProccessingfInfoDic[@"unit"] = @"inch";
    //NSLog(@"inch");
    if ([imageProccessingfInfoDic[@"select_sizes"] isEqualToString:@"STANDARD"]) {

        [self.standardUnitSegement setSelectedSegmentIndex:1];
        [self.collectioView reloadData];
    }else{

        [self.customUnitSegment setSelectedSegmentIndex:1];
        if (![self.widthTF.text isEqualToString:@""] || ![self.heightTF.text isEqualToString:@""]) {
            self.widthTF.text = [self returnRightMetric:self.widthTF.text typeOfMetricUsed:@"cm"];
            self.heightTF.text = [self returnRightMetric:self.heightTF.text typeOfMetricUsed:@"cm"];
        }
    }
//     }
    defaulImageSize = [self calculateCustomDefaultSize:img];
    
}
#pragma mark - onClickRatioLocked
- (IBAction)onClickRatioLocked:(id)sender {
}
#pragma mark - onClickInfo
- (IBAction)onClickInfo:(id)sender {
}
#pragma mark - covertor
- (NSString *)returnRightMetric:(NSString *)theMeasure typeOfMetricUsed:(NSString *)metricType
{
    //[imageProccessingfInfoDic[@"unit"] isEqualToString:@"cm"];
    NSString *result = nil;
    if ([imageProccessingfInfoDic[@"unit"] isEqualToString:@"cm"]) {
        if (![metricType isEqualToString:@"pixeltocm"] && ((self.customUnitSegment.selectedSegmentIndex==1) && (!self.standardBtn.selected))) {
            float oneCm = 1/2.54; // 1 cm = 0.393701 inch;
            float convertInchToCm = [theMeasure floatValue] * oneCm;
            result = [NSString stringWithFormat:@"%.1f", convertInchToCm];
            
        } else if ([imageProccessingfInfoDic[@"unit"] isEqualToString:@"cm"] && ![metricType isEqualToString:@"pixeltocm"] ) {
            float oneInch = 2.54; // 1 inch = 2.54 cm;
            float convertCmToInch = [theMeasure floatValue] * oneInch;
            result = [NSString stringWithFormat:@"%.1f", convertCmToInch];
        }else if ([metricType isEqualToString:@"pixeltocm"]) {
            if ([imageProccessingfInfoDic[@"unit"] isEqualToString:@"cm"]) {
                float onePx = 0.0264583333; // 1 onePx = 0.0264583333 cm;
                float pixelToCM = [theMeasure floatValue] * onePx;
                result = [NSString stringWithFormat:@"%.1f", pixelToCM];
            }else{
                float onePx = 0.0104166667; // 1 onePx = 0.0104166667 in;
                float pixelToIn = [theMeasure floatValue] * onePx;
                result = [NSString stringWithFormat:@"%.1f", pixelToIn];
            }
        }
    }else{
        if ([imageProccessingfInfoDic[@"unit"] isEqualToString:@"inch"] && ![metricType isEqualToString:@"pixeltocm"]) {
            float oneCm = 1/2.54; // 1 cm = 0.393701 inch;
            float convertInchToCm = [theMeasure floatValue] * oneCm;
            result = [NSString stringWithFormat:@"%.1f", convertInchToCm];
            
        } else if ([imageProccessingfInfoDic[@"unit"] isEqualToString:@"cm"] && ![metricType isEqualToString:@"pixeltocm"] ) {
            float oneInch = 2.54; // 1 inch = 2.54 cm;
            float convertCmToInch = [theMeasure floatValue] * oneInch;
            result = [NSString stringWithFormat:@"%.1f", convertCmToInch];
        }else if ([metricType isEqualToString:@"pixeltocm"]) {
            if ([imageProccessingfInfoDic[@"unit"] isEqualToString:@"cm"]) {
                float onePx = 0.0264583333; // 1 onePx = 0.0264583333 cm;
                float pixelToCM = [theMeasure floatValue] * onePx;
                result = [NSString stringWithFormat:@"%.1f", pixelToCM];
            }else{
                float onePx = 0.0104166667; // 1 onePx = 0.0104166667 in;
                float pixelToIn = [theMeasure floatValue] * onePx;
                result = [NSString stringWithFormat:@"%.1f", pixelToIn];
            }
        }
    }
    
    
    
    return result;
}
- (CGSize)convertInchOrCmToPixel:(NSString *)width height:(NSString *)height{
    CGSize result;
    if ([imageProccessingfInfoDic[@"unit"] isEqualToString:@"cm"]) {
        float oneCm = 37.7; //1 cm   = 37.7952755906 pixel (X) 
        float cmTopixelW = [width floatValue] * oneCm;
        float cmTopixelH = [height floatValue] * oneCm;
        result = CGSizeMake([[NSString stringWithFormat:@"%.1f", cmTopixelW] floatValue], [[NSString stringWithFormat:@"%.1f", cmTopixelH] floatValue]);
    }else{
        float oneInch = 96; // 1 in  =  96 pixel (X)
        float inchTopixelW = [width floatValue] * oneInch;
        float inchTopixelH = [height floatValue] * oneInch;
        result = CGSizeMake([[NSString stringWithFormat:@"%.1f", inchTopixelW] floatValue], [[NSString stringWithFormat:@"%.1f", inchTopixelH] floatValue]);
    }
    return result;
}
#pragma mark - TextFieldMethods
-(void)nextAction{
    [self doneAction];
}
-(void)doneAction{
    
    if ([self.heightTF.text isEqualToString:@""] && [self.widthTF.text isEqualToString:@""]) {
        [self.view makeToast:@"please enter width or height" duration:2.0 position:CSToastPositionCenter];
        return;
    }
    
    if (![self.widthTF.text isEqualToString:@""]) {
        self.widthTF.text = [NSString stringWithFormat:@"%.1f",[self.widthTF.text floatValue]];
        if (![self.heightTF.text isEqualToString:@""]) {
            BOOL rightWValue = [self textFieldShouldReturn:self.widthTF isW:YES];
            BOOL rightHValue = [self textFieldShouldReturn:self.heightTF isW:NO];
            if (rightHValue && rightWValue) {
                [self getpixelFromInchWithInputImage:_imageToEdit];
            }else{
                if (self.customUnitSegment.selectedSegmentIndex == 0) {
                    [self.widthTF.text intValue] == 0?[self.widthTF becomeFirstResponder]:[self.heightTF becomeFirstResponder];
                    [self showAlertPopUpWithMessage:[NSString stringWithFormat:@"Min: 10 x 10 cm, Max: %.1f x %.1f cm",maxLimitTobeSelect.width*2.54,maxLimitTobeSelect.height*2.54]];
                    self.widthTF.text = [NSString stringWithFormat:@"%.1f",defaulImageSize.width];
                    self.heightTF.text = [NSString stringWithFormat:@"%.1f",defaulImageSize.height];
                    
                }else{
                    [self.widthTF.text intValue] == 0?[self.widthTF becomeFirstResponder]:[self.heightTF becomeFirstResponder];;
                    [self showAlertPopUpWithMessage:[NSString stringWithFormat:@"Min: 4 x 4 Inch, Max: %.1f x %.1f Inch",maxLimitTobeSelect.width,maxLimitTobeSelect.height]];
                    self.widthTF.text = [NSString stringWithFormat:@"%.1f",defaulImageSize.width];
                    self.heightTF.text = [NSString stringWithFormat:@"%.1f",defaulImageSize.height];
                    
                }
                CGSize imageWOrH = [self convertInchOrCmToPixel:self.widthTF.text height:self.heightTF.text];
                NSString *zoomFactor = [self checkImageSize:_imageToEdit :CGSizeMake([self.widthTF.text floatValue], [self.heightTF.text floatValue]) type:@""];
                [self openCropeerWithCropRectSize:CGSizeMake(0, 0) aspectRatio:CGSizeMake(imageWOrH.width , imageWOrH.height) isCustom:YES image:_imageToEdit zoomFactor:[zoomFactor floatValue]];
            }
        }
        else{
            if (self.useFullImageSegment.selectedSegmentIndex == 0) {
                if (self.customUnitSegment.selectedSegmentIndex == 0) {
                    BOOL rightWValue = [self textFieldShouldReturn:self.widthTF isW:YES];
                    if (!rightWValue){
//                        self.widthTF.text = ([self.widthTF.text integerValue] < 10 )?@"10":[NSString stringWithFormat:@"%.1f",MIN(maxLimitTobeSelect.width*2.54, defaulImageSize.width)];
                         self.heightTF.text = ([self.heightTF.text integerValue] < 10 )?@"10":[NSString stringWithFormat:@"%.1f",MIN(maxLimitTobeSelect.height*2.54, defaulImageSize.height)];
                        
                    }
                    self.heightTF.text = [self calculateRatioWidth:self.widthTF.text andHeight:@"" inputImage:_imageToEdit];
                    BOOL rightHValue = [self textFieldShouldReturn:self.heightTF isW:NO];
                    if (!rightHValue) {
//                        self.heightTF.text = ([self.heightTF.text integerValue] < 10 )?@"10":[NSString stringWithFormat:@"%.1f",MIN(maxLimitTobeSelect.height*2.54, defaulImageSize.height)];
                        self.widthTF.text = ([self.widthTF.text integerValue] < 10 )?@"10":[NSString stringWithFormat:@"%.1f",MIN(maxLimitTobeSelect.width*2.54, defaulImageSize.width)];
                    }
                    [self getpixelFromInchWithInputImage:_imageToEdit];
                }else{
                    if ([self.widthTF.text integerValue] < 4 ){
                     //   [self showAlertPopUpWithMessage:@"Width can't be less than 4.0"];
                        [self showAlertPopUpWithMessageAndHandler:@"Width can't be less than 4.0" andCompletionHandler:^(int result) {
                            [self.widthTF becomeFirstResponder];
                        }];
                        return;
                    }
                    BOOL rightWValue = [self textFieldShouldReturn:self.widthTF isW:YES];
                    if (!rightWValue) {
//                        self.widthTF.text = [self.widthTF.text integerValue] < 4?@"4":[NSString stringWithFormat:@"%.1f",MIN(maxLimitTobeSelect.width, defaulImageSize.width)];
                         self.heightTF.text = ([self.heightTF.text integerValue] < 4 )?@"4":[NSString stringWithFormat:@"%.1f",MIN(maxLimitTobeSelect.height, defaulImageSize.height)];
                    }
                    self.heightTF.text = [self calculateRatioWidth:self.widthTF.text andHeight:@"" inputImage:_imageToEdit];
                    if ([self.heightTF.text integerValue] < 4){
                        self.heightTF.text =  @"4";
                    }
                    BOOL rightHValue = [self textFieldShouldReturn:self.heightTF isW:NO];
                    if (!rightHValue) {
//                        self.heightTF.text = ([self.heightTF.text integerValue] < 4 )?@"4":[NSString stringWithFormat:@"%.1f",MIN(maxLimitTobeSelect.height, defaulImageSize.height)];
                        self.widthTF.text = [self.widthTF.text integerValue] < 4?@"4":[NSString stringWithFormat:@"%.1f",MIN(maxLimitTobeSelect.width, defaulImageSize.width)];
                    }
                    [self getpixelFromInchWithInputImage:_imageToEdit];
                }
            }
        }
    }else{
        self.heightTF.text = [NSString stringWithFormat:@"%.1f",[self.heightTF.text floatValue]];
        if (self.useFullImageSegment.selectedSegmentIndex == 0) {
            if (self.customUnitSegment.selectedSegmentIndex == 0) {
                
                BOOL rightHValue = [self textFieldShouldReturn:self.heightTF isW:NO];
                if (!rightHValue) {
                    self.heightTF.text = ([self.heightTF.text integerValue] < 10 )?@"4":[NSString stringWithFormat:@"%.1f",MIN(maxLimitTobeSelect.height, defaulImageSize.height)];
//                    self.widthTF.text = ([self.widthTF.text integerValue] < 10 )?@"10":[NSString stringWithFormat:@"%.1f",MIN(maxLimitTobeSelect.width*2.54, defaulImageSize.width)];
                }
                self.widthTF.text = [self calculateRatioWidth:@"" andHeight:self.heightTF.text inputImage:_imageToEdit];
                BOOL rightWValue = [self textFieldShouldReturn:self.widthTF isW:YES];
                if (!rightWValue) {
                    self.widthTF.text = ([self.widthTF.text integerValue] < 10 )?@"4":[NSString stringWithFormat:@"%.1f",MIN(maxLimitTobeSelect.width, defaulImageSize.width)];
//                    self.heightTF.text = ([self.heightTF.text integerValue] < 10 )?@"10":[NSString stringWithFormat:@"%.1f",MIN(maxLimitTobeSelect.height*2.54, defaulImageSize.height)];
                }
                [self getpixelFromInchWithInputImage:_imageToEdit];
            }else{
                
                BOOL rightHValue = [self textFieldShouldReturn:self.heightTF isW:NO];
                if (!rightHValue) {
                    self.heightTF.text = [self.heightTF.text integerValue] < 4?@"4":[NSString stringWithFormat:@"%.1f",MIN(maxLimitTobeSelect.height, defaulImageSize.height)];
//                    self.widthTF.text = ([self.widthTF.text integerValue] < 4 )?@"4":[NSString stringWithFormat:@"%.1f",MIN(maxLimitTobeSelect.width, defaulImageSize.width)];
                }
                self.widthTF.text = [self calculateRatioWidth:@"" andHeight:self.heightTF.text inputImage:_imageToEdit];
                BOOL rightWValue = [self textFieldShouldReturn:self.widthTF isW:YES];
                if (!rightWValue) {
                    self.widthTF.text = ([self.widthTF.text integerValue] < 4 )?@"4":[NSString stringWithFormat:@"%.1f",MIN(maxLimitTobeSelect.width, defaulImageSize.width)];
//                    self.heightTF.text = [self.heightTF.text integerValue] < 4?@"4":[NSString stringWithFormat:@"%.1f",MIN(maxLimitTobeSelect.height, defaulImageSize.height)];
                }
                [self getpixelFromInchWithInputImage:_imageToEdit];
            }
        }
    }
}
-(NSString*)calculateRatioWidth:(NSString*)width andHeight:(NSString*)height inputImage:(UIImage*)image{
    NSString *result;
    float ratio = image.size.width/image.size.height;
    if ([width isEqualToString:@""]) {
       
        //calculate width =EnteredHeight * Ratio
        result = [NSString stringWithFormat:@"%.1f",[height floatValue]*ratio];
    }else{
      //  float ratio = image.size.height/image.size.width;
        //calculate height = EnteredWidth/Ratio
        result = [NSString stringWithFormat:@"%.1f",[width floatValue]/ (ratio)];
    }
    return result;
}
-(void)showAlertPopUpWithMessage:(NSString*)message {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"FrameShop" message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //button click event
        self.view.userInteractionEnabled = YES;
    }];
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}
-(void)showAlertPopUpWithMessageAndHandler:(NSString*)message andCompletionHandler:(void (^)(int result))completionHandler{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"FrameShop" message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //button click event
        completionHandler(1);
        self.view.userInteractionEnabled = YES;
    }];
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField isW:(BOOL)value
{
    
    
    if (self.customUnitSegment.selectedSegmentIndex == 0) {
        NSNumberFormatter* formatter= [[NSNumberFormatter alloc]init];
        formatter.numberStyle= NSNumberFormatterDecimalStyle;
        formatter.allowsFloats= YES;
        formatter.maximum= value?[NSNumber numberWithFloat:(maxLimitTobeSelect.width*2.54)]:[NSNumber numberWithFloat:(maxLimitTobeSelect.height*2.54)];
        formatter.minimum= @10.0;
        return [formatter numberFromString: textField.text]!= nil;
    }
    NSNumberFormatter* formatter= [[NSNumberFormatter alloc]init];
    formatter.numberStyle= NSNumberFormatterDecimalStyle;
    formatter.allowsFloats= YES;
    formatter.maximum= value?[NSNumber numberWithFloat:(maxLimitTobeSelect.width)]:[NSNumber numberWithFloat:(maxLimitTobeSelect.height)];
    formatter.minimum= @4.0;
    return [formatter numberFromString: textField.text]!= nil;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    // Allow to remove character (Backspace)
    if (_fullImageChoiceBtn.selected) {
        if (textField == self.widthTF) {
            self.heightTF.text = @"";
        }else if(textField == self.heightTF){
            self.widthTF.text = @"";
        }
    }
    int numberOfDecimal = 1;
    if ([string isEqualToString:@""]) {
        return true;
    }
    // Block multiple dot
    if ([textField.text containsString:@"."] && [string isEqualToString:@"."]) {
        return false;
    }
    // Check here decimal places
    if ([textField.text containsString:@"."]) {
        NSString *strDecimalPlace = [[textField.text componentsSeparatedByString:@"."] lastObject];
        if (strDecimalPlace.length < numberOfDecimal) {
            return true;
        }
        else {
            return false;
        }
    }
    if (self.customUnitSegment.selectedSegmentIndex == 0) {
        if (textField.text.length >4 && [textField.text containsString:@"."]){
            return false;
        }
        if (textField.text.length >2 && ![textField.text containsString:@"."]){
            if(![string containsString:@"."]){
                textField.text = [NSString stringWithFormat:@"%@.",textField.text];
            }
            
            if(textField.text.length == 4){
                if(textField == _widthTF){
                    [_widthTF resignFirstResponder];
                    [_heightTF becomeFirstResponder];
                }else{
                    [_heightTF resignFirstResponder];
                    [_widthTF becomeFirstResponder];
                }
            }
        }
    }else{
        if (textField.text.length >2 && [textField.text containsString:@"."]){
            return false;
        }
        if (textField.text.length >1 && ![textField.text containsString:@"."]){
            if(![string containsString:@"."]){
                textField.text = [NSString stringWithFormat:@"%@.",textField.text];
            }
            
            //            if(textField.text.length == 3){
            //                if(textField == _widthTF){
            //                    [_widthTF resignFirstResponder];
            //                    [_heightTF becomeFirstResponder];
            //                }else{
            //                    [_heightTF resignFirstResponder];
            //                    [_widthTF becomeFirstResponder];
            //                }
            //            }
            
        }
    }
    return true;
}
#pragma mark - loadCropper
-(void)openCropeerWithCropRectSize:(CGSize)cropViewSize aspectRatio:(CGSize)aspectRatio isCustom:(BOOL)isCustom image:(UIImage*)inputImage zoomFactor:(CGFloat)zoom{
    if (cropController != nil) {
        [cropController.view removeFromSuperview];
        cropController = nil;
    }
    
    if ([sku isEqualToString:CANVAS_PRINTING_SKU])
        zoom = 1.0f;
    
    if (isCustom) {
        [self loadCustomCropper:cropViewSize aspectRatio:aspectRatio image:inputImage zommFactor:zoom];
    }else{
        [self loadStandardCropperAspectRatio:aspectRatio image:inputImage zommFactor:zoom];
    }
}
#pragma mark - loadCustomCropper
-(void)loadCustomCropper:(CGSize)cropViewSize aspectRatio:(CGSize)aspectRatio image:(UIImage*)inputImage zommFactor:(CGFloat)maxZoom{
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isCustom"];
    cropController = [[TOCropViewController alloc] initWithCroppingStyle:TOCropViewCroppingStyleDefault image:inputImage];
    if (isiPhone4S) {
        cropController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height*0.45);
    }else{
        cropController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height*0.55);
    }
    
    cropController.delegate = self;
    if (!self.fullImageChoiceBtn.selected) {
        cropController.cropView.isCustomCropping = YES;
        [self.ratioLockSegment setSelectedSegmentIndex:1];
        self.lockImageView.image = [UIImage imageNamed:@"lock_iconH.png"];
        self.unlockImageView.image = [UIImage imageNamed:@"unlock_iconH.png"];
        cropController.cropView.userInteractionEnabled = YES;
    }else{
        [self.ratioLockSegment setSelectedSegmentIndex:0];
        self.lockImageView.image = [UIImage imageNamed:@"lock_icon.png"];
        self.unlockImageView.image = [UIImage imageNamed:@"unlock_icon.png"];
        cropController.cropView.userInteractionEnabled = NO;
    }
    if (aspectRatio.width != 0.0) {
        cropController.aspectRatioLockEnabled = YES;
        cropController.customAspectRatio = aspectRatio;
    }else{
        cropController.aspectRatioLockEnabled = NO;
    }
    if (maxZoom == 0) {
        cropController.cropView.maximumZoomScale = 2.5;
    }else{
        cropController.cropView.maximumZoomScale = maxZoom;
    }
   
    cropController.imageCropFrame = CGRectMake(0.0, 0.0, cropViewSize.width, cropViewSize.height);
    [self.view addSubview:cropController.view];
    [self.view insertSubview:self.effectBtn aboveSubview:self->cropController.view];
    self.activity.hidden = YES;
                   [self.activity stopAnimating];
}
#pragma mark - loadStandardCropper
-(void)loadStandardCropperAspectRatio:(CGSize)aspectRatio image:(UIImage*)inputImage zommFactor:(CGFloat)maxZoom{
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isCustom"];
    cropController = [[TOCropViewController alloc] initWithCroppingStyle:TOCropViewCroppingStyleDefault image:inputImage];
   if (isiPhone4S) {
        cropController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height*0.45);
    }else{
        cropController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height*0.55);
    }
    cropController.delegate = self;
     if (self.useFullImageSegment.selectedSegmentIndex == 0) {
        [self.ratioLockSegment setSelectedSegmentIndex:0];
         self.lockImageView.image = [UIImage imageNamed:@"lock_icon.png"];
        self.unlockImageView.image = [UIImage imageNamed:@"unlock_icon.png"];
         //cropController.cropView.userInteractionEnabled = NO;
     }else{
         [self.ratioLockSegment setSelectedSegmentIndex:1];
         self.lockImageView.image = [UIImage imageNamed:@"lock_iconH.png"];
        self.unlockImageView.image = [UIImage imageNamed:@"unlock_iconH.png"];
        // cropController.cropView.userInteractionEnabled = YES;
     }
    
   // [self ratioLockedSeg:_ratioLockSegment];
//    self.ratioImageView.image = [UIImage imageNamed:@"lock_icon.png"];
    cropController.cropView.isCustomCropping = NO;
    cropController.cropView.maximumZoomScale = maxZoom;
    // cropController.cropView.userInteractionEnabled = NO;
    cropController.aspectRatioLockEnabled = YES;
    [cropController setAspectRatioSize:aspectRatio animated:YES];
    
    
    [self.view addSubview:cropController.view];
    [self.view insertSubview:self.effectBtn aboveSubview:self->cropController.view];
    self.activity.hidden = YES;
   [self.activity stopAnimating];
}




#pragma mark - getPixelPerInch
-(void)getpixelFromInchWithInputImage:(UIImage *)img{
    CGSize imageWOrH = [self convertInchOrCmToPixel:self.widthTF.text height:self.heightTF.text];
    BOOL isSizeGood =[self checkImageSize:imageWOrH.width :imageWOrH.height andInputImage:img];
    if (isSizeGood) {
         imageProccessingfInfoDic[WIDTH] = [imageProccessingfInfoDic[UNIT] isEqualToString:@"cm"]?self.widthTF.text:[NSString stringWithFormat:@"%.1f",[self.widthTF.text floatValue]*2.54];
               imageProccessingfInfoDic[HEIGHT] = [imageProccessingfInfoDic[UNIT] isEqualToString:@"cm"]?self.heightTF.text:[NSString stringWithFormat:@"%.1f",[self.heightTF.text floatValue]*2.54];
        if (self.useFullImageSegment.selectedSegmentIndex == 0) {
            NSString *zoomFactor = [self checkImageSize:img :CGSizeMake([self.widthTF.text floatValue], [self.heightTF.text floatValue]) type:@""];
            if (![zoomFactor isEqualToString: @""]) {
                [self openCropeerWithCropRectSize:CGSizeMake(0, 0) aspectRatio:CGSizeMake(imageWOrH.width , imageWOrH.height) isCustom:NO image:img zoomFactor:[zoomFactor floatValue]];
                if (self.useFullImageSegment.selectedSegmentIndex == 0) {
                cropController.cropView.userInteractionEnabled = NO;
                }
            }else{
                self.widthTF.text = [NSString stringWithFormat:@"%.1f",defaulImageSize.width];
                self.heightTF.text = [NSString stringWithFormat:@"%.1f",defaulImageSize.height];
                [self showAlertPopUpWithMessage:@"The Zoom factor of image is > 4 is large based on this image, we will reduce it to the maximum recommended size."];
            }
        }else{
            NSString *zoomFactor = [self checkImageSize:img :CGSizeMake([self.widthTF.text floatValue], [self.heightTF.text floatValue]) type:@""];
            [self openCropeerWithCropRectSize:CGSizeMake(imageWOrH.width, imageWOrH.height) aspectRatio:CGSizeMake(0.0, 0.0) isCustom:YES image:img zoomFactor:[zoomFactor floatValue]];
            //cropController.imageCropFrame = CGRectMake(0, 0,imageWOrH.width , imageWOrH.height);
        }
    }
}

#pragma mark - checkImageSize
-(BOOL)checkImageSize:(CGFloat)imageW :(CGFloat)imageH andInputImage:(UIImage *)img {
    if (imageW <MIN_IMAGE_SIZE && imageH<MIN_IMAGE_SIZE) {
        [self showAlertPopUpWithMessage:@"The crop size or Zooming area of image you have selected is of Low Resolution."];
        _activity.hidden = true;
        [_activity stopAnimating];
        onSelectNextButton = false;
        return NO;
    }
    else{
        if (![imageProccessingfInfoDic[@"select_sizes"] isEqualToString:@"CUSTOM"]){
            return YES;
        }
        if ([imageProccessingfInfoDic[@"select_sizes"] isEqualToString:@"CUSTOM"] && !onSelectEffect) {
        if (imageH > img.size.height || imageW > img.size.width) {
            self.widthTF.text = [NSString stringWithFormat:@"%.1f",defaulImageSize.width];
            self.heightTF.text = [NSString stringWithFormat:@"%.1f",defaulImageSize.height];
            [self showAlertPopUpWithMessage:@"The size you have entered is too large based on the quality of this image, we will reduce it to the maximum recommended size."];
            return NO;
        }else{
            return YES;
        }
        }else{
            if (imageH > img.size.height || imageW > img.size.width) {
                self.widthTF.text = [NSString stringWithFormat:@"%.1f",defaulImageSize.width];
                self.heightTF.text = [NSString stringWithFormat:@"%.1f",defaulImageSize.height];
                [self showAlertPopUpWithMessage:@"The size you have entered is too large based on the quality of this image, we will reduce it to the maximum recommended size."];
                return NO;
            }else{
                
                return YES;
            }
        }
    }
}

-(NSString*)checkImageSize:(UIImage*)selectedImage :(CGSize)ratioSelected type:(NSString *)key{
    if ([imageProccessingfInfoDic[UNIT] isEqualToString:@"cm"]) {
        ratioSelected = CGSizeMake(ratioSelected.width *0.393701, ratioSelected.height*0.393701);//covert to inch
    }
    
    float requiredSize,actualImageSize;
    if (selectedImage.size.width >  selectedImage.size.height)
    {
        //    NSLog(@"Select Image is in Landscape Mode ....");
        requiredSize = ratioSelected.width*300;
        actualImageSize = selectedImage.size.width;
    }
    else
    {
        //Portrait and square.
        requiredSize = ratioSelected.height*300;
        actualImageSize = selectedImage.size.height;
    }
    
    float scaleFactor = requiredSize/actualImageSize;
    
    if ((scaleFactor<=4) && ((ratioSelected.height <= maxLimitTobeSelect.height) && (ratioSelected.width <= maxLimitTobeSelect.width))){
        //enableSize
        float zoomFactorPercentage = (4/scaleFactor);//*100
        return [NSString stringWithFormat:@"%.1f", zoomFactorPercentage];
        
    }else{
        //disableSize
        return @"";
    }
}
#pragma mark - TOCropViewController
- (void)cropViewController:(nonnull TOCropViewController *)cropViewController
        didCropImageToRect:(CGRect)cropRect
                     angle:(NSInteger)angle{
    
//    BOOL isCorrect = [self checkImageSize:cropRect.size.width :cropRect.size.height];
//    if (isCorrect) {
//        if ([imageProccessingfInfoDic[@"select_sizes"] isEqualToString:@"CUSTOM"]) {
//            if (cropRect.size.width != 0 && cropRect.size.height != 0) {
//                self.widthTF.text = [self returnRightMetric:[NSString stringWithFormat:@"%.1f",cropRect.size.width] typeOfMetricUsed:@"pixeltocm"];
//                self.heightTF.text = [self returnRightMetric:[NSString stringWithFormat:@"%.1f",cropRect.size.height] typeOfMetricUsed:@"pixeltocm"];
//            }
//        }
//    }
}

- (void)cropViewController:(nonnull TOCropViewController *)cropViewController
            didCropToImage:(nonnull UIImage *)image withRect:(CGRect)cropRect
                     angle:(NSInteger)angle{
    
 
    
    BOOL isCorrect = [self checkImageSize:cropRect.size.width :cropRect.size.height andInputImage:_imageToEdit];
    if (isCorrect) {
        if (onSelectEffect) {
            self.activity.hidden = YES;
            [self.activity stopAnimating];
            onSelectEffect = NO;
            NSMutableArray *arr=[APP_DELEGATE.db getDataForField:USER_PRODUCT where:nil];
            arr[0][TYPE_CHOOSED]    =  imageProccessingfInfoDic[TYPE_CHOOSED];
            arr[0][USE_FULL_IMAGE] =  [NSString stringWithFormat:@"%@",imageProccessingfInfoDic[@"use_full_image"]];
            arr[0][SELECTED_INDEX]  =  [NSString stringWithFormat:@"%ld",(long)selectedIndex.row];
            arr[0][CROPPING_TYPE] =  imageProccessingfInfoDic[@"select_sizes"];
            arr[0][UNIT] =  imageProccessingfInfoDic[@"unit"];
            arr[0][HEIGHT_TF] =  [NSString stringWithFormat:@"%@",imageProccessingfInfoDic[HEIGHT]];
            arr[0][WIDTH_TF] =  [NSString stringWithFormat:@"%@",imageProccessingfInfoDic[WIDTH]];
            arr[0][ZOOM_FACTOR] =  imageProccessingfInfoDic[@"factor"];
            [APP_DELEGATE.db update:USER_PRODUCT :arr[0] :[NSString stringWithFormat:@"id='%@'",arr[0][@"id"]]];
            //imageProccessingfInfoDic[@"imagetoedit"] =  [Global getImageFromDoucumentWithName:@"frame"];
            [self performSegueWithIdentifier:@"filter" sender:nil];
           
        }
        if (onSelectNextButton) {
            onSelectNextButton = NO;
            NSMutableArray *arr=[APP_DELEGATE.db getDataForField:USER_PRODUCT where:nil];
            if (arr.count!=0) {
                if (![imageProccessingfInfoDic[@"select_sizes"] isEqualToString:@"STANDARD"]) {
                    CGSize imageWOrH;
                    float W = [imageProccessingfInfoDic[@"unit"] isEqualToString:@"cm"]?[imageProccessingfInfoDic[WIDTH] floatValue]:[imageProccessingfInfoDic[WIDTH] floatValue]*0.393701;
                    float H = [imageProccessingfInfoDic[@"unit"] isEqualToString:@"cm"]?[imageProccessingfInfoDic[HEIGHT] floatValue]:[imageProccessingfInfoDic[HEIGHT] floatValue]*0.393701;
                    imageWOrH  = [self convertInchOrCmToPixel:[NSString stringWithFormat:@"%.1f",W] height:[NSString stringWithFormat:@"%.1f",H]];
                    [Global saveImagesInDocumentDirectory:UIImagePNGRepresentation(image) ImageId:@"frame"];
                    arr[0][USE_FULL_IMAGE] =  [NSString stringWithFormat:@"%@",imageProccessingfInfoDic[@"use_full_image"]];
                }else{
                    CGSize imageWOrH;
                    float W = [imageProccessingfInfoDic[WIDTH] floatValue];
                    float H = [imageProccessingfInfoDic[HEIGHT] floatValue];
                    imageWOrH  = [self convertInchOrCmToPixel:[NSString stringWithFormat:@"%.1f",W] height:[NSString stringWithFormat:@"%.1f",H]];
                    [Global saveImagesInDocumentDirectory:UIImagePNGRepresentation(image) ImageId:@"frame"];
                }
                arr[0][TYPE_CHOOSED]    =  imageProccessingfInfoDic[TYPE_CHOOSED];
                arr[0][SELECTED_INDEX]  =  [NSString stringWithFormat:@"%ld",(long)selectedIndex.row];
                arr[0][CROPPING_TYPE] =  imageProccessingfInfoDic[@"select_sizes"];
                arr[0][UNIT] =  imageProccessingfInfoDic[@"unit"];
                arr[0][HEIGHT_TF] =  [NSString stringWithFormat:@"%@",imageProccessingfInfoDic[@"height"]];
                arr[0][WIDTH_TF] =  [NSString stringWithFormat:@"%@",imageProccessingfInfoDic[@"width"]];
                arr[0][ZOOM_FACTOR] =  imageProccessingfInfoDic[@"factor"];
                [APP_DELEGATE.db update:USER_PRODUCT :arr[0] :[NSString stringWithFormat:@"id='%@'",arr[0][@"id"]]];
            }
           // self.heightTF.text = @"";
           // self.widthTF.text = @"";
            self.activity.hidden = YES;
            [self.activity stopAnimating];
            
            
                if ([sku isEqualToString:FRAME_SKU] ) {
                    [self performSegueWithIdentifier:@"frame" sender:nil];
//                    [self performSegueWithIdentifier:@"canva" sender:nil];
                    
                }
           else if ([sku isEqualToString:CANVAS_PRINTING_SKU] ) {
                [self performSegueWithIdentifier:@"canva" sender:nil];
//               [self performSegueWithIdentifier:@"canvaCropping" sender:nil];
               
                
           }else if ([sku isEqualToString:ACRYLIC_PHOTOBOOK]){
                   [self performSegueWithIdentifier:@"summaryType2" sender:nil];
           }
                else{
                    [self performSegueWithIdentifier:@"acry" sender:nil];
                }
            
            
            
            
        }
    }
    
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"zoomF"]);
    
}


- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}
- (void)cropViewController:(nonnull TOCropViewController *)cropViewController
    didCropToCircularImage:(nonnull UIImage *)image withRect:(CGRect)cropRect
                     angle:(NSInteger)angle{
}
- (void)updatedCropViewFrame:(CGRect)cropView{
    self.widthTF.text = [self returnRightMetric:[NSString stringWithFormat:@"%.1f",cropView.size.width] typeOfMetricUsed:@"pixeltocm"];
    self.heightTF.text = [self returnRightMetric:[NSString stringWithFormat:@"%.1f",cropView.size.height] typeOfMetricUsed:@"pixeltocm"];
    //    if (cropController.imageCropFrame.size.width != self.finalImage.image.size.width && cropController.imageCropFrame.size.height != self.finalImage.image.size.height) {
    //        [self doneAction];
    //    }
    
}
#pragma mark - Segue support

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"filter"]) {
        FilterViewController *filterViewController = [(UINavigationController*)segue.destinationViewController topViewController];
        filterViewController.imageInfoDic = self->imageProccessingfInfoDic;
        //filterViewController.inputImage = (UIImage*)sender;
        
      
        
        
        
    }
}

//



#pragma mark - UICollectionViewDataSource and Delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return selectedOptionArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"CollectionViewCell";
    UICollectionViewCell *cell =[_collectioView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    NSArray *viewArray = [cell.contentView subviews];
    for (UIView *v in viewArray) {
        [v removeFromSuperview];
    }
    cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, (self.collectioView.frame.size.width/4)-5, 35);
    UILabel *sizeLabel = [[UILabel alloc] init];
    sizeLabel.text = self.standardUnitSegement.selectedSegmentIndex == 0  ? [NSString stringWithFormat:@"%@x%@cm",[self returnRightMetric:[NSString stringWithFormat:@"%@",self->selectedOptionArr[indexPath.row][WIDTH]] typeOfMetricUsed:@"inches"],[self returnRightMetric:[NSString stringWithFormat:@"%@",self->selectedOptionArr[indexPath.row][HEIGHT]] typeOfMetricUsed:@"inches"]]:[NSString stringWithFormat:@"%@\"x%@\"",self->selectedOptionArr[indexPath.row][WIDTH],self->selectedOptionArr[indexPath.row][HEIGHT]];
    sizeLabel.textColor = [UIColor darkGrayColor];//[UIColor colorWithRed:60/255.0 green:69/255.0 blue:143/255.0 alpha:1.0];
    sizeLabel.frame = CGRectMake(0, 0, cell.frame.size.width, cell.frame.size.height);
    if (isiPhone5s) {
        [sizeLabel setFont: [self.portraitBtn.titleLabel.font fontWithSize: 10.0]];
    }else{
        [sizeLabel setFont: [self.portraitBtn.titleLabel.font fontWithSize: 12.0]];
    }
    
    sizeLabel.textAlignment = NSTextAlignmentCenter;
    sizeLabel.adjustsFontSizeToFitWidth = YES;
    [Global roundCorner:cell roundValue:0.0 borderWidth:2.5 borderColor:[UIColor colorWithRed:239/255.0 green:239/255.0 blue:239/255.0 alpha:1.0]];
    sizeLabel.backgroundColor = UIColor.clearColor;
//    UIButton *layerButton = [[UIButton alloc] init];
//    layerButton.frame = cell.frame;
//    layerButton.backgroundColor = UIColor.clearColor;
//    [layerButton addTarget:self action:@selector(onClickDiactivatedSize) forControlEvents:UIControlEventTouchUpInside];
//    [cell.contentView addSubview:layerButton];
    cell.contentView.backgroundColor = UIColor.whiteColor;
    [cell.contentView addSubview:sizeLabel];
    
    NSString *isActive = self->selectedOptionArr[indexPath.row][@"isActive"];
    if ([isActive isEqualToString:@"NO"]) {
        //layerButton.hidden = NO;
        sizeLabel.backgroundColor = [UIColor colorWithWhite:0.5 alpha:0.5];
    }else{
        //layerButton.hidden = YES;
        sizeLabel.backgroundColor = UIColor.clearColor;
    }
    if (_isFromPreview) {
        _isFromPreview = !_isFromPreview;
        sizeLabel.textColor = [UIColor whiteColor];
        cell.contentView.backgroundColor = [UIColor colorWithRed:60/255.0 green:69/255.0 blue:143/255.0 alpha:1.0];
        cell.contentView.clipsToBounds = YES;
//        if([sku isEqualToString:CANVAS_PRINTING_SKU]){
//            if ([imageProccessingfInfoDic[@"type_choosed"] isEqualToString:@"Square"]) {
//                selectedIndex = [NSIndexPath indexPathForRow:2 inSection:indexPath.section];
//            }else{
//                selectedIndex = [NSIndexPath indexPathForRow:3 inSection:indexPath.section];
//            }
//        }else{
        selectedIndex = indexPath;
//        }
    }
    if (indexPath.row == selectedOptionArr.count-1) {
        if (selectedIndex != nil) {
            [self collectionView:self.collectioView didSelectItemAtIndexPath:selectedIndex];
            [self highlightedIndexCell:selectedIndex];
            if (indexPath.row == selectedIndex.row) {
                sizeLabel.textColor = [UIColor whiteColor];
                cell.contentView.backgroundColor = [UIColor colorWithRed:60/255.0 green:69/255.0 blue:143/255.0 alpha:1.0];
                cell.contentView.clipsToBounds = YES;
            }
        }
    }
    
    return cell;
}
-(void)onClickDiactivatedSize{
    [self showAlertPopUpWithMessage:@"This size is not available as the image selected is not high enough quality, please choose from the available sizes."];
}
- (CGSize)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake((self.collectioView.frame.size.width/4)-5, 35);
}

- (void)collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    NSString *isActive = self->selectedOptionArr[indexPath.row][@"isActive"];
    if ([isActive isEqualToString:@"YES"]) {
        UICollectionViewCell *preSelectedCell = [_collectioView cellForItemAtIndexPath:highLightedIndex];
        NSArray *preSelectedCellArray = [preSelectedCell.contentView subviews];
        for (UILabel *label in preSelectedCellArray) {
            
            label.textColor = [UIColor darkGrayColor];
            preSelectedCell.contentView.backgroundColor = UIColor.whiteColor;
            
        }
        
        UICollectionViewCell *cell = [_collectioView cellForItemAtIndexPath:indexPath];
        NSArray *viewArray = [cell.contentView subviews];
        for (UILabel *label in viewArray) {
            label.textColor = [UIColor whiteColor];
        }
        cell.contentView.backgroundColor = [UIColor colorWithRed:60/255.0 green:69/255.0 blue:143/255.0 alpha:1.0];
        cell.contentView.clipsToBounds = YES;
        highLightedIndex = indexPath;
    }
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [[NSUserDefaults standardUserDefaults] setValue:@"0" forKey:@"zoomF"];
    if (indexPath.row < selectedOptionArr.count) {
        [[NSUserDefaults standardUserDefaults] setValue:self->selectedOptionArr[indexPath.row][ZOOMFACTOR] forKey:@"maxZoomF"];
    }
   
   
    if (indexPath.row >self->selectedOptionArr.count-1) {
        [self collectionView:self.collectioView didSelectItemAtIndexPath:[NSIndexPath indexPathForRow:self->selectedOptionArr.count-1 inSection:0]];
        return;
    }
    NSString *isActive = self->selectedOptionArr[indexPath.row][@"isActive"];
    if ([isActive isEqualToString:@"YES"]) {
        selectedIndex = indexPath;
        float W = [self->selectedOptionArr[indexPath.row][WIDTH] floatValue];
        float H = [self->selectedOptionArr[indexPath.row][HEIGHT] floatValue];
        imageProccessingfInfoDic[WIDTH] = [NSString stringWithFormat:@"%.1f",W*2.54] ;
        imageProccessingfInfoDic[HEIGHT] = [NSString stringWithFormat:@"%.1f",H*2.54];
        imageProccessingfInfoDic[ZOOMFACTOR] = self->selectedOptionArr[indexPath.row][ZOOMFACTOR];
        [self openCropeerWithCropRectSize:CGSizeMake(0, 0) aspectRatio:CGSizeMake(W, H) isCustom:NO image:_imageToEdit zoomFactor:[self->selectedOptionArr[indexPath.row][ZOOMFACTOR] floatValue]];
    }else{
//        if (!(imageProccessingfInfoDic[@"isSeletingMinSize"])) {
//            [self onClickDiactivatedSize];
//        }else{
        for (int i = 1; i<= self->selectedOptionArr.count; i++) {
            
            selectedIndex = [NSIndexPath indexPathForRow:indexPath.row-i inSection:indexPath.section];
           
            if (selectedIndex.row >= 0 && selectedIndex.row != NSNotFound) {
                //     NSLog(@"%@,%@",selectedIndex,indexPath);
                NSString *isActive = self->selectedOptionArr[selectedIndex.row][@"isActive"];
                if ([isActive isEqualToString:@"YES"]) {
                    if (selectedIndex == nil) {
                        return;
                    }
                    float W = [self->selectedOptionArr[selectedIndex.row][WIDTH] floatValue];
                    float H = [self->selectedOptionArr[selectedIndex.row][HEIGHT] floatValue];
                    imageProccessingfInfoDic[@"isSeletingMinSize"] = @YES;
                    imageProccessingfInfoDic[WIDTH] = [NSString stringWithFormat:@"%.1f",W*2.54];
                    imageProccessingfInfoDic[HEIGHT] = [NSString stringWithFormat:@"%.1f",H*2.54];
                    imageProccessingfInfoDic[ZOOMFACTOR] = self->selectedOptionArr[selectedIndex.row][ZOOMFACTOR];
                    [self openCropeerWithCropRectSize:CGSizeMake(0, 0) aspectRatio:CGSizeMake(W, H) isCustom:NO image:_imageToEdit zoomFactor:[self->selectedOptionArr[selectedIndex.row][ZOOMFACTOR] floatValue]];
                    [self collectionView:self.collectioView didHighlightItemAtIndexPath:selectedIndex];
                    break;
                }
            }
            
//        }
        }
    }
}
-(void)highlightedIndexCell:(NSIndexPath*)indexPath{
    if (indexPath.row >self->selectedOptionArr.count-1) {
        return;
    }
    NSString *isActive = self->selectedOptionArr[indexPath.row][@"isActive"];
    if ([isActive isEqualToString:@"YES"]) {
        UICollectionViewCell *cell = [_collectioView cellForItemAtIndexPath:indexPath];
        NSArray *viewArray = [cell.contentView subviews];
        for (UILabel *label in viewArray) {
            label.textColor = [UIColor whiteColor];
        }
        cell.contentView.backgroundColor = [UIColor colorWithRed:60/255.0 green:69/255.0 blue:143/255.0 alpha:1.0];
        cell.contentView.clipsToBounds = YES;
        highLightedIndex = indexPath;
    }
}
- (IBAction)useFullImageSeg:(id)sender {
    self.customUnitSegment.selected = !self.customUnitSegment.selected;
    if (self.useFullImageSegment.selectedSegmentIndex == 0) {
        [self onClickUseFullImage:[UIButton new]];
    }else{
        [self onNoUseFullImage:[UIButton new]];
    }
}
- (IBAction)ratioLockedSeg:(id)sender {
    if (self.ratioLockSegment.selectedSegmentIndex == 0) {
     //   [self.ratioLockSegment setImage:[UIImage imageNamed:@"image.png"] forSegmentAtIndex:self.ratioLockSegment.selectedSegmentIndex];
        cropController.aspectRatioLockEnabled = YES;
    }else{
        cropController.aspectRatioLockEnabled = NO;
    }
}
//for_custom_unitSelectSeg
- (IBAction)unitSelectSeg:(id)sender {
    self.customUnitSegment.selected = !self.customUnitSegment.selected;
    if (self.customUnitSegment.selectedSegmentIndex == 1) {
        [[NSUserDefaults standardUserDefaults] setObject:@"inch" forKey:TOGGLE_UNIT];
        [self onClickInch:[UIButton new] andInputImage:_imageToEdit];
        
    }else{
        [[NSUserDefaults standardUserDefaults] setObject:@"cm" forKey:TOGGLE_UNIT];
        [self onClickCm:[UIButton new] andInputImage:_imageToEdit];
    }
    
}

- (IBAction)standardUnitSeg:(id)sender {
    if (self.standardUnitSegement.selectedSegmentIndex == 1) {
        [[NSUserDefaults standardUserDefaults] setObject:@"inch" forKey:TOGGLE_UNIT];
        [self onClickInch:[UIButton new] andInputImage:_imageToEdit];
    }else {
        
        [[NSUserDefaults standardUserDefaults] setObject:@"cm" forKey:TOGGLE_UNIT];
        [self onClickCm:[UIButton new] andInputImage:_imageToEdit];
    }
    self.standardUnitSegement.selected = !self.standardUnitSegement.selected;
}
@end
