//
//  SceneDelegate.h
//  TabPlay
//
//  Created by Saurabh anand on 23/01/20.
//  Copyright © 2020 Saurabh Anand. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

