//
//  ViewController.m
//  TabPlay
//
//  Created by Saurabh anand on 23/01/20.
//  Copyright © 2020 Saurabh Anand. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTabBar];
}

-(void)setTabBar{
  self.tabBarController.tabBar.barTintColor =  [UIColor colorWithRed:0.376 green:0.729 blue:0.318 alpha:1.000];
}

@end
