//
//  FilterViewController.m
//  FrameShop
//
//  Created by Saurabh anand on 16/12/19.
//  Copyright © 2019 Mayank Barnwal. All rights reserved.
//

#import "FilterViewController.h"
#import "VFilterCollectionViewCell.h"
@interface FilterViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>{

    CIImage *inputImageCIImage,*smallIconImageCIImage;
    NSArray *filterNameList,*filterDisplayNameList;
    CIContext *context;
    int filterIndex;
    UIImage *smallIconImage;
    UIImage *filteredImage;

    VFilterCollectionViewCell *selectCells;
}
@end
@implementation FilterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
  
    self.mainLoader.hidden = YES;
    [Global roundCorner:self.applyFilterBtn roundValue:self.applyFilterBtn.frame.size.height/2 borderWidth:0.0 borderColor:[UIColor whiteColor]];
    [Global roundCorner:self.dismissBtn roundValue:self.dismissBtn.frame.size.height/2 borderWidth:0.1 borderColor:[UIColor colorWithRed:60/255.0 green:69/255.0 blue:143/255.0 alpha:1.0]];
    _activity.hidden=YES;
    self.collectionView.delegate =  self;
    self.collectionView.dataSource = self;
  
    filterNameList = [NSArray arrayWithObjects:
    @"No Filter",
    @"CIPhotoEffectChrome",
    @"CIPhotoEffectFade",
    @"CIPhotoEffectInstant",
    @"CIPhotoEffectMono",
    @"CIPhotoEffectProcess",
    @"CIPhotoEffectTonal",
    @"CIPhotoEffectTransfer",
    @"CISepiaTone", nil];
    filterDisplayNameList = [NSArray arrayWithObjects:
    @"Normal",
    @"Chrome",
    @"Fade",
    @"Instant",
    @"Mono",
    @"Process",
    @"Tonal",
    @"Transfer",
    @"Sepia", nil];
   
    context = [[CIContext alloc] initWithOptions:nil];
    
    UINib * nib = [UINib nibWithNibName:@"VFilterCollectionViewCell" bundle:nil];
    [_collectionView registerNib:nib forCellWithReuseIdentifier:@"VFilterCollectionViewCell"];
    
   
    
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    BG_THREAD
    self->_inputImage =  [Global getImageFromDoucumentWithName:@"frame"];
    MAIN_THREAD
    [self setUpView];
    END_BLOCK
    END_BLOCK
    
    
    
  
  
}

-(void)setUpView{
   
    float width,height, ratio;
    
    
    
    
    ratio=_inputImage.size.width/_inputImage.size.height;
    
    if (_inputImage.size.width>=_inputImage.size.height) {
     
        width=self.view.bounds.size.width-20;
        height=width/ratio;
    }
    else {
        height=self.view.bounds.size.height*0.6;
        width=ratio*height;
        
    }
   
    
    _imagePreview.frame=CGRectMake((self.view.bounds.size.width/2)-(width/2), (
                                                                               (self.navigationController.navigationBar.frame.size.height ?: 0.0)), width, height);
    _imagePreview.image = [Global getMidiumSizeImage:_inputImage imageSize:(width>height)?width:height];
    
 
    
    smallIconImage = [Global getMidiumSizeImage:_inputImage imageSize:80];
    
  
    
    smallIconImageCIImage = [[CIImage alloc] initWithImage:smallIconImage];
    
//    _imagePreview.image =[Global getMidiumSizeImage:_inputImage imageSize:_imagePreview.frame.size.height>_imagePreview.frame.size.width?_imagePreview.frame.size.height:_imagePreview.frame.size.width];
    
    inputImageCIImage = [[CIImage alloc] initWithImage:_imagePreview.image];
    
    
    if (self.imageInfoDic[@"index"] == nil) {
        filterIndex = 0;
    }else{
        filterIndex = [self.imageInfoDic[@"index"] intValue];
        
        [self applyFilterOnOriginalImage:inputImageCIImage];
        
    }
    
    _mainLoader.center = _imagePreview.center;
    
    _collectionView.frame = CGRectMake(0, _imagePreview.frame.origin.y+height+10, self.view.frame.size.width, 100);
    selectCells = nil;
    [_collectionView reloadData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    
    //[self.collectionView reloadData];
    [self.collectionView  scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:filterIndex inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
}
- (IBAction)closeBtnAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];

}

- (IBAction)nextBtnAction:(id)sender {
    self.mainLoader.hidden = NO;
    [self.mainLoader startAnimating];
  
    self.view.userInteractionEnabled = NO;
    _activity.hidden=false;
    [_activity startAnimating];
    _nextBtn.enabled=false;
    _nextBtn.alpha=0.1f;
//    @autoreleasepool{
//    CIImage * test = [[CIImage alloc] initWithImage:self->_inputImage];
//    test = nil;
//
//    }
    

    BG_THREAD
    
   
    self->filteredImage = [self createFilterFilterName:self->filterNameList[self->filterIndex] ciimageRef:[[CIImage alloc] initWithImage:self->_inputImage]];
    MAIN_THREAD
    if (self->filteredImage == nil && self->filterIndex == 0) {
        self->filteredImage = self->_inputImage;
    }
    self.imageInfoDic[@"index"] = [NSString stringWithFormat:@"%d",self->filterIndex];
    //sas 10
   // [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:self.imageInfoDic[@"index"]];
  //  UIImage *temImg=self.filterView.processedImage;
  
               self->_activity.hidden=true;
               [self->_activity stopAnimating];
               self->_nextBtn.enabled=true;
              // self->_nextBtn.alpha=1.0f;
               self.view.userInteractionEnabled = YES;
               self.mainLoader.hidden = YES;
               [self.mainLoader stopAnimating];
    
  
               [self dismissViewControllerAnimated:YES completion:^{
                   NSMutableDictionary * userInfo = [[NSMutableDictionary alloc] init];
                   userInfo[@"image"]= self->filteredImage;
                   userInfo[@"effectName"] = self.imageInfoDic[@"effectName"];
                   userInfo[WIDTH] = self.imageInfoDic[WIDTH];
                   userInfo[HEIGHT] = self.imageInfoDic[HEIGHT];
                   userInfo[@"index"] = self.imageInfoDic[@"index"];
                   userInfo[ZOOMFACTOR] = self.imageInfoDic[ZOOMFACTOR];
                   NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
                   [nc postNotificationName:@"show_effect" object:self userInfo:userInfo];
               }];
    END_BLOCK
    END_BLOCK
    
   
    
    
   
    
  
      
    
}



- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return (smallIconImage != nil)?filterNameList.count:0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    static NSString *identifier = @"VFilterCollectionViewCell";
    
    VFilterCollectionViewCell *cells =[_collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    UIImage *imageToshow = smallIconImage;
    if (indexPath.row != 0) {
        imageToshow = [self createFilterFilterName:filterNameList[indexPath.row] ciimageRef:smallIconImageCIImage];
    }
    cells.iconImg.image = imageToshow;
    cells.iconImg.contentMode = UIViewContentModeScaleAspectFill;
    cells.name.text = filterDisplayNameList[indexPath.row];

  
    if (filterIndex == indexPath.row) {
        cells.backgroundColor = APP_BLUE_COLOR;
        
    }else{
        cells.backgroundColor = UIColor.clearColor;
    }
    if (selectCells == nil) {
        selectCells = cells;
    }
    return cells;
}

- (CGSize)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(80, self.collectionView.frame.size.height);
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
   
    if (indexPath.row == filterIndex) {
        return;
    }
    self.mainLoader.hidden = NO;
    [self.mainLoader startAnimating];
    
    selectCells.backgroundColor = UIColor.clearColor;
    selectCells =(VFilterCollectionViewCell *) [_collectionView cellForItemAtIndexPath:indexPath];
    selectCells.backgroundColor = APP_BLUE_COLOR;
    
    filterIndex = (int)indexPath.row;
    if (filterIndex != 0) {
        [self applyFilterOnOriginalImage:inputImageCIImage];
    }else{
        self.imagePreview.image =_imagePreview.image = [Global getMidiumSizeImage:_inputImage imageSize:(_imagePreview.frame.size.width>_imagePreview.frame.size.height)?_imagePreview.frame.size.width:_imagePreview.frame.size.height];
        self.mainLoader.hidden = YES;
        [self.mainLoader stopAnimating];

    }

}

- (UIImage *)resizeimage:(UIImage *)image
{
    float ratio = 0.2;
    CGSize resizeSize = CGSizeMake(image.size.width*ratio, image.size.height*ratio);
    UIGraphicsBeginImageContext(resizeSize);
//    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0);
    [image drawInRect:CGRectMake(0, 0, resizeSize.width, resizeSize.height)];
    UIImage *resizeImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return resizeImage;
}

- (UIImage *)createFilterFilterName:(NSString*)name ciimageRef:(CIImage *)imageRef{
   
    
    CIImage *inputImage;
//    if (isOriginalImage) {
//        inputImage = inputImageCIImage;
//    }else{
//        inputImage = smallIconImageCIImage;
//    }
    
    inputImage = imageRef;
    
    CIFilter *myFilter = [CIFilter filterWithName:name keysAndValues:@"inputImage",inputImage, nil];
    
    
    
    CIImage *filteredImage = [myFilter outputImage];
    
    CGRect extent = [filteredImage extent];

        CGImageRef cgImage = [context createCGImage:filteredImage fromRect:extent];

        UIImage *resultImage = [[UIImage alloc] initWithCGImage:cgImage];
    cgImage = nil;
    inputImage = nil;
    filteredImage=nil;
   
    [self->context clearCaches];
    
    
        return resultImage;
    
   
   
}

- (void)applyFilterOnOriginalImage:(CIImage *)imgRef{
    NSString *filterName = filterNameList[filterIndex];
    filteredImage = [self createFilterFilterName:filterName ciimageRef:imgRef];
    if (filteredImage!=nil) {
        self.imagePreview.image = [Global getMidiumSizeImage:filteredImage imageSize:_imagePreview.frame.size.height>_imagePreview.frame.size.width?_imagePreview.frame.size.height:_imagePreview.frame.size.width];
    }
    
    self.mainLoader.hidden = YES;
    [self.mainLoader stopAnimating];
}


@end
