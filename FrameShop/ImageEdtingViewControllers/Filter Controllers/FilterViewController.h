//
//  FilterViewController.h
//  FrameShop
//
//  Created by Saurabh anand on 16/12/19.
//  Copyright © 2019 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>


#import "Global.h"
#import "AlbumCollectionViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface FilterViewController : UIViewController

@property UIImage *inputImage;
@property NSMutableDictionary *imageInfoDic;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activity;
@property (weak, nonatomic) IBOutlet UIButton *nextBtn;
@property (weak, nonatomic) IBOutlet UIImageView *imagePreview;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (weak, nonatomic) IBOutlet UIButton *dismissBtn;
@property (weak, nonatomic) IBOutlet UIButton *applyFilterBtn;
- (IBAction)nextBtnAction:(id)sender;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *mainLoader;

@end

NS_ASSUME_NONNULL_END
