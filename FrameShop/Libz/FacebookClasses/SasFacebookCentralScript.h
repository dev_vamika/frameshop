//
//  SasFacebookCentralScript.h
//  PicMe
//
//  Created by Saurabh Anand Srivastava on 18/07/17.
//  Copyright © 2017 Saurabh Anand Srivastava. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "Global.h"
@class SasFacebookCentralScript;
@protocol SasFacebookCentralScriptDelegate <NSObject>
    @optional
    
-(void)fbLogInSuccessFulWithInfo:(NSMutableDictionary *)infoDic ;
-(void)fbLogInFailsFulWithError:(NSString *)              error ;
-(void)fbLogInCancel ;
-(void)fbRequestSuccessWithData:(NSMutableArray *)dataArray ;
-(void)fbRequestFailsWithError:(NSString *)error ;


    
@end



@interface SasFacebookCentralScript : NSObject
@property(strong,nonatomic)id<SasFacebookCentralScriptDelegate> delegate;
    
-(void)loginWithFacebook;
//-(void)getFriendList;
//-(void)sendAppLinkToFriends :(UIViewController *)baseController;
-(void) fechUserDataUsingFacebook;
- (BOOL)isLogin;
-(void)logOutFromFb;
-(void)getFBUserAlbums;
-(void)getAlbumCoverPicWhereAlbumId:(NSString *)albumId;
-(void) getFbPhotosOfAlbumWithAlbumId:(NSString*) albumId;
-(void)getFbLocationDicFromImageWhereImageId:(NSString *)imageId;
@end
