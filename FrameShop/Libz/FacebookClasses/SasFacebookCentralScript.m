//
//  SasFacebookCentralScript.m
//  PicMe
//
//  Created by Saurabh Anand Srivastava on 18/07/17.
//  Copyright © 2017 Saurabh Anand Srivastava. All rights reserved.
//

#import "SasFacebookCentralScript.h"

@implementation SasFacebookCentralScript

    
-(void)loginWithFacebook{
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logOut];
    [login logInWithPermissions:@[@"public_profile",@"email",@"user_photos"]
                 fromViewController:nil
                            handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                                if (error) {
                                    [self.delegate fbLogInFailsFulWithError:error.description];
                                } else if (result.isCancelled) {
                                    [self.delegate fbLogInCancel];
                                } else {
                                    [self performSelector:@selector(fechUserDataUsingFacebook) withObject:self afterDelay:0.5 ];
                                }
                            }];
    }
    



- (BOOL)isLogin
{
    
    BOOL isLoggedIn = NO;
    if ([FBSDKAccessToken currentAccessToken].tokenString){

        isLoggedIn = YES;
    }
    
    return isLoggedIn;
}

-(void)logOutFromFb{
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logOut];
}
    
#pragma mark - Facebook
-(void) fechUserDataUsingFacebook
    {
        NSMutableDictionary *infoDic=[[NSMutableDictionary alloc] init];
        if ([FBSDKAccessToken currentAccessToken]) {
            [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me?fields=email,name,picture.width(300),first_name,last_name" parameters:@{}]
             startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                 if (!error) {
                     NSMutableDictionary *tempUserDic = (NSMutableDictionary*) result;
                     if ([tempUserDic[@"email"] length]==0) {
                         infoDic[@"email"]=@"NA";
                     }
                     else{
                         infoDic[@"email"]=tempUserDic[@"email"];
                     }
                     infoDic[@"name"]       =tempUserDic[@"name"];
                     infoDic[@"profilePic"] =tempUserDic[@"picture"][@"data"][@"url"];
                     infoDic[@"gender"]     =tempUserDic[@"gender"];
                     infoDic[@"id"]         =tempUserDic[@"id"];
                     infoDic[@"dob"]        =tempUserDic[@"birthday"];
                     infoDic[@"fName"]      =tempUserDic[@"first_name"];
                     infoDic[@"lName"]      =tempUserDic[@"last_name"];
                     //[self getFriendList];
                     [self.delegate fbLogInSuccessFulWithInfo:infoDic];
                 }
                 else
                 {
                     FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
                     [login logOut];
                     [self.delegate fbLogInFailsFulWithError:error.description];
                 }
             }];
        }
    }

//-(void)getFriendList{
//
//    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me/taggable_friends" parameters:nil]
//     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
//         if (!error) {
//
//
//
//
//
//         }
//         else
//         {
//
//             FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
//             [login logOut];
//             [self.delegate fbLogInFailsFulWithError:error.description];
//
//
//
//
//         }
//     }];
//
//
//
//    }

    
//    -(void)sendAppLinkToFriends :(UIViewController *)baseController{
////        FBSDKAppInviteContent *content =[[FBSDKAppInviteContent alloc] init];
////        content.appLinkURL = [NSURL URLWithString:APP_FB_INVITE_LINK];
////        //optionally set previewImageURL
////         content.appInvitePreviewImageURL = [NSURL URLWithString:@"https://s-media-cache-ak0.pinimg.com/736x/9b/a2/57/9ba25796112cad616be27e473ae1e149--kids-cartoon-characters-childhood-characters.jpg"];
////
////        // Present the dialog. Assumes self is a view controller
////        // which implements the protocol `FBSDKAppInviteDialogDelegate`.
////        [FBSDKAppInviteDialog showFromViewController:baseController
////                                         withContent:content
////                                            delegate:nil];
//
//    }
//



-(void)getFBUserAlbums{
    
    
    NSMutableArray *albumArr=[[NSMutableArray alloc] init];

    FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
                                  initWithGraphPath:@"/me"
                                  parameters:@{ @"fields": @"albums{cover_photo,count,id,name}",}
                                  HTTPMethod:@"GET"];
    [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                          id result,
                                          NSError *error)
     {
         // Handle the result
         
       
         
         if (result)
         {
             NSDictionary *albumDic = result[@"albums"];
             NSArray *tmpFirstArr = (NSArray*)albumDic[@"data"];
             
             // filter blank album
             __block NSMutableArray  *tmpArr = [NSMutableArray new];
             for (NSDictionary *tDict in tmpFirstArr) {
                 if ([tDict objectForKey:@"cover_photo"]) {
                     [tmpArr addObject:tDict];
                 }
             }
           
            
             for (NSDictionary   *tmpDict in tmpArr) {
                 // create temp
                 NSMutableDictionary    *groupDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                                      [NSNull null],@"url",
                                                      [NSNull null ],@"image",
                                                      [tmpDict objectForKey:@"name"],@"name",
                                                      [tmpDict objectForKey:@"id"],@"id",
                                                      [NSNumber numberWithInt:0],@"count",
                                                      [NSNumber numberWithBool:NO],@"frame",
                                                      [NSNumber numberWithBool:YES],@"frame_applied",
                                                      [NSNumber numberWithBool:YES],@"count_applied",[tmpDict objectForKey:@"count"],@"count", nil];
                 
                 
                 [albumArr addObject:groupDict];
                
             }
             
             [self.delegate fbRequestSuccessWithData:albumArr];
             
             
             
             
         }
         else{
             [self.delegate fbRequestFailsWithError:error.description];
         }
}];
}


-(void)getAlbumCoverPicWhereAlbumId:(NSString *)albumId{
    
    
//    NSDictionary *params = @{
//                             @"type": @"thumbnail",
//                             };
//    /* make the API call */
//    FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
//                                  initWithGraphPath:[NSString stringWithFormat:@"/%@/picture?type=album",albumId]
//                                  parameters:nil
//                                  HTTPMethod:@"GET"];
//    [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
//                                          id result,
//                                          NSError *error) {
//        // Handle the result
//        if (!error) {
//            result;
//        }
//
//
//
//
//
//
//
//
//
//
//
//
//    }];
    
    [self sendGetValue:albumId];
    
    
    
    
    
    
    
    

}
//111931655544980
-(void)sendGetValue:(NSString*)albumId{
    NSString    *access_token = [FBSDKAccessToken currentAccessToken].tokenString;
    NSString    *url = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=album&sdk=ios&format=json&access_token=%@",albumId,access_token];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    [request setCachePolicy:NSURLRequestUseProtocolCachePolicy];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"GET"];
    [request setHTTPBody:[@"" dataUsingEncoding:NSUTF8StringEncoding]];
    //NSURLResponse * response = nil;
   // NSError * error = nil;
  //  NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    
    //sas 10 comment it if not working
    
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:[NSURL URLWithString:url]
              completionHandler:^(NSData *data,
                                  NSURLResponse *response,
                                  NSError *error) {
        if (!error) {
            if (data==nil) {
                 dispatch_async(dispatch_get_main_queue(), ^{
                            [self.delegate fbRequestFailsWithError:nil];
                        });
                 
             }
             else{
                 NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
                 dic[@"imageData"]=data;
                 NSMutableArray *arr=[[NSMutableArray alloc] init];
                 [arr addObject:dic];
                dispatch_async(dispatch_get_main_queue(), ^{
                      [self.delegate fbRequestSuccessWithData:arr];
                 });
                
             }
        }

      }] resume];
    
    
    
   
    
//uncomment it if not working sas 10
    
//    if (data==nil) {
//        dispatch_async(dispatch_get_main_queue(), ^{
//                   [self.delegate fbRequestFailsWithError:nil];
//               });
//
//    }
//    else{
//        NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
//        dic[@"imageData"]=data;
//        NSMutableArray *arr=[[NSMutableArray alloc] init];
//        [arr addObject:dic];
//       dispatch_async(dispatch_get_main_queue(), ^{
//             [self.delegate fbRequestSuccessWithData:arr];
//        });
//
//    }
    
}
- (NSData *)sendSynchronousRequest:(NSURLRequest *)request returningResponse:(NSURLResponse **)response error:(NSError **)error
{

    NSError __block *err = NULL;
    NSData __block *data;
    BOOL __block reqProcessed = false;
    NSURLResponse __block *resp;

    [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData * _Nullable _data, NSURLResponse * _Nullable _response, NSError * _Nullable _error) {
        resp = _response;
        err = _error;
        data = _data;
        reqProcessed = true;
    }] resume];

    while (!reqProcessed) {
        [NSThread sleepForTimeInterval:0];
    }

    *response = resp;
    *error = err;
    return data;
}
-(void) getFbPhotosOfAlbumWithAlbumId:(NSString*) albumId
{
    NSMutableArray *arr=[[NSMutableArray alloc] init];
    
    //@{ @"fields": @"albums{cover_photo,count,id,name}",} 2436416679766911/photos?fields=picture
    FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
                                  initWithGraphPath:[NSString stringWithFormat:@"/%@/photos?fields=picture,source,place,created_time,images&limit=1000",albumId]
                                  parameters:@{}
                                  HTTPMethod:@"GET"];
    [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                          id result,
                                          NSError *error)
     {
         // Handle the result
       
        
         if (!error)
         {
       
             for (int i=0; i<[result[@"data"] count]; i++) {
                 NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
                 dic[@"id"]=result[@"data"] [i][@"id"];
                 dic[@"thumb"]=result[@"data"] [i][@"picture"];
                 dic[@"image"]=result[@"data"] [i][@"images"][0][@"source"];
                 dic[@"date"]=result[@"data"] [i][@"created_time"];
                 dic[@"place"]= @"" ;
                 dic[@"lat"]  =  @"";
                 dic[@"longi"]= @"";
                 
                 
                 
                 if (result[@"data"][i][@"place"][@"location"]!=nil) {
                     dic[@"place"]= [NSString stringWithFormat:@"%@ %@",result[@"data"][i][@"place"][@"location"][@"city"],result[@"data"][i][@"place"][@"location"][@"country"]] ;
                     dic[@"lat"]  =  result[@"data"][i][@"place"][@"location"][@"latitude"];
                     dic[@"longi"]= result[@"data"][i][@"place"][@"location"][@"longitude"];
                 }
                
                 if (dic[@"date"]!=nil) {
                     NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
                     [dateFormatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH:mm:ssZ"];
                     NSDate* date = [dateFormatter dateFromString:dic[@"date"]];
                     
                     NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                     [dateFormat setDateFormat:@"dd-MMM-YYYY  HH:mm:aa"];
                     
                     dic[@"date"]=[NSString stringWithFormat:@"%@",[dateFormat stringFromDate:date]];
                     
                 }
                 
                
                 
                 
               //  [self locationDicFromImageWhereImageId:result[@"data"] [i][@"id"]];
                 [arr addObject:dic];
                 
                 
             }
             
             [self.delegate fbRequestSuccessWithData:arr];
             
         }
         else{
             [self.delegate fbRequestFailsWithError:error.description];
         }
             
         
     }];
    
    
}


-(void)getFbLocationDicFromImageWhereImageId:(NSString *)imageId{
    FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
                                  initWithGraphPath:[NSString stringWithFormat:@"/%@?fields=place",imageId]
                                  parameters:@{}
                                  HTTPMethod:@"GET"];
    [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                          id result,
                                          NSError *error)
     {
         // Handle the result
         
         
         if (!error)
         {
             
             NSLog(@"%@",result[@"place"][@"location"]);
             
         }
         else{
             NSLog(@"%@",error.description);
         }
         
         
     }];
    
    
}


    
@end
