//
//  Validation.h
//
//
//  Created by Saurabh Anand on 5/21/14.
//  Copyright (c) 2014 Synch Soft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Validation : NSObject
+(BOOL)isValidEmail:(NSString *)email;
+(BOOL)isValidText:(NSString *)text;
+(BOOL)isValidNumber:(NSString *)number;
+(BOOL)isPassMatch:(NSString *)pass conpass:(NSString *)conpass;
+(BOOL)isMonth:(NSString *)month;
+(BOOL)isDay:(NSString *)day;
+(BOOL)isYear:(NSString *)year;
+(BOOL)isPass:(NSString *)pass;

+(void)showToast:(NSString *)msg viewController:(UIViewController *)cont;
+(BOOL)isValidDate:(NSDate *)date;
+(BOOL)isPastDate:(NSDate *)date;
-(void)addLoader:(UIView *)mainView height:(float)height width:(float)width activity :(BOOL)isYes;
+(BOOL)isValidPhoneNumber:(NSString *)number;
+(BOOL)isValidPostal:(NSString *)number;
-(void)removeLoader;
+(void)setValidationMarkToTextField:(UITextField *)textField isCorrect:(BOOL)isCorrect;
+(BOOL)isValidTextName:(NSString *)text;
@end
