//
//  DbManager.h
//  Skill Grok
//
//  Created by experience on 5/22/14.
//  Copyright (c) 2014 Alcanzar Soft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "Global.h"

#define USERTABLE                              @"User"
#define PRODUCT_TABLE                          @"Products"

#define FRAME_TABLE                            @"Frames"
#define FRAME_CATEGORIES                       @"FramesCategories"

#define METABOARD_CATEGORIES                  @"MetaboardCategories"
#define METABOARD_TABLE                       @"Metaboards"
#define USER_PRODUCT                          @"UserProduct"
#define GIFTCARD_TABLE                        @"GiftCards"
#define ADDRESS_TABLE                        @"ShippingAddress"
#define CART_TABLE                            @"Cart"
//UserProduct keys
#define HEIGHT_TF           @"height_tf"
#define WIDTH_TF            @"width_tf"
#define USE_FULL_IMAGE      @"use_full_image"
#define CROPPING_TYPE       @"cropping_type"//cropping_type
#define UNIT                @"unit"
#define IMAGE               @"image"
#define FILTER_APPLIED      @"filter_applied"
#define FILTERED_IMAGE      @"filtered_image"
#define SELECTED_SIZE       @"selected_size"
#define TYPE_CHOOSED        @"type_choosed"
#define ZOOM_FACTOR         @"zoom_factor"
#define SELECTED_INDEX      @"selected_index"


@class DbManager;

@protocol DbManagerDelegate <NSObject>

@optional
-(void)successFullyGetData:(NSMutableArray *)Array;


-(void)showPopUp;
@end

@interface DbManager : NSObject
{
    sqlite3 *db;
}



@property (nonatomic, strong) id<DbManagerDelegate> delegate;
-(void) createDatabase:(NSFileManager *)manager;
-(void)insertIntoTable: (NSDictionary *)data table_name:(NSString *)tableName;
-(void)delTableWithTableName:(NSString *)table;
-(void) update:(NSString*)table_name : (NSDictionary *) data :(NSString*) whereClause;
-(NSMutableArray *)getDataForField:(NSString*)tableName where:(NSString *)whereClause;
-(void)delTableWithTableName:(NSString *)table where:(NSString *)where;
-(void) alterTable:(NSString*) tableName Colomn:(NSString*) colomnName DefaultValue:(NSString*) defaultValue;
-(NSMutableArray *)getDataForFieldWhereTableName:(NSString*)tableName andQuery:(NSString *)whereClause;
-(NSMutableArray *)getDataForFieldWithQuery:(NSString *)query;
@end
