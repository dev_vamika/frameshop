//
//  DbManager.m
//  Skill Grok
//
//  Created by experience on 5/22/14.
//  Copyright (c) 2014 Alcanzar Soft. All rights reserved.
//

#import "DBManager.h"
#import "FMDB.h"



@implementation DbManager

NSString *docsDir;
    NSString *date;
NSArray *dirPaths;
NSString *databasePath;

#pragma mark-  Common


-(void) createDatabase:(NSFileManager *)manager {
    [self openDefinedData];
     //[self createTable:@"Food"];
    return;
}

-(void)openDefinedData{
    BOOL success;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    // Database filename can have extension db/sqlite.
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *appDBPath = [documentsDirectory stringByAppendingPathComponent:DATABASE_NAME];
    databasePath=appDBPath;
    success = [fileManager fileExistsAtPath:appDBPath];
      NSLog(@"%@",databasePath);
   // [self createTable:@"Schedule"];
    if (success) {
        return;
    }
    // The writable database does not exist, so copy the default to the appropriate location.
    NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:DATABASE_NAME];
    success = [fileManager copyItemAtPath:defaultDBPath toPath:appDBPath error:&error];
    NSAssert(success, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
}

-(void)createTable:(NSString*) tableName
{
    FMDatabase *database = [FMDatabase databaseWithPath:[self getDBPath]];
    [database open];
    NSString *query = @"";
//    sqlite3_stmt *createStmt = nil;
//    if (sqlite3_open([[self getDBPath] UTF8String], &db) == SQLITE_OK) {
        if([tableName isEqualToString:@"User"])
        {

          query = [NSString stringWithFormat:@"CREATE TABLE 'User' ( 'email' TEXT check(typeof('email') = 'text') , 'userName' TEXT check(typeof('userName') = 'text'), 'dob' TEXT check(typeof('dob') = 'text'), 'gender' TEXT check(typeof('gender') = 'text'), 'unit' TEXT check(typeof('unit') = 'text'), 'picUrl' TEXT check(typeof('picUrl') = 'text'), 'height' TEXT check(typeof('height') = 'text'), 'weight' TEXT check(typeof('weight') = 'text'))"];

//            if (sqlite3_prepare_v2(db, [query UTF8String], -1, &createStmt, NULL) == SQLITE_OK) {
//                sqlite3_exec(db, [query UTF8String], NULL, NULL, NULL);
//            }
        }
        if([tableName isEqualToString:@"Plan"])
        {
            query = [NSString stringWithFormat:@"CREATE TABLE 'Plan' ( 'planName' TEXT check(typeof('planName') = 'text') , 'planId' TEXT check(typeof('planId') = 'text'), 'packageId' TEXT check(typeof('packageId') = 'text'), 'objective' TEXT check(typeof('objective') = 'text'), 'level' TEXT check(typeof('level') = 'text'), 'equipment' TEXT check(typeof('equipment') = 'text'), 'dietPlan' TEXT check(typeof('dietPlan') = 'text'), 'duration' TEXT check(typeof('duration') = 'text'),'status' TEXT check(typeof('status') = 'text'))"];

//            if (sqlite3_prepare_v2(db, [query UTF8String], -1, &createStmt, NULL) == SQLITE_OK) {
//                sqlite3_exec(db, [query UTF8String], NULL, NULL, NULL);
//            }
        }
        if([tableName isEqualToString:@"Exercises"])
        {

            query = [NSString stringWithFormat:@"CREATE TABLE 'Exercises' ('exerciseId' TEXT check(typeof('exerciseId') = 'text'), 'name' TEXT check(typeof('name') = 'text'), 'calBurn' TEXT check(typeof('calBurn') = 'text'),PRIMARY KEY (exerciseId))"];
 
//            if (sqlite3_prepare_v2(db, [query UTF8String], -1, &createStmt, NULL) == SQLITE_OK) {
//                sqlite3_exec(db, [query UTF8String], NULL, NULL, NULL);
//            }
        }
        if([tableName isEqualToString:@"PlanPackage"])
        {

            query = [NSString stringWithFormat:@"CREATE TABLE 'PlanPackage' ( 'fType' TEXT check(typeof('fType') = 'text') , 'fDuration' TEXT check(typeof('fDuration') = 'text'), 'dOrder' TEXT check(typeof('dOrder') = 'text'), 'dExerciseId' TEXT check(typeof('dExerciseId') = 'text'), 'cPlanId' TEXT check(typeof('cPlanId') = 'text'), 'bDay' TEXT check(typeof('bDay') = 'text'), 'aWeek' TEXT check(typeof('aWeek') = 'text'))"];

//            if (sqlite3_prepare_v2(db, [query UTF8String], -1, &createStmt, NULL) == SQLITE_OK) {
//                sqlite3_exec(db, [query UTF8String], NULL, NULL, NULL);
//            }
        }
        
        if([tableName isEqualToString:@"Food"])
        {

            query = [NSString stringWithFormat:@"CREATE TABLE 'Food' ( 'Proteins' TEXT check(typeof('Proteins') = 'text') , 'Name' TEXT check(typeof('Name') = 'text'), 'Fat' TEXT check(typeof('Fat') = 'text'), 'FoodId' TEXT check(typeof('FoodId') = 'text'), 'Calories' TEXT check(typeof('Calories') = 'text'), 'Carbs' TEXT check(typeof('Carbs') = 'text'),PRIMARY KEY (FoodId))"];
 
//            if (sqlite3_prepare_v2(db, [query UTF8String], -1, &createStmt, NULL) == SQLITE_OK) {
//                sqlite3_exec(db, [query UTF8String], NULL, NULL, NULL);
//            }
        }

        if([tableName isEqualToString:@"Schedule"])
        {

             query = [NSString stringWithFormat:@"CREATE TABLE 'Schedule' ( 'userId' TEXT check(typeof('userId') = 'text') , 'time' TEXT check(typeof('time') = 'text'), 'planId' TEXT check(typeof('planId') = 'text'), 'cPlanId' TEXT check(typeof('cPlanId') = 'text'))"];
  
//            if (sqlite3_prepare_v2(db, [query UTF8String], -1, &createStmt, NULL) == SQLITE_OK) {
//                sqlite3_exec(db, [query UTF8String], NULL, NULL, NULL);
//            }
        }
        
        if([tableName isEqualToString:@"Video"])
        {
            query = [NSString stringWithFormat:@"CREATE TABLE 'Video' ('exerciseId' TEXT check(typeof('exerciseId') = 'text'), 'Url' TEXT check(typeof('Url') = 'text'),'videoId' TEXT check(typeof('videoId') = 'text'),PRIMARY KEY (videoId) )"];
//            if (sqlite3_prepare_v2(db, [query UTF8String], -1, &createStmt, NULL) == SQLITE_OK) {
//                sqlite3_exec(db, [query UTF8String], NULL, NULL, NULL);
//            }
        }
        [database executeUpdate:query];
    
        [database close];
//    }
}

//--------------------------------------------------------------------------------------------------------------------------------------//

- (NSString *) getDBPath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
    NSString *documentsDir = [paths objectAtIndex:0];
   return [documentsDir stringByAppendingPathComponent:DATABASE_NAME];
}
//--------------------------------------------------------------------------------------------------------------------------------------//

-(void)insertIntoTable: (NSDictionary *)data table_name:(NSString *)tableName{
  
    FMDatabase *database = [FMDatabase databaseWithPath:[self getDBPath]];
    [database open];
    
//        NSString *key = nil;
//        NSString *value = nil;
//        for (NSString *k in [data allKeys]) {
//            key = (key == nil)?[NSString stringWithString:k] : [key stringByAppendingFormat:@",%@",k];
//            value =(value == nil)?@"?":[value stringByAppendingString:@",?"];
//        }
//        NSString *query = [NSString stringWithFormat:@"INSERT INTO %@(%@) VALUES(%@)", tableName, key, value];
    NSMutableArray* newCols = [[NSMutableArray alloc] init];
    NSMutableArray* newVals = [[NSMutableArray alloc] init];
    
    for (NSString *key in [data allKeys]) {
//        if([data[key] isKindOfClass:[NSArray class]]){
//            NSArray *arr = data[key];
//            if (arr.count == 0) {
//                [newCols addObject:[NSString stringWithString:key]];
//                [newVals addObject:@""];
//                return;
//            }
//        }
        if(![data[key] isKindOfClass:[NSArray class]]){
        [newCols addObject:[NSString stringWithString:key]];
            
            
        [newVals addObject:[NSString stringWithFormat:@"'%@'",[data[key] stringByReplacingOccurrencesOfString:@"'" withString:@"''"]]];
        }
        
    }
    
    NSString *query = [NSString stringWithFormat:@"INSERT INTO %@(%@) VALUES (%@)", tableName, [newCols componentsJoinedByString:@", "], [newVals componentsJoinedByString:@", "]];
        [database executeUpdate:query];
        [database close];
}


-(void) createValues:(sqlite3_stmt *)stmt :(NSDictionary *)dict{
    
    NSLock *_lock = [NSLock new];
    [_lock lock];
    
    int i=0;
    for (NSString *key in [dict allKeys]) {
        if([dict objectForKey:key] != [NSNull null])
        {
            sqlite3_bind_text(stmt, i+1, [[dict objectForKey:key] UTF8String], -1, SQLITE_TRANSIENT);
        }
        i++;
    }
    [_lock unlock];
}



-(void) alterTable:(NSString*) tableName Colomn:(NSString*) colomnName DefaultValue:(NSString*) defaultValue{
    FMDatabase *database = [FMDatabase databaseWithPath:[self getDBPath]];
    
    [database open];
    
    NSString *updateSQL = [NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"ALTER TABLE %@ ADD COLUMN %@ VARCHAR DEFAULT '%@'",tableName,colomnName,defaultValue]];
    
    [database executeUpdate:updateSQL];
    
    [database close];
}



-(void) update:(NSString*)table_name : (NSDictionary *) data :(NSString*) whereClause{
    
    FMDatabase *database = [FMDatabase databaseWithPath:[self getDBPath]];
    [database open];
    
    NSMutableArray* valueArray = [[NSMutableArray alloc] init];
    
    for (NSString *key in [data allKeys]) {
        
        NSString *tempString = [NSString stringWithFormat:@"%@",data[key]];
        
        [valueArray addObject:[NSString stringWithFormat:@"%@ = '%@'",key,[tempString stringByReplacingOccurrencesOfString:@"'" withString:@"''"]]];
    }
    
    NSString *query = [NSString stringWithFormat:@"UPDATE %@ SET %@ where %@ ", table_name, [valueArray componentsJoinedByString:@", "],whereClause];
    
    [database executeUpdate:query];
    
    [database close];

}
-(void)delTableWithTableName:(NSString *)table where:(NSString *)where{
    
    FMDatabase *database = [FMDatabase databaseWithPath:[self getDBPath]];
    [database open];
    
    NSString *query = @"";
    
    if (where == nil) {
        query = [NSString stringWithFormat:@"DELETE FROM  %@ ",table];
    }
    else {
        query = [NSString stringWithFormat:@"DELETE FROM %@ Where %@",table,where];
    }
//            NSString *query =[NSString stringWithFormat:@"DELETE FROM %@ Where %@",table,where];
            [database executeUpdate:query];

            [database close];
    
}

-(void)delTableWithTableName:(NSString *)table{
    
    FMDatabase *database = [FMDatabase databaseWithPath:[self getDBPath]];
    [database open];

    
            NSString *query =[NSString stringWithFormat:@"DELETE FROM %@",table];
            [database executeUpdate:query];

        [database close];
}

-(NSMutableArray *)getDataForField:(NSString*)tableName where:(NSString *)whereClause
    {
        
                NSString *query = @"";

                if (whereClause == nil) {
                    query = [NSString stringWithFormat:@"SELECT * FROM %@ ",tableName];
                }
                else {
                    query = [NSString stringWithFormat:@"SELECT * FROM %@ where %@",tableName, whereClause];
                }
        FMDatabase *database = [FMDatabase databaseWithPath:[self getDBPath]];
        
        [database open];
        
        NSMutableArray *dataArray = [[NSMutableArray alloc] init];
        
        FMResultSet *results = [database executeQuery:query];
        
        while([results next]) {
            
            NSMutableDictionary *tempDisc=[[NSMutableDictionary alloc] init];
            
            for (int i=0; i < results.columnCount;i++ )
            {
                NSString *column = [results columnNameForIndex:i];
                NSString *value =  [results stringForColumn:column];
                
                [tempDisc setObject:value == nil ? @"" : value  forKey:column];
            }
            [dataArray addObject:tempDisc];
        }
        [database close];
        
        return dataArray;
    }


-(NSMutableArray *)getDataForFieldWhereTableName:(NSString*)tableName andQuery:(NSString *)whereClause
{
    NSString *query = @"";
    
    if (whereClause == nil) {
        query = [NSString stringWithFormat:@"SELECT * FROM %@ ",tableName];
    }
    else {
        query = [NSString stringWithFormat:@"SELECT * FROM %@ where %@",tableName, whereClause];
    }
    
    FMDatabase *database = [FMDatabase databaseWithPath:[self getDBPath]];
    
    [database open];
    
    NSMutableArray *arrayToReturn = [[NSMutableArray alloc] init];
    
    FMResultSet *results = [database executeQuery:query];
    
    while([results next]) {
        
        NSMutableDictionary *tempDisc=[[NSMutableDictionary alloc] init];
        
        for (int i=0; i < results.columnCount;i++ )
        {
            NSString *column = [results columnNameForIndex:i];
            NSString *value =  [results stringForColumn:column];
            
            [tempDisc setObject:value == nil ? @"" : value  forKey:column];
        }
        [arrayToReturn addObject:tempDisc];
    }
    [database close];
    
    return arrayToReturn ;
    
}
-(NSMutableArray *)getDataForFieldWithQuery:(NSString *)query
{
    FMDatabase *database = [FMDatabase databaseWithPath:[self getDBPath]];
    
    [database open];
    
    NSMutableArray *arrayToReturn = [[NSMutableArray alloc] init];
    
    FMResultSet *results = [database executeQuery:query];
    
    while([results next]) {
        
        NSMutableDictionary *tempDisc=[[NSMutableDictionary alloc] init];
        
        for (int i=0; i < results.columnCount;i++ )
        {
            NSString *column = [results columnNameForIndex:i];
            NSString *value =  [results stringForColumn:column];
            
            [tempDisc setObject:value == nil ? @"" : value  forKey:column];
        }
        [arrayToReturn addObject:tempDisc];
    }
    [database close];
    
    return arrayToReturn ;
    
}



@end


