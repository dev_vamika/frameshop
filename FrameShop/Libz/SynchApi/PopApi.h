//
//  PopApi.h
//  test
//
//  Created by Saurabh Anand on 19/05/15.
//  Copyright (c) 2015 zudac. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "JSONDictionaryExtensions.h"
//#define SERVER_ADDRESS                      @"https://infinity.frameshop.com.au/api/"

#define SERVER_ADDRESS                     @"https://frameshop.synchsoftventures.com/api/"
#define REGISTER_USER                       @"register"
#define LOGIN_USER                          @"login"
#define GET_PRODUCTS                        @"products"
#define GET_FRAMES_CATEGORY                 @"frames/all-frames"
#define GET_FRAMES_OF_CATEGORY              @"category"
#define SEARCH_FRAMES                       @"frame/search/"
#define GET_METABOARDS                      @"matboards/all-matboards"
#define GET_GIFTCARDS                       @"giftcard/gift-cards"
#define ADD_GIFTCARDS_CART                  @"cart/add"
#define ADD_SHIPPING_ADDRESS                @"address/shipping-address"
#define GET_SHIPPING_ADDRESS                @"address/get-shipping-address"
#define DEL_SHIPPING_ADDRESS                @"address/delete-shipping-address"
#define EDIT_SHIPPING_ADDRESS               @"address/shipping-address"
#define GET_SHIPPING_ADDRESS_ID             @"address/shipping-address/"

#define SEARCH_SHIPPING_ADDRESS             @"https://hosted.mastersoftgroup.com/harmony/rest/au/address"
#define SEARCH_LOCAL                       @"https://hosted.mastersoftgroup.com/harmony/rest/au/locality"



#define GET_CART_ITEMS                      @"cart/list"
#define DEL_CART_ITEMS                      @"cart/delete"

#define ADD_PRODUCT_CART                    @"cart/add"
#define GET_PRICE                           @"price-calculator"

#define UPLOAD_IMAGE                        @"uploads/image-upload"

#define REQ_TRANSACTION_ID                   @"order/request-transaction-identifier"
#define CHECK_COUPON                         @"promocode/apply-promocode"
#define CHANG_QUANTITY                       @"cart/adjustments"
#define COMPLETE_PAYMENT                     @"transaction/charge"
#define REQUEST_INVOICE                     @"order/request-invoice/"


#define GET_ORDER_HOSTORY                    @"order/history"
#define MASTERSOFT_API_USER                    @"frameshopuser"
#define MASTERSOFT_API_PASSWORD                @"zPrDGFTfkSRzwTzANtTErPuk1G6ituIM"

@class PopApi;
@protocol PopApiDelegate <NSObject>
@optional

-(void)connectionError;
-(void)requestFailedSessionExpired:(NSDictionary *)dataPack;
-(void)requestFailedServerError:(NSDictionary *)dataPack;
-(void)requestFailedServiceUnavailableDueToMaintenance:(NSDictionary *)dataPack;
-(void)requestFailedMethodNotFound:(NSDictionary *)dataPack;
-(void)requestFailedNotFound:(NSDictionary *)dataPack;
-(void)requestFailedUnauthorised:(NSDictionary *)dataPack;
-(void)requestFailedBadRequest:(NSDictionary *)dataPack;
-(void)requestSucceededWithData:(NSDictionary *)dataPack;
-(void)requestSucceededButActionFailed:(NSDictionary *)dataPack;
-(void)uploadedData:(NSString *)pecentage andByte:(float)bytes;
-(void)requestSucceededButNoDataFound:(NSDictionary *)dataPack;
-(void)requestFailedDueToServerIsuueWithError:(NSString*)errorDes;


@end
@interface PopApi : NSObject<NSURLSessionDataDelegate>{
    
        NSURLConnection                       *connection_;
        NSMutableData                         *responseData_;
        NSError                               *connectionError_;
}
@property (strong,nonatomic) id<PopApiDelegate> delegate;//NSURLSessionDelegate
-(void)sendPostRequst:(NSDictionary *)dic;
-(void)sendGetRequst:(NSString *)url andToken:(NSString *)token;
-(void) stripePaymentWithToken:(NSString*) stripeToken andAmount:(NSString*) amount ;
-(void)uploadImage:(UIImage *)image fId:(NSString *)fid url:(NSString *)url accessToken:(NSString *)token type:(NSString *)type orderId:(NSString *)orderId index:(NSString *)index quantity:(NSString*)quantity;
//-(void)callApiAsynchronous:(NSMutableDictionary *)dic;
-(void)sendDelRequst:(NSString *)url andToken:(NSString *)token;
-(void)uploadImage:(UIImage *)image orderId:(NSString *)orderId url:(NSString *)url accessToken:(NSString *)token type:(NSString *)type;
-(void)addressSugPostApi:(NSDictionary *)dic;
@end
