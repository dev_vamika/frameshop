//
//  PopApi.m
//  test
//
//  Created by Saurabh Anand on 19/05/15.
//  Copyright (c) 2015 zudac. All rights reserved.
//

#import "PopApi.h"
#import "Global.h"

#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)



@implementation PopApi{
    
    NSString *type;
    NSMutableData *buffer;
}


-(void)sendPostRequst:(NSDictionary *)dic{

    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",dic[@"url"]]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];

    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    NSString *customUserAgent =[NSString stringWithFormat:@"FrameShopiOS/%@.%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"],[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"]];
    
    [request addValue:customUserAgent forHTTPHeaderField:@"User-Agent"];
    
    
    
    [request addValue:[NSString stringWithFormat:@"Bearer %@",dic[@"token"] ] forHTTPHeaderField: @"Authorization"];
    [request setHTTPMethod:@"POST"];
    [request setTimeoutInterval:200];

    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    NSArray*keys=[dic allKeys];
    for (int i=0; i<keys.count; i++) {
        [dictionary setValue:dic[keys[i]] forKey:keys[i]];

    }


    NSData *postData = [[dictionary copy] JSONValue];;

    NSString *str = [[NSString alloc] initWithData:postData encoding:NSUTF8StringEncoding];


    NSLog(@"**********************************JSON REQUEST*********************************");
    NSLog(@"%@",str);
    [request setHTTPBody:postData];
    [request addValue:[NSString stringWithFormat:@"%lu",(unsigned long)postData.length] forHTTPHeaderField:@"Content-Length"];

    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate connectionError];
            });
                return;
            }
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        NSError *parseError = nil;
        NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
        NSLog(@"**********************************JSON RESPONSE*********************************");
        NSLog(@"%@",jsonObject);
        dispatch_async(dispatch_get_main_queue(), ^{
            [self parseDataWhereHttpResPonse:httpResponse andJsonObject:jsonObject andUrl:url];
        });
       
    }];
    [postDataTask resume];


    
    
    
}

-(void)parseDataWhereHttpResPonse:( NSHTTPURLResponse *)httpResponse andJsonObject:(NSDictionary *)jsonObject andUrl:(NSURL *)url{
    
        if(httpResponse.statusCode == 200)
        {
            NSMutableDictionary *dic=[jsonObject mutableCopy];
            
            if ([url.absoluteString isEqualToString:SEARCH_SHIPPING_ADDRESS]||[url.absoluteString isEqualToString:SEARCH_LOCAL]) {
                dic[@"url"]=url.absoluteString;
                [self.delegate requestSucceededWithData:dic];
                
            }
            else{
                
                dic[@"url"]=[url.absoluteString componentsSeparatedByString:SERVER_ADDRESS][1];
                
                
                if ([jsonObject[@"success"]boolValue]) {
                    [self.delegate requestSucceededWithData:dic];
                }
                else{
                    if ([dic[@"message"] isKindOfClass:[NSDictionary class]]) {
                        [self.delegate requestSucceededButActionFailed:dic];
                        return;
                    }
                    if ([dic[@"message"] isEqualToString:@""] || [dic[@"message"] containsString:@"LoginException"]) {
                        [self.delegate requestSucceededButActionFailed:dic];
                    }
                    else if ([dic[@"message"] containsString:@"Unauthenticated."]){
                        //                [self.delegate requestSucceededButActionFailed:dic];
                    }
                    else if ([dic[@"message"] containsString:@"already exists."]){
                        [self.delegate requestSucceededButActionFailed:dic];
                    }
                    else{
                        [self.delegate requestFailedServiceUnavailableDueToMaintenance:jsonObject];
                    }
                                                                                                                          
                }
            }
        }
        else if(httpResponse.statusCode == 202 || httpResponse.statusCode == 403){
            
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
            
            dic[@"url"]=[url.absoluteString componentsSeparatedByString:SERVER_ADDRESS][1];
            dic[@"data"] = jsonObject;
            
            [self.delegate requestSucceededButActionFailed:dic];
        }
        else if(httpResponse.statusCode == 204){
            [self.delegate requestSucceededButNoDataFound:jsonObject];
        }
        else if(httpResponse.statusCode == 401){
            
            [self.delegate requestFailedUnauthorised:jsonObject];
        }
        else if(httpResponse.statusCode == 402){
            [self.delegate requestFailedBadRequest:jsonObject];
        }
        else if(httpResponse.statusCode == 404){
            [self.delegate requestFailedNotFound:jsonObject];
        }
        else if(httpResponse.statusCode == 405){
            [self.delegate requestFailedMethodNotFound:jsonObject];
        }
        else if(httpResponse.statusCode == 409){
            [self.delegate requestFailedMethodNotFound:jsonObject];
        }
        else if(httpResponse.statusCode == 500){
            [self.delegate requestFailedServerError:jsonObject];
        }
        else if(httpResponse.statusCode == 502){
            [self.delegate requestFailedSessionExpired:jsonObject];
        }
        else if(httpResponse.statusCode == 503){
            [self.delegate requestFailedServiceUnavailableDueToMaintenance:jsonObject];
            
        }
        else
        {
            NSLog(@"Error");
        }
    
}


    
-(void)sendDelRequst:(NSString *)url andToken:(NSString *)token{
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    NSURL *api_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",url]];
    NSMutableURLRequest *request;
    request = [[NSMutableURLRequest alloc] initWithURL:api_url];
    [request setHTTPMethod:@"DELETE"];
    [request addValue:[NSString stringWithFormat:@"Bearer %@",token] forHTTPHeaderField: @"Authorization"];
    [request addValue:@"application/json" forHTTPHeaderField: @"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField: @"Accept"];
    
    [request setTimeoutInterval:60];

    
    NSLog(@"**********************************JSON REQUEST*********************************");
 
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate connectionError];
            });
                return;
            }
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        NSError *parseError = nil;
        NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
        NSLog(@"**********************************JSON RESPONSE*********************************");
        NSLog(@"%@",jsonObject);
        dispatch_async(dispatch_get_main_queue(), ^{
            [self parseDataWhereHttpResPonse:httpResponse andJsonObject:jsonObject andUrl:api_url];
        });
       
    }];
    [postDataTask resume];
    
    
}

-(void)addressSugPostApi:(NSDictionary *)dic{
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    NSURL *api_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",dic[@"url"]]];
    
    NSMutableURLRequest *request;
    request = [[NSMutableURLRequest alloc] initWithURL:api_url];
    [request setHTTPMethod:@"POST"];
    
  
    
    [request addValue:@"application/json" forHTTPHeaderField: @"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField: @"Accept"];
   
    NSString *authString=[NSString stringWithFormat:@"Basic %@",[self encodeStringTo64:[NSString stringWithFormat:@"%@:%@",MASTERSOFT_API_USER,MASTERSOFT_API_PASSWORD]]];
    
    
    [request addValue:authString  forHTTPHeaderField: @"Authorization"];
      
    
    
    
    [request setTimeoutInterval:120];
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    NSArray*keys=[dic allKeys];
    for (int i=0; i<keys.count; i++) {
        [dictionary setValue:dic[keys[i]] forKey:keys[i]];
        
    }
    NSData *data = [[dictionary copy] JSONValue];
    
    
    
    NSString *str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    
    NSLog(@"**********************************JSON REQUEST*********************************");
    NSLog(@"%@",str);
    
    
    [request setHTTPBody:data]; //set the data as the post body
    [request addValue:[NSString stringWithFormat:@"%lu",(unsigned long)data.length] forHTTPHeaderField:@"Content-Length"];
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate connectionError];
            });
                return;
            }
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        NSError *parseError = nil;
        NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
        NSLog(@"**********************************JSON RESPONSE*********************************");
        NSLog(@"%@",jsonObject);
        dispatch_async(dispatch_get_main_queue(), ^{
            
                [self parseDataWhereHttpResPonse:httpResponse andJsonObject:jsonObject andUrl:api_url];
           
           
        });
        
    }];
    [postDataTask resume];
    
    
}

- (NSString*)encodeStringTo64:(NSString*)fromString
{
    NSData *plainData = [fromString dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64String;
    if ([plainData respondsToSelector:@selector(base64EncodedStringWithOptions:)]) {
        base64String = [plainData base64EncodedStringWithOptions:kNilOptions];  // iOS 7+
    }

    return base64String;
}


//-(void)callApiAsynchronous:(NSMutableDictionary *)dic{
//    
//    
//    
//    NSMutableURLRequest *request;
//    
//    request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",dic[@"url"]]]];
//    
//    
//    [request setHTTPMethod:@"POST"];
//    [request addValue:@"application/json" forHTTPHeaderField: @"Content-Type"];
//    [request setTimeoutInterval:60];
//    
//    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
//    NSArray*keys=[dic allKeys];
//    for (int i=0; i<keys.count; i++) {
//        [dictionary setValue:dic[keys[i]] forKey:keys[i]];
//        
//    }
//    NSData *data = [[dictionary copy] JSONValue];
//    
//    
//    
//    NSString *str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
//    
//    
//    NSLog(@"**********************************JSON REQUEST*********************************");
//    NSLog(@"%@",str);
//    
//    
//    [request setHTTPBody:data]; //set the data as the post body
//    [request addValue:[NSString stringWithFormat:@"%lu",(unsigned long)data.length] forHTTPHeaderField:@"Content-Length"];
//    
//    
//    [NSURLConnection sendAsynchronousRequest:request
//                                       queue:[NSOperationQueue mainQueue]
//                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
//        // optionally update the UI to say 'done'
//        if (!error) {
//            
//            
//            id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
//            NSString *str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
//            // NSString *str = [[NSString alloc] initWithData:responseData_ encoding:NSUTF8StringEncoding];
//            NSLog(@"**********************************JSON RESPONSE*********************************");
//            NSLog(@"%@",str);
//            //  id jsonObject = [NSJSONSerialization JSONObjectWithData:responseData_ options:NSJSONReadingAllowFragments error:&error];
//            
//            
//            switch ([jsonObject[@"statusCode"] integerValue]) {
//                case 200:
//                    [self.delegate requestSucceededWithData:jsonObject];
//                    break;
//                case 202:
//                    [self.delegate requestSucceededButActionFailed:jsonObject];
//                    break;
//                case 204:
//                    [self.delegate requestSucceededButNoDataFound:jsonObject];
//                    break;
//                case 401:
//                    [self.delegate requestFailedBadRequest:jsonObject];
//                    break;
//                case 402:
//                    [self.delegate requestFailedUnauthorised:jsonObject];
//                    break;
//                case 404:
//                    [self.delegate requestFailedNotFound:jsonObject];
//                    break;
//                case 405:
//                    [self.delegate requestFailedMethodNotFound:jsonObject];
//                    break;
//                case 409:
//                    [self.delegate requestFailedMethodNotFound:jsonObject];
//                    break;
//                case 500:
//                    [self.delegate requestFailedServerError:jsonObject];
//                    break;
//                case 502:
//                    [self.delegate requestFailedSessionExpired:jsonObject];
//                    break;
//                case 503:
//                    [self.delegate requestFailedServiceUnavailableDueToMaintenance:jsonObject];
//                    break;
//                default:
//                    break;
//            }
//            
//            
//            
//            // update the UI here (and only here to the extent it depends on the json)
//        } else {
//            // update the UI to indicate error
//        }
//    }];
//}


-(void)sendGetRequst:(NSString *)url andToken:(NSString *)token{
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    NSURL *api_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",url]];
    NSMutableURLRequest *request;
    
    request = [[NSMutableURLRequest alloc] initWithURL:api_url];
    
    
    [request setHTTPMethod:@"GET"];
    [request addValue:[NSString stringWithFormat:@"Bearer %@",token] forHTTPHeaderField: @"Authorization"];
    [request addValue:@"application/json" forHTTPHeaderField: @"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField: @"Accept"];
    NSString *customUserAgent =[NSString stringWithFormat:@"FrameShopiOS/%@.%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"],[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"]];
    
    [request addValue:customUserAgent forHTTPHeaderField:@"User-Agent"];
    
    [request setTimeoutInterval:60];

    
    
    NSLog(@"**********************************GET JSON REQUEST*********************************");
    
    NSLog(@"URL ==> %@",url);

    
    
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate connectionError];
            });
                return;
            }
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        NSError *parseError = nil;
        NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
        NSLog(@"**********************************JSON RESPONSE*********************************");
        NSLog(@"%@",jsonObject);
        dispatch_async(dispatch_get_main_queue(), ^{
            [self parseDataWhereHttpResPonse:httpResponse andJsonObject:jsonObject andUrl:api_url];
        });
       
    }];
    [postDataTask resume];
    
    
}


-(void)uploadImage:(UIImage *)image fId:(NSString *)fid url:(NSString *)url accessToken:(NSString *)token type:(NSString *)type orderId:(NSString *)orderId index:(NSString *)index quantity:(NSString*)quantity {
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    NSURL *api_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",url]];
    
    NSLog(@"**********************************UPLOADING IMAGE*********************************");
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:api_url];
    
    //UIImage *testimage=[UIImage imageNamed:@"back.png"];
    //testimage=[self removeExifFromImage:testimage];
    
    NSData *data=
    UIImagePNGRepresentation(image);
    
    //UIImageJPEGRepresentation(image, 1.0);
    
  
    
    NSLog(@"File size is : %.2f KB",(float)data.length/1024.0f);
    [request setHTTPMethod:@"POST"];
    // [request addValue:@"postValues" forHTTPHeaderField:@"METHOD"];
    [request setTimeoutInterval:3600];
    NSMutableData *body = [[NSMutableData alloc] init];
    [body appendData:[NSData dataWithData:data]];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSString *customUserAgent =[NSString stringWithFormat:@"FrameShopiOS/%@.%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"],[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"]];
    
    [request addValue:customUserAgent forHTTPHeaderField:@"User-Agent"];
    
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"img\"; filename=%@_%@.png\r\n",orderId,index ]dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[NSData dataWithData:data]];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [request setHTTPBody:body];
    [request setValue:fid forHTTPHeaderField:@"iduser"];
    [request setValue:orderId forHTTPHeaderField:@"order_id"];
    [request setValue:type forHTTPHeaderField:@"uploadtype"];
    //[request setValue:quantity forHTTPHeaderField:@"quantity"];
    [request setValue:token forHTTPHeaderField:@"access-token"];
    
    [request addValue:[NSString stringWithFormat:@"%lu",(unsigned long)body.length] forHTTPHeaderField:@"Content-Length"];
    
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        NSError *parseError = nil;
        NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
        NSLog(@"**********************************JSON RESPONSE*********************************");
        NSLog(@"%@",jsonObject);
        if(httpResponse.statusCode == 200)
        {
            NSMutableDictionary *dic=[jsonObject mutableCopy];
            
            if ([api_url.absoluteString isEqualToString:SEARCH_SHIPPING_ADDRESS]||[api_url.absoluteString isEqualToString:SEARCH_LOCAL]) {
                dic[@"url"]=api_url.absoluteString;
                [self.delegate requestSucceededWithData:dic];
                
            }
            else{
                
                dic[@"url"]=[api_url.absoluteString componentsSeparatedByString:SERVER_ADDRESS][1];
                
                
                if ([jsonObject[@"success"]boolValue]) {
                    [self.delegate requestSucceededWithData:dic];
                }
                else{
                    if ([dic[@"message"] isKindOfClass:[NSDictionary class]]) {
                        [self.delegate requestSucceededButActionFailed:dic];
                        return;
                    }
                    if ([dic[@"message"] isEqualToString:@""] || [dic[@"message"] containsString:@"LoginException"]) {
                        [self.delegate requestSucceededButActionFailed:dic];
                    }
                    else if ([dic[@"message"] containsString:@"Unauthenticated."]){
                        //                [self.delegate requestSucceededButActionFailed:dic];
                    }
                    else if ([dic[@"message"] containsString:@"already exists."]){
                        [self.delegate requestSucceededButActionFailed:dic];
                    }
                    else{
                        [self.delegate requestFailedServiceUnavailableDueToMaintenance:jsonObject];
                    }
                    
                }
            }
        }
        else if(httpResponse.statusCode == 202 || httpResponse.statusCode == 403){
            
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
            
            dic[@"url"]=[api_url.absoluteString componentsSeparatedByString:SERVER_ADDRESS][1];
            dic[@"data"] = jsonObject;
            
            [self.delegate requestSucceededButActionFailed:dic];
        }
        else if(httpResponse.statusCode == 204){
            [self.delegate requestSucceededButNoDataFound:jsonObject];
        }
        else if(httpResponse.statusCode == 401){
            
            [self.delegate requestFailedUnauthorised:jsonObject];
        }
        else if(httpResponse.statusCode == 402){
            [self.delegate requestFailedBadRequest:jsonObject];
        }
        else if(httpResponse.statusCode == 404){
            [self.delegate requestFailedNotFound:jsonObject];
        }
        else if(httpResponse.statusCode == 405){
            [self.delegate requestFailedMethodNotFound:jsonObject];
        }
        else if(httpResponse.statusCode == 409){
            [self.delegate requestFailedMethodNotFound:jsonObject];
        }
        else if(httpResponse.statusCode == 500){
            [self.delegate requestFailedServerError:jsonObject];
        }
        else if(httpResponse.statusCode == 502){
            [self.delegate requestFailedSessionExpired:jsonObject];
        }
        else if(httpResponse.statusCode == 503){
            [self.delegate requestFailedServiceUnavailableDueToMaintenance:jsonObject];
            
        }
        else
        {
            NSLog(@"Error");
        }
    }];
    [postDataTask resume];
    
    
    
}


-(void)uploadImage:(UIImage *)image orderId:(NSString *)orderId url:(NSString *)url accessToken:(NSString *)token type:(NSString *)type {
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    NSURL *api_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",url]];
    
    NSLog(@"**********************************UPLOADING IMAGE*********************************");
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:api_url];
    
    //UIImage *testimage=[UIImage imageNamed:@"back.png"];
    //testimage=[self removeExifFromImage:testimage];
    
    NSData *data= UIImagePNGRepresentation(image);
    
    //UIImageJPEGRepresentation(image, 1.0);
    
    
    
    
    
    NSLog(@"File size is : %.2f KB",(float)data.length/1024.0f);
    [request setHTTPMethod:@"POST"];
    // [request addValue:@"postValues" forHTTPHeaderField:@"METHOD"];
    [request setTimeoutInterval:3600];
    NSMutableData *body = [[NSMutableData alloc] init];
    
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSString *customUserAgent =[NSString stringWithFormat:@"FrameShopiOS/%@.%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"],[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"]];
    
    [request addValue:customUserAgent forHTTPHeaderField:@"User-Agent"];
    
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"image\"; filename=\"%@.png\"\r\n",orderId] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[NSData dataWithData:data]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    
    
    
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"order_id\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[orderId dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"media_type\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[type dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    
    // close form
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    // set request body
    
    
    
    //    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    //    [body appendData:[[NSString stringWithFormat:@"Content-Disposition:form-data; name=\"%@\"\r\n\r\n", orderId]dataUsingEncoding:NSUTF8StringEncoding]];
    //
    //     [body appendData:[[NSString stringWithFormat:@"%@",orderId ]dataUsingEncoding:NSUTF8StringEncoding]];
    //
    //
    //    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    //
    //
    //     [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"media_type\"\r\n" ]dataUsingEncoding:NSUTF8StringEncoding]];
    //     [body appendData:[[NSString stringWithFormat:@"%@",type ]dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    
    
    
    
    
    
    [request setHTTPBody:body];
    
    
    //[request setValue:quantity forHTTPHeaderField:@"quantity"];
    [request addValue:[NSString stringWithFormat:@"Bearer %@",token] forHTTPHeaderField: @"Authorization"];
    
    [request addValue:[NSString stringWithFormat:@"%lu",(unsigned long)body.length] forHTTPHeaderField:@"Content-Length"];
    
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    if(!connection){
        NSLog(@"Connection Failed");
    }
    
//    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
//        NSError *parseError = nil;
//        NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
//        NSLog(@"**********************************JSON RESPONSE*********************************");
//        NSLog(@"%@",jsonObject);
//        dispatch_async(dispatch_get_main_queue(), ^{
//            if(httpResponse.statusCode == 200)
//            {
//                NSMutableDictionary *dic=[jsonObject mutableCopy];
//
//                if ([api_url.absoluteString isEqualToString:SEARCH_SHIPPING_ADDRESS]||[api_url.absoluteString isEqualToString:SEARCH_LOCAL]) {
//                    dic[@"url"]=api_url.absoluteString;
//                    [self.delegate requestSucceededWithData:dic];
//
//                }
//                else{
//
//                    dic[@"url"]=[api_url.absoluteString componentsSeparatedByString:SERVER_ADDRESS][1];
//
//
//                    if ([jsonObject[@"success"]boolValue]) {
//                        [self.delegate requestSucceededWithData:dic];
//                    }
//                    else{
//                        if ([dic[@"message"] isKindOfClass:[NSDictionary class]]) {
//                            [self.delegate requestSucceededButActionFailed:dic];
//                            return;
//                        }
//                        if ([dic[@"message"] isEqualToString:@""] || [dic[@"message"] containsString:@"LoginException"]) {
//                            [self.delegate requestSucceededButActionFailed:dic];
//                        }
//                        else if ([dic[@"message"] containsString:@"Unauthenticated."]){
//                            //                [self.delegate requestSucceededButActionFailed:dic];
//                        }
//                        else if ([dic[@"message"] containsString:@"already exists."]){
//                            [self.delegate requestSucceededButActionFailed:dic];
//                        }
//                        else{
//                            [self.delegate requestFailedServiceUnavailableDueToMaintenance:jsonObject];
//                        }
//
//                    }
//                }
//            }
//            else if(httpResponse.statusCode == 202 || httpResponse.statusCode == 403){
//
//                NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
//
//                dic[@"url"]=[api_url.absoluteString componentsSeparatedByString:SERVER_ADDRESS][1];
//                dic[@"data"] = jsonObject;
//
//                [self.delegate requestSucceededButActionFailed:dic];
//            }
//            else if(httpResponse.statusCode == 204){
//                [self.delegate requestSucceededButNoDataFound:jsonObject];
//            }
//            else if(httpResponse.statusCode == 401){
//
//                [self.delegate requestFailedUnauthorised:jsonObject];
//            }
//            else if(httpResponse.statusCode == 402){
//                [self.delegate requestFailedBadRequest:jsonObject];
//            }
//            else if(httpResponse.statusCode == 404){
//                [self.delegate requestFailedNotFound:jsonObject];
//            }
//            else if(httpResponse.statusCode == 405){
//                [self.delegate requestFailedMethodNotFound:jsonObject];
//            }
//            else if(httpResponse.statusCode == 409){
//                [self.delegate requestFailedMethodNotFound:jsonObject];
//            }
//            else if(httpResponse.statusCode == 500){
//                [self.delegate requestFailedServerError:jsonObject];
//            }
//            else if(httpResponse.statusCode == 502){
//                [self.delegate requestFailedSessionExpired:jsonObject];
//            }
//            else if(httpResponse.statusCode == 503){
//                [self.delegate requestFailedServiceUnavailableDueToMaintenance:jsonObject];
//
//            }
//            else
//            {
//                NSLog(@"Error");
//            }
//        });
//
//    }];
//    [postDataTask resume];
    
}






//- (UIImage *)compressImage:(UIImage *)image{
//    float actualHeight = image.size.height;
//    float actualWidth = image.size.width;
//    float maxHeight = 600.0;
//    float maxWidth = 800.0;
//    float imgRatio = actualWidth/actualHeight;
//    float maxRatio = maxWidth/maxHeight;
//    float compressionQuality = 0.5;//50 percent compression
//    
//    if (actualHeight > maxHeight || actualWidth > maxWidth) {
//        if(imgRatio < maxRatio){
//            //adjust width according to maxHeight
//            imgRatio = maxHeight / actualHeight;
//            actualWidth = imgRatio * actualWidth;
//            actualHeight = maxHeight;
//        }
//        else if(imgRatio > maxRatio){
//            //adjust height according to maxWidth
//            imgRatio = maxWidth / actualWidth;
//            actualHeight = imgRatio * actualHeight;
//            actualWidth = maxWidth;
//        }else{
//            actualHeight = maxHeight;
//            actualWidth = maxWidth;
//        }
//    }
//    
//    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
//    UIGraphicsBeginImageContext(rect.size);
//    [image drawInRect:rect];
//    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
//    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
//    UIGraphicsEndImageContext();
//    
//    return [UIImage imageWithData:imageData];
//}



- (NSData *) removeExifFromImage:(UIImage *) image {
    
    NSData *data = UIImagePNGRepresentation([self rotateImage:image]);
    
    CGImageSourceRef source = CGImageSourceCreateWithData((CFDataRef)data, NULL);
    NSMutableData *mutableData = nil;
    
    if (source) {
        CFStringRef types = CGImageSourceGetType(source);
        size_t count = CGImageSourceGetCount(source);
        mutableData = [NSMutableData data];
        
        CGImageDestinationRef destination = CGImageDestinationCreateWithData((CFMutableDataRef)mutableData, types, count, NULL);
        
        NSDictionary *removeExifProperties = @{(id)kCGImagePropertyExifDictionary: (id)kCFNull,
                                               (id)kCGImagePropertyGPSDictionary : (id)kCFNull};
        
        if (destination) {
            for (size_t index = 0; index < count; index++) {
                CGImageDestinationAddImageFromSource(destination, source, index, (__bridge CFDictionaryRef)removeExifProperties);
            }
            
            if (!CGImageDestinationFinalize(destination)) {
                NSLog(@"CGImageDestinationFinalize failed");
            }
            
            CFRelease(destination);
        }
        
        CFRelease(source);
    }
    return mutableData;
    
}

- (UIImage *) rotateImage:(UIImage *) image {
    
    if (image.imageOrientation == UIImageOrientationUp) return image;
    
    UIGraphicsBeginImageContextWithOptions(image.size, NO, image.scale);
    [image drawInRect:(CGRect){0, 0, image.size}];
    UIImage *normalizedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return normalizedImage;
}

















- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData*)data{
    if(responseData_ == nil ) {
        responseData_ = [[NSMutableData alloc] initWithCapacity:80192];
    }
    [responseData_ appendData:data];
    
    
    
}
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [self.delegate connectionError];
}

- (void)connection:(NSURLConnection *)connection   didSendBodyData:(NSInteger)bytesWritten
 totalBytesWritten:(NSInteger)totalBytesWritten
totalBytesExpectedToWrite:(NSInteger)totalBytesExpectedToWrite{
    
    float pecentage=((float)totalBytesWritten/(float)totalBytesExpectedToWrite)*100;
    
    
    [self.delegate uploadedData:[NSString stringWithFormat:@"%f%%",pecentage] andByte:(float)bytesWritten];
    
}


- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
    
    
    NSLog(@"Server Response with status code %ld",(long)[httpResponse statusCode]);
    
    
    
    
    switch ((long)[httpResponse statusCode]) {
            case 401:
                       [self.delegate requestFailedUnauthorised:nil];
                       break;
        case 500:
            [self.delegate requestFailedDueToServerIsuueWithError:@"Something is wrong as operation couldn’t be completed ! Please hold while we investigate this issue."];
            break;
        case 501:
            [self.delegate requestFailedDueToServerIsuueWithError:@"Something is wrong as operation couldn’t be completed ! Please hold while we investigate this issue."];
            break;
        case 502:
            [self.delegate requestFailedDueToServerIsuueWithError:@" Something is wrong! Please hold while we investigate this issue."];
            break;
        case 503:
            //[self.delegate requestFailedDueToServerIsuueWithError:@"Something is wrong! Please hold couldn’t get any response from servers."];
            break;
        case 504:
            [self.delegate requestFailedDueToServerIsuueWithError:@"We Ran Out of Time, Server did not respond in time."];
            break;
            
        default:
            break;
    }
    
    
    if ([httpResponse statusCode] == 500)
    {
        // Do whatever you want to do after getting response
    }
    
    
    
    
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    
    
    
    if (connectionError_)
    {
        [self.delegate connectionError];
        
    }
    else
    {
        if(responseData_ == nil ) {
            responseData_ = [[NSMutableData alloc] initWithCapacity:80192];
        }
        
        NSError* error = nil;
        
        
        NSString *str = [[NSString alloc] initWithData:responseData_ encoding:NSUTF8StringEncoding];
        NSLog(@"**********************************JSON RESPONSE*********************************");
        NSLog(@"%@",str);
        NSLog(@"%@",connection.currentRequest.URL.absoluteString);
        
        id jsonObject = [NSJSONSerialization JSONObjectWithData:responseData_ options:NSJSONReadingAllowFragments error:&error];
        
        //NSString *url=[NSString stringWithFormat:@"%@",]
        
        
        
        NSMutableDictionary *dic=[jsonObject mutableCopy];
        
        
        if ([connection.currentRequest.URL.absoluteString isEqualToString:SEARCH_SHIPPING_ADDRESS]||[connection.currentRequest.URL.absoluteString isEqualToString:SEARCH_LOCAL]) {
            dic[@"url"]=connection.currentRequest.URL.absoluteString;
             [self.delegate requestSucceededWithData:dic];
            
        }
        else{
        
        dic[@"url"]=[connection.currentRequest.URL.absoluteString componentsSeparatedByString:SERVER_ADDRESS][1];
        
        
        if ([jsonObject[@"success"]boolValue]) {
            [self.delegate requestSucceededWithData:dic];
        }
        else{
            if ([dic[@"message"] isKindOfClass:[NSDictionary class]]) {
                [self.delegate requestSucceededButActionFailed:dic];
                return;
            }
            if ([dic[@"message"] isEqualToString:@""] || [dic[@"message"] containsString:@"LoginException"]) {
                [self.delegate requestSucceededButActionFailed:dic];
            }
            else if ([dic[@"message"] containsString:@"Unauthenticated."]){
//                [self.delegate requestSucceededButActionFailed:dic];
            }
            else if ([dic[@"message"] containsString:@"already exists."]){
                [self.delegate requestSucceededButActionFailed:dic];
            }
            else{
                [self.delegate requestFailedServiceUnavailableDueToMaintenance:jsonObject];
            }
            
        }
        }
        
        //        switch ([jsonObject[@"statusCode"] integerValue]) {
        //            case 200:
        //                  [self.delegate requestSucceededWithData:jsonObject];
        //                break;
        //            case 202:
        //                [self.delegate requestSucceededButActionFailed:jsonObject];
        //                break;
        //            case 204:
        //
        //            {
        //                NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
        //                dic[@"data"]=jsonObject;
        //                dic[@"url"]=[NSString stringWithFormat:@"%@",connection.currentRequest.URL];
        //               [self.delegate requestSucceededButNoDataFound:dic];
        //            }
        //
        //
        //                break;
        //            case 401:
        //                [self.delegate requestFailedBadRequest:jsonObject];
        //                break;
        //            case 402:
        //                [self.delegate requestFailedUnauthorised:jsonObject];
        //                break;
        //            case 404:
        //                [self.delegate requestFailedNotFound:jsonObject];
        //                break;
        //            case 405:
        //                [self.delegate requestFailedMethodNotFound:jsonObject];
        //                break;
        //            case 409:
        //                [self.delegate requestFailedMethodNotFound:jsonObject];
        //                break;
        //            case 500:
        //                [self.delegate requestFailedServerError:jsonObject];
        //                break;
        //            case 502:
        //                [self.delegate requestFailedSessionExpired:jsonObject];
        //                break;
        //            case 503:
        //                [self.delegate requestFailedServiceUnavailableDueToMaintenance:jsonObject];
        //                break;
        //            default:
        //                break;
        //        }
        //
        
        
        
        
        
        
        
    }responseData_=Nil;
}


-(void) stripePaymentWithToken:(NSString*) stripeToken andAmount:(NSString*) amount{
    //    NSString    *url = SERVER_URL_WITH_API(API_STRIPE_PAYMENT);
    
    NSMutableDictionary *dictParam = [NSMutableDictionary new];
    [dictParam setObject:stripeToken forKey:@"stripe_token"];
    [dictParam setObject:amount forKey:@"amount"];
}



@end
