//
//  Validation.m
//  Skill Grok
//
//  Created by experience on 5/21/14.
//  Copyright (c) 2014 Alcanzar Soft. All rights reserved.
//

#import "Validation.h"


@implementation Validation{
    UIActivityIndicatorView *activity;
    UIView *view;
    NSString *type;
}

+(BOOL)isValidEmail:(NSString *)email{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
    
    
}
+(BOOL)isValidText:(NSString *)text{
    
    if (text.length>0 && ![text isEqualToString:@" " ]) {
        return YES;
    }
    return NO;
    
}
+(BOOL)isValidTextName:(NSString *)text{
   
    if (!([text isKindOfClass:[NSNull class]]) ){
        if([text containsString:@" "]){
            
            NSString *str = [text stringByReplacingOccurrencesOfString:@" " withString:@""];
            if ((str.length>0)) {
                return YES;
            }
        }else{
            if ((text.length>0)) {
                return YES;
            }
        }
    }
    
   
    return NO;
}
+(BOOL)isValidPhoneNumber:(NSString *)number{
    NSString *expression = @"^\\+[1-9]{1}[0-9]{3,14}$";
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:nil];
    NSUInteger numberOfMatches = [regex numberOfMatchesInString:number
                                                        options:0
                                                          range:NSMakeRange(0, [number length])];
    if (numberOfMatches == 0 || number.length<=0)
        return NO;
    
    return YES;
    
}

+(BOOL)isValidPostal:(NSString *)number{
    NSString *expression = @"^([0-9]+)?(\\.([0-9]{1,2})?)?$";
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:nil];
    NSUInteger numberOfMatches = [regex numberOfMatchesInString:number
                                                        options:0
                                                          range:NSMakeRange(0, [number length])];
    if (numberOfMatches == 0 || number.length<4)
        return NO;
    
    return YES;
    
}



+(BOOL)isValidNumber:(NSString *)number{
    NSString *expression = @"^([0-9]+)?(\\.([0-9]{1,2})?)?$";
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:nil];
    NSUInteger numberOfMatches = [regex numberOfMatchesInString:number
                                                        options:0
                                                          range:NSMakeRange(0, [number length])];
    if (numberOfMatches == 0 || number.length<=0)
        return NO;
    
    return YES;
    
}
+(BOOL)isPassMatch:(NSString *)pass conpass:(NSString *)conpass{
    if ([pass isEqualToString:conpass]) {
        return YES;
    }
    return NO;
    
}
+(BOOL)isMonth:(NSString *)month{
    // ////  NSLog(@"%d",[month intValue]);
    if ([month intValue]<=12&&[month intValue]>0) {
        return YES;
    }
    return NO;
    
}
+(BOOL)isDay:(NSString *)day{
    if ([day intValue]<=31 && [day intValue]>0) {
        return YES;
    }
    return NO;
    
}
+(BOOL)isYear:(NSString *)year{
    if (0<[year intValue] && year.length>=4) {
        return YES;
    }
    return NO;
    
}
+(BOOL)isPass:(NSString *)pass{
    if (pass.length>7) {
        return YES;
    }
    return NO;
    
}


+(void)showToast:(NSString *)msg viewController:(UIViewController *)cont{
    
}

+(BOOL)isValidDate:(NSDate *)date{
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyyMMdd"];
    int dateInt=[[dateFormatter stringFromDate:date] intValue];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyyMMdd";
    int com = [[formatter stringFromDate:[NSDate date]] intValue];
    
    if (dateInt>com) {
        return NO;
    }
    
    
    return YES;
    
}


+(BOOL)isPastDate:(NSDate *)date{
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyyMMdd"];
    int dateInt=[[dateFormatter stringFromDate:date] intValue];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyyMMdd";
    int com = [[formatter stringFromDate:[NSDate date]] intValue];
    
    if (dateInt<com) {
        return YES;
    }
    
    
    return NO;
    
}





-(void)addLoader:(UIView *)mainView height:(float)height width:(float)width activity :(BOOL)isYes{
    
    view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    [view setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.4]];
    [mainView addSubview:view];
    if (isYes) {
        activity=[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        activity.frame=CGRectMake((width/2)-25, (height/2)-25, 50, 50);
        
        [mainView addSubview:activity];
        [activity startAnimating];
    }
    
    //[loader removeFromSuperview ];
}
-(void)removeLoader{
    [activity stopAnimating];
    [activity removeFromSuperview];
    [view removeFromSuperview ];
    
}

+(void)setValidationMarkToTextField:(UITextField *)textField isCorrect:(BOOL)isCorrect{
    
    if (textField.rightView==nil) {
       
        UIView *view=[[UIView alloc] init];
                         view.frame=CGRectMake(0, 0, 30, 30);
                        
                         
                         
                         UIImageView *imageview=[[UIImageView alloc] init];
                         imageview.frame=CGRectMake(0, 0, 15, 15);
                         imageview.contentMode=UIViewContentModeScaleAspectFit;
                         textField.rightViewMode=UITextFieldViewModeAlways;
                         textField.rightView=view;
        imageview.image=(isCorrect)? [UIImage imageNamed:@"greenTick.png"]:[UIImage imageNamed:@"incorrect.png"];
                         [view addSubview:imageview];
        imageview.center=view.center;
    }
    else{
        
        NSArray *arr = [textField.rightView subviews];

                for (UIView* b in arr)
                      {
                          if ([ b isKindOfClass:[UIImageView class]]) {
                              
                              UIImageView *imageView=(UIImageView *)b;
                            imageView.image=(isCorrect)? [UIImage imageNamed:@"greenTick.png"]:[UIImage imageNamed:@"incorrect.png"];
                              
                              
                          }

                             



                      }
           
        
        
    }
    
    
    
}
@end
