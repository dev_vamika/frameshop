//
//  SummaryCell.h
//  FrameShop
//
//  Created by Vamika on 23/03/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Global.h"
NS_ASSUME_NONNULL_BEGIN

@interface SummaryCell : UITableViewCell<UICollectionViewDelegate,UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UIImageView *topImage;
@property (weak, nonatomic) IBOutlet UILabel *sizeLbl;
@property (weak, nonatomic) IBOutlet UILabel *priceLbl;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageController;
@property (weak, nonatomic) IBOutlet UIImageView *framePreviewImage;
@property (weak, nonatomic) IBOutlet UIImageView *imagePreview;
@property (weak, nonatomic) IBOutlet UIView *insideView;
@property (weak, nonatomic) IBOutlet UILabel *framePrint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *framePreviewWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *framePreviewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *insideViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *insideViewWidth;
@property (weak, nonatomic) IBOutlet UILabel *framePreViewHeightLbl;
@property (weak, nonatomic) IBOutlet UILabel *framePreviewWidthLbl;
@property (weak, nonatomic) IBOutlet UILabel *insideWidthLbl;
@property (weak, nonatomic) IBOutlet UILabel *insiideHeightLbl;
@property (weak, nonatomic) IBOutlet UIView *insideScaleTopView;
@property (weak, nonatomic) IBOutlet UIButton *viewMore;
@property (weak, nonatomic) IBOutlet UIView *lowerBaseView;
@property (weak, nonatomic) IBOutlet UILabel *visible;
@property (weak, nonatomic) IBOutlet UILabel *outside;

@property (weak, nonatomic) IBOutlet UILabel *subDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
-(void)refreshCollectionView;
@end

NS_ASSUME_NONNULL_END
