//
//  SummaryImagePreviewCell.h
//  FrameShop
//
//  Created by vamika on 03/07/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SummaryImagePreviewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imagePreviewContainer;

@end

NS_ASSUME_NONNULL_END
