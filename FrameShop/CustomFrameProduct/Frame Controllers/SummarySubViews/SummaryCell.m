//
//  SummaryCell.m
//  FrameShop
//
//  Created by Vamika on 23/03/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import "SummaryCell.h"
#import "SummaryImagePreviewCell.h"
#import "DBManager.h"
@implementation SummaryCell
{
    UIImage *rightSplitImage;
    NSMutableArray *imageArray;
    DbManager *db;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)refreshCollectionView{
    
    float imageHeight,imageWidth;
    UIImage *img = _framePreviewImage.image;
    float ratio=_framePreviewImage.image.size.width/_framePreviewImage.image.size.height;
    if (img.size.height>img.size.width) {
           imageHeight=_framePreviewImage.frame.size.height;
           imageWidth=imageHeight*ratio;
       }
    else if (img.size.height==img.size.width){
        imageWidth  =_framePreviewImage.frame.size.width;
        imageHeight =imageWidth/ratio;
    }
       else{
           imageWidth =self.contentView.bounds.size.width;
           imageHeight=imageWidth/ratio;
       }
    _framePreviewWidth.constant = imageWidth;
    _framePreviewHeight.constant = imageHeight;
    _insideViewWidth.constant = imageWidth;
    _insideViewHeight.constant = imageHeight;
    //self->_mainImage.frame=CGRectMake(0, 0, imageWidth, imageHeight);
    imageArray = [[NSMutableArray alloc] init];
//    leftSplitImage = [self splitImage:self.imagePreview.image rectToCrop:CGRectMake(0, 0, self.imagePreview.image.size.width / 2.0, self.imagePreview.image.size.height / 2.0)];
    rightSplitImage = [self splitImage:self.imagePreview.image rectToCrop:CGRectMake(self.imagePreview.image.size.width / 2.0, self.imagePreview.image.size.height / 2.0,  self.imagePreview.image.size.width, self.imagePreview.image.size.height / 2.0)];
//    [imageArray addObject:self.imagePreview.image];
//    [imageArray addObject:rightSplitImage];
    
    self.pageController.hidden = YES;
    
    
    db = [[DbManager alloc] init];
   
    NSMutableArray *arr=[db getDataForField:USER_PRODUCT where:nil];
   NSMutableArray * frmArr=[db getDataForField:FRAME_TABLE where:[NSString stringWithFormat:@"id='%@'",arr[0][@"frame"]]];
    
    

       NSMutableDictionary *outDic=[[NSMutableDictionary alloc] init];
         NSDictionary * temDic=[self getGlassWidthWhereInfoDic:arr[0]];
    
     float margine  = (([frmArr[0][@"width"] floatValue])*2)+(([frmArr[0][@"rebate"] floatValue])*2);
//    _sizeLbl.text = @"";
    outDic[@"width"]       =[NSString stringWithFormat:@"%.1f",[temDic[@"width"]floatValue]+margine];
         outDic[@"height"]       =[NSString stringWithFormat:@"%.1f",[temDic[@"height"]floatValue]+margine];
   if ([temDic[@"width"] floatValue]<21 && [temDic[@"height"] floatValue]<21 ) {
       self.titleLabel.text = @"Hanger + Standback";
       self.subDescriptionLabel.text = @"Your frame will be made with both metal hangers and a standback, suitable for hanging on a wall or standing on a desk.";
       [imageArray addObject:[UIImage imageNamed:@"Hanger.jpeg"]];
   }
    else if ([temDic[@"width"] floatValue]<21 && [temDic[@"height"] floatValue]<29.7 ) {
        //HANGER_STANDBACK
        self.titleLabel.text = @"Hanger + Standback";
        self.subDescriptionLabel.text = @"Your frame will be made with both metal hangers and a standback, suitable for hanging on a wall or standing on a desk.";
        [imageArray addObject:[UIImage imageNamed:@"Hanger.jpeg"]];
    }
    else{
        //D_RINGS_STRING;
        self.titleLabel.text = @"D-Rings + String";
        self.subDescriptionLabel.text = @"Your frame will be made with a string hanging system to hang on the wall.";
        [imageArray addObject:[UIImage imageNamed:@"D_String.jpeg"]];
        
    }
    if ([arr[0][@"unit"] isEqualToString:@"cm"]) {
         _visible.text=[NSString stringWithFormat:@"Image Size: %.1f x %.1f cm",[arr[0][@"width_tf"]floatValue],[arr[0][@"height_tf"]floatValue]];
         _outside.text=[NSString stringWithFormat:@"Overall Image Size(approx): %.1fx%.1f cm",[outDic[@"width"] floatValue],[outDic[@"height"] floatValue]];
    }
    else{

        
        _visible.text=[NSString stringWithFormat:@"Image Size: %.1f x %.1f inch", floor([arr[0][@"width_tf"] floatValue]/2.5) ,floor([arr[0][@"height_tf"] floatValue]/2.5)];
        _outside.text=[NSString stringWithFormat:@"Overall Image Size(approx): %.1fx%.1f inch",[outDic[@"width"] floatValue]/2.5,[outDic[@"height"] floatValue]/2.5];
    }
    float imageSize = [arr[0][@"width_tf"] floatValue]>[arr[0][@"height_tf"] floatValue]?[arr[0][@"width_tf"] floatValue]:[arr[0][@"height_tf"] floatValue];
    if (imageSize < 30) {
        self.topImage.image = [UIImage imageNamed:@"bg_wall.jpg"];
    }else if (imageSize <= 60) {
        self.topImage.image = [UIImage imageNamed:@"bg_table.jpg"];
    }else if (imageSize > 60){
        self.topImage.image = [UIImage imageNamed:@"bg_sofa.jpg"];
    }
    
     UINib *nib = [UINib nibWithNibName:@"SummaryImagePreviewCell" bundle:nil];
     [_collectionView registerNib:nib forCellWithReuseIdentifier:@"SummaryImagePreviewCell"];
     _collectionView.delegate = self;
     _collectionView.dataSource = self;
     [_collectionView reloadData];
      
     
//    _outside.text=[NSString stringWithFormat:@"Overall (approx): %@ x %@ cm",temDic[@"width"],temDic[@"height"]];
   
     
      
     

    
    
    
    //DO NOT DELETE IT
    
   // _insideView.backgroundColor=[UIColor redColor];
    
   // _insideView.alpha=0.9;
    
//
//    float topMar,bottomMar,rightMar,leftMar;
//    if ([arr[0][@"style"] isEqualToString:@"none"]) {
//        topMar=0;
//        bottomMar=0;
//        leftMar=0;
//        rightMar=0;
//    }
//    else{
//        topMar=[arr[0][@"top"] floatValue]+5;
//        bottomMar=[arr[0][@"bottom"] floatValue]+5;
//        leftMar=[arr[0][@"left"] floatValue]+5;
//        rightMar=[arr[0][@"right"] floatValue]+10;
//    }
//    NSMutableArray *frmArr=[[NSMutableArray alloc] init];
//    frmArr=[db getDataForField:FRAME_TABLE where:[NSString stringWithFormat:@"id='%@'",arr[0][@"frame"]]];
//
//
//    NSMutableDictionary *sizeDic=[self getGlassWidthWhereInfoDic:arr[0]];
//     //Top Width
//    float margine  = (([frmArr[0][@"width"] floatValue])*2)-(([frmArr[0][@"rebate"] floatValue])*2);
//    sizeDic[@"outWidth"]       =[NSString stringWithFormat:@"%.01f",[sizeDic[@"width"]floatValue]+margine];
//    sizeDic[@"outHeight"]       =[NSString stringWithFormat:@"%.01f",[sizeDic[@"height"]floatValue]+margine];
//
//
//
//
//    CGRect lineFrom;
//    lineFrom.origin.x=0;
//    lineFrom.origin.y=-10;
//
//    _framePreviewWidthLbl.frame=CGRectMake(lineFrom.origin.x+10, lineFrom.origin.y-20, 20, 20);
//    _framePreviewWidthLbl.text=[NSString stringWithFormat:@"%@ cm",sizeDic[@"outWidth"]] ;
//    [_framePreviewWidthLbl sizeToFit];
//
//
//    CGRect lineTo;
//    lineTo.origin.x=_insideViewWidth.constant;
//    lineTo.origin.y=-10;
//
//    [Global drawLineFromPoint:lineFrom toPoint:lineTo whereLineWidth:0.5 andLineColour:[UIColor blackColor] atBaseView:_insideView];
//
//    [self drawTipLineWherePosition:lineFrom isVerTical:YES];
//    [self drawTipLineWherePosition:lineTo   isVerTical:YES];
//
//
//
//   //Inside width
//    lineFrom.origin.x=37+topMar;
//    lineFrom.origin.y=_insideViewHeight.constant/2;
//
//       _insideWidthLbl.frame=CGRectMake(lineFrom.origin.x+10, lineFrom.origin.y-20, 20, 20);
//       _insideWidthLbl.text=[NSString stringWithFormat:@"%@ cm",sizeDic[@"width"]] ;;
//       [_insideWidthLbl sizeToFit];
//
//     lineTo.origin.x=(_insideViewWidth.constant-35)-leftMar;
//     lineTo.origin.y=_insideViewHeight.constant/2;
//
//      [Global drawLineFromPoint:lineFrom toPoint:lineTo whereLineWidth:0.5 andLineColour:[UIColor blackColor] atBaseView:_insideView];
//
//    [self drawTipLineWherePosition:lineFrom isVerTical:YES];
//    [self drawTipLineWherePosition:lineTo   isVerTical:YES];
//
//
//
//    //Top Height
//
//    lineFrom.origin.x=_insideViewWidth.constant+10;
//    lineFrom.origin.y=0;
//
//   lineTo.origin.x=_insideViewWidth.constant+10;
//   lineTo.origin.y=_insideViewHeight.constant;
//
//    _framePreViewHeightLbl.frame=CGRectMake(lineTo.origin.x+10, lineTo.origin.y-20, 20, 20);
//    _framePreViewHeightLbl.text=[NSString stringWithFormat:@"%@ cm",sizeDic[@"outHeight"]] ;
//    [_framePreViewHeightLbl sizeToFit];
//
//    [Global drawLineFromPoint:lineFrom toPoint:lineTo whereLineWidth:0.5 andLineColour:[UIColor blackColor] atBaseView:_insideView];
//
//    [self drawTipLineWherePosition:lineFrom isVerTical:NO];
//    [self drawTipLineWherePosition:lineTo   isVerTical:NO];
//
//
//   //Bottom Height
//          lineFrom.origin.x=_insideViewWidth.constant-35-rightMar;
//         lineFrom.origin.y=37+topMar;
//
//        lineTo.origin.x=_insideViewWidth.constant-35-rightMar;
//        lineTo.origin.y=_insideViewHeight.constant-37-bottomMar;
//
//       _insiideHeightLbl.frame=CGRectMake(lineTo.origin.x-30, lineTo.origin.y-20, 20, 20);
//       _insiideHeightLbl.text=[NSString stringWithFormat:@"%@ cm",sizeDic[@"height"]] ;
//       [_insiideHeightLbl sizeToFit];
//
//
//         [Global drawLineFromPoint:lineFrom toPoint:lineTo whereLineWidth:0.5 andLineColour:[UIColor blackColor] atBaseView:_insideView];
//
//    [self drawTipLineWherePosition:lineFrom isVerTical:NO];
//       [self drawTipLineWherePosition:lineTo   isVerTical:NO];
 
    
}

-(NSMutableDictionary *)getGlassWidthWhereInfoDic:(NSMutableDictionary *)infoDic{
    NSMutableDictionary * price=[[NSMutableDictionary alloc] init];
    float width =[infoDic[@"width_tf"] floatValue];
    float height=[infoDic[@"height_tf"] floatValue];
    if (!([infoDic[@"style"] isEqualToString:@"none"])) {
        width=width+[infoDic[@"right"]floatValue ]+[infoDic[@"left"]floatValue ];
        height=height+[infoDic[@"top"]floatValue ]+[infoDic[@"bottom"]floatValue ];
        if ([infoDic[@"style"] isEqualToString:@"Double"]){
            width=width+([infoDic[@"bottomUniformWidth"] floatValue]*2);
            height=height+([infoDic[@"bottomUniformWidth"] floatValue]*2);
        }
        
    }
    price[@"width"] =[NSString stringWithFormat:@"%.01f",width];
    price[@"height"]=[NSString stringWithFormat:@"%.01f",height];
    return price;
}


-(void)drawTipLineWherePosition:(CGRect)pointOfTip isVerTical:(BOOL)isVer{
    
    CGRect lineFrom, lineTo;
       
    if (isVer) {
          lineFrom.origin.x=pointOfTip.origin.x;
          lineFrom.origin.y=pointOfTip.origin.y-5;
             
          lineTo.origin.x=pointOfTip.origin.x;
          lineTo.origin.y=pointOfTip.origin.y+5;
    }
    else{
          lineFrom.origin.x=pointOfTip.origin.x-5;
          lineFrom.origin.y=pointOfTip.origin.y;
             
          lineTo.origin.x=pointOfTip.origin.x+5;
          lineTo.origin.y=pointOfTip.origin.y;
    }
  
       
       [Global drawLineFromPoint:lineFrom toPoint:lineTo whereLineWidth:0.5 andLineColour:[UIColor blackColor] atBaseView:_insideView];
    
}

-(UIColor*)getCheckMarkWhereBgColor:(NSString *)hexString{
    
    
    unsigned rgbValue = 0;
       NSScanner *scanner = [NSScanner scannerWithString:hexString];
       [scanner setScanLocation:1]; // bypass '#' character
       [scanner scanHexInt:&rgbValue];
    int r=((rgbValue & 0xFF0000) >> 16);
    int g=((rgbValue & 0xFF00) >> 8);
    int b=(rgbValue & 0xFF);
      
    int lightness = ((r*299)+(g*587)+(b*114))/1000; //get lightness value

    if (lightness < 127) { //127 = middle of possible lightness value
        return [UIColor whiteColor];
    }
    else  return [UIColor blackColor];
        

}

#pragma mark - UICollectionViewDataSource and Delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return imageArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *identifier = @"SummaryImagePreviewCell";
    SummaryImagePreviewCell *frameView =[_collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    //self.collectionView.frame.size.height
    frameView.imagePreviewContainer.image = [Global getMidiumSizeImage:imageArray[indexPath.row] imageSize:500];
    return frameView;
    
}

- (CGSize)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(self.collectionView.frame.size.width, self.collectionView.frame.size.height);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
}
- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath{
    self.pageController.currentPage = indexPath.row;
}
- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath{
    if (_pageController.currentPage == indexPath.row) {
        _pageController.currentPage = [_collectionView indexPathForCell:_collectionView.visibleCells.firstObject].row;
    }
}
-(UIImage*)splitImage:(UIImage*)image rectToCrop:(CGRect)rect{
    CGImageRef tmpImgRef = image.CGImage;
    CGImageRef topImgRef = CGImageCreateWithImageInRect(tmpImgRef, rect);
    UIImage *imageRect = [UIImage imageWithCGImage:topImgRef];
    CGImageRelease(topImgRef);
    return imageRect;
}
@end
