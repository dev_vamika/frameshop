//
//  FrameViewController.m
//  FrameShop
//
//  Created by Saurabh anand on 19/12/19.
//  Copyright © 2019 Mayank Barnwal. All rights reserved.
//

#import "FrameViewController.h"
#import "Global.h"
#import "SummaryCell.h"
@interface FrameViewController (){
  
    DbManager *db;
    BOOL isMenu,isLoaded;
    NSString *payablePrice;
    int rowCount;
    float heightConstent;
  
}

@end

@implementation FrameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _loader.hidden = YES;
   // self.navigationController.navigationBar.topItem.title = @"";
    rowCount=0;
    db=[[DbManager alloc] init];
    UISwipeGestureRecognizer * swipeleft=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeleft:)];
    swipeleft.direction=UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeleft];
    [Global roundCorner:_addToCartButton roundValue:_addToCartButton.frame.size.height/2 borderWidth:0.0 borderColor:UIColor.clearColor];
    [Global roundCorner:_cartCountLbl roundValue:_cartCountLbl.frame.size.height/2 borderWidth:2 borderColor:[UIColor whiteColor]];
    
    heightConstent=self.addTocartWidth.constant;
    UIImage *img=[Global getMidiumSizeImage:[Global getImageFromDoucumentWithName:@"preview"] imageSize:500];
    [Global saveImagesInDocumentDirectory:UIImagePNGRepresentation(img) ImageId:@"frame_preview"];
 int count=[[[NSUserDefaults standardUserDefaults] valueForKey:CART_COUNT] intValue];
    if (count>9) {
        _cartCountLbl.text=@"9+";
    }
    else{
        _cartCountLbl.text=[NSString stringWithFormat:@"%d",[[[NSUserDefaults standardUserDefaults] valueForKey:CART_COUNT] intValue]];
    }
    _cartCountLbl.hidden=[_cartCountLbl.text isEqualToString:@"0"];
    
    
    
    
    

  
 
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if (isMenu) {
        isMenu = NO;
    }
    rowCount=1;
  [_tableView reloadData];
   // [self getItemPrice];
  
     [self.navigationItem setBackBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil]];
}

-(void)updateAddToCartFrame{
    self.view.userInteractionEnabled = NO;
    _loader.hidden=NO;
    [_loader startAnimating];
    [_addToCartButton setTitle:@"" forState:UIControlStateNormal];
    [UIView animateWithDuration:0.5 animations:^{
        self->_addTocartWidth.constant = (self.addToCartButton.frame.size.height*2)-self.view.frame.size.width;
    }];
}
-(void)removeAddToCartLoader{
    [_addToCartButton setTitle:@"Add To Cart" forState:UIControlStateNormal];
    self.view.userInteractionEnabled = YES;
    _loader.hidden=YES;
    [_loader stopAnimating];
    [UIView animateWithDuration:0.5 animations:^{
        self->_addTocartWidth.constant = 0.0;
    }];
}

-(void)swipeleft:(UISwipeGestureRecognizer*)gestureRecognizer
{
      if ([gestureRecognizer locationInView:self.view].x>self.view.bounds.size.width*0.80) {
          isMenu=YES;
            [Global showSideMenu];
       }
}

-(void)sendDataToServerData{
    [self updateAddToCartFrame];
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    NSString *urlString=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,ADD_PRODUCT_CART];
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
    dic[@"url"]=urlString;
    
    NSMutableArray *arr=[[NSMutableArray alloc] init];
    arr=[db getDataForField:USERTABLE where:nil];

   

    if (arr.count!=0) {
        dic[@"token"]=arr[0][@"token"];
        
        NSMutableArray *tempArr=[[NSMutableArray alloc] init];
        NSMutableArray *frmArr=[[NSMutableArray alloc] init];
        NSMutableArray *proArr=[[NSMutableArray alloc] init];
        NSMutableArray *matArr=[[NSMutableArray alloc] init];
        
        tempArr=[db getDataForField:USER_PRODUCT where:nil];
        
        dic[@"linked_id"] =tempArr[0][@"id"];
        dic[@"order_type"]=@"product";
        
        dic[@"vgroove"]=[NSNumber numberWithBool:false];
        if ([tempArr[0][@"topMat"] isEqualToString:@"V Groove"]) {
            dic[@"vgroove"]=[NSNumber numberWithBool:true];
        }
        
        
        frmArr=[db getDataForField:FRAME_TABLE where:[NSString stringWithFormat:@"id='%@'",tempArr[0][@"frame"]]];
        dic[@"payable_amount"]=[NSString stringWithFormat:@"%.2f",[frmArr[0][@"base_price"] floatValue]] ;
        
        
        NSMutableDictionary *cartOrderDic=[[NSMutableDictionary alloc] init];
        proArr=[db getDataForField:PRODUCT_TABLE where:[NSString stringWithFormat:@"id='%@'",tempArr[0][@"id"]]];
        
        dic[@"payable_amount"]=[NSString stringWithFormat:@"%.2f",[payablePrice floatValue]]  ;
        
        cartOrderDic[@"thumb_url"]   =NULL;
        cartOrderDic[@"type"]        =proArr[0][@"sku"];
        cartOrderDic[@"category_id"] =frmArr[0][@"frame_category_id"];
        cartOrderDic[@"width"]       =tempArr[0][@"width_tf"];
        cartOrderDic[@"height"]       =tempArr[0][@"height_tf"];
        cartOrderDic[@"depth"]       =@"5";
        cartOrderDic[@"product_name"] = proArr[0][@"name"];
        
        NSMutableDictionary *sizeDic=[[NSMutableDictionary alloc] init];
        NSMutableDictionary *opDic=[[NSMutableDictionary alloc] init];
        NSMutableDictionary *glDic=[[NSMutableDictionary alloc] init];
        NSMutableDictionary *outDic=[[NSMutableDictionary alloc] init];
        
        opDic[@"width"]       =tempArr[0][@"width_tf"];
        opDic[@"height"]       =tempArr[0][@"height_tf"];
        sizeDic[@"opening"]=opDic;
        
        NSDictionary * temDic=[self getGlassWidthWhereInfoDic:tempArr[0]];
        glDic[@"width"]       =temDic[@"width"];
        glDic[@"height"]       =temDic[@"height"];
        sizeDic[@"glass"]=glDic;
        
        float margine  = (([frmArr[0][@"width"] floatValue])*2)+(([frmArr[0][@"rebate"] floatValue])*2);
        outDic[@"width"]       =[NSString stringWithFormat:@"%0.2f",[temDic[@"width"]floatValue]+margine];
        outDic[@"height"]       =[NSString stringWithFormat:@"%0.2f",[temDic[@"height"]floatValue]+margine];
        sizeDic[@"outside"]=outDic;
        
        cartOrderDic[@"size"]       =sizeDic;
        
        NSMutableDictionary *imgDic=[[NSMutableDictionary alloc] init];
        imgDic[@"url"]        =NULL;
        imgDic[@"name"]       =NULL;
        imgDic[@"paper"]      =@"LUSTER";
        
        cartOrderDic[@"image"]       =imgDic;
        
        cartOrderDic[@"frame"]      =frmArr[0][@"sku"];
        cartOrderDic[@"slip"]       =NULL;
        cartOrderDic[@"fillet"]     =NULL;
        cartOrderDic[@"backing"]    =@"SELF_ADHESIVE_FOAMCORE";
        cartOrderDic[@"glass"]      =@"PERSPEX";
        cartOrderDic[@"location"]      =frmArr[0][@"location"];
        NSMutableDictionary *matDic=[[NSMutableDictionary alloc] init];
        NSMutableDictionary *topDic=[[NSMutableDictionary alloc] init];
        NSMutableDictionary *botDic=[[NSMutableDictionary alloc] init];
        if (!([tempArr[0][@"style"] isEqualToString:@"none"])) {
            
            topDic[@"top"]=tempArr[0][@"top"];
            topDic[@"left"]=tempArr[0][@"left"];
            topDic[@"bottom"]=tempArr[0][@"bottom"];
            topDic[@"right"]=tempArr[0][@"right"];
            
            
            
            matArr=[db getDataForField:METABOARD_TABLE where:[NSString stringWithFormat:@"id='%@'",tempArr[0][@"topMatBoard"]]];
            topDic[@"code"]=matArr[0][@"code"];
            topDic[@"name"]=matArr[0][@"name"];
            topDic[@"location"]= matArr[0][@"location"];
            
            if ([tempArr[0][@"style"] isEqualToString:@"Double"]){
                botDic[@"top"]=tempArr[0][@"bottomUniformWidth"];
                botDic[@"left"]=tempArr[0][@"bottomUniformWidth"];
                botDic[@"bottom"]=tempArr[0][@"bottomUniformWidth"];
                botDic[@"right"]=tempArr[0][@"bottomUniformWidth"];
                [matArr removeAllObjects];
                matArr=[db getDataForField:METABOARD_TABLE where:[NSString stringWithFormat:@"id='%@'",tempArr[0][@"bottomMat"]]];
                botDic[@"code"]=matArr[0][@"code"];
                botDic[@"name"]=matArr[0][@"name"];
                botDic[@"location"]= matArr[0][@"location"];
                matDic[@"bottom_mat"]      =botDic;
            }
            
            
            
            matDic[@"top_mat"]        =topDic;
            //matDic[@"location"] = matArr[0][@"location"];
            
            cartOrderDic[@"matboards"]      =matDic;
            
        }
        
        
        
        
        
        dic[@"cart_order_info"] = cartOrderDic;
        
        
        [api sendPostRequst:dic];
    }
    
}

-(NSString *)getItemPrice{
     
        
        NSMutableArray *arr=[[NSMutableArray alloc] init];
        arr=[db getDataForField:USERTABLE where:nil];

       

        if (arr.count!=0) {
        

            NSMutableArray *tempArr=[[NSMutableArray alloc] init];
         
            
            tempArr=[db getDataForField:USER_PRODUCT where:nil];
             payablePrice=[NSString stringWithFormat:@" %.2f",[tempArr[0][@"price"] floatValue]] ;
            return payablePrice;;
        }
    return @"0";
}


-(NSMutableDictionary *)getGlassWidthWhereInfoDic:(NSMutableDictionary *)infoDic{
    NSMutableDictionary * price=[[NSMutableDictionary alloc] init];
    float width =[infoDic[@"width_tf"] floatValue];
    float height=[infoDic[@"height_tf"] floatValue];
    
    if (!([infoDic[@"style"] isEqualToString:@"none"])) {
        width=width+[infoDic[@"right"]floatValue ]+[infoDic[@"left"]floatValue ];
        height=height+[infoDic[@"top"]floatValue ]+[infoDic[@"bottom"]floatValue ];
        if ([infoDic[@"style"] isEqualToString:@"Double"]){
            width=width+([infoDic[@"bottomUniformWidth"] floatValue]*2);
            height=height+([infoDic[@"bottomUniformWidth"] floatValue]*2);
        }
        
    }
    price[@"width"] =[NSString stringWithFormat:@"%0.2f",width];
    price[@"height"]=[NSString stringWithFormat:@"%0.2f",height];
    return price;
}


-(void)signUpUser{
    
   
    
   
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
   dic[@"name"]=[[UIDevice currentDevice] name];
    dic[@"email"]=[NSString stringWithFormat:@"%@@frameshop.com.au",[Global getDeviceId]];
    dic[@"password"]=@"qwxcjeub32ne";
    dic[@"c_password"]=@"qwxcjeub32ne";
    dic[@"url"]=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,REGISTER_USER];
    [api sendPostRequst:dic];
    
}
-(void)logInUser{
   
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
   
   dic[@"email"]=[NSString stringWithFormat:@"%@@frameshop.com.au",[Global getDeviceId]];
    dic[@"password"]=@"qwxcjeub32ne";
   
    dic[@"url"]=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,LOGIN_USER];
    [api sendPostRequst:dic];
}

- (IBAction)addToCartAction:(id)sender {
    
   
    
    [[NSUserDefaults standardUserDefaults] setValue:nil forKey:CART_ID];
    [self sendDataToServerData];
}

- (IBAction)closeAction:(id)sender {
     [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return rowCount;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SummaryCell";
    SummaryCell *cell = (SummaryCell *)[_tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SummaryCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    UIImage *img=[Global getMidiumSizeImage:[Global getImageFromDoucumentWithName:@"preview"] imageSize:500];
    cell.imagePreview.image = img;
    UITapGestureRecognizer *singleFingerTap =
            [[UITapGestureRecognizer alloc] initWithTarget:self
                                                    action:@selector(handleSingleTap:)];
    [cell.imagePreview addGestureRecognizer:singleFingerTap];
   
    cell.sizeLbl.text = [NSString stringWithFormat:@"Price: $%@",[self getItemPrice]];
   
    cell.framePreviewImage.image = [Global getMidiumSizeImage:[Global getImageFromDoucumentWithName:@"frame_preview"] imageSize:cell.framePreviewImage.frame.size.width];
   
    [cell.viewMore addTarget:self action:@selector(onSelectViewMore) forControlEvents:UIControlEventTouchUpInside];
   
    [cell refreshCollectionView];
    return cell;
}
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
     [self performSegueWithIdentifier:@"zoomPreview" sender:[Global getImageFromDoucumentWithName:@"preview"]];
}

-(void)onSelectViewMore{
    [self performSegueWithIdentifier:@"viewdetail" sender:nil];

}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
      //  [self performSegueWithIdentifier:@"detail" sender:nil];
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
   NSArray *nibViews = [[NSBundle mainBundle] loadNibNamed:@"SummaryCell" owner:self options:nil];
    SummaryCell *sqr = [nibViews objectAtIndex:0];
   return sqr.frame.size.height;
    
     
}
#pragma mark - segue delegates
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
   if ([[segue identifier] isEqualToString:@"upload"]) {
        
    UploadImageViewController *albumContentsViewController = [segue destinationViewController];
    
    albumContentsViewController.delegate=self;
    

    }
    else  if ([[segue identifier] isEqualToString:@"viewdetail"]) {
           
           SummaryPopUpView *albumContentsViewController = [segue destinationViewController];
           
           albumContentsViewController.infoDic=(NSMutableDictionary *)sender;
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        
         SummaryCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        
        
           albumContentsViewController.thumbImage=cell.imagePreview.image;
           
       }else if([[segue identifier] isEqualToString:@"zoomPreview"]){
           ProductImagePreviewVC *productImagePreviewVC = [segue destinationViewController];
           NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
           dic[@"image"] = sender;
           productImagePreviewVC.previewImageDic = dic;
       }
//    else if([[segue identifier] isEqualToString:@"zoomPreview"]){
//        ProductImagePreviewVC *productImagePreviewVC = [segue destinationViewController];
//        
//        productImagePreviewVC.previewImage=[Global getImageFromDoucumentWithName:@"preview"];
//    }

}

#pragma mark - Upload delegates

-(void)didFinishUploading{
    //[self performSelector:@selector(subscribe) withObject:self afterDelay:3.0 ];
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:FINISH_UPLOADING object:self userInfo:nil];
    [self dismissViewControllerAnimated:NO completion:nil];
}
-(void)didFailUploading{
    
}

#pragma mark -Pop api Delegates

-(void)connectionError{
    isLoaded=YES;

}
-(void)requestFailedSessionExpired:(NSDictionary *)dataPack{
    isLoaded=YES;
 
}
-(void)requestFailedServerError:(NSDictionary *)dataPack{
    isLoaded=YES;
    
}
-(void)requestFailedServiceUnavailableDueToMaintenance:(NSDictionary *)dataPack{
    isLoaded=YES;
    self.view.userInteractionEnabled = YES;
       _loader.hidden=YES;
       [_loader stopAnimating];
       [_addToCartButton setTitle:@"Add To Cart" forState:UIControlStateNormal];
    
    
      [UIView animateWithDuration:0.5 animations:^{
          self->_addTocartWidth.constant = self->heightConstent;
        }];
    [Global showSimpleAlert:self andTitle:FRAMESHOP andMessage:dataPack[@"message"]];
    
}
-(void)requestFailedMethodNotFound:(NSDictionary *)dataPack{
    isLoaded=YES;
    
}
-(void)requestFailedNotFound:(NSDictionary *)dataPack{
    isLoaded=YES;
    
}
-(void)requestFailedUnauthorised:(NSDictionary *)dataPack{
    isLoaded=YES;
     [self logInUser];
  
}
-(void)requestFailedBadRequest:(NSDictionary *)dataPack{
    isLoaded=YES;
   
}
-(void)requestSucceededWithData:(NSDictionary *)dataPack{

    if ([dataPack[@"url"] isEqualToString:LOGIN_USER]) {
           
           [db delTableWithTableName:USERTABLE];
           NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
          
           dic[@"id"]              =[NSString stringWithFormat:@"%@",dataPack[@"data"][@"id"]];
           dic[@"name"]            =dataPack[@"data"][@"name"];
           dic[@"email"]           =dataPack[@"data"][@"email"];
           dic[@"created_at"]      =dataPack[@"data"][@"created_at"];
           dic[@"updated_at"]      =dataPack[@"data"][@"updated_at"];
           dic[@"token"]           =dataPack[@"data"][@"access_token"];
           
           [db insertIntoTable:dic table_name:USERTABLE];
           [self sendDataToServerData];
        }
       
       else if ([dataPack[@"url"] isEqualToString:REGISTER_USER]){
         
           NSMutableDictionary *dic =[[NSMutableDictionary alloc] init];
           dic[@"token"]            =dataPack[@"data"][@"token"];
           dic[@"id"]               =[NSString stringWithFormat:@"%@",dic[@"id"]];
             
           [db insertIntoTable:dic table_name:USERTABLE];
           [self sendDataToServerData];
       }
    
       else{
           [self removeAddToCartLoader];
           
           [[NSUserDefaults standardUserDefaults] setValue:dataPack[@"data"][@"cart_order_id"] forKey:CART_ID];
          
          [self performSegueWithIdentifier:@"upload" sender:nil];
           
           
           }
          
           
    
       
    
}
-(void)requestSucceededButActionFailed:(NSDictionary *)dataPack{
    [self removeAddToCartLoader];
     isLoaded=YES;
    
}
-(void)uploadedData:(NSString *)pecentage andByte:(float)bytes{
    
}
-(void)requestSucceededButNoDataFound:(NSDictionary *)dataPack{
    [self removeAddToCartLoader];
    isLoaded=YES;
    
}
-(void)requestFailedDueToServerIsuueWithError:(NSString*)errorDes{
   
}



- (IBAction)onclickCart:(id)sender {
    [Global callMenuViewWithCode:@"0"];
}
- (IBAction)onMenuClicked:(id)sender {
    isMenu=YES;
    [Global showSideMenu];
}
@end
