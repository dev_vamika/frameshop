//
//  FrameSettingViewController.h
//  FrameShop
//
//  Created by Saurabh anand on 20/12/19.
//  Copyright © 2019 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MatsSubView.h"



#import "ChooseFrameViewController.h"
#import "OptionViewController.h"
#import "PopApi.h"
#import "DBManager.h"
#import "CreatFrameScript.h"
#import "SearchViewController.h"
#import "FrameViewController.h"
#import "OptionPopUpVC.h"
NS_ASSUME_NONNULL_BEGIN

@interface FrameSettingViewController : UIViewController<ChooseFrameViewControllerDelegate,OptionViewControllerDelegate,MatsSubViewDelegate,PopApiDelegate,SearchViewControllerDelegate,FrameViewControllerDelegate,OptionPopUpVCDelegate,CreatFrameScriptDelegate>
@property (weak, nonatomic) IBOutlet UIButton *nxtBtrn;

//@property (weak, nonatomic) IBOutlet UIScrollView *horizontalScroll;
@property (weak, nonatomic) IBOutlet UIImageView *mainImage;

- (IBAction)nextAction:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *cartCount;
@property (weak, nonatomic) IBOutlet UILabel *priceCount;
- (IBAction)cartBtn:(id)sender;
- (IBAction)menuBtn:(id)sender;


@property (weak, nonatomic) IBOutlet UIView *frameView;
@property NSString *cameFrom;
@property (weak, nonatomic) IBOutlet UIView *settingView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loaderActivity;
@property (weak, nonatomic) IBOutlet UIView *priceView;
@property (weak, nonatomic) IBOutlet UIImageView *dotImage;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *priceLoader;


@end

NS_ASSUME_NONNULL_END
