//
//  ProductImagePreviewVC.h
//  FrameShop
//
//  Created by vamika on 12/06/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ProductImagePreviewVC : UIViewController
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *imageLoader;
@property NSMutableDictionary *previewImageDic;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;
- (IBAction)onclickDone:(id)sender;
@end

NS_ASSUME_NONNULL_END
