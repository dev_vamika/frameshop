//
//  OptionPopUpVC.m
//  FrameShop
//
//  Created by Vamika on 13/05/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import "OptionPopUpVC.h"
#import "SortingTableCell.h"
@interface OptionPopUpVC ()
{
    NSIndexPath *currentIndex;
    NSString *titleName;
}
@end

@implementation OptionPopUpVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [Global roundCorner:_optionTable roundValue:10.0 borderWidth:0.0 borderColor:UIColor.clearColor];
    NSMutableDictionary *strDic =  _optionArray[0];
    if ([strDic isKindOfClass:[NSString class]]) {
        titleName = @"Sorting";
        _tableHeight.constant = 220;
    }else{
        titleName = @"Categories";
        _tableHeight.constant = self.view.frame.size.height*0.7;
    }
    UITapGestureRecognizer *singleFingerTap =
         [[UITapGestureRecognizer alloc] initWithTarget:self
                                                 action:@selector(handleSingleTap:)];
       [self.backView addGestureRecognizer:singleFingerTap];
    
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    [self.delegate cancelPoopUp];
    [self dismissViewControllerAnimated:NO completion:nil];
}
#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    if ([titleName isEqualToString:@"Sorting"]) {
        return self.optionArray.count;
//    }
//    return 11;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellOne";
    SortingTableCell *cell = (SortingTableCell *)[_optionTable dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SortingTableCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    NSString *str;
     NSMutableDictionary *strDic =  _optionArray[0];
    if ([strDic isKindOfClass:[NSString class]]) {
        cell.titleLabel.text = _optionArray[indexPath.row];
        str=[NSString stringWithFormat:@"%@",  [[NSUserDefaults standardUserDefaults] valueForKey:FRAME_SORT_ID]];
       
        currentIndex=[NSIndexPath indexPathForRow:[str intValue] inSection:0];
        
        
    }else{
        cell.titleLabel.text = _optionArray[indexPath.row][@"name"];
        str=[NSString stringWithFormat:@"%@",  [[NSUserDefaults standardUserDefaults] valueForKey:FRAME_CAT_ID]];
        if ([str isEqualToString:[NSString stringWithFormat:@"%@",_optionArray[indexPath.row][@"id"]] ]) {
                   currentIndex=indexPath;
               }
        
    }
    
    

    
   // if ((indexPath.row != currentIndex.row) ||  (currentIndex == nil)) {
    if (currentIndex == nil) {
        cell.imageRadioView.image = [UIImage imageNamed:@"unselected_radio_icon.png"];
    }else{
        if (indexPath.row == currentIndex.row) {
             cell.imageRadioView.image = [UIImage imageNamed:@"selected_radio_icon.png"];
        
         }else{
              cell.imageRadioView.image = [UIImage imageNamed:@"unselected_radio_icon.png"];
         }
        
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
//- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
//   return YES;
//}
//
//- (void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
//  // Add your Colour.
//    SortingTableCell *cell = (SortingTableCell *)[tableView cellForRowAtIndexPath:indexPath];
//    cell.imageRadioView.image = [UIImage imageNamed:@"selected_radio_icon.png"];
//}
//
//- (void)tableView:(UITableView *)tableView didUnhighlightRowAtIndexPath:(NSIndexPath *)indexPath {
//  // Reset Colour.
//    if (indexPath.row == currentIndex.row) {
//        return;
//    }
//    SortingTableCell *cell = (SortingTableCell *)[tableView cellForRowAtIndexPath:indexPath];
//    cell.imageRadioView.image = [UIImage imageNamed:@"unselected_radio_icon.png"];
//
//}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    currentIndex = indexPath;
    [_optionTable reloadData];
    [self dismissViewControllerAnimated:NO completion:^{
        if (![self->titleName isEqualToString:@"Sorting"]) {
            [[NSUserDefaults standardUserDefaults] setValue:self->_optionArray[indexPath.row][@"id"] forKey:FRAME_CAT_ID];
            [self.delegate didPressCategoryWithOptionDic: self->_optionArray[indexPath.row]];
            
        }else{
            [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%d",(int)indexPath.row] forKey:FRAME_SORT_ID];
            [self.delegate didPressSortingWithOptionDic:indexPath];
        }
    }];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
     if(section == 0){
    return 60.0f;
     }
    return 0.0f;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(section == 0){
        UIView* headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 60)];
        headerView.backgroundColor = UIColor.whiteColor;
        UILabel* headerLabel = [[UILabel alloc] init];
        UIButton* closeButton = [[UIButton alloc] init];
        headerLabel.frame = headerView.frame;
        closeButton.frame = CGRectMake(headerView.frame.size.width - 35, 17, 25, 25);
        [closeButton setImage:[UIImage imageNamed:@"close_white_icon.png"] forState:UIControlStateNormal];
        
        [closeButton addTarget:self action:@selector(closePopUPWindow) forControlEvents:UIControlEventTouchUpInside];
        headerLabel.backgroundColor = [UIColor clearColor];
        headerLabel.textColor = [UIColor whiteColor];
        headerLabel.font = self.sampleLbl.font;
        headerLabel.text = titleName;
        headerLabel.textAlignment = NSTextAlignmentCenter;
        headerLabel.numberOfLines = 2;
        [headerView addSubview:headerLabel];
        [headerView addSubview:closeButton];
        headerView.backgroundColor = APP_BLUE_COLOR;
        return headerView;
    }
    return nil;
}
-(void) closePopUPWindow{
    [self.delegate cancelPoopUp];
    [self dismissViewControllerAnimated:NO completion:nil];
}
//- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
//{
//    return 40.0;
//}
//- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
//    UIView *backVIEW = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.optionTable.frame.size.width, 40)];
//    backVIEW.backgroundColor = UIColor.whiteColor;
//    UILabel *titleLbl = [[UILabel alloc] initWithFrame:CGRectMake((self.optionTable.frame.size.width/2)-50, 0, 100, 30)];
//    titleLbl.font = [UIFont fontWithName:self.sampleLbl.font.familyName size:15.0];
//    titleLbl.text = @"OK";
//    titleLbl.textAlignment = NSTextAlignmentCenter;
//    titleLbl.textColor = [UIColor whiteColor];
//    titleLbl.backgroundColor = APP_BLUE_COLOR;
//    [Global roundCorner:titleLbl roundValue:15.0 borderWidth:0.0 borderColor:UIColor.clearColor];
//    UITapGestureRecognizer *singleFingerTap =
//    [[UITapGestureRecognizer alloc] initWithTarget:self
//                                            action:@selector(footerTap:)];
//    [backVIEW addGestureRecognizer:singleFingerTap];
//    [backVIEW addSubview:titleLbl];
//
//    return backVIEW;
//}
//- (void)footerTap:(UITapGestureRecognizer *)recognizer
//{
//    [self dismissViewControllerAnimated:YES completion:^{
//        if (![self->titleName isEqualToString:@"Sorting"]) {
//            [self.delegate didPressCategoryWithOptionDic: self->_optionArray[self->currentIndex.row]];
//        }else{
//            [self.delegate didPressSortingWithOptionDic:self->currentIndex];
//        }
//    }];
//}
@end
