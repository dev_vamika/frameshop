//
//  FrameSettingViewController.m
//  FrameShop
//
//  Created by Saurabh anand on 20/12/19.
//  Copyright © 2019 Mayank Barnwal. All rights reserved.
//

#import "FrameSettingViewController.h"
#import "Global.h"
//#define margineMain 5.0f


//#define margineMain ([UIScreen mainScreen].bounds.size.height == 568)?65.0f:5
@interface FrameSettingViewController (){
    
    BOOL isMenu;
    DbManager *db;
    CGRect mainImageFrame,singleViewFrame,singleViewImageFrame;
    NSMutableDictionary *productDic,*viewControllers;
    int imageCount;
    UIView *singleFrame,*doubleFrame,*vgrowView;
    UIColor *btnColor;
    UIImageView *singleMatImage,*doubleMatImage;
    CreatFrameScript *frameScript;
    NSTimer *timer;
    NSTimeInterval timeStamp;
    BOOL isFirstPrice;
    float margineMain;
}

@end

@implementation FrameSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Choose a Frame";
    // self.navigationController.navigationBar.topItem.title = @"";
    db             =[[DbManager alloc] init];
    productDic     =[[NSMutableDictionary alloc] init];
    viewControllers=[[NSMutableDictionary alloc] init];
    timeStamp=0;
    isFirstPrice=true;
    
    _priceView.frame = CGRectMake(self.view.frame.size.width -97, 10, 97, 43);
    
    CGRect bounds = _priceView.bounds;
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:bounds
                                                   byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerBottomLeft)
                                                         cornerRadii:CGSizeMake(_priceView.frame.size.height/2, _priceView.frame.size.height/2)];
    
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = bounds;
    maskLayer.path = maskPath.CGPath;
    
    _priceView.layer.mask = maskLayer;
    UITapGestureRecognizer *swipeLeft = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTaponPriceLabel:)];
    
    
  
    
   
    
    [self.priceView addGestureRecognizer:swipeLeft];
    
    
    [Global roundCorner:_cartCount roundValue:_cartCount.frame.size.height/2 borderWidth:2 borderColor:[UIColor whiteColor]];
    
    [self.navigationItem setBackBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil]];
    
    UISwipeGestureRecognizer * swipeleft=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeleft:)];
    swipeleft.direction=UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeleft];
    
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [self.mainImage addGestureRecognizer:singleFingerTap];
    [self.frameView addGestureRecognizer:singleFingerTap];
    
    
    
    productDic[@"frameStyle"]     =@"none";
    productDic[@"topMat"]         =@"none";
    productDic[@"imageOverlap"]   =@"NO";
    productDic[@"singleCoreColor"]=@"none";
    productDic[@"doubleCoreColor"]=@"none";
    productDic[@"uniformWidth"]   =@"5.1";
    productDic[@"customTop"]      =@"5.1";
    productDic[@"customBottom"]   =@"5.1";
    productDic[@"customLeft"]     =@"5.1";
    productDic[@"customRight"]    =@"5.1";
    productDic[@"frameId"]        =@"";
    productDic[@"doubleFrameWidth"] =@"0.5";
    
    
    NSMutableArray *arr=[db getDataForField:USER_PRODUCT where:nil];
    if (arr.count!=0) {
        
        arr[0][@"uniformWidth"]= @"5.1";
        arr[0][@"top"]=@"5.1";
        arr[0][@"bottom"]=@"5.1";
        arr[0][@"left"]=@"5.1";
        arr[0][@"right"]=@"5.1";
        arr[0][@"bottomUniformWidth"]=@"0.5";
        arr[0][@"style"]=@"none";
        arr[0][@"isUni"]=@"1";
        [db update:USER_PRODUCT :arr[0] :[NSString stringWithFormat:@"id='%@'",arr[0][@"id"]]];
        productDic[@"height_tf"] =arr[0][@"height_tf"];
        productDic[@"width_tf"] =arr[0][@"width_tf"];
        
    }
    
    
    
    
   // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishCart) name:CART_REFRESH object:nil];
    btnColor=_nxtBtrn.backgroundColor;
    _nxtBtrn.backgroundColor=[UIColor lightGrayColor];
    _nxtBtrn.enabled=NO;
    
    singleFrame=[[UIView alloc] init];
    doubleFrame=[[UIView alloc] init];
    
    [self setUpViews];
    [[NSUserDefaults standardUserDefaults] setValue:@"pop" forKey:FRAME_CAT_ID];
    [[NSUserDefaults standardUserDefaults] setValue:@"0" forKey:FRAME_SORT_ID];
    //[Global deleteFilesFromCacheWhichContain:@"frameBar"];
    frameScript=[[CreatFrameScript alloc] init];
    frameScript.parentView=_frameView;
    frameScript.delegate=self;
    _priceCount.frame=CGRectMake(10, 0, _priceView.frame.size.width-20, _priceView.frame.size.height);
    _dotImage.frame=CGRectMake(8, (_priceView.frame.size.height-_dotImage.frame.size.height)/2, _dotImage.frame.size.width, _dotImage.frame.size.height);
    _priceLoader.frame=CGRectMake((_priceView.frame.size.width-_priceLoader.frame.size.width)/2, (_priceView.frame.size.height-_priceLoader.frame.size.height)/2, _priceLoader.frame.size.width, _priceLoader.frame.size.height);
    _priceLoader.hidden=YES;
    _priceCount.text=@"$0";
    _dotImage.hidden = YES;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if (!isMenu) {
        
        
    }
    else{
        isMenu=NO;
    }
    int count=[[[NSUserDefaults standardUserDefaults] valueForKey:CART_COUNT] intValue];
    if (count>9) {
        _cartCount.text=@"9+";
    }
    else{
        _cartCount.text=[NSString stringWithFormat:@"%d",[[[NSUserDefaults standardUserDefaults] valueForKey:CART_COUNT] intValue]];
    }
    _cartCount.hidden=[_cartCount.text isEqualToString:@"0"];
}



-(void)callPriceApi{
    _priceCount.text=@"";
    [_priceLoader startAnimating];
    _priceLoader.hidden=false;
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    NSString *urlString=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,GET_PRICE];
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
    dic[@"url"]=urlString;
    
    NSMutableArray *arr=[[NSMutableArray alloc] init];
    arr=[db getDataForField:USERTABLE where:nil];
    
    _nxtBtrn.backgroundColor=[UIColor lightGrayColor];
    _nxtBtrn.enabled=NO;
    
    if (arr.count!=0) {
        dic[@"token"]=arr[0][@"token"];
        
        NSMutableArray *tempArr=[[NSMutableArray alloc] init];
        NSMutableArray *frmArr=[[NSMutableArray alloc] init];
        NSMutableArray *proArr=[[NSMutableArray alloc] init];
        NSMutableArray *matArr=[[NSMutableArray alloc] init];
        
        tempArr=[db getDataForField:USER_PRODUCT where:nil];
        
        
        tempArr[0][@"price"] =  @"0";
        [db update:USER_PRODUCT :tempArr[0] :[NSString stringWithFormat:@"id='%@'",tempArr[0][@"id"]]];
        
        
        proArr=[db getDataForField:PRODUCT_TABLE where:[NSString stringWithFormat:@"id='%@'",tempArr[0][@"id"]]];
        frmArr=[db getDataForField:FRAME_TABLE where:[NSString stringWithFormat:@"id='%@'",tempArr[0][@"frame"]]];
        
        dic[@"type"]=@"product";
        dic[@"quantity"]=@"1";
        dic[@"product_type"]=/*proArr[0][@"sku"]*/ FRAME_SKU;
        NSMutableDictionary *cartOrderDic=[[NSMutableDictionary alloc] init];
        
        cartOrderDic[@"image_width"]        =tempArr[0][@"width_tf"];
        cartOrderDic[@"image_height"]       =tempArr[0][@"height_tf"];
        cartOrderDic[@"frame"]             =frmArr[0][@"sku"];
        cartOrderDic[@"opening_type"]      =@"PHOTO";
        cartOrderDic[@"hanger"]      =@"HANGER_STANDBACK";
        cartOrderDic[@"glass"]      =@"PERSPEX";
        cartOrderDic[@"backing"]      =@"SELF_ADHESIVE_FOAMCORE";
        cartOrderDic[@"paper"]      =@"LUSTER";
        cartOrderDic[@"is_pack"]      =@(false);
        
        
        NSDictionary * temDic=[self getGlassWidthWhereInfoDic:tempArr[0]];
        
        if ([temDic[@"width"] floatValue]<21 && [temDic[@"height"] floatValue]<21 ) {
            cartOrderDic[@"hanger"]      = @"Hanger + Standback";
        }
        else if ([temDic[@"width"] floatValue]<21 && [temDic[@"height"] floatValue]<29.7 ) {
            cartOrderDic[@"hanger"]      =@"Hanger + Standback";
        }else{
            cartOrderDic[@"hanger"]      =@"D_RINGS_STRING";
        }
        
        
        cartOrderDic[@"vgroove"]=[NSNumber numberWithBool:false];
        if ([tempArr[0][@"topMat"] isEqualToString:@"V Groove"]) {
            cartOrderDic[@"vgroove"]=[NSNumber numberWithBool:true];
        }
        
        cartOrderDic[@"is_pack"]=[NSNumber numberWithBool:false];
        
        
        
        
        
        
        NSMutableDictionary *topDic=[[NSMutableDictionary alloc] init];
        NSMutableDictionary *botDic=[[NSMutableDictionary alloc] init];
        if (!([tempArr[0][@"style"] isEqualToString:@"none"]) && !([tempArr[0][@"style"] isEqualToString:@""])) {
            
            topDic[@"top"]=tempArr[0][@"top"];
            topDic[@"left"]=tempArr[0][@"left"];
            topDic[@"bottom"]=tempArr[0][@"bottom"];
            topDic[@"right"]=tempArr[0][@"right"];
            
            
            
            matArr=[db getDataForField:METABOARD_TABLE where:[NSString stringWithFormat:@"id='%@'",tempArr[0][@"topMatBoard"]]];
            topDic[@"code"]=matArr[0][@"code"];
            topDic[@"name"]=matArr[0][@"name"];
            cartOrderDic[@"top_mat"]      =topDic;
            
            if ([tempArr[0][@"style"] isEqualToString:@"Double"]){
                botDic[@"top"]=tempArr[0][@"bottomUniformWidth"];
                botDic[@"left"]=tempArr[0][@"bottomUniformWidth"];
                botDic[@"bottom"]=tempArr[0][@"bottomUniformWidth"];
                botDic[@"right"]=tempArr[0][@"bottomUniformWidth"];
                [matArr removeAllObjects];
                matArr=[db getDataForField:METABOARD_TABLE where:[NSString stringWithFormat:@"id='%@'",tempArr[0][@"bottomMat"]]];
                if (matArr.count!=0) {
                    botDic[@"code"]=matArr[0][@"code"];
                    botDic[@"name"]=matArr[0][@"name"];
                    cartOrderDic[@"bottom_mat"]      =botDic;
                }
                
            }
            
        }
        
        
        
        dic[@"data"] =cartOrderDic;
        NSLog(@"caling price api");
        timeStamp=0;
        [timer invalidate];
        [api sendPostRequst:dic];
    }
}

-(void)getItemPrice{
    
    _priceCount.text=@"";
    [_priceLoader startAnimating];
    _priceLoader.hidden=false;
    _nxtBtrn.backgroundColor=[UIColor lightGrayColor];
    _nxtBtrn.enabled=NO;
    
    if (isFirstPrice) {
        [self callPriceApi];
        isFirstPrice=false;
        return;
    }
    
    timeStamp = [[NSDate date] timeIntervalSince1970];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:self->timeStamp];
        
        
        NSTimeInterval difference = [[NSDate date] timeIntervalSinceDate:date];
        
        
        if (difference<1 ){
            NSLog(@"Halt %f",difference);
            return;
        }
        else{
            [self callPriceApi];
        }
    });
    
    
    
    
    
    
}

-(NSMutableDictionary *)getGlassWidthWhereInfoDic:(NSMutableDictionary *)infoDic{
    NSMutableDictionary * price=[[NSMutableDictionary alloc] init];
    float width =[infoDic[@"width_tf"] floatValue];
    float height=[infoDic[@"height_tf"] floatValue];
    if (!([infoDic[@"style"] isEqualToString:@"none"])) {
        width=width+[infoDic[@"right"]floatValue ]+[infoDic[@"left"]floatValue ];
        height=height+[infoDic[@"top"]floatValue ]+[infoDic[@"bottom"]floatValue ];
        if ([infoDic[@"style"] isEqualToString:@"Double"]){
            width=width+([infoDic[@"bottomUniformWidth"] floatValue]*2);
            height=height+([infoDic[@"bottomUniformWidth"] floatValue]*2);
        }
        
    }
    price[@"width"] =[NSString stringWithFormat:@"%0.2f",width];
    price[@"height"]=[NSString stringWithFormat:@"%0.2f",height];
    return price;
}

- (void)handleSingleTaponPriceLabel:(UITapGestureRecognizer *)recognizer
{
    
    if (_priceView.frame.size.width == 20) {
        _dotImage.hidden = YES;
        _priceView.frame = CGRectMake(self.view.frame.size.width -97, 10, 97, 43);
        _priceCount.frame=CGRectMake(10, 0, _priceView.frame.size.width-10, _priceView.frame.size.height);
        _priceCount.textAlignment = NSTextAlignmentCenter;
        
        
        [UIView animateWithDuration:1
                         animations:^{
            [self.view layoutIfNeeded];
        }];
    }else{
        _dotImage.hidden = NO;
        _priceView.frame = CGRectMake(self.view.frame.size.width -20, 10, 20, 43);
        
        
        
        [UIView animateWithDuration:1
                         animations:^{
            [self.view layoutIfNeeded];
        }];
    }
    
}
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    if (isFirstPrice) {
        return;
    }
    [self getFramePreviewImage];
    [self performSegueWithIdentifier:@"framezoom" sender:[Global getImageFromDoucumentWithName:@"preview"]];
}

-(void)closeAllOpenOption{
    ChooseFrameViewController *viewControler=(ChooseFrameViewController *)viewControllers[@"frame"];
    [viewControler closeAllOption];
}

-(void)didFinishCart{
    int count=[[[NSUserDefaults standardUserDefaults] valueForKey:CART_COUNT] intValue];
    if (count>9) {
        _cartCount.text=@"9+";
    }
    else{
        _cartCount.text=[NSString stringWithFormat:@"%d",[[[NSUserDefaults standardUserDefaults] valueForKey:CART_COUNT] intValue]];
    }
    _cartCount.hidden=[_cartCount.text isEqualToString:@"0"];
}
-(void)swipeleft:(UISwipeGestureRecognizer*)gestureRecognizer
{
    if ([gestureRecognizer locationInView:self.view].x>self.view.bounds.size.width*0.80) {
        [self closeAllOpenOption];
        isMenu=YES;
        [Global showSideMenu];
    }
}

-(void)setUpViews{
    
    
    _frameView.frame=CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height*0.35);
    _nxtBtrn.frame  =CGRectMake(23, self.view.bounds.size.height-115, self.view.bounds.size.width-46, 41);
    
    [Global roundCorner:_nxtBtrn roundValue:_nxtBtrn.frame.size.height/2 borderWidth:0.0 borderColor:[UIColor whiteColor]];
    
    if (@available(iOS 11.0, *)) {
        
        
        
        
        for (UIWindow *window in [UIApplication sharedApplication].windows) {
            
            
            if (window.isKeyWindow) {
                CGFloat bottomPadding = window.safeAreaInsets.bottom;
                if (bottomPadding!=0) {
                    _nxtBtrn.frame=CGRectMake(23, self.view.bounds.size.height-bottomPadding-141, self.view.bounds.size.width-46, 41);
                }
            }
            
        }
        
        
        
        
    }
    
    
    float barWidth=[self getBarWidthWhereParentView:_frameView];
    
    BG_THREAD
    UIImage *img=[Global getImageFromDoucumentWithName:@"frame"];
    MAIN_THREAD

        if (img!=nil) {
       
        float imageHeight,imageWidth;
        
            imageHeight=[self->productDic[@"height_tf"] floatValue];
            imageWidth=[self->productDic[@"width_tf"] floatValue];
        
        if (isiPhone6 && imageWidth>imageHeight) {
            self->margineMain=100;
        }
        else if(isiPhone5s){
            self->margineMain=100;
        }
        else{
            self->margineMain=65;
        }
        
        float ratio=img.size.height/img.size.width;
        //float ratio=imageHeight/imageWidth;
        if (imageHeight<imageWidth) {
            
            imageWidth=self->_frameView.frame.size.width-(self->margineMain)-(barWidth *2);
            imageHeight=imageWidth*ratio;
        }
        
        
        else {
            imageHeight=self->_frameView.frame.size.height-self->margineMain-(barWidth *2);
            imageWidth=imageHeight/ratio;
        }
        
        self->_mainImage.frame=CGRectMake(0, 0, imageWidth, imageHeight);
            self->_mainImage.image=[Global getMidiumSizeImage:img imageSize:[UIScreen mainScreen].bounds.size.height];
        self->_mainImage.center=self->_frameView.center;
        
        
        float settingViewHeight=self.nxtBtrn.frame.origin.y-self->_frameView.frame.size.height;
        // self->_settingView.backgroundColor=[UIColor redColor];
        
        self->_settingView.frame=CGRectMake(0, self->_frameView.frame.size.height, self.view.bounds.size.width, settingViewHeight);
            self->mainImageFrame=self->_mainImage.frame;
        [self setSettingsViewWhereIndex:0];
        
    }
    self->_loaderActivity.center=self.frameView.center;
    [self->_loaderActivity stopAnimating];
    self->_loaderActivity.hidden=YES;
    self->_loaderActivity.backgroundColor=[UIColor whiteColor];
    [Global roundCorner:self->_loaderActivity roundValue:2 borderWidth:0 borderColor:nil];
    END_BLOCK
    END_BLOCK
}



-(float)getScallDownFactorWhereParentView:(UIView *)parentView{
    
    
    NSMutableArray *arr=[db getDataForField:USER_PRODUCT where:nil];
    float workSpaceWidth=parentView.bounds.size.width-60;
    float frameWidth=0.0f;
    float matWidth=0.0f;
    float doubleMatWidth=0.0f;
    float imageWidth=[arr[0][@"width_tf"] floatValue];
    float imageHeight=[arr[0][@"height_tf"] floatValue];
    if (imageHeight<imageWidth) {
        imageWidth=imageHeight;
    }
    
    
    float barWidth=0.0f;
    NSMutableArray *tem=[db getDataForField:FRAME_TABLE where:[NSString stringWithFormat:@"id='%@'",arr[0][@"frame"]]];
    
    if (tem.count!=0) {
        frameWidth=[tem[0][@"width"] floatValue]*2;
    }
    if (![arr[0][@"style"] isEqualToString:@"none"]) {
        
        matWidth=[arr[0][@"left"] floatValue]+[arr[0][@"right"] floatValue];
        
        if ([arr[0][@"style"] isEqualToString:@"Double"]) {
            doubleMatWidth=[arr[0][@"bottomUniformWidth"] floatValue]*2;
        }
        
    }
    
    barWidth=(workSpaceWidth/(matWidth+imageWidth+frameWidth+doubleMatWidth))/2;
    return barWidth;
    
    
}


-(float)getBarWidthWhereParentView:(UIView *)parentView{
    
    
    
    NSMutableArray *arr=[db getDataForField:USER_PRODUCT where:nil];
    
    float frameWidth=0.0f;
    
    
    float barWidth=0.0f;
    NSMutableArray *tem=[db getDataForField:FRAME_TABLE where:[NSString stringWithFormat:@"id='%@'",arr[0][@"frame"]]];
    
    if (tem.count!=0) {
        frameWidth=[tem[0][@"width"] floatValue];
    }
    
    
    barWidth=frameWidth*[self getScallDownFactorWhereParentView:parentView];
    return barWidth;
    
    
    
    
}


-(void)setSettingsViewWhereIndex:(int)index{
    
    
    for (UIView* b in [_settingView subviews])
    {
        if (!(b.tag==1||b.tag==2)) {
            [b removeFromSuperview];
        }
        
    }
    
    
    switch (index) {
        case 0:
            [self showFrameSetting];
            break;
        case 1:
            [self showBottomViewWhereType:@"mat"];
            break;
        case 2:
            // [self showBottomViewWhereType:@"glass"];
            break;
        case 3:
            // [self showBottomViewWhereType:@"print"];
            break;
        case 4:
            // [self showExtraSetting];
            break;
            
        default:
            break;
    }
}

-(void)showFrameSetting{
    
    
    if (viewControllers[@"frame"]!=nil) {
        
        
        [self.settingView addSubview:[(ChooseFrameViewController *)viewControllers[@"frame"] view]];
    }
    else{
        ChooseFrameViewController * frameSubView=[[ChooseFrameViewController alloc] init];
        //frameSubView.frameArray=self->frameArray;
        frameSubView.view.frame=CGRectMake(0, 0, _settingView.frame.size.width,  _settingView.frame.size.height);
        frameSubView.delegate=self;
        frameSubView.type = _cameFrom;
        [self.settingView addSubview:frameSubView.view];
        [frameSubView refreshView];
        
        viewControllers[@"frame"]=frameSubView;
        
        
    }
    
    
    
}


-(void)showBottomViewWhereType:(NSString *)type{
    if (viewControllers[type]!=nil) {
        
        [self.settingView addSubview:[(MatsSubView *)viewControllers[type] view]];
        
    }
    else{
        
        MatsSubView * frameSubView=[[MatsSubView alloc] init];
        frameSubView.delegate=self;
        frameSubView.type=type;
        
        frameSubView.view.frame=CGRectMake(0, 0, _settingView.frame.size.width,  _settingView.frame.size.height);
        frameSubView.delegate=self;
        
        [self.settingView addSubview:frameSubView.view];
        viewControllers[type]=frameSubView;
        
    }
    
    
}

- (IBAction)cartBtn:(id)sender{
    [self closeAllOpenOption];
    [Global callMenuViewWithCode:@"0"];
}
- (IBAction)menuBtn:(id)sender{
    [self closeAllOpenOption];
    isMenu=YES;
    [Global showSideMenu];
}
- (IBAction)nextAction:(id)sender {
    
    [self closeAllOpenOption];
    [self getFramePreviewImage];
    [self performSegueWithIdentifier:@"final" sender:nil];
}





-(void)getFramePreviewImage{
    
    
    
    UIImage *preview;
    UIView *baseView=[[UIView alloc] init];
  
    
    float baseHeight=0;
    float baseWidth=0;
    float baseRatio=_frameView.frame.size.width/_frameView.frame.size.height;
    baseWidth=_frameView.frame.size.width*2;
    baseHeight=baseWidth/baseRatio;
    
    baseView.frame=CGRectMake(0, 0, baseWidth, baseHeight);
    baseView.backgroundColor=[UIColor clearColor];
    float barWidth=[self getBarWidthWhereParentView:baseView];
   // baseView.frame=CGRectMake(0, 0, imageWidth+barWidth+barWidth, imageHeight+barWidth+barWidth);
    
      
    float  imageHeight=[productDic[@"height_tf"] floatValue];
    float imageWidth=[productDic[@"width_tf"] floatValue];
          
          float ratio=_mainImage.image.size.width/_mainImage.image.size.height;
          
          if (imageWidth>imageHeight) {
              imageWidth=baseWidth-barWidth-barWidth;
              imageHeight=imageWidth/ratio;
          }
          else{
              imageHeight=baseHeight-barWidth-barWidth;
              imageWidth=imageHeight*ratio;
          }
          
          
     
    
    
    
    UIImageView *imageView=[[UIImageView alloc] init];
    imageView.frame=CGRectMake((baseWidth-imageWidth)/2, (baseHeight-imageHeight)/2, imageWidth, imageHeight);
    [baseView addSubview:imageView];
    imageView.image=_mainImage.image;
    
    if ([[productDic[@"frameStyle"] uppercaseString] isEqualToString:[@"None" uppercaseString]]) {
        
        baseView.frame=CGRectMake(0, 0, imageWidth+barWidth+barWidth, imageHeight+barWidth+barWidth);
        imageView.frame=CGRectMake(barWidth, barWidth, imageWidth, imageHeight);
        
        frameScript.barWidth=barWidth;
        frameScript.parentView=baseView;
        frameScript.childrenView=imageView;
        [frameScript addFrameToChild];
        
        preview =[self pb_takeSnapshotofView:baseView];
        
        [Global saveImagesInDocumentDirectory:UIImagePNGRepresentation(preview) ImageId:@"preview"];
      //  if ([productDic[@"sku"] isEqualToString:FRAME_SK U]){
        [Global saveImagesInDocumentDirectory:UIImagePNGRepresentation(preview) ImageId:@"frame_preview"];
      //  }
        [imageView removeFromSuperview];
        
    }
    else{
        UIView * singleView=[[UIView alloc] init];
        UIView * doubleView=[[UIView alloc] init];
        UIImageView *singleMatImg=[[UIImageView alloc] init];
        UIImageView *doubleMatImg=[[UIImageView alloc] init];
        
        
        if ([[productDic[@"frameStyle"] uppercaseString] isEqualToString:[@"Double" uppercaseString]]){
            [self creatDoubleFrameWhereParentView:baseView andSingleView:singleView andDoubleView:doubleView andImage:imageView andSingleMatImage:singleMatImg andDoubleMatImage:doubleMatImg];
            
            if (doubleMatImage.image!=nil) {
                doubleMatImg.image=doubleMatImage.image;
            }
            else{
                doubleView.backgroundColor=doubleFrame.backgroundColor;
            }
            
            
            
        }
        else{
            [self setSingleFrameWhereParentView:baseView andSingleView:singleView andDoubleView:doubleView andImage:imageView andSingleMatImage:singleMatImg andSidemargineMain:margineMain];
            
        }
        
        
        
        
        if (singleMatImage.image!=nil) {
            singleMatImg.image=singleMatImage.image;
            //              [singleView insertSubview:singleMatImg belowSubview:doubleView];
        }
        else{
            singleView.backgroundColor=singleFrame.backgroundColor;
        }
        
        
        
        NSArray *arr = [baseView subviews];
        
        for (UIView* b in arr)
        {
            if ((b.tag==2)) {
                
                [b removeFromSuperview];
                
            }
            
        }
        
        
        //        baseView.backgroundColor=[UIColor redColor];
        
        
        
        
        
        baseView.frame=CGRectMake(0, 0, singleView.frame.size.width+barWidth+barWidth, singleView.frame.size.height+barWidth+barWidth);
        singleView.center=baseView.center;
        
        
        
        
        
        frameScript.barWidth=barWidth;
        frameScript.parentView=baseView;
        frameScript.childrenView=singleView;
        [frameScript addFrameToChild];
        
        
        
        preview=[self pb_takeSnapshotofView:baseView];
        
        
        
        [Global saveImagesInDocumentDirectory:UIImagePNGRepresentation(preview) ImageId:@"preview"];
        [Global saveImagesInDocumentDirectory:UIImagePNGRepresentation(preview) ImageId:@"frame_preview"];
        
        [singleView removeFromSuperview];
        
        
        
        
    }
    
    
    
    
    preview=[self pb_takeSnapshotofView:baseView];
    [Global saveImagesInDocumentDirectory:UIImagePNGRepresentation(preview) ImageId:@"frame_preview"];
    
    [baseView removeFromSuperview];
    baseView=nil;
    
}



- (UIImage *)pb_takeSnapshotofView:(UIView *)view {
    UIImage *image;
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, NO, [UIScreen mainScreen].scale);
    [view drawViewHierarchyInRect:view.bounds afterScreenUpdates:YES];
    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
    
}
- (UIImage *)saveFrameView:(UIView *)view {
    self.mainImage.hidden = YES;
    UIImage *image;
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, NO, [UIScreen mainScreen].scale);
    [view drawViewHierarchyInRect:view.bounds afterScreenUpdates:YES];
    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.mainImage.hidden = NO;
    return image;
}




#pragma mark - API CALLING

-(void)getDataForFrame{
    
    NSArray *arrs=[db getDataForField:FRAME_TABLE where:nil];
    if (arrs.count!=0) {
        // [self refreshFrames];
        
    }
    
    
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    NSString *urlString=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,GET_FRAMES_CATEGORY];
    NSMutableArray *arr=[[NSMutableArray alloc] init];
    arr=[db getDataForField:USERTABLE where:nil];
    if (arr.count!=0) {
        [api sendGetRequst:urlString andToken:arr[0][@"token"]];
    }
    else{
        [self logInUser];
    }
}

-(void)signUpUser{
    
    
    
    
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
    dic[@"name"]=[[UIDevice currentDevice] name];
    dic[@"email"]=[NSString stringWithFormat:@"%@@frameshop.com.au",[Global getDeviceId]];
    dic[@"password"]=@"qwxcjeub32ne";
    dic[@"c_password"]=@"qwxcjeub32ne";
    dic[@"url"]=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,REGISTER_USER];
    [api sendPostRequst:dic];
    
}
-(void)logInUser{
    
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
    
    dic[@"email"]=[NSString stringWithFormat:@"%@@frameshop.com.au",[Global getDeviceId]];
    dic[@"password"]=@"qwxcjeub32ne";
    
    dic[@"url"]=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,LOGIN_USER];
    [api sendPostRequst:dic];
}

-(void)refreshFrames{
    
    
    
    if (viewControllers[@"frame"]!=nil) {
        //UIView *view=[(ChooseFrameViewController *)viewControllers[@"frame"] view];
        [ (ChooseFrameViewController *)viewControllers[@"frame"] refreshView];
        
    }
    
}

- (UIImage*)imageByCropping:(UIImage *)imageToCrop toRect:(CGRect)rect
{
    CGImageRef imageRef = CGImageCreateWithImageInRect([imageToCrop CGImage], rect);
    UIImage *cropped = [UIImage imageWithCGImage:imageRef];
    return cropped;
    
}


#pragma mark - Mat settings methods

-(void)setSingleFrameWhereParentView:(UIView *)parentView andSingleView:(UIView *)singleView andDoubleView:(UIView *)doubleView andImage:(UIImageView *)imageView andSingleMatImage:(UIImageView *)singleMat andSidemargineMain:(float)margineMain{
    //productDic[@"uniformWidth"]=[NSString stringWithFormat:@"%f",matWidth];
    productDic[@"frameStyle"]=@"Single";
    
    [parentView addSubview:imageView];
    [doubleView removeFromSuperview];
    [vgrowView removeFromSuperview];
    NSMutableArray *arr=[db getDataForField:USER_PRODUCT where:nil];
    if (arr.count!=0) {
        arr[0][@"style"]=productDic[@"frameStyle"];
        [db update:USER_PRODUCT :arr[0] :[NSString stringWithFormat:@"id='%@'",arr[0][@"id"]]];
        
    }
    
    float matBoardLeftWidth=[arr[0][@"left"] floatValue]*[self getScallDownFactorWhereParentView:parentView];
    float matBoardRighttWidth=[arr[0][@"right"] floatValue]*[self getScallDownFactorWhereParentView:parentView];
    float matBoardTopWidth=[arr[0][@"top"] floatValue]*[self getScallDownFactorWhereParentView:parentView];
    float matBoardBottomWidth=[arr[0][@"bottom"] floatValue]*[self getScallDownFactorWhereParentView:parentView];
   
    
    float imageHeight=[productDic[@"width_tf"] floatValue];
    float imageWidth=[productDic[@"height_tf"] floatValue];
    
    float ratio=imageView.frame.size.height/imageView.frame.size.width;
    
    if (imageHeight<imageWidth) {
        
        imageWidth=parentView.frame.size.width-(margineMain)-matBoardLeftWidth-matBoardRighttWidth- ([self getBarWidthWhereParentView:parentView]*2);
        
        imageHeight=imageWidth*ratio;
    }
    else{
        imageHeight=parentView.frame.size.height-(margineMain)-matBoardTopWidth-matBoardBottomWidth-([self getBarWidthWhereParentView:parentView]*2);
        
        imageWidth=imageHeight/ratio;
        
    }
    
     
    
    
    imageView.frame=CGRectMake(0, 0, imageWidth, imageHeight);
    imageView.center=parentView.center;
    
    if (singleView==nil) {
        singleView=[[UIView alloc] init];
    }
    
    singleView.frame=CGRectMake(0, 0, imageWidth+matBoardLeftWidth+matBoardRighttWidth, imageHeight+matBoardTopWidth+matBoardBottomWidth);
    singleView.tag=1;
    
    singleMat.frame=singleView.frame;
    CGRect temp=singleMat.frame;
    temp.origin.x=0;
    temp.origin.y=0;
    singleMat.frame=temp;
    
    
    if ([productDic[@"singleCoreColor"] isEqualToString:@"none"]) {
        singleView.backgroundColor=[UIColor blackColor];
    }
    else{
        
        if (singleView.backgroundColor==nil && singleMat.image==nil) {
            singleView.backgroundColor=[Global colorFromHexString:productDic[@"singleCoreColor"]];
        }
    }
    
    [parentView addSubview:singleView];
    singleView.center=parentView.center;
    imageView.frame=CGRectMake(matBoardLeftWidth, matBoardTopWidth, imageWidth, imageHeight);
    
    [Global roundCorner:imageView roundValue:0 borderWidth:1.0 borderColor:[UIColor whiteColor]];
    
    [singleView addSubview:imageView];
    [singleView insertSubview:singleMat belowSubview:imageView];
    
    if ([self compairTwoOptionsWhereFirst:arr[0][@"topMat"] andSecond:@"V Groove"]) {
        [self addTopMat];
    }
    
    frameScript.parentView=parentView;
    frameScript.barWidth=[self getBarWidthWhereParentView:parentView];
    frameScript.childrenView=singleView;
    [frameScript addFrameToChild];
    
}

-(void)setDoubleFrame{
    
    [self creatDoubleFrameWhereParentView:_frameView andSingleView:singleFrame andDoubleView:doubleFrame andImage:_mainImage andSingleMatImage:singleMatImage andDoubleMatImage:doubleMatImage];
    
}

-(void)creatDoubleFrameWhereParentView:(UIView *)parentView andSingleView:(UIView *)singleView andDoubleView:(UIView *)doubleView andImage:(UIImageView *)imageView andSingleMatImage:(UIImageView *)singleMat andDoubleMatImage:(UIImageView *)doubleMat{
    
    productDic[@"frameStyle"]=@"Double";
    [parentView addSubview:imageView];
    [singleView removeFromSuperview];
    [vgrowView removeFromSuperview];
    NSMutableArray *arr=[db getDataForField:USER_PRODUCT where:nil];
    if (arr.count!=0) {
        arr[0][@"style"]=productDic[@"frameStyle"];
        [db update:USER_PRODUCT :arr[0] :[NSString stringWithFormat:@"id='%@'",arr[0][@"id"]]];
        
    }
    float doubleLeft=[arr[0][@"bottomUniformWidth"] floatValue]*[self getScallDownFactorWhereParentView:parentView];
    float doubleRight=[arr[0][@"bottomUniformWidth"] floatValue]*[self getScallDownFactorWhereParentView:parentView];
    float doubleTop=[arr[0][@"bottomUniformWidth"] floatValue]*[self getScallDownFactorWhereParentView:parentView];
    float doubleBottom=[arr[0][@"bottomUniformWidth"] floatValue]*[self getScallDownFactorWhereParentView:parentView];
    
    float matBoardLeftWidth=[arr[0][@"left"] floatValue]*[self getScallDownFactorWhereParentView:parentView];
    float matBoardRighttWidth=[arr[0][@"right"] floatValue]*[self getScallDownFactorWhereParentView:parentView];
    float matBoardTopWidth=[arr[0][@"top"] floatValue]*[self getScallDownFactorWhereParentView:parentView];
    float matBoardBottomWidth=[arr[0][@"bottom"] floatValue]*[self getScallDownFactorWhereParentView:parentView];
  
    float  imageHeight=[productDic[@"height_tf"] floatValue];
    float imageWidth=[productDic[@"width_tf"] floatValue];
    
   
    float ratio=imageView.frame.size.height/imageView.frame.size.width;
    
    if (imageHeight<imageWidth) {
        
        imageWidth=parentView.frame.size.width-(margineMain)-matBoardLeftWidth-matBoardRighttWidth-(doubleTop)-(doubleBottom)-([self getBarWidthWhereParentView:parentView]*2)-20;
        imageHeight=imageWidth*ratio;
    }
    else{
        imageHeight=parentView.frame.size.height-(margineMain)-matBoardTopWidth-matBoardBottomWidth-(doubleTop)-(doubleBottom)-([self getBarWidthWhereParentView:parentView]*2)-20;
        
        imageWidth=imageHeight/ratio;
    }
    
    imageView.frame=CGRectMake(doubleLeft, doubleTop, imageWidth, imageHeight);
    
    if (singleView==nil) {
        singleView=[[UIView alloc] init];
    }
    if (doubleView==nil) {
        doubleView=[[UIView alloc] init];
    }
    
    singleView.frame=CGRectMake(0, 0, imageWidth+doubleLeft+doubleRight+matBoardLeftWidth+matBoardRighttWidth, imageHeight+doubleTop+doubleBottom+matBoardTopWidth+matBoardBottomWidth);
    
    [parentView addSubview:singleView];
    
    singleView.center=parentView.center;
    
    doubleView.frame=CGRectMake(matBoardLeftWidth, matBoardTopWidth, doubleLeft+doubleRight+imageWidth, doubleTop+doubleBottom+imageHeight);
    
    [singleView addSubview:doubleView];
    [doubleView addSubview:imageView];
    singleMat.frame=singleView.frame;
    
    CGRect temp=singleMat.frame;
    temp.origin.x=0;
    temp.origin.y=0;
    singleMat.frame=temp;
    
    doubleMat.frame=doubleView.frame;
    temp=doubleMat.frame;
    temp.origin.x=0;
    temp.origin.y=0;
    doubleMat.frame=temp;
    
    
    [doubleView insertSubview:doubleMat belowSubview:imageView];
    [singleView insertSubview:singleMat belowSubview:doubleView];
    
    [Global roundCorner:doubleView roundValue:0 borderWidth:1 borderColor:[UIColor whiteColor]];
    [Global roundCorner:imageView roundValue:0 borderWidth:1 borderColor:[UIColor whiteColor]];
    
    if ([self compairTwoOptionsWhereFirst:arr[0][@"topMat"] andSecond:@"V Groove"]) {
        [self addTopMat];
    }
    
    frameScript.barWidth=[self getBarWidthWhereParentView:parentView];
    frameScript.childrenView=singleView;
    frameScript.parentView=parentView;
    [frameScript addFrameToChild];
    
}


-(void)removeSingleDoubleFrame{
    _mainImage.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1);
    productDic[@"frameStyle"]=@"none";
    productDic[@"topMat"]=@"none";
    NSMutableArray *arr=[db getDataForField:USER_PRODUCT where:nil];
    if (arr.count!=0) {
        arr[0][@"style"]=@"none";
        arr[0][@"topMat"]=@"";
        [db update:USER_PRODUCT :arr[0] :[NSString stringWithFormat:@"id='%@'",arr[0][@"id"]]];
    }
    
    [_frameView addSubview:_mainImage];
    [singleFrame removeFromSuperview];
    [doubleFrame removeFromSuperview];
    [vgrowView removeFromSuperview];
    float imageHeight=[productDic[@"height_tf"] floatValue];
    float imageWidth=[productDic[@"width_tf"] floatValue];
//    float imageHeight=productDic[@"width"];
//    float imageWidth=_mainImage.frame.size.width;
    float ratio=_mainImage.frame.size.height/_mainImage.frame.size.width;
    
    if (imageHeight<imageWidth) {
        
        imageWidth=_frameView.frame.size.width-(margineMain)-([self getBarWidthWhereParentView:_frameView]*2);
        imageHeight=imageWidth*ratio;
    }
    else{
        imageHeight=_frameView.frame.size.height-(margineMain)-([self getBarWidthWhereParentView:_frameView]*2);
        imageWidth=imageHeight/ratio;
    }
    
    
    
    _mainImage.frame=CGRectMake(0, 0, imageWidth, imageHeight);
    _mainImage.center=_frameView.center;
    _mainImage.center=_frameView.center;
    frameScript.barWidth=[self getBarWidthWhereParentView:_frameView];
    frameScript.childrenView=_mainImage;
    frameScript.parentView=_frameView;
    [frameScript addFrameToChild];
    
    [Global roundCorner:_mainImage roundValue:0 borderWidth:0 borderColor:nil];
    
}

-(void)addCoreColor:(NSMutableDictionary *)info{
    
    [_loaderActivity startAnimating];
    _loaderActivity.hidden=NO;
    
    if ([info[@"type"] isEqualToString:@"wc"]){
        
        if ([info[@"mat"] isEqualToString:@"top"]) {
            
            NSMutableArray *arr=[db getDataForField:USER_PRODUCT where:nil];
            if (arr.count!=0) {
                arr[0][@"topMatBoard"]=[NSString stringWithFormat:@"%@",info[@"selectedOption"][@"id"]];
                [db update:USER_PRODUCT :arr[0] :[NSString stringWithFormat:@"id='%@'",arr[0][@"id"]]];
                
            }
            [[NSUserDefaults standardUserDefaults] setValue: info[@"selectedOption"][@"id"] forKey:SINGLE_MAT_ID];
            productDic[@"singleCoreColor"]=info[@"selectedOption"][@"color_hex"];
            [self setMatColorOfMat:singleFrame andInfo:info];
        }
        else{
            
            NSMutableArray *arr=[db getDataForField:USER_PRODUCT where:nil];
            if (arr.count!=0) {
                arr[0][@"bottomMat"]=[NSString stringWithFormat:@"%@",info[@"selectedOption"][@"id"]];
                [db update:USER_PRODUCT :arr[0] :[NSString stringWithFormat:@"id='%@'",arr[0][@"id"]]];
                
            }
            
            productDic[@"doubleCoreColor"]=info[@"selectedOption"][@"color_hex"];
            [[NSUserDefaults standardUserDefaults] setValue: info[@"selectedOption"][@"id"] forKey:DOUBLE_MAT_ID];
            [self setMatColorOfMat:doubleFrame andInfo:info];
        }
        
    }
    [self getItemPrice];
    
}
-(void)setMatColorOfMat:(UIView *)view andInfo:(NSMutableDictionary *)info{
    
    UIImageView *workingView;
    
    if (!( ( [info[@"selectedOption"][@"tile_image"] isEqual:(id)[NSNull null]] ) ||[info[@"selectedOption"][@"tile_image"] isEqualToString:@"<null>"]) ) {
        
        if (view==singleFrame) {
            
            if (singleMatImage==nil) {
                singleMatImage=[[UIImageView alloc] init];
            }
            singleMatImage.frame=CGRectMake(0, 0, singleFrame.frame.size.width, singleFrame.frame.size.height);
            
            if (![productDic[@"topMat"] isEqualToString:@"none"] ) {
                [singleFrame insertSubview:singleMatImage belowSubview:vgrowView];
            }
            else if ([productDic[@"frameStyle"] isEqualToString:@"Double"]){
                [singleFrame insertSubview:singleMatImage belowSubview:doubleFrame];
            }
            else{
                [singleFrame insertSubview:singleMatImage belowSubview:_mainImage];
            }
            //
            workingView=singleMatImage;
        }
        else{
            
            
            if (doubleMatImage==nil) {
                doubleMatImage=[[UIImageView alloc] init];
            }
            doubleMatImage.frame=CGRectMake(0, 0, doubleFrame.frame.size.width, doubleFrame.frame.size.height);
            
            [doubleFrame insertSubview:doubleMatImage belowSubview:_mainImage];
            
            
            
            workingView=doubleMatImage;
            
            
        }
        
        
        UIImage *img=[Global getImageFromDoucumentWithName:[NSString stringWithFormat:@"meta_tile_%@",info[@"selectedOption"][@"id"]]];
        
        workingView.image=[Global getMidiumSizeImage:img imageSize:(workingView.frame.size.width>workingView.frame.size.height)?workingView.frame.size.width+50:workingView.frame.size.height+50];
        [_loaderActivity stopAnimating];
        _loaderActivity.hidden=YES;
        if (img==nil) {
            
            [_loaderActivity startAnimating];
            _loaderActivity.hidden=NO;
            NSString *str=info[@"selectedOption"][@"id"];
            
            dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
            dispatch_async(q, ^{
                //[Global startShimmeringToView:cell.contentView];
                NSData *datas;
                
                datas =[NSData dataWithContentsOfURL:[NSURL URLWithString:info[@"selectedOption"][@"tile_image"]]] ;
                
                [Global saveImagesInDocumentDirectory:datas ImageId:[NSString stringWithFormat:@"meta_tile_%@",str]];
                
                
                UIImage *img = [[UIImage alloc] initWithData:datas];
                dispatch_async(dispatch_get_main_queue(), ^{
                    if(img!=nil){
                        
                        
                        
                        if (view==self->singleFrame) {
                            if ([[[NSUserDefaults standardUserDefaults] valueForKey:SINGLE_MAT_ID] intValue] ==[str intValue]) {
                                workingView.image=[Global getMidiumSizeImage:img imageSize:(workingView.frame.size.width>workingView.frame.size.height)?workingView.frame.size.width+50:workingView.frame.size.height+50];
;
                            }
                            
                            
                        }
                        
                        
                        else  if (view==self->doubleFrame) {
                            if ([[[NSUserDefaults standardUserDefaults] valueForKey:DOUBLE_MAT_ID] intValue]==[str intValue]) {
                                workingView.image=[Global getMidiumSizeImage:img imageSize:(workingView.frame.size.width>workingView.frame.size.height)?workingView.frame.size.width+50:workingView.frame.size.height+50];
;
                            }
                        }
                        
                    }    // cells.iconImage.image=img;
                    [self->_loaderActivity stopAnimating];
                    self->_loaderActivity.hidden=YES;
                    
                });
            });
            
        }
        
    }
    else{
        if (view==singleFrame){
            [singleMatImage removeFromSuperview];
            singleMatImage=nil;
        }
        if (view==doubleFrame){
            [doubleMatImage removeFromSuperview];
            doubleMatImage=nil;
        }
        view.backgroundColor=[Global colorFromHexString:info[@"selectedOption"][@"color_hex"]];
        [_loaderActivity stopAnimating];
        _loaderActivity.hidden=YES;
    }
}

-(void)addTopMat{
    productDic[@"topMat"]=@"vg";
    NSMutableArray *arr=[db getDataForField:USER_PRODUCT where:nil];
    if (arr.count!=0) {
        arr[0][@"topMat"]=@"V Groove";
        [db update:USER_PRODUCT :arr[0] :[NSString stringWithFormat:@"id='%@'",arr[0][@"id"]]];
        
    }
    
    if (vgrowView==nil) {
        vgrowView=[[UIView alloc] init];
    }
    UIView * temp;
    
    if ([arr[0][@"style"] isEqualToString:@"Single"]) {
        temp=_mainImage;
    }
    else{
        temp=doubleFrame;
    }
    
    vgrowView.frame=CGRectMake(0, 0, temp.frame.size.width+15, temp.frame.size.height+15);
    vgrowView.backgroundColor=[UIColor clearColor];
    [singleFrame insertSubview:vgrowView belowSubview:_mainImage];
    vgrowView.center=temp.center;
    [Global roundCorner:vgrowView roundValue:0 borderWidth:3 borderColor:[UIColor whiteColor]];
    
}

-(void)removeTopMat{
    
    [vgrowView removeFromSuperview];
    productDic[@"topMat"]=@"none";
    [vgrowView removeFromSuperview];
    
    
    
    NSMutableArray *arr=[db getDataForField:USER_PRODUCT where:nil];
    if (arr.count!=0) {
        arr[0][@"topMat"]=@"";
        [db update:USER_PRODUCT :arr[0] :[NSString stringWithFormat:@"id='%@'",arr[0][@"id"]]];
        
    }
}

-(void)makeViewFrameInCenter:(UIView *)view{
    CGRect tempF=view.frame;
    tempF.origin.x=(view.superview.frame.size.width-tempF.size.width)/2;
    
    view.frame=tempF;
    
}


#pragma mark - Search Frame delegate
-(void)didChooseSearchFrameOption:(NSMutableDictionary *)infoDic{
    
    ChooseFrameViewController *viewControler=(ChooseFrameViewController *)viewControllers[@"frame"];
    [viewControler wrapCell:infoDic];
    
    [self didChooseFrameOption:infoDic];
    
}



#pragma mark - OptionPopUpVCDelegate
- (void)didPressCategoryWithOptionDic:(NSMutableDictionary *)dic{
    ChooseFrameViewController *viewControler=(ChooseFrameViewController *)viewControllers[@"frame"];
    [viewControler didSelectCategory:dic];
}
- (void)didPressSortingWithOptionDic:(NSIndexPath *)indexpath{
    ChooseFrameViewController *viewControler=(ChooseFrameViewController *)viewControllers[@"frame"];
    [viewControler didSelectSortingWithIndex:indexpath];
}
- (void)cancelPoopUp{
    ChooseFrameViewController *viewControler=(ChooseFrameViewController *)viewControllers[@"frame"];
    [viewControler dissmissPopUp];
}
#pragma mark - Choose Frame delegate

- (void)didPressCategoryWithOptionArray:(NSMutableArray *)infoArr{
    [self performSegueWithIdentifier:@"categorypopup" sender:infoArr];
}
- (void)didPressSortingWithOptionArray:(NSMutableArray *)infoArr{
    [self performSegueWithIdentifier:@"categorypopup" sender:infoArr];
}
-(void)didChooseFrameOption:(NSMutableDictionary *)infoDic{
        self->_dotImage.hidden = YES;
        self->_priceView.frame = CGRectMake(self.view.frame.size.width -97, 10, 97, 43);
        self->_priceCount.frame=CGRectMake(10, 0, self->_priceView.frame.size.width-10, _priceView.frame.size.height);
        self->_priceCount.textAlignment = NSTextAlignmentCenter;
 
    
    
    
    [UIView animateWithDuration:1
                     animations:^{
        [self.view layoutIfNeeded];
    }];
    
    _nxtBtrn.backgroundColor=btnColor;
    _nxtBtrn.enabled=YES;
    
    NSString *frameId=[NSString stringWithFormat:@"%@",infoDic[@"id"]] ;
    
    [_loaderActivity startAnimating];
    _loaderActivity.hidden=NO;
    
    [[NSUserDefaults standardUserDefaults] setValue:frameId forKey:@"frame"];
    
    NSMutableArray *arr=[db getDataForField:USER_PRODUCT where:nil];
    if (arr.count!=0) {
        arr[0][@"frame"]=[NSString stringWithFormat:@"%@",infoDic[@"id"]];
        arr[0][@"style"]=productDic[@"frameStyle"];
        [db delTableWithTableName:USER_PRODUCT];
        [db insertIntoTable:arr[0] table_name:USER_PRODUCT];
    }
    UIImage *bar=[Global getImageFromDoucumentWithName:[NSString stringWithFormat:@"frameBar_%@",infoDic[@"id"]]];
    if (bar!=nil) {
        // [self changeFrameAppearance:bar ];
        frameScript.frameImage=bar;
        [frameScript changeFrameAppearance];
        
        
        if ([productDic[@"frameStyle"] isEqualToString:@"none"]) {
            [self removeSingleDoubleFrame];
        }
        else if ([productDic[@"frameStyle"] isEqualToString:@"Single"]) {
            [self setSingleFrameWhereParentView:_frameView andSingleView:singleFrame andDoubleView:doubleFrame andImage:_mainImage andSingleMatImage:singleMatImage andSidemargineMain:margineMain];
        }
        else{
            [self setDoubleFrame];
        }
        [self getItemPrice];
        return;
    }
    dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(q, ^{
        
        NSData *datas;
        NSString *currentId=[frameId mutableCopy];
        datas =[NSData dataWithContentsOfURL:[NSURL URLWithString:infoDic[@"image_url_t"]]] ;
        
        [Global saveImagesInDocumentDirectory:datas ImageId:[NSString stringWithFormat:@"frameBar_%@",frameId]];
        
        
        UIImage *img = [[UIImage alloc] initWithData:datas];
        dispatch_async(dispatch_get_main_queue(), ^{
            if(img!=nil )
            {
                NSLog(@"Current==> %@  Frame =====> %@",currentId,[[NSUserDefaults standardUserDefaults] valueForKey:@"frame"]);
                
                if (![currentId isEqualToString:[[NSUserDefaults standardUserDefaults] valueForKey:@"frame"]]) {
                    NSLog(@"Nothing to change");
                    return ;
                }
                self->frameScript.frameImage=img;
                [self->frameScript changeFrameAppearance];
                //  [self changeFrameAppearance:img];
                
                if ([self->productDic[@"frameStyle"] isEqualToString:@"none"]) {
                    [self removeSingleDoubleFrame];
                }
                else if ([self->productDic[@"frameStyle"] isEqualToString:@"Single"]) {
                    [self setSingleFrameWhereParentView:self->_frameView andSingleView:self->singleFrame andDoubleView:self->doubleFrame andImage:self->_mainImage andSingleMatImage:self->singleMatImage andSidemargineMain:self->margineMain];
                }
                else{
                    [self setDoubleFrame];
                }
                [self getItemPrice];
                
                
                
            }
        });
    });
    
}

-(void)didChooseOptionType:(NSMutableDictionary *)infoDic{
    
    
    [self performSegueWithIdentifier:@"option" sender:infoDic];
}

-(void)didChooseSearchOption:(UISearchBar *)searchBar{
    
    
    
    [self performSegueWithIdentifier:@"search" sender:nil];
    
}
- (void)matsAction:(int)index {
    
    [self setSettingsViewWhereIndex:1];
}
-(void)didPressSkip{
    NSLog(@"Skip Frame");
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Segue support

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    CGRect frame=CGRectMake(0, self.view.bounds.size.height*0.70, self.view.bounds.size.width, self.view.bounds.size.height*0.40);
    
    if ([[segue identifier] isEqualToString:@"option"]) {
        
        OptionViewController *albumContentsViewController = [segue destinationViewController];
        albumContentsViewController.delegate=self;
        albumContentsViewController.infoDic=(NSMutableDictionary *)sender;
        albumContentsViewController.previousFrame=frame;
        
    }
    
    else if ([[segue identifier] isEqualToString:@"search"]) {
        
        SearchViewController *albumContentsViewController = [segue destinationViewController];
        
        albumContentsViewController.delegate=self;
        
        
    }else if([[segue identifier] isEqualToString:@"categorypopup"]){
        OptionPopUpVC *optionPopUpVC = [segue destinationViewController];
        optionPopUpVC.delegate = self;
        optionPopUpVC.optionArray = sender;
    }else if([[segue identifier] isEqualToString:@"framezoom"]){
        ProductImagePreviewVC *productImagePreviewVC = [segue destinationViewController];
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
        dic[@"image"] = sender;
        productImagePreviewVC.previewImageDic = dic;
    }
    
}

#pragma mark - Creat frame delegate

-(void)didCreatedFrameSuccessfully{
    [_loaderActivity stopAnimating];
    _loaderActivity.hidden=YES;
    
}

#pragma mark - Mat Frame delegate
-(void)didChooseMatOption:(NSMutableDictionary *)infoDic{
    
    [self performSegueWithIdentifier:@"option" sender:infoDic];
    
}

-(void)changeFrameSettingWhereType:(NSMutableDictionary *)infoDic{
    
}

-(BOOL)compairTwoOptionsWhereFirst:(NSString *)option1 andSecond:(NSString *)option2{
    if ([[option1 uppercaseString] isEqualToString:[option2 uppercaseString]]) {
        return YES;
    }
    else{
        return NO;
    }
}
-(void)didSelectSingleFrame{
    
    productDic[@"frameStyle"]=@"Single";
    [self setSingleFrameWhereParentView:_frameView andSingleView:singleFrame andDoubleView:doubleFrame andImage:_mainImage andSingleMatImage:singleMatImage andSidemargineMain:margineMain];
    
    
}
-(void)didSelectDoubleFrame{
    [self setDoubleFrame];
}
-(void)didSelectNoneFrame{
    [self removeSingleDoubleFrame];
    [self getItemPrice];
}
- (void)framAction:(int)index{
    
    [self setSettingsViewWhereIndex:0];
}

-(void)didSelectCoreColor:(NSMutableDictionary *)infoDic{
    
    if ([infoDic[@"selectedOption"][@"id"] length]==0) {
        
        [self removeSingleDoubleFrame];
        
    }
    else{
        [self addCoreColor:infoDic];
    }
    
}


#pragma mark - Option View delegate


-(void)didChangeVGMatSetting:(BOOL)isChecked{
    if (isChecked) {
        [self addTopMat];
    }
    else{
        [self removeTopMat];
    }
}

-(void)didEnterUniformWidthForTopMatSetting:(float)width{
    
    
    NSMutableArray *arr=[db getDataForField:USER_PRODUCT where:nil];
    
    if (arr.count!=0) {
        arr[0][@"top"]=[NSString stringWithFormat:@"%.02f",width];
        arr[0][@"right"]=[NSString stringWithFormat:@"%.02f",width];
        arr[0][@"left"]=[NSString stringWithFormat:@"%.02f",width];
        arr[0][@"bottom"]=[NSString stringWithFormat:@"%.02f",width];
        arr[0][@"uniformWidth"]=[NSString stringWithFormat:@"%.02f",width];
        arr[0][@"isUni"]=@"1";
        [db update:USER_PRODUCT :arr[0] :[NSString stringWithFormat:@"id='%@'",arr[0][@"id"]]];
        
    }
    productDic[@"customBottom"]=[NSString stringWithFormat:@"%.02f",width];
    productDic[@"customLeft"]=[NSString stringWithFormat:@"%.02f",width];
    productDic[@"customRight"]=[NSString stringWithFormat:@"%.02f",width];
    productDic[@"customTop"]=[NSString stringWithFormat:@"%.02f",width];
    productDic[@"uniformWidth"]=[NSString stringWithFormat:@"%.02f",width];
    productDic[@"isUni"]=@"1";
    
    if ([arr[0][@"style"] isEqualToString:@"Single"]) {
        [self setSingleFrameWhereParentView:_frameView andSingleView:singleFrame andDoubleView:doubleFrame andImage:_mainImage andSingleMatImage:singleMatImage andSidemargineMain:margineMain];
    }
    else{
        [self setDoubleFrame];
    }
    [self getItemPrice];
}

-(void)didEnterTopWidthForTopMatSetting:(float)width{
    NSMutableArray *arr=[db getDataForField:USER_PRODUCT where:nil];
    
    if (arr.count!=0) {
        arr[0][@"top"]=[NSString stringWithFormat:@"%.02f",width];
        arr[0][@"isUni"]=@"0";
        [db update:USER_PRODUCT :arr[0] :[NSString stringWithFormat:@"id='%@'",arr[0][@"id"]]];
        
    }
    productDic[@"customTop"]=[NSString stringWithFormat:@"%.02f",width];
    productDic[@"isUni"]=@"0";
    if ([arr[0][@"style"] isEqualToString:@"Single"]) {
        [self setSingleFrameWhereParentView:_frameView andSingleView:singleFrame andDoubleView:doubleFrame andImage:_mainImage andSingleMatImage:singleMatImage andSidemargineMain:margineMain];
    }
    else{
        [self setDoubleFrame];
    }
    
    [self getItemPrice];
    
}

-(void)didEnterBottomWidthForTopMatSetting:(float)width{
    
    NSMutableArray *arr=[db getDataForField:USER_PRODUCT where:nil];
    
    if (arr.count!=0) {
        arr[0][@"bottom"]=[NSString stringWithFormat:@"%.02f",width];
        arr[0][@"isUni"]=@"0";
        [db update:USER_PRODUCT :arr[0] :[NSString stringWithFormat:@"id='%@'",arr[0][@"id"]]];
        
    }
    productDic[@"customBottom"]=[NSString stringWithFormat:@"%.02f",width];
    productDic[@"isUni"]=@"0";
    if ([arr[0][@"style"] isEqualToString:@"Single"]) {
        [self setSingleFrameWhereParentView:_frameView andSingleView:singleFrame andDoubleView:doubleFrame andImage:_mainImage andSingleMatImage:singleMatImage andSidemargineMain:margineMain];
    }
    else{
        [self setDoubleFrame];
    }
    [self getItemPrice];
    
}
-(void)didEnterLeftWidthForTopMatSetting:(float)width{
    NSMutableArray *arr=[db getDataForField:USER_PRODUCT where:nil];
    
    if (arr.count!=0) {
        arr[0][@"left"]=[NSString stringWithFormat:@"%.02f",width];
        arr[0][@"isUni"]=@"0";
        [db update:USER_PRODUCT :arr[0] :[NSString stringWithFormat:@"id='%@'",arr[0][@"id"]]];
        
    }
    productDic[@"customLeft"]=[NSString stringWithFormat:@"%.02f",width];
    productDic[@"isUni"]=@"0";
    if ([arr[0][@"style"] isEqualToString:@"Single"]) {
        [self setSingleFrameWhereParentView:_frameView andSingleView:singleFrame andDoubleView:doubleFrame andImage:_mainImage andSingleMatImage:singleMatImage andSidemargineMain:margineMain];
    }
    else{
        [self setDoubleFrame];
    }
    [self getItemPrice];
    
}
-(void)didEnterRightWidthForTopMatSetting:(float)width{
    
    NSMutableArray *arr=[db getDataForField:USER_PRODUCT where:nil];
    
    if (arr.count!=0) {
        arr[0][@"right"]=[NSString stringWithFormat:@"%.02f",width];
        arr[0][@"isUni"]=@"0";
        [db update:USER_PRODUCT :arr[0] :[NSString stringWithFormat:@"id='%@'",arr[0][@"id"]]];
        
    }
    productDic[@"customRight"]=[NSString stringWithFormat:@"%.02f",width];
    productDic[@"isUni"]=@"0";
    if ([arr[0][@"style"] isEqualToString:@"Single"]) {
        [self setSingleFrameWhereParentView:_frameView andSingleView:singleFrame andDoubleView:doubleFrame andImage:_mainImage andSingleMatImage:singleMatImage andSidemargineMain:margineMain];
    }
    else{
        [self setDoubleFrame];
    }
    [self getItemPrice];
}
-(void)didEnterUniformWidthForBottomMatSetting:(float)width{
    NSMutableArray *arr=[db getDataForField:USER_PRODUCT where:nil];
    
    if (arr.count!=0) {
        arr[0][@"bottomUniformWidth"]=[NSString stringWithFormat:@"%.02f",width];
        
        [db update:USER_PRODUCT :arr[0] :[NSString stringWithFormat:@"id='%@'",arr[0][@"id"]]];
        
    }
    productDic[@"doubleFrameWidth"]=[NSString stringWithFormat:@"%.02f",width];
    productDic[@"isUni"]=@"0";
    
    [self setDoubleFrame];
    
}


#pragma mark - POP API DELEGATES
-(void)connectionError{
    timeStamp=0;
}
-(void)requestFailedSessionExpired:(NSDictionary *)dataPack{
    
}
-(void)requestFailedServerError:(NSDictionary *)dataPack{
    
}
-(void)requestFailedServiceUnavailableDueToMaintenance:(NSDictionary *)dataPack{
    [Global showSimpleAlert:self andTitle:FRAMESHOP andMessage:dataPack[@"message"]];
}
-(void)requestFailedMethodNotFound:(NSDictionary *)dataPack{
    
}
-(void)requestFailedNotFound:(NSDictionary *)dataPack{
    
}
-(void)requestFailedUnauthorised:(NSDictionary *)dataPack{
     [self logInUser];
}
-(void)requestFailedBadRequest:(NSDictionary *)dataPack{
    
}
-(void)requestSucceededWithData:(NSDictionary *)dataPack{
    
    if ([dataPack[@"url"] isEqualToString:LOGIN_USER]) {
        
        [db delTableWithTableName:USERTABLE];
        NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
        
        dic[@"id"]              =[NSString stringWithFormat:@"%@",dataPack[@"data"][@"id"]];
        dic[@"name"]            =dataPack[@"data"][@"name"];
        dic[@"email"]           =dataPack[@"data"][@"email"];
        dic[@"created_at"]      =dataPack[@"data"][@"created_at"];
        dic[@"updated_at"]      =dataPack[@"data"][@"updated_at"];
        dic[@"token"]           =dataPack[@"data"][@"access_token"];
        
        [db insertIntoTable:dic table_name:USERTABLE];
        [self getDataForFrame];
    }
    
    else if ([dataPack[@"url"] isEqualToString:REGISTER_USER]){
        
        NSMutableDictionary *dic =[[NSMutableDictionary alloc] init];
        dic[@"token"]            =dataPack[@"data"][@"token"];
        dic[@"id"]               =[NSString stringWithFormat:@"%@",dic[@"id"]];
        
        [db insertIntoTable:dic table_name:USERTABLE];
        [self getDataForFrame];
    }
    
    else  if ([dataPack[@"url"] isEqualToString:GET_PRICE]){
        _priceCount.text=[NSString stringWithFormat:@"$%.2f",[dataPack[@"data"][@"price"] floatValue]] ;
        [_priceLoader stopAnimating];
        _priceLoader.hidden=true;
        NSMutableArray*    tempArr=[db getDataForField:USER_PRODUCT where:nil];
        
        
        tempArr[0][@"price"] =[NSString stringWithFormat:@"%.2f",[dataPack[@"data"][@"price"] floatValue]];
        tempArr[0][@"shipping_price"] =[NSString stringWithFormat:@"%.2f",[dataPack[@"data"][@"shipping_charges"] floatValue]];
        [db update:USER_PRODUCT :tempArr[0] :[NSString stringWithFormat:@"id='%@'",tempArr[0][@"id"]]];
        _nxtBtrn.backgroundColor=APP_BLUE_COLOR;
        _nxtBtrn.enabled=YES;
        
    }
    
    else {
        
        NSMutableArray *arr=[dataPack[@"data"][@"popular_frames"] mutableCopy];
        [arr addObjectsFromArray:[dataPack[@"data"][@"on_sale_frames"] mutableCopy]];
        
        [db delTableWithTableName:FRAME_TABLE];
        for (int i=0; i<arr.count; i++) {
            if ([arr[i][@"status"] isEqualToString:@"Active"]) {
                
                
                NSMutableDictionary *temDic=[[NSMutableDictionary alloc] init];
                
                temDic[@"id"]                =[NSString stringWithFormat:@"%@",arr[i][@"id"]];
                temDic[@"base_price"]        =[NSString stringWithFormat:@"%@",arr[i][@"base_price"]];
                temDic[@"color"]             =arr[i][@"color"];
                temDic[@"frame_category_id"] =[NSString stringWithFormat:@"%@",arr[i][@"frame_category_id"]];
                temDic[@"is_popular"]        = [NSString stringWithFormat:@"%@",arr[i][@"is_popular"]];
                
                temDic[@"is_on_sale"]  =[NSString stringWithFormat:@"%@",arr[i][@"is_on_sale"]];
                temDic[@"max_limit"]   =[NSString stringWithFormat:@"%@",arr[i][@"max_limit"]];
                temDic[@"min_limit"]   =[NSString stringWithFormat:@"%@",arr[i][@"min_limit"]];
                
                temDic[@"height"]   =[NSString stringWithFormat:@"%@",arr[i][@"height"]];
                temDic[@"width"]   =[NSString stringWithFormat:@"%@",arr[i][@"width"]];
                temDic[@"rebate"]   =[NSString stringWithFormat:@"%@",arr[i][@"rebate"]];
                
                temDic[@"sku"]     =arr[i][@"SKU"];
                temDic[@"tags"]    =[NSString stringWithFormat:@"%@",arr[i][@"tags"]];
                
                temDic[@"material_description"] =arr[i][@"frame_material"][@"description"];
                temDic[@"material_name"]        =arr[i][@"frame_material"][@"name"];
                
              
                if (arr[i][@"location"] == (id)[NSNull null] || [arr[i][@"location"] length]==0 ){
                    temDic[@"location"] = @"NA";
                }
                else{
                    temDic[@"location"] = arr[i][@"location"];
                }
                
                for (int j=0; j<[arr[i][@"frame_image"] count]; j++) {
                    if ([arr[i][@"frame_image"][j][@"image_property"] isEqualToString:@"C"]) {
                        temDic[@"image_url_c"]=arr[i][@"frame_image"][j][@"image_url"];
                    }
                    else{
                        temDic[@"image_url_t"]=arr[i][@"frame_image"][j][@"image_url"];
                        
                    }
                }
                [db insertIntoTable:temDic table_name:FRAME_TABLE];
            }
            
        }
        [db delTableWithTableName:FRAME_CATEGORIES];
        NSMutableArray *arr2=[dataPack[@"data"][@"frame_categories"] mutableCopy];
        for (int i=0; i<arr2.count; i++) {
            
            NSMutableDictionary *temDic=[[NSMutableDictionary alloc] init];
            
            temDic[@"id"]                =[NSString stringWithFormat:@"%@",arr2[i][@"id"]];
            temDic[@"name"]              =[NSString stringWithFormat:@"%@",arr2[i][@"name"]];
            temDic[@"description"]       =arr2[i][@"description"];
            temDic[@"is_deleted"]        =[NSString stringWithFormat:@"%@",arr2[i][@"is_deleted"]];
            temDic[@"mini_icon"]        = [NSString stringWithFormat:@"%@",arr2[i][@"mini_icon"]];
            
            NSArray *tem = [arr2[i][@"name"] componentsSeparatedByString:@"-"];
            if (tem.count>1) {
                temDic[@"name"]=tem[1];
            }
            
            [db insertIntoTable:temDic table_name:FRAME_CATEGORIES];
        }
        
        [self refreshFrames];
    }
    
}
-(void)requestSucceededButActionFailed:(NSDictionary *)dataPack{
    
    if ([dataPack[@"url"] isEqualToString:REGISTER_USER] || [dataPack[@"message"] isEqualToString:@"Unauthenticated."]) {
        [self logInUser];
    }
    else if ([dataPack[@"url"] isEqualToString:LOGIN_USER]) {
        
        [self signUpUser];
        NSLog(@"something bad happned with server login signup both failed");
    }
    else{
        NSLog(@"there is some problme with product api");
        timeStamp=0;
        
    }
    
}
-(void)uploadedData:(NSString *)pecentage andByte:(float)bytes{
    
}
-(void)requestSucceededButNoDataFound:(NSDictionary *)dataPack{
    
}
-(void)requestFailedDueToServerIsuueWithError:(NSString*)errorDes{
    
}


@end

