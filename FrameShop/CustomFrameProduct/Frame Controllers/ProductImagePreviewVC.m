//
//  ProductImagePreviewVC.m
//  FrameShop
//
//  Created by vamika on 12/06/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import "ProductImagePreviewVC.h"
#import "Global.h"
@interface ProductImagePreviewVC ()<UIScrollViewDelegate>

@end

@implementation ProductImagePreviewVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.imageLoader startAnimating];
    _scrollView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    _imageView.frame = CGRectMake(10, 0, self.scrollView.frame.size.width-20, self.scrollView.frame.size.height);
   // [Global roundCorner:_doneButton roundValue:_doneButton.frame.size.height/2 borderWidth:1.0 borderColor:APP_BLUE_COLOR];
    self.scrollView.minimumZoomScale = 1.0;
    self.scrollView.maximumZoomScale = 2.0;
    self.scrollView.zoomScale = 1.0;
    self.scrollView.delegate = self ;
//    UITapGestureRecognizer *singleFingerTap =
//    [[UITapGestureRecognizer alloc] initWithTarget:self
//     [self.view addGestureRecognizer:singleFingerTap]                                  action:@selector(handleSingleTap:)];
    
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [self.view addGestureRecognizer:singleFingerTap];
   // [self.frameView addGestureRecognizer:singleFingerTap];
    
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    _imageView.image = self.previewImageDic[@"image"];
    if (_imageView.image != nil) {
        [self.imageLoader stopAnimating];
        self.imageLoader.hidden = YES;
    }
    
}
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{
    return self.imageView;
}
- (IBAction)onclickDone:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer{
    [self dismissViewControllerAnimated:NO completion:nil];
}
@end
