//
//  CreatFrameScript.h
//  FrameShop
//
//  Created by Saurabh Srivastav on 16/06/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN
@class CreatFrameScript;
@protocol CreatFrameScriptDelegate <NSObject>
@optional
//-(void)didCreatedFrameSuccessfully:(UIImage *)previewImage andFramePreview:(UIImage *)framePreview;
-(void)didCreatedFrameSuccessfully;

@end
@interface CreatFrameScript : NSObject
@property float barWidth;
@property UIImage *frameImage,*preview,*framePreview,*mainImage;
@property UIView *parentView,*childrenView,*singleFrameView,*doubleFrameView;
@property UIImageView *mainImageView;
-(void)changeFrameAppearance;
-(void)addFrameToChild;

@property(strong,nonatomic)id<CreatFrameScriptDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
