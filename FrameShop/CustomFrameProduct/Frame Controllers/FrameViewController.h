//
//  FrameViewController.h
//  FrameShop
//
//  Created by Saurabh anand on 19/12/19.
//  Copyright © 2019 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PopApi.h"
#import "DBManager.h"
#import "Global.h"
#import "UploadImageViewController.h"
#import "SummaryPopUpView.h"
#import "ProductImagePreviewVC.h"
#import "ProductImagePreviewVC.h"
NS_ASSUME_NONNULL_BEGIN

@protocol FrameViewControllerDelegate <NSObject>
@optional
-(void)didFinishCart;


@end
@interface FrameViewController : UIViewController<PopApiDelegate,UploadImageViewControllerDelegate>
@property(strong,nonatomic)id<FrameViewControllerDelegate> delegate;

- (IBAction)addToCartAction:(id)sender;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *addToCartButton;
- (IBAction)onclickCart:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *cartCountLbl;
@property (weak, nonatomic) IBOutlet UIButton *menuButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *addTocartWidth;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loader;
- (IBAction)onMenuClicked:(id)sender;

@end

NS_ASSUME_NONNULL_END
