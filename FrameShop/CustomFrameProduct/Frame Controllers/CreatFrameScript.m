//
//  CreatFrameScript.m
//  FrameShop
//
//  Created by Saurabh Srivastav on 16/06/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import "CreatFrameScript.h"

@implementation CreatFrameScript{
    int imageCount;
    CGRect cropFrame;
    UIView *previewView;
  
  
}

#pragma mark - Creat Frame
-(void)changeFrameAppearance {
    
    
   
  //  [borderImageViews removeAllObjects];
    [self deleteFilesFromCacheWhichContain:@"border"];




    float temY=0.0f;
    int count=0;

 float height=_frameImage.size.height;
 float width =_frameImage.size.width;

    if (width>height) {
        width=height;
    }

    
    count=0;
    temY=0;
    do{
        
      
       UIImage *temImage=[self imageByCropping:_frameImage toRect:CGRectMake(temY, 0, width, height)];
       
            
        if (temImage.size.height*0.40<=temImage.size.width) {
            [self saveImagesInDocumentDirectory:UIImagePNGRepresentation(temImage) ImageId:[NSString stringWithFormat:@"border%dU",count]];
             count++;
            
        }
        
        temY=temY+width;
        
       
        temImage=nil;
        
    }while (temY<=_frameImage.size.width);
    
  
  
    
}

-(void)addFrameToChild {

    cropFrame=_childrenView.frame;
    
    
   
    
   
       NSArray *arr = [_parentView subviews];
  
   for (UIView* b in arr)
         {
             if ((b.tag==2)) {
               
                 [b removeFromSuperview];
                
             }
           
         }
    
  
   float frameBarWidth=_barWidth;
    [self setUpRightbarWithBarWidth:frameBarWidth andBaseFrame:_childrenView.frame andParentView:_parentView];
   [self setUpLeftbarWithBarWidth:frameBarWidth andBaseFrame:_childrenView.frame andParentView:_parentView];
   [self setUpUpbarWithBarWidth:frameBarWidth   andBaseFrame:_childrenView.frame andParentView:_parentView];
   [self setUpDownbarWithBarWidth:frameBarWidth andBaseFrame:_childrenView.frame andParentView:_parentView];
    
   // [self getPreviewImages];
  
    [self.delegate didCreatedFrameSuccessfully];
   
}



-(void)setUpRightbarWithBarWidth:(float)barWidth andBaseFrame:(CGRect)mainFrame andParentView:(UIView *)parentView{
    float heightOfFrameBar=mainFrame.size.height+barWidth;
    
    int totalTiles=(int)  ceil(heightOfFrameBar/barWidth) ;
    
    float y=mainFrame.origin.y-barWidth;
    imageCount=0;
    
    for (int i=0; i<totalTiles; i++) {
        UIImageView *imageView=[[UIImageView alloc] init];
        float width=barWidth;
        float height=barWidth;
       
        imageView.frame=CGRectMake(mainFrame.origin.x+mainFrame.size.width, y,width, height);
      
                imageView.image=[self getImageTilesOfFrame];
                 imageView.tag=2;
                
                [parentView addSubview:imageView];
        
          imageView.transform = CGAffineTransformMakeRotation(M_PI_2);
        
         
        if (i==totalTiles-1) {
            CGRect temp=imageView.frame;
            temp.size.height=mainFrame.origin.y+mainFrame.size.height-y+barWidth;
            temp.origin.y=y;
            imageView.frame=temp;
            
            
        }
        
       y=y+barWidth;
        
    }
    
    
}


-(void)setUpLeftbarWithBarWidth:(float)barWidth andBaseFrame:(CGRect)mainFrame andParentView:(UIView *)parentView{
    float heightOfFrameBar=mainFrame.size.height+barWidth;
    
    int totalTiles=(int)  ceil(heightOfFrameBar/barWidth) ;
    
    float y=mainFrame.origin.y+mainFrame.size.height;
    imageCount=0;
    for (int i=0; i<totalTiles; i++) {
        UIImageView *imageView=[[UIImageView alloc] init];
        float width=barWidth;
        float height=barWidth;
        
        imageView.frame=CGRectMake(mainFrame.origin.x-barWidth, y,width, height);
        
        double radi=270*M_PI/180;
         
                imageView.image=[self getImageTilesOfFrame];
                 imageView.tag=2;
                
                [parentView addSubview:imageView];
     
          //imageView.transform = CGAffineTransformMakeRotation(4.7123872954);
        imageView.transform = CGAffineTransformMakeRotation(radi);
         
        if (i==totalTiles-1) {
            CGRect temp=imageView.frame;
            temp.size.height=y+barWidth-(mainFrame.origin.y-barWidth);
            temp.origin.y=mainFrame.origin.y-barWidth;
            imageView.frame=temp;
            
            
           
            //[frameBgView addSubview:imageView];
            break;
        }
        
       y=y-barWidth;
        
    }
    
    
}

-(void)setUpUpbarWithBarWidth:(float)barWidth andBaseFrame:(CGRect)mainFrame andParentView:(UIView *)parentView{
    
    float widthOfFrameBar=mainFrame.size.width+barWidth+.5;
    int totalTiles=(int)  ceil(widthOfFrameBar/barWidth) ;
    
    float x=mainFrame.origin.x-barWidth;
   imageCount=0;
    
    
    for (int i=0; i<totalTiles; i++) {
        
        UIImageView *imgview=[[UIImageView alloc] init];
        float width=barWidth;
        float height=barWidth;
     
        
        
        
        
        if (i==totalTiles-1) {
            
            width= (mainFrame.origin.x-barWidth)+widthOfFrameBar-x;
        }
        
         imgview.frame=CGRectMake(x, mainFrame.origin.y-barWidth,width, height);
         
        // [frameBgView addSubview:imgview];
        x=x+barWidth;
        

        imgview.image=[self getImageTilesOfFrame];
        
        
         imgview.tag=2;
        
        [parentView addSubview:imgview];
        
        if (i==0) {

                    [self cutCorneresOfView:imgview whereCo1:CGPointMake(0, 0) andCo2:CGPointMake(barWidth,0) andCo3:CGPointMake(barWidth,barWidth)];
                  
        }
        
        
        
    }
    
    UIImageView *imgeView=[[UIImageView alloc] init];
    imgeView.frame=CGRectMake(mainFrame.origin.x+mainFrame.size.width-1, mainFrame.origin.y-barWidth, barWidth, barWidth);
    
    [parentView addSubview:imgeView];
    imgeView.tag=2;

       
    imgeView.image=[self getImageTilesOfFrame];
    

        
    [self cutCorneresOfView:imgeView whereCo1:CGPointMake(barWidth, 0) andCo2:CGPointMake(0,barWidth) andCo3:CGPointMake(-barWidth, -barWidth)];
       
    
}

-(void)setUpDownbarWithBarWidth:(float)barWidth andBaseFrame:(CGRect)mainFrame andParentView:(UIView *)parentView{
    
    float widthOfFrameBar=mainFrame.size.width+barWidth+.5;
    
    int totalTiles=(int)  ceil(widthOfFrameBar/barWidth) ;
    
    float x=mainFrame.origin.x-barWidth;
    imageCount=0;
    
    for (int i=0; i<totalTiles; i++) {
        
        UIImageView *imgview=[[UIImageView alloc] init];
        float width=barWidth;
        float height=barWidth;
      if (i==totalTiles-1) {
            
            width= (mainFrame.origin.x-barWidth)+widthOfFrameBar-x;
        }
        
         imgview.frame=CGRectMake(x, mainFrame.origin.y+mainFrame.size.height,width, height);
        
      
        x=x+barWidth;
       
      
        imgview.image=[self getImageTilesOfFrame];
         imgview.tag=2;
        
        [parentView addSubview:imgview];
        
        if (i==0) {
                   [self cutCorneresOfView:imgview whereCo1:CGPointMake(0, 0) andCo2:CGPointMake(barWidth,0) andCo3:CGPointMake(barWidth,barWidth)];
                   
        }
        
         imgview.layer.transform = CATransform3DMakeRotation(M_PI,1.0,0.0,0.0);
        
   
    }
    
    
    
    UIImageView *imgeView=[[UIImageView alloc] init];
    imgeView.frame=CGRectMake(mainFrame.origin.x+mainFrame.size.width-1, mainFrame.origin.y+mainFrame.size.height, barWidth, barWidth);
   
    [parentView addSubview:imgeView];
    imgeView.tag=2;
   
      
       
    imgeView.image=[self getImageTilesOfFrame];
    

        [self cutCorneresOfView:imgeView whereCo1:CGPointMake(barWidth, 0) andCo2:CGPointMake(0,barWidth) andCo3:CGPointMake(-barWidth, -barWidth)];
        imgeView.layer.transform = CATransform3DMakeRotation(M_PI,1.0,0.0,0.0);
    
}

-(void)cutCorneresOfView:(UIView *)view whereCo1:(CGPoint)point1 andCo2:(CGPoint)point2 andCo3:(CGPoint)point3  {
    UIBezierPath* trianglePath = [UIBezierPath bezierPath];
                  [trianglePath moveToPoint:point1];
                  [trianglePath addLineToPoint:point2];
                  [trianglePath addLineToPoint:point3];
                  [trianglePath closePath];

                  CAShapeLayer *shapeLayer = [CAShapeLayer layer];
                     shapeLayer.path = trianglePath.CGPath;
                     [view.layer setMask:shapeLayer];
               CGRect temp=view.frame;
               temp.origin.x=temp.origin.x+1;
               view.frame=temp;
}


-(UIImage *)getImageTilesOfFrame{
    UIImage *img;
//          if (borderImageViews[[NSString stringWithFormat:@"border%dU.jpg",imageCount]]!=nil) {
//              img=(UIImage *)borderImageViews[[NSString stringWithFormat:@"border%dU.jpg",imageCount]];
//          }
//          else{
               img=[self getImageFromDoucumentWithName:[NSString stringWithFormat:@"border%dU.png",imageCount]];
             
              if (img==nil) {
                  imageCount=0;
                  img=[self getImageFromDoucumentWithName:[NSString stringWithFormat:@"border%dU.png",imageCount]];
              }
             // float ratio=img.size.width/img.size.height;
            //  width=height*ratio;
            

//              borderImageViews[[NSString stringWithFormat:@"border%dU.jpg",imageCount]]=img;
         // }
          imageCount++;
    return img;
}

- (UIImage*)imageByCropping:(UIImage *)imageToCrop toRect:(CGRect)rect
{
    CGImageRef imageRef = CGImageCreateWithImageInRect([imageToCrop CGImage], rect);
    UIImage *cropped = [UIImage imageWithCGImage:imageRef];
    return cropped;
    
}
-(BOOL) saveImagesInDocumentDirectory:(NSData*) pngData ImageId:(NSString*) imageId{
    
    NSString *filePath = [self documentsPathForFileName:[NSString stringWithFormat:@"%@.png",imageId]];
    
    if([[NSFileManager defaultManager] fileExistsAtPath:filePath]){
        [self deleteItemFromPath:filePath];
    }
    
    [pngData writeToFile:filePath atomically:YES];
    
    return YES;
}


-(void)setSingleFrame{
    
}




-(void)deleteFileFromDocumentWithName:(NSString *)name{
    NSString *filePath = [self documentsPathForFileName:[NSString stringWithFormat:@"%@",name]];
    
    if([[NSFileManager defaultManager] fileExistsAtPath:filePath]){
        [self deleteItemFromPath:filePath];
    }
    
}
-(void)deleteFilesFromCacheWhichContain:(NSString *)name{
    
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    
    NSError *error;
    NSArray *directoryContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsPath error:&error];
    

    for (int i=0; i<[directoryContents count]; i++)
    {
        NSString* fileName = [directoryContents objectAtIndex:i];
        if ([fileName rangeOfString:name].location != NSNotFound){
            [self deleteFileFromDocumentWithName:fileName ];
            
        }
    }
    
}

- (NSString *)documentsPathForFileName:(NSString *)name
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    return [documentsPath stringByAppendingPathComponent:name];
}
-(void) deleteItemFromPath:(NSString*) imagePath{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    [fileManager removeItemAtPath:imagePath error:NULL];
}

-(UIImage *)getImageFromDoucumentWithName:(NSString *)imageName{
    
    if (([imageName isKindOfClass:[NSNull class]]) )
        return nil;
    
    imageName = [imageName stringByReplacingOccurrencesOfString: @".png" withString:@""];
    
    
    
    NSData *pngData = [NSData dataWithContentsOfFile:[self documentsPathForFileName:[NSString stringWithFormat:@"%@.png",imageName]]];
    UIImage *img = [[UIImage alloc] initWithData:pngData];
    
    if (img != nil) {
        return img;
    }
    else{
        return nil;
    }
}







@end
