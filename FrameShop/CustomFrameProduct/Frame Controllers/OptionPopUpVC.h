//
//  OptionPopUpVC.h
//  FrameShop
//
//  Created by Vamika on 13/05/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Global.h"
NS_ASSUME_NONNULL_BEGIN
@protocol OptionPopUpVCDelegate <NSObject>
@optional
-(void)didPressCategoryWithOptionDic:(NSMutableDictionary*)dic;
-(void)didPressSortingWithOptionDic:(NSIndexPath*)indexpath;
- (void)cancelPoopUp;
@end
@interface OptionPopUpVC : UIViewController
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableHeight;
@property(strong,nonatomic)id<OptionPopUpVCDelegate> delegate;
@property (weak, nonatomic) IBOutlet UITableView *optionTable;
@property (weak, nonatomic) IBOutlet UILabel *sampleLbl;
@property (weak, nonatomic) IBOutlet UIView *backView;
@property NSMutableArray *optionArray;
@property NSString *type;

@end

NS_ASSUME_NONNULL_END
