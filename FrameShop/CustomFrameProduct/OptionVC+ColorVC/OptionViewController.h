//
//  OptionViewController.h
//  FrameShop
//
//  Created by Saurabh anand on 23/12/19.
//  Copyright © 2019 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MatsSettingsView.h"
NS_ASSUME_NONNULL_BEGIN
@class OptionViewController;
@protocol OptionViewControllerDelegate <NSObject>
@optional
-(void)didSelectOption:(NSMutableDictionary *)infoDic;
-(void)didSelectOptionDimention:(NSMutableDictionary *)infoDic andValue:(float)value;
-(void)didChangeVGMatSetting:(BOOL)isChecked;

-(void)didEnterUniformWidthForTopMatSetting:(float)width;
-(void)didEnterTopWidthForTopMatSetting:(float)width;
-(void)didEnterBottomWidthForTopMatSetting:(float)width;
-(void)didEnterLeftWidthForTopMatSetting:(float)width;
-(void)didEnterRightWidthForTopMatSetting:(float)width;
-(void)didEnterUniformWidthForBottomMatSetting:(float)width;

@end
@interface OptionViewController : UIViewController<MatsSettingsViewDelegate>
@property(strong,nonatomic)id<OptionViewControllerDelegate> delegate;

@property NSMutableDictionary *infoDic;
@property CGRect previousFrame;

@property (weak, nonatomic) IBOutlet UIView *touchView;


@end

NS_ASSUME_NONNULL_END
