//
//  OptionViewController.m
//  FrameShop
//
//  Created by Saurabh anand on 23/12/19.
//  Copyright © 2019 Mayank Barnwal. All rights reserved.
//

#import "OptionViewController.h"
#import "Global.h"
#import "DBManager.h"

@interface OptionViewController (){
    NSMutableArray *optionArray;
    int selectedRow;
    NSIndexPath *previousIndexPath;
    NSMutableDictionary *dic,*cellDic;
    DbManager *db;
    NSMutableArray *arr;
  
}

@end

@implementation OptionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    dic=[[NSMutableDictionary alloc] init];
    cellDic=[[NSMutableDictionary alloc] init];
    
    
    optionArray=[[NSMutableArray alloc] init];
    selectedRow=4;
    self.title=_infoDic[@"value"];
    db=[[DbManager alloc] init];
    NSMutableArray *arr=[db getDataForField:USER_PRODUCT where:nil];
    
    
    MatsSettingsView *matSettings = [[MatsSettingsView alloc] initWithNibName:@"MatsSettingsView" bundle:nil];
    if ([arr[0][@"style"] isEqualToString:@"Double"]) {
         matSettings.view.frame = CGRectMake(0, self.view.frame.size.height-matSettings.view.frame.size.height-60, self.view.frame.size.width, matSettings.view.frame.size.height+60);
    }else{
    matSettings.view.frame = CGRectMake(0, self.view.frame.size.height-matSettings.view.frame.size.height, self.view.frame.size.width, matSettings.view.frame.size.height-5);
        matSettings.doubleView.hidden  = YES;
    }
    matSettings.view.clipsToBounds = YES;
    matSettings.delegate=self;
    [Global roundCorner:matSettings.view roundValue:10.0 borderWidth:0.0 borderColor:UIColor.clearColor];
    [self.view addSubview:matSettings.view];
    [self addChildViewController:matSettings];
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [self.touchView addGestureRecognizer:singleFingerTap];
    
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    

    
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
  
     [self dismissViewControllerAnimated:YES completion:nil];
}

-(NSDictionary *)cellDicWhereType:(NSString *)type andDisplayName:(NSString *)displayName {
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
    dic[@"type"]=type;
    dic[@"option"]=displayName;
    
    
    return dic;
}









#pragma mark - Mat Setting Delegate
-(void)didChangeVGMat:(BOOL)isChecked{
    [self.delegate didChangeVGMatSetting:isChecked];
}
-(void)didEnterUniformWidthForTopMat:(float)width{
    [self.delegate didEnterUniformWidthForTopMatSetting:width];
}
-(void)didEnterTopWidthForTopMat:(float)width{
    [self.delegate didEnterTopWidthForTopMatSetting:width];
}
-(void)didEnterBottomWidthForTopMat:(float)width{
    [self.delegate didEnterBottomWidthForTopMatSetting:width];
}
-(void)didEnterLeftWidthForTopMat:(float)width{
    [self.delegate didEnterLeftWidthForTopMatSetting:width];
}
-(void)didEnterRightWidthForTopMat:(float)width{
    [self.delegate didEnterRightWidthForTopMatSetting:width];
}
-(void)didEnterUniformWidthForBottomMat:(float)width{
    [self.delegate didEnterUniformWidthForBottomMatSetting:width];
}
#pragma mark - UniformWidth Delegate
-(void)didChooseWidth:(NSMutableDictionary *)infoDic andValue:(float)value{
    dic[infoDic[@"type"]]=[NSString stringWithFormat:@"%f",value];
    [self.delegate didSelectOptionDimention:infoDic andValue:value];
}

@end
