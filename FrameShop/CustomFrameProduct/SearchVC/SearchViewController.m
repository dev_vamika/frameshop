//
//  SearchViewController.m
//  FrameShop
//
//  Created by Saurabh anand on 20/01/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import "SearchViewController.h"

@interface SearchViewController (){
    NSMutableArray *searchArray;
    DbManager *db;
 
}

@end

@implementation SearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    searchArray=[[NSMutableArray alloc] init];

    db=[[DbManager alloc] init];
    UINib * nib = [UINib nibWithNibName:@"FrameCardCell" bundle:nil];
    [_collectionView registerNib:nib forCellWithReuseIdentifier:@"FrameCardCell"];
    _activityLoader.hidden=true;
    [self setUpView];
    // Do any additional setup after loading the view.
}

-(void)setUpView{
    [_searchBar becomeFirstResponder];
  //  self->_searchBar.frame  = CGRectMake(0, 0, self.view.bounds.size.width,self->_searchBar.frame.size.height);
 // self->_collectionView.frame  = CGRectMake(0, self->_searchBar.frame.size.height, self.view.bounds.size.width,self.view.bounds.size.height*0.40);
 //   _activityLoader.frame=CGRectMake(self.searchBar.frame.size.width-_activityLoader.frame.size.width*6, (_searchBar.frame.size.height-_activityLoader.frame.size.height)/2, _activityLoader.frame.size.width, _activityLoader.frame.size.height);
    _activityLoader.hidden=true;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self animateView];
}

-(void)animateView{
  
    [UIView animateWithDuration:0.0 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        
           
       } completion:^(BOOL finished) {
           
       }];
}




-(void)getSearchFrameFromServerWhereString:(NSString *)searchText{
   
    _activityLoader.hidden=false;
    [_activityLoader startAnimating];
    PopApi *api=[[PopApi alloc] init];
      api.delegate=self;
    NSString *urlString=[NSString stringWithFormat:@"%@%@%@",SERVER_ADDRESS,SEARCH_FRAMES,searchText];
    
    NSMutableArray *arr=[[NSMutableArray alloc] init];
    arr=[db getDataForField:USERTABLE where:nil];
    
    if (arr.count!=0) {
        [api sendGetRequst:urlString andToken:arr[0][@"token"]];
    }
   
    
}


-(void)signUpUser{
    
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
   dic[@"name"]=[[UIDevice currentDevice] name];
    dic[@"email"]=[NSString stringWithFormat:@"%@@frameshop.com.au",[Global getDeviceId]];
    dic[@"password"]=@"qwxcjeub32ne";
    dic[@"c_password"]=@"qwxcjeub32ne";
    dic[@"url"]=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,REGISTER_USER];
    [api sendPostRequst:dic];
    
}
-(void)logInUser{
   
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
   
   dic[@"email"]=[NSString stringWithFormat:@"%@@frameshop.com.au",[Global getDeviceId]];
    dic[@"password"]=@"qwxcjeub32ne";
   
    dic[@"url"]=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,LOGIN_USER];
    [api sendPostRequst:dic];
}






#pragma mark - UICollectionViewDataSource and Delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return searchArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"FrameCardCell";
    FrameCardCell *frameView =[_collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
   
    [Global roundCorner:frameView.contentView roundValue:0 borderWidth:0.5 borderColor:[UIColor lightGrayColor]];
    
    frameView.infoDic=searchArray[indexPath.row];
    frameView.CurrentIndex = indexPath;
   
     frameView.expendedView.hidden=YES;
     frameView.imageLeftMargine.constant=10;
    [frameView refreshCell];
    return frameView;
}
- (CGSize)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
     return CGSizeMake((self.collectionView.frame.size.width/4)-5, _collectionView.frame.size.height*0.95);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    //sas delegate unrecognized
    [db insertIntoTable:searchArray[indexPath.row] table_name:FRAME_TABLE];
    
    [self.delegate didChooseSearchFrameOption:searchArray[indexPath.row]];
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark Search bar delegate
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
   // NSLog(@"====>%@",searchBar.text);
}

- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
  // NSLog(@"====>%@",_searchBar.text);
    
    return YES;
    
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText;{
    
    
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
  
    [self getSearchFrameFromServerWhereString:_searchBar.text];
     [_collectionView reloadData];
}
- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - POP API DELEGATES
-(void)connectionError{
    _activityLoader.hidden=true;
    [_activityLoader stopAnimating];
}
-(void)requestFailedSessionExpired:(NSDictionary *)dataPack{
    _activityLoader.hidden=true;
       [_activityLoader stopAnimating];
}
-(void)requestFailedServerError:(NSDictionary *)dataPack{
    _activityLoader.hidden=true;
       [_activityLoader stopAnimating];
}
-(void)requestFailedServiceUnavailableDueToMaintenance:(NSDictionary *)dataPack{
    _activityLoader.hidden=true;
       [_activityLoader stopAnimating];
    [Global showSimpleAlert:self andTitle:FRAMESHOP andMessage:dataPack[@"message"]];
}
-(void)requestFailedMethodNotFound:(NSDictionary *)dataPack{
    _activityLoader.hidden=true;
       [_activityLoader stopAnimating];
}
-(void)requestFailedNotFound:(NSDictionary *)dataPack{
    _activityLoader.hidden=true;
       [_activityLoader stopAnimating];
}
-(void)requestFailedUnauthorised:(NSDictionary *)dataPack{
    _activityLoader.hidden=true;
       [_activityLoader stopAnimating];
     [self logInUser];
}
-(void)requestFailedBadRequest:(NSDictionary *)dataPack{
    _activityLoader.hidden=true;
       [_activityLoader stopAnimating];
}
-(void)requestSucceededWithData:(NSDictionary *)dataPack{
    
    if ([dataPack[@"url"] isEqualToString:LOGIN_USER]) {
        
        [db delTableWithTableName:USERTABLE];
        NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
       
        dic[@"id"]              =[NSString stringWithFormat:@"%@",dataPack[@"data"][@"id"]];
        dic[@"name"]            =dataPack[@"data"][@"name"];
        dic[@"email"]           =dataPack[@"data"][@"email"];
        dic[@"created_at"]      =dataPack[@"data"][@"created_at"];
        dic[@"updated_at"]      =dataPack[@"data"][@"updated_at"];
        dic[@"token"]           =dataPack[@"data"][@"access_token"];
        
        [db insertIntoTable:dic table_name:USERTABLE];
       
     }
    
    else if ([dataPack[@"url"] isEqualToString:REGISTER_USER]){
      
        NSMutableDictionary *dic =[[NSMutableDictionary alloc] init];
        dic[@"token"]            =dataPack[@"data"][@"token"];
        dic[@"id"]               =[NSString stringWithFormat:@"%@",dic[@"id"]];
          
        [db insertIntoTable:dic table_name:USERTABLE];
       
    }
    else{
        _activityLoader.hidden=true;
           [_activityLoader stopAnimating];
        // reload table with product data
        [searchArray removeAllObjects];
        NSMutableArray *arr=[dataPack[@"data"][@"frames"] mutableCopy];
        
    
      
        for (int i=0; i<arr.count; i++) {
            if ([arr[i][@"status"] isEqualToString:@"Active"]) {
             
            NSMutableDictionary *temDic=[[NSMutableDictionary alloc] init];
            
            temDic[@"id"]                =[NSString stringWithFormat:@"%@",arr[i][@"id"]];
            temDic[@"base_price"]        =[NSString stringWithFormat:@"%@",arr[i][@"base_price"]];
            temDic[@"color"]             =arr[i][@"color"];
            temDic[@"frame_category_id"] =[NSString stringWithFormat:@"%@",arr[i][@"frame_category_id"]];
            temDic[@"is_popular"]        = [NSString stringWithFormat:@"%@",arr[i][@"is_popular"]];
            
            temDic[@"is_on_sale"]  =[NSString stringWithFormat:@"%@",arr[i][@"is_on_sale"]];
            temDic[@"max_limit"]   =[NSString stringWithFormat:@"%@",arr[i][@"max_limit"]];
            temDic[@"min_limit"]   =[NSString stringWithFormat:@"%@",arr[i][@"min_limit"]];
           
            temDic[@"height"]   =[NSString stringWithFormat:@"%@",arr[i][@"height"]];
            temDic[@"width"]   =[NSString stringWithFormat:@"%@",arr[i][@"width"]];
            temDic[@"rebate"]   =[NSString stringWithFormat:@"%@",arr[i][@"rebate"]];
           
            temDic[@"sku"]     =arr[i][@"SKU"];
            temDic[@"tags"]    =[NSString stringWithFormat:@"%@",arr[i][@"tags"]];
           
          //  NSLog(@"%@",arr[i][@"frame_material"]);
            if (arr[i][@"frame_material"] == (id)[NSNull null] ){
                temDic[@"material_description"] =@"NA";
                temDic[@"material_name"]        =@"NA";
            }
            else{
                temDic[@"material_description"] =arr[i][@"frame_material"][@"description"];
                 temDic[@"material_name"]        =arr[i][@"frame_material"][@"name"];
            }
           // temDic[@"material_description"] =arr[i][@"frame_material"][@"description"];
           // temDic[@"material_name"]        =arr[i][@"frame_material"][@"name"];
            
           
            
            for (int j=0; j<[arr[i][@"frame_image"] count]; j++) {
                if ([arr[i][@"frame_image"][j][@"image_property"] isEqualToString:@"C"]) {
                    temDic[@"image_url_c"]=arr[i][@"frame_image"][j][@"image_url"];
                }
                else{
                    temDic[@"image_url_t"]=arr[i][@"frame_image"][j][@"image_url"];

                }
            }
           
            searchArray[searchArray.count]=temDic;
            
           }
        }
        
        
        [_collectionView reloadData];
    
    
    }
    

}
-(void)requestSucceededButActionFailed:(NSDictionary *)dataPack{
    
    if ([dataPack[@"url"] isEqualToString:REGISTER_USER] || [dataPack[@"message"] isEqualToString:@"Unauthenticated."]) {
       //go To login
       
        [self logInUser];
    }
   else if ([dataPack[@"url"] isEqualToString:LOGIN_USER]) {
          //something bad happned with server
       [self signUpUser];
       NSLog(@"something bad happned with server login signup both failed");
       }
   else{
       _activityLoader.hidden=true;
          [_activityLoader stopAnimating];
        NSLog(@"there is some problme with product api");
       //there is some problme with product api
   }
    
   
}
-(void)uploadedData:(NSString *)pecentage andByte:(float)bytes{
    
}
-(void)requestSucceededButNoDataFound:(NSDictionary *)dataPack{
    _activityLoader.hidden=true;
       [_activityLoader stopAnimating];
}
-(void)requestFailedDueToServerIsuueWithError:(NSString*)errorDes{
    _activityLoader.hidden=true;
       [_activityLoader stopAnimating];
}

@end
