//
//  SearchViewController.h
//  FrameShop
//
//  Created by Saurabh anand on 20/01/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PopApi.h"
#import "Global.h"
#import "DBManager.h"
#import "FrameCardCell.h"
NS_ASSUME_NONNULL_BEGIN

@class SearchViewController;
@protocol SearchViewControllerDelegate <NSObject>
@optional
-(void)didChooseSearchFrameOption:(NSMutableDictionary *)infoDic;

@end
@interface SearchViewController : UIViewController<PopApiDelegate,UISearchBarDelegate,FrameCardCellDelegate>
@property(strong,nonatomic)id<SearchViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityLoader;
@property CGRect priviousFrame;
@end

NS_ASSUME_NONNULL_END
