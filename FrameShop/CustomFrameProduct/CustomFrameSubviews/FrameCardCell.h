//
//  FrameCardCell.h
//  FrameShop
//
//  Created by Saurabh Srivastav on 14/04/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol FrameCardCellDelegate <NSObject>
@optional
-(void)didSelectFrameType:(NSMutableDictionary *)infoDic index:(NSIndexPath*)indexPath;
-(void)imageUpdatedSucceessfully:(UIImage *)image index:(NSIndexPath*)indexPath;
@end

@interface FrameCardCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *previewImage;
@property(strong,nonatomic)id<FrameCardCellDelegate> delegate;
@property NSIndexPath *CurrentIndex;
@property NSMutableDictionary *infoDic;
-(void)refreshCell;
@property (weak, nonatomic) IBOutlet UILabel *sku;
@property (weak, nonatomic) IBOutlet UILabel *width;
@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UIStackView *lblStack;
@property (weak, nonatomic) IBOutlet UIView *expendedView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageLeftMargine;
@property (weak, nonatomic) IBOutlet UILabel *stackSku;
@property (weak, nonatomic) IBOutlet UILabel *stackWidth;
@property (weak, nonatomic) IBOutlet UILabel *depth;
@property (weak, nonatomic) IBOutlet UILabel *rebate;
@property (weak, nonatomic) IBOutlet UILabel *mat;
@property (weak, nonatomic) IBOutlet UILabel *color;

@end

NS_ASSUME_NONNULL_END
