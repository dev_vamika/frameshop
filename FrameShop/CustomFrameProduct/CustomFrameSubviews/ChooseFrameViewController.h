//
//  ChooseFrameViewController.h
//  FrameShop
//
//  Created by Saurabh anand on 20/12/19.
//  Copyright © 2019 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Global.h"
#import "PopApi.h"
#import "FrameCardCell.h"

NS_ASSUME_NONNULL_BEGIN
@class ChooseFrameViewController;
@protocol ChooseFrameViewControllerDelegate <NSObject>
@optional
-(void)didChooseFrameOption:(NSMutableDictionary *)infoDic;
-(void)didChooseOptionType:(NSMutableDictionary *)infoDic;
-(void)didChooseSearchOption:(UISearchBar *)searchBar;
-(void)didPressSortingWithOptionArray:(NSMutableArray *)infoArr;
-(void)didPressCategoryWithOptionArray:(NSMutableArray *)infoArr;
-(void)didPressSkip;
- (void)framAction:(int)index;
- (void)matsAction:(int)index;
@end
@interface ChooseFrameViewController : UIViewController<PopApiDelegate,FrameCardCellDelegate,UISearchBarDelegate>
@property(strong,nonatomic)id<ChooseFrameViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@property NSMutableArray *frameArray;
@property (weak, nonatomic) IBOutlet UIView *topView;
-(void)updateCell:(NSMutableDictionary *)updatedDic;
@property (weak, nonatomic) IBOutlet UIButton *frameBtn;
@property (weak, nonatomic) IBOutlet UIButton *matsBtn;
@property (weak, nonatomic) IBOutlet UILabel *warningLbl;
- (IBAction)framButtonAction:(id)sender;
- (IBAction)matsAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *dropDownBtn;
- (IBAction)rightButtonAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *serachButton;
- (IBAction)onSearch:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *rightBtn;
@property (weak, nonatomic) IBOutlet UILabel *catTypeLbl;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property BOOL isLoaded;
@property (weak, nonatomic) IBOutlet UIView *chooseOptionView;
@property (weak, nonatomic) IBOutlet UILabel *optionTitleButton;
@property NSString *type;
-(void)wrapCell:(NSMutableDictionary *)infoDic;
-(void)refreshView;
-(void)closeAllOption;
@property NSString *defaultSku;
-(void)didSelectCategory:(NSMutableDictionary*)dic;
@property (weak, nonatomic) IBOutlet UIImageView *listIcon;
-(void)didSelectSortingWithIndex:(NSIndexPath*)index;
-(void)dissmissPopUp;
@end

NS_ASSUME_NONNULL_END
