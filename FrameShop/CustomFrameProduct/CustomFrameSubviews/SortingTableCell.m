//
//  SortingTableCell.m
//  FrameShop
//
//  Created by Vamika on 24/03/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import "SortingTableCell.h"

@implementation SortingTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
