//
//  MatsSubView.m
//  FrameShop
//
//  Created by Saurabh anand on 20/12/19.
//  Copyright © 2019 Mayank Barnwal. All rights reserved.
//

#import "MatsSubView.h"
#import "Global.h"
#import "AlbumCollectionViewCell.h"
#import "DBManager.h"
@interface MatsSubView (){
    NSMutableArray *optionArray;
    int selectedRow;
    NSMutableArray *dataArray;
    NSIndexPath *previousIndexPathSingle,*previousIndexPathDouble;
    DbManager *db;
    NSString *matId;
    int singleViewWidthConstant;
    UIImageView *topImageView,*bottomImageView;
}

@end

@implementation MatsSubView

- (void)viewDidLoad {
    [super viewDidLoad];
    [Global roundCorner:_settingButton roundValue:10 borderWidth:0 borderColor:nil];
    [Global roundCorner:_optionalBtn roundValue:10 borderWidth:0 borderColor:nil];
    [Global roundCorner:_colorBtn roundValue:5 borderWidth:0 borderColor:nil];
    [Global roundCorner:self.colorDetailContainerView roundValue:10 borderWidth:0 borderColor:nil];
    [Global roundCorner:_tableView roundValue:5 borderWidth:0.2 borderColor:[UIColor lightGrayColor]];
    [Global roundCorner:_frameBtn roundValue:_frameBtn.frame.size.height/2 borderWidth:0 borderColor:nil];
    [Global roundCorner:_matBtn roundValue:_matBtn.frame.size.height/2 borderWidth:0 borderColor:nil];
    
    [Global roundCorner:_noneButton roundValue:10 borderWidth:0.0 borderColor:[UIColor lightGrayColor]];
    [Global roundCorner:_singleButton roundValue:10 borderWidth:0.0 borderColor:[UIColor lightGrayColor]];
    [Global roundCorner:_doubleButton roundValue:10 borderWidth:0.0 borderColor:[UIColor lightGrayColor]];
    [Global roundCorner:_topMatBg roundValue:10 borderWidth:1.0 borderColor:[UIColor lightGrayColor]];
    [Global roundCorner:_BottomMatBg roundValue:10 borderWidth:1.0 borderColor:[UIColor lightGrayColor]];
    [Global roundCorner:_topMatColor roundValue:_topMatColor.frame.size.height/2 borderWidth:1.0 borderColor:[UIColor lightGrayColor]];
    [Global roundCorner:_BottomMatColor roundValue:_BottomMatColor.frame.size.height/2 borderWidth:1.0 borderColor:[UIColor lightGrayColor]];
    
    
    
    self.BottomMatBg.hidden  = YES;
    self.topMatBg.hidden     = YES;
    _tableView.hidden        = YES;
    
    dataArray  =[[NSMutableArray alloc] init];
    db         =[[DbManager alloc] init];
    optionArray=[[NSMutableArray alloc] init];
    
    UINib * nib = [UINib nibWithNibName:@"AlbumCollectionViewCell" bundle:nil];
    
    [_colorCollectionView  registerNib:nib forCellWithReuseIdentifier:@"AlbumCollectionViewCell"];
    [_doubleCollectionView registerNib:[UINib nibWithNibName:@"AlbumCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"AlbumCollectionViewCell"];
    
    _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    selectedRow=4;
    matId      =@"1";
    
    //[self setDicToArray:@"mats_settings" andValue:@"V-Groove" toArray:optionArray] ;
    [self getDataForMat];
    
    [_tableView reloadData           ];
    [_colorCollectionView  reloadData];
    [_doubleCollectionView reloadData];
    _settingButton.enabled=false;
    [self onClickNone:_noneButton];
}

-(void)viewWillAppear:(BOOL)animated{
    //[self onClickNone:_noneButton];
    [super viewWillAppear:animated];
}







-(void)getDataForMat{
//    _collectionContainerView.backgroundColor=[UIColor groupTableViewBackgroundColor];
    //   [Global startShimmeringToView:_collectionContainerView];
    
    
    _colorCollectionView.userInteractionEnabled=true;
    [_colorCollectionView reloadData];
    if(isiPhone5s){
        singleViewWidthConstant = 157;
    }else{
    singleViewWidthConstant = self.settingButton.frame.size.width+self.settingButton.frame.origin.x -self.topMatBg.frame.size.width-5;
    }
    self.topMatBg.hidden = NO;
    [self deSelectAllOptions];
    self.singleButton.selected = YES;
    self.singleButton.backgroundColor = [UIColor colorWithRed:60/255.0 green:69/255.0 blue:143/255.0 alpha:1.0];
    self.BottomMatBg.hidden = YES;
    
    if (self.topMatWidth.constant != singleViewWidthConstant) {
        if (isiPhone10R_10SMAX) {
            self.topMatWidth.constant = self.settingButton.frame.size.width+self.settingButton.frame.origin.x -self.topMatBg.frame.size.width+15;
        }else if(isiPhone5s){
            self.topMatWidth.constant = 157;
        }
        else
        {
            self.topMatWidth.constant = self.settingButton.frame.size.width+self.settingButton.frame.origin.x -self.topMatBg.frame.size.width-5;
        }
        
        singleViewWidthConstant = self.topMatWidth.constant;
    }
    
    
    
    
    
    [dataArray removeAllObjects];
    dataArray=[db getDataForField:METABOARD_TABLE where:[NSString stringWithFormat:@"category_id ='%@'",matId]];
    if (dataArray.count!=0) {
        [_colorCollectionView reloadData];
    }
    
    
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    NSString *urlString=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,GET_METABOARDS];
    
    NSMutableArray *arr=[[NSMutableArray alloc] init];
    arr=[db getDataForField:USERTABLE where:nil];
    
    if (arr.count!=0) {
        [api sendGetRequst:urlString andToken:arr[0][@"token"]];
    }
    
}




-(void)signUpUser{
    
    
    
    
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
    dic[@"name"]=[[UIDevice currentDevice] name];
    dic[@"email"]=[NSString stringWithFormat:@"%@@frameshop.com.au",[Global getDeviceId]];
    dic[@"password"]=@"qwxcjeub32ne";
    dic[@"c_password"]=@"qwxcjeub32ne";
    dic[@"url"]=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,REGISTER_USER];
    [api sendPostRequst:dic];
    
}
-(void)logInUser{
    
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
    
    dic[@"email"]=[NSString stringWithFormat:@"%@@frameshop.com.au",[Global getDeviceId]];
    dic[@"password"]=@"qwxcjeub32ne";
    
    dic[@"url"]=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,LOGIN_USER];
    [api sendPostRequst:dic];
}

-(void)showDataOfCore:(NSString *)coreId{
    [dataArray removeAllObjects];
    dataArray=[db getDataForField:METABOARD_TABLE where:[NSString stringWithFormat:@"category_id ='%@'",coreId]];
    [_colorCollectionView reloadData];
}


- (IBAction)onFrame:(id)sender {
    [self.delegate framAction:0];
}

-(UIImage *)getCheckMarkWhereBgColor:(NSString *)hexString{
    
    
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    int r=((rgbValue & 0xFF0000) >> 16);
    int g=((rgbValue & 0xFF00) >> 8);
    int b=(rgbValue & 0xFF);
    
    int lightness = ((r*299)+(g*587)+(b*114))/1000; //get lightness value
    
    if (lightness < 127) { //127 = middle of possible lightness value
        return [UIImage imageNamed:@"whiteCheck.png"];
    }
    else  return [UIImage imageNamed:@"blackCheck.png"];
    
    
}




#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return optionArray.count;
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil )
    {
        cell =[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    UIFont *font=[UIFont fontWithName:@"Roboto-Light" size:15];
    
    if ([_type isEqualToString:@"print"]) {
        if (indexPath.row==selectedRow) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        else{
            cell.accessoryType =UITableViewCellAccessoryNone;
        }
        
        // cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.text=optionArray[indexPath.row][@"value"];
    cell.textLabel.font=font;
    cell.textLabel.textColor=[UIColor darkTextColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([_type isEqualToString:@"print"]) {
        // cell.accessoryType = UITableViewCellAccessoryCheckmark;
        selectedRow=(int)indexPath.row;
        [_tableView reloadData];
    }
    
    
    [self.delegate didChooseMatOption:optionArray[indexPath.row]];
    
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 44;
}
- (IBAction)onSetting:(id)sender {
    
    //  self.settingButton.selected  = !self.settingButton.selected;
    //    if (self.settingButton.selected ) {
    //        [self.tableView setHidden:YES];
    //        self.tableView.alpha=0.0;
    //        [UIView transitionWithView:_tableView duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^(void){
    //            self.tableView.alpha=1.0;
    //        } completion:nil];
    [self.delegate didChooseMatOption:[[NSMutableDictionary alloc] init]];
    //    }
    //    else{
    //        self.tableView.alpha=1.0;
    //        [UIView transitionWithView:_tableView duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^(void){
    //            self.tableView.alpha=0.0;
    //            [self.tableView setHidden:YES];
    //        } completion:nil];
    //    }
    //    [_tableView reloadData];
}


#pragma mark - UICollectionViewDelegate

-(NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
    
}
- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    
    
    return (dataArray.count ==0)?40:dataArray.count;
}

#define kImageViewTag 1 // the image view inside the collection view cell prototype is tagged with "1"

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
    static NSString *identifier = @"AlbumCollectionViewCell";
    
    AlbumCollectionViewCell *cells; //=[_colorCollectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    if (cv == _colorCollectionView) {
        cells = [_colorCollectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    }else{
        cells = [_doubleCollectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        
    }
    
    float cellWidth=(self.view.bounds.size.width/10);
    float cellHeight=cellWidth;
    [Global stopShimmeringToView:cells.iconImage];
     cells.frame=CGRectMake(cells.frame.origin.x, cells.frame.origin.y, cellWidth, cellHeight);
    
    cells.albumName.hidden=YES;
    cells.photoCount.hidden=YES;
    cells.iconImage.image=nil;
    cells.iconImage.hidden=YES;
    cells.contentView.frame=CGRectMake(0, 0, cellWidth, cellHeight);
    cells.contentView.backgroundColor=[UIColor groupTableViewBackgroundColor];
    [Global roundCorner:cells.contentView roundValue:cellHeight/2 borderWidth:0.5 borderColor:[UIColor lightGrayColor]];
    [Global startShimmeringToView:cells.contentView];
    
    if (dataArray.count!=0) {
        
        [Global stopShimmeringToView:cells.contentView];
        
        if (!( ( [dataArray[indexPath.row][@"thumb_image"] isEqual:(id)[NSNull null]] ) ||[dataArray[indexPath.row][@"thumb_image"] isEqualToString:@"<null>"]) ) {
            
            UIImageView *imageView=[[UIImageView alloc] init];
            imageView.frame=CGRectMake(0, 0, cellWidth, cellHeight);
            imageView.tag=2;
            [cells insertSubview:imageView belowSubview:cells.iconImage];
            [cells addSubview:cells.iconImage];
            [Global roundCorner:imageView roundValue:imageView.frame.size.height/2 borderWidth:0.2 borderColor:[UIColor lightGrayColor]];
            
            UIImage *img=[Global getImageFromDoucumentWithName:[NSString stringWithFormat:@"meta_thumb_%@",self->dataArray[indexPath.row][@"id"]]];
            
            imageView.image=[Global getMidiumSizeImage:img imageSize:40];
            
            if (img==nil) {
                NSString *str=dataArray[indexPath.row][@"id"];
                
                dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
                dispatch_async(q, ^{
                    //[Global startShimmeringToView:cell.contentView];
                    NSData *datas;
                    
                    datas =[NSData dataWithContentsOfURL:[NSURL URLWithString:self->dataArray[indexPath.row][@"thumb_image"]]] ;
                    
                    [Global saveImagesInDocumentDirectory:datas ImageId:[NSString stringWithFormat:@"meta_thumb_%@",str]];
                    
                    
                    UIImage *img = [[UIImage alloc] initWithData:datas];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if(img!=nil)
                            // cells.iconImage.image=img;
                            imageView.image=[Global getMidiumSizeImage:img imageSize:40];;
                        
                        
                        
                        
                    });
                });
            }
            
            
            
            
            
        }
        else{
            
            
            NSArray *arr = [cells subviews];
            
            for (UIView* b in arr)
            {
               
                    if (b.tag==2) {
                        [b removeFromSuperview];
                       
                    }
                    
                    
               
                
            }
            
            
            
            cells.contentView.backgroundColor=[Global colorFromHexString:dataArray[indexPath.row][@"color_hex"]];
        }
        
        
        
        
        
        
        
        if ((cv == _colorCollectionView ?indexPath==previousIndexPathSingle :indexPath==previousIndexPathDouble) && !_noneButton.isSelected ) {
            UIImage *img=[self getCheckMarkWhereBgColor:dataArray[indexPath.row][@"color_hex"]];
            cells.iconImage.hidden=false;
            cells.iconImage.image=img;
            cells.iconImage.frame=CGRectMake(0, 0, cells.frame.size.width-15, cells.frame.size.height-15);
            cells.iconImage.center=cells.contentView.center;
            
            [Global stopShimmeringToView:cells.iconImage];
            cells.iconImage.backgroundColor=[UIColor clearColor];
            if (cells.contentView.layer.cornerRadius > cells.contentView.frame.size.height) {
                [Global roundCorner:cells.contentView roundValue:cells.contentView.frame.size.height/2 borderWidth:1.5 borderColor:[UIColor grayColor]];
            }
            
            //    [Global roundCorner:cells.contentView roundValue:cells.contentView.frame.size.height/2 borderWidth:1.5 borderColor:[UIColor grayColor]];
            
        }
    }
    return cells;
    
    
}


- (BOOL)collectionView:(UICollectionView *)collectionView
shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (BOOL)collectionView:(UICollectionView *)collectionView
shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath;
{
    return YES;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (dataArray.count==0) {
        return;
    }
    
    
    
    
    if (previousIndexPathSingle!=nil || previousIndexPathDouble!=nil) {
        AlbumCollectionViewCell *svc;
        if (collectionView == _colorCollectionView) {
            svc = (AlbumCollectionViewCell *)[_colorCollectionView cellForItemAtIndexPath:previousIndexPathSingle];
        }else{
            svc= (AlbumCollectionViewCell *)[_doubleCollectionView cellForItemAtIndexPath:previousIndexPathDouble];
        }
        svc.iconImage.image=nil;
        svc.iconImage.hidden=true;
        [Global roundCorner:svc.contentView roundValue:svc.contentView.frame.size.height/2 borderWidth:0.2 borderColor:[UIColor lightGrayColor]];
        //svc.contentView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1);
    }
    
    [bottomImageView removeFromSuperview];
    
    
    AlbumCollectionViewCell *svc;
    if (collectionView == _colorCollectionView) {
        [topImageView removeFromSuperview];
        svc= (AlbumCollectionViewCell *)[_colorCollectionView cellForItemAtIndexPath:indexPath];
        previousIndexPathSingle=indexPath;
        _topMatColor.hidden=false;
        _topMatColor.backgroundColor = [Global colorFromHexString:dataArray[indexPath.row][@"color_hex"]];
        self.topMatColorName.text =[NSString stringWithFormat:@"%@ %@",dataArray[indexPath.row][@"code"],dataArray[indexPath.row][@"name"]];
        [self.topMatColorName adjustsFontSizeToFitWidth];
        
        if (!( ( [dataArray[indexPath.row][@"thumb_image"] isEqual:(id)[NSNull null]] ) ||[dataArray[indexPath.row][@"thumb_image"] isEqualToString:@"<null>"]) ) {
            if (topImageView==nil) {
                topImageView=[[UIImageView alloc] init];
                topImageView.frame=CGRectMake(0, 0, _topMatColor.frame.size.width, _topMatColor.frame.size.width);
                [_topMatColor addSubview:topImageView];
                topImageView.image=[Global getImageFromDoucumentWithName:[NSString stringWithFormat:@"meta_thumb_%@",self->dataArray[indexPath.row][@"id"]]];
            }
        }
        
        
        
        
        
    }else{
        [bottomImageView removeFromSuperview];
        svc= (AlbumCollectionViewCell *)[_doubleCollectionView cellForItemAtIndexPath:indexPath];
        previousIndexPathDouble=indexPath;
        _BottomMatColor.backgroundColor = [Global colorFromHexString:dataArray[indexPath.row][@"color_hex"]];
        self.bottomMatColorName.text =[NSString stringWithFormat:@"%@ %@",dataArray[indexPath.row][@"code"],dataArray[indexPath.row][@"name"]];
        [self.bottomMatColorName adjustsFontSizeToFitWidth];
        
        if (!( ( [dataArray[indexPath.row][@"thumb_image"] isEqual:(id)[NSNull null]] ) ||[dataArray[indexPath.row][@"thumb_image"] isEqualToString:@"<null>"]) ) {
            if (bottomImageView==nil) {
                bottomImageView=[[UIImageView alloc] init];
                bottomImageView.frame=CGRectMake(0, 0, _BottomMatColor.frame.size.width, _BottomMatColor.frame.size.width);
                [_BottomMatColor addSubview:bottomImageView];
                bottomImageView.image=[Global getImageFromDoucumentWithName:[NSString stringWithFormat:@"meta_thumb_%@",self->dataArray[indexPath.row][@"id"]]];
            }
        }
        
        
        
        
    }
    UIImage *img=[self getCheckMarkWhereBgColor:dataArray[indexPath.row][@"color_hex"]];
    svc.iconImage.hidden=false;
    svc.iconImage.image=img;
    svc.iconImage.frame=CGRectMake(0, 0, svc.frame.size.width-15, svc.frame.size.height-15);
    svc.iconImage.center=svc.contentView.center;
    // [svc addSubview:svc.iconImage];
    [Global stopShimmeringToView:svc.iconImage];
    svc.iconImage.backgroundColor=[UIColor clearColor];
    //svc.iconImage.backgroundColor=[UIColor blackColor];
    [Global roundCorner:svc.contentView roundValue:svc.contentView.frame.size.height/2 borderWidth:1.5 borderColor:[UIColor grayColor]];
    
    
    //svc.contentView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.5, 1.5);
    NSMutableDictionary * dic = [[NSMutableDictionary alloc] init];
    
    if (collectionView == _colorCollectionView) {
        
        dic[@"option"] =@"Core: White";
        dic[@"type"] =@"wc";
        dic[@"mat"] =@"top";
        dic[@"selectedOption"] =dataArray[indexPath.row];
        if (_noneButton.isSelected) {
            [self deSelectAllOptions];
            self.singleButton.selected = YES;
            self.singleButton.backgroundColor = APP_BLUE_COLOR;
            [self.delegate didSelectSingleFrame];
            
            
        }
    }
    else{
        
        dic[@"option"] =@"Core: White";
        dic[@"type"] =@"wc";
        dic[@"selectedOption"] =dataArray[indexPath.row];
        dic[@"mat"] =@"bottom";
    }
    
    
    self.colorNameLbl.text =[NSString stringWithFormat:@"COLOUR SWATCH | %@ %@",dataArray[indexPath.row][@"code"],dataArray[indexPath.row][@"name"]];
    [self.colorNameLbl adjustsFontSizeToFitWidth];
    
    self.optionalBtn.backgroundColor = svc.contentView.backgroundColor;
    [self.delegate didSelectCoreColor:dic];
    _settingButton.enabled=YES;
}

#pragma mark collection view cell layout / size
- (CGSize)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    
    
    float cellWidth=(self.view.bounds.size.width/10);
    float cellHeight=cellWidth;
    
    CGSize size=CGSizeMake(cellWidth, cellHeight);
    //sqr.bounds.size.width=(_collectionView.bounds.size.width/2)-4;
    return size;
    
    
}

#pragma mark collection view cell paddings
- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0); // top, left, bottom, right
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 5.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 5.0;
}


-(void)defaultSelectCell:(UICollectionView *)collectionView{
    UIImage *img;
    
    
    
    AlbumCollectionViewCell *svc;
    if (collectionView == _colorCollectionView) {
        
        if (previousIndexPathSingle == nil) {
             previousIndexPathSingle= [NSIndexPath indexPathForRow:0 inSection:0];
        }
        
        img=[self getCheckMarkWhereBgColor:dataArray[previousIndexPathSingle.row][@"color_hex"]];
        svc= (AlbumCollectionViewCell *)[_colorCollectionView cellForItemAtIndexPath:previousIndexPathSingle];
        _topMatColor.hidden=false;
        
        _topMatColor.backgroundColor = [Global colorFromHexString:dataArray[previousIndexPathSingle.row][@"color_hex"]];
        self.topMatColorName.text =[NSString stringWithFormat:@"%@ %@",dataArray[previousIndexPathSingle.row][@"code"],dataArray[previousIndexPathSingle.row][@"name"]];
        [self.topMatColorName adjustsFontSizeToFitWidth];
    }else{
        if (previousIndexPathDouble==nil) {
              previousIndexPathDouble= [NSIndexPath indexPathForRow:0 inSection:0];
        }
        
        img=[self getCheckMarkWhereBgColor:dataArray[previousIndexPathDouble.row][@"color_hex"]];
        svc= (AlbumCollectionViewCell *)[_doubleCollectionView cellForItemAtIndexPath:previousIndexPathDouble];
        self.bottomMatColorName.text =[NSString stringWithFormat:@"%@ %@",dataArray[previousIndexPathDouble.row][@"code"],dataArray[previousIndexPathDouble.row][@"name"]];
        [self.bottomMatColorName adjustsFontSizeToFitWidth];
        _BottomMatColor.backgroundColor = [Global colorFromHexString:dataArray[previousIndexPathDouble.row][@"color_hex"]];
    }
    
 
    svc.iconImage.hidden=false;
    svc.iconImage.image=img;
    svc.iconImage.frame=CGRectMake(0, 0, svc.frame.size.width-15, svc.frame.size.height-15);
    svc.iconImage.center=svc.contentView.center;
    [Global stopShimmeringToView:svc.iconImage];
    svc.iconImage.backgroundColor=[UIColor clearColor];
    //svc.iconImage.backgroundColor=[UIColor blackColor];
    [Global roundCorner:svc.contentView roundValue:svc.contentView.frame.size.height/2 borderWidth:1.5 borderColor:[UIColor grayColor]];
    
    
    //svc.contentView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.5, 1.5);
    NSMutableDictionary * dic = [[NSMutableDictionary alloc] init];
    
    if (collectionView == _colorCollectionView) {
        
        dic[@"option"] =@"Core: White";
        dic[@"type"] =@"wc";
        dic[@"mat"] =@"top";
        dic[@"selectedOption"] =dataArray[previousIndexPathSingle.row];
        if (_noneButton.isSelected) {
            [self deSelectAllOptions];
            self.singleButton.selected = YES;
            self.singleButton.backgroundColor = [UIColor colorWithRed:60/255.0 green:69/255.0 blue:143/255.0 alpha:1.0];
            
        }
    }
    else{
        
        dic[@"option"] =@"Core: White";
        dic[@"type"] =@"wc";
        dic[@"selectedOption"] =dataArray[previousIndexPathDouble.row];
        dic[@"mat"] =@"bottom";
    }
    
    
    
    
    self.optionalBtn.backgroundColor = svc.contentView.backgroundColor;
    [self.delegate didSelectCoreColor:dic];
    
    
}



#pragma mark - POP API DELEGATES
-(void)connectionError{
    
}
-(void)requestFailedSessionExpired:(NSDictionary *)dataPack{
    
}
-(void)requestFailedServerError:(NSDictionary *)dataPack{
    
}
-(void)requestFailedServiceUnavailableDueToMaintenance:(NSDictionary *)dataPack{
    [Global showSimpleAlert:self andTitle:FRAMESHOP andMessage:dataPack[@"message"]];
}
-(void)requestFailedMethodNotFound:(NSDictionary *)dataPack{
    
}
-(void)requestFailedNotFound:(NSDictionary *)dataPack{
    
}
-(void)requestFailedUnauthorised:(NSDictionary *)dataPack{
     [self logInUser];
}
-(void)requestFailedBadRequest:(NSDictionary *)dataPack{
    
}
-(void)requestSucceededWithData:(NSDictionary *)dataPack{
    
    if ([dataPack[@"url"] isEqualToString:LOGIN_USER]) {
        
        [db delTableWithTableName:USERTABLE];
        NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
        
        dic[@"id"]              =[NSString stringWithFormat:@"%@",dataPack[@"data"][@"id"]];
        dic[@"name"]            =dataPack[@"data"][@"name"];
        dic[@"email"]           =dataPack[@"data"][@"email"];
        dic[@"created_at"]      =dataPack[@"data"][@"created_at"];
        dic[@"updated_at"]      =dataPack[@"data"][@"updated_at"];
        dic[@"token"]           =dataPack[@"data"][@"access_token"];
        
        [db insertIntoTable:dic table_name:USERTABLE];
        [self getDataForMat];
    }
    
    else if ([dataPack[@"url"] isEqualToString:REGISTER_USER]){
        
        NSMutableDictionary *dic =[[NSMutableDictionary alloc] init];
        dic[@"token"]            =dataPack[@"data"][@"token"];
        dic[@"id"]               =[NSString stringWithFormat:@"%@",dic[@"id"]];
        
        [db insertIntoTable:dic table_name:USERTABLE];
        [self getDataForMat];
    }
    else{
        // reload table with product data
        [dataArray removeAllObjects];
        NSMutableArray *arr=[dataPack[@"data"] mutableCopy];
        [db delTableWithTableName:METABOARD_CATEGORIES];
        [db delTableWithTableName:METABOARD_TABLE];
        
        
        
        for (int i=0; i<arr.count; i++) {
            NSMutableDictionary *temDic=[[NSMutableDictionary alloc] init];
            
            temDic[@"id"]                =[NSString stringWithFormat:@"%@",arr[i][@"id"]];
            temDic[@"name"]        =[NSString stringWithFormat:@"%@",arr[i][@"name"]];
            temDic[@"price"]             =[NSString stringWithFormat:@"%@",arr[i][@"price_column"]];
            temDic[@"description"] =(arr[i][@"description"] == (id)[NSNull null] ||[arr[i][@"description"] length] == 0 )?@"":arr[i][@"description"];
            
            
            [db insertIntoTable:temDic table_name:METABOARD_CATEGORIES];
            
            
            //
            for (int j=0; j<[arr[i][@"matboards"] count]; j++) {
                
                NSMutableDictionary *temp=[[NSMutableDictionary alloc] init];
                temp[@"id"]          =[NSString stringWithFormat:@"%@",arr[i][@"matboards"][j][@"id"]];
                temp[@"code"]        =[NSString stringWithFormat:@"%@",arr[i][@"matboards"][j][@"code"]];
                temp[@"name"]        =[NSString stringWithFormat:@"%@",arr[i][@"matboards"][j][@"name"]];
                
                temp[@"description"] =(arr[i][@"matboards"][j][@"description"] == (id)[NSNull null] ||[arr[i][@"matboards"][j][@"description"] length] == 0 )?@"":arr[i][@"matboards"][j][@"description"];
                
                temp[@"sort_order"]        =[NSString stringWithFormat:@"%@",arr[i][@"matboards"][j][@"sort_order"]];
                temp[@"max_width"]         =[NSString stringWithFormat:@"%@",arr[i][@"matboards"][j][@"max_width"]];
                temp[@"max_height"]        =[NSString stringWithFormat:@"%@",arr[i][@"matboards"][j][@"max_heigh"]];
                temp[@"category_id"]       =[NSString stringWithFormat:@"%@",arr[i][@"matboards"][j][@"category_id"]];
                temp[@"color_hex"]         =[NSString stringWithFormat:@"#%@",arr[i][@"matboards"][j][@"color_hex"]];
                
                temp[@"core_hex"]             =[NSString stringWithFormat:@"#%@",arr[i][@"matboards"][j][@"core_hex"]];
                temp[@"thumb_image"]          =[NSString stringWithFormat:@"%@",arr[i][@"matboards"][j][@"thumb_image"]];
                temp[@"tile_image"]           =[NSString stringWithFormat:@"%@",arr[i][@"matboards"][j][@"tile_image"]];
                temp[@"location"]             =[NSString stringWithFormat:@"%@",arr[i][@"matboards"][j][@"location"]];
                
                
                
                [db insertIntoTable:temp table_name:METABOARD_TABLE];
                
                
                if ([temp[@"category_id"] isEqualToString:matId]) {
                    
                    
                    dataArray[dataArray.count]=temp;
                    
                    
                }
            }
            
        }
        //   _colorCollectionView.backgroundColor=[UIColor clearColor];
        //        [Global stopShimmeringToView:_collectionContainerView];
        _colorCollectionView.userInteractionEnabled=true;
        [_colorCollectionView reloadData];
        //        singleViewWidthConstant = self.settingButton.frame.size.width+self.settingButton.frame.origin.x -self.topMatBg.frame.size.width-5;
        //
        //        self.topMatBg.hidden = NO;
        //        [self deSelectAllOptions];
        //        self.singleButton.selected = YES;
        //        self.singleButton.backgroundColor = [UIColor colorWithRed:60/255.0 green:69/255.0 blue:143/255.0 alpha:1.0];
        //        self.BottomMatBg.hidden = YES;
        //
        //        if (self.topMatWidth.constant != singleViewWidthConstant) {
        //            self.topMatWidth.constant = self.settingButton.frame.size.width+self.settingButton.frame.origin.x -self.topMatBg.frame.size.width-5;
        //            singleViewWidthConstant = self.topMatWidth.constant;
        //        }
        //
        //        [self onClickNone:_noneButton];
    }
}
-(void)requestSucceededButActionFailed:(NSDictionary *)dataPack{
    
    if ([dataPack[@"url"] isEqualToString:REGISTER_USER] || [dataPack[@"message"] isEqualToString:@"Unauthenticated."]) {
        //go To login
        
        [self logInUser];
    }
    else if ([dataPack[@"url"] isEqualToString:LOGIN_USER]) {
        //something bad happned with server
        [self signUpUser];
        NSLog(@"something bad happned with server login signup both failed");
    }
    else{
        NSLog(@"there is some problme with product api");
        //there is some problme with product api
    }
    
    
}
-(void)uploadedData:(NSString *)pecentage andByte:(float)bytes{
    
}
-(void)requestSucceededButNoDataFound:(NSDictionary *)dataPack{
    
}
-(void)requestFailedDueToServerIsuueWithError:(NSString*)errorDes{
    
}




- (IBAction)onClickNone:(id)sender {
    _settingButton.enabled=false;
    [self deSelectAllOptions];
    self.noneButton.selected = YES;
    self.noneButton.backgroundColor = [UIColor colorWithRed:60/255.0 green:69/255.0 blue:143/255.0 alpha:1.0];
    self.BottomMatBg.hidden = YES;
    self.topMatWidth.constant = singleViewWidthConstant;
//    previousIndexPathDouble=nil;
//    previousIndexPathSingle=nil;
    [_colorCollectionView reloadData];
    // [_doubleCollectionView reloadData];
    [_doubleCollectionView reloadData];
    [self.delegate didSelectNoneFrame];
    self.topMatColorName.text=@"";
    self.topMatColor.hidden=YES;
   
    
}

- (IBAction)onClickSingle:(id)sender {
    _settingButton.enabled=true;
    self.topMatBg.hidden = NO;
    [self deSelectAllOptions];
    self.singleButton.selected = YES;
    self.singleButton.backgroundColor = [UIColor colorWithRed:60/255.0 green:69/255.0 blue:143/255.0 alpha:1.0];
    self.BottomMatBg.hidden = YES;
    
    if (self.topMatWidth.constant != singleViewWidthConstant) {
        if (isiPhone5s) {
            self.topMatWidth.constant = self.settingButton.frame.size.width+self.settingButton.frame.origin.x -self.topMatBg.frame.size.width-7;
            singleViewWidthConstant = self.topMatWidth.constant;
        }else{
            self.topMatWidth.constant = self.settingButton.frame.size.width+self.settingButton.frame.origin.x -self.topMatBg.frame.size.width-5;
            singleViewWidthConstant = self.topMatWidth.constant;
        }
        
    }
    
    [self.delegate didSelectSingleFrame];
    
    //if (previousIndexPathSingle==nil) {
        [self defaultSelectCell:_colorCollectionView];
  //  }
    [_colorCollectionView scrollToItemAtIndexPath:previousIndexPathSingle
                                 atScrollPosition:UICollectionViewScrollPositionLeft
                                         animated:NO];
    [_doubleCollectionView reloadData];
}

- (IBAction)onClickDouble:(id)sender {
    _settingButton.enabled=true;
    [self deSelectAllOptions];
    self.doubleButton.selected = YES;
    self.doubleButton.backgroundColor = [UIColor colorWithRed:60/255.0 green:69/255.0 blue:143/255.0 alpha:1.0];
    self.BottomMatBg.hidden = NO;
    if (self.topMatWidth.constant != singleViewWidthConstant) {
        if (isiPhone5s) {
            singleViewWidthConstant = self.settingButton.frame.size.width+self.settingButton.frame.origin.x -self.topMatBg.frame.size.width-7;
        }else{
            singleViewWidthConstant = self.settingButton.frame.size.width+self.settingButton.frame.origin.x -self.topMatBg.frame.size.width-5;
        }
    }
    self.topMatWidth.constant = 0.0;
    [_doubleCollectionView reloadData];
    [_colorCollectionView reloadData];
    [self.delegate didSelectDoubleFrame];
    
   // if (previousIndexPathSingle==nil) {
        [self defaultSelectCell:_colorCollectionView];
   // }
   // if (previousIndexPathDouble==nil) {
        [self defaultSelectCell:_doubleCollectionView];
    //}
    
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self->_colorCollectionView scrollToItemAtIndexPath:self->previousIndexPathSingle
                                           atScrollPosition: UICollectionViewScrollPositionCenteredVertically
                                                   animated:NO];
        [self->_doubleCollectionView scrollToItemAtIndexPath:self->previousIndexPathDouble
                                            atScrollPosition: UICollectionViewScrollPositionCenteredVertically
                                                    animated:NO];
    });
    
    
    //[_doubleCollectionView scrollToItemAtIndexPath:previousIndexPathDouble
    //  atScrollPosition: UICollectionViewScrollPositionCenteredVertically
    //      animated:YES];
    
}
-(void)deSelectAllOptions{
    self.noneButton.selected = NO;
    self.singleButton.selected = NO;
    self.doubleButton.selected = NO;
    self.noneButton.backgroundColor = [UIColor colorWithRed:239/255.0 green:239/255.0 blue:239/255.0 alpha:1.0];
    self.singleButton.backgroundColor = [UIColor colorWithRed:239/255.0 green:239/255.0 blue:239/255.0 alpha:1.0];
    self.doubleButton.backgroundColor = [UIColor colorWithRed:239/255.0 green:239/255.0 blue:239/255.0 alpha:1.0];
}
@end

