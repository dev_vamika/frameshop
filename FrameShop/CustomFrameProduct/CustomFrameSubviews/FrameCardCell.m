//
//  FrameCardCell.m
//  FrameShop
//
//  Created by Saurabh Srivastav on 14/04/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import "FrameCardCell.h"
#import "Global.h"
@implementation FrameCardCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    
    
    // Initialization code
}

-(void)refreshCell{
    
    
    //  _previewImage.hidden=YES;
    
    
    _sku.text=_infoDic[@"sku"];
    _price.text=[NSString stringWithFormat:@"Price Rate %@",_infoDic[@"base_price"]];
    _width.text=[NSString stringWithFormat:@"Width: %.1fcm",[_infoDic[@"width"]floatValue]];
    _stackSku.text=_infoDic[@"sku"];
    _stackWidth.text=[NSString stringWithFormat:@"%.1fcm",[_infoDic[@"width"]floatValue]];
    _depth.text=[NSString stringWithFormat:@"%.1fcm",[_infoDic[@"height"]floatValue]];
    _rebate.text=[NSString stringWithFormat:@"%.1fcm",[_infoDic[@"rebate"]floatValue]];
    _mat.text=[NSString stringWithFormat:@"%@",_infoDic[@"material_name"]];
    _color.text=[NSString stringWithFormat:@"%@",_infoDic[@"color"]];
    
    
    
    
    UIImage *temp =[Global getImageFromDoucumentWithName:[NSString stringWithFormat:@"frame_%@",_infoDic[@"id"]]];
    if (temp!=nil) {
        _previewImage.image=[Global getMidiumSizeImage:temp imageSize:_previewImage.frame.size.height];
        [self setImageFrameWhereImageWidth:temp.size.width andHeight:temp.size.height];
        
    }
    
    
    else{
        
        NSString *imageName=[NSString stringWithFormat:@"frame_%@",self->_infoDic[@"id"]];
        dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(q, ^{
            
            NSData *datas;
            
            datas =[NSData dataWithContentsOfURL:[NSURL URLWithString:self->_infoDic[@"image_url_c"]]] ;
            [Global saveImagesInDocumentDirectory:datas ImageId:imageName];
            
            
            
            UIImage *img = [[UIImage alloc] initWithData:datas];
            dispatch_async(dispatch_get_main_queue(), ^{
                if(img!=nil){
                    self->_previewImage.image=[Global getMidiumSizeImage:img imageSize:self->_previewImage.frame.size.height];
                    [self setImageFrameWhereImageWidth:img.size.width andHeight:img.size.height];
                    
                }
                
            });
        });
        
    }
    
    
}

-(void)setImageFrameWhereImageWidth:(float )imageWidth andHeight:(float)imageHeight{
    
    if ([_infoDic[@"cHeight"] floatValue]== 0) {
        [self performSelector:@selector(updateSize) withObject:self afterDelay:0.3 ];
    }
    else{
        NSLog(@"Not called==> %@",_infoDic[@"cHeight"]);
    }
    
}

-(void)updateSize{
    [self.delegate imageUpdatedSucceessfully:_previewImage.image index:_CurrentIndex];
}


@end
