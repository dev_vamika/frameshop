//
//  ChooseFrameViewController.m
//  FrameShop
//
//  Created by Saurabh anand on 20/12/19.
//  Copyright © 2019 Mayank Barnwal. All rights reserved.
//

#import "ChooseFrameViewController.h"
#import "DBManager.h"
@interface ChooseFrameViewController (){
    NSMutableArray *optionArray,*sortingArray,*priviousArray;
    NSIndexPath  *sortIndexPath;
    DbManager *db;
    NSString *selectedCatId;
    NSString *frameId;
    NSIndexPath *expendedIndex;
    int globarSortedIndex;
    float minCellSize;
    
    
}

@end

@implementation ChooseFrameViewController

- (void)viewDidLoad {
    
    
    
    [super viewDidLoad];
    [_collectionView setHidden:YES];
    db=[[DbManager alloc] init];
    globarSortedIndex=0;
    frameId=@"";
    _searchBar.hidden = YES;
    _dropDownBtn.hidden=NO;
           _rightBtn.hidden = NO;
    optionArray=[[NSMutableArray alloc] init];
    sortingArray=[[NSMutableArray alloc] init];
    priviousArray=[[NSMutableArray alloc] init];
    _frameArray=[[NSMutableArray alloc] init];
    [Global roundCorner:_serachButton roundValue:10 borderWidth:0 borderColor:nil];
    [Global roundCorner:_rightBtn roundValue:10 borderWidth:0 borderColor:nil];
//    [Global roundCorner:_dropDownBtn roundValue:10 borderWidth:0 borderColor:nil];
 [Global roundCorner:_chooseOptionView roundValue:10 borderWidth:0 borderColor:nil];
   
    [Global roundCorner:_frameBtn roundValue:_frameBtn.frame.size.height/2 borderWidth:0 borderColor:nil];
    [Global roundCorner:_matsBtn roundValue:_matsBtn.frame.size.height/2 borderWidth:0 borderColor:nil];
    // [self deSelectAllOptions];
  
    
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
    dic[@"id"]=@"pop";
    dic[@"name"]=@"Popular";
    optionArray[optionArray.count]=[dic mutableCopy];
    dic[@"id"]=@"sale";
    dic[@"name"]=@"On Sale";
    
    optionArray[optionArray.count]=[dic mutableCopy];
    [optionArray addObjectsFromArray:[db getDataForField:FRAME_CATEGORIES where:nil]];
    
    sortingArray[sortingArray.count]=@"Sort By Width: smallest first";
    sortingArray[sortingArray.count]=@"Sort By Width: largest first";
    sortingArray[sortingArray.count]=@"Sort By Price: low to high";
    sortingArray[sortingArray.count]=@"Sort By Price: high to low";
    
   
    UINib *nib = [UINib nibWithNibName:@"FrameCell" bundle:nil];
    [_collectionView registerNib:nib forCellWithReuseIdentifier:@"FrameCell"];
    nib = [UINib nibWithNibName:@"FrameCardCell" bundle:nil];
    [_collectionView registerNib:nib forCellWithReuseIdentifier:@"FrameCardCell"];
    
    _defaultSku=@"224F";
    
    _warningLbl.hidden=true;
    _searchBar.backgroundImage=nil;
    [Global roundCorner:_searchBar roundValue:0 borderWidth:1 borderColor:[UIColor whiteColor]];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}

-(void)getDataForFrame{
    self.view.userInteractionEnabled=false;
     _collectionView.hidden=NO;
     _warningLbl.hidden=true;
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    NSString *urlString=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,GET_FRAMES_CATEGORY];
    NSMutableArray *arr=[[NSMutableArray alloc] init];
    arr=[db getDataForField:USERTABLE where:nil];
    if (arr.count!=0) {
        [api sendGetRequst:urlString andToken:arr[0][@"token"]];
    }
    else{
        [self logInUser];
    }
}


-(NSMutableArray *)sortChildSortsBaseArray:(NSMutableArray *)baseArray andSetWith:(NSString *)setString  andSortValue:(NSString *)sort shouldAce:(BOOL)ace{
    
    NSMutableArray *finalArray=[[NSMutableArray alloc] init];
    NSMutableArray *filteredArray = [NSMutableArray array];
    
    
    
    for (int i =0; i<baseArray.count; i++)
    {
        NSMutableDictionary* E1 = [baseArray objectAtIndex:i];
        
        NSPredicate *pdr=     [NSPredicate predicateWithFormat:@"%K == %@" , setString,[E1 objectForKey:setString]];
        BOOL hasDuplicate = [[filteredArray filteredArrayUsingPredicate:pdr] count] > 0;
        
        if (!hasDuplicate)
        {
            [filteredArray addObject:E1];
        }
    }
    
    NSMutableArray *temArr=[[NSMutableArray alloc] init];
    temArr               =[baseArray mutableCopy];
    
    
    
    for (int i=0; i<filteredArray.count; i++) {
        NSMutableArray *arr=[[NSMutableArray alloc] init];
        
        
        for (int j=0; j<temArr.count; j++) {
            if ([temArr[j][setString] isEqualToString:filteredArray[i][setString]]) {
                arr[arr.count]=temArr[j];
                
                
            }
            
        }
        
        arr=[self sortedFrameArrayWhereRawArray:arr withKey:sort shouldAcending:ace];
        
        finalArray[finalArray.count]=arr;
        
    }
    
    return finalArray;
    
}







-(NSMutableArray *)sortArrayWidthHiddenSorts:(int)code andBaseArray:(NSMutableArray *)baseArray {
  
    
    switch (code) {
            
        case 0:{
            
            baseArray=  [self sortedFrameArrayWhereRawArray:_frameArray withKey:@"width" shouldAcending:YES];
            NSMutableArray *final=[[NSMutableArray alloc] init];
            NSMutableArray *temp=[[NSMutableArray alloc] init];
            
            temp=[self sortChildSortsBaseArray:baseArray andSetWith:@"width" andSortValue:@"base_price" shouldAce:YES];
            
            for (int i=0; i<temp.count; i++) {
                
                NSMutableArray *temp1=[[NSMutableArray alloc] init];
                temp1=[self sortChildSortsBaseArray:temp[i] andSetWith:@"base_price" andSortValue:@"sku" shouldAce:YES];
                
                for (int j=0; j<temp1.count; j++) {
                    
                    for (int k=0; k<[temp1[j] count]; k++) {
                        NSLog(@"%@",temp1[j][k]);
                        final[final.count]=temp1[j][k];
                    }
                    
                }
                
            }
            
            return final;
            
        }
            break;
            
            
        case 1:
        {
            
            baseArray=  [self sortedFrameArrayWhereRawArray:_frameArray withKey:@"width" shouldAcending:NO];
            
            NSMutableArray *final=[[NSMutableArray alloc] init];
            NSMutableArray *temp=[[NSMutableArray alloc] init];
            
            temp=[self sortChildSortsBaseArray:baseArray andSetWith:@"width" andSortValue:@"base_price" shouldAce:NO];
            
            for (int i=0; i<temp.count; i++) {
                
                
                NSMutableArray *temp1=[[NSMutableArray alloc] init];
                temp1=[self sortChildSortsBaseArray:temp[i] andSetWith:@"base_price" andSortValue:@"sku" shouldAce:YES];
                
                for (int j=0; j<temp1.count; j++) {
                    
                    for (int k=0; k<[temp1[j] count]; k++) {
                        NSLog(@"%@",temp1[j][k]);
                        final[final.count]=temp1[j][k];
                    }
                    
                    
                }
                
            }
            
            return final;
            
        }
            break;
            
            
        case 2:{
            baseArray=  [self sortedFrameArrayWhereRawArray:_frameArray withKey:@"base_price" shouldAcending:YES];
            NSMutableArray *final=[[NSMutableArray alloc] init];
            
            NSMutableArray *temp=[[NSMutableArray alloc] init];
            
            temp=[self sortChildSortsBaseArray:baseArray andSetWith:@"base_price" andSortValue:@"width" shouldAce:YES];
            
            for (int i=0; i<temp.count; i++) {
                NSMutableArray *temp1=[[NSMutableArray alloc] init];
                temp1=[self sortChildSortsBaseArray:temp[i] andSetWith:@"width" andSortValue:@"sku" shouldAce:YES];
                
                for (int j=0; j<temp1.count; j++) {
                    
                    for (int k=0; k<[temp1[j] count]; k++) {
                        
                        final[final.count]=temp1[j][k];
                    }
                    
                    
                }
                
            }
            
            
            return final;
            
        }
            break;
            
        case 3:
        {
            
            baseArray=  [self sortedFrameArrayWhereRawArray:_frameArray withKey:@"base_price" shouldAcending:NO];
            
            NSMutableArray *final=[[NSMutableArray alloc] init];
            NSMutableArray *temp=[[NSMutableArray alloc] init];
            
            temp=[self sortChildSortsBaseArray:baseArray andSetWith:@"base_price" andSortValue:@"width" shouldAce:NO];
            
            for (int i=0; i<temp.count; i++) {
                NSMutableArray *temp1=[[NSMutableArray alloc] init];
                temp1=[self sortChildSortsBaseArray:temp[i] andSetWith:@"width" andSortValue:@"sku" shouldAce:YES];
                
                for (int j=0; j<temp1.count; j++) {
                    
                    for (int k=0; k<[temp1[j] count]; k++) {
                        NSLog(@"%@",temp1[j][k]);
                        final[final.count]=temp1[j][k];
                    }
                    
                    
                }
            }
            
            
            return final;
            
        }
            
            
        default:
            break;
    }
    
    return baseArray;
}


-(void)refreshView{
    
    
    
    
     _collectionView.hidden=NO;
     _warningLbl.hidden=true;
    selectedCatId=@"pop";
    
   // self.frameArray=[db getDataForField:FRAME_TABLE where:@"is_popular='1'"];
    
    globarSortedIndex=0;
    
    
    _frameArray= [self sortArrayWidthHiddenSorts:globarSortedIndex andBaseArray:_frameArray];
    
    
    if ([_type isEqualToString:@"canvas"]) {
        [_matsBtn setTitle:@"SKIP" forState:UIControlStateNormal];

    }
    
    
    
    if ([frameId isEqualToString:@""]) {
        if (self.frameArray.count!=0) {
            
            NSMutableArray *arr=[[NSMutableArray alloc] init];
            arr=[db getDataForField:FRAME_TABLE where:@"sku='224F'"];
            if (arr.count>0) {
                [self.delegate didChooseFrameOption:arr[0]];
                frameId=[NSString stringWithFormat:@"%@",arr[0][@"id"]];
            }
            
            
            
        }
    }
    [optionArray removeAllObjects];
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
    dic[@"id"]=@"pop";
    dic[@"name"]=@"Popular";
    optionArray[optionArray.count]=[dic mutableCopy];
    dic[@"id"]=@"sale";
    dic[@"name"]=@"On Sale";
    
    optionArray[optionArray.count]=[dic mutableCopy];
    [optionArray addObjectsFromArray:[db getDataForField:FRAME_CATEGORIES where:nil]];
    
    
    [self.collectionView setHidden:NO];
    [self.collectionView reloadData];
    
    [self performSelector:@selector(addLoader) withObject:self afterDelay:0.1 ];
    
    [self getDataForFrame];
    
    
    
}

-(void)addLoader{
    if (_frameArray.count==0)
        [Global startShimmeringToView:_collectionView];
    
}


-(NSMutableArray *)sortedFrameArrayWhereRawArray:(NSMutableArray *)baseArray withKey:(NSString *)keyValue shouldAcending:(BOOL )ace{
    
    
    if ([keyValue isEqualToString:@"sku"]) {
        NSSortDescriptor *sortByName;
        sortByName = [NSSortDescriptor sortDescriptorWithKey:keyValue
                                                   ascending:ace];


        NSArray *sortDescriptors = [NSArray arrayWithObject:sortByName];
        NSArray *sortedArray = [baseArray sortedArrayUsingDescriptors:sortDescriptors];
        [baseArray removeAllObjects];
        [baseArray addObjectsFromArray:sortedArray];
        return baseArray;
    }
    
    

    

    NSSortDescriptor *aSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:keyValue ascending:ace comparator:^(id obj1, id obj2) {

        if ([obj1 floatValue] > [obj2 floatValue]) {
            return (NSComparisonResult)NSOrderedDescending;
        }
        if ([obj1 floatValue] < [obj2 floatValue]) {
            return (NSComparisonResult)NSOrderedAscending;
        }
        return (NSComparisonResult)NSOrderedSame;
    }];
    
    
    NSArray *sortDescriptors = [NSArray arrayWithObject:aSortDescriptor];
       NSArray *sortedArray = [baseArray sortedArrayUsingDescriptors:sortDescriptors];
       [baseArray removeAllObjects];
       [baseArray addObjectsFromArray:sortedArray];
       return baseArray;
    
}


-(void)getFramesWhereCategoryId:(NSString *)catId{
    _collectionView.hidden=NO;
     _warningLbl.hidden=true;
    selectedCatId=catId;
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    NSString *urlString=[NSString stringWithFormat:@"%@%@/%@/frames",SERVER_ADDRESS,GET_FRAMES_OF_CATEGORY,catId];
    NSMutableArray *arr=[[NSMutableArray alloc] init];
    arr=[db getDataForField:USERTABLE where:nil];
    
    if (arr.count!=0) {
        [api sendGetRequst:urlString andToken:arr[0][@"token"]];
    }
}

-(void)getSearchFrameFromServerWhereString:(NSString *)searchText{
    _collectionView.hidden=NO;
     _warningLbl.hidden=true;
    PopApi *api=[[PopApi alloc] init];
      api.delegate=self;
    NSString *urlString=[NSString stringWithFormat:@"%@%@%@",SERVER_ADDRESS,SEARCH_FRAMES,searchText];
    
    NSMutableArray *arr=[[NSMutableArray alloc] init];
    arr=[db getDataForField:USERTABLE where:nil];
    
    if (arr.count!=0) {
        [api sendGetRequst:urlString andToken:arr[0][@"token"]];
    }
   
    
}


-(void)signUpUser{
    
    
    
    
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
   dic[@"name"]=[[UIDevice currentDevice] name];
    dic[@"email"]=[NSString stringWithFormat:@"%@@frameshop.com.au",[Global getDeviceId]];
    dic[@"password"]=@"qwxcjeub32ne";
    dic[@"c_password"]=@"qwxcjeub32ne";
    dic[@"url"]=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,REGISTER_USER];
    [api sendPostRequst:dic];
    
}
-(void)logInUser{
    
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
    
   dic[@"email"]=[NSString stringWithFormat:@"%@@frameshop.com.au",[Global getDeviceId]];
    dic[@"password"]=@"qwxcjeub32ne";
    
    dic[@"url"]=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,LOGIN_USER];
    [api sendPostRequst:dic];
}



-(void)updateCell:(NSMutableDictionary *)updatedDic{
    if ([updatedDic[@"type"] isEqualToString:@"sort"]) {
        optionArray[0][@"option"]=[NSString stringWithFormat:@"Sort %@",updatedDic[@"selectedOption"][@"option"]];
    }
    else{
        optionArray[1][@"option"]=[NSString stringWithFormat:@"Frame Category :%@",updatedDic[@"selectedOption"][@"option"]]   ;
    }
  
}


-(void)closeAllOption{

    _dropDownBtn.selected=false;
    self.dropDownBtn.transform = CGAffineTransformMakeRotation(0);
    
   
    
}


- (IBAction)sortAction:(id)sender {
    
}

- (IBAction)frameAction:(id)sender {
    
}
- (IBAction)framButtonAction:(id)sender{
    //  [self deSelectAllOptions];
   
    [self.delegate framAction:0];
}
- (IBAction)matsAction:(id)sender{
    //    [self deSelectAllOptions];
    //    self.matsBtn.selected = YES;
    //    self.matsBtn.backgroundColor = [UIColor colorWithRed:60/255.0 green:69/255.0 blue:143/255.0 alpha:1.0];
    if ([_type isEqualToString:@"canvas"]) {
        [self.delegate didPressSkip];
    }
    else
    [self.delegate matsAction:1];
}
-(void)deSelectAllOptions{
   
    self.matsBtn.selected = NO;
    
    self.matsBtn.backgroundColor = [UIColor colorWithRed:239/255.0 green:239/255.0 blue:239/255.0 alpha:1.0];
}
- (IBAction)rightButtonAction:(id)sender {
    if (_serachButton.isSelected && [_searchBar.text isEqualToString:@""]) {
        [self onSearch:_serachButton];
    }
    [self.delegate didPressCategoryWithOptionArray:sortingArray];
//    if (!self.tableView.isHidden) {
//        [self dropDownAction:_dropDownBtn];
//    }
    self.rightBtn.selected  = !self.rightBtn.selected;
    if (self.rightBtn.selected ) {
        self.rightBtn.backgroundColor= APP_BLUE_COLOR;
//        self.rightBtn.backgroundColor = APP_BLUE_COLOR;
//        [self.sortingTable setHidden:NO];
//        self.sortingTable.alpha=0.0;
//        [UIView transitionWithView:_tableView duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^(void){
//            self.sortingTable.alpha=1.0;
//        } completion:nil];
    }
//    else{
//        self.rightBtn.backgroundColor = APP_GRAY_COLOR;
//        self.sortingTable.alpha=1.0;
//        [UIView transitionWithView:_tableView duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^(void){
//            self.sortingTable.alpha=0.0;
//            [self.sortingTable setHidden:YES];
//        } completion:nil];
//    }
//    [_sortingTable reloadData];
}
- (IBAction)dropDownAction:(id)sender {
    

    
    [self.delegate didPressCategoryWithOptionArray:optionArray];
//    if (!self.sortingTable.isHidden) {
//        [self rightButtonAction:_rightBtn];
//    }
    self.dropDownBtn.selected  = !self.dropDownBtn.selected;
    if (self.dropDownBtn.selected ) {
        self.listIcon.image = [UIImage imageNamed:@"list_white_icon.png"];
        self.chooseOptionView.backgroundColor = APP_BLUE_COLOR;
//        self.dropDownBtn.backgroundColor = APP_BLUE_COLOR;
//        self.dropDownBtn.transform = CGAffineTransformMakeRotation(M_PI);
//        [self.tableView setHidden:NO];
//        self.tableView.alpha=0.0;
//        [UIView transitionWithView:_tableView duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^(void){
//            self.tableView.alpha=1.0;
//        } completion:nil];
    }
//        else{
//        self.dropDownBtn.backgroundColor = APP_GRAY_COLOR;
//        self.dropDownBtn.transform = CGAffineTransformMakeRotation(M_PI*-2);
//        self.tableView.alpha=1.0;
//        [UIView transitionWithView:_tableView duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^(void){
//            self.tableView.alpha=0.0;
//            [self.tableView setHidden:YES];
//        } completion:nil];
//    }
//    [_tableView reloadData];
}
- (IBAction)onSearch:(id)sender {
    _serachButton.selected=!_serachButton.isSelected;
    if (_serachButton.isSelected) {
        _chooseOptionView.hidden=YES;
        _searchBar.hidden = NO;
        _serachButton.tintColor = UIColor.whiteColor;
        _serachButton.backgroundColor=APP_BLUE_COLOR;
        _dropDownBtn.hidden=YES;
       
        [_searchBar becomeFirstResponder];
    }
    else{
        _chooseOptionView.hidden=NO;
        _searchBar.hidden = YES;
        _serachButton.backgroundColor=_rightBtn.backgroundColor;
        _serachButton.tintColor = UIColor.blackColor;
        _dropDownBtn.hidden=NO;
       
        [_searchBar resignFirstResponder];
        _collectionView.hidden=false;
        _warningLbl.hidden=true;
        if ([_searchBar.text isEqualToString:@""]) {
            return;
        }
        _frameArray=[priviousArray mutableCopy];
        [priviousArray removeAllObjects];
        expendedIndex=nil;
        if ([selectedCatId isEqualToString:@"pop"]) {
            self.frameArray=[db getDataForField:FRAME_TABLE where:@"is_popular='1'"];
        }
        
        else if ([selectedCatId isEqualToString:@"sale"]) {
            self.frameArray=[db getDataForField:FRAME_TABLE where:@"is_on_sale='1'"];
        }
        
        
        else{
            self.frameArray=[db getDataForField:FRAME_TABLE where:[NSString stringWithFormat:@"frame_category_id='%@'",selectedCatId]];
        }
        
        _frameArray= [self sortArrayWidthHiddenSorts:globarSortedIndex andBaseArray:_frameArray];
        [_collectionView reloadData];
        
        
    }
    
    [self closeAllOption];
    
    
    
    
    
    //    if (!self.tableView.isHidden) {
    //        [self dropDownAction:[UIButton new]];
    //
    //    }
    //[self.delegate didChooseSearchOption:_searchBar];
    
    
    
}

-(void)shouldScrollToTop{
    if (_frameArray.count!=0) {
        [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]
                                    atScrollPosition:UICollectionViewScrollPositionLeft
                                            animated:YES];
    }
}

-(void)wrapCell:(NSMutableDictionary *)infoDic{
    expendedIndex=nil;
    _defaultSku=infoDic[@"sku"];
    [_collectionView reloadData];
}
-(void)dissmissPopUp{
    self.rightBtn.selected  = NO;
    self.rightBtn.backgroundColor= APP_LIGHT_GRAY_COLOR;
    
    self.dropDownBtn.selected  = NO;
    self.listIcon.image = [UIImage imageNamed:@"list_icon.png"];
    self.chooseOptionView.backgroundColor = APP_LIGHT_GRAY_COLOR;
}

-(void)didSelectSortingWithIndex:(NSIndexPath*)index{
    self.rightBtn.selected  = NO;
    self.rightBtn.backgroundColor= APP_LIGHT_GRAY_COLOR;
    //    self.rightBtn.backgroundColor = APP_GRAY_COLOR;
    [self shouldScrollToTop];
    globarSortedIndex=(int)index.row;
    int idd = (int)[_frameArray[expendedIndex.row][@"id"] intValue];
    _frameArray= [self sortArrayWidthHiddenSorts:globarSortedIndex andBaseArray:_frameArray];
    for (int i = 0; i<_frameArray.count; i++) {
        if ([_frameArray[i][@"id"] intValue] == idd){
            expendedIndex = [NSIndexPath indexPathForRow:i inSection:0];
        }
    }
    //  expendCellIndex = nil;
    sortIndexPath = nil;
    
    [_collectionView reloadData];
    sortIndexPath=index;
    self.rightBtn.selected  = NO;
}

-(void)didSelectCategory:(NSMutableDictionary*)dic{
    self.frameArray = [[NSMutableArray alloc] init];
     expendedIndex=nil;
    self.dropDownBtn.selected  = NO;
    self.listIcon.image = [UIImage imageNamed:@"list_icon.png"];
    self.chooseOptionView.backgroundColor = APP_LIGHT_GRAY_COLOR;
//    self.dropDownBtn.backgroundColor = APP_GRAY_COLOR;
    [self shouldScrollToTop];
    self.optionTitleButton.text = dic[@"name"];
          
           if ([dic[@"id"]isEqualToString:@"pop" ]) {
               selectedCatId=@"pop";
               self.frameArray=[db getDataForField:FRAME_TABLE where:@"is_popular='1'"];
           }
           else if ([dic[@"id"]isEqualToString:@"sale" ]) {
               selectedCatId=@"sale";
               self.frameArray=[db getDataForField:FRAME_TABLE where:@"is_on_sale='1'"];
           }
           else{
               self.frameArray=[db getDataForField:FRAME_TABLE where:[NSString stringWithFormat:@"frame_category_id='%@'",selectedCatId]];
               if (self.frameArray.count==0) {
                   
                   [Global startShimmeringToView:self.collectionView];
                   
                   _collectionView.userInteractionEnabled=false;
                   
               }
               
               [self getFramesWhereCategoryId:dic[@"id"]];
           }
           _frameArray= [self sortArrayWidthHiddenSorts:globarSortedIndex andBaseArray:_frameArray];
          
//           self.collectionView.hidden = NO;
           [_collectionView reloadData];
}
#pragma mark - search bar Delegate
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    return YES;
}                     // return NO to not become first responder
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
   // [self.delegate didChooseSearchOption:searchBar];
}
- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar{
    return YES;
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    
    _isLoaded=false;
    [_collectionView reloadData];
    if (_frameArray.count>0) {
         priviousArray=[_frameArray mutableCopy];
    }
   
    [_frameArray removeAllObjects];
    [searchBar resignFirstResponder];
    [self performSelector:@selector(addLoader) withObject:self afterDelay:0.1 ];
    [self getSearchFrameFromServerWhereString:searchBar.text];
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    if (_serachButton.isSelected && [_searchBar.text isEqualToString:@""]) {
        [self onSearch:_serachButton];
    }
}
#pragma mark - UICollectionViewDataSource and Delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    minCellSize=(self.collectionView.frame.size.width/4)-5;
    
    return   (self.frameArray.count!=0)?_frameArray.count:10;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *identifier = @"FrameCardCell";
    FrameCardCell *frameView =[_collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    frameView.delegate=self;
    frameView.CurrentIndex=indexPath;

    frameView.expendedView.hidden=YES;
    frameView.imageLeftMargine.constant=10;
    frameView.previewImage.image=nil;
    
    

    
    [Global roundCorner:frameView.contentView roundValue:0.0 borderWidth:0.8 borderColor:[UIColor lightGrayColor]];

    
    
    if (_frameArray.count>0) {
        
        frameView.infoDic=_frameArray[indexPath.row];
        [frameView refreshCell];
        
        
        
        if (expendedIndex==indexPath){
            
            [Global roundCorner:frameView.contentView roundValue:0.0 borderWidth:2.0 borderColor:APP_BLUE_COLOR];

            frameView.expendedView.hidden=NO;
            frameView.imageLeftMargine.constant=210;
            frameView.lblStack.hidden=false;
        }
        else{
            frameView.expendedView.hidden=YES;
            frameView.imageLeftMargine.constant=10;
            frameView.lblStack.hidden=false;

        }
        
        
    }
    else{
       // frameView.contentView.backgroundColor=[UIColor groupTableViewBackgroundColor];
        frameView.lblStack.hidden=YES;
    }
    
    
    
    
    return frameView;
}
- (CGSize)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (_frameArray.count>0) {
        if ((expendedIndex==nil) && [_frameArray[indexPath.row][@"sku"] isEqualToString:_defaultSku] ) {
            
            expendedIndex=indexPath;
        }
        
        if (expendedIndex==indexPath ) {
            
            
            return CGSizeMake([_frameArray[indexPath.row][@"cWidth"] floatValue]+210, _collectionView.frame.size.height*0.95);
            
        }
        
        
        UIImage *img=[Global getImageFromDoucumentWithName:[NSString stringWithFormat:@"frame_%@",_frameArray[indexPath.row][@"id"]]];
       
        if (img!=nil) {
            
            float ratio=img.size.width/img.size.height;
            float height=_collectionView.frame.size.height*0.95;
               float width= (ratio *height);
               
               if (width<minCellSize) {
                   width=minCellSize;
               }
               
             _frameArray[indexPath.row][@"cWidth"]=[NSString stringWithFormat:@"%f",width];
               _frameArray[indexPath.row][@"cHeight"]=[NSString stringWithFormat:@"%f",height];
          
            
        }
        
        
        
        
        if ([_frameArray[indexPath.row][@"cHeight"] floatValue]>0) {
            return CGSizeMake([_frameArray[indexPath.row][@"cWidth"] floatValue], _collectionView.frame.size.height*0.95);
            
        }
        float cellWidth=70+([_frameArray[indexPath.row][@"width"]floatValue]*5);
        float cellHeight=_collectionView.frame.size.height*0.95;
        return CGSizeMake(cellWidth, cellHeight);
    }
    return CGSizeMake((self.collectionView.frame.size.width/4)-5, _collectionView.frame.size.height*0.95);
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    FrameCardCell *preSelectedCell = (FrameCardCell*)[_collectionView cellForItemAtIndexPath:expendedIndex];

    
    preSelectedCell.expendedView.hidden=YES;
    preSelectedCell.imageLeftMargine.constant=10;
   

    
    [Global roundCorner:preSelectedCell.contentView roundValue:0 borderWidth:0.5 borderColor:[UIColor grayColor]];
    
    FrameCardCell *cell = (FrameCardCell*)[_collectionView cellForItemAtIndexPath:indexPath];
    
    
    [Global roundCorner:cell.contentView roundValue:0.0 borderWidth:2.0 borderColor:APP_BLUE_COLOR];
    
    expendedIndex=indexPath;
    [_collectionView performBatchUpdates:^{

        
        
        cell.imageLeftMargine.constant=210;
        cell.expendedView.hidden=NO;
        


    } completion:^(BOOL finished) {
        
        [self.collectionView scrollToItemAtIndexPath:self->expendedIndex
                                           atScrollPosition:UICollectionViewScrollPositionLeft
                                                   animated:YES];
        
        
        
    }];
    _defaultSku=_frameArray[indexPath.row][@"sku"];
    [self closeAllOption];
    [self.delegate didChooseFrameOption:cell.infoDic];
}







- (void)imageUpdatedSucceessfully:(UIImage *)image index:(NSIndexPath *)indexPath{
    
    
    float ratio=image.size.width/image.size.height;
    
    
    
    
    
    NSLog(@"%ld",(long)indexPath.row);
    
    if ( _frameArray.count==0) {
        return;
    }
    
    float height=_collectionView.frame.size.height*0.95;
    float width= (ratio *height);
    
    if (width<minCellSize) {
        width=minCellSize;
    }
    
    
    
    _frameArray[indexPath.row][@"cWidth"]=[NSString stringWithFormat:@"%f",width];
    _frameArray[indexPath.row][@"cHeight"]=[NSString stringWithFormat:@"%f",height];
    
    
    
    
    
    
    
    [_collectionView performBatchUpdates:^{
        
        
        
        
        [_collectionView reloadItemsAtIndexPaths:@[indexPath]];
        
    } completion:^(BOOL finished) {}];
    
    
    
}

-(void) addshadow:(UIView *)view{
    view.layer.masksToBounds = NO;
    view.layer.shadowColor = [[UIColor lightGrayColor] CGColor];
    view.layer.shadowOffset = CGSizeMake(0,0);
    view.layer.shadowOpacity = 4;
    view.layer.shadowRadius = 2;
    
}
#pragma mark - POP API DELEGATES
-(void)connectionError{
    
    _warningLbl.hidden=false;
    _collectionView.hidden=true;
        self.view.userInteractionEnabled=true;
    
    
    
    
}
-(void)requestFailedSessionExpired:(NSDictionary *)dataPack{
    self.view.userInteractionEnabled=true;
}
-(void)requestFailedServerError:(NSDictionary *)dataPack{
    self.view.userInteractionEnabled=true;
}

-(void)requestFailedMethodNotFound:(NSDictionary *)dataPack{
    self.view.userInteractionEnabled=true;
}
-(void)requestFailedNotFound:(NSDictionary *)dataPack{
    self.view.userInteractionEnabled=true;
}
-(void)requestFailedUnauthorised:(NSDictionary *)dataPack{
    self.view.userInteractionEnabled=true;
     [self logInUser];
}
-(void)requestFailedBadRequest:(NSDictionary *)dataPack{
    self.view.userInteractionEnabled=true;
}
-(void)requestSucceededWithData:(NSDictionary *)dataPack{
    if ([dataPack[@"url"] isEqualToString:LOGIN_USER]) {
        
        [db delTableWithTableName:USERTABLE];
        NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
        
        dic[@"id"]              =[NSString stringWithFormat:@"%@",dataPack[@"data"][@"id"]];
        dic[@"name"]            =dataPack[@"data"][@"name"];
        dic[@"email"]           =dataPack[@"data"][@"email"];
        dic[@"created_at"]      =dataPack[@"data"][@"created_at"];
        dic[@"updated_at"]      =dataPack[@"data"][@"updated_at"];
        dic[@"token"]           =dataPack[@"data"][@"access_token"];
        
        [db insertIntoTable:dic table_name:USERTABLE];
        [self getFramesWhereCategoryId:selectedCatId];
    }
    
    else if ([dataPack[@"url"] isEqualToString:REGISTER_USER]){
        
        NSMutableDictionary *dic =[[NSMutableDictionary alloc] init];
        dic[@"token"]            =dataPack[@"data"][@"token"];
        dic[@"id"]               =[NSString stringWithFormat:@"%@",dic[@"id"]];
        
        [db insertIntoTable:dic table_name:USERTABLE];
        [self getFramesWhereCategoryId:selectedCatId];
    }
    
    else if ([dataPack[@"url"] isEqualToString:GET_FRAMES_CATEGORY]){
        [_frameArray removeAllObjects];
        dispatch_async(dispatch_get_main_queue(), ^{
            self.view.userInteractionEnabled=true;
        });
        
        [db delTableWithTableName:FRAME_TABLE];
        NSMutableArray *arr=[dataPack[@"data"][@"popular_frames"] mutableCopy];
        [arr addObjectsFromArray:[dataPack[@"data"][@"on_sale_frames"] mutableCopy]];
        
        
        for (int i=0; i<arr.count; i++) {
            if ([arr[i][@"status"] isEqualToString:@"Active"]) {
                
                
                NSMutableDictionary *temDic=[[NSMutableDictionary alloc] init];
                
                temDic[@"id"]                =[NSString stringWithFormat:@"%@",arr[i][@"id"]];
                temDic[@"base_price"]        =[NSString stringWithFormat:@"%@",arr[i][@"base_price"]];
                temDic[@"color"]             =arr[i][@"color"];
                temDic[@"frame_category_id"] =[NSString stringWithFormat:@"%@",arr[i][@"frame_category_id"]];
                temDic[@"is_popular"]        = [NSString stringWithFormat:@"%@",arr[i][@"is_popular"]];
                
                temDic[@"is_on_sale"]  =[NSString stringWithFormat:@"%@",arr[i][@"is_on_sale"]];
                temDic[@"max_limit"]   =[NSString stringWithFormat:@"%@",arr[i][@"max_limit"]];
                temDic[@"min_limit"]   =[NSString stringWithFormat:@"%@",arr[i][@"min_limit"]];
                
                temDic[@"height"]   =[NSString stringWithFormat:@"%@",arr[i][@"height"]];
                temDic[@"width"]   =[NSString stringWithFormat:@"%@",arr[i][@"width"]];
                temDic[@"rebate"]   =[NSString stringWithFormat:@"%@",arr[i][@"rebate"]];
                
                temDic[@"sku"]     =arr[i][@"SKU"];
                temDic[@"tags"]    =[NSString stringWithFormat:@"%@",arr[i][@"tags"]];
                
                temDic[@"material_description"] =arr[i][@"frame_material"][@"description"];
                temDic[@"material_name"]        =arr[i][@"frame_material"][@"name"];
                temDic[@"cWidth"]=@"0";
                temDic[@"cHeight"]=@"0";
                
               
                
                if (arr[i][@"location"] == (id)[NSNull null] || [arr[i][@"location"] length]==0 ){
                    temDic[@"location"] = @"NA";
                }
                else{
                    temDic[@"location"] = arr[i][@"location"];
                }
                
                for (int j=0; j<[arr[i][@"frame_image"] count]; j++) {
                    if ([arr[i][@"frame_image"][j][@"image_property"] isEqualToString:@"C"]) {
                        temDic[@"image_url_c"]=arr[i][@"frame_image"][j][@"image_url"];
                    }
                    else{
                        temDic[@"image_url_t"]=arr[i][@"frame_image"][j][@"image_url"];
                        
                    }
                }
                
                
                [db insertIntoTable:temDic table_name:FRAME_TABLE];
                
                
                if ([temDic[@"sku"] isEqualToString:@"224F"]) {
                    
                    if ([frameId isEqualToString:@""]) {
                        
                        [self.delegate didChooseFrameOption:temDic];
                        frameId=[NSString stringWithFormat:@"%@",temDic[@"id"]];
                        ;
                    }
                }
                
                
                
            }
            
            
        }
        [db delTableWithTableName:FRAME_CATEGORIES];
        NSMutableArray *arr2=[dataPack[@"data"][@"frame_categories"] mutableCopy];
        for (int i=0; i<arr2.count; i++) {
            
            NSMutableDictionary *temDic=[[NSMutableDictionary alloc] init];
            
            temDic[@"id"]                =[NSString stringWithFormat:@"%@",arr2[i][@"id"]];
            temDic[@"name"]              =[NSString stringWithFormat:@"%@",arr2[i][@"name"]];
            temDic[@"description"]       =arr2[i][@"description"];
            temDic[@"is_deleted"]        =[NSString stringWithFormat:@"%@",arr2[i][@"is_deleted"]];
            temDic[@"mini_icon"]        = [NSString stringWithFormat:@"%@",arr2[i][@"mini_icon"]];
            
            NSArray *tem = [arr2[i][@"name"] componentsSeparatedByString:@"-"];
            if (tem.count>1) {
                temDic[@"name"]=tem[1];
            }
            
            [db insertIntoTable:temDic table_name:FRAME_CATEGORIES];
        }
        
        
        
        
        
        
        if ([selectedCatId isEqualToString:@"pop"]) {
            self.frameArray=[db getDataForField:FRAME_TABLE where:@"is_popular='1'"];
        }
        else if ([selectedCatId isEqualToString:@"sale"]) {
            self.frameArray=[db getDataForField:FRAME_TABLE where:@"is_on_sale='1'"];
        }
        else{
            self.frameArray=[db getDataForField:FRAME_TABLE where:[NSString stringWithFormat:@"frame_category_id='%@'",selectedCatId]];
        }
        
        
        
        
        _frameArray= [self sortArrayWidthHiddenSorts:globarSortedIndex andBaseArray:_frameArray];
        [optionArray removeAllObjects];
        NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
        dic[@"id"]=@"pop";
        dic[@"name"]=@"Popular";
        optionArray[optionArray.count]=[dic mutableCopy];
        dic[@"id"]=@"sale";
        dic[@"name"]=@"On Sale";
        
        optionArray[optionArray.count]=[dic mutableCopy];
         [optionArray addObjectsFromArray:[db getDataForField:FRAME_CATEGORIES where:nil]];
        
        
      
        
        [_collectionView reloadData];
        
        [Global stopShimmeringToView:self.collectionView];
        
        
    }
    
    
    else if ([Global isString:dataPack[@"url"] contains:SEARCH_FRAMES]){
        
       self.view.userInteractionEnabled=true;
         
               NSMutableArray *arr=[dataPack[@"data"][@"frames"] mutableCopy];
               
           
             
               for (int i=0; i<arr.count; i++) {
                   if ([arr[i][@"status"] isEqualToString:@"Active"]) {
                    
                   NSMutableDictionary *temDic=[[NSMutableDictionary alloc] init];
                   
                   temDic[@"id"]                =[NSString stringWithFormat:@"%@",arr[i][@"id"]];
                   temDic[@"base_price"]        =[NSString stringWithFormat:@"%@",arr[i][@"base_price"]];
                   temDic[@"color"]             =arr[i][@"color"];
                   temDic[@"frame_category_id"] =[NSString stringWithFormat:@"%@",arr[i][@"frame_category_id"]];
                   temDic[@"is_popular"]        = [NSString stringWithFormat:@"%@",arr[i][@"is_popular"]];
                   
                   temDic[@"is_on_sale"]  =[NSString stringWithFormat:@"%@",arr[i][@"is_on_sale"]];
                   temDic[@"max_limit"]   =[NSString stringWithFormat:@"%@",arr[i][@"max_limit"]];
                   temDic[@"min_limit"]   =[NSString stringWithFormat:@"%@",arr[i][@"min_limit"]];
                  
                   temDic[@"height"]   =[NSString stringWithFormat:@"%@",arr[i][@"height"]];
                   temDic[@"width"]   =[NSString stringWithFormat:@"%@",arr[i][@"width"]];
                   temDic[@"rebate"]   =[NSString stringWithFormat:@"%@",arr[i][@"rebate"]];
                  
                   temDic[@"sku"]     =arr[i][@"SKU"];
                   temDic[@"tags"]    =[NSString stringWithFormat:@"%@",arr[i][@"tags"]];
                  
                 //  NSLog(@"%@",arr[i][@"frame_material"]);
                   if (arr[i][@"frame_material"] == (id)[NSNull null] ){
                       temDic[@"material_description"] =@"NA";
                       temDic[@"material_name"]        =@"NA";
                   }
                   else{
                       temDic[@"material_description"] =arr[i][@"frame_material"][@"description"];
                        temDic[@"material_name"]        =arr[i][@"frame_material"][@"name"];
                   }
                  // temDic[@"material_description"] =arr[i][@"frame_material"][@"description"];
                  // temDic[@"material_name"]        =arr[i][@"frame_material"][@"name"];
                   
                      
                   
                       if (arr[i][@"location"] == (id)[NSNull null] || [arr[i][@"location"] length]==0 ){
                           temDic[@"location"] = @"NA";
                       }
                       else{
                           temDic[@"location"] = arr[i][@"location"];
                       }
                       
                       
                   for (int j=0; j<[arr[i][@"frame_image"] count]; j++) {
                       if ([arr[i][@"frame_image"][j][@"image_property"] isEqualToString:@"C"]) {
                           temDic[@"image_url_c"]=arr[i][@"frame_image"][j][@"image_url"];
                       }
                       else{
                           temDic[@"image_url_t"]=arr[i][@"frame_image"][j][@"image_url"];

                       }
                   }
                        [db insertIntoTable:temDic table_name:FRAME_TABLE];
                       _frameArray[_frameArray.count]=temDic;
                   
                  }
               }
               
               
               _collectionView.userInteractionEnabled=true;
        expendedIndex=nil;
       // _defaultSku=@"";
        _frameArray= [self sortArrayWidthHiddenSorts:globarSortedIndex andBaseArray:_frameArray];
              [_collectionView reloadData];
        [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UICollectionViewScrollPositionRight animated:NO];
              [Global stopShimmeringToView:self.collectionView];
        
        
    }
    else    {
        // reload table with product data
        [_frameArray removeAllObjects];
        NSMutableArray *arr=[dataPack[@"data"][@"frames"] mutableCopy];
        
        self.view.userInteractionEnabled=true;
        
        for (int i=0; i<arr.count; i++) {
            if ([arr[i][@"status"] isEqualToString:@"Active"]) {
                
                
                NSMutableDictionary *temDic=[[NSMutableDictionary alloc] init];
                
                temDic[@"id"]                =[NSString stringWithFormat:@"%@",arr[i][@"id"]];
                temDic[@"base_price"]        =[NSString stringWithFormat:@"%@",arr[i][@"base_price"]];
                temDic[@"color"]             =arr[i][@"color"];
                temDic[@"frame_category_id"] =[NSString stringWithFormat:@"%@",arr[i][@"frame_category_id"]];
                temDic[@"is_popular"]        = [NSString stringWithFormat:@"%@",arr[i][@"is_popular"]];
                
                temDic[@"is_on_sale"]  =[NSString stringWithFormat:@"%@",arr[i][@"is_on_sale"]];
                temDic[@"max_limit"]   =[NSString stringWithFormat:@"%@",arr[i][@"max_limit"]];
                temDic[@"min_limit"]   =[NSString stringWithFormat:@"%@",arr[i][@"min_limit"]];
                
                temDic[@"height"]   =[NSString stringWithFormat:@"%@",arr[i][@"height"]];
                temDic[@"width"]   =[NSString stringWithFormat:@"%@",arr[i][@"width"]];
                temDic[@"rebate"]   =[NSString stringWithFormat:@"%@",arr[i][@"rebate"]];
                
                temDic[@"sku"]     =arr[i][@"SKU"];
                temDic[@"tags"]    =[NSString stringWithFormat:@"%@",arr[i][@"tags"]];
                
                temDic[@"material_description"] =arr[i][@"frame_material"][@"description"];
                temDic[@"material_name"]        =arr[i][@"frame_material"][@"name"];
                
            
                
                if (arr[i][@"location"] == (id)[NSNull null] || [arr[i][@"location"] length]==0 ){
                    temDic[@"location"] = @"NA";
                }
                else{
                    temDic[@"location"] = arr[i][@"location"];
                }
                
                
                
                for (int j=0; j<[arr[i][@"frame_image"] count]; j++) {
                    if ([arr[i][@"frame_image"][j][@"image_property"] isEqualToString:@"C"]) {
                        temDic[@"image_url_c"]=arr[i][@"frame_image"][j][@"image_url"];
                    }
                    else{
                        temDic[@"image_url_t"]=arr[i][@"frame_image"][j][@"image_url"];
                        
                    }
                }
                
                temDic[@"cHeight"]=@"0";
                temDic[@"cWidth"]=@"0";
                
                [db insertIntoTable:temDic table_name:FRAME_TABLE];
                
            }
        }
        
        
        _collectionView.userInteractionEnabled=true;
        
        
        if ([selectedCatId isEqualToString:@"pop"]) {
            self.frameArray=[db getDataForField:FRAME_TABLE where:@"is_popular='1'"];
        }
        
        else if ([selectedCatId isEqualToString:@"sale"]) {
            self.frameArray=[db getDataForField:FRAME_TABLE where:@"is_on_sale='1'"];
        }
        
        
        else{
            self.frameArray=[db getDataForField:FRAME_TABLE where:[NSString stringWithFormat:@"frame_category_id='%@'",selectedCatId]];
        }
        
        _frameArray= [self sortArrayWidthHiddenSorts:globarSortedIndex andBaseArray:_frameArray];
        [_collectionView reloadData];
        
        [Global stopShimmeringToView:self.collectionView];
        
    }
    
    
}
-(void)requestSucceededButActionFailed:(NSDictionary *)dataPack{
    _warningLbl.hidden=false;
    self.view.userInteractionEnabled=true;
    if ([dataPack[@"url"] isEqualToString:REGISTER_USER] || [dataPack[@"message"] isEqualToString:@"Unauthenticated."]) {
        //go To login
        
        [self logInUser];
    }
    else if ([dataPack[@"url"] isEqualToString:LOGIN_USER]) {
        //something bad happned with server
       _collectionView.hidden=YES;
    }
    
    
      else if ([Global isString:dataPack[@"url"] contains:SEARCH_FRAMES]){
          
        
          
          _collectionView.hidden=YES;
          [Global stopShimmeringToView:_collectionView];
          
      }
    
    else{
        _collectionView.hidden=YES;
        
        //there is some problme with product api
    }
    
    
}
-(void)requestFailedServiceUnavailableDueToMaintenance:(NSDictionary *)dataPack{
    self.view.userInteractionEnabled=true;
    _warningLbl.hidden=false;
     _collectionView.hidden=YES;
    _warningLbl.text = dataPack[@"message"];
    
}
-(void)uploadedData:(NSString *)pecentage andByte:(float)bytes{
    
}
-(void)requestSucceededButNoDataFound:(NSDictionary *)dataPack{
    self.view.userInteractionEnabled=true;
}
-(void)requestFailedDueToServerIsuueWithError:(NSString*)errorDes{
    self.view.userInteractionEnabled=true;
}



@end

