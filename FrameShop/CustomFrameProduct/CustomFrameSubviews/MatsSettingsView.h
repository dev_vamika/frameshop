//
//  MatsSettingsView.h
//  FrameShop
//
//  Created by Vamika on 19/03/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Validation.h"
#define DOUBLEMAT_MAX_ALERT  @"Max Double Mat Width is 5 cm or 2 inch"
#define DOUBLEMAT_MIN_ALERT  @"Mininum Double Mat Width is 0.5 cm or 0.2 inch"
#define TOPMAT_MAX_ALERT  @"Max Top Mat Width is 20 cm or 8 inch"
#define TOPMAT_MIN_ALERT  @"Mininum Top Mat Width is 2.5 cm or 1 inch"

NS_ASSUME_NONNULL_BEGIN
@class MatsSettingsView;
@protocol MatsSettingsViewDelegate <NSObject>
@optional
-(void)didChangeVGMat:(BOOL)isChecked;
-(void)didEnterUniformWidthForTopMat:(float)width;
-(void)didEnterTopWidthForTopMat:(float)width;
-(void)didEnterBottomWidthForTopMat:(float)width;
-(void)didEnterLeftWidthForTopMat:(float)width;
-(void)didEnterRightWidthForTopMat:(float)width;
-(void)didEnterUniformWidthForBottomMat:(float)width;


@end
@interface MatsSettingsView : UIViewController<UITextFieldDelegate>
@property(strong,nonatomic)id<MatsSettingsViewDelegate> delegate;
- (IBAction)onSelectUniformWidth:(id)sender;
- (IBAction)onSelectCustomWidtj:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *uniformWidthTF;
@property (weak, nonatomic) IBOutlet UITextField *topTF;
@property (weak, nonatomic) IBOutlet UITextField *bottomTF;
@property (weak, nonatomic) IBOutlet UITextField *leftTF;
@property (weak, nonatomic) IBOutlet UITextField *rightTF;
@property (weak, nonatomic) IBOutlet UITextField *doubleWidthTF;
- (IBAction)onSelectVGrovve:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *uniLbl;
@property (weak, nonatomic) IBOutlet UILabel *cusLbl;
@property (weak, nonatomic) IBOutlet UILabel *doubleMatLbl;
@property (weak, nonatomic) IBOutlet UIButton *vGrooveButton;
@property (weak, nonatomic) IBOutlet UIButton *uniformButton;
@property (weak, nonatomic) IBOutlet UIButton *customButton;
@property (weak, nonatomic) IBOutlet UISegmentedControl *unitSegment;
@property (weak, nonatomic) IBOutlet UILabel *uniformUnitLbl;
@property (weak, nonatomic) IBOutlet UILabel *customUnitLbl;
@property (weak, nonatomic) IBOutlet UILabel *doubleUnitLbl;
@property (weak, nonatomic) IBOutlet UILabel *top;
@property (weak, nonatomic) IBOutlet UILabel *bottom;
@property (weak, nonatomic) IBOutlet UILabel *left;
@property (weak, nonatomic) IBOutlet UILabel *right;
@property (weak, nonatomic) IBOutlet UIView *topBar;
- (IBAction)closeAction:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *doubleView;

@end

NS_ASSUME_NONNULL_END
