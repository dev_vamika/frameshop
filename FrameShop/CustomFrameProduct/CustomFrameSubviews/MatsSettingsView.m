//
//  MatsSettingsView.m
//  FrameShop
//
//  Created by Vamika on 19/03/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import "MatsSettingsView.h"
#import "Global.h"
#import "DBManager.h"
@interface MatsSettingsView (){
    DbManager *db;

}

@end

@implementation MatsSettingsView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    db=[[DbManager alloc] init];

    [Global roundCorner:_uniformWidthTF roundValue:0.0 borderWidth:0.5 borderColor:UIColor.lightGrayColor];
    [Global roundCorner:_topTF roundValue:0.0 borderWidth:0.5 borderColor:UIColor.lightGrayColor];
    [Global roundCorner:_bottomTF roundValue:0.0 borderWidth:0.5 borderColor:UIColor.lightGrayColor];
    [Global roundCorner:_leftTF roundValue:0.0 borderWidth:0.5 borderColor:UIColor.lightGrayColor];
    [Global roundCorner:_rightTF roundValue:0.0 borderWidth:0.5 borderColor:UIColor.lightGrayColor];
    [Global roundCorner:_doubleWidthTF roundValue:0.0 borderWidth:0.5 borderColor:UIColor.lightGrayColor];
   [self.unitSegment setTitleTextAttributes:@{NSFontAttributeName:self.uniLbl.font,
          NSForegroundColorAttributeName:APP_GRAY_COLOR}
         forState:UIControlStateNormal];
      [self.unitSegment setTitleTextAttributes:@{NSFontAttributeName:self.uniLbl.font,
       NSForegroundColorAttributeName:APP_BLUE_COLOR}
      forState:UIControlStateSelected];
    _uniformWidthTF.placeholder=@"2.5";
    _topTF.placeholder         =@"2.5";
    _bottomTF.placeholder      =@"2.5";
    _leftTF.placeholder        =@"2.5";
    _rightTF.placeholder       =@"2.5";
    _doubleWidthTF.placeholder =@"0.5";
   
   //
    
    NSMutableArray *arr=[db getDataForField:USER_PRODUCT where:nil];
    if (arr.count!=0) {
        
       
        
        
        if ([arr[0][@"style"] isEqualToString:@"Double"]) {
            _doubleMatLbl.hidden=false;
            _doubleWidthTF.hidden=false;
            _doubleUnitLbl.hidden=false;
            

        }
        else{
            _doubleMatLbl.hidden=true;
            _doubleWidthTF.hidden=true;
            _doubleUnitLbl.hidden=true;
        }
       
        if ([arr[0][@"topMat"] isEqualToString:@"V Groove"]) {
            _vGrooveButton.selected=YES;
        }
        
        
        
        if ([arr[0][@"isUni"] isEqualToString:@"1"]) {
              _uniformButton.selected=YES;
                _customButton.selected=NO;
                _uniformWidthTF.enabled=true;
                _topTF.enabled=false;
                _bottomTF.enabled=false;
                _leftTF.enabled=false;
                _rightTF.enabled=false;
            _topTF.textColor=[UIColor lightGrayColor];
            _leftTF.textColor=[UIColor lightGrayColor];
            _rightTF.textColor=[UIColor lightGrayColor];
            _bottomTF.textColor=[UIColor lightGrayColor];
            _top.textColor=[UIColor lightGrayColor];
            _bottom.textColor=[UIColor lightGrayColor];
            _left.textColor=[UIColor lightGrayColor];
            _right.textColor=[UIColor lightGrayColor];
            _customUnitLbl.textColor=[UIColor lightGrayColor];
            
           
        }
        else{
               _uniformButton.selected=NO;
               _customButton.selected=YES;
               _uniformWidthTF.enabled=false;
               _topTF.enabled=true;
               _bottomTF.enabled=true;
               _leftTF.enabled=true;
               _rightTF.enabled=true;
            _uniformWidthTF.textColor=[UIColor lightGrayColor];
        
              _uniformUnitLbl.textColor=[UIColor lightGrayColor];
        }
    
         _uniformWidthTF.text=([arr[0][@"top"]floatValue]!=0)? [NSString stringWithFormat:@"%.01f",[arr[0][@"top"] floatValue]]:@"";
        _topTF.text=([arr[0][@"top"]floatValue]!=0)? [NSString stringWithFormat:@"%.01f",[arr[0][@"top"] floatValue]]:@"";
          _bottomTF.text=([arr[0][@"bottom"]floatValue]!=0)? [NSString stringWithFormat:@"%.01f",[arr[0][@"bottom"] floatValue]]:@"";
          _leftTF.text=([arr[0][@"left"]floatValue]!=0)? [NSString stringWithFormat:@"%.01f",[arr[0][@"left"] floatValue]]:@"";
        _rightTF.text=([arr[0][@"right"]floatValue]!=0)? [NSString stringWithFormat:@"%.01f",[arr[0][@"right"] floatValue]]:@"";
        
        
           
         
       
           

        _doubleWidthTF.text=([arr[0][@"bottomUniformWidth"]floatValue]!=0)? [NSString stringWithFormat:@"%.01f",[arr[0][@"bottomUniformWidth"] floatValue]]:@"";
           
      
        if ([arr[0][@"unit"] isEqualToString:@"cm"]) {
                            
                              [self.unitSegment setSelectedSegmentIndex:0];
                            

                          }
                          else{
                                
                               [self.unitSegment setSelectedSegmentIndex:1];
                              [self onUnitChoose:self.unitSegment];
                           
                          }
        
        
        
    }
    
    
    [Global roundCorner:_topBar roundValue:_topBar.frame.size.height/2 borderWidth:0 borderColor:nil];
    
    
}

-(void)setCustomWidthsDefaults{
    
    NSMutableArray *arr=[db getDataForField:USER_PRODUCT where:nil];
    if (arr.count!=0) {
    
    _topTF.text=([arr[0][@"top"]floatValue]!=0)? [NSString stringWithFormat:@"%.01f",[arr[0][@"top"] floatValue]]:@"";
    _bottomTF.text=([arr[0][@"bottom"]floatValue]!=0)? [NSString stringWithFormat:@"%.01f",[arr[0][@"bottom"] floatValue]]:@"";
    _leftTF.text=([arr[0][@"left"]floatValue]!=0)? [NSString stringWithFormat:@"%.01f",[arr[0][@"left"] floatValue]]:@"";
    _rightTF.text=([arr[0][@"right"]floatValue]!=0)? [NSString stringWithFormat:@"%.01f",[arr[0][@"right"] floatValue]]:@"";
    }
}




- (IBAction)onUnitChoose:(id)sender {
    switch (_unitSegment.selectedSegmentIndex) {
        case 0:
            _customUnitLbl.text=@" cm";
            _uniformUnitLbl.text=@" cm";
            _doubleUnitLbl.text=@" cm";
            _uniformWidthTF.placeholder=@"2.5";
            _topTF.placeholder=@"2.5";
            _bottomTF.placeholder=@"2.5";
            _leftTF.placeholder=@"2.5";
            _rightTF.placeholder=@"2.5";
            _doubleWidthTF.placeholder=@"0.5";
            
           
            _uniformWidthTF.text= [_uniformWidthTF.text floatValue]==0?@"":[NSString stringWithFormat:@"%.01f",[_uniformWidthTF.text floatValue]*2.5];
            _topTF.text= [_topTF.text floatValue]==0?@"":[NSString stringWithFormat:@"%.01f",[_topTF.text floatValue]*2.5];
            _bottomTF.text=[_bottomTF.text floatValue]==0?@"":[NSString stringWithFormat:@"%.01f",[_bottomTF.text floatValue]*2.5];
            _leftTF.text=[_leftTF.text floatValue]==0?@"":[NSString stringWithFormat:@"%.01f",[_leftTF.text floatValue]*2.5];
            _rightTF.text=[_rightTF.text floatValue]==0?@"":[NSString stringWithFormat:@"%.01f",[_rightTF.text floatValue]*2.5];
            _doubleWidthTF.text=[_doubleWidthTF.text floatValue]==0?@"":[NSString stringWithFormat:@"%.01f",[_doubleWidthTF.text floatValue]*2.5];
           
            
            break;
            case 1:
            _customUnitLbl.text=@" inch";
            _uniformUnitLbl.text=@" inch";
            _doubleUnitLbl.text=@" inch";
            _uniformWidthTF.placeholder=@"1";
            _topTF.placeholder=@"1";
            _bottomTF.placeholder=@"1";
            _leftTF.placeholder=@"1";
            _rightTF.placeholder=@"1";
            _doubleWidthTF.placeholder=@"0.19";
            
            _uniformWidthTF.text= [_uniformWidthTF.text floatValue]==0?@"":[NSString stringWithFormat:@"%.01f",[_uniformWidthTF.text floatValue]/2.5];
            _topTF.text= [_topTF.text floatValue]==0?@"":[NSString stringWithFormat:@"%.01f",[_topTF.text floatValue]/2.5];
            _bottomTF.text=[_bottomTF.text floatValue]==0?@"":[NSString stringWithFormat:@"%.01f",[_bottomTF.text floatValue]/2.5];
            _leftTF.text=[_leftTF.text floatValue]==0?@"":[NSString stringWithFormat:@"%.01f",[_leftTF.text floatValue]/2.5];
            _rightTF.text=[_rightTF.text floatValue]==0?@"":[NSString stringWithFormat:@"%.01f",[_rightTF.text floatValue]/2.5];
            _doubleWidthTF.text=[_doubleWidthTF.text floatValue]==0?@"":[NSString stringWithFormat:@"%.01f",[_doubleWidthTF.text floatValue]/2.5];
            break;
            
        default:
            break;
    }
    
    db=[[DbManager alloc] init];
    NSMutableArray *arrdb=[db getDataForField:USER_PRODUCT where:nil];
    
    if (!([_customUnitLbl.text isKindOfClass:[NSNull class]]) )
    arrdb[0][UNIT] =  [_customUnitLbl.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    [db update:USER_PRODUCT :arrdb[0] :[NSString stringWithFormat:@"id='%@'",arrdb[0][@"id"]]];
}

- (IBAction)onSelectUniformWidth:(id)sender {
    _uniformButton.selected=YES;
    _customButton.selected=NO;
    _uniformWidthTF.enabled=true;
    _topTF.enabled=false;
    _bottomTF.enabled=false;
    _leftTF.enabled=false;
    _rightTF.enabled=false;
    
    
    
    [self.delegate didEnterUniformWidthForTopMat:[_uniformWidthTF.text floatValue]];
    
    _topTF.text=_uniformWidthTF.text;
    _bottomTF.text=_uniformWidthTF.text;
     _leftTF.text=_uniformWidthTF.text;
     _rightTF.text=_uniformWidthTF.text;
    
    _topTF.textColor=[UIColor lightGrayColor];
    _leftTF.textColor=[UIColor lightGrayColor];
    _rightTF.textColor=[UIColor lightGrayColor];
    _bottomTF.textColor=[UIColor lightGrayColor];
    _top.textColor=[UIColor lightGrayColor];
    _bottom.textColor=[UIColor lightGrayColor];
    _left.textColor=[UIColor lightGrayColor];
    _right.textColor=[UIColor lightGrayColor];
    _customUnitLbl.textColor=[UIColor lightGrayColor];
    _uniformWidthTF.textColor=[UIColor blackColor];
       _uniformUnitLbl.textColor=[UIColor blackColor];
}




- (IBAction)onSelectCustomWidtj:(id)sender {
    _uniformButton.selected=NO;
    _customButton.selected=YES;
    _uniformWidthTF.enabled=false;
    _topTF.enabled=true;
    _bottomTF.enabled=true;
    _leftTF.enabled=true;
    _rightTF.enabled=true;
    
    _topTF.textColor=[UIColor blackColor];
    _leftTF.textColor=[UIColor blackColor];
    _rightTF.textColor=[UIColor blackColor];
    _bottomTF.textColor=[UIColor blackColor];
    _top.textColor=[UIColor blackColor];
    _bottom.textColor=[UIColor blackColor];
    _left.textColor=[UIColor blackColor];
    _right.textColor=[UIColor blackColor];
    _customUnitLbl.textColor=[UIColor blackColor];
    _uniformWidthTF.textColor=[UIColor lightGrayColor];
    _uniformUnitLbl.textColor=[UIColor lightGrayColor];
    
       
}

- (IBAction)onSelectVGrovve:(id)sender {
    _vGrooveButton.selected=!_vGrooveButton.isSelected;
    [self.delegate didChangeVGMat:_vGrooveButton.isSelected];
}


-(void)textFieldDidEndEditing:(UITextField *)textField reason:(UITextFieldDidEndEditingReason)reason API_AVAILABLE(ios(10.0)){
    float value=[textField.text floatValue];
    if ([Validation isValidNumber:textField.text]) {
        
        if (_unitSegment.selectedSegmentIndex!=0) {
            value=[textField.text floatValue]*2.5;
        }
    }
    
        if (textField==_doubleWidthTF) {
            if (value<0.5) {
                value=0.5;
                
                textField.text= (_unitSegment.selectedSegmentIndex!=0)?@"0.2":@"0.5";
                [Global showSimpleAlert:self andTitle:FRAMESHOP andMessage:DOUBLEMAT_MIN_ALERT];
                
            }
         else  if (value>5) {
                value=5;
                
                textField.text= (_unitSegment.selectedSegmentIndex!=0)?@"2":@"5";
              [Global showSimpleAlert:self andTitle:FRAMESHOP andMessage:DOUBLEMAT_MAX_ALERT];
            }
        }
        
        else{
        if (value<2.5) {
            value=2.5;
            
            textField.text= (_unitSegment.selectedSegmentIndex!=0)?@"1":@"2.5";
             [Global showSimpleAlert:self andTitle:FRAMESHOP andMessage:TOPMAT_MIN_ALERT];
        }
      else  if (value>20) {
            value=20;
            
            textField.text= (_unitSegment.selectedSegmentIndex!=0)?@"8":@"20";
          [Global showSimpleAlert:self andTitle:FRAMESHOP andMessage:TOPMAT_MAX_ALERT];
        }
        
        }
        if (textField == _uniformWidthTF) {
            _topTF.text   =_uniformWidthTF.text;
            _bottomTF.text=_uniformWidthTF.text;
            _leftTF.text  =_uniformWidthTF.text;
            _rightTF.text =_uniformWidthTF.text;
            [self.delegate didEnterUniformWidthForTopMat:value];
        }
        else if (textField ==_topTF){
            _uniformWidthTF.text=_topTF.text;
            [self.delegate didEnterTopWidthForTopMat:value];
        }
        else if (textField ==_bottomTF){
           
            [self.delegate didEnterBottomWidthForTopMat:value];
        }
        else if (textField ==_leftTF){
           
            [self.delegate didEnterLeftWidthForTopMat:value];
        }
        else if (textField ==_rightTF){
            
            [self.delegate didEnterRightWidthForTopMat:value];
        }
        else if (textField ==_doubleWidthTF){
            
            [self.delegate didEnterUniformWidthForBottomMat:value];
        }
       
        
        
    
   
    
   
    
} // if implemented, called in place of textFieldDidEndEditing:



             // called when clear button pressed. return NO to ignore (no notifications)
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
   
    if ([textField.text containsString:@"."] && [string isEqualToString:@"."]) {
        return false;
    }
    if ((textField.text.length == 2 && ![textField.text containsString:@"."]) && ![string isEqualToString:@""]){
        if(![string containsString:@"."]){
            textField.text = [NSString stringWithFormat:@"%@.",textField.text];
            return YES;
        }
    }
     if (textField.text.length > 2 && [textField.text containsString:@"."]){
        NSArray *compnent = [textField.text componentsSeparatedByString:@"."];
        NSString *str = compnent.lastObject;
        if(str.length > 0){
            textField.text = [NSString stringWithFormat:@"%@.%@",compnent.firstObject,[str substringToIndex:[str length] - 1]];
        }else{
             return true;
        }
    }
    if (textField.text.length >4 && [textField.text containsString:@"."]){
           return false;
       }
    if ([[NSString stringWithFormat:@"%@%@",textField.text,string] length]>5) {
        textField.text=textField.text;
    }
    else if ([[NSString stringWithFormat:@"%@%@",textField.text,string] floatValue]>99){
        textField.text=textField.text;
    }
    
    return YES;
}

- (IBAction)closeAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
