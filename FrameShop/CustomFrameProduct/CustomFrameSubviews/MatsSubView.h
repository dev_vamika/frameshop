//
//  MatsSubView.h
//  FrameShop
//
//  Created by Saurabh anand on 20/12/19.
//  Copyright © 2019 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopApi.h"
NS_ASSUME_NONNULL_BEGIN
@class MatsSubView;
@protocol MatsSubViewDelegate <NSObject>
@optional
-(void)didSelectCoreColor:(NSMutableDictionary *)infoDic;
-(void)didChooseMatOption:(NSMutableDictionary *)infoDic;
-(void)changeFrameSettingWhereType:(NSMutableDictionary *)infoDic;
    - (void)framAction:(int)index;
-(void)didSelectSingleFrame;
-(void)didSelectDoubleFrame;
-(void)didSelectNoneFrame;


@end
@interface MatsSubView : UIViewController<PopApiDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property(strong,nonatomic)id<MatsSubViewDelegate> delegate;
@property NSString *type;

- (IBAction)onFrame:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *frameBtn;
@property (weak, nonatomic) IBOutlet UIButton *matBtn;
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIButton *settingButton;
- (IBAction)onSetting:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *optionalBtn;
@property (weak, nonatomic) IBOutlet UIView *colorDetailContainerView;
@property (weak, nonatomic) IBOutlet UILabel *colorNameLbl;
@property (weak, nonatomic) IBOutlet UICollectionView *colorCollectionView;
@property (weak, nonatomic) IBOutlet UIButton *colorBtn;
@property (weak, nonatomic) IBOutlet UICollectionView *doubleCollectionView;
@property (weak, nonatomic) IBOutlet UIButton *noneButton;
@property (weak, nonatomic) IBOutlet UIButton *singleButton;
@property (weak, nonatomic) IBOutlet UIButton *doubleButton;
@property (weak, nonatomic) IBOutlet UIView *topMatBg;
@property (weak, nonatomic) IBOutlet UIView *topMatColor;
@property (weak, nonatomic) IBOutlet UILabel *topMatColorName;
@property (weak, nonatomic) IBOutlet UIView *BottomMatBg;
@property (weak, nonatomic) IBOutlet UIView *BottomMatColor;
@property (weak, nonatomic) IBOutlet UILabel *bottomMatColorName;
- (IBAction)onClickNone:(id)sender;
- (IBAction)onClickSingle:(id)sender;
- (IBAction)onClickDouble:(id)sender;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topMatWidth;
@property (weak, nonatomic) IBOutlet UIView *topMatTopView;
@property (weak, nonatomic) IBOutlet UIView *topMatBar;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionTop;
@property (weak, nonatomic) IBOutlet UIView *collectionContainerView;
@end

NS_ASSUME_NONNULL_END
