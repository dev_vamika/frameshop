//
//  SortingTableCell.h
//  FrameShop
//
//  Created by Vamika on 24/03/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SortingTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageRadioView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

NS_ASSUME_NONNULL_END
