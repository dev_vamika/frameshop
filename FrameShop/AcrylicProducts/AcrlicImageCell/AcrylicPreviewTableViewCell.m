//
//  AcrylicPreviewTableViewCell.m
//  FrameShop
//
//  Created by Saurabh Srivastav on 18/02/21.
//  Copyright © 2021 Mayank Barnwal. All rights reserved.
//

#import "AcrylicPreviewTableViewCell.h"
#import "AcrylicPhotoBlockCollectionViewCell.h"
#import "Global.h"
#import "AppDelegate.h"
@implementation AcrylicPreviewTableViewCell{

//    UIImage *img,*normalImg,*acrylicImg;
    bool isChanged;
//    NSString * sku;
}

- (void)awakeFromNib {
   // [super awakeFromNib];
    UINib *nib = [UINib nibWithNibName:@"AcrylicSliderViewCell" bundle:nil];
    [_collectionView registerNib:nib forCellWithReuseIdentifier:@"AcrylicSliderViewCell"];
    UINib *nib2 = [UINib nibWithNibName:@"AcrylicPhotoBlockCollectionViewCell" bundle:nil];
    [_collectionView registerNib:nib2 forCellWithReuseIdentifier:@"AcrylicPhotoBlockCollectionViewCell"];
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    _pageController.numberOfPages = 3;
    _pageController.currentPage = _previewNumber;
    _pageController.currentPageIndicatorTintColor = APP_BLUE_COLOR;
    [_collectionView setAlwaysBounceVertical:false];
    [_collectionView setBouncesZoom:false];
    [_collectionView setContentOffset:CGPointZero];
    //[self->_collectionView reloadData];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
- (void)layoutSubviews{
    //[_collectionView  scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:_previewNumber inSection:0] atScrollPosition:UICollectionViewScrollPositionTop animated:YES];
}
-(void)genrateSlideView{
   
//    if (_previewNumber != 0){
//        isChanged = true;
//    }else{
//        isChanged = false;
//        
//    }
   
    _pageController.currentPage = _previewNumber;
    //[self.collectionView  scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:_previewNumber inSection:0] atScrollPosition:UICollectionViewScrollPositionNone animated:YES];
}




#pragma mark - UICollectionViewDataSource and Delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    return  (_productDic[@"normalImage"]!=nil) ?  3 :0 ;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *identifier = @"AcrylicSliderViewCell";
    static NSString *identifier2 = @"AcrylicPhotoBlockCollectionViewCell";
    AcrylicSliderViewCell *frameView =[_collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    

        
        
        if ([_productDic[@"sku"] isEqualToString:BLOCK_MOUNT_SKU] || [_productDic[@"sku"] isEqualToString:CANVAS_PRINTING_SKU]){
            
            if (indexPath.row == 0){
                frameView.sliderImage.image =[Global getMidiumSizeImage:_productDic[@"acrylicImage"] imageSize:frameView.sliderImage.frame.size.width*1.5]  ;
                _productDic[[NSString stringWithFormat:@"index_%d",(int)indexPath.row]]= _productDic[@"acrylicImage"];
                //[Global addshadow:frameView.sliderImage radious:4];
            }
        else if(indexPath.row == 1){
            
            UIImage *img =_productDic[@"normalImage"];
            
            frameView.sliderImage.image =[Global getMidiumSizeImage:_productDic[@"normalImage"] imageSize:frameView.sliderImage.frame.size.width*1.5];
            _productDic[[NSString stringWithFormat:@"index_%d",(int)indexPath.row]]= _productDic[@"normalImage"];
        }
        else if(indexPath.row == 2){
            if (([_productDic[@"width_tf"] floatValue]>30) ||([_productDic[@"height_tf"] floatValue]>30) ) {
                frameView.sliderImage.image =[Global getMidiumSizeImage:([_productDic[@"sku"] isEqualToString:CANVAS_PRINTING_SKU])?[UIImage imageNamed:@"canvas_printing.jpg"]:  [UIImage imageNamed:@"block_mount_hanging_wall_mount.png"] imageSize:frameView.sliderImage.frame.size.width*1.5]  ;
                _productDic[[NSString stringWithFormat:@"index_%d",(int)indexPath.row]]= ([_productDic[@"sku"] isEqualToString:CANVAS_PRINTING_SKU])?[UIImage imageNamed:@"canvas_printing.jpg"]:  [UIImage imageNamed:@"block_mount_hanging_wall_mount.png"];
                
                //[UIImage imageNamed:@"block_mount_hanging_wall_mount.png"];
            }
            else{
                frameView.sliderImage.image = [Global getMidiumSizeImage:([_productDic[@"sku"] isEqualToString:CANVAS_PRINTING_SKU])?[UIImage imageNamed:@"canvas_printing.jpg"]: [UIImage imageNamed:@"block_mount_hanging_sawtooth.png"] imageSize:frameView.sliderImage.frame.size.width*1.5] ;
                _productDic[[NSString stringWithFormat:@"index_%d",(int)indexPath.row]]= ([_productDic[@"sku"] isEqualToString:CANVAS_PRINTING_SKU])?[UIImage imageNamed:@"canvas_printing.jpg"]: [UIImage imageNamed:@"block_mount_hanging_sawtooth.png"];
            }
           
            
        }
        else{
            
        }
        }
        
        else{
        
        if (indexPath.row == 0) {
            
            
            
            frameView.sliderImage.image = [Global getMidiumSizeImage:_productDic[@"normalImage"] imageSize:frameView.sliderImage.frame.size.width*1.5];
          
            _productDic[[NSString stringWithFormat:@"index_%d",(int)indexPath.row]]= _productDic[@"normalImage"];
            //[Global addshadow:frameView.sliderImage radious:4];
            
            
        }else if(indexPath.row == 1){
            if ([_productDic[@"sku"] isEqualToString:ACRYLIC_PHOTOBOOK]){
                AcrylicPhotoBlockCollectionViewCell *frameView =[_collectionView dequeueReusableCellWithReuseIdentifier:identifier2 forIndexPath:indexPath];

                UIImage *mg = [Global getMidiumSizeImage:_productDic[@"normalImage"] imageSize:frameView.preViewImage.frame.size.width];
                frameView.mg = mg;
                frameView.preViewImage.image = _previewImage;
                [Global addshadow:frameView.preViewImage radious:4];
               // [Global addshadow:frameView.mainView radious:4];
                _productDic[[NSString stringWithFormat:@"index_%d",(int)indexPath.row]]= _previewImage;
                [frameView layoutSubviews];
                return  frameView;
            }else{
            frameView.sliderImage.image =[Global getMidiumSizeImage:_productDic[@"acrylicImage"] imageSize:frameView.sliderImage.frame.size.width*1.5]  ;
            }
           // frameView.sliderImage.image = acrylicImg;
            [Global addshadow:frameView.sliderImage radious:4];
            _productDic[[NSString stringWithFormat:@"index_%d",(int)indexPath.row]]= _productDic[@"acrylicImage"];
            
            
        }
        else{
 
                
                if (([_productDic[@"width_tf"] floatValue]>30) ||([_productDic[@"height_tf"] floatValue]>30) )  {
                    frameView.sliderImage.image = [Global getMidiumSizeImage:[UIImage imageNamed:@"acroption2.png"] imageSize:frameView.sliderImage.frame.size.width*1.5];
                    _productDic[[NSString stringWithFormat:@"index_%d",(int)indexPath.row]]= [UIImage imageNamed:@"acroption2.png"];
                }
                else{
                    frameView.sliderImage.image = [Global getMidiumSizeImage:[UIImage imageNamed:@"acroption1.png"] imageSize:frameView.sliderImage.frame.size.width*1.5];
                    _productDic[[NSString stringWithFormat:@"index_%d",(int)indexPath.row]]= [UIImage imageNamed:@"acroption1.png"];
                }
           
                
                [Global addshadow:frameView.sliderImage radious:0];
                
                //  [frameView showNormal];
                
        
        }
        }
        //        if (normalImg !=nil && acrylicImg !=nil) {
      
        //        }
        //
    
    return frameView;
    
}

- (CGSize)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(self.collectionView.frame.size.width, _collectionView.frame.size.height);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{

  
    [Global callFullImage:_productDic[[NSString stringWithFormat:@"index_%d",(int)indexPath.row]]];
    
   
}
- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath{
  //  [_delegate indexChanged:indexPath.row];
    self.pageController.currentPage = indexPath.row;
   // _previewNumber = indexPath.row;
}
- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath{
    if (_pageController.currentPage == indexPath.row) {
        _pageController.currentPage = [_collectionView indexPathForCell:_collectionView.visibleCells.firstObject].row;
    }
}


@end
