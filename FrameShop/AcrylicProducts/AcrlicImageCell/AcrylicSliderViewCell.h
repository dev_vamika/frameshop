//
//  AcrylicSliderViewCell.h
//  FrameShop
//
//  Created by Vamika on 12/03/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AcrylicSliderViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *sliderImage;

@end

NS_ASSUME_NONNULL_END
