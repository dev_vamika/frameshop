//
//  AcrylicDetailTableViewCell.h
//  FrameShop
//
//  Created by Saurabh Srivastav on 18/02/21.
//  Copyright © 2021 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBManager.h"
#import "PopApi.h"
NS_ASSUME_NONNULL_BEGIN
@class AcrylicDetailTableViewCell;
@protocol AcrylicDetailTableViewCellDelegate <NSObject>
@optional
-(void)didGetPrice:(NSDictionary *)infoDic;


@end
@interface AcrylicDetailTableViewCell : UITableViewCell<PopApiDelegate>
@property(strong,nonatomic)id<AcrylicDetailTableViewCellDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activity;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *sizeLbl;
@property (weak, nonatomic) IBOutlet UILabel *priceLbl;
@property (weak, nonatomic) IBOutlet UILabel *productDescriptionLbl;
@property (weak, nonatomic) IBOutlet UIImageView *productImage;
@property (weak, nonatomic) IBOutlet UILabel *productTitle;

@property NSDictionary *productDic;
-(void)setUpView;
-(void)setUpHistoryView;
@end

NS_ASSUME_NONNULL_END
