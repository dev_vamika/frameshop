//
//  CanvasTableViewCell.m
//  FrameShop
//
//  Created by Saurabh Srivastav on 18/02/21.
//  Copyright © 2021 Mayank Barnwal. All rights reserved.
//

#import "CanvasTableViewCell.h"
#import "Global.h"
#import "DBManager.h"
@implementation CanvasTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    //[Global roundCorner:_hangingView roundValue:5 borderWidth:0.5 borderColor:APP_BLUE_COLOR];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)setUpHistoryView{
    _priceLbl.text=[NSString stringWithFormat:@"Price: $%0.2f",[[NSString stringWithFormat:@"%@",_productDic[@"price"]] floatValue]] ;
    _sizeLbl.text=[NSString stringWithFormat:@"Print Size: %.1f x %.1f cm",[_productDic[@"width"]floatValue],[_productDic[@"height"]floatValue]];
    _overAllSize.text=[NSString stringWithFormat:@"Overall Size: %.1f x %.1f cm",[_productDic[@"width"]floatValue],[_productDic[@"height"]floatValue]];
    _productDescriptionLbl.text = @"Your image will be professionally printed on a fine art canvas, hand-stretched onto high quality timber bars and fitted with a hanging system on the back.";
    
    if ([_productDic[@"edge"] isEqualToString:@"WHITE"]) {
        _productTitle.text = @"Gallery Wrap";
       // _productDescriptionLbl.text = @"Your image will be printed and stretched onto a frame.";
        _descriptionLabel.text = @"";
        self->_productDis.text =@"Your image will continue over the edge of the canvas, some of the content will be lost to the edge of the canvas but will create a flowing continuation of your image.";
        self->_edgeType.text = @"Gallery Wrap";
       // self.productImage.image = [UIImage imageNamed:@"canvas_blank.png"];
        
    }
    else if ([_productDic[@"edge"] isEqualToString:@"MIRROR"]){
        _productTitle.text = @"Mirror Wrap";
       // _productDescriptionLbl.text = @"Your image will be printed and stretched onto a frame.";
        _descriptionLabel.text = @"";
        self->_productDis.text =@"Your image will be cloned/mirrored along the edges of the canvas, this is great for landscapes or scenery shots to have it look like it continues over the edge.";
        self->_edgeType.text = @"Mirror Wrap";
        //self.productImage.image = [UIImage imageNamed:@"canvas_blank.png"];
        
    }
    else{
        
        _productTitle.text = @"Colour Wrap";
        //_productDescriptionLbl.text = @"Your image will be printed and rolled in a tube.";
        _descriptionLabel.text = @"";
        self->_edgeType.text = @"Colour Wrap";
       // self.productImage.image = [UIImage imageNamed:@"canvas_rolled.png"];
        self->_productDis.text =@"Your image will be printed on the face of the canvas and will have a solid colour of your choice wrapped along the edges of the canvas.";
        
        
    }
    if (_productImage.image == nil) {
       
        
        dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(q, ^{
            //[Global startShimmeringToView:cell.contentView];
            NSData *datas;

            datas =[NSData dataWithContentsOfURL:[NSURL URLWithString:self->_productDic[@"thumb_url"]]] ;
            


            UIImage *img = [[UIImage alloc] initWithData:datas];
            dispatch_async(dispatch_get_main_queue(), ^{
                if(img!=nil)
                    self.productImage.image = img;

            });
        });
        
        
    }
    
}

-(void)setUpView{
    
    
    _priceLbl.text=[NSString stringWithFormat:@"Price: $%0.2f",[[NSString stringWithFormat:@"%@",_productDic[@"price"]] floatValue]] ;
    
    if ([_productDic[UNIT] isEqualToString:@"inch"]) {
        float oneCm = 1/2.54;//inch
        
        _sizeLbl.text=[NSString stringWithFormat:@"Print Size: %0.1f x %0.1f inch",([_productDic[@"width_tf"] floatValue]) *oneCm,([_productDic[@"height_tf"] floatValue] ) *oneCm];
        _overAllSize.text=[NSString stringWithFormat:@"Overall Size: %0.1f x %0.1f inch",([_productDic[@"width_tf"] floatValue]) *oneCm,([_productDic[@"height_tf"] floatValue]) *oneCm];
        
    }else{
        _sizeLbl.text=[NSString stringWithFormat:@"Print Size: %.1f x %.1f cm",[_productDic[@"width_tf"]floatValue],[_productDic[@"height_tf"]floatValue]];
        _overAllSize.text=[NSString stringWithFormat:@"Overall Size: %.1f x %.1f cm",[_productDic[@"width_tf"]floatValue],[_productDic[@"height_tf"]floatValue]];
    }
    
    self->_productDescriptionLbl.text = @"  Your image will be professionally printed on a fine art canvas, hand-stretched onto high quality timber bars and fitted with a hanging system on the back.";
    
    if ([self->_productDic[@"core"] isEqualToString:@"BLACK"]) {
        //COLOR
        
     
//        self->_productTitle.text = @"Color Wrap";
       // self->_productDescriptionLbl.text = @"Your image will be printed and rolled in a tube.";
        self->_descriptionLabel.text = @"";
        self->_productDis.text =@"";
        self->_edgeType.text = @"Colour Wrap";
        self->_productDis.text =@"Your image will be printed on the face of the canvas and will have a solid colour of your choice wrapped along the edges of the canvas.";
        
    }
    else if ([self->_productDic[@"core"] isEqualToString:@"WHITE"]) {
       // self->_productTitle.text = @"Gallery Wrap";
       // self->_productDescriptionLbl.text = @"Your image will be printed and stretched onto a frame.";
        self->_descriptionLabel.text = @"";
        
        
        self->_productDis.text =@"Your image will continue over the edge of the canvas, some of the content will be lost to the edge of the canvas but will create a flowing continuation of your image.";
        self->_edgeType.text = @"Gallery Wrap";
    }
    else{
      //  self->_productTitle.text = @"Mirror Wrap";
       // self->_productDescriptionLbl.text = @"Your image will be printed and stretched onto a frame.";
        self->_descriptionLabel.text = @"Your image will be cloned/mirrored along the edges of the canvas, this is great for landscapes or scenery shots to have it look like it continues over the edge.";
      
        
        self->_productDis.text =@"Your image will be cloned/mirrored along the edges of the canvas, this is great for landscapes or scenery shots to have it look like it continues over the edge.";
        self->_edgeType.text = @"Mirror Wrap";
    }
    
    
    
    if (@available(iOS 13.0, *)) {
        _productImage.backgroundColor = [UIColor systemGray6Color];
    } else {
        // Fallback on earlier versions
        _productImage.backgroundColor = [UIColor lightTextColor];
    }
    
    
    dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(q, ^{
//<<<<<<< HEAD
        
        
        UIImage *img =  [Global getImageFromDoucumentWithName:@"frame_preview"];
        

       // UIImage *img =  [Global getImageFromDoucumentWithName:@"frame_preview"];
//>>>>>>> 885b81a4 (new Code With V2)
        dispatch_async(dispatch_get_main_queue(), ^{
            if(img!=nil){
                self->_productImage.backgroundColor = [UIColor clearColor];
               // if (self.productImage.image != nil){
                self.productImage.image = img;
//<<<<<<< HEAD
               // }
              

 
                
                
//=======
//>>>>>>> 885b81a4 (new Code With V2)
            }
        });
    });
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    if ([_productDic[@"core"] isEqualToString:@"WHITE"]) {
        
        
        
        
    }
    else if ([_productDic[@"core"] isEqualToString:@"MIRROR"]){
        
        
        
        
    }
    else{
        
        
        
        
        
    }
    [self.delegate didGetPrice:_productDic];
    
}
@end
