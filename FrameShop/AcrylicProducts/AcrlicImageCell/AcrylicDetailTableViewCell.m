//
//  AcrylicDetailTableViewCell.m
//  FrameShop
//
//  Created by Saurabh Srivastav on 18/02/21.
//  Copyright © 2021 Mayank Barnwal. All rights reserved.
//

#import "AcrylicDetailTableViewCell.h"
#import "Global.h"

@implementation AcrylicDetailTableViewCell{
    DbManager *db;
    NSString *payableAmount;
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    db=[[DbManager alloc] init];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)setUpHistoryView{
    
    _activity.hidden = YES;
    _sizeLbl.text=[NSString stringWithFormat:@"Print Size: %.1f x %.1f cm",[_productDic[@"width"]floatValue],[_productDic[@"height"]floatValue]];
    _priceLbl.text=[NSString stringWithFormat:@"Price: $%0.2f",[[NSString stringWithFormat:@"%@",_productDic[@"price"]] floatValue]] ;
    
    if (([_productDic[@"width"] floatValue]>30) ||([_productDic[@"height"] floatValue]>30) ) {
        //        self.standImageView.image = [UIImage imageNamed:@"stand.jpg"];
        self.productImage.image =[Global getMidiumSizeImage:  [UIImage imageNamed:@"acroption2.png"] imageSize:self.productImage.frame.size.width*1.5];
        //self.boltButton.selected = YES;
        //self.standPlusHangerButton.selected = NO;
        self.productDescriptionLbl.text = @"Your acrylic glass print will come with a support frame to provide stability and an easy to use hanging system for your wall.";
        
        
        
        self.productTitle.text =([_productDic[@"sku"] isEqualToString:BLOCK_MOUNT_SKU])?@"Support Frame":  @"Wall Mount";
    }
    else{
        // self.standPlusHangerButton.selected = YES;
        self.productImage.image =[Global getMidiumSizeImage: [UIImage imageNamed:@"standH.jpg"] imageSize:self.productImage.frame.size.width*1.5]  ;
        self.productDescriptionLbl.text = @"Your acrylic glass print will come with a keyhole hanging system and standback for you to optionally place on your desk.";
        
        self.productTitle.text =([_productDic[@"sku"] isEqualToString:BLOCK_MOUNT_SKU])?@"Sawtooth":  @"Standback and Keyhole";
        //        self.wallMountImage.image = [UIImage imageNamed:@"wallmount.png"];
    }
}

-(void)setUpView{
    
    
    if ([_productDic[UNIT] isEqualToString:@"inch"]) {
        float oneCm = 1/2.54;//inch
        _sizeLbl.text=[NSString stringWithFormat:@"Print Size: %0.1f x %0.1f inch",[_productDic[@"width_tf"] floatValue] *oneCm,[_productDic[@"height_tf"] floatValue] *oneCm];
    }else{
        _sizeLbl.text=[NSString stringWithFormat:@"Print Size: %.1f x %.1f cm",[_productDic[@"width_tf"]floatValue],[_productDic[@"height_tf"]floatValue]];
    }
    
    _nameLbl.text = @"ACRYLIC PRINTS";
    // _canvasView.hidden = true;
    
    _priceLbl.text=@"";
    
    if (isiPhone5s) {
        self.productImage.frame = CGRectMake(self.productImage.frame.origin.x, self.productImage.frame.origin.y, self.productImage.frame.size.width, 100);
    }
    if (([_productDic[@"width_tf"] floatValue]>30) ||([_productDic[@"height_tf"] floatValue]>30) ) {
        //        self.standImageView.image = [UIImage imageNamed:@"stand.jpg"];
        self.productImage.image = [Global getMidiumSizeImage: [UIImage imageNamed:@"acroption2.png"] imageSize:self.productImage.frame.size.width*1.5];
        //self.boltButton.selected = YES;
        //self.standPlusHangerButton.selected = NO;
        self.productDescriptionLbl.text = @"Your acrylic glass print will come with a support frame to provide stability and an easy to use hanging system for your wall.";
        
        
        
        self.productTitle.text =([_productDic[@"sku"] isEqualToString:BLOCK_MOUNT_SKU])?@"Support Frame":  @"Wall Mount";
    }
    else{
        // self.standPlusHangerButton.selected = YES;
        self.productImage.image =  [Global getMidiumSizeImage: [UIImage imageNamed:@"standH.jpg"] imageSize:self.productImage.frame.size.width*1.5]  ;
        self.productDescriptionLbl.text = @"Your acrylic glass print will come with a keyhole hanging system and standback for you to optionally place on your desk.";
        
        self.productTitle.text =([_productDic[@"sku"] isEqualToString:BLOCK_MOUNT_SKU])?@"Sawtooth":  @"Standback and Keyhole";
        //        self.wallMountImage.image = [UIImage imageNamed:@"wallmount.png"];
    }
    
    
    
    [self getItemPrice];
}

-(void)showLoader:(BOOL)shouldShow{
    _activity.hidden = !shouldShow;
    if (shouldShow) {
        [_activity startAnimating];
    }
    else{
        [_activity stopAnimating];
    }
}

-(void)getItemPrice{
    
    [self showLoader:YES];
    if ([_productDic[@"sku"] isEqualToString:CANVAS_PRINTING_SKU]) {
        
        
        
        
        // [_actiVityBar removeFromSuperview];
        _priceLbl.text=[NSString stringWithFormat:@"Price: $%0.2f",[[NSString stringWithFormat:@"%@",_productDic[@"price"]] floatValue]] ;
        [self.delegate didGetPrice:_productDic];
        
        
        
        
    }
    
    else{
        PopApi *api=[[PopApi alloc] init];
        api.delegate=self;
        NSString *urlString=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,GET_PRICE];
        NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
        dic[@"url"]=urlString;
        
        NSMutableArray *arr=[[NSMutableArray alloc] init];
        arr=[db getDataForField:USERTABLE where:nil];
        
        if (arr.count!=0) {
            dic[@"token"]=arr[0][@"token"];
            
            NSMutableArray *proArr=[[NSMutableArray alloc] init];
            proArr=[db getDataForField:PRODUCT_TABLE where:[NSString stringWithFormat:@"id='%@'",_productDic[@"id"]]];
            dic[@"width"]=@([_productDic[@"width_tf"] floatValue]) ;
            dic[@"height"]=@([_productDic[@"height_tf"] floatValue]);
            
            if ([_productDic[@"sku"] isEqualToString:BLOCK_MOUNT_SKU]) {
                dic[@"type"]=@"product";
                dic[@"product_type"]=_productDic[@"sku"];
                dic[@"backing"]=@"MDF";
                dic[@"hanger"]=@"product";
                
                if (([_productDic[@"width_tf"] floatValue]>30) ||([_productDic[@"height_tf"] floatValue]>30) )  {
                    dic[@"hanger"] =@"HANGER_SUPPORT_FRAME";
                }else{
                    dic[@"hanger"] =@"HANGER_SAWTOOTH";
                }
                
                
            }
            else{
                
                dic[@"type"]=@"material";
                if (([_productDic[@"width_tf"] floatValue]>30) ||([_productDic[@"height_tf"] floatValue]>30) )  {
                    dic[@"material_type"]=@"ACRYLIC_WALL_MOUNT";
                }
                else{
                    dic[@"material_type"]=@"ACRYLIC_STANDBACK";
                }
                
            }
            [api sendPostRequst:dic];
            
            
        }
        
    }
    
}

-(void)signUpUser{
    
    
    
    
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
    dic[@"name"]=[[UIDevice currentDevice] name];
    dic[@"email"]=[NSString stringWithFormat:@"%@@frameshop.com.au",[Global getDeviceId]];
    dic[@"password"]=@"qwxcjeub32ne";
    dic[@"c_password"]=@"qwxcjeub32ne";
    dic[@"url"]=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,REGISTER_USER];
    [api sendPostRequst:dic];
    
}
-(void)logInUser{
    
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
    
    dic[@"email"]=[NSString stringWithFormat:@"%@@frameshop.com.au",[Global getDeviceId]];
    dic[@"password"]=@"qwxcjeub32ne";
    
    dic[@"url"]=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,LOGIN_USER];
    [api sendPostRequst:dic];
}
#pragma mark -Pop api Delegates

-(void)connectionError{
    [self showLoader:NO];
}
-(void)requestFailedSessionExpired:(NSDictionary *)dataPack{
    [self showLoader:NO];
}
-(void)requestFailedServerError:(NSDictionary *)dataPack{
    [self showLoader:NO];
}
-(void)requestFailedServiceUnavailableDueToMaintenance:(NSDictionary *)dataPack{
    [self showLoader:NO];
    //  [_actiVityBar removeFromSuperview];
    //[Global showSimpleAlert:self andTitle:FRAMESHOP andMessage:dataPack[@"message"]];
}
-(void)requestFailedMethodNotFound:(NSDictionary *)dataPack{
    [self showLoader:NO];
    // [_actiVityBar removeFromSuperview];
}
-(void)requestFailedNotFound:(NSDictionary *)dataPack{
    [self showLoader:NO];
    // [_actiVityBar removeFromSuperview];
}
-(void)requestFailedUnauthorised:(NSDictionary *)dataPack{
    [self showLoader:NO];
    // [_act [self showLoader:NO];iVityBar removeFromSuperview];
    [self logInUser];
}
-(void)requestFailedBadRequest:(NSDictionary *)dataPack{
    [self showLoader:NO];
    // [_actiVityBar removeFromSuperview];
}
-(void)requestSucceededWithData:(NSDictionary *)dataPack{
    
    [self showLoader:NO];
    if ([dataPack[@"url"] isEqualToString:LOGIN_USER]) {
        
        [db delTableWithTableName:USERTABLE];
        NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
        
        dic[@"id"]              =[NSString stringWithFormat:@"%@",dataPack[@"data"][@"id"]];
        dic[@"name"]            =dataPack[@"data"][@"name"];
        dic[@"email"]           =dataPack[@"data"][@"email"];
        dic[@"created_at"]      =dataPack[@"data"][@"created_at"];
        dic[@"updated_at"]      =dataPack[@"data"][@"updated_at"];
        dic[@"token"]           =dataPack[@"data"][@"access_token"];
        
        [db insertIntoTable:dic table_name:USERTABLE];
        [self getItemPrice];
    }
    
    else if ([dataPack[@"url"] isEqualToString:REGISTER_USER]){
        
        NSMutableDictionary *dic =[[NSMutableDictionary alloc] init];
        dic[@"token"]            =dataPack[@"data"][@"token"];
        dic[@"id"]               =[NSString stringWithFormat:@"%@",dic[@"id"]];
        
        [db insertIntoTable:dic table_name:USERTABLE];
        [self getItemPrice];
    }
    else  if ([dataPack[@"url"] isEqualToString:GET_PRICE]){
        
        payableAmount=[NSString stringWithFormat:@"%@",dataPack[@"data"][@"price"]] ;
        
        _priceLbl.text=[NSString stringWithFormat:@"Price: $%0.2f",[[NSString stringWithFormat:@"%@",dataPack[@"data"][@"price"]] floatValue]] ;
        NSMutableArray*    tempArr=[db getDataForField:USER_PRODUCT where:nil];
        
        tempArr[0][@"shipping_price"] =[NSString stringWithFormat:@"%.2f",[dataPack[@"data"][@"shipping_charges"] floatValue]];
        //  tempArr[0][@"price"] =[NSString stringWithFormat:@"%.2f",[dataPack[@"data"][@"price"] floatValue]];
        [db update:USER_PRODUCT :tempArr[0] :[NSString stringWithFormat:@"id='%@'",tempArr[0][@"id"]]];
        [self.delegate didGetPrice:dataPack[@"data"]];
    }
    
    
}
-(void)requestSucceededButActionFailed:(NSDictionary *)dataPack{
    [self showLoader:NO];
    //[self.delegate serverRequestFailed];
    //[_actiVityBar removeFromSuperview];
}
-(void)uploadedData:(NSString *)pecentage andByte:(float)bytes{
    
}
-(void)requestSucceededButNoDataFound:(NSDictionary *)dataPack{
    [self showLoader:NO];
    // [_actiVityBar removeFromSuperview];
}
-(void)requestFailedDueToServerIsuueWithError:(NSString*)errorDes{
    [self showLoader:NO];
    // [_actiVityBar removeFromSuperview];
}

@end
