//
//  CanvasTableViewCell.h
//  FrameShop
//
//  Created by Saurabh Srivastav on 18/02/21.
//  Copyright © 2021 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class CanvasTableViewCell;
@protocol CanvasTableViewCellDelegate <NSObject>
@optional
-(void)didGetPrice:(NSDictionary *_Nonnull)infoDic;


@end
@interface CanvasTableViewCell : UITableViewCell
@property(strong,nonatomic)id<CanvasTableViewCellDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UILabel *overAllSize;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activity;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *sizeLbl;
@property (weak, nonatomic) IBOutlet UILabel *priceLbl;
@property (weak, nonatomic) IBOutlet UILabel *productDescriptionLbl;
@property (weak, nonatomic) IBOutlet UIImageView *productImage;
@property (weak, nonatomic) IBOutlet UILabel *productTitle;
@property (weak, nonatomic) IBOutlet UIView *hangingView;
@property (weak, nonatomic) IBOutlet UILabel *edgeType;
@property (weak, nonatomic) IBOutlet UILabel *productDis;
@property NSDictionary *productDic;
-(void)setUpView;
-(void)setUpHistoryView;
@end

NS_ASSUME_NONNULL_END
