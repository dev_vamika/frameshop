//
//  AcrylicPreviewTableViewCell.h
//  FrameShop
//
//  Created by Saurabh Srivastav on 18/02/21.
//  Copyright © 2021 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AcrylicSliderViewCell.h"
NS_ASSUME_NONNULL_BEGIN
@protocol MaintainState <NSObject>
@optional

-(void)indexChanged:(int *)val;;



@end
@interface AcrylicPreviewTableViewCell : UITableViewCell<UICollectionViewDelegate,UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property NSMutableDictionary * productDic;
@property int  previewNumber;
@property UIImage *  previewImage;
-(void)genrateSlideView;
@property (strong,nonatomic) id<MaintainState> delegate;
@property (weak, nonatomic) IBOutlet UIPageControl *pageController;
@end

NS_ASSUME_NONNULL_END
