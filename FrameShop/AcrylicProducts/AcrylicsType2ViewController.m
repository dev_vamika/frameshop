//
//  AcrylicsType2ViewController.m
//  FrameShop
//
//  Created by Saif on 30/07/21.
//  Copyright © 2021 Mayank Barnwal. All rights reserved.
//

#import "AcrylicsType2ViewController.h"
#import "AcrylicSliderViewCell.h"
#import "AppDelegate.h"
//#import "Global.h"
@interface AcrylicsType2ViewController (){
    float imageWidth,imageHeight;
    NSMutableDictionary * cellDic;
    NSMutableDictionary *productDic;
    BOOL isLoaded,isMenu;
    NSString *payableAmount;
    UIColor *btnColor;
    int savedIndex;
    UIImage *prvImg;
}

@end

@implementation AcrylicsType2ViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
//    _preViewImage.image = image;
//    _preViewImage.hidden = true;
    _loader.hidden=YES;
    savedIndex = 0;
    cellDic = [[NSMutableDictionary alloc] init];
    
    
    
    [Global roundCorner:self.addToCartButton roundValue:self.addToCartButton.frame.size.height/2 borderWidth:0.0 borderColor:[UIColor cyanColor]];
    
    
    btnColor=_addToCartButton.backgroundColor;
    _addToCartButton.backgroundColor=[UIColor lightGrayColor];
    _addToCartButton.enabled=NO;
    
    
    [_loaderActivity startAnimating];
    _loaderActivity.hidden = false;
    
    UISwipeGestureRecognizer * swipeleft=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeleft:)];
    swipeleft.direction=UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeleft];
   
    [Global roundCorner:_cartCountLbl roundValue:_cartCountLbl.frame.size.height/2 borderWidth:2 borderColor:[UIColor whiteColor]];
   
    int count=[[[NSUserDefaults standardUserDefaults] valueForKey:CART_COUNT] intValue];
    if (count>9) {
        _cartCountLbl.text=@"9+";
    }
    else{
        _cartCountLbl.text=[NSString stringWithFormat:@"%d",[[[NSUserDefaults standardUserDefaults] valueForKey:CART_COUNT] intValue]];
    }
    _cartCountLbl.hidden=[_cartCountLbl.text isEqualToString:@"0"];
    
    
    NSMutableArray *arr=[[NSMutableArray alloc] init];
    arr=[APP_DELEGATE.db getDataForField:USER_PRODUCT where:nil];
    if (arr.count!=0) {
        productDic=arr[0];
        
        NSArray * arrD = [APP_DELEGATE.db getDataForField:PRODUCT_TABLE where:[NSString stringWithFormat:@"id='%@'",arr[0][@"id"]] ];
        
        if (arrD.count != 0) {
            productDic[@"sku"]= arrD[0][@"sku"];
        }
        
    }
    
    if ([productDic[@"sku"] isEqualToString:BLOCK_MOUNT_SKU]) {
        
        self.title = @"Wood Prints";
        
    }
    else if ([productDic[@"sku"] isEqualToString:CANVAS_PRINTING_SKU]){
        
        self.title = @"Wrap Style";
    }else if ([productDic[@"sku"] isEqualToString:ACRYLIC_PHOTOBOOK]){
        
        self.title = @"Acrylic Photo Book";
    }
    else{
        self.title = @"Acrylic Glass Prints";
    }
    
    
   // _preViewImage.image = img;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        CGRect rect = self->_superMainView.bounds;
        //CGRectMake(0, 0, _superMainView.frame.size.width, _superMainView.frame.size.height);
       // CGFloat scale = 8.0;

//        UIGraphicsBeginImageContextWithOptions(rect.size, false, scale);
        //IGraphicsBeginImageContext(_superMainView.frame.size);
        
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 2.0);
           // [_superMainView.layer renderInContext:UIGraphicsGetCurrentContext()];
       // CGRect st = CGRectMake(0, 0, rect.size.width, rect.size.height);
//        CGContextRef context = UIGraphicsGetCurrentContext();
//        [self.superMainView drawLayer:self.superMainView.layer inContext:context];
        [self->_superMainView drawViewHierarchyInRect:rect afterScreenUpdates:YES];
        UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
       // return img;
        self->prvImg = img;
        self->_mainView.hidden = true;
        self->_superMainView.hidden = true;
        [Global saveImagesInDocumentDirectory:UIImageJPEGRepresentation(img, 2.0) ImageId:@"acrylicImage"];
        [self->_tableView reloadData];
        self->_topimage.image = nil;
        self->_bottomImage.image = nil;
        self->_centerImage.image = nil;
        self->_rightImage.image = nil;
        self->_leftImage.image = nil;
        [self->_superMainView removeFromSuperview];
         
    });
    
       

    [self genrateSlideView];
       // [_loaderActivity stopAnimating];
       // _loaderActivity.hidden = true;
      //  [_tableView reloadData];
}

//- (IBAction)savePrev:(id)sender {
//    CGRect rect = [self.superMainView bounds];
//
//    UIGraphicsBeginImageContext(rect.size);
//    
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    //[self.superMainView.layer renderInContext:context];
//    [_superMainView drawViewHierarchyInRect:_superMainView.bounds afterScreenUpdates:YES];
//    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//   // return img;
//prvImg = img;
//        _mainView.hidden = true;
//        _superMainView.hidden = true;
//    [_tableView reloadData];
//}


-(void)genrateSlideView{
        
    
        dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(q, ^{
            
            
            UIImage *img =  [Global getImageFromDoucumentWithName:@"frame"];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if(img!=nil){
                  
                    if ([self->productDic[@"sku"] isEqualToString:BLOCK_MOUNT_SKU]) {
                        self->productDic[@"normalImage"]  = [Global acrylic2DViewWithGradientWhereImage:img shouldGradient:NO]  ;
                        self->productDic[@"acrylicImage"] = [Global convertImageTo3dWhereBacking:CORE_BLACK andImageView:nil andImage:self->productDic[@"normalImage"] withGradient:NO andCoreColor:[UIColor blackColor] sku:self->productDic[@"sku"] andWidth:[self->productDic[@"width_tf"] floatValue]];
                        
                    }
                    else if ([self->productDic[@"sku"] isEqualToString:CANVAS_PRINTING_SKU]){
                        self->productDic[@"normalImage"] = [Global acrylic2DViewWithGradientWhereImage:img shouldGradient:NO]  ;
                        self->productDic[@"acrylicImage"] = [Global getImageFromDoucumentWithName:@"preview"];
                        //NSMutableArray *arr=[self->db getDataForField:USER_PRODUCT where:nil];
                        // if (arr.count!=0) {
                        if ([self->productDic[@"core"] isEqualToString:@"BLACK"]) {
                            //COLOR
                           // self->acrylicImg = [Global convertImageTo3dWhereBacking:CORE_BLACK andImageView:nil andImage:self->normalImg withGradient:NO andCoreColor:[Global colorFromHexString:self->_productDic[@"color"]]];
                            
                        }
                        else if ([self->productDic[@"core"] isEqualToString:@"WHITE"]) {
                            //Galarry
                           // self->acrylicImg = [Global convertImageTo3dWhereBacking:CORE_WHITE andImageView:nil andImage:self->normalImg withGradient:NO andCoreColor:[UIColor clearColor]];
                        }
                        else{
                            //mirror
                           // self->acrylicImg = [Global convertImageTo3dWhereBacking:CORE_MIRROR andImageView:nil andImage:self->normalImg withGradient:NO andCoreColor:[UIColor clearColor]];
                        }
                        //  }
                    }
                    else {
                        self->productDic[@"normalImage"] = [Global acrylic2DViewWithGradientWhereImage:img shouldGradient:YES]  ;
                        
                        
                        self->productDic[@"acrylicImage"] = [Global convertImageTo3dWhereBacking:CORE_BLACK andImageView:nil andImage:self->productDic[@"normalImage"] withGradient:YES andCoreColor:nil sku:self->productDic[@"sku"] andWidth:[self->productDic[@"width_tf"] floatValue]];
                        
                      
                        
                    }
                    [Global saveImagesInDocumentDirectory:UIImagePNGRepresentation(self->productDic[@"normalImage"]) ImageId:@"preview"];
                    [Global saveImagesInDocumentDirectory:UIImagePNGRepresentation(self->productDic[@"acrylicImage"]) ImageId:@"frame_preview"];
                    
                    
                    [self->_loaderActivity stopAnimating];
                    self->_loaderActivity.hidden = true;
                    [self->_tableView setAlwaysBounceVertical:false];
                    [self->_tableView setBouncesZoom:false];
                    [self->_tableView setBounces:false];
                    [self->_tableView reloadData];
                    
                    
                   // [self->_collectionView reloadData];
                    
                }
                
                
                
            });
        });
    }


    


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    UIImage *mg = [Global getImageFromDoucumentWithName:@"frame"];
    //[Global getMidiumSizeImage:[Global getImageFromDoucumentWithName:@"origImage"] imageSize:_centerImage.frame.size.width*1.5];
    //[Global getImageFromDoucumentWithName:@"frame"];
    
 //   UIImage *mg =  [Global acrylic2DViewWithGradientWhereImage:mgi shouldGradient:YES];
    [Global makeAcrylicPhotoBook:_topimage mainView: _mainView bottomImage: _bottomImage leftImage: _leftImage rightImage: _rightImage centerImage: _centerImage mainImageHeightConstant: _mainImageHeightConstant mainImageWidthtConstant: _mainImageWidthConstant imageToUse:mg]; 
}

-(void)updateAddToCartFrame{
    self.view.userInteractionEnabled = NO;
    _loader.hidden=NO;
    [_loader startAnimating];
    [_addToCartButton setTitle:@"" forState:UIControlStateNormal];
    [UIView animateWithDuration:0.5 animations:^{
        self->_addTocartWidth.constant = (self.addToCartButton.frame.size.height*2)-self.view.frame.size.width;
    }];
}
-(void)removeAddToCartLoader{
    self.view.userInteractionEnabled = YES;
    _loader.hidden=YES;
    [_loader stopAnimating];
    [_addToCartButton setTitle:@"Add To Cart" forState:UIControlStateNormal];
    [UIView animateWithDuration:0.5 animations:^{
        self->_addTocartWidth.constant = 0.0;
    }];
}

-(void)swipeleft:(UISwipeGestureRecognizer*)gestureRecognizer
{
    if ([gestureRecognizer locationInView:self.view].x>self.view.bounds.size.width*0.80) {
        isMenu=YES;
        [Global showSideMenu];
    }
}


-(void)signUpUser{
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
    dic[@"name"]=[[UIDevice currentDevice] name];
    dic[@"email"]=[NSString stringWithFormat:@"%@@frameshop.com.au",[Global getDeviceId]];
    dic[@"password"]=@"qwxcjeub32ne";
    dic[@"c_password"]=@"qwxcjeub32ne";
    dic[@"url"]=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,REGISTER_USER];
    [api sendPostRequst:dic];
}

-(void)logInUser{
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
    dic[@"email"]=[NSString stringWithFormat:@"%@@frameshop.com.au",[Global getDeviceId]];
    dic[@"password"]=@"qwxcjeub32ne";
    dic[@"url"]=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,LOGIN_USER];
    [api sendPostRequst:dic];
}


-(void)sendDataToServerData{
    [self updateAddToCartFrame];
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    NSString *urlString=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,ADD_PRODUCT_CART];
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
    dic[@"url"]=urlString;
    
    NSMutableArray *arr=[[NSMutableArray alloc] init];
    arr=[APP_DELEGATE.db getDataForField:USERTABLE where:nil];
    
    NSMutableArray *tempArr=[[NSMutableArray alloc] init];
    NSMutableArray *proArr=[[NSMutableArray alloc] init];
    tempArr=[APP_DELEGATE.db getDataForField:USER_PRODUCT where:nil];
    proArr=[APP_DELEGATE.db getDataForField:PRODUCT_TABLE where:[NSString stringWithFormat:@"id='%@'",tempArr[0][@"id"]]];
    
    
    if (arr.count!=0) {
        dic[@"token"]=arr[0][@"token"];
        dic[@"linked_id"] =productDic[@"id"];
        dic[@"order_type"]=@"product";
        dic[@"payable_amount"]=payableAmount ;
        
        NSMutableDictionary *cartOrderDic=[[NSMutableDictionary alloc] init];
        
        cartOrderDic[@"type"]        =productDic[@"sku"];
        cartOrderDic[@"width"]       =productDic[@"width_tf"];
        cartOrderDic[@"height"]       =productDic[@"height_tf"];
        
        if ([productDic[@"sku"] isEqualToString:BLOCK_MOUNT_SKU]) {
            cartOrderDic[@"backing"]        =@"mdf";
            
            if (([productDic[@"width_tf"] floatValue]>30) ||([productDic[@"height_tf"] floatValue]>30) )  {
                cartOrderDic[@"hanger"] =@"HANGER_SUPPORT_FRAME";
            }else{
                cartOrderDic[@"hanger"] =@"HANGER_SAWTOOTH";
            }
        }
        else if ([productDic[@"sku"] isEqualToString:CANVAS_PRINTING_SKU]){
            
            cartOrderDic[@"printing"]       =@YES;
            cartOrderDic[@"stretching"]       =@NO;
            cartOrderDic[@"edge"]       =productDic[@"core"];
            if ([productDic[@"core"] isEqualToString:@"WHITE"]) {
                cartOrderDic[@"stretching"]       =@YES;
                
            }
            
            
            cartOrderDic[@"color"]       =productDic[@"color"];
            
        }if ([productDic[@"sku"] isEqualToString:ACRYLIC_PHOTOBOOK]){
            
//            cartOrderDic[@"printing"]       =@YES;
//            cartOrderDic[@"stretching"]       =@NO;
//            cartOrderDic[@"edge"]       =productDic[@"core"];
//            if ([productDic[@"core"] isEqualToString:@"WHITE"]) {
//                cartOrderDic[@"stretching"]       =@YES;
//
//            }
//
//
//            cartOrderDic[@"color"]       =productDic[@"color"];
            return;
        }
        
        else{
            cartOrderDic[@"category_id"] =@"1214";
            cartOrderDic[@"depth"]       =@"5";
            cartOrderDic[@"image"]       =NULL;
            cartOrderDic[@"bolts"]       =[NSNumber numberWithBool:false];
            cartOrderDic[@"bolt_colour"] =NULL;
            if (([productDic[@"width_tf"] floatValue]>30) ||([productDic[@"height_tf"] floatValue]>30) )  {
                cartOrderDic[@"stand_back"] =[NSNumber numberWithBool:false];
            }else{
                cartOrderDic[@"stand_back"] =[NSNumber numberWithBool:true];
            }
        }
        cartOrderDic[@"product_name"] = proArr[0][@"name"];
        dic[@"cart_order_info"] =cartOrderDic;
        
        
        [api sendPostRequst:dic];
    }
    
}

-(void)savePreviewImage{
    
    //    [Global saveImagesInDocumentDirectory:UIImageJPEGRepresentation(acrylicImg, 1.0) ImageId:@"preview"];
    //    [Global saveImagesInDocumentDirectory:UIImageJPEGRepresentation(acrylicImg, 1.0) ImageId:@"frame_preview"];
    
    [self sendDataToServerData];
}




- (IBAction)onclickCart:(id)sender {
    [Global callMenuViewWithCode:@"0"];
}
- (IBAction)onMenuClicked:(id)sender {
    isMenu=YES;
    [Global showSideMenu];
}

#pragma mark - Table view data source
-(void)indexChanged:(int *)val{
    NSLog(@"abc");
    savedIndex = val;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return productDic !=nil ? 2 :0;
    
    
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    if (indexPath.row == 0) {
        if (cellDic[@"preview"]!=nil) {
            return cellDic[@"preview"];
        }
        else{
        static NSString *simpleTableIdentifier = @"AcrylicPreviewTableViewCell";
        AcrylicPreviewTableViewCell *cell = (AcrylicPreviewTableViewCell *)[_tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
            
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"AcrylicPreviewTableViewCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
           // cell.previewNumber = savedIndex;
            //cell.delegate = self;
            cell.previewImage = prvImg;
        cell.productDic = productDic;
        [cell genrateSlideView];
           // [cell.collectionView  scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:savedIndex inSection:0] atScrollPosition:UICollectionViewScrollPositionNone animated:YES];
//            if (productDic[@"normalImage"]!=nil) {
//                cellDic[@"preview"] = cell;
//            }
            
            cell.layoutSubviews;
        return cell;
        }
        
    }
    else{
        
        
        if ([self->productDic[@"sku"] isEqualToString:BLOCK_MOUNT_SKU] || [self->productDic[@"sku"] isEqualToString:ACRYLIC_PHOTOBOOK]) {
            
            static NSString *simpleTableIdentifier = @"BlockMountTableViewCell";
            BlockMountTableViewCell *cell = (BlockMountTableViewCell *)[_tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
            
            if (cell == nil)
            {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"BlockMountTableViewCell" owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
            
            cell.productDic = productDic;
            cell.delegate = self;
            [cell setUpView];
            
            
            return cell;
        }
        else if ([self->productDic[@"sku"] isEqualToString:ACRYLIC_SKU] ) {
            
            static NSString *simpleTableIdentifier = @"AcrylicDetailTableViewCell";
            AcrylicDetailTableViewCell *cell = (AcrylicDetailTableViewCell *)[_tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
            
            if (cell == nil)
            {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"AcrylicDetailTableViewCell" owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
            
            cell.productDic = productDic;
            cell.delegate = self;
            [cell setUpView];
            
            
            return cell;
            
        }
        else{
            
            static NSString *simpleTableIdentifier = @"CanvasTableViewCell";
            CanvasTableViewCell *cell = (CanvasTableViewCell *)[_tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
            
            if (cell == nil)
            {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CanvasTableViewCell" owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
            
            cell.productDic = productDic;
            cell.delegate = self;
            [cell setUpView];
            
            
            return cell;
            
        }
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //can be use in future
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return UIScreen.mainScreen.bounds.size.width + 75;
    }
    else{
        if ([self->productDic[@"sku"] isEqualToString:BLOCK_MOUNT_SKU])
            return  500;
        return  460;
    }
    
    
}







//cartzoom


- (IBAction)onClickInfo:(id)sender {
}
- (IBAction)onClickAddToCart:(id)sender {
    [self savePreviewImage];
    
}

#pragma mark - Detail delegates
-(void)didGetPrice:(NSDictionary *)infoDic{
    payableAmount=[NSString stringWithFormat:@"%@",infoDic[@"price"]] ;
    _addToCartButton.backgroundColor=btnColor;
    _addToCartButton.enabled=true;
}

#pragma mark - segue delegates
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"upload"]) {
        
        UploadImageViewController *albumContentsViewController = [segue destinationViewController];
        
        albumContentsViewController.delegate=self;
        
        
    }
    
}
#pragma mark - Upload delegates

-(void)didFinishUploading{
    //[self performSelector:@selector(subscribe) withObject:self afterDelay:3.0 ];
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:FINISH_UPLOADING object:self userInfo:nil];
    [self dismissViewControllerAnimated:NO completion:nil];
}
-(void)didFailUploading{
    
}

#pragma mark -Pop api Delegates

-(void)connectionError{
    isLoaded=YES;
    // [_actiVityBar removeFromSuperview];
}
-(void)requestFailedSessionExpired:(NSDictionary *)dataPack{
    isLoaded=YES;
    //[_actiVityBar removeFromSuperview];
}
-(void)requestFailedServerError:(NSDictionary *)dataPack{
    isLoaded=YES;
    // [_actiVityBar removeFromSuperview];
}
-(void)requestFailedServiceUnavailableDueToMaintenance:(NSDictionary *)dataPack{
    isLoaded=YES;
    //  [_actiVityBar removeFromSuperview];
    [Global showSimpleAlert:self andTitle:FRAMESHOP andMessage:dataPack[@"message"]];
}
-(void)requestFailedMethodNotFound:(NSDictionary *)dataPack{
    isLoaded=YES;
    // [_actiVityBar removeFromSuperview];
}
-(void)requestFailedNotFound:(NSDictionary *)dataPack{
    isLoaded=YES;
    // [_actiVityBar removeFromSuperview];
}
-(void)requestFailedUnauthorised:(NSDictionary *)dataPack{
    isLoaded=YES;
    // [_actiVityBar removeFromSuperview];
    [self logInUser];
}
-(void)requestFailedBadRequest:(NSDictionary *)dataPack{
    isLoaded=YES;
    // [_actiVityBar removeFromSuperview];
}
-(void)requestSucceededWithData:(NSDictionary *)dataPack{
    
    
    if ([dataPack[@"url"] isEqualToString:LOGIN_USER]) {
        
        [APP_DELEGATE.db delTableWithTableName:USERTABLE];
        NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
        
        dic[@"id"]              =[NSString stringWithFormat:@"%@",dataPack[@"data"][@"id"]];
        dic[@"name"]            =dataPack[@"data"][@"name"];
        dic[@"email"]           =dataPack[@"data"][@"email"];
        dic[@"created_at"]      =dataPack[@"data"][@"created_at"];
        dic[@"updated_at"]      =dataPack[@"data"][@"updated_at"];
        dic[@"token"]           =dataPack[@"data"][@"access_token"];
        
        [APP_DELEGATE.db insertIntoTable:dic table_name:USERTABLE];
        [self genrateSlideView];
    }
    
    else if ([dataPack[@"url"] isEqualToString:REGISTER_USER]){
        
        NSMutableDictionary *dic =[[NSMutableDictionary alloc] init];
        dic[@"token"]            =dataPack[@"data"][@"token"];
        dic[@"id"]               =[NSString stringWithFormat:@"%@",dic[@"id"]];
        
        [APP_DELEGATE.db insertIntoTable:dic table_name:USERTABLE];
        [self genrateSlideView];
    }
    
    else{
        [self removeAddToCartLoader];
        [[NSUserDefaults standardUserDefaults] setValue:dataPack[@"data"][@"cart_order_id"] forKey:CART_ID];
        
        [self performSegueWithIdentifier:@"upload" sender:nil];
        
        
    }
    
    
}
-(void)requestSucceededButActionFailed:(NSDictionary *)dataPack{
    [self removeAddToCartLoader];
    isLoaded=YES;
    //[_actiVityBar removeFromSuperview];
}
-(void)uploadedData:(NSString *)pecentage andByte:(float)bytes{
    
}
-(void)requestSucceededButNoDataFound:(NSDictionary *)dataPack{
    [self removeAddToCartLoader];
    isLoaded=YES;
    // [_actiVityBar removeFromSuperview];
}
-(void)requestFailedDueToServerIsuueWithError:(NSString*)errorDes{
    isLoaded=YES;
    // [_actiVityBar removeFromSuperview];
}

@end
