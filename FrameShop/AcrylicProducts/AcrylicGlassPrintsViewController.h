//
//  AcrylicGlassPrintsViewController.h
//  FrameShop
//
//  Created by Saurabh Srivastav on 08/02/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Global.h"
#import "DBManager.h"
#import "PopApi.h"
#import "UploadImageViewController.h"
#import "AcrylicPreviewTableViewCell.h"
#import "AcrylicDetailTableViewCell.h"
#import "BlockMountTableViewCell.h"
#import "CanvasTableViewCell.h"
NS_ASSUME_NONNULL_BEGIN

@interface AcrylicGlassPrintsViewController : UIViewController<PopApiDelegate,UploadImageViewControllerDelegate,AcrylicDetailTableViewCellDelegate,BlockMountTableViewCellDelegate,CanvasTableViewCellDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;



@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loaderActivity;

- (IBAction)onClickInfo:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *addToCartButton;

- (IBAction)onClickAddToCart:(id)sender;




@property (weak, nonatomic) IBOutlet UILabel *cartCountLbl;
@property (weak, nonatomic) IBOutlet UIButton *menuButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *addTocartWidth;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loader;

- (IBAction)onclickCart:(id)sender;
- (IBAction)onMenuClicked:(id)sender;
@end

NS_ASSUME_NONNULL_END
