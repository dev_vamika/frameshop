//
//  ShippingViewController.h
//  FrameShop
//
//  Created by Saurabh Srivastav on 03/03/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Global.h"
#import "PopApi.h"
#import "DBManager.h"
#import "ShippingTableViewCell.h"
#import "LoaderTableViewCell.h"
#import "AddAddressViewController.h"
#import <WebKit/WebKit.h>
#import "WarningSubView.h"
#import "summarySectionView.h"
NS_ASSUME_NONNULL_BEGIN
@class ShippingViewController ;
@protocol ShippingViewControllerDelegate <NSObject>
@optional
-(void)didSelectProceedAction:(int)pageIndex;
-(void)shouldEnableScrolling:(BOOL)isEnable;
-(void)didSelectAdress:(NSMutableDictionary *)addressDic;
@end
@interface ShippingViewController : UIViewController<PopApiDelegate,ShippingTableViewCellDelegate,AddAddressViewControllerDelegate,UITextFieldDelegate>
@property(strong,nonatomic)id<ShippingViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITextField *emailTxt;
@property (weak, nonatomic) IBOutlet UIButton *procdBtn;
@property (weak, nonatomic) IBOutlet UITextField *addressTxt;
@property (weak, nonatomic) IBOutlet UIButton *addEmailBtn;
- (IBAction)proceedAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *addAddressBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *saperatorHeight;
@property int index;
//@property NSString *addressId;
@property NSMutableDictionary *addressDic;
@end

NS_ASSUME_NONNULL_END
