//
//  AddAddressViewController.m
//  FrameShop
//
//  Created by Saurabh Srivastav on 18/03/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import "AddAddressViewController.h"
#import "Global.h"
#import "DBManager.h"
#import "IQKeyboardManager.h"
@interface AddAddressViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>{
    DbManager *db;
    NSMutableArray * dataArray;
    BOOL isStreetAd;
    NSTimer *timer;
    NSTimeInterval timeStamp;
    CGSize keyboardSize;
    float addBtnConstent;
}
@end

@implementation AddAddressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[IQKeyboardManager sharedManager] setShouldShowToolbarPlaceholder:false];
    [IQKeyboardManager sharedManager].previousNextDisplayMode = IQPreviousNextDisplayModeDefault;
    
    self.fullNameTxt.delegate = self;
    self.comTxt.delegate = self;
    self.floorTxt.delegate = self;
    self.streetTxt.delegate = self;
    self.subTxt.delegate = self;
    self.statTxt.delegate = self;
    self.postTxt.delegate = self;
    self.mobTxt.delegate = self;
    self.lastNameTxt.delegate = self;
    
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.hidden = YES;
    
    
    [Global addShadow:_tableView];
    dataArray=[[NSMutableArray alloc] init];
    _loader.hidden = YES;
    db=[[DbManager alloc] init];
    
    self.toolbar.hidden = YES;
    self.fullNameTxt.inputAccessoryView = self.toolbar;
    self.lastNameTxt.inputAccessoryView =  self.toolbar;
    self.streetTxt.inputAccessoryView =  self.toolbar;
    self.mobTxt.inputAccessoryView =  self.toolbar;
    self.subTxt.inputAccessoryView =  self.toolbar;
    self.comTxt.inputAccessoryView =  self.toolbar;
    
    _warningTextSub.hidden=YES;
    _warningTextStreet.hidden=YES;
    _streetLoader.hidden=YES;
    _subLoader.hidden=YES;
    addBtnConstent=_addAddressWidth.constant;
    
    //    [Global roundCorner:_backView roundValue:15 borderWidth:0 borderColor:nil];
    //    [self addshadow:_backView];

    
    [Global roundCorner:_addBtn roundValue:_addBtn.frame.size.height/2 borderWidth:0 borderColor:nil];
    _sampleView.hidden = YES;
    
    [Global roundCorner:_fullNameTxt roundValue:ROUND_CORNER borderWidth:BORDER_WIDTH borderColor:APP_BLUE_COLOR];
    [Global roundCorner:_lastNameTxt roundValue:ROUND_CORNER borderWidth:BORDER_WIDTH borderColor:APP_BLUE_COLOR];
    [Global roundCorner:_mobTxt roundValue:ROUND_CORNER borderWidth:BORDER_WIDTH borderColor:APP_BLUE_COLOR];
    [Global roundCorner:_comTxt roundValue:ROUND_CORNER borderWidth:BORDER_WIDTH borderColor:APP_BLUE_COLOR];
    [Global roundCorner:_streetTxt roundValue:ROUND_CORNER borderWidth:BORDER_WIDTH borderColor:APP_BLUE_COLOR];
    [Global roundCorner:_subTxt roundValue:ROUND_CORNER borderWidth:BORDER_WIDTH borderColor:APP_BLUE_COLOR];
    [Global roundCorner:_countryTxt roundValue:ROUND_CORNER borderWidth:BORDER_WIDTH borderColor:APP_BLUE_COLOR];
    
    [Global Padding:_fullNameTxt];
    [Global Padding:_lastNameTxt];
    [Global Padding:_mobTxt];
    [Global Padding:_comTxt];
    [Global Padding:_streetTxt];
    [Global Padding:_subTxt];
    [Global Padding:_countryTxt];
    if (_addressInfo!=nil) {
        _type=@"edit";
        _fullNameTxt.text=[[_addressInfo[@"full_name"] componentsSeparatedByString:@" "] firstObject];
        _lastNameTxt.text=[[_addressInfo[@"full_name"] componentsSeparatedByString:@" "] lastObject];
        _comTxt.text=_addressInfo[@"company_name"];
        _floorTxt.text=[NSString stringWithFormat:@"%@",_addressInfo[@"floor_or_unit"]]  ;
        _mobTxt.text=[NSString stringWithFormat:@"%@",_addressInfo[@"mobile_number"]]  ;
        _postTxt.text=[NSString stringWithFormat:@"%@",_addressInfo[@"postcode"]]  ;
        _statTxt.text=_addressInfo[@"state"];
        _streetTxt.text=_addressInfo[@"street_address"];
        
        
        _subTxt.text=[NSString stringWithFormat:@"%@, %@ %@",_addressInfo[@"suburbs"],_addressInfo[@"state"],_addressInfo[@"postcode"]];
        
        
        [_addBtn setTitle:@"Save this Address" forState:UIControlStateNormal];
        [self checkValidation:_fullNameTxt];
        [self checkValidation:_lastNameTxt];
        [self checkValidation:_mobTxt];
        [self checkValidation:_streetTxt];
        [self checkValidation:_subTxt];
        if ([Validation isValidText:_comTxt.text]) {
            [self checkValidation:_comTxt];
        }
        
        
    }
    else{
        _addressInfo=[[NSMutableDictionary alloc] init];
    }
    
    
    _countryTxt.hidden=YES;
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    _tableView.frame = CGRectMake(0, 0, 0, 0);
    _tableView.backgroundColor =[UIColor groupTableViewBackgroundColor];
    _countryTxt.text=@"Australia";
    [Validation setValidationMarkToTextField:_countryTxt isCorrect:YES];
    
}
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    //CGPoint location = [recognizer locationInView:[recognizer.view superview]];
    [self.navigationController popViewControllerAnimated:YES];
    //    [self dismissViewControllerAnimated:YES completion:nil];
    //Do stuff here...
}
-(void)updateFrame{
    self.view.userInteractionEnabled = NO;
    _loader.hidden=NO;
    [_loader startAnimating];
    [_addBtn setTitle:@"" forState:UIControlStateNormal];
    [UIView animateWithDuration:0.5 animations:^{
        self->_addAddressWidth.constant = (self.addBtn.frame.size.height*2)-self.backView.frame.size.width;
    }];
}
-(void)removeLoader{
    self.view.userInteractionEnabled = YES;
    _loader.hidden=YES;
    [_loader stopAnimating];
    [_addBtn setTitle:@"Add this Address" forState:UIControlStateNormal];
    [UIView animateWithDuration:0.5 animations:^{
        self->_addAddressWidth.constant = 0.0;
    }];
}
-(void)sendDataToServerData{
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    NSString *urlString=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,ADD_SHIPPING_ADDRESS];
    
    _addressInfo[@"url"]=urlString;
    NSMutableArray *arr=[[NSMutableArray alloc] init];
    arr=[db getDataForField:USERTABLE where:nil];
    NSLog(@"arr==%@",arr);
    if (arr.count!=0) {
        _addressInfo[@"token"]=arr[0][@"token"];
        
        [api sendPostRequst:_addressInfo];
    }
    
    
}
-(void)editAddress{
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    NSString *urlString=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,EDIT_SHIPPING_ADDRESS];
    
    _addressInfo[@"url"]=urlString;
    NSMutableArray *arr=[[NSMutableArray alloc] init];
    arr=[db getDataForField:USERTABLE where:nil];
    
    if (arr.count!=0) {
        _addressInfo[@"token"]=arr[0][@"token"];
        //[api sendGetRequst:urlString andToken:arr[0][@"token"]];
        [api sendPostRequst:_addressInfo];
    }
    
    
}

-(void)signUpUser{
    
    
    
    
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
    dic[@"name"]=[[UIDevice currentDevice] name];
    dic[@"email"]=[NSString stringWithFormat:@"%@@frameshop.com.au",[Global getDeviceId]];
    dic[@"password"]=@"qwxcjeub32ne";
    dic[@"c_password"]=@"qwxcjeub32ne";
    dic[@"url"]=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,REGISTER_USER];
    [api sendPostRequst:dic];
    
}
-(void)logInUser{
    
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
    
    dic[@"email"]=[NSString stringWithFormat:@"%@@frameshop.com.au",[Global getDeviceId]];
    dic[@"password"]=@"qwxcjeub32ne";
    
    dic[@"url"]=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,LOGIN_USER];
    [api sendPostRequst:dic];
}


-(void)searchAddress:(NSString *)addressString isStreetAddress:(BOOL)isStreet{
    [dataArray removeAllObjects];
    [_tableView reloadData];
    _tableView.hidden=YES;
    
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    NSString *urlString;
    if (isStreet) {
        isStreetAd=YES;
        urlString=SEARCH_SHIPPING_ADDRESS;
        _streetLoader.hidden=false;
        [_streetLoader startAnimating];
        
    }
    else{
        isStreetAd=NO;
        urlString=SEARCH_LOCAL;
        _subLoader.hidden=false;
        [_subLoader startAnimating];
    }
    
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
    dic[@"url"]=urlString;
    
    NSMutableArray *arr=[[NSMutableArray alloc] init];
    NSMutableDictionary *tem=[[NSMutableDictionary alloc] init];
    tem[@"fullAddress"]=addressString;
    arr[arr.count]=tem;
    
    dic[@"payload"]=arr;
    dic[@"sourceOfTruth"]=@"AUPAF";
    
    [api addressSugPostApi:dic];
    
    
    
    
}




- (IBAction)addAction:(id)sender {
    
    if (![_type isEqualToString:@"edit"]) {
        _addressInfo[@"email"]=[[NSUserDefaults standardUserDefaults] valueForKey:EMAIL];
    }
    if (![Validation isValidTextName:_fullNameTxt.text]){
        _fullNameTxt.text = @"";
        [Global showSimpleAlert:self andTitle:FRAMESHOP andMessage:@"Please fill First Name"];
        return;
    }
    else if (![Validation isValidTextName:_lastNameTxt.text]){
        _lastNameTxt.text = @"";
        [Global showSimpleAlert:self andTitle:FRAMESHOP andMessage:@"Please fill Last Name"];
        return;
    }
    
    else  if (![Validation isValidText:_streetTxt.text]){
        [Global showSimpleAlert:self andTitle:FRAMESHOP andMessage:@"Please enter your address"];
        return;
    }
    
    else if (![Validation isValidText:_addressInfo[@"suburbs"]]){
        [Global showSimpleAlert:self andTitle:FRAMESHOP andMessage:@"Please select address from address list"];
        return;
    }
    
    else  if (![Validation isValidText:_addressInfo[@"state"]] ){
        [Global showSimpleAlert:self andTitle:FRAMESHOP andMessage:@"Please select address from address list"];
        return;
    }
    else if (![Validation isValidPostal: _addressInfo[@"postcode"]]){
        [Global showSimpleAlert:self andTitle:FRAMESHOP andMessage:@"Please select address from address list"];
        return;
    }
    else if (![Validation isValidText:_mobTxt.text]){
        [Global showSimpleAlert:self andTitle:FRAMESHOP andMessage:@"Please fill Valid mobile number"];
        return;
    }
    
    //    else if (![Validation isValidEmail:_addressInfo[@"email"]] ){
    //        [Global showSimpleAlert:self andTitle:FRAMESHOP andMessage:@"Please fill Valid email"];
    //        return;
    //    }
    
    else{
        
        _addressInfo[@"full_name"]=[Global makeFirstLatterUpperofString:[NSString stringWithFormat:@"%@ %@",_fullNameTxt.text,_lastNameTxt.text]];
        _addressInfo[@"company_name"]=_comTxt.text;
        _addressInfo[@"mobile_number"]=_mobTxt.text;
        if ([_type isEqualToString:@"edit"]) {
            [self updateFrame];
            [self editAddress];
        }
        else
            [self updateFrame];
        [self sendDataToServerData];
        
    }
    
    
    
    
    
    
    
    
}

- (IBAction)closeAction:(id)sender {
    
    // [self saveAddressToLocal];
    //    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) addshadow:(UIView *)view{
    view.layer.masksToBounds = NO;
    view.layer.shadowColor = [[UIColor lightGrayColor] CGColor];
    view.layer.shadowOffset = CGSizeMake(0,0);
    view.layer.shadowOpacity = 4;
    view.layer.shadowRadius = 6;
    
}

#pragma mark -Text Field Delegates




- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    self.toolbar.hidden = NO;
    if (textField==_countryTxt) {
        return NO;
    }
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
   
}






- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    
    _tableView.hidden = NO;
    _tableView.frame = CGRectMake(0, 0, 0,0);
    float cellHeight=0.0;
    int footerHeihgt = 40;
    cellHeight= MIN((dataArray.count *30.0)+footerHeihgt , 150);
    
    if (textField == _streetTxt || textField == _subTxt) {
        
        
        
        
        _warningTextStreet.hidden=true;
        _warningTextSub.hidden=YES;
        _subLoader.hidden=true;
        _streetLoader.hidden=true;
        [_streetLoader stopAnimating];
        [_subLoader stopAnimating];
        _tableView.hidden=true;
        if (textField == _subTxt) {
            _tableView.frame = CGRectMake(_subView.frame.origin.x, _subView.frame.origin.y -cellHeight, _subView.frame.size.width,cellHeight);
            
            _addressInfo[@"suburbs"]=@"";
            
        }
        else{
            _tableView.frame = CGRectMake(_streetView.frame.origin.x, _streetView.frame.origin.y -cellHeight, _streetView.frame.size.width,cellHeight);
            _addressInfo[@"street_address"]= _streetTxt.text;
        }
        
        
        
        if (textField.text.length>2) {
            
            _warningTextStreet.hidden=true;
            _warningTextSub.hidden=YES;
            
            [self searchAddress:[NSString stringWithFormat:@"%@",textField.text] isStreetAddress:(textField==_streetTxt)];
        }
        else{
            _tableView.hidden=true;
            ( textField == _streetTxt)?(_warningTextStreet.hidden=false):(_warningTextSub.hidden=false);
        }
        
    }
    
    else if (textField == _fullNameTxt) {
        [_fullNameTxt resignFirstResponder];
        [self.lastNameTxt becomeFirstResponder];
        
    }
    else if (textField == _lastNameTxt) {
        [_lastNameTxt resignFirstResponder];
        [self.mobTxt becomeFirstResponder];
        
    }
    else if (textField == _mobTxt) {
        [_mobTxt resignFirstResponder];
        [self.comTxt becomeFirstResponder];
        
    }
    else if (textField == _comTxt) {
        [_comTxt resignFirstResponder];
        [self.streetTxt becomeFirstResponder];
        
    }
    return YES;
}


- (void)textFieldDidEndEditing:(UITextField *)textField{

    [self checkValidation:textField];
    
}
- (void)textFieldDidChangeSelection:(UITextField *)textField{
    if ([textField.text isEqualToString:@""]) {
        _tableView.hidden = YES;
        _tableView.frame = CGRectMake(0, 0, 0,0);
    }
    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField isW:(BOOL)value{
    _tableView.hidden = YES;
    _tableView.frame = CGRectMake(0, 0, 0,0);
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    _tableView.hidden = NO;
    _tableView.frame = CGRectMake(0, 0, 0,0);
    float cellHeight=0.0;
    int footerHeihgt = 40;
    cellHeight= MIN((dataArray.count *30.0)+footerHeihgt , 150);
   // NSString * searchStr = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (textField == _streetTxt || textField == _subTxt) {
        _warningTextStreet.hidden=true;
        _warningTextSub.hidden=YES;
        _subLoader.hidden=true;
        _streetLoader.hidden=true;
        [_streetLoader stopAnimating];
        [_subLoader stopAnimating];
        _tableView.hidden=true;
        if (textField == _subTxt) {
            _tableView.frame = CGRectMake(_subView.frame.origin.x, _subView.frame.origin.y -cellHeight, _subView.frame.size.width,cellHeight);
            
            _addressInfo[@"suburbs"]=@"";
            
        }
        else{
            _tableView.frame = CGRectMake(_streetView.frame.origin.x, _streetView.frame.origin.y -cellHeight, _streetView.frame.size.width,cellHeight);
            _addressInfo[@"street_address"]=_streetTxt.text;
        }
        
        NSDictionary *dic = @{@"textField" : textField};
        
        [timer invalidate];
        
        if (timeStamp==0) {
            timeStamp = [[NSDate date] timeIntervalSince1970];
        }
        if (![timer isValid]) {
            timer= [NSTimer scheduledTimerWithTimeInterval: 0.5
                                                    target: self
                                                  selector: @selector(handleTimer:)
                                                  userInfo: dic
                                                   repeats: YES];
        }
        
        
        // NSTimeInterval is defined as double
        
        
        
        
        
        
    }
    
    return YES;
    
}

-(void)handleTimer:(NSTimer *)timer{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeStamp];
    
    NSDictionary *dict = [timer userInfo];
    NSTimeInterval difference = [[NSDate date] timeIntervalSinceDate:date];
   
    
    if (difference>1) {
        
        if ([(UITextField *)dict[@"textField"] text].length>2) {
            
            _warningTextStreet.hidden=true;
            _warningTextSub.hidden=YES;
            
            //[self searchAddress:[NSString stringWithFormat:@"%@",searchStr] isStreetAddress:(textField==_streetTxt)];
            NSString *searchStr;
            if ((UITextField *)dict[@"textField"]==_streetTxt) {
                
                searchStr=_streetTxt.text;
                
            }
            else{
                
                searchStr=_subTxt.text;
            }
            
            [self searchAddress:[NSString stringWithFormat:@"%@",searchStr] isStreetAddress:((UITextField *)dict[@"textField"]==_streetTxt)];
            [timer invalidate];
            timeStamp=0;
            
        }
        else{
            _tableView.hidden=true;
            ( (UITextField *)dict[@"textField"]  == _streetTxt)?(_warningTextStreet.hidden=false):(_warningTextSub.hidden=false);
        }
        
        
        
        
        
    }
    
}


-(void)checkValidation:(UITextField *)textField{
    
    if (textField==_comTxt) {
        if ([Validation isValidText:_comTxt.text]){
            // [Global roundCorner:textField roundValue:ROUND_CORNER borderWidth:BORDER_WIDTH borderColor:[UIColor greenColor]];
            
            [Validation setValidationMarkToTextField:textField isCorrect:YES];
            
            
        }
        
    }
    
    else  if (textField == _streetTxt)  {
        
        
        
        if (![Validation isValidText:textField.text]){
            //            [Global roundCorner:textField roundValue:ROUND_CORNER borderWidth:BORDER_WIDTH borderColor:[UIColor redColor]];
            [Validation setValidationMarkToTextField:textField isCorrect:NO];
            
        }
        else{
            //            [Global roundCorner:textField roundValue:ROUND_CORNER borderWidth:BORDER_WIDTH borderColor:[UIColor greenColor]];
            [Validation setValidationMarkToTextField:textField isCorrect:YES];
        }
        
        
    }
    
    else  if (textField == _subTxt)  {
        
        if (![Validation isValidText:_addressInfo[@"suburbs"]]){
            // [Global roundCorner:textField roundValue:ROUND_CORNER borderWidth:BORDER_WIDTH borderColor:[UIColor redColor]];
            [Validation setValidationMarkToTextField:textField isCorrect:NO];
        }
        else{
            // [Global roundCorner:textField roundValue:ROUND_CORNER borderWidth:BORDER_WIDTH borderColor:[UIColor greenColor]];
            [Validation setValidationMarkToTextField:textField isCorrect:YES];
        }
        
        
    }
    
    
    else{
        if (textField == _fullNameTxt) {
            NSArray *componentsSeparatedByWhiteSpace = [_fullNameTxt.text componentsSeparatedByString:@" "];
            if([componentsSeparatedByWhiteSpace count] > 1){
                NSLog(@"Found whitespace");
                _fullNameTxt.text = [_fullNameTxt.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            }
        }
       else if (textField == _lastNameTxt) {
           NSArray *componentsSeparatedByWhiteSpace = [_lastNameTxt.text componentsSeparatedByString:@" "];
           if([componentsSeparatedByWhiteSpace count] > 1){
               NSLog(@"Found whitespace");
               
               if (!([_lastNameTxt.text isKindOfClass:[NSNull class]]) )
               
               _lastNameTxt.text = [_lastNameTxt.text stringByReplacingOccurrencesOfString:@" " withString:@""];
           }
       }
        
        
        if ( ![Validation isValidText:textField.text]) {
            //  [Global roundCorner:textField roundValue:ROUND_CORNER borderWidth:BORDER_WIDTH borderColor:[UIColor redColor]];
            [Validation setValidationMarkToTextField:textField isCorrect:NO];
        }
        else{
            //[Global roundCorner:textField roundValue:ROUND_CORNER borderWidth:BORDER_WIDTH borderColor:[UIColor greenColor]];
            [Validation setValidationMarkToTextField:textField isCorrect:YES];
        }
        
    }
}





#pragma mark -Pop api Delegates

-(void)connectionError{
    [Global showSimpleAlert:self andTitle:FRAMESHOP andMessage:@"Please check your connection and try again latter"];
    //[self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)requestFailedSessionExpired:(NSDictionary *)dataPack{
    
}
-(void)requestFailedServerError:(NSDictionary *)dataPack{
    
}
-(void)requestFailedServiceUnavailableDueToMaintenance:(NSDictionary *)dataPack{
    self.view.userInteractionEnabled = YES;
       _loader.hidden=YES;
       [_loader stopAnimating];
       [_addBtn setTitle:@"Add this Address" forState:UIControlStateNormal];
       [UIView animateWithDuration:0.5 animations:^{
           self->_addAddressWidth.constant = self->addBtnConstent;
       }];
    [Global showSimpleAlert:self andTitle:FRAMESHOP andMessage:dataPack[@"message"]];
}
-(void)requestFailedMethodNotFound:(NSDictionary *)dataPack{
    
}
-(void)requestFailedNotFound:(NSDictionary *)dataPack{
    
}
-(void)requestFailedUnauthorised:(NSDictionary *)dataPack{
     [self logInUser];
}
-(void)requestFailedBadRequest:(NSDictionary *)dataPack{
    
}
-(void)requestSucceededWithData:(NSDictionary *)dataPack{
    
    if ([dataPack[@"url"] isEqualToString:LOGIN_USER]) {
        
        [db delTableWithTableName:USERTABLE];
        NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
        
        dic[@"id"]              =[NSString stringWithFormat:@"%@",dataPack[@"data"][@"id"]];
        dic[@"name"]            =dataPack[@"data"][@"name"];
        dic[@"email"]           =dataPack[@"data"][@"email"];
        dic[@"created_at"]      =dataPack[@"data"][@"created_at"];
        dic[@"updated_at"]      =dataPack[@"data"][@"updated_at"];
        dic[@"token"]           =dataPack[@"data"][@"access_token"];
        
        [db insertIntoTable:dic table_name:USERTABLE];
        [self sendDataToServerData];
    }
    
    else if ([dataPack[@"url"] isEqualToString:REGISTER_USER]){
        
        NSMutableDictionary *dic =[[NSMutableDictionary alloc] init];
        dic[@"token"]            =dataPack[@"data"][@"token"];
        dic[@"id"]               =[NSString stringWithFormat:@"%@",dic[@"id"]];
        
        [db insertIntoTable:dic table_name:USERTABLE];
        [self sendDataToServerData];
    }
    else if ([dataPack[@"url"] isEqualToString:SEARCH_SHIPPING_ADDRESS] ||[dataPack[@"url"] isEqualToString:SEARCH_LOCAL] ){
        
        
        _subLoader.hidden=true;
        _streetLoader.hidden=true;
        [_streetLoader stopAnimating];
        [_subLoader stopAnimating];
        
        
        [dataArray removeAllObjects];
        dataArray=[dataPack[@"payload"] mutableCopy];
        
        for (int i=0; i<[dataArray count]; i++) {
            if ([dataArray [i][@"flatUnitNumber"]length ]!=0) {
                
                NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
                dic=[dataArray[i] mutableCopy];
                dic[@"fullAddress"]=[NSString stringWithFormat:@"%@ / %@ %@, %@ %@ %@ ",dataArray[i][@"flatUnitNumber"],dataArray[i][@"streetNumber"],dataArray[i][@"street"],dataArray[i][@"locality"],dataArray[i][@"state"],dataArray[i][@"postcode"]];
                
                
                dataArray[i]=dic;
                
                //   dataArray[i][@"fullAddress"]=[NSString stringWithFormat:@"%@ / %@ %@, %@ %@ %@ ",dataArray[i][@"flatUnitNumber"],dataArray[i][@"streetNumber"],dataArray[i][@"street"],dataArray[i][@"locality"],dataArray[i][@"state"],dataArray[i][@"postcode"]];
                
            }
        }
        
        
        
        
        
        float cellHeight=0.0;
        int footerHeihgt = 40;
        cellHeight= MIN((dataArray.count *30.0)+footerHeihgt , 150);
        _tableView.frame = CGRectMake(_tableView.frame.origin.x, _tableView.frame.origin.y, _tableView.frame.size.width, cellHeight);
        if (dataArray.count >0) {
            _tableView.hidden=NO;
            [_tableView reloadData];
        }
        
        
        
    }
    
    
    
    else if ([_type isEqualToString:@"edit"]){
        [self removeLoader];
        [self.delegate addressDidEditServer:[dataPack[@"data"] mutableCopy]];
        //        [self dismissViewControllerAnimated:YES completion:nil];
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    
    else{
        [self removeLoader];
        [self.delegate addressDidSaveToserver:[dataPack[@"data"] mutableCopy]];
        [self.navigationController popViewControllerAnimated:YES];
        //        [self dismissViewControllerAnimated:YES completion:nil];
        
        
    }
    
    
    
    
    
}
-(void)requestSucceededButActionFailed:(NSDictionary *)dataPack{
    [Global showSimpleAlert:self andTitle:FRAMESHOP andMessage:@"Something went wrong please try again latter"];
    //    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)uploadedData:(NSString *)pecentage andByte:(float)bytes{
    
}
-(void)requestSucceededButNoDataFound:(NSDictionary *)dataPack{
    
}
-(void)requestFailedDueToServerIsuueWithError:(NSString*)errorDes{
    
}



#pragma mark -Table Delegates


- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.textLabel.font = self.fullNameTxt.font;
    
    if ([[dataArray[indexPath.row] allKeys] containsObject:@"eid"]) {
        
        cell.textLabel.text = dataArray[indexPath.row][@"fullAddress"];
        
    }
    else{
        cell.textLabel.text = dataArray[indexPath.row][@"msg"];
    }
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //
    return dataArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 30.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 40.0;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *backVIEW = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 40)];
    backVIEW.backgroundColor = APP_BLUE_COLOR;
    UILabel *titleLbl = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, backVIEW.frame.size.width-10, 30)];
    titleLbl.font = [UIFont fontWithName:self.fullNameTxt.font.familyName size:15.0];
    titleLbl.text = @"HIDE";
    titleLbl.textAlignment = NSTextAlignmentCenter;
    titleLbl.textColor = [UIColor whiteColor];
    titleLbl.backgroundColor = [UIColor clearColor];
    
    
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(footerTap:)];
    [backVIEW addGestureRecognizer:singleFingerTap];
    [backVIEW addSubview:titleLbl];
    return backVIEW;
}
- (void)footerTap:(UITapGestureRecognizer *)recognizer
{
    self.tableView.hidden = YES;
    _tableView.frame = CGRectMake(0, 0, 0,0);
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[dataArray[indexPath.row] allKeys] containsObject:@"eid"]) {
        
        
        if (isStreetAd) {
            
            if ([dataArray [indexPath.row][@"flatUnitNumber"]length ]!=0){
                _streetTxt.text=  [NSString stringWithFormat:@"%@ / %@ %@ ",dataArray[indexPath.row][@"flatUnitNumber"],dataArray[indexPath.row][@"streetNumber"],dataArray[indexPath.row][@"street"]];
            }
            else{
                _streetTxt.text=  [NSString stringWithFormat:@"%@ %@ %@",dataArray[indexPath.row][@"streetNumber"],dataArray[indexPath.row][@"streetName"],dataArray[indexPath.row][@"streetType"]];
            }
            
            _streetTxt.text=[Global makeFirstLatterUpperofString:_streetTxt.text];
            _addressInfo[@"street_address"]=_streetTxt.text;
            [Validation setValidationMarkToTextField:_streetTxt isCorrect:YES];
            
            
        }
        
        _subTxt.text=[NSString stringWithFormat:@"%@, %@ %@",[Global makeFirstLatterUpperofString:dataArray[indexPath.row][@"locality"]],[dataArray[indexPath.row][@"state"] uppercaseString],dataArray[indexPath.row][@"postcode"]];
        
        //_subTxt.text=[Global makeFirstLatterUpperofString:_subTxt.text];
        [Validation setValidationMarkToTextField:_subTxt isCorrect:YES];
        
        _addressInfo[@"suburbs"]=[Global makeFirstLatterUpperofString:dataArray[indexPath.row][@"locality"]];
        _addressInfo[@"state"]=[dataArray[indexPath.row][@"state"] uppercaseString];
        _addressInfo[@"postcode"]=[dataArray[indexPath.row][@"postcode"] uppercaseString];
        [self footerTap:nil];
        
    }
    else{
        
    }
    
}



@end
