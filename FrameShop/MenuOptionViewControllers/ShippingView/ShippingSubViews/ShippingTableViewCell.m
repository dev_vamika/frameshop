//
//  ShippingTableViewCell.m
//  FrameShop
//
//  Created by Saurabh Srivastav on 17/03/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import "ShippingTableViewCell.h"

@implementation ShippingTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];

    [Global roundCorner:_editBtn roundValue:10.0 borderWidth:0 borderColor:nil];

//    [_adressText setTextContainerInset:UIEdgeInsetsZero];
//    _adressText.textContainer.lineFragmentPadding =0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
- (IBAction)trashAction:(id)sender {
//    if (_checkBtn.isSelected) {
//        [self.delegate didDeSelectAdress:_infoDic andIndexPath:_indexPath];
//    }
    
    [self.delegate didDeleteAdress:_infoDic andIndexPath:_indexPath];
    
}
- (IBAction)editBtnAction:(id)sender {
    [self.delegate didEditAdress:_infoDic andIndexPath:_indexPath];
}

-(void)setLables{
    
    
//    if([_infoDic[@"email"] isEqual:[NSNull null]]){
//        _infoDic[@"email"]=@"";
//    }
//
//
    NSString *str;
    str=@"";
//
    
    
    if (_infoDic[@"company_name"] == (id)[NSNull null] || [_infoDic[@"company_name"] length]==0 ) {
        _infoDic[@"company_name"]=@"";
    }
    if (_infoDic[@"floor_or_unit"] == (id)[NSNull null] || [_infoDic[@"floor_or_unit"] length]==0 ) {
        _infoDic[@"floor_or_unit"]=@"";
    }
    
    if (![_infoDic[@"company_name"] isEqualToString:@""] ) {
        if(![_infoDic[@"company_name"] isEqualToString:@"<null>"])
            str=[NSString stringWithFormat:@"%@%@",str,_infoDic[@"company_name"]];
    }
    if (![_infoDic[@"floor_or_unit"] isEqualToString:@""]) {
        if(![_infoDic[@"floor_or_unit"] isEqualToString:@"<null>"])
            str=[NSString stringWithFormat:@"%@\n%@",str,_infoDic[@"floor_or_unit"]];
    }
    
//    if (![_infoDic[@"email"] isEqualToString:@""]) {
//
//
//        str=[NSString stringWithFormat:@"%@\n%@\n%@\n%@,%@\nMobile Number: %@ ",str,_infoDic[@"street_address"],_infoDic[@"suburbs"],_infoDic[@"state"],_infoDic[@"postcode"],_infoDic[@"mobile_number"]];
//
//    }
//    else{
    
    if ([str length]==0) {
         str=[NSString stringWithFormat:@"%@\n%@,%@ %@\n%@", _infoDic[@"street_address"],_infoDic[@"suburbs"],_infoDic[@"state"],_infoDic[@"postcode"],_infoDic[@"mobile_number"]];
    }
    else{
         str=[NSString stringWithFormat:@"%@\n%@\n%@ %@,%@\n%@",str, _infoDic[@"street_address"],_infoDic[@"suburbs"],_infoDic[@"state"],_infoDic[@"postcode"],_infoDic[@"mobile_number"]];
    }
    
       
        
  //  }
    
    
   
    
    _adressText.text=str;
    
    
    
    
    
    
    _fullName.text=[NSString stringWithFormat:@"%@", (_infoDic[@"full_name"]!=nil)?_infoDic[@"full_name"]:@"NA"];
    
    
}
- (IBAction)checkAction:(id)sender {
    if (_checkBtn.isSelected) {
        return;
    }
    
    _checkBtn.selected=!_checkBtn.isSelected;
    if (_checkBtn.isSelected) {
        [self.delegate didSelectAdress:_infoDic andIndexPath:_indexPath];
    }
    else{
        [self.delegate didDeSelectAdress:_infoDic andIndexPath:_indexPath];
    }
}
@end
