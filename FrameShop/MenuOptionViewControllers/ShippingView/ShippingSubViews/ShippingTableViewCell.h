//
//  ShippingTableViewCell.h
//  FrameShop
//
//  Created by Saurabh Srivastav on 17/03/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Global.h"
NS_ASSUME_NONNULL_BEGIN
@class ShippingTableViewCell;
@protocol ShippingTableViewCellDelegate <NSObject>
@optional
-(void)didSelectAdress:(NSDictionary *)infoDic andIndexPath:(NSIndexPath *)indexPath;
-(void)didDeSelectAdress:(NSDictionary *)infoDic andIndexPath:(NSIndexPath *)indexPath;
-(void)didDeleteAdress:(NSDictionary *)infoDic andIndexPath:(NSIndexPath *)indexPath;
-(void)didEditAdress:(NSMutableDictionary *)infoDic andIndexPath:(NSIndexPath *)indexPath;

@end
@interface ShippingTableViewCell : UITableViewCell

@property(strong,nonatomic)id<ShippingTableViewCellDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *fullName;
@property (weak, nonatomic) IBOutlet UILabel *coName;
@property (weak, nonatomic) IBOutlet UILabel *floor;
@property (weak, nonatomic) IBOutlet UILabel *street;
@property (weak, nonatomic) IBOutlet UILabel *sub;
@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UILabel *state;
@property (weak, nonatomic) IBOutlet UILabel *postal;
@property (weak, nonatomic) IBOutlet UILabel *mob;
@property (weak, nonatomic) IBOutlet UIButton *checkBtn;
- (IBAction)checkAction:(id)sender;
@property NSMutableDictionary *infoDic;
@property NSIndexPath * indexPath;
@property (weak, nonatomic) IBOutlet UILabel *adressText;
- (IBAction)trashAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *editBtn;
@property (weak, nonatomic) IBOutlet UIButton *delBtn;
-(void)setLables;
@end

NS_ASSUME_NONNULL_END
