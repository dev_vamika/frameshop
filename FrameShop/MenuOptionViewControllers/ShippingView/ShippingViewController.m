//
//  ShippingViewController.m
//  FrameShop
//
//  Created by Saurabh Srivastav on 03/03/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import "ShippingViewController.h"
#import "Validation.h"
#import "UIView+Toast.h"
@interface ShippingViewController (){
    
    
    DbManager *db;
    NSMutableArray *dataArray;
    BOOL isLoaded;
    NSIndexPath *selectIndexPath,*editIndexPath;
    
}

@end

@implementation ShippingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    db=[[DbManager alloc] init];
    dataArray=[[NSMutableArray alloc] init];
    _addressDic = [[NSMutableDictionary alloc] init];
    _addEmailBtn.transform  =CGAffineTransformMakeRotation(M_PI / -4);
    _addAddressBtn.transform=CGAffineTransformMakeRotation(M_PI / -4);
    _emailTxt.text=[[NSUserDefaults standardUserDefaults] objectForKey:EMAIL];
    
   
    
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:EMAIL]length]!=0) {
        if ([Validation isValidEmail:_emailTxt.text]) {
            [Validation setValidationMarkToTextField:_emailTxt isCorrect:YES];
        }
        else{
             [Validation setValidationMarkToTextField:_emailTxt isCorrect:NO];
        }
    }
    
    
    
    _procdBtn.backgroundColor=[UIColor lightGrayColor];
    _procdBtn.enabled=NO;
    [Global roundCorner:_emailTxt roundValue:5.0 borderWidth:1.5 borderColor:APP_BLUE_COLOR];
    [Global Padding:_emailTxt];
    NSMutableArray *userInfo=[db getDataForField:USERTABLE where:nil];
    
    if (userInfo.count!=0) {
        [self loadProductData];
    }
    else
    {
        [self signUpUser];
    }
    [self.navigationItem setBackBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil]];
    
    // [[NSUserDefaults standardUserDefaults] setValue:nil forKey:ADDRESS_ID];
    
    
    // [self.delegate shouldEnableScrolling:false];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    
    
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
}
-(void)swipeleft:(UISwipeGestureRecognizer*)gestureRecognizer
{
    
}

-(void)loadProductData{
    
    
    
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    NSString *urlString=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,GET_SHIPPING_ADDRESS];
    
    NSMutableArray *arr=[[NSMutableArray alloc] init];
    arr=[db getDataForField:USERTABLE where:nil];
    NSLog(@"arr %@",arr);
    if (arr.count!=0) {
        [api sendGetRequst:urlString andToken:arr[0][@"token"]];
    }
    else{
        [self logInUser];
    }
}
-(void)deleteAddress:(NSMutableDictionary *)dic{
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    NSString *urlString=[NSString stringWithFormat:@"%@%@/%@",SERVER_ADDRESS,DEL_SHIPPING_ADDRESS,dic[@"id"]];
    
    NSMutableArray *arr=[[NSMutableArray alloc] init];
    arr=[db getDataForField:USERTABLE where:nil];
    NSLog(@"arr %@",arr);
    if (arr.count!=0) {
        [api sendDelRequst:urlString andToken:arr[0][@"token"]];
    }
}

-(void)signUpUser{
    
    
    
    
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
    dic[@"name"]=[[UIDevice currentDevice] name];
    dic[@"email"]=[NSString stringWithFormat:@"%@@frameshop.com.au",[Global getDeviceId]];
    dic[@"password"]=@"qwxcjeub32ne";
    dic[@"c_password"]=@"qwxcjeub32ne";
    dic[@"url"]=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,REGISTER_USER];
    [api sendPostRequst:dic];
    
}
-(void)logInUser{
    
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
    
    dic[@"email"]=[NSString stringWithFormat:@"%@@frameshop.com.au",[Global getDeviceId]];
    dic[@"password"]=@"qwxcjeub32ne";
    
    dic[@"url"]=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,LOGIN_USER];
    [api sendPostRequst:dic];
}


- (IBAction)proceedAction:(id)sender {
    NSString *email = [[NSUserDefaults standardUserDefaults] valueForKey:EMAIL];
    
    email=[Global removeEndSpaceFrom:email];
    
    if (![Validation isValidEmail:email]) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:FRAMESHOP
                                      message:@"Please enter the email"
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {
        }];
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    
    [self.delegate didSelectProceedAction:self.index];
}
#pragma mark -Textview Delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
      NSString * newStr = [textField.text stringByReplacingCharactersInRange:range withString:string];
    newStr=[Global removeEndSpaceFrom:newStr];
  
    
    [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%@",newStr] forKey:EMAIL];
    
    if ([Validation isValidEmail:newStr]) {
        [Validation setValidationMarkToTextField:textField isCorrect:YES];
    }
    else{
         [Validation setValidationMarkToTextField:textField isCorrect:NO];
    }
    
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [_emailTxt resignFirstResponder];
    return NO;
}



#pragma mark - Segue support

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    
    
    if ([[segue identifier] isEqualToString:@"add"]) {
        
        AddAddressViewController *ditalView = [segue destinationViewController];
        
        ditalView.addressInfo=(NSMutableDictionary *)sender;
        
        ditalView.delegate=self;
        
    }
    
    
}

#pragma mark - Addressview Delegate
-(void)addressDidSaveToPrefrence:(NSDictionary *)infoDic{
    
    
    
    
    
}
-(void)addressDidSaveToserver:(NSMutableDictionary *)infoDic{
    selectIndexPath=nil;
    
    if (dataArray.count==0) {
        [[NSUserDefaults standardUserDefaults] setValue:nil forKey:ADDRESS_ID];
    }
    else{
         [[NSUserDefaults standardUserDefaults] setValue:infoDic[@"id"] forKey:ADDRESS_ID];
    }
    
    [dataArray insertObject:infoDic atIndex:0];
    
    isLoaded=YES;
    
    [_tableView reloadData];
    if (dataArray.count == 0) {
        _saperatorHeight.constant = 0;
    }else{
         _saperatorHeight.constant = 3;
    }
    
}
-(void)addressDidEditServer:(NSMutableDictionary *)infoDic{
    selectIndexPath=nil;
    if (editIndexPath != nil) {
        dataArray[editIndexPath.row]=infoDic;
        [_tableView reloadData];
    }
    editIndexPath=nil;
    
    
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return (isLoaded)?dataArray.count:4;
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (isLoaded) {
        static NSString *simpleTableIdentifier = @"ShippingTableViewCell";
        ShippingTableViewCell *cell = (ShippingTableViewCell *)[_tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ShippingTableViewCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        cell.delegate=self;
        cell.infoDic=dataArray[indexPath.row];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.indexPath=indexPath;
        cell.checkBtn.selected=false;
        
        
       
        
        
       if( [[[NSUserDefaults standardUserDefaults] valueForKey:ADDRESS_ID] intValue]==[dataArray[indexPath.row][@"id"] intValue] ) {
            
            selectIndexPath=indexPath;
            _procdBtn.backgroundColor=APP_BLUE_COLOR;
            _procdBtn.enabled=YES;
            
            [self didSelectAdress:dataArray[indexPath.row] andIndexPath:indexPath];
        }
        if ([[NSUserDefaults standardUserDefaults] valueForKey:ADDRESS_ID]==nil ){
        selectIndexPath= [NSIndexPath indexPathForItem:0 inSection:0];
        _procdBtn.backgroundColor=APP_BLUE_COLOR;
        _procdBtn.enabled=YES;
            [self didSelectAdress:dataArray[0] andIndexPath:selectIndexPath];
        }
        
      //  [self didSelectAdress:dataArray[0] andIndexPath:selectIndexPath];
        
        if (indexPath==selectIndexPath) {
            cell.checkBtn.selected=true;
        }else{
            cell.checkBtn.selected=false;
        }
        [cell setLables];
        
        return cell;
    }
    
    else{
        static NSString *simpleTableIdentifier = @"LoaderTableViewCell";
        LoaderTableViewCell *cell = (LoaderTableViewCell *)[_tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"LoaderTableViewCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 60.0;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *backVIEW = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 60)];
    backVIEW.backgroundColor = UIColor.whiteColor;
    UILabel *titleLbl = [[UILabel alloc] initWithFrame:CGRectMake(13, 55 - _emailTxt.frame.size.height, backVIEW.frame.size.width-26, _emailTxt.frame.size.height)];
    titleLbl.font = [UIFont fontWithName:self.emailTxt.font.familyName size:15.0];
    titleLbl.text = @"  + Add New Address";
    titleLbl.textColor = APP_BLUE_COLOR;
    titleLbl.backgroundColor = [UIColor whiteColor];
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    [backVIEW addGestureRecognizer:singleFingerTap];
    [backVIEW addSubview:titleLbl];
    [Global roundCorner:titleLbl roundValue:5.0 borderWidth:1.5 borderColor:APP_BLUE_COLOR];
    return backVIEW;
}
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    
    
    [[NSUserDefaults standardUserDefaults] setValue:_emailTxt.text forKey:EMAIL];
    
    [self performSegueWithIdentifier:@"add" sender:nil];
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (isLoaded) {
        NSArray *nibViews = [[NSBundle mainBundle] loadNibNamed:@"ShippingTableViewCell" owner:self options:nil];
        ShippingTableViewCell *sqr = [nibViews objectAtIndex:0];
        CGFloat  height = [self heightForlabel:[NSString stringWithFormat:@"%@\n%@\n%@,%@ %@\n%@",@"", sqr.infoDic[@"street_address"],sqr.infoDic[@"suburbs"],sqr.infoDic[@"state"],sqr.infoDic[@"postcode"],sqr.infoDic[@"mobile_number"]] font:sqr.adressText.font width:sqr.adressText.frame.size.width];
        return sqr.frame.size.height + height;
    }
    else{
        return self.tableView.bounds.size.width/2;
    }
    
    
}
-(CGFloat)heightForlabel:(NSString*)text  font:(UIFont *)font  width:(CGFloat)width {
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, width,CGFLOAT_MAX)];//CGFloat.greatestFiniteMagnitude
    label.numberOfLines = 0;
    label.lineBreakMode = NSLineBreakByWordWrapping;
    label.font = font;
    label.text = text;
    [label sizeToFit];

    return label.frame.size.height;
}

#pragma mark - Shipping Cell Delegate
-(void)didSelectAdress:(NSDictionary *)infoDic andIndexPath:(NSIndexPath *)indexPath{
    
    _addressDic = [infoDic mutableCopy];
    if (selectIndexPath == nil) {
        selectIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        ShippingTableViewCell *cell1=[_tableView cellForRowAtIndexPath:selectIndexPath];
        cell1.checkBtn.selected=false;
    }else{
        ShippingTableViewCell *cell1=[_tableView cellForRowAtIndexPath:selectIndexPath];
        cell1.checkBtn.selected=false;
    }
   
    
    
    
    selectIndexPath=indexPath;
    
    
    
    // _addressId=infoDic[@"id"];
    [[NSUserDefaults standardUserDefaults] setValue:infoDic[@"id"] forKey:ADDRESS_ID];
    
    
    ShippingTableViewCell *cell=[_tableView cellForRowAtIndexPath:indexPath];
    cell.checkBtn.selected=YES;
    
    
    
    
    _procdBtn.backgroundColor=APP_BLUE_COLOR;
    _procdBtn.enabled=YES;
    [self.delegate didSelectAdress:[infoDic mutableCopy]];
}
-(void)didDeleteAdress:(NSDictionary *)infoDic andIndexPath:(NSIndexPath *)indexPath{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Are you sure?" message:@"You will not be able to undo this action!" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Yes, delete it!" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        if ([self.addressDic[@"id"] intValue]==[infoDic[@"id"] intValue]) {
            self->_addressDic=nil;
            self->_procdBtn.backgroundColor=[UIColor lightGrayColor];
            self->_procdBtn.enabled=NO;
            
            if (indexPath==self->selectIndexPath) {
                self->selectIndexPath=nil;
                [[NSUserDefaults standardUserDefaults] setValue:nil forKey:ADDRESS_ID];
            }
        }
        if (indexPath.row<self->dataArray.count) {
            [self->dataArray removeObjectAtIndex:indexPath.row];
        }
        
        [self->_tableView reloadData];
        [self deleteAddress:[infoDic mutableCopy]];
        if (self->dataArray.count == 0) {
            self->_saperatorHeight.constant = 0;
        }
    }];
    UIAlertAction *no = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //button click event
    }];
    [alert addAction:ok];
    [alert addAction:no];
    [self presentViewController:alert animated:YES completion:nil];
    
}
-(void)didDeSelectAdress:(NSDictionary *)infoDic andIndexPath:(NSIndexPath *)indexPath{
    //    self->_addressDic=nil;
    //    selectIndexPath=nil;
    //    [_tableView reloadData];
    //    _procdBtn.backgroundColor=[UIColor lightGrayColor];
    //     [[NSUserDefaults standardUserDefaults] setValue:nil forKey:ADDRESS_ID];
    //
    //    _procdBtn.enabled=NO;
    
    
}

-(void)didEditAdress:(NSMutableDictionary *)infoDic andIndexPath:(NSIndexPath *)indexPath{
    
    editIndexPath=indexPath;
    infoDic[@"type"]=@"edit";
    
    [self performSegueWithIdentifier:@"add" sender:infoDic];
    
}


#pragma mark - POP API DELEGATES
-(void)connectionError{
    
    isLoaded=YES;
    [_tableView reloadData];
    
    // [Global showWarningScreen:nil];
    
    
}
-(void)requestFailedSessionExpired:(NSDictionary *)dataPack{
    
}
-(void)requestFailedServerError:(NSDictionary *)dataPack{
    
}
-(void)requestFailedServiceUnavailableDueToMaintenance:(NSDictionary *)dataPack{
    
    
    isLoaded=YES;
    [_tableView reloadData];
    if (dataArray.count == 0) {
        _saperatorHeight.constant = 0;
    }
    if (![dataPack[@"message"] isEqualToString:@"Shipping Address  does not Exist!"]) {
        [Global showSimpleAlert:self andTitle:FRAMESHOP andMessage:dataPack[@"message"]];
    }
}
-(void)requestFailedMethodNotFound:(NSDictionary *)dataPack{
    
}
-(void)requestFailedNotFound:(NSDictionary *)dataPack{
    if ([dataPack[@"error"] isEqualToString:@"Shipping Address  does not Exist!"]) {
        isLoaded=YES;
        [_tableView reloadData];
        _saperatorHeight.constant = 0;
    }
}
-(void)requestFailedUnauthorised:(NSDictionary *)dataPack{
     [self logInUser];
}
-(void)requestFailedBadRequest:(NSDictionary *)dataPack{
    
}
-(void)requestSucceededWithData:(NSDictionary *)dataPack{
    
    if ([dataPack[@"url"] isEqualToString:LOGIN_USER]) {
        
        [db delTableWithTableName:USERTABLE];
        NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
        
        dic[@"id"]              =[NSString stringWithFormat:@"%@",dataPack[@"data"][@"id"]];
        dic[@"name"]            =dataPack[@"data"][@"name"];
        dic[@"email"]           =dataPack[@"data"][@"email"];
        dic[@"created_at"]      =dataPack[@"data"][@"created_at"];
        dic[@"updated_at"]      =dataPack[@"data"][@"updated_at"];
        dic[@"token"]           =dataPack[@"data"][@"access_token"];
        
        [db insertIntoTable:dic table_name:USERTABLE];
        [self loadProductData];
    }
    
    else if ([dataPack[@"url"] isEqualToString:REGISTER_USER]){
        
        NSMutableDictionary *dic =[[NSMutableDictionary alloc] init];
        dic[@"token"]            =dataPack[@"data"][@"token"];
        dic[@"id"]               =[NSString stringWithFormat:@"%@",dic[@"id"]];
        
        [db insertIntoTable:dic table_name:USERTABLE];
        [self loadProductData];
    }
    else if ([dataPack[@"url"] isEqualToString:GET_SHIPPING_ADDRESS]){
        
        
        
        [dataArray removeAllObjects];
        NSArray *arr=[dataPack[@"data"] mutableCopy];
        [db delTableWithTableName:ADDRESS_TABLE];
        for (int i=0; i<arr.count; i++) {
            
            
            
            
            NSMutableDictionary *temDic=[[NSMutableDictionary alloc] init];
            
            temDic[@"id"]               =[NSString stringWithFormat:@"%@",arr[i][@"id"]];
            temDic[@"user_id"]          =[NSString stringWithFormat:@"%@",arr[i][@"user_id"]];
            temDic[@"full_name"]        =[Global makeFirstLatterUpperofString:arr[i][@"full_name"]]   ;
            temDic[@"email"]            =(arr[i][@"email"] == (id)[NSNull null] || [arr[i][@"email"] length] == 0 )?@"":arr[i][@"email"];
            temDic[@"floor_or_unit"]    =(arr[i][@"floor_or_unit"] == (id)[NSNull null] || [arr[i][@"floor_or_unit"] length] == 0 )?@"":arr[i][@"floor_or_unit"];
            temDic[@"mobile_number"]    =[NSString stringWithFormat:@"%@",arr[i][@"mobile_number"]];
            temDic[@"postcode"]         =[NSString stringWithFormat:@"%@",arr[i][@"postcode"]];
            temDic[@"state"]            =arr[i][@"state"];
            temDic[@"street_address"]   =arr[i][@"street_address"];
            temDic[@"suburbs"]          =arr[i][@"suburbs"];
            temDic[@"company_name"]     =(arr[i][@"company_name"] == (id)[NSNull null] || [arr[i][@"company_name"] length] == 0 )?@"": arr[i][@"company_name"];
            dataArray[dataArray.count]=temDic;
            [db insertIntoTable:temDic table_name:ADDRESS_TABLE];
            
            
            
        }
        dataArray=[[[dataArray reverseObjectEnumerator] allObjects] mutableCopy];
        isLoaded=YES;
        [_tableView reloadData];
         if (dataArray.count != 0) {
             _saperatorHeight.constant = 3;
         }
    }
    
    
    
}
-(void)requestSucceededButActionFailed:(NSDictionary *)dataPack{
    
    if ([dataPack[@"url"] isEqualToString:REGISTER_USER] || [dataPack[@"message"] isEqualToString:@"Unauthenticated."]) {
        //go To login
        
        [self logInUser];
    }
    else if ([dataPack[@"url"] isEqualToString:LOGIN_USER]) {
        //something bad happned with server
        [self signUpUser];
        NSLog(@"something bad happned with server login signup both failed");
    }
    else{
        isLoaded=YES;
        [_tableView reloadData];
        if (dataArray.count == 0) {
            _saperatorHeight.constant = 0;
        }
    }
    
    
}
-(void)uploadedData:(NSString *)pecentage andByte:(float)bytes{
    
}
-(void)requestSucceededButNoDataFound:(NSDictionary *)dataPack{
    
}
-(void)requestFailedDueToServerIsuueWithError:(NSString*)errorDes{
    
}

@end
