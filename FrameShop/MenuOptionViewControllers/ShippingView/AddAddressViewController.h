//
//  AddAddressViewController.h
//  FrameShop
//
//  Created by Saurabh Srivastav on 18/03/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Validation.h"
#import "PopApi.h"
NS_ASSUME_NONNULL_BEGIN
#define BORDER_WIDTH                               1.0
#define ROUND_CORNER                               3.0
@class AddAddressViewController;
@protocol AddAddressViewControllerDelegate <NSObject>
@optional
-(void)addressDidSaveToPrefrence:(NSDictionary *)infoDic;
-(void)addressDidSaveToserver:(NSMutableDictionary *)infoDic;
-(void)addressDidEditServer:(NSMutableDictionary *)infoDic;

@end
@interface AddAddressViewController : UIViewController<PopApiDelegate,UITextFieldDelegate>
@property(strong,nonatomic)id<AddAddressViewControllerDelegate> delegate;
@property NSMutableDictionary *addressInfo;
@property NSIndexPath *indexPath;
@property (weak, nonatomic) IBOutlet UIView *backView;
//@property (weak, nonatomic) IBOutlet UIView *nameView;
//@property (weak, nonatomic) IBOutlet UIView *companyView;
//@property (weak, nonatomic) IBOutlet UIView *florView;
@property (weak, nonatomic) IBOutlet UIView *streetView;
@property (weak, nonatomic) IBOutlet UIView *subView;
@property (weak, nonatomic) IBOutlet UIView *sampleView;
//@property (weak, nonatomic) IBOutlet UIView *stateView;
//@property (weak, nonatomic) IBOutlet UIView *postView;
//@property (weak, nonatomic) IBOutlet UIView *mobileView;
@property (weak, nonatomic) IBOutlet UIButton *addBtn;
@property (weak, nonatomic) IBOutlet UITextField *fullNameTxt;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTxt;
@property (weak, nonatomic) IBOutlet UITextField *comTxt;
@property (weak, nonatomic) IBOutlet UITextField *floorTxt;
@property (weak, nonatomic) IBOutlet UITextField *streetTxt;
@property (weak, nonatomic) IBOutlet UITextField *subTxt;
@property (weak, nonatomic) IBOutlet UITextField *statTxt;
@property (weak, nonatomic) IBOutlet UITextField *postTxt;
@property (weak, nonatomic) IBOutlet UITextField *mobTxt;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *addAddressWidth;
@property (weak, nonatomic) IBOutlet UITextField *countryTxt;
@property (weak, nonatomic) IBOutlet UIToolbar *toolbar;
@property NSString *type;
- (IBAction)addAction:(id)sender;
- (IBAction)closeAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loader;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *streetLoader;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *subLoader;
@property (weak, nonatomic) IBOutlet UILabel *warningTextStreet;
@property (weak, nonatomic) IBOutlet UILabel *warningTextSub;
@end

NS_ASSUME_NONNULL_END
