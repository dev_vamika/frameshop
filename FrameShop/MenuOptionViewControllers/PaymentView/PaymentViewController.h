//
//  PaymentViewController.h
//  FrameShop
//
//  Created by Saurabh Srivastav on 03/03/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopApi.h"
#import <WebKit/WebKit.h>
#import "DBManager.h"
#import "DiscountPopUp.h"

//#import "PayPalMobile.h"
#import "Validation.h"
NS_ASSUME_NONNULL_BEGIN
@class PaymentViewController;
@protocol PaymentViewControllerDelegate <NSObject>
@optional
-(void)didSelectProceedAction:(int)pageIndex;
-(void)didAskToEmail:(int)pageIndex;
-(void)paymentFailedOrCanceled:(int)pageIndex;
@end
@interface PaymentViewController : UIViewController<PopApiDelegate>
@property(strong,nonatomic)id<PaymentViewControllerDelegate> delegate;
- (IBAction)payAction:(id)sender;
@property int index;
@property (weak, nonatomic) IBOutlet UIView *couponView;
@property (weak, nonatomic) IBOutlet UITextField *couponTextField;
@property (weak, nonatomic) IBOutlet UIButton *couponApplyButton;
@property (weak, nonatomic) IBOutlet UIView *giftCardView;
- (IBAction)onClickCouponApply:(id)sender;
//@property (weak, nonatomic) IBOutlet UITextField *giftCardTextField;
//@property (weak, nonatomic) IBOutlet UIButton *giftCardApplyButton;
//- (IBAction)onClickGiftCardApply:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *westpacButton;
@property (weak, nonatomic) IBOutlet UIButton *paypalButton;
- (IBAction)onClickPaypal:(id)sender;
- (IBAction)onClickWestpac:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *cardHolderName;
@property (weak, nonatomic) IBOutlet UITextField *cardNumberTF;
@property (weak, nonatomic) IBOutlet UITextField *expiryDateTF;
@property (weak, nonatomic) IBOutlet UILabel *subTotalLabel;
@property (weak, nonatomic) IBOutlet UILabel *shippingLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalPriceLabel;
@property (weak, nonatomic) IBOutlet UIView *paymentView;
- (IBAction)onClickTermAndCondition:(id)sender;
- (IBAction)onClickPrivacyPolicy:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *secureCheckOutLbl;
@property (weak, nonatomic) IBOutlet UIImageView *secureLockImage;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UILabel *andLabel;
@property (weak, nonatomic) IBOutlet UIButton *termAndConditionButton;
@property (weak, nonatomic) IBOutlet UIButton *privacypolicyButton;

@property (weak, nonatomic) IBOutlet UIButton *payNowButton;
@property (weak, nonatomic) IBOutlet UIButton *payPalPayNowButton;
- (IBAction)payWithPaypal:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *paymentViewBottomBar;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *priceDetailContainerTop;
@property NSMutableArray *orderIdArray;
@property (weak, nonatomic) IBOutlet UILabel *discountTxt;

@property float cartAmount;
@property (weak, nonatomic) IBOutlet UITextField *cvvTF;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *payNowWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *paypalWidth;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loader;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *couponHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *giftCardHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *paymentHeight;
- (IBAction)onClickHaveAvoucher:(id)sender;
- (IBAction)onClickHaveAGiftCard:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *haveVoucherBtn;
@property (weak, nonatomic) IBOutlet UIButton *haveGiftCardBtn;

@property (weak, nonatomic) IBOutlet UIImageView *wespacLogoImage;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *couponActivity;
@property (weak, nonatomic) IBOutlet UIImageView *paypalLogoImage;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *giftCardActivity;
@property NSDictionary *addressInfoDic;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *discountHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *couponTextHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *giftTextHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *paymentMethodTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *discountViewHeight;
@property (weak, nonatomic) IBOutlet UITextView *userNoteTextView;
@property (weak, nonatomic) IBOutlet UILabel *userNoteSubLabel;
@end

NS_ASSUME_NONNULL_END
