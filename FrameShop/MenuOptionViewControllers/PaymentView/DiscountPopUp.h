//
//  DiscountPopUp.h
//  FrameShop
//
//  Created by Saurabh Srivastav on 31/03/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Global.h"
NS_ASSUME_NONNULL_BEGIN

@interface DiscountPopUp : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *valueLbl;
@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UIImageView *checkMark;
@property (weak, nonatomic) IBOutlet UIView *checkBackView;
@property (weak, nonatomic) IBOutlet UILabel *disLbl;

@property (weak, nonatomic) IBOutlet UILabel *messageLbl;
@property (weak, nonatomic) IBOutlet UIButton *okBtn;
- (IBAction)okAction:(id)sender;
@property NSString *valueTxt;
@property NSString *couponCodeName;
@end

NS_ASSUME_NONNULL_END
