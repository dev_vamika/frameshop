//
//  DiscountPopUp.m
//  FrameShop
//
//  Created by Saurabh Srivastav on 31/03/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import "DiscountPopUp.h"

@interface DiscountPopUp ()

@end

@implementation DiscountPopUp

- (void)viewDidLoad {
    [super viewDidLoad];
    [Global roundCorner:_backView roundValue:10 borderWidth:0 borderColor:nil];
    [self addshadow:_backView radious:6];
    //[self performSelector:@selector(hidePopUp) withObject:self afterDelay:1.5 ];
    [Global roundCorner:_okBtn roundValue:5 borderWidth:0 borderColor:nil];
    _valueLbl.text=_valueTxt;
    _messageLbl.text = [NSString stringWithFormat:@"%@ Successfully Applied !",[_couponCodeName uppercaseString]];
    CAShapeLayer * maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: _checkBackView.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii: (CGSize){15.0, 15.}].CGPath;

   _checkBackView.layer.mask = maskLayer;
    _checkBackView.backgroundColor=[Global colorFromHexString:@"#eeffee"];
    // Do any additional setup after loading the view from its nib.
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    NSMutableArray *arr=[[NSMutableArray alloc] init];
    for (int i=1; i<39; i++) {
        arr[arr.count]=[UIImage imageNamed:[NSString stringWithFormat:@"paymentDone_%d.png",i]];
    }
  
    _disLbl.text=[NSString stringWithFormat:@"You will recive a %@ discount on your order",_valueTxt];
    _checkMark.animationImages=arr;
    _checkMark.animationDuration = 1.0;/* Number of images x 1/30 gets you 30FPS */;

    // Set repeat count
   _checkMark.animationRepeatCount = 0; /* 0 means infinite */
[self performSelector:@selector(hidePopUp) withObject:self afterDelay:1.0 ];
    // Start animating
    [_checkMark startAnimating];
    [self  setAnimationToLbl:_messageLbl];
    [self  setAnimationToLbl:_disLbl];
   
}

-(void)hidePopUp{
    [_checkMark stopAnimating];
}

-(void) addshadow:(UIView *)view radious:(float)radious{
    view.layer.masksToBounds = NO;
    view.layer.shadowColor = [[UIColor grayColor] CGColor];
    view.layer.shadowOffset = CGSizeMake(0,0);
    view.layer.shadowOpacity = 0.7;
    view.layer.shadowRadius = radious;
    
}

-(void)setAnimationToLbl:(UILabel *)lbl{
    NSString *str=lbl.text;
    
    [UIView beginAnimations:@"animateText" context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    [UIView setAnimationDuration:1.5f];
    [lbl setAlpha:0];
    [lbl setText:str];
    [lbl setAlpha:1];
    [UIView commitAnimations];
}


- (IBAction)okAction:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}
@end
