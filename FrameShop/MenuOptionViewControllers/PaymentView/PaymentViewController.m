//
//  PaymentViewController.m
//  FrameShop
//
//  Created by Saurabh Srivastav on 03/03/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import "PaymentViewController.h"
#import "Global.h"
#import "BraintreePayPal.h"
#import "BraintreeCore.h"
#import "BraintreeDropIn.h"
#import "BTDataCollector.h"
#import "WebViewController.h"
@interface PaymentViewController ()<UITextFieldDelegate,BTViewControllerPresentingDelegate,BTAppSwitchDelegate,UITextViewDelegate>
{
    NSString *previousTextFieldContent;
    UITextRange *previousSelection;
    DbManager *db;
    NSString *transactionId;
    float oldValue;
    NSString *paymentType;
}
//@property (strong, nonatomic) IBOutlet PayPalConfiguration *payPalConfiguration;
@property (nonatomic, strong) BTAPIClient *braintreeClient;
@property (nonatomic, strong) BTPayPalDriver *payPalDriver;
@property (nonatomic, strong) BTDataCollector *dataCollector;
@end

@implementation PaymentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _loader.hidden = YES;
    // Do any additional setup after loading the view.
    db=[[DbManager alloc] init];
    self.cardNumberTF.delegate = self;
    self.expiryDateTF.delegate = self;
    self.cvvTF.delegate = self;
    self.cardHolderName.delegate = self;
    [_cardNumberTF addTarget:self action:@selector(reformatAsCardNumber:) forControlEvents:UIControlEventEditingChanged];
    [_expiryDateTF addTarget:self action:@selector(creditCardExpiryFormatter:) forControlEvents:UIControlEventEditingChanged];
    self.couponApplyButton.hidden = YES;
//    self.giftCardApplyButton.hidden = YES;
    [self onClickPaypal:_paypalButton];
    [Global roundCorner:_cvvTF roundValue:5.0 borderWidth:1.0 borderColor:APP_BLUE_COLOR];
    [Global roundCorner:_cardNumberTF roundValue:5.0 borderWidth:1.0 borderColor:APP_BLUE_COLOR];
    [Global roundCorner:_cardHolderName roundValue:5.0 borderWidth:1.0 borderColor:APP_BLUE_COLOR];
    [Global roundCorner:_expiryDateTF roundValue:5.0 borderWidth:1.0 borderColor:APP_BLUE_COLOR];
    [Global roundCorner:_giftCardView roundValue:5.0 borderWidth:1.0 borderColor:APP_BLUE_COLOR];
    [Global roundCorner:_couponView roundValue:5.0 borderWidth:1.0 borderColor:APP_BLUE_COLOR];
    [Global roundCorner:_payPalPayNowButton roundValue:_payPalPayNowButton.frame.size.height/2 borderWidth:0.0 borderColor:APP_BLUE_COLOR];
    [Global Padding:_cardNumberTF];
    [Global Padding:_expiryDateTF];
    [Global Padding:_cardHolderName];
    [Global Padding:_cvvTF];
    [Global Padding:_couponTextField];
//    [Global Padding:_giftCardTextField];
//    _payPalConfiguration = [[PayPalConfiguration alloc] init];
//    _payPalConfiguration.acceptCreditCards = NO;
//    _payPalConfiguration.payPalShippingAddressOption = PayPalShippingAddressOptionPayPal;
    _giftCardHeight.constant = 0.0;
    _couponHeight.constant = 0.0;
    _couponActivity.hidden=YES;
    _giftCardActivity.hidden=YES;
    _userNoteTextView.text = @"Write your special order requests here...";
    _userNoteTextView.textColor = APP_GRAY_COLOR;
    _userNoteTextView.delegate = self;
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    oldValue=_cartAmount;
    
    self.payNowButton.userInteractionEnabled = NO;
    self.payNowButton.backgroundColor = APP_GRAY_COLOR;
      if (_orderIdArray==nil || _addressInfoDic==nil) {
        
          self.paypalButton.userInteractionEnabled = NO;
          self.payPalPayNowButton.backgroundColor = APP_GRAY_COLOR;
      }else{
          self.payNowButton.userInteractionEnabled = YES;
                   self.payNowButton.backgroundColor = APP_BLUE_COLOR;
                   self.paypalButton.userInteractionEnabled = YES;
                   self.payPalPayNowButton.backgroundColor = [UIColor colorWithRed:39/255.0 green:174/255.0 blue:96/255.0 alpha:1.0];
      }
    
    
    BOOL isGiftShipping = [[NSUserDefaults standardUserDefaults] boolForKey:@"gift_shipping"];
    BOOL isProductShipping = [[NSUserDefaults standardUserDefaults] boolForKey:@"product_shipping"];
    
    if (!isProductShipping && isGiftShipping){
        [self hideDiscount];
        self.paymentMethodTop.constant = -30;
        self.couponTextHeight.constant = 0.0;
        self.giftTextHeight.constant = 0.0;
        [self.haveVoucherBtn setTitle:@"" forState:UIControlStateNormal];
        [self.haveGiftCardBtn setTitle:@"" forState:UIControlStateNormal];
        self.shippingLabel.text = @"$0";
    }else{
    NSMutableArray*    tempArr=[db getDataForField:USER_PRODUCT where:nil];
    if(tempArr.count >0 ){
        if ([[NSString stringWithFormat:@"%@",tempArr[0][@"shipping_price"]] intValue] == 0) {
            
            self.shippingLabel.text = @"$20";
        }else{
            self.shippingLabel.text = [NSString stringWithFormat:@"$%@",tempArr[0][@"shipping_price"]];
        }
    }else{
        self.shippingLabel.text = @"$20";
    }
        
        self.paymentMethodTop.constant = 10;
        self.couponTextHeight.constant = 15;
        [self showDiscount];
        self.giftTextHeight.constant = 15;
        [self.haveVoucherBtn setTitle:@"Have a Coupon or Gift Card?" forState:UIControlStateNormal];
        [self.haveGiftCardBtn setTitle:@"Add Order Comments" forState:UIControlStateNormal];
    }
    
    if (!([self.shippingLabel.text isKindOfClass:[NSNull class]]) )
    
    _totalPriceLabel.text=[NSString stringWithFormat:@"$%.02f",_cartAmount+[[self.shippingLabel.text stringByReplacingOccurrencesOfString:@"$" withString:@""] intValue]];
    _subTotalLabel.text=[NSString stringWithFormat:@"$%.02f",_cartAmount];
 //   [self performSegueWithIdentifier:@"loadWeb" sender:nil];
    if (!([_discountTxt.text isKindOfClass:[NSNull class]]) ){
    if ([[_discountTxt.text stringByReplacingOccurrencesOfString:@"$" withString:@""] intValue] != 0) {
        [self showDiscount];
    }else{
        [self hideDiscount];
    }
    }
}
- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
}
-(void) showDiscount{
    self.discountViewHeight.constant = 100;
    self.discountHeight.constant = 20;
}
-(void)hideDiscount{
    self.discountViewHeight.constant = 80;
    self.discountHeight.constant = 0.0;
}
#pragma mark - Class Methods

-(void)updatePayNowFrame{
    self.view.userInteractionEnabled = NO;
    _loader.hidden=NO;
    [_loader startAnimating];
    [_payNowButton setTitle:@"" forState:UIControlStateNormal];
    [UIView animateWithDuration:0.5 animations:^{
        self->_payNowWidth.constant = (self.payNowButton.frame.size.height*2)-self.view.frame.size.width;
    }];
}
-(void)removePayNowLoader{
    self.view.userInteractionEnabled = YES;
    _loader.hidden=YES;
    [_loader stopAnimating];
    [_payNowButton setTitle:@"Place Your Order" forState:UIControlStateNormal];
    [UIView animateWithDuration:0.5 animations:^{
        self->_payNowWidth.constant = 0.0;
    }];
}
-(void)updatePayPalFrame{
    self.view.userInteractionEnabled = NO;
    _loader.hidden=NO;
    [_loader startAnimating];//
    [_payPalPayNowButton setTitle:@"" forState:UIControlStateNormal];
    [_payPalPayNowButton setImage:nil forState:UIControlStateNormal];
    [UIView animateWithDuration:0.5 animations:^{
        self->_paypalWidth.constant = (self.payPalPayNowButton.frame.size.height*2)-self.view.frame.size.width;
    }];
}
-(void)removePayPalLoader{
    self.view.userInteractionEnabled = YES;
    _loader.hidden=YES;
    [_loader stopAnimating];
    [_payPalPayNowButton setTitle:@"Place Your Order" forState:UIControlStateNormal];
//    [_payPalPayNowButton setImage:[UIImage imageNamed:@"paypal_checkout_icon.png"] forState:UIControlStateNormal];
    [UIView animateWithDuration:0.5 animations:^{
        self->_paypalWidth.constant = 0.0;
    }];
}

-(void)deSelectAllPaymentButton{
    self.priceDetailContainerTop.constant = 5.0;
    self.paymentViewBottomBar.hidden = NO;
    self.payNowButton.hidden = YES;
    self.payPalPayNowButton.hidden = YES;
    self.westpacButton.selected = NO;
    self.paypalButton.selected = NO;
    [Global roundCorner:_paypalLogoImage roundValue:_paypalButton.frame.size.height/2 borderWidth:1.0 borderColor:APP_GRAY_COLOR];
    [Global roundCorner:_wespacLogoImage roundValue:_paypalButton.frame.size.height/2 borderWidth:1.0 borderColor:APP_GRAY_COLOR];
    _paypalLogoImage.image = [UIImage imageNamed:@"paypalunselectbtn.png"];
    _paypalLogoImage.backgroundColor = UIColor.whiteColor;
    _wespacLogoImage.backgroundColor = UIColor.whiteColor;
}



#pragma mark - API Methods

-(void)requestTransactionIdWidthUserAgent:(NSString *)userAgent{
    
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
    dic[@"shipping_mode"]=@"normal";
    if (_couponApplyButton.isSelected) {
        dic[@"promo_code"]=_couponTextField.text;
    }
    
    
//   else if (_giftCardApplyButton.isSelected) {
//        dic[@"promo_code"]=_giftCardTextField.text;
//    }
    else
        dic[@"promo_code"]=@"none";
    dic[@"user_notes"]=[_userNoteTextView.text isEqualToString:@"Write your special order requests here..."]?@"":_userNoteTextView.text;
    dic[@"shipping_address_id"]=_addressInfoDic[@"id"];
    dic[@"user_agent"]=userAgent;
    
    if (!([self.shippingLabel.text isKindOfClass:[NSNull class]]) ){
    
    if ([[self.shippingLabel.text stringByReplacingOccurrencesOfString:@"$" withString:@""] intValue] == 0) {
        dic[@"is_gift_card"] =@YES;
    }
    }
    dic[@"order_ids"]= _orderIdArray;
    dic[@"notifier_email"]=[[NSUserDefaults standardUserDefaults] valueForKey:EMAIL];
    dic[@"url"]=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,REQ_TRANSACTION_ID];
    
    NSMutableArray *arr=[[NSMutableArray alloc] init];
    arr=[db getDataForField:USERTABLE where:nil];
    NSLog(@"arr %@",arr);
    if (arr.count!=0) {
        dic[@"token"]=arr[0][@"token"];
        [api sendPostRequst:dic];
        
    }
    
    
    
    
}

-(void)completePayMentWithOption:(NSString *)type andInfo:(NSMutableDictionary *)infoDic{
    PopApi *api = [[PopApi alloc] init];
    api.delegate = self;
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
    
    dic[@"transaction_id"]= transactionId;
    
    
    if ([type isEqualToString:@"paypal"]) {
        
        NSMutableDictionary *paymentPayloaddic=[[NSMutableDictionary alloc] init];
        paymentPayloaddic[@"token"]=infoDic[@"token"];
        paymentPayloaddic[@"platform"]=@"iOS";
        paymentPayloaddic[@"version"]=@"1.0";
        paymentPayloaddic[@"environment"]=@"production";
        dic[@"device_data"] = infoDic[@"device_data"];
        dic[@"payment_payload"]=paymentPayloaddic;
        dic[@"payment_node"]=@"braintree";
    }
    else if ([type isEqualToString:@"promo"]){
        dic[@"payment_node"]=@"promo_code";
    }else if ([type isEqualToString:@"westpac"]) {
        
        NSMutableDictionary *paymentPayloaddic=[[NSMutableDictionary alloc] init];
       paymentPayloaddic[@"token"]=infoDic[@"singleUseTokenId"];
        paymentPayloaddic[@"platform"]=@"iOS";
        paymentPayloaddic[@"version"]=@"1.0";
        paymentPayloaddic[@"environment"]=@"production";
        
        dic[@"payment_payload"]=paymentPayloaddic;
        dic[@"payment_node"]=@"westpac";
    }
    
    NSMutableArray *arr=[[NSMutableArray alloc] init];
    arr=[db getDataForField:USERTABLE where:nil];
    NSLog(@"arr %@",arr);
    if (arr.count!=0) {
        dic[@"token"]=arr[0][@"token"];
    }
    dic[@"url"]=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,COMPLETE_PAYMENT];
    [api sendPostRequst:dic];
}




-(void)signUpUser{
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
   dic[@"name"]=[[UIDevice currentDevice] name];
    dic[@"email"]=[NSString stringWithFormat:@"%@@frameshop.com.au",[Global getDeviceId]];
    dic[@"password"]=@"qwxcjeub32ne";
    dic[@"c_password"]=@"qwxcjeub32ne";
    dic[@"url"]=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,REGISTER_USER];
    [api sendPostRequst:dic];
    
}
-(void)logInUser{
    
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
    
   dic[@"email"]=[NSString stringWithFormat:@"%@@frameshop.com.au",[Global getDeviceId]];
    dic[@"password"]=@"qwxcjeub32ne";
    
    dic[@"url"]=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,LOGIN_USER];
    [api sendPostRequst:dic];
}

-(void)applyDiscountWithCode:(NSString *)promoCode{
//    [_giftCardTextField resignFirstResponder];
    [_couponTextField resignFirstResponder];
    PopApi *api=[[PopApi alloc] init];
           api.delegate=self;
           NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
           
           dic[@"promocode"]=promoCode;
           dic[@"cart_amount"]=@(_cartAmount);
           dic[@"url"]=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,CHECK_COUPON];
           
           NSMutableArray *arr=[[NSMutableArray alloc] init];
           arr=[db getDataForField:USERTABLE where:nil];
           NSLog(@"arr %@",arr);
           if (arr.count!=0) {
               dic[@"token"]=arr[0][@"token"];
               [api sendPostRequst:dic];
               
           }
}


#pragma mark - Pay Pal Methods

-(void)loadPaypalPaymentView{
    
    [self showDropIn:BRAINTREE_API_LIVE];
}
- (void)fetchClientToken {
    // TODO: Switch this URL to your own authenticated API
    NSURL *clientTokenURL = [NSURL URLWithString:@"https://braintree-sample-merchant.herokuapp.com/client_token"];
    NSMutableURLRequest *clientTokenRequest = [NSMutableURLRequest requestWithURL:clientTokenURL];
    [clientTokenRequest setValue:@"text/plain" forHTTPHeaderField:@"Accept"];

    [[[NSURLSession sharedSession] dataTaskWithRequest:clientTokenRequest completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        // TODO: Handle errors
        NSString *clientToken = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
        [self showDropIn:clientToken];
        // As an example, you may wish to present Drop-in at this point.
        // Continue to the next section to learn more...
    }] resume];
}
- (void)showDropIn:(NSString *)clientTokenOrTokenizationKey {
    BTDropInRequest *request = [[BTDropInRequest alloc] init];
  
    
    NSString * str = self.shippingLabel.text;
    
    if (([self.shippingLabel.text isKindOfClass:[NSNull class]]) ){
        str = @"0";
    }
    
    BTPayPalRequest *paypalRequest = [[BTPayPalRequest alloc] initWithAmount:[NSString stringWithFormat:@"%f",_cartAmount+[[str stringByReplacingOccurrencesOfString:@"$" withString:@""] intValue]]];
    paypalRequest.currencyCode = @"AUD";
    request.payPalRequest = paypalRequest;
    
    BTDropInController *dropIn = [[BTDropInController alloc] initWithAuthorization:BRAINTREE_API_LIVE request:request handler:^(BTDropInController * _Nonnull controller, BTDropInResult * _Nullable result, NSError * _Nullable error) {
        
        if (error != nil) {
            NSLog(@"ERROR");
            [self removePayPalLoader];
//            [self.delegate paymentFailedOrCanceled:self->_index];
        } else if (result.cancelled) {
            NSLog(@"CANCELLED");
            [self dismissViewControllerAnimated:YES completion:nil];
            [self removePayPalLoader];
//            [self.delegate paymentFailedOrCanceled:self->_index];
        } else {
            [self dismissViewControllerAnimated:YES completion:nil];
            BTAPIClient *apiClient = [[BTAPIClient alloc] initWithAuthorization:BRAINTREE_API_LIVE];
            self->_dataCollector = [[BTDataCollector alloc] initWithAPIClient:apiClient];
            [self->_dataCollector collectDeviceData:^(NSString * _Nonnull deviceData) {
                NSLog(@"deviceData: %@", deviceData);
                NSMutableDictionary *dic =[[NSMutableDictionary alloc] init];
                dic[@"token"] = result.paymentMethod.nonce;
                dic[@"device_data"]    = deviceData;
                [self completePayMentWithOption:@"paypal" andInfo:dic];
            }];
           
            
            // Use the BTDropInResult properties to update your UI
            // result.paymentOptionType
            // result.paymentMethod
            // result.paymentIcon
            // result.paymentDescription
        }
    }];
    [self presentViewController:dropIn animated:YES completion:nil];
}
-(BOOL)isValidDetail{
    if (_orderIdArray==nil) {
           [Global showSimpleAlert:self andTitle:FRAMESHOP andMessage:@"Cart items are not loaded yet"];
           return NO;
       }
       else  if (_addressInfoDic==nil) {
           [Global showSimpleAlert:self andTitle:FRAMESHOP andMessage:@"Please select a Shipping Address"];
           return NO;
       }
       else if (![Validation isValidEmail: [[NSUserDefaults standardUserDefaults] valueForKey:EMAIL]])
       {
           [Global showSimpleAlert:self andTitle:FRAMESHOP andMessage:@"Please enter a valid email"];
           [self.delegate didAskToEmail:_index];
           return NO;
       }
    return YES;
}

-(void)stopLoader{
    
    [_couponActivity stopAnimating];
    [_giftCardActivity stopAnimating];
    _couponActivity.hidden=YES;
    _giftCardActivity.hidden=YES;
    [self removePayNowLoader];
    [self removePayPalLoader];
}

#pragma mark - Actions

- (IBAction)payWithPaypal:(id)sender{
    

    
    
    if (![self isValidDetail]) {
        return;
    }
    

    paymentType = @"paypal";
    [self updatePayPalFrame];
    WKWebView* webView = [[WKWebView alloc] initWithFrame:CGRectZero];
    webView.translatesAutoresizingMaskIntoConstraints=false;
    [self.view addSubview:webView];
    
    [webView evaluateJavaScript:@"navigator.userAgent" completionHandler:^(id result, NSError *error) {
        if (error == nil) {
            if (result != nil) {
//                NSString*  secretAgent = [NSString stringWithFormat:@"%@", result];
                //FrameShopiOS/version_number.build_number
                NSString *customUserAgent =[NSString stringWithFormat:@"FrameShopiOS/%@.%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"],[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"]];
                [self requestTransactionIdWidthUserAgent:customUserAgent];
                
                
            }
        } else {
            NSLog(@"evaluateJavaScript error : %@", error.localizedDescription);
        }
        
    }];
    
}



- (IBAction)payAction:(id)sender {
    if (![self isValidDetail]) {
              return;
          }
    paymentType = @"westpac";
    [self updatePayNowFrame];
    WKWebView* webView = [[WKWebView alloc] initWithFrame:CGRectZero];
    webView.translatesAutoresizingMaskIntoConstraints=false;
    [self.view addSubview:webView];

    [webView evaluateJavaScript:@"navigator.userAgent" completionHandler:^(id result, NSError *error) {
        if (error == nil) {
            if (result != nil) {
//                NSString*  secretAgent = [NSString stringWithFormat:@"%@", result];
NSString *customUserAgent =[NSString stringWithFormat:@"FrameShopiOS/%@.%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"],[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"]];
                [self requestTransactionIdWidthUserAgent:customUserAgent];


            }
        } else {
            NSLog(@"evaluateJavaScript error : %@", error.localizedDescription);
        }

    }];
    
}
-(void)generateSingleUseTokenWithServer{
    if (_cardNumberTF.text.length > 19 || _cardNumberTF.text.length < 19) {
        [Global showSimpleAlert:self andTitle:FRAMESHOP andMessage:@"Please enter valid card number"];
        [self removePayNowLoader];
        return;
    }
    if (_cvvTF.text.length < 3 || _cvvTF.text.length >3) {
        [Global showSimpleAlert:self andTitle:FRAMESHOP andMessage:@"Please enter valid cvv "];
        [self removePayNowLoader];
        return;
    }
    if (_cardHolderName.text.length < 4 ) {
        [Global showSimpleAlert:self andTitle:FRAMESHOP andMessage:@"Card holder name is too short! Please enter at least 4 characters."];
        [self removePayNowLoader];
        return;
    }
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
    dic[@"paymentMethod"]=@"creditCard";
    dic[@"cardNumber"]=self.cardNumberTF.text;
    dic[@"cardholderName"]=self.cardHolderName.text;
    dic[@"expiryDateMonth"]=[[self.expiryDateTF.text componentsSeparatedByString:@"/"] firstObject];//@"02";
    dic[@"expiryDateYear"]=[[self.expiryDateTF.text componentsSeparatedByString:@"/"] lastObject];//@"21";
    dic[@"cvn"]=self.cvvTF.text;//@"847";
    dic[@"url"] = @"https://api.payway.com.au/rest/v1/single-use-tokens";
    [self callWestPacPostApi:dic];
}
-(void)callWestPacPostApi:(NSMutableDictionary*)dic{
      NSString *urlString=dic[@"url"];
      NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
      [request setURL:[NSURL URLWithString:urlString]];
      [request setHTTPMethod:@"POST"];
      NSDictionary *headers = @{
        @"Authorization": @"Basic UTExNTgxX1BVQl93cnM1OG5zdXNqamZybml2Z3Y0bWM0YXpnOGd0dHp2YW4zM3lwY3A5amdxc2NkamV0bW03Njh3YTRqemY6"
      };

      [request setAllHTTPHeaderFields:headers];
      NSMutableData *body = [NSMutableData data];
      
      NSString *boundary = @"----WebKitFormBoundary7MA4YWxkTrZu0gW";
      NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
      [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
      
      //@"client_id"
      [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
      [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"paymentMethod\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
      [body appendData:[dic[@"paymentMethod"] dataUsingEncoding:NSUTF8StringEncoding]];
      [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
      
      [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
      [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"cardNumber\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
      [body appendData:[dic[@"cardNumber"] dataUsingEncoding:NSUTF8StringEncoding]];
      [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
      
      [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
      [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"cardholderName\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
      [body appendData:[dic[@"cardholderName"] dataUsingEncoding:NSUTF8StringEncoding]];
      [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
      
      [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
      [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"expiryDateMonth\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
      [body appendData:[dic[@"expiryDateMonth"] dataUsingEncoding:NSUTF8StringEncoding]];
      [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
      
    
      [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
      [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"expiryDateYear\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
      [body appendData:[dic[@"expiryDateYear"] dataUsingEncoding:NSUTF8StringEncoding]];
      [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"cvn\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[dic[@"cvn"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
      // close form
      [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
      // set request body
      [request setHTTPBody:body];
      NSError* error = nil;
      //return and test
      NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
      id jsonObject = [NSJSONSerialization JSONObjectWithData:returnData options:NSJSONReadingAllowFragments error:&error];
    //jsonObject[@"paymentMethod"]
    //transactionId = jsonObject[@"singleUseTokenId"];
    NSString *string = jsonObject[@"data"][0][@"message"];
    NSString *token = jsonObject[@"singleUseTokenId"];
    if(token != nil && ![token isEqualToString:@""]){
        [self completePayMentWithOption:@"westpac" andInfo:jsonObject];
    }else{
       [Global showSimpleAlert:self andTitle:FRAMESHOP andMessage:string];
        [self removePayPalLoader];
        [self removePayNowLoader];


        return;
    }
   
    
   

}

- (IBAction)onClickCouponApply:(id)sender {
    
    if ([_couponTextField.text isEqualToString:@""]){
        [_couponTextField resignFirstResponder];
        self.couponApplyButton.hidden = YES;
        return;
    }

    
    if (_couponApplyButton.isSelected) {
        _cartAmount=oldValue;
        
        if (!([self.shippingLabel.text isKindOfClass:[NSNull class]]) ){
        
        _totalPriceLabel.text=[NSString stringWithFormat:@"$%.02f",_cartAmount+[[self.shippingLabel.text stringByReplacingOccurrencesOfString:@"$" withString:@""] intValue]];
        }
        _discountTxt.text=[NSString stringWithFormat:@"$0"];
        _couponApplyButton.selected=NO;
        _couponTextField.text=@"";
        
        if (!([_discountTxt.text isKindOfClass:[NSNull class]]) ){
        
        if ([[_discountTxt.text stringByReplacingOccurrencesOfString:@"$" withString:@""] intValue] != 0) {
            [self showDiscount];
        }else{
            [self hideDiscount];
        }
        }
        return;
    }
        _couponApplyButton.enabled=false;
        _couponApplyButton.selected=YES;
        _couponActivity.hidden=false;
        [_couponActivity startAnimating];
        [self applyDiscountWithCode:_couponTextField.text];
    
    
}

//- (IBAction)onClickGiftCardApply:(id)sender {
//    
//   
//    
//     if (_giftCardApplyButton.isSelected) {
//          _cartAmount=oldValue;
//          _totalPriceLabel.text=[NSString stringWithFormat:@"$%.02f",_cartAmount];
//          _discountTxt.text=[NSString stringWithFormat:@"$0"];
//          _giftCardApplyButton.selected=NO;
//          _giftCardTextField.text=@"";
//      }
//      else{
//           _giftCardApplyButton.enabled=false;
//          _giftCardApplyButton.selected=YES;
//          _giftCardActivity.hidden=false;
//          [_giftCardActivity startAnimating];
//          [self applyDiscountWithCode:_giftCardTextField.text];
//      }
//}


- (IBAction)onClickHaveAvoucher:(id)sender {
    
//    if (_haveGiftCardBtn.isSelected) {
//        [self onClickHaveAGiftCard:_haveGiftCardBtn];
//        if (_giftCardApplyButton.isSelected) {
//            [self onClickGiftCardApply:_giftCardApplyButton];
//        }
//    }
    
    _haveVoucherBtn.selected = !_haveVoucherBtn.selected;
    if (_haveVoucherBtn.selected) {
        _couponHeight.constant = 35.0;
        self.scrollView.contentSize = CGSizeMake(_scrollView.contentSize.width, _scrollView.contentSize.height+30);
    }else{
        _couponHeight.constant = 0.0;
        self.scrollView.contentSize = CGSizeMake(_scrollView.contentSize.width, _scrollView.contentSize.height-30);
    }
}

- (IBAction)onClickHaveAGiftCard:(id)sender {
    
//    if (_haveVoucherBtn.isSelected ) {
//           [self onClickHaveAvoucher:_haveVoucherBtn];
//        if (_couponApplyButton.isSelected) {
//            [self onClickCouponApply:_couponApplyButton];
//        }
//       }
    
    _haveGiftCardBtn.selected = !_haveGiftCardBtn.selected;
    if (_haveGiftCardBtn.selected) {
        _giftCardHeight.constant = 80.0;
        self.scrollView.contentSize = CGSizeMake(_scrollView.contentSize.width, _scrollView.contentSize.height+80);
    }else{
        _giftCardHeight.constant = 0.0;
        self.scrollView.contentSize = CGSizeMake(_scrollView.contentSize.width, _scrollView.contentSize.height-80);
    }
}


- (IBAction)onClickPaypal:(id)sender {
    [self deSelectAllPaymentButton];
    self.paypalButton.selected = YES;
//    if (self.paypalButton.selected) {
        self.payPalPayNowButton.hidden = NO;
        self.paymentView.hidden = YES;
        self.paymentViewBottomBar.hidden = YES;
//        if ([[[NSString stringWithFormat:@"$%.02f",_cartAmount+[[self.shippingLabel.text stringByReplacingOccurrencesOfString:@"$" withString:@""] intValue]] stringByReplacingOccurrencesOfString:@"$" withString:@""] intValue] != 0) {
//             self.payPalPayNowButton.backgroundColor = [UIColor colorWithRed:60.0/255.0 green:167/255.0 blue:230/255.0 alpha:1.0];
//            [self.payPalPayNowButton setTitle:@"" forState:UIControlStateNormal];
//            [self.payPalPayNowButton setImage:[UIImage imageNamed:@"paypal_checkout_icon.png"] forState:UIControlStateNormal];
//        }else{
//            [self.payPalPayNowButton setImage:nil forState:UIControlStateNormal];
//            [self.payPalPayNowButton setTitle:@"Place Order" forState:UIControlStateNormal];
//            self.payPalPayNowButton.backgroundColor = APP_BLUE_COLOR;
//
//        }
       
        self.priceDetailContainerTop.constant = 5-self.paymentView.frame.size.height;
        [Global roundCorner:_paypalLogoImage roundValue:_paypalButton.frame.size.height/2 borderWidth:1.0 borderColor:APP_BLUE_COLOR];
        [Global roundCorner:_wespacLogoImage roundValue:_paypalButton.frame.size.height/2 borderWidth:1.0 borderColor:APP_GRAY_COLOR];
        _paypalLogoImage.image = [UIImage imageNamed:@"paypalselectbtn.png"];
        _paypalLogoImage.backgroundColor = [UIColor whiteColor];
        _wespacLogoImage.backgroundColor = APP_GRAY_COLOR;
        
//    }
    
    
}

- (IBAction)onClickWestpac:(id)sender {
    [self deSelectAllPaymentButton];
    self.westpacButton.selected = YES;
    if (self.westpacButton.selected) {
        self.payNowButton.hidden = NO;
        self.paymentView.hidden = NO;
        [Global roundCorner:_wespacLogoImage roundValue:_paypalButton.frame.size.height/2 borderWidth:1.0 borderColor:APP_BLUE_COLOR];
        [Global roundCorner:_paypalLogoImage roundValue:_paypalButton.frame.size.height/2 borderWidth:1.0 borderColor:APP_GRAY_COLOR];
        _paypalLogoImage.image = [UIImage imageNamed:@"paypalunselectbtn.png"];
        _paypalLogoImage.backgroundColor = APP_GRAY_COLOR;
               _wespacLogoImage.backgroundColor = [UIColor whiteColor];
    }
}

- (IBAction)onClickTermAndCondition:(id)sender {
    [self performSegueWithIdentifier:@"loadWeb" sender:@"Terms & Conditions"];
}
- (IBAction)onClickPrivacyPolicy:(id)sender {
    [self performSegueWithIdentifier:@"loadWeb" sender:@"Privacy Policy"];
}

#pragma mark - Pay Pal Delegates

//- (void)payPalPaymentViewController:(PayPalPaymentViewController *)paymentViewController
//                 didCompletePayment:(PayPalPayment *)completedPayment {
//    
//     [self dismissViewControllerAnimated:NO completion:nil];
//    [self completePayMentWithOption:@"payPal" andInfo:[completedPayment.confirmation mutableCopy]];
//    
//}
//
//- (void)payPalPaymentDidCancel:(nonnull PayPalPaymentViewController *)paymentViewController {
//    [self dismissViewControllerAnimated:YES completion:^{
//        NSLog(@"Cancel paypal Payment process");
//        [self removePayPalLoader];
//    }];
//}
//



#pragma mark - Text Field Delegates

-(BOOL)textField:(UITextField *)textField
shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string
{
    if (textField == self.cardNumberTF) {
        if (textField.text.length > 18 && [string isEqualToString:@""]) {
            previousTextFieldContent = textField.text;
            previousSelection = textField.selectedTextRange;
            return YES;
        }
        else  if (textField.text.length > 18){
            [self.cardNumberTF resignFirstResponder];
            [self.expiryDateTF becomeFirstResponder];
            return NO;
        }
        
    }
    else if (textField == self.expiryDateTF) {
        if (textField.text.length > 4 && [string isEqualToString:@""]) {
            return YES;
        }
        else  if (textField.text.length > 4){
            [self.expiryDateTF resignFirstResponder];
            [self.cvvTF becomeFirstResponder];
            return NO;
        }
        
    }
    else if (textField == self.cvvTF) {
        if (textField.text.length > 2 && [string isEqualToString:@""]) {
            return YES;
        }
        else  if (textField.text.length > 2){
            [self.cvvTF resignFirstResponder];
            [self.cardHolderName becomeFirstResponder];
            return NO;
        }
        
    }
    
    else if (textField == self.couponTextField) {
        self.couponApplyButton.hidden = NO;
        _couponApplyButton.enabled=YES;
        self->_couponApplyButton.selected=false;
//        self.giftCardApplyButton.hidden = YES;
//        if (self.couponApplyButton.isSelected) {
//            [self onClickCouponApply:_couponApplyButton];
//        }
        
        return YES;
    }
//    else if (textField == self.giftCardTextField) {
//        self.couponApplyButton.hidden = YES;
//        self.giftCardApplyButton.hidden = NO;
//        if (self.giftCardApplyButton.isSelected) {
//            [self onClickGiftCardApply:_giftCardApplyButton];
//        }
//        return YES;
//    }
    return YES;
}

#pragma mark - Card format methods

-(void)reformatAsCardNumber:(UITextField *)textField
{
    NSUInteger targetCursorPosition =
    [textField offsetFromPosition:textField.beginningOfDocument
                       toPosition:textField.selectedTextRange.start];
    
    NSString *cardNumberWithoutSpaces =
    [self removeNonDigits:textField.text
andPreserveCursorPosition:&targetCursorPosition];
    
    if ([cardNumberWithoutSpaces length] > 19) {
        [textField setText:previousTextFieldContent];
        textField.selectedTextRange = previousSelection;
        return;
    }
    
    NSString *cardNumberWithSpaces =
    [self insertCreditCardSpaces:cardNumberWithoutSpaces
       andPreserveCursorPosition:&targetCursorPosition];
    
    textField.text = cardNumberWithSpaces;
    UITextPosition *targetPosition =
    [textField positionFromPosition:[textField beginningOfDocument]
                             offset:targetCursorPosition];
    
    [textField setSelectedTextRange:
     [textField textRangeFromPosition:targetPosition
                           toPosition:targetPosition]
     ];
}



- (NSString *)removeNonDigits:(NSString *)string
    andPreserveCursorPosition:(NSUInteger *)cursorPosition
{
    NSUInteger originalCursorPosition = *cursorPosition;
    NSMutableString *digitsOnlyString = [NSMutableString new];
    for (NSUInteger i=0; i<[string length]; i++) {
        unichar characterToAdd = [string characterAtIndex:i];
        if (isdigit(characterToAdd)) {
            NSString *stringToAdd =
            [NSString stringWithCharacters:&characterToAdd
                                    length:1];
            
            [digitsOnlyString appendString:stringToAdd];
        }
        else {
            if (i < originalCursorPosition) {
                (*cursorPosition)--;
            }
        }
    }
    
    return digitsOnlyString;
}
- (NSString *)insertCreditCardSpaces:(NSString *)string
           andPreserveCursorPosition:(NSUInteger *)cursorPosition
{
    bool is456 = [string hasPrefix: @"1"];
    bool is465 = [string hasPrefix: @"34"] ||
    [string hasPrefix: @"37"] ||
    
    // Diners Club
    [string hasPrefix: @"300"] ||
    [string hasPrefix: @"301"] ||
    [string hasPrefix: @"302"] ||
    [string hasPrefix: @"303"] ||
    [string hasPrefix: @"304"] ||
    [string hasPrefix: @"305"] ||
    [string hasPrefix: @"309"] ||
    [string hasPrefix: @"36"] ||
    [string hasPrefix: @"38"] ||
    [string hasPrefix: @"39"];
    bool is4444 = !(is456 || is465);
    
    NSMutableString *stringWithAddedSpaces = [NSMutableString new];
    NSUInteger cursorPositionInSpacelessString = *cursorPosition;
    for (NSUInteger i=0; i<[string length]; i++) {
        bool needs465Spacing = (is465 && (i == 4 || i == 10 || i == 15));
        bool needs456Spacing = (is456 && (i == 4 || i == 9 || i == 15));
        bool needs4444Spacing = (is4444 && i > 0 && (i % 4) == 0);
        
        if (needs465Spacing || needs456Spacing || needs4444Spacing) {
            [stringWithAddedSpaces appendString:@" "];
            if (i < cursorPositionInSpacelessString) {
                (*cursorPosition)++;
            }
        }
        unichar characterToAdd = [string characterAtIndex:i];
        NSString *stringToAdd =
        [NSString stringWithCharacters:&characterToAdd length:1];
        
        [stringWithAddedSpaces appendString:stringToAdd];
    }
    
    return stringWithAddedSpaces;
}
- (void)creditCardExpiryFormatter:(id)sender {
    if (self.expiryDateTF.text.length == 2) {
        self.expiryDateTF.text = [NSString stringWithFormat:@"%@", [self.expiryDateTF.text substringToIndex:self.expiryDateTF.text.length]];
    }
    else if (self.expiryDateTF.text.length == 4) {
        if (![self.expiryDateTF.text containsString:@"/"]) {
            self.expiryDateTF.text = [NSString stringWithFormat:@"%@/%@", [self.expiryDateTF.text substringToIndex:2], [self.expiryDateTF.text substringFromIndex:2]];
        }
        
    }
}


#pragma mark - Segue support

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    
    
    if ([[segue identifier] isEqualToString:@"dis"]) {
        
        DiscountPopUp *ditalView = [segue destinationViewController];
        ditalView.valueTxt=(NSString *)sender;
        ditalView.couponCodeName  = self.couponTextField.text;//[self.couponTextField.text isEqualToString:@""]?self.giftCardTextField.text:self.couponTextField.text;
        
        
    }else if ([[segue identifier] isEqualToString:@"loadWeb"]) {
        WebViewController *webView = [segue destinationViewController];
        webView.type = (NSString*) sender;
        webView.isFrom = @"payment";
        if ([(NSString*)sender isEqualToString:@"Terms & Conditions"]){
            webView.url = TERMS_AND_CONDITION_URL;
        }else{
            webView.url = PRIVACY_URL;
        }
    }
    
    
}

#pragma mark -Pop api Delegates

-(void)connectionError{
    [Global showSimpleAlert:self andTitle:FRAMESHOP andMessage:NO_INTERNET_MSG];
    _couponApplyButton.enabled=YES;
//    _giftCardApplyButton.enabled=YES;
    
    if (_couponApplyButton.isSelected ) {
        self->_couponApplyButton.selected=false;
//        self->_giftCardApplyButton.selected=false;
    }
    
    [self stopLoader];
    
}
-(void)requestFailedSessionExpired:(NSDictionary *)dataPack{
    [self stopLoader];
    
}
-(void)requestFailedServerError:(NSDictionary *)dataPack{
    [self stopLoader];
    
}
-(void)requestFailedServiceUnavailableDueToMaintenance:(NSDictionary *)dataPack{
    [self stopLoader];
    [Global showSimpleAlert:self andTitle:FRAMESHOP andMessage:dataPack[@"message"]];
}
-(void)requestFailedMethodNotFound:(NSDictionary *)dataPack{
    [self stopLoader];
    
}
-(void)requestFailedNotFound:(NSDictionary *)dataPack{
    [self stopLoader];
    if ([dataPack[@"error"] isEqualToString:@"Promocode is Invalid or It has expired!"]) {
        [Global showSimpleAlert:self andTitle:FRAMESHOP andMessage:dataPack[@"message"]];
    }
    
}
-(void)requestFailedUnauthorised:(NSDictionary *)dataPack{
     [self logInUser];
    [self stopLoader];
    
}
-(void)requestFailedBadRequest:(NSDictionary *)dataPack{
    [self stopLoader];
    
}
-(void)requestSucceededWithData:(NSDictionary *)dataPack{
    
    if ([dataPack[@"url"] isEqualToString:LOGIN_USER]) {
        
        [db delTableWithTableName:USERTABLE];
        NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
        
        dic[@"id"]              =[NSString stringWithFormat:@"%@",dataPack[@"data"][@"id"]];
        dic[@"name"]            =dataPack[@"data"][@"name"];
        dic[@"email"]           =dataPack[@"data"][@"email"];
        dic[@"created_at"]      =dataPack[@"data"][@"created_at"];
        dic[@"updated_at"]      =dataPack[@"data"][@"updated_at"];
        dic[@"token"]           =dataPack[@"data"][@"access_token"];
        
        [db insertIntoTable:dic table_name:USERTABLE];
        [self payAction:_payNowButton];
    }
    
    else if ([dataPack[@"url"] isEqualToString:REGISTER_USER]){
        
        NSMutableDictionary *dic =[[NSMutableDictionary alloc] init];
        dic[@"token"]            =dataPack[@"data"][@"token"];
        dic[@"id"]               =[NSString stringWithFormat:@"%@",dic[@"id"]];
        
        [db insertIntoTable:dic table_name:USERTABLE];
        [self payAction:_payNowButton];
    }
    else  if ([dataPack[@"url"] isEqualToString:REQ_TRANSACTION_ID]){
        
        transactionId=dataPack[@"data"][@"transaction_id"];
        
        //([dataPack[@"data"][@"pay_state"] isEqualToString:@"fully_paid_by_coupon"])
        NSString *amount = [NSString stringWithFormat:@"%@",dataPack[@"data"][@"payable_amount"]];
        
        if (!([self.shippingLabel.text isKindOfClass:[NSNull class]]) ){
        
        if (([amount isEqualToString:@"0"] || _cartAmount==0) && [[self.shippingLabel.text stringByReplacingOccurrencesOfString:@"$" withString:@""] intValue] == 0) {
            [self completePayMentWithOption:@"promo" andInfo:dataPack[@"data"]];
        }
        else if ([paymentType isEqualToString:@"westpac"]) {
            [self generateSingleUseTokenWithServer];
        }else if ([paymentType isEqualToString:@"paypal"]) {
            [self loadPaypalPaymentView];
        }
        }
    }
    
    else  if ([dataPack[@"url"] isEqualToString:COMPLETE_PAYMENT]){
        [Global deleteFilesFromCacheWhichContain:@"cart_product"];
        [Global deleteFilesFromCacheWhichContain:@"frame"];
        [Global deleteFilesFromCacheWhichContain:@"preview"];
    [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"gift_shipping"];
       [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"product_shipping"];
        [self removePayPalLoader];
        [self removePayNowLoader];
        if ([dataPack[@"data"][@"Payment"][@"payment_status"] isEqualToString:@"payment_failed"]) {
             [Global showSimpleAlert:self andTitle:FRAMESHOP andMessage:dataPack[@"message"]];
        }else{
           
        NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
        dic[@"code"]=@"2";
        
        [[NSUserDefaults standardUserDefaults] setValue:nil forKey:CART_COUNT];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:SHOW_RATING];
        [db delTableWithTableName:CART_TABLE];
        NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
        [nc postNotificationName:FINISH_UPLOADING object:self userInfo:dic];
        [self dismissViewControllerAnimated:NO completion:nil];
        }
    }
    
    
    else{
        [self stopLoader];
        _couponApplyButton.enabled=YES;
//        _giftCardApplyButton.enabled=YES;
        _cartAmount=[dataPack[@"data"][@"after_promocode_applied_price"] floatValue];
        
        if (!([self.shippingLabel.text isKindOfClass:[NSNull class]]) ){
        
        _totalPriceLabel.text=[NSString stringWithFormat:@"$%.02f",_cartAmount+[[self.shippingLabel.text stringByReplacingOccurrencesOfString:@"$" withString:@""] intValue]];
            
        }
        
        _discountTxt.text=[NSString stringWithFormat:@"-$%.02f",oldValue-_cartAmount];
        if (!([_discountTxt.text isKindOfClass:[NSNull class]]) ){
            
       
        
        if ([[_discountTxt.text stringByReplacingOccurrencesOfString:@"$" withString:@""] intValue] != 0) {
            [self showDiscount];
        }else{
            [self hideDiscount];
        }
        }
        NSArray *arr=dataPack[@"data"][@"promocode_details"];
        if (arr.count!=0) {
            if ([arr[0][@"promo_type"] isEqualToString:@"percentage_off"]) {
                [self performSegueWithIdentifier:@"dis" sender:[NSString stringWithFormat:@"%@%%",arr[0][@"deduction_value"]]];
            }
            else if ([arr[0][@"promo_type"] isEqualToString:@"currency_off"]){
                 [self performSegueWithIdentifier:@"dis" sender:[NSString stringWithFormat:@"%.0f$",oldValue-_cartAmount]];
            }
           
           
            
           
        }
        
        if ([dataPack[@"data"][@"after_promocode_applied_price"] floatValue]==0) {
         
            [self onClickPaypal:_paypalButton];
        }
        
        
    }
    
    
    
    
}
-(void)requestSucceededButActionFailed:(NSDictionary *)dataPack{
    [self stopLoader];
    _couponApplyButton.enabled=YES;
    self->_couponApplyButton.selected=true;
//    _giftCardApplyButton.enabled=YES;
    if ([dataPack[@"url"] isEqualToString:CHECK_COUPON]){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"FrameShop" message:dataPack[@"message"] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            //button click event
//            self->_giftCardApplyButton.selected=false;
            
        }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }else  if ([dataPack[@"url"] isEqualToString:COMPLETE_PAYMENT]){
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
}
-(void)uploadedData:(NSString *)pecentage andByte:(float)bytes{
    
}
-(void)requestSucceededButNoDataFound:(NSDictionary *)dataPack{
    [self stopLoader];
}
-(void)requestFailedDueToServerIsuueWithError:(NSString*)errorDes{
    [self stopLoader];
}

#pragma mark - BTViewControllerPresentingDelegate

// Required
- (void)paymentDriver:(id)paymentDriver
requestsPresentationOfViewController:(UIViewController *)viewController {
    [self presentViewController:viewController animated:YES completion:nil];
}

// Required
- (void)paymentDriver:(id)paymentDriver
requestsDismissalOfViewController:(UIViewController *)viewController {
    [viewController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - BTAppSwitchDelegate

// Optional - display and hide loading indicator UI
- (void)appSwitcherWillPerformAppSwitch:(id)appSwitcher {
    [self showLoadingUI];
    
    // You may also want to subscribe to UIApplicationDidBecomeActiveNotification
    // to dismiss the UI when a customer manually switches back to your app since
    // the payment button completion block will not be invoked in that case (e.g.
    // customer switches back via iOS Task Manager)
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(hideLoadingUI:)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
}

- (void)appSwitcherWillProcessPaymentInfo:(id)appSwitcher {
    [self hideLoadingUI:nil];
}

- (void)appSwitcher:(nonnull id)appSwitcher didPerformSwitchToTarget:(BTAppSwitchTarget)target {
    //
}

#pragma mark - Private methods

- (void)showLoadingUI {
    //
}

- (void)hideLoadingUI:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidBecomeActiveNotification
                                                  object:nil];
    //
}
#pragma mark - UITextView delegates
- (BOOL) textViewShouldBeginEditing:(UITextView *)textView {
    _userNoteTextView.text = @"";
    _userNoteTextView.textColor = [UIColor blackColor];
    return YES;
}

-(void) textViewDidChange:(UITextView *)textView {
    
    if(_userNoteTextView.text.length == 0) {
        _userNoteTextView.textColor = APP_GRAY_COLOR;
        _userNoteTextView.text = @"Write your special order requests here...";
        [_userNoteTextView resignFirstResponder];
    }
}
-(BOOL) textViewShouldEndEditing:(UITextView *)textView {
    [_userNoteTextView resignFirstResponder];
    if(_userNoteTextView.text.length == 0) {
        _userNoteTextView.textColor = APP_GRAY_COLOR;
        _userNoteTextView.text = @"Write your special order requests here...";
        return YES;
    }
    return YES;
}
- (BOOL)textView:(UITextView *)txtView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
   
//    if( [text rangeOfCharacterFromSet:[NSCharacterSet newlineCharacterSet]].location == NSNotFound ) {
//        return YES;
//    }
//    [txtView resignFirstResponder];
    
    int limit = 119;
    if (![_userNoteSubLabel.text isEqualToString:@"Characters Remaining: 0"]) {
        _userNoteSubLabel.text = [NSString stringWithFormat:@"Characters Remaining:: %lu",120 - [txtView.text length]];
        if ([_userNoteSubLabel.text isEqualToString:@"Characters Remaining: 119"] && [text isEqualToString:@""]) {
            _userNoteSubLabel.text = @"Characters Remaining:: 120";
        }
    }else{
       if ([_userNoteSubLabel.text isEqualToString:@"Characters Remaining:: 0"] && [text isEqualToString:@""]) {
            _userNoteSubLabel.text = @"Characters Remaining: 1";
        }
    }
    
    NSLog(@"%lu",(unsigned long)[txtView.text length]);
    return !([txtView.text length]>limit && [text length] > range.length);
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}
@end
