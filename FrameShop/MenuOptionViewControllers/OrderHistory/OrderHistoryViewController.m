//
//  OrderHistoryViewController.m
//  FrameShop
//
//  Created by Saurabh Srivastav on 02/03/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//
#import "OrderHistoryViewController.h"
#import "Global.h"
@interface OrderHistoryViewController (){
    BOOL isLoaded,isNextPageLoading;
    NSMutableArray *dataArray;
    DbManager *db;
    WarningSubView *subView;
    NSString *nextUrl;
}

@end

@implementation OrderHistoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    isLoaded=NO;
    dataArray=[[NSMutableArray alloc] init];
    db=[[DbManager alloc] init];
 
    [self getOrderDataFromServerDataWithUrl:[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,GET_ORDER_HOSTORY]];
    [Global roundCorner:_continueShopping roundValue:_continueShopping.frame.size.height/2 borderWidth:0.0 borderColor:UIColor.clearColor];
    self.continueShopping.hidden = YES;
    _activityBottom.hidden=YES;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationItem setBackBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil]];
    
    self.navigationController.navigationBar.tintColor=[UIColor blackColor];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:SHOW_RATING] == YES) {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:SHOW_RATING];
       // [self performSegueWithIdentifier:@"rating" sender:nil];
        [SKStoreReviewController requestReview];
    }
}


- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    [subView.view removeFromSuperview];
    isLoaded=NO;
    [_tableView reloadData];
    
    [self performSelector:@selector(getData) withObject:nil afterDelay:1.0];
    
    //[self getCartDataFromServerData];
}

-(void)getData{
    nextUrl=@"";
    [self getOrderDataFromServerDataWithUrl:[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,GET_ORDER_HOSTORY]];
}
- (IBAction)continueShoppingAction:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)getOrderDataFromServerDataWithUrl:(NSString *)url{
    
    //    dataArray=[[db getDataForField:CART_TABLE where:nil] mutableCopy];
    //       if (dataArray.count!=0) {
    //           isLoaded=YES;
    //           [_tableView reloadData];
    //
    //
    //       }
    
    
    
    
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    NSString *urlString=url;
    
    NSMutableArray *arr=[[NSMutableArray alloc] init];
    arr=[db getDataForField:USERTABLE where:nil];
    NSLog(@"arr %@",arr);
    if (arr.count!=0) {
        
        
        [api sendGetRequst:urlString andToken:arr[0][@"token"]];
    }
    else
        [self logInUser];
    
}

-(void)signUpUser{
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
    dic[@"name"]=[[UIDevice currentDevice] name];
    dic[@"email"]=[NSString stringWithFormat:@"%@@frameshop.com.au",[Global getDeviceId]];
    dic[@"password"]=@"qwxcjeub32ne";
    dic[@"c_password"]=@"qwxcjeub32ne";
    dic[@"url"]=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,REGISTER_USER];
    [api sendPostRequst:dic];
    
}
-(void)logInUser{
    
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
    
    dic[@"email"]=[NSString stringWithFormat:@"%@@frameshop.com.au",[Global getDeviceId]];
    dic[@"password"]=@"qwxcjeub32ne";
    
    dic[@"url"]=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,LOGIN_USER];
    [api sendPostRequst:dic];
}

-(void)showNoItem{
    self.continueShopping.hidden = NO;
    [dataArray removeAllObjects];
    isLoaded=YES;
    [subView.view removeFromSuperview];
    if (subView==nil) {
        subView=[[WarningSubView alloc] init];
    }
    subView.view.frame = CGRectMake( _tableView.frame.origin.x,  _tableView.frame.origin.y,  _tableView.frame.size.width,
                                    self.tableView.frame.size.height);
   
    [self.view addSubview:subView.view];
    subView.titleText.text=NO_ITEM_ORDERHISTORY;
    subView.noCartIcon.image=[UIImage imageNamed:@"emptyorder.png"];
    subView.subTitleTxt.text = NO_ITEM_ORDERHISTORY_SUB;
    _tableView.backgroundColor=[UIColor clearColor];
    [_tableView reloadData];
    [_activityBottom stopAnimating];
    _activityBottom.hidden=YES;
}




- (IBAction)detail:(id)sender {
    [self performSegueWithIdentifier:@"det" sender:nil];
}

#pragma mark - Scroll View Delegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    CGFloat height = scrollView.frame.size.height;

          CGFloat contentYoffset = scrollView.contentOffset.y;

          CGFloat distanceFromBottom = scrollView.contentSize.height - contentYoffset;

          if(distanceFromBottom <= height)
          {
              if (nextUrl != nil && nextUrl != (id)[NSNull null] && ![nextUrl isEqualToString:@""] && isLoaded && !isNextPageLoading) {
                  isNextPageLoading=YES;
                  _activityBottom.hidden=false;
                  [_activityBottom startAnimating];
                  [self getOrderDataFromServerDataWithUrl:nextUrl];
              }
          }
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return (isLoaded)?dataArray.count:5;
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    if (isLoaded) {
    static NSString *simpleTableIdentifier = @"OrderCellTableViewCell";
    OrderCellTableViewCell *cell = (OrderCellTableViewCell *)[_tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"OrderCellTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    
    if (isLoaded) {
        
        
        cell.orderStatus.backgroundColor         =[UIColor clearColor];
        cell.detailBtn.hidden=false;
        cell.orderDate.hidden=false;
        cell.cost.hidden=false;
        cell.status.hidden=false;
        cell.transactionNumber.hidden=false;
       
        cell.transactionIdTitle.backgroundColor=[UIColor clearColor];
        cell.costTitle.backgroundColor=[UIColor clearColor];
        cell.orderDateTitile.backgroundColor=[UIColor clearColor];
        cell.cellHeading.backgroundColor=[UIColor clearColor];
        cell.pro1.backgroundColor=[UIColor clearColor];
        cell.pro2.backgroundColor=[UIColor clearColor];
        cell.pro3.backgroundColor=[UIColor clearColor];
        cell.pro4.backgroundColor=[UIColor clearColor];
        cell.more.backgroundColor=[UIColor colorWithRed:158/225 green:158/255 blue:162/2255 alpha:0.5];
        cell.infoDic=dataArray[indexPath.row];
        [cell setLbl];
        
        [Global stopShimmeringToView:cell.contentView];
        
        
        
        
        
    }
    else{
        
        
        cell.transactionIdTitle.backgroundColor=[UIColor groupTableViewBackgroundColor];
        cell.costTitle.backgroundColor=[UIColor groupTableViewBackgroundColor];
        cell.orderDateTitile.backgroundColor=[UIColor groupTableViewBackgroundColor];
        cell.cellHeading.backgroundColor=[UIColor groupTableViewBackgroundColor];
        
        cell.status.hidden=YES;
        
        
        cell.orderStatus.backgroundColor   =[UIColor groupTableViewBackgroundColor];
        
        
        cell.orderDate.hidden=YES;
        cell.cost.hidden=YES;
        cell.transactionNumber.hidden=YES;
        
        cell.transactionIdTitle.text=@"";
        cell.costTitle.text=@"";
        cell.orderDateTitile.text=@"";
        cell.cellHeading.text=@"";
        cell.orderStatus.text=@"";
        cell.more.text=@"";
        cell.orderView.backgroundColor=[UIColor groupTableViewBackgroundColor];
        cell.detailBtn.hidden=YES;
        cell.pro1.backgroundColor=[UIColor groupTableViewBackgroundColor];
        cell.pro2.backgroundColor=[UIColor groupTableViewBackgroundColor];
        cell.pro3.backgroundColor=[UIColor groupTableViewBackgroundColor];
        cell.pro4.backgroundColor=[UIColor groupTableViewBackgroundColor];
        cell.more.backgroundColor=[UIColor groupTableViewBackgroundColor];
        
        [Global startShimmeringToView:cell.contentView];
    }
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    
    return cell;
    
    
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (isLoaded) {
        [self performSegueWithIdentifier:@"detail" sender:dataArray[indexPath.row]];
    }
    
    
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSArray *nibViews = [[NSBundle mainBundle] loadNibNamed:@"OrderCellTableViewCell" owner:self options:nil];
    OrderCellTableViewCell *sqr = [nibViews objectAtIndex:0];
    return sqr.frame.size.height;
    
    
}



- (IBAction)closeAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Segue support

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    
    
    if ([[segue identifier] isEqualToString:@"detail"]) {
        
        OrderHistoryDetailViewController *ditalView = [segue destinationViewController];
        
        ditalView.infoDic=[(NSMutableDictionary *)sender mutableCopy];
        
    }
    
    
}

#pragma mark -Pop api Delegates

-(void)connectionError{
    if (dataArray.count==0) {
        [_activityBottom stopAnimating];
        _activityBottom.hidden=YES;
        [subView.view removeFromSuperview];
        
        
        if (subView==nil) {
            subView=[[WarningSubView alloc] init];
        }
        subView.view.frame=
        CGRectMake( _tableView.frame.origin.x,  _tableView.frame.origin.y,  _tableView.frame.size.width, self.tableView.frame.size.height);
        subView.titleText.text=NO_INTERNET_MSG;
        subView.subTitleTxt.text=@"Check you internet connection. Tap try again";
        subView.noCartIcon.image=[UIImage imageNamed:@"noInternet.png"];
        [self.view addSubview:subView.view];
        UITapGestureRecognizer *singleFingerTap =
        [[UITapGestureRecognizer alloc] initWithTarget:self
                                                action:@selector(handleSingleTap:)];
        [subView.view addGestureRecognizer:singleFingerTap];
       
    }
    
    isLoaded=NO;
    [_tableView reloadData];
}
-(void)requestFailedSessionExpired:(NSDictionary *)dataPack{
    
}
-(void)requestFailedServerError:(NSDictionary *)dataPack{
    
}
-(void)requestFailedServiceUnavailableDueToMaintenance:(NSDictionary *)dataPack{
        [self showNoItem];
    [Global showSimpleAlert:self andTitle:FRAMESHOP andMessage:dataPack[@"message"]];
}
-(void)requestFailedMethodNotFound:(NSDictionary *)dataPack{
    
}
-(void)requestFailedNotFound:(NSDictionary *)dataPack{
    
}
-(void)requestFailedUnauthorised:(NSDictionary *)dataPack{
     [self logInUser];
}
-(void)requestFailedBadRequest:(NSDictionary *)dataPack{
    
}
-(void)requestSucceededWithData:(NSDictionary *)dataPack{
    
    if ([dataPack[@"url"] isEqualToString:LOGIN_USER]) {
        
        [db delTableWithTableName:USERTABLE];
        NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
        
        dic[@"id"]              =[NSString stringWithFormat:@"%@",dataPack[@"data"][@"id"]];
        dic[@"name"]            =dataPack[@"data"][@"name"];
        dic[@"email"]           =dataPack[@"data"][@"email"];
        dic[@"created_at"]      =dataPack[@"data"][@"created_at"];
        dic[@"updated_at"]      =dataPack[@"data"][@"updated_at"];
        dic[@"token"]           =dataPack[@"data"][@"access_token"];
        
        [db insertIntoTable:dic table_name:USERTABLE];
        [self getOrderDataFromServerDataWithUrl:nextUrl];
    }
    
    else if ([dataPack[@"url"] isEqualToString:REGISTER_USER]){
        
        NSMutableDictionary *dic =[[NSMutableDictionary alloc] init];
        dic[@"token"]            =dataPack[@"data"][@"token"];
        dic[@"id"]               =[NSString stringWithFormat:@"%@",dic[@"id"]];
        
        [db insertIntoTable:dic table_name:USERTABLE];
        [self getOrderDataFromServerDataWithUrl:nextUrl];
    }
    else  if ([dataPack[@"url"] containsString:GET_ORDER_HOSTORY]){
        
       if (nextUrl == nil || nextUrl == (id)[NSNull null] || [nextUrl isEqualToString:@""])
       {
            [dataArray removeAllObjects];
           dataArray=[dataPack[@"data"][@"transactions"][@"data"] mutableCopy];
        }
        
       else{
           isNextPageLoading=false;
           [dataArray addObjectsFromArray:[dataPack[@"data"][@"transactions"][@"data"] mutableCopy]];
           [_activityBottom stopAnimating];
           _activityBottom.hidden=YES;
       }
        
        
        nextUrl=dataPack[@"data"][@"transactions"][@"next_page_url"];
        
        isLoaded=YES;
        [_tableView reloadData];
        if (dataArray.count==0) {
            [self showNoItem];
        }
        
        
    }
    
    
    
}




-(void)requestSucceededButActionFailed:(NSDictionary *)dataPack{
    
    if ([dataPack[@"url"] isEqualToString:GET_ORDER_HOSTORY]) {
        [self showNoItem];
    }
    
}
-(void)uploadedData:(NSString *)pecentage andByte:(float)bytes{
    
}
-(void)requestSucceededButNoDataFound:(NSDictionary *)dataPack{
    
}
-(void)requestFailedDueToServerIsuueWithError:(NSString*)errorDes{
    
}
@end
