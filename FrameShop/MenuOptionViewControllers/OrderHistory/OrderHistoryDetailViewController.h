//
//  OrderHistoryDetailViewController.h
//  FrameShop
//
//  Created by Saurabh Srivastav on 02/03/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Global.h"
#import "PopApi.h"
#import "OrderAddressTableViewCell.h"
#import "OrderPriceTableViewCell.h"
#import "OrderSummaryTableViewCell.h"
#import "OrderHistoryTableViewCell.h"
#import "HeaderView.h"
#import "SummaryPopUpView.h"
NS_ASSUME_NONNULL_BEGIN

@interface OrderHistoryDetailViewController : UIViewController<PopApiDelegate>

@property NSMutableDictionary *infoDic;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loader;
@end

NS_ASSUME_NONNULL_END
