//
//  OrderHistoryViewController.h
//  FrameShop
//
//  Created by Saurabh Srivastav on 02/03/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <StoreKit/SKStoreReviewController.h>
#import "OrderHistoryTableViewCell.h"
#import "PopApi.h"
#import "DBManager.h"
#import "WarningSubView.h"
#import "OrderHistoryDetailViewController.h"
#import "OrderCellTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface OrderHistoryViewController : UIViewController<PopApiDelegate,UIScrollViewDelegate>
- (IBAction)detail:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *continueShopping;
- (IBAction)continueShoppingAction:(id)sender;
- (IBAction)closeAction:(id)sender;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityBottom;
@end

NS_ASSUME_NONNULL_END
