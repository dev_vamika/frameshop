//
//  OrderHistoryDetailViewController.m
//  FrameShop
//
//  Created by Saurabh Srivastav on 02/03/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import "OrderHistoryDetailViewController.h"
#import "DBManager.h"
@interface OrderHistoryDetailViewController (){
    
    
    NSMutableArray *itemArray;
    DbManager *db;
    UIImage *detailImage;
}

@end

@implementation OrderHistoryDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    itemArray=[[NSMutableArray alloc] init];
    _loader.hidden = YES;
    
    
    _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    itemArray=_infoDic[@"orders"];
    
    [_tableView reloadData];
}





#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return (section==2)?itemArray.count:1;
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:
        {
            static NSString *simpleTableIdentifier = @"OrderSummaryTableViewCell";
            OrderSummaryTableViewCell *cell = (OrderSummaryTableViewCell *)[_tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
            
            if (cell == nil)
            {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"OrderSummaryTableViewCell" owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
            
            cell.infoDic=_infoDic;
            [cell setLables];
           
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
            break;
        case 1:
        {
            static NSString *simpleTableIdentifier = @"OrderAddressTableViewCell";
            OrderAddressTableViewCell *cell = (OrderAddressTableViewCell *)[_tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
            
            if (cell == nil)
            {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"OrderAddressTableViewCell" owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
            cell.infoDic=_infoDic;
            [cell setAddress];
            
            
            
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
            break;
        case 3:
        {
            static NSString *simpleTableIdentifier = @"OrderPriceTableViewCell";
            OrderPriceTableViewCell *cell = (OrderPriceTableViewCell *)[_tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
            
            if (cell == nil)
            {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"OrderPriceTableViewCell" owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
            cell.infoDic=_infoDic;
            [cell setLbls];
            
            
            
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
            break;
            
        default:{
            
            
            static NSString *simpleTableIdentifier = @"OrderHistoryTableViewCell";
            OrderHistoryTableViewCell *cell = (OrderHistoryTableViewCell *)[_tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
            
            if (cell == nil)
            {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"OrderHistoryTableViewCell" owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
            cell.infoDic=itemArray[indexPath.row];
            [cell setView];
            
            
            
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
            
            
        }
            break;
    }
    
    
    
    
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 2) {
        OrderHistoryTableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        if (![cell.infoDic[@"product_type"] isEqualToString:@"gift_card"]) {
        detailImage = cell.previewImage.image;
        [self performSegueWithIdentifier:@"viewMore" sender:  itemArray[indexPath.row] ];
        }
    }
}
#pragma mark - segue delegates
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"viewMore"]) {
        SummaryPopUpView *albumContentsViewController = [segue destinationViewController];
       ;
        NSMutableDictionary *dic =[ (NSMutableDictionary *)sender [@"order_meta"] mutableCopy];
        
     
        
        dic[@"price"] =[NSString stringWithFormat:@"%0.2f", [((NSMutableDictionary *)sender)[@"cart_meta"] [@"payable_amount"] floatValue] ] ;
        
        
        albumContentsViewController.infoDic=dic;
        albumContentsViewController.thumbImage=detailImage;
        
    }
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    
    switch (indexPath.section) {
        case 0:
        {
            
            NSArray *nibViews = [[NSBundle mainBundle] loadNibNamed:@"OrderSummaryTableViewCell" owner:self options:nil];
            OrderSummaryTableViewCell *sqr = [nibViews objectAtIndex:0];
            return sqr.frame.size.height;
            
        }
            break;
        case 1:
        {
            NSArray *nibViews = [[NSBundle mainBundle] loadNibNamed:@"OrderAddressTableViewCell" owner:self options:nil];
            OrderAddressTableViewCell *sqr = [nibViews objectAtIndex:0];
            NSMutableDictionary *addressDic=[[NSMutableDictionary alloc] init];
            addressDic =[ sqr.infoDic[@"shipping"] mutableCopy];
            return sqr.frame.size.height + [self heightForlabel:[NSString stringWithFormat:@"%@\n%@ %@,%@",addressDic[@"street_address"],addressDic[@"suburbs"],addressDic[@"state"],addressDic[@"postcode"]] font:sqr.addressText.font width:sqr.addressText.frame.size.width];
            
        }
            break;
        case 3:
        {
            NSArray *nibViews = [[NSBundle mainBundle] loadNibNamed:@"OrderPriceTableViewCell" owner:self options:nil];
            OrderPriceTableViewCell *sqr = [nibViews objectAtIndex:0];
            return sqr.frame.size.height;
            
        }
            break;
            
        default:{
            
            NSArray *nibViews = [[NSBundle mainBundle] loadNibNamed:@"OrderHistoryTableViewCell" owner:self options:nil];
            OrderHistoryTableViewCell *sqr = [nibViews objectAtIndex:0];
            return sqr.frame.size.height;
            
        }
            break;
    }
    
    
    
    
    
}
-(CGFloat)heightForlabel:(NSString*)text  font:(UIFont *)font  width:(CGFloat)width {
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, width,CGFLOAT_MAX)];//CGFloat.greatestFiniteMagnitude
    label.numberOfLines = 0;
    label.lineBreakMode = NSLineBreakByWordWrapping;
    label.font = font;
    label.text = text;
    [label sizeToFit];

    return label.frame.size.height;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    HeaderView *view=[[HeaderView alloc] init];
    view.view.frame=CGRectMake(0, 0, _tableView.frame.size.width, 10);
    view.invoiceButton.hidden = YES;
    [Global roundCorner:view.invoiceButton roundValue:5.0 borderWidth:0.0 borderColor:nil];
    [view.invoiceButton addTarget:self action:@selector(onClickRequestInvoice) forControlEvents:UIControlEventTouchUpInside];
    [view setCellInfo:_infoDic];
    switch (section) {
        case 0:
            if ([_infoDic[@"payment_status"] isEqualToString:@"payment_successful"] || [_infoDic[@"payment_status"] isEqualToString:@"payment_successfull"]) {
                view.invoiceButton.hidden = NO;
            }else{
                view.invoiceButton.hidden = YES;
            }
            view.headerTitle.text=@"Order Summary";
            break;
        case 1:
            view.headerTitle.text=@"Shipping Address";
            break;
        case 2:
            view.headerTitle.text=@"Item Summary";
            break;
        default:
            view.headerTitle.text=@"Final Price Summary";
            break;
    }
    
    
    return view.view;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        NSArray *tem = _infoDic[@"orders"];
        if (tem.count == 1) {
            if ([tem [0][@"product_type"] isEqualToString:@"gift_card"]) {
                return 40;
            }
        }
        return  108;
    }
    return 40;
}

-(void)onClickRequestInvoice{
    _loader.hidden = NO;
    [_loader startAnimating];
    db=[[DbManager alloc] init];
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    NSString *urlString=[NSString stringWithFormat:@"%@%@%@",SERVER_ADDRESS,REQUEST_INVOICE,_infoDic[@"transaction_id"]];
    
    NSMutableArray *arr=[[NSMutableArray alloc] init];
    arr=[db getDataForField:USERTABLE where:nil];
     NSLog(@"arr %@",arr);
    if (arr.count!=0) {
        [api sendGetRequst:urlString andToken:arr[0][@"token"]];
    }else{
        [self logInUser];
    }
 
}

-(void)signUpUser{
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
    dic[@"name"]=[[UIDevice currentDevice] name];
    dic[@"email"]=[NSString stringWithFormat:@"%@@frameshop.com.au",[Global getDeviceId]];
    dic[@"password"]=@"qwxcjeub32ne";
    dic[@"c_password"]=@"qwxcjeub32ne";
    dic[@"url"]=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,REGISTER_USER];
    [api sendPostRequst:dic];
    
}
-(void)logInUser{
    
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
    
    dic[@"email"]=[NSString stringWithFormat:@"%@@frameshop.com.au",[Global getDeviceId]];
    dic[@"password"]=@"qwxcjeub32ne";
    
    dic[@"url"]=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,LOGIN_USER];
    [api sendPostRequst:dic];
}

#pragma mark -Pop api Delegates

-(void)connectionError{
    _loader.hidden = YES;
    [_loader stopAnimating];
}
-(void)requestFailedSessionExpired:(NSDictionary *)dataPack{
    _loader.hidden = YES;
       [_loader stopAnimating];
}
-(void)requestFailedServerError:(NSDictionary *)dataPack{
    _loader.hidden = YES;
       [_loader stopAnimating];
}
-(void)requestFailedServiceUnavailableDueToMaintenance:(NSDictionary *)dataPack{
   _loader.hidden = YES;
      [_loader stopAnimating];
}
-(void)requestFailedMethodNotFound:(NSDictionary *)dataPack{
    _loader.hidden = YES;
       [_loader stopAnimating];
}
-(void)requestFailedNotFound:(NSDictionary *)dataPack{
    _loader.hidden = YES;
       [_loader stopAnimating];
}
-(void)requestFailedUnauthorised:(NSDictionary *)dataPack{
    _loader.hidden = YES;
       [_loader stopAnimating];
}
-(void)requestFailedBadRequest:(NSDictionary *)dataPack{
    _loader.hidden = YES;
       [_loader stopAnimating];
}
-(void)requestSucceededWithData:(NSDictionary *)dataPack{
    _loader.hidden = YES;
       [_loader stopAnimating];
    if ([dataPack[@"url"] isEqualToString:LOGIN_USER]) {
        
        [db delTableWithTableName:USERTABLE];
        NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
        
        dic[@"id"]              =[NSString stringWithFormat:@"%@",dataPack[@"data"][@"id"]];
        dic[@"name"]            =dataPack[@"data"][@"name"];
        dic[@"email"]           =dataPack[@"data"][@"email"];
        dic[@"created_at"]      =dataPack[@"data"][@"created_at"];
        dic[@"updated_at"]      =dataPack[@"data"][@"updated_at"];
        dic[@"token"]           =dataPack[@"data"][@"access_token"];
        
        [db insertIntoTable:dic table_name:USERTABLE];
        [self onClickRequestInvoice];
    }
    
    else if ([dataPack[@"url"] isEqualToString:REGISTER_USER]){
        
        NSMutableDictionary *dic =[[NSMutableDictionary alloc] init];
        dic[@"token"]            =dataPack[@"data"][@"token"];
        dic[@"id"]               =[NSString stringWithFormat:@"%@",dic[@"id"]];
        
        [db insertIntoTable:dic table_name:USERTABLE];
        [self onClickRequestInvoice];
    }
    else{
        
        [Global showSimpleAlert:self andTitle:FRAMESHOP andMessage:@"An invoice has been sent to your mail. Please check your inbox."];
    }
    
    
    
    
    
}
-(void)requestSucceededButActionFailed:(NSDictionary *)dataPack{
    _loader.hidden = YES;
       [_loader stopAnimating];
}
-(void)uploadedData:(NSString *)pecentage andByte:(float)bytes{
    _loader.hidden = YES;
       [_loader stopAnimating];
}
-(void)requestSucceededButNoDataFound:(NSDictionary *)dataPack{
    _loader.hidden = YES;
       [_loader stopAnimating];
}
-(void)requestFailedDueToServerIsuueWithError:(NSString*)errorDes{
    _loader.hidden = YES;
       [_loader stopAnimating];
}

@end
