//
//  OrderCellTableViewCell.m
//  FrameShop
//
//  Created by Saurabh Srivastav on 11/05/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import "OrderCellTableViewCell.h"

@implementation OrderCellTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [Global roundCorner:_orderView roundValue:_orderView.frame.size.height/2 borderWidth:0 borderColor:nil];
    [Global roundCorner:_backView roundValue:2 borderWidth:0.5 borderColor:[UIColor lightGrayColor]];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)setLbl{
    _transactionNumber.text=_infoDic[@"transaction_id"];
    _orderDate.text=[Global convertUTCIntoDate:_infoDic[@"created_at"]];
    _cost.text=[NSString stringWithFormat:@"$%0.2f",[_infoDic[@"processed_amount"] floatValue]];
    
  
    
    if ([_infoDic[@"internal_order_status"] isEqualToString:@"in_process"] || [_infoDic[@"internal_order_status"] isEqualToString:@"in process"] || [_infoDic[@"internal_order_status"] isEqualToString:@"In Process"]) {
//         _orderView.backgroundColor=[UIColor greenColor];
        _orderStatus.text = @"In Process";
        _orderStatus.textColor = shipping_in_process_dark;
    }
    else if ([_infoDic[@"internal_order_status"] isEqualToString:@"order_shipped"] || [_infoDic[@"internal_order_status"] isEqualToString:@"Shipped"]){
//        _orderView.backgroundColor=[UIColor redColor];
        _orderStatus.text = @"Shipped";
        _orderStatus.textColor = shipping_in_shipped_dark;
    }
    else if ([_infoDic[@"internal_order_status"] isEqualToString:@"order_delivered"] || [_infoDic[@"internal_order_status"] isEqualToString:@"Completed"] || [_infoDic[@"internal_order_status"] isEqualToString:@"order_completed"]){
//         _orderView.backgroundColor=[UIColor yellowColor];
        _orderStatus.text = @"Completed";
        _orderStatus.textColor = delivered_in_shipped_dark;
    }

//    else if ([_infoDic[@"payment_status"] isEqualToString:@"payment_fraud"] ||[_infoDic[@"payment_status"] isEqualToString:@"order_cancelled"] ){
//
//                  _orderView.backgroundColor=[UIColor redColor];
//              }
//          else {
//
////        _orderView.backgroundColor=[UIColor orangeColor];
//    }
    
    NSString *str=@"";
    
    NSArray *tem=_infoDic[@"orders"];
//    if (tem.count == 1) {
//        if ([tem [0][@"product_type"] isEqualToString:@"gift_card"]) {
//            _orderStatus.text = @"Completed";
//            _orderStatus.textColor = delivered_in_shipped_dark;
//        }
//    }
    for (int i=0; i<tem.count; i++) {
        
        switch (i) {
            case 0:
            {
                
                if ([tem [i][@"product_type"] isEqualToString:@"gift_card"]) {
                    str= [NSString stringWithFormat:@"%@ Gift Card",tem[i][@"gift_card"][@"card_name"]] ;
                    [self setImageToImageView:_pro1 andImageUrl:tem[i][@"gift_card"][@"card_pic_url"] andId:tem[i][@"id"]];
                }
                
                else{
                    
                    str=tem[i][@"product"][@"name"];
                    [self setImageToImageView:_pro1 andImageUrl:tem[i][@"order_meta"][@"thumb_url"] andId:tem[i][@"id"]];
                }
                
                
                
            }
                break;
            case 1:
            {
                if ([tem [i][@"product_type"] isEqualToString:@"gift_card"]) {
                    
                    [self setImageToImageView:_pro2 andImageUrl:tem[i][@"gift_card"][@"card_pic_url"] andId:tem[i][@"id"]];
                }
                
                else
                    [self setImageToImageView:_pro2 andImageUrl:tem[i][@"order_meta"][@"thumb_url"] andId:tem[i][@"id"]];
                
            }
                break;
            case 2:
            {
                if ([tem [i][@"product_type"] isEqualToString:@"gift_card"]) {
                    
                    [self setImageToImageView:_pro3 andImageUrl:tem[i][@"gift_card"][@"card_pic_url"] andId:tem[i][@"id"]];
                }
                
                else
                    
                    [self setImageToImageView:_pro3 andImageUrl:tem[i][@"order_meta"][@"thumb_url"] andId:tem[i][@"id"]];
                
            }
                break;
            case 3:
            {
                if ([tem [i][@"product_type"] isEqualToString:@"gift_card"]) {
                    
                    [self setImageToImageView:_pro4 andImageUrl:tem[i][@"gift_card"][@"card_pic_url"] andId:tem[i][@"id"]];
                }
                
                else
                    
                    [self setImageToImageView:_pro4 andImageUrl:tem[i][@"order_meta"][@"thumb_url"] andId:tem[i][@"id"]];
                
            }
                
                break;
            case 4:
            {
                if ([tem [i][@"product_type"] isEqualToString:@"gift_card"]) {
                    
                    [self setImageToImageView:_pro5 andImageUrl:tem[i][@"gift_card"][@"card_pic_url"] andId:tem[i][@"id"]];
                }
                
                else
                    
                    [self setImageToImageView:_pro5 andImageUrl:tem[i][@"order_meta"][@"thumb_url"] andId:tem[i][@"id"]];
                
            }
                
                break;
                
            default:
                break;
        }
    }
    
    
    if (tem.count>1) {
        str=[NSString stringWithFormat:@"%@ and %d more",str, (int)tem.count-1];
    }
    
    
    
    if (tem.count<=4) {
        _more.hidden=YES;
    }
    else{
        _more.hidden=NO;
        _more.text=[NSString stringWithFormat:@"%d+",(int)tem.count-4];
    }
    
    _cellHeading.text=[NSString stringWithFormat:@"%@",_infoDic[@"transaction_id"]];
    
//    NSArray *listItems = [_cellHeading.text componentsSeparatedByString:@" and "];
//    
//    
//    if (listItems.count>1) {
//        NSString *str=[NSString stringWithFormat:@"and %@",listItems[1]];
//        UIColor *color = APP_BLUE_COLOR;
//        
//        UIFont *font = [UIFont fontWithName:_cellHeading.font.fontName size:13.0];
//        
//        
//        NSMutableAttributedString *attrsString =  [[NSMutableAttributedString alloc] initWithAttributedString:_cellHeading.attributedText];
//        
//        // search for word occurrence
//        NSRange range = [_cellHeading.text rangeOfString:str];
//        if (range.location != NSNotFound) {
//            [attrsString addAttribute:NSForegroundColorAttributeName value:color range:range];
//            
//            [attrsString addAttribute:NSFontAttributeName value:font range:range];
//        }
//        
//        // set attributed text
//        _cellHeading.attributedText = attrsString;
//        
//        
//    }
    
    
    
    
    
    
}








-(void)setImageToImageView:(UIImageView *)imageView andImageUrl:(NSString *)url andId:(NSString *)orderId{
    
    
    imageView.image=[Global getImageFromDoucumentWithName:[NSString stringWithFormat:@"order_%@",orderId]];
    
    if (imageView.image==nil) {
        
        
        
        
        NSString *str=[NSString stringWithFormat:@"order_%@",orderId];
        NSString *mediaUrl=url;
        
        imageView.backgroundColor=[UIColor groupTableViewBackgroundColor];
        
        dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(q, ^{
            //[Global startShimmeringToView:cell.contentView];
            NSData *datas;
            
            datas =[NSData dataWithContentsOfURL:[NSURL URLWithString:mediaUrl]] ;
            
            [Global saveImagesInDocumentDirectory:datas ImageId:str];
            
            
            UIImage *img = [[UIImage alloc] initWithData:datas];
            dispatch_async(dispatch_get_main_queue(), ^{
                if(img!=nil){
                    imageView.image=img;
                    imageView.backgroundColor=[UIColor clearColor];
                }
                
                
            });
        });
        
    }
    
}







- (IBAction)viewDetailAction:(id)sender {
}
@end
