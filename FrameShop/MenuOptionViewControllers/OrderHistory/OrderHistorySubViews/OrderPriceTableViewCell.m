//
//  OrderPriceTableViewCell.m
//  FrameShop
//
//  Created by Saurabh Srivastav on 12/05/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import "OrderPriceTableViewCell.h"

@implementation OrderPriceTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [Global roundCorner:_backView roundValue:2 borderWidth:0.5 borderColor:[UIColor lightGrayColor]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
-(void)setLbls{
    

   _subTotal.text=[NSString stringWithFormat:@"$%0.2f",[_infoDic[@"order_amount"]floatValue]];
   _shipping.text=[NSString stringWithFormat:@"$%0.2f",[_infoDic[@"shipping_charges"] floatValue]];
   _discount.text=[NSString stringWithFormat:@"-$%.2f",[_infoDic[@"promo_code_adjustments"] floatValue]];
   _total.text=[NSString stringWithFormat:@"$%0.2f",[_infoDic[@"processed_amount"] floatValue]];
    //  _total.text=[NSString stringWithFormat:@"$%0.2f",[_infoDic[@"order_amount"] floatValue]+[_infoDic[@"processed_amount"] floatValue]+[_infoDic[@"shipping_charges"] floatValue]];
}
@end
