//
//  OrderHistoryTableViewCell.m
//  FrameShop
//
//  Created by Saurabh Srivastav on 02/03/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import "OrderHistoryTableViewCell.h"
#import "Global.h"
@implementation OrderHistoryTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    // [self addshadow:self.backView radious:2.0];
    [Global roundCorner:self.backView roundValue:2 borderWidth:0.5 borderColor:[UIColor lightGrayColor]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


-(void)setView{
    
    
    self.previewImage.image=[Global getImageFromDoucumentWithName:[NSString stringWithFormat:@"order_%@",_infoDic[@"id"]]];
    
    
    if(_previewImage.image==nil){
        
        NSString *str=[NSString stringWithFormat:@"order_%@",_infoDic[@"id"]];
        NSString *mediaUrl=_infoDic[@"order_meta"][@"thumb_url"];
        
        self.previewImage.backgroundColor=[UIColor groupTableViewBackgroundColor];
        
        dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(q, ^{
            //[Global startShimmeringToView:cell.contentView];
            NSData *datas;
            
            datas =[NSData dataWithContentsOfURL:[NSURL URLWithString:mediaUrl]] ;
            
            [Global saveImagesInDocumentDirectory:datas ImageId:str];
            
            
            UIImage *img = [[UIImage alloc] initWithData:datas];
            dispatch_async(dispatch_get_main_queue(), ^{
                if(img!=nil){
                    self.previewImage.image=img;
                    self.previewImage.backgroundColor=[UIColor clearColor];
                }
                
                
            });
        });
        
    }
    else{
        self.previewImage.backgroundColor=[UIColor clearColor];
    }
    
    
    _price.text=[NSString stringWithFormat:@"$%0.2f",[_infoDic[@"cart_meta"][@"payable_amount"] floatValue]];
    _quantity.text=[NSString stringWithFormat:@"%@",_infoDic[@"quantity"]];
    if ([_infoDic[@"product_type"] isEqualToString:@"gift_card"]) {
        _viewMoreDetailBtn.hidden = YES;
        _orderNumber.text=[NSString stringWithFormat:@"%@ Gift Card",_infoDic[@"gift_card"][@"card_name"]];
        _imageSizeTitle.text=@"To:";
        _itemSizeTitle.text=@"Email:";
        _itemSize.text=[NSString stringWithFormat:@"%@",_infoDic[@"order_meta"][@"email_to"]]  ;
        _imageSize.text=[NSString stringWithFormat:@"%@",_infoDic[@"order_meta"][@"name_to"]]  ;
    }
    
    else{
        _viewMoreDetailBtn.hidden = NO;
        _imageSizeTitle.text=@"Image Size";
        _itemSizeTitle.text=@"Item Size";
        _orderNumber.text=_infoDic[@"product"][@"name"];
        
     
        
        if ([_infoDic[@"product"][@"SKU"] isEqualToString:FRAME_SKU]) {
            _imageSize.text=[NSString stringWithFormat:@"%.1fx%.1f cm",[_infoDic[@"order_meta"][@"size"][@"opening"][@"width"] floatValue],[_infoDic[@"order_meta"][@"size"][@"opening"][@"height"] floatValue]];
            _itemSize.text=[NSString stringWithFormat:@"%.1fx%.1f cm",[_infoDic[@"order_meta"][@"size"][@"outside"][@"width"] floatValue],[_infoDic[@"order_meta"][@"size"][@"outside"][@"height"] floatValue]];
        }
        else  if ([_infoDic[@"product"][@"SKU"] isEqualToString:ACRYLIC_SKU] || [_infoDic[@"product"][@"SKU"] isEqualToString:BLOCK_MOUNT_SKU]){
            _imageSize.text=[NSString stringWithFormat:@"%.1fx%.1f cm",[_infoDic[@"order_meta"][@"width"] floatValue],[_infoDic[@"order_meta"][@"height"] floatValue]];
            _itemSize.text=[NSString stringWithFormat:@"%.1fx%.1f cm",[_infoDic[@"order_meta"][@"width"] floatValue],[_infoDic[@"order_meta"][@"height"] floatValue]];
        }
        
        
        
        
    }
    
    
}


-(void) addshadow:(UIView *)view radious:(float)radious{
    view.layer.masksToBounds = NO;
    view.layer.shadowColor = [[UIColor grayColor] CGColor];
    view.layer.shadowOffset = CGSizeMake(0,0);
    view.layer.shadowOpacity = 0.7;
    view.layer.shadowRadius = radious;
    
}
@end
