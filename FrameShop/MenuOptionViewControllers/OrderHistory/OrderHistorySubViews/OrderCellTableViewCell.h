//
//  OrderCellTableViewCell.h
//  FrameShop
//
//  Created by Saurabh Srivastav on 11/05/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Global.h"
NS_ASSUME_NONNULL_BEGIN

@interface OrderCellTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *status;
@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UILabel *transactionNumber;
@property (weak, nonatomic) IBOutlet UILabel *cost;
@property (weak, nonatomic) IBOutlet UILabel *orderStatus;
@property (weak, nonatomic) IBOutlet UILabel *orderDate;
@property (weak, nonatomic) IBOutlet UIImageView *pro1;
@property (weak, nonatomic) IBOutlet UIImageView *pro2;
@property (weak, nonatomic) IBOutlet UIImageView *pro3;
@property (weak, nonatomic) IBOutlet UILabel *cellHeading;
@property (weak, nonatomic) IBOutlet UILabel *transactionIdTitle;
@property (weak, nonatomic) IBOutlet UILabel *costTitle;

@property (weak, nonatomic) IBOutlet UILabel *orderDateTitile;
@property (weak, nonatomic) IBOutlet UIImageView *pro4;
@property (weak, nonatomic) IBOutlet UILabel *more;
- (IBAction)viewDetailAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *detailBtn;
@property (weak, nonatomic) IBOutlet UIView *orderView;
@property (weak, nonatomic) IBOutlet UIImageView *pro5;
@property (weak, nonatomic) IBOutlet UIStackView *stackView;
@property NSMutableDictionary *infoDic;
-(void)setLbl;
@end

NS_ASSUME_NONNULL_END
