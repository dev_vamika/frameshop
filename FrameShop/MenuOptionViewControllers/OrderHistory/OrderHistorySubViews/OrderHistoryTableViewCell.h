//
//  OrderHistoryTableViewCell.h
//  FrameShop
//
//  Created by Saurabh Srivastav on 02/03/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface OrderHistoryTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *previewImage;
@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UIButton *viewMoreDetailBtn;

@property (weak, nonatomic) IBOutlet UILabel *orderNumber;//titleLbl

@property (weak, nonatomic) IBOutlet UILabel *price;

@property (weak, nonatomic) IBOutlet UILabel *imageSize;
@property (weak, nonatomic) IBOutlet UILabel *itemSize;
@property (weak, nonatomic) IBOutlet UILabel *imageSizeTitle;
@property (weak, nonatomic) IBOutlet UILabel *itemSizeTitle;

@property (weak, nonatomic) IBOutlet UILabel *quantity;
@property NSMutableDictionary *infoDic;
-(void)setView;
@end

NS_ASSUME_NONNULL_END
