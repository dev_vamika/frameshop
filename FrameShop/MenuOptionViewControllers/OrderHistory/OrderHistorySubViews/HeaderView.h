//
//  HeaderView.h
//  FrameShop
//
//  Created by Saurabh Srivastav on 12/05/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HeaderView : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *headerTitle;
@property (weak, nonatomic) IBOutlet UIButton *invoiceButton;
@property (weak, nonatomic) IBOutlet UILabel *statusLbl;
-(void)setCellInfo:(NSMutableDictionary *)infoDic;
@end

NS_ASSUME_NONNULL_END
