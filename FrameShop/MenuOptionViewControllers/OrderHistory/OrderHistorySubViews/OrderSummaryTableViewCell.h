//
//  OrderSummaryTableViewCell.h
//  FrameShop
//
//  Created by Saurabh Srivastav on 12/05/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface OrderSummaryTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UILabel *orderNumber;
@property (weak, nonatomic) IBOutlet UILabel *orderDate;
@property (weak, nonatomic) IBOutlet UIImageView *paymentTypeImg;
@property (weak, nonatomic) IBOutlet UILabel *paymentText;
@property (weak, nonatomic) IBOutlet UILabel *shippingDate;
@property (weak, nonatomic) IBOutlet UILabel *trackingInfo;
@property (weak, nonatomic) IBOutlet UILabel *orderStatus;
@property NSMutableDictionary *infoDic;
-(void)setLables;
@property (weak, nonatomic) IBOutlet UIView *orderStatusView;
@end

NS_ASSUME_NONNULL_END
