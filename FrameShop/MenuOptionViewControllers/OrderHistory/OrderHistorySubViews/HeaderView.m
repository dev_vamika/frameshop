//
//  HeaderView.m
//  FrameShop
//
//  Created by Saurabh Srivastav on 12/05/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import "HeaderView.h"
#import "Global.h"
@interface HeaderView ()

@end

@implementation HeaderView

- (void)viewDidLoad {
    [super viewDidLoad];
    
}
-(void)setCellInfo:(NSMutableDictionary *)infoDic{
    if ([infoDic[@"internal_order_status"] isEqualToString:@"in_process"] || [infoDic[@"internal_order_status"] isEqualToString:@"in process"] || [infoDic[@"internal_order_status"] isEqualToString:@"In Process"]) {
        _statusLbl.text = in_process_status_string;//infoDic[@"shipping_notes"][@"text"];
        _statusLbl.textColor = shipping_in_process_dark;
        [Global roundCorner:_statusLbl roundValue:5.0 borderWidth:0.8 borderColor:shipping_in_process_dark];
        _statusLbl.backgroundColor = shipping_in_process_light;
    }
    else if ([infoDic[@"internal_order_status"] isEqualToString:@"order_shipped"] || [infoDic[@"internal_order_status"] isEqualToString:@"Shipped"]){
        _statusLbl.text = shipping_in_status_string;
        _statusLbl.textColor = shipping_in_shipped_dark;
        [Global roundCorner:_statusLbl roundValue:5.0 borderWidth:0.8 borderColor:shipping_in_shipped_dark];
        _statusLbl.backgroundColor = shipping_in_shipped_light;
    }
    else if ([infoDic[@"internal_order_status"] isEqualToString:@"order_delivered"] || [infoDic[@"internal_order_status"] isEqualToString:@"Completed"] || [infoDic[@"internal_order_status"] isEqualToString:@"order_completed"]){
        _statusLbl.text = delivered_status_string;
        _statusLbl.textColor = delivered_in_shipped_dark;
        [Global roundCorner:_statusLbl roundValue:5.0 borderWidth:0.8 borderColor:delivered_in_shipped_dark];
        _statusLbl.backgroundColor = delivered_in_shipped_light;
    }
    NSArray *tem = infoDic[@"orders"];
    if (tem.count == 1) {
        if ([tem [0][@"product_type"] isEqualToString:@"gift_card"]) {
            _statusLbl.text = @"";
            _statusLbl.textColor = UIColor.whiteColor;
            _statusLbl.backgroundColor = UIColor.whiteColor;
            
        }
    }
}

@end
