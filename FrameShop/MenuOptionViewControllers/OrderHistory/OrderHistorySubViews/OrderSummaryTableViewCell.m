//
//  OrderSummaryTableViewCell.m
//  FrameShop
//
//  Created by Saurabh Srivastav on 12/05/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import "OrderSummaryTableViewCell.h"
#import "Global.h"
@implementation OrderSummaryTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [Global roundCorner:_orderStatusView roundValue:_orderStatusView.frame.size.height/2 borderWidth:0 borderColor:nil];
    // Initialization code
    [Global roundCorner:_backView roundValue:2 borderWidth:0.5 borderColor:[UIColor lightGrayColor]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)setLables{
    if ([_infoDic[@"internal_order_status"] isEqualToString:@"in_process"] || [_infoDic[@"internal_order_status"] isEqualToString:@"in process"] || [_infoDic[@"internal_order_status"] isEqualToString:@"In Process"]) {
//         _orderView.backgroundColor=[UIColor greenColor];
        _orderStatus.text = @"In Process";
        _orderStatus.textColor = shipping_in_process_dark;
    }
    else if ([_infoDic[@"internal_order_status"] isEqualToString:@"order_shipped"] || [_infoDic[@"internal_order_status"] isEqualToString:@"Shipped"]){
//        _orderView.backgroundColor=[UIColor redColor];
        _orderStatus.text = @"Shipped";
        _orderStatus.textColor = shipping_in_shipped_dark;
    }
    else if ([_infoDic[@"internal_order_status"] isEqualToString:@"order_delivered"] || [_infoDic[@"internal_order_status"] isEqualToString:@"Completed"] || [_infoDic[@"internal_order_status"] isEqualToString:@"order_completed"]){
//         _orderView.backgroundColor=[UIColor yellowColor];
        _orderStatus.text = @"Completed";
        _orderStatus.textColor = delivered_in_shipped_dark;
    }
//    NSArray *tem=_infoDic[@"orders"];
//    _orderStatus.text = tem[0][@"internal_order_status"];
    _orderNumber.text=[NSString stringWithFormat:@"%@", _infoDic[@"transaction_id"]];
    _orderDate.text=[NSString stringWithFormat:@"%@" ,[Global convertUTCIntoDate:_infoDic[@"created_at"]]];
    _paymentText.hidden=YES;
    
    
    
    
    
//    NSArray *tem=_infoDic[@"orders"];
//    if (tem.count == 1) {
//        if ([tem [0][@"product_type"] isEqualToString:@"gift_card"]) {
//            _orderStatus.text = @"Completed";
//            _orderStatus.textColor = delivered_in_shipped_dark;
//        }
//    }
    
    
    
    
    
    
    
    
    
//    _shippingDate.text=[NSString stringWithFormat:@":%@",[Global convertUTCIntoDate:_infoDic[@"shipped_at"]]];
    
    
    if ([_infoDic[@"payment_status"] isEqualToString:@"payment_successfull"] || [_infoDic[@"payment_status"] isEqualToString:@"payment_successful"]) {
        _orderStatusView.backgroundColor=[UIColor greenColor];
        
        if (_infoDic[@"payment"]==(id)[NSNull null]) {
            _paymentTypeImg.hidden=YES;
            _paymentText.hidden=NO;
            _paymentText.text=@"Contact to customer care";
        }
        else
            if ([_infoDic[@"payment"][@"payment_node"] isEqualToString:@"paypal"]) {
                _paymentTypeImg.image=[UIImage imageNamed:@"braintree_logo.png"];
            }
            else if ([_infoDic[@"payment"][@"payment_node"] isEqualToString:@"westpac"]){
                _paymentTypeImg.image=[UIImage imageNamed:@"westpac_main_icon.png"];
            }
            else if ([_infoDic[@"payment"][@"payment_node"] isEqualToString:@"promo_code"]){
                _paymentTypeImg.hidden=YES;
                _paymentText.hidden=NO;
                _paymentText.text=@"COUPON";
//                _paymentTypeImg.image=[UIImage imageNamed:@"gift_logo_icon.png"];
            }
        
        
        
        
    }
    else if ([_infoDic[@"payment_status"] isEqualToString:@"payment_failed"]){
        _paymentTypeImg.hidden=YES;
        _paymentText.hidden=NO;
        _paymentText.text=@"Failed";
        _orderStatusView.backgroundColor=[UIColor redColor];
        _paymentTypeImg.hidden=YES;
    }
    else if ([_infoDic[@"payment_status"] isEqualToString:@"ready_for_payment"]){
        _paymentText.hidden=NO;
        _paymentText.text=@"Pending";
        _orderStatusView.backgroundColor=[UIColor yellowColor];
        _paymentTypeImg.hidden=YES;
    }
    else if ([_infoDic[@"payment_status"] isEqualToString:@"payment_fraud"] ||[_infoDic[@"payment_status"] isEqualToString:@"order_cancelled"] ){
        _paymentText.hidden=NO;
        _paymentText.text=@"Contact customer care";
        _orderStatusView.backgroundColor=[UIColor redColor];
        _paymentTypeImg.hidden=YES;
    }
    else {
        _paymentText.hidden=NO;
        _paymentText.text=@"Contact customer care";
        _orderStatusView.backgroundColor=[UIColor orangeColor];
        _paymentTypeImg.hidden=YES;
    }
    
    
    
    
    
    
    
}
@end
