//
//  OrderAddressTableViewCell.m
//  FrameShop
//
//  Created by Saurabh Srivastav on 12/05/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import "OrderAddressTableViewCell.h"
#import "Global.h"
@implementation OrderAddressTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [Global roundCorner:_backView roundValue:2 borderWidth:0.5 borderColor:[UIColor lightGrayColor]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
-(void)setAddress{
    NSMutableDictionary *addressDic=[[NSMutableDictionary alloc] init];
    
    
    if (!( ( [_infoDic[@"shipping"] isEqual:(id)[NSNull null]] )) ){
    
    addressDic =[ _infoDic[@"shipping"] mutableCopy];
    NSLog(@"addressDic %@",addressDic);
    
    NSString *str;
    str=@"";
    
    _email.text=[NSString stringWithFormat:@"%@",_infoDic[@"notifier_email"]];
    
    if (addressDic[@"company_name"] == (id)[NSNull null] || [addressDic[@"company_name"] length]==0 ) {
        addressDic[@"company_name"]=@"";
    }
    if (addressDic[@"floor_or_unit"] == (id)[NSNull null] || [addressDic[@"floor_or_unit"] length]==0 ) {
        addressDic[@"floor_or_unit"]=@"";
    }
    
    if (![addressDic[@"company_name"] isEqualToString:@""] ) {
        if(![addressDic[@"company_name"] isEqualToString:@"<null>"]){}
           // str=[NSString stringWithFormat:@"%@",addressDic[@"company_name"]];
            else{
                _companyHeight.constant = 0;}
    }else{
        _companyHeight.constant = 0;
    }
    if (![addressDic[@"floor_or_unit"] isEqualToString:@""]) {
        if(![addressDic[@"floor_or_unit"] isEqualToString:@"<null>"])
            str=[NSString stringWithFormat:@"%@\n%@",str,addressDic[@"floor_or_unit"]];
    }
        if (str.length == 0) {
            str=[NSString stringWithFormat:@"%@\n%@ %@,%@",addressDic[@"street_address"],addressDic[@"suburbs"],addressDic[@"state"],addressDic[@"postcode"]];
        }else{
            str=[NSString stringWithFormat:@"%@ %@\n%@ %@,%@",str ,addressDic[@"street_address"],addressDic[@"suburbs"],addressDic[@"state"],addressDic[@"postcode"]];
        }
       _addressText.text=[NSString stringWithFormat:@"%@",str];
        _addressText.translatesAutoresizingMaskIntoConstraints = true;
        [_addressText sizeToFit];
//        _addressText.scrollEnabled = false;
//        _addressHeight.constant = _addressText.contentSize.height;
    _fullName.text=[NSString stringWithFormat:@"%@",addressDic[@"full_name"]] ;
    _mobNum.text=[NSString stringWithFormat:@"%@",addressDic[@"mobile_number"]];
        _companyName.text=[NSString stringWithFormat:@"%@",addressDic[@"company_name"]];
    }
}
@end
