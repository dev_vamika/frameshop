//
//  OrderPriceTableViewCell.h
//  FrameShop
//
//  Created by Saurabh Srivastav on 12/05/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Global.h"
NS_ASSUME_NONNULL_BEGIN

@interface OrderPriceTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UILabel *total;
@property (weak, nonatomic) IBOutlet UILabel *shipping;
@property (weak, nonatomic) IBOutlet UILabel *discount;
@property NSMutableDictionary *infoDic;
@property (weak, nonatomic) IBOutlet UILabel *subTotal;
-(void)setLbls;
@end

NS_ASSUME_NONNULL_END
