//
//  OrderAddressTableViewCell.h
//  FrameShop
//
//  Created by Saurabh Srivastav on 12/05/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface OrderAddressTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *addressText;
@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UILabel *fullName;
@property NSMutableDictionary *infoDic;
@property (weak, nonatomic) IBOutlet UILabel *mobNum;
@property (weak, nonatomic) IBOutlet UILabel *email;
@property (weak, nonatomic) IBOutlet UILabel *companyName;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *companyHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *addressHeight;
-(void)setAddress;
@end

NS_ASSUME_NONNULL_END
