//
//  CartPageViewController.h
//  FrameShop
//
//  Created by Saurabh Srivastav on 03/03/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CartViewController.h"
#import "PaymentViewController.h"
#import "ShippingViewController.h"
NS_ASSUME_NONNULL_BEGIN

@interface CartPageViewController : UIViewController<UIPageViewControllerDataSource,UIPageViewControllerDelegate,CartViewControllerDelegate,ShippingViewControllerDelegate,UIScrollViewDelegate,PaymentViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UIView *tabBar;
@property (weak, nonatomic) IBOutlet UIView *indicatorBar;
@property (weak, nonatomic) IBOutlet UIButton *cartBtn;
@property (weak, nonatomic) IBOutlet UIButton *shippingBtn;
@property (weak, nonatomic) IBOutlet UIButton *paymentBtn;
- (IBAction)closeAction:(id)sender;
- (IBAction)cartAction:(id)sender;
- (IBAction)shippingAction:(id)sender;
- (IBAction)paymentAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *crtBtn;
@property (weak, nonatomic) IBOutlet UIButton *shpngBtn;

@property (weak, nonatomic) IBOutlet UIButton *payBtn;


@end

NS_ASSUME_NONNULL_END
