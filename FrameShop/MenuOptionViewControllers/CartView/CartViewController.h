//
//  CartViewController.h
//  FrameShop
//
//  Created by Saurabh Srivastav on 03/03/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CartTableViewCell.h"
#import "Global.h"
#import "DBManager.h"
#import "WarningSubView.h"
#import "PopApi.h"
#import "SummaryPopUpView.h"
NS_ASSUME_NONNULL_BEGIN
@class CartViewController;
@protocol CartViewControllerDelegate <NSObject>
@optional
-(void)didSelectProceedAction:(int)pageIndex;
-(void)didLoadedCartItem:(NSMutableArray *)orderArray;

@end
@interface CartViewController : UIViewController<PopApiDelegate,CartTableViewCellDelegate>
@property(strong,nonatomic)id<CartViewControllerDelegate> delegate;
@property int index;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *prcdBtn;
@property (weak, nonatomic) IBOutlet UIButton *ContinueShoppingBtn;
@property float totalAmount;
@property NSMutableArray *selectedCartItems;

- (IBAction)proceedAction:(id)sender;
- (IBAction)continueShoppingAction:(id)sender;
@end

NS_ASSUME_NONNULL_END
