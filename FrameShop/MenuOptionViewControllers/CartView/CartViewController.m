//
//  CartViewController.m
//  FrameShop
//
//  Created by Saurabh Srivastav on 03/03/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import "CartViewController.h"

@interface CartViewController (){
    BOOL isLoaded;
    NSMutableArray *dataArray;
    DbManager *db;
    WarningSubView *subView;
    UIImage *detailImage;
    
}

@end

@implementation CartViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    dataArray        =[[NSMutableArray alloc] init];
    _selectedCartItems=[[NSMutableArray alloc] init];
    db=[[DbManager alloc] init];
    subView=[[WarningSubView alloc] init];
    // Do any additional setup after loading the view.
    isLoaded=NO;
    _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    _prcdBtn.enabled=false;
    _prcdBtn.backgroundColor=[UIColor lightGrayColor];
    
    
    [Global roundCorner:_ContinueShoppingBtn roundValue:_ContinueShoppingBtn.frame.size.height/2 borderWidth:1.0 borderColor:APP_BLUE_COLOR];
    
    [self getCartDataFromServerData];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}

#pragma mark - Class Methods
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    [subView.view removeFromSuperview];
    isLoaded=NO;
    [_tableView reloadData];
    
    [self performSelector:@selector(getCartDataFromServerData) withObject:nil afterDelay:1.0];
    
}


#pragma mark - API Methods

-(void)getCartDataFromServerData{
    
    //    dataArray=[[db getDataForField:CART_TABLE where:nil] mutableCopy];
    //    if (dataArray.count!=0) {
    //        isLoaded=YES;
    //        [_tableView reloadData];
    //    }
    
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    NSString *urlString=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,GET_CART_ITEMS];
    
    NSMutableArray *arr=[[NSMutableArray alloc] init];
    arr=[db getDataForField:USERTABLE where:nil];
     NSLog(@"arr %@",arr);
    if (arr.count!=0) {
        
        
        [api sendGetRequst:urlString andToken:arr[0][@"token"]];
    }
    else
        [self logInUser];
    
}

-(void)signUpUser{
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
    dic[@"name"]=[[UIDevice currentDevice] name];
    dic[@"email"]=[NSString stringWithFormat:@"%@@frameshop.com.au",[Global getDeviceId]];
    dic[@"password"]=@"qwxcjeub32ne";
    dic[@"c_password"]=@"qwxcjeub32ne";
    dic[@"url"]=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,REGISTER_USER];
    [api sendPostRequst:dic];
    
}
-(void)logInUser{
    
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
    
    dic[@"email"]=[NSString stringWithFormat:@"%@@frameshop.com.au",[Global getDeviceId]];
    dic[@"password"]=@"qwxcjeub32ne";
    
    dic[@"url"]=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,LOGIN_USER];
    [api sendPostRequst:dic];
}




-(void)deleteCartItemFromServerWhereItemId:(NSString *)itemId{
    PopApi *api=[[PopApi alloc] init];
    
    NSString *urlString=[NSString stringWithFormat:@"%@%@/%@",SERVER_ADDRESS,DEL_CART_ITEMS,itemId];
    
    NSMutableArray *arr=[[NSMutableArray alloc] init];
    arr=[db getDataForField:USERTABLE where:nil];
     NSLog(@"arr %@",arr);
    if (arr.count!=0) {
        
        [api sendDelRequst:urlString andToken:arr[0][@"token"]];
        
    }
    else
        [self logInUser];
}



#pragma mark - Actions


- (IBAction)continueShoppingAction:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)proceedAction:(id)sender {
    //if (_selectedCartItems.count!=0) {
    [self.delegate didSelectProceedAction:self.index];
    //}
    
}








#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return (isLoaded)?dataArray.count:4;
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    if (isLoaded) {
    static NSString *simpleTableIdentifier = @"CartTableViewCell";
    CartTableViewCell *cell = (CartTableViewCell *)[_tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CartTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    cell.delegate=self;
    cell.previewImage.backgroundColor   =[UIColor groupTableViewBackgroundColor];
    if (isLoaded) {
        
        
        cell.productName.backgroundColor    =[UIColor clearColor];
        cell.price.backgroundColor          =[UIColor clearColor];
        
        cell.trashBtn.hidden                =false;
        cell.subTitle.backgroundColor       =[UIColor clearColor];
        cell.selectBtn.hidden=false;
        cell.indexPath=indexPath;
        cell.productName.text=dataArray[indexPath.row][@"product_name"];
        cell.price.text=[NSString stringWithFormat:@"$%@", dataArray[indexPath.row][@"card_base_price"]] ;
        cell.amount=[dataArray[indexPath.row][@"card_base_price"] floatValue];
        cell.subTitle.text=dataArray[indexPath.row][@"name_to"];
        cell.qty.text= dataArray[indexPath.row][@"quantity"];
        cell.infoDic=dataArray[indexPath.row];
        cell.viewMoreBtn.hidden=false;
        
        
        if ([dataArray[indexPath.row][@"order_type"] isEqualToString:@"gift_card"]) {
            [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"gift_shipping"];//@"product_shipping"
            cell.qtyView.hidden                     =true;
            cell.viewMoreBtn.hidden = YES;
        }
        else{
            [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"product_shipping"];
            cell.qtyView.hidden                     =false;
            cell.viewMoreBtn.hidden = NO;
        }
        
        
        
        
        
        cell.previewImage.image=[Global getImageFromDoucumentWithName:[NSString stringWithFormat:@"cart_%@_%@",self->dataArray[indexPath.row][@"order_type"],self->dataArray[indexPath.row][@"id"]]];
        if (cell.previewImage.image!=nil) {
            cell.previewImage.backgroundColor=[UIColor clearColor];
        }
        
        
        NSString *str=[NSString stringWithFormat:@"cart_%@_%@",self->dataArray[indexPath.row][@"order_type"],self->dataArray[indexPath.row][@"id"]];
        NSString *mediaUrl=dataArray[indexPath.row][@"media_url"];
        
        dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(q, ^{
            //[Global startShimmeringToView:cell.contentView];
            NSData *datas;
            if (self->isLoaded) {
                datas =[NSData dataWithContentsOfURL:[NSURL URLWithString:mediaUrl]] ;
                
                [Global saveImagesInDocumentDirectory:datas ImageId:str];
            }
            
            UIImage *img = [[UIImage alloc] initWithData:datas];
            dispatch_async(dispatch_get_main_queue(), ^{
                if(img!=nil){
                    cell.previewImage.image=img;
                     cell.previewImage.backgroundColor=[UIColor clearColor];
                }
                
                
            });
        });
        
        [Global stopShimmeringToView:cell.contentView];
        
    }
    else{
        
        cell.productName.backgroundColor    =[UIColor groupTableViewBackgroundColor];
        cell.price.backgroundColor          =[UIColor groupTableViewBackgroundColor];
        cell.qtyView.hidden                     =true;
        cell.subTitle.backgroundColor       =[UIColor groupTableViewBackgroundColor];
        cell.previewImage.image             =nil;
        cell.selectBtn.hidden=true;
        cell.productName.text    =@"";
        cell.price.text          =@"";
        cell.qty.text            =@"";
        cell.subTitle.text       =@"";
        cell.trashBtn.hidden     =true;
        cell.viewMoreBtn.hidden=true;
        
        [Global startShimmeringToView:cell.contentView];
    }
    
    
//    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    
    
    
    return cell;
    
    
    
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSArray *nibViews = [[NSBundle mainBundle] loadNibNamed:@"CartTableViewCell" owner:self options:nil];
    CartTableViewCell *sqr = [nibViews objectAtIndex:0];
    return sqr.frame.size.height;
    
    
}



#pragma mark -Cart Cell Delegates

-(void)didSelectItem:(NSIndexPath *)indexPath{
    _selectedCartItems[_selectedCartItems.count]=dataArray[indexPath.row];
    _prcdBtn.backgroundColor=APP_BLUE_COLOR;
    _prcdBtn.enabled=YES;
    
    
}
-(void)didDeleteItemIndexPath:(NSIndexPath *)indexPath{
    CartTableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    [cell stopLoader];
    if ([dataArray[indexPath.row][@"order_type"] isEqualToString:@"gift_card"]) {
        [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"gift_shipping"];
    }else{
        [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"product_shipping"];
    }
    if (indexPath.row<dataArray.count) {
        [dataArray removeObjectAtIndex:indexPath.row];
    }
    if (indexPath.row<_selectedCartItems.count) {
        [_selectedCartItems removeObjectAtIndex:indexPath.row];
    }
    
    if (!([cell.price.text isKindOfClass:[NSNull class]]) )
        _totalAmount = _totalAmount - [[cell.price.text stringByReplacingOccurrencesOfString:@"$" withString:@""] floatValue];
    
    [_tableView reloadData];
    if (dataArray.count==0) {
        [self requestSucceededButActionFailed:nil];
    }
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:CART_REFRESH object:self userInfo:nil];
    
   
    
    
}
-(void)didDeSelectItem:(NSIndexPath *)indexPath{
    //Will use in future
    
}
-(void)didPressDeleteItemIndexPath:(NSIndexPath *)indexPath{
    CartTableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:DELETE_TITLE message:DELETE_MSG preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Yes, delete it!" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        [cell removeItemFromCart];
        
        
        
    }];
    UIAlertAction *no = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [cell stopLoader];
    }];
    [alert addAction:ok];
    [alert addAction:no];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)didPressMoreDetail:(NSIndexPath *)indexPath{
    
    
    CartTableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    detailImage=cell.previewImage.image;
    
    
    [self performSegueWithIdentifier:@"proDel" sender: dataArray[indexPath.row]];
}

-(void)didChangeQuantity:(NSMutableDictionary *)infoDic andAddedAmount:(float)amount andIndexPath:(NSIndexPath *)indexPath{
    
    dataArray[indexPath.row]=infoDic;
    _totalAmount=_totalAmount+amount;
    // [self.delegate didLoadedCartItem:_selectedCartItems];
    NSLog(@"ddddf");
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:CART_REFRESH object:self userInfo:nil];
}
#pragma mark - segue delegates
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"proDel"]) {
        
        NSMutableDictionary *dic =[  (NSMutableDictionary *)sender mutableCopy];
        
        dic = [Global convertNsstringToDic:dic[@"cart_order_info"]];
        dic[@"price"]=(NSMutableDictionary *)sender[@"card_base_price"];
        
        SummaryPopUpView *albumContentsViewController = [segue destinationViewController];
      //  [Global convertNsstringToDic:dataArray[indexPath.row][@"cart_order_info"]]
        albumContentsViewController.infoDic=dic;
        albumContentsViewController.thumbImage=detailImage;
        
    }
    
}

#pragma mark -Pop api Delegates

-(void)connectionError{
    if (dataArray.count==0) {
        [subView.view removeFromSuperview];
        subView.view.frame=
        _tableView.frame;
        [self.view addSubview:subView.view];
        subView.titleText.text=NO_INTERNET_MSG;
        subView.subTitleTxt.text=@"Check you internet connection. Tap try again";
        subView.noCartIcon.image=[UIImage imageNamed:@"noInternet.png"];
        UITapGestureRecognizer *singleFingerTap =
        [[UITapGestureRecognizer alloc] initWithTarget:self
                                                action:@selector(handleSingleTap:)];
        [subView.view addGestureRecognizer:singleFingerTap];
    }
    
    
}
-(void)requestFailedSessionExpired:(NSDictionary *)dataPack{
    
}
-(void)requestFailedServerError:(NSDictionary *)dataPack{
    
}
-(void)requestFailedServiceUnavailableDueToMaintenance:(NSDictionary *)dataPack{
    [dataArray removeAllObjects];
    isLoaded=NO;
    [subView.view removeFromSuperview];
    subView.view.frame=CGRectMake(self.tableView.frame.origin.x, self.tableView.frame.origin.y, self.tableView.frame.size.width, self.tableView.frame.size.height);
    [self.view addSubview:subView.view];
    subView.noCartIcon.image=[UIImage imageNamed:@"emptyCart.png"];
    subView.titleText.text = dataPack[@"message"];
    _tableView.backgroundColor=[UIColor clearColor];
    
    _prcdBtn.enabled=false;
    _prcdBtn.backgroundColor=[UIColor lightGrayColor];
    
    [[NSUserDefaults standardUserDefaults] setValue:nil forKey:CART_COUNT];
}
-(void)requestFailedMethodNotFound:(NSDictionary *)dataPack{
    
}
-(void)requestFailedNotFound:(NSDictionary *)dataPack{
    if ([dataPack[@"error"] isEqualToString:@"there is nothing in cart!"]) {
        [dataArray removeAllObjects];
        isLoaded=NO;
        [subView.view removeFromSuperview];
        subView.view.frame=
        CGRectMake(self.tableView.frame.origin.x, self.tableView.frame.origin.y, self.tableView.frame.size.width, self.tableView.frame.size.height);
        [self.view addSubview:subView.view];
        subView.noCartIcon.image=[UIImage imageNamed:@"emptyCart.png"];
        _tableView.backgroundColor=[UIColor clearColor];
        
        _prcdBtn.enabled=false;
        _prcdBtn.backgroundColor=[UIColor lightGrayColor];
        
        [[NSUserDefaults standardUserDefaults] setValue:nil forKey:CART_COUNT];
    }
}
-(void)requestFailedUnauthorised:(NSDictionary *)dataPack{
     [self logInUser];
}
-(void)requestFailedBadRequest:(NSDictionary *)dataPack{
    
}
-(void)requestSucceededWithData:(NSDictionary *)dataPack{
    
    if ([dataPack[@"url"] isEqualToString:LOGIN_USER]) {
        
        [db delTableWithTableName:USERTABLE];
        NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
        
        dic[@"id"]              =[NSString stringWithFormat:@"%@",dataPack[@"data"][@"id"]];
        dic[@"name"]            =dataPack[@"data"][@"name"];
        dic[@"email"]           =dataPack[@"data"][@"email"];
        dic[@"created_at"]      =dataPack[@"data"][@"created_at"];
        dic[@"updated_at"]      =dataPack[@"data"][@"updated_at"];
        dic[@"token"]           =dataPack[@"data"][@"access_token"];
        
        [db insertIntoTable:dic table_name:USERTABLE];
        [self getCartDataFromServerData];
    }
    
    else if ([dataPack[@"url"] isEqualToString:REGISTER_USER]){
        
        NSMutableDictionary *dic =[[NSMutableDictionary alloc] init];
        dic[@"token"]            =dataPack[@"data"][@"token"];
        dic[@"id"]               =[NSString stringWithFormat:@"%@",dic[@"id"]];
        
        [db insertIntoTable:dic table_name:USERTABLE];
        [self getCartDataFromServerData];
    }
    else  if ([dataPack[@"url"] isEqualToString:GET_CART_ITEMS]){
        
        [dataArray removeAllObjects];
        [_selectedCartItems removeAllObjects];
        [db delTableWithTableName:CART_TABLE];
        NSArray *arr=[dataPack[@"data"] mutableCopy];
        
        
        _totalAmount=0;
        
        for (int i=0; i<arr.count; i++) {
            
            
            
            NSMutableDictionary *temDic=[[NSMutableDictionary alloc] init];
            
            temDic[@"id"]          =[NSString stringWithFormat:@"%@",arr[i][@"id"]];
            temDic[@"cart_order_id"]        =arr[i][@"cart_order_id"];
            temDic[@"linked_id"]         =[NSString stringWithFormat:@"%@",arr[i][@"linked_id"]];
            temDic[@"order_type"]         =arr[i][@"order_type"];
            temDic[@"order_current_status"]      =arr[i][@"order_current_status"];
            temDic[@"card_base_price"]  =[NSString stringWithFormat:@"%.2f",[arr[i][@"payable_amount"] floatValue]];
            _totalAmount=_totalAmount+[temDic[@"card_base_price"] floatValue];
            temDic[@"cart_order_info"]=[Global convertDicToJson:arr[i][@"cart_order_info"]];
            temDic[@"quantity"]          =[NSString stringWithFormat:@"%@",arr[i][@"quantity"]];
            
            if ([arr[i][@"order_type"] isEqualToString:@"gift_card"]) {
                temDic[@"productInfo"]=[Global convertDicToJson:arr[i][@"gift_card"]];
                temDic[@"media_url"] =[NSString stringWithFormat:@"%@",arr[i][@"gift_card"][@"card_pic_url"]];
                temDic[@"product_name"] =[NSString stringWithFormat:@"%@",arr[i][@"gift_card"][@"card_name"]];
                temDic[@"name_to"]=[NSString stringWithFormat:@"To: %@",arr[i][@"cart_order_info"][@"name_to"]];
                
            }
            else{
                temDic[@"media_url"] =[NSString stringWithFormat:@"%@",arr[i][@"cart_order_info"][@"thumb_url"]];
                temDic[@"product_name"] =[NSString stringWithFormat:@"%@",arr[i][@"product"][@"name"]];
                temDic[@"productInfo"]=[Global convertDicToJson:arr[i][@"product"]];
                temDic[@"name_to"]=[NSString stringWithFormat:@"Frame: %@",arr[i][@"cart_order_info"][@"frame"]];
                
                NSString * sku = [Global convertNsstringToDic:temDic[@"productInfo"]] [@"SKU"];
                
                
                if ([sku isEqualToString:ACRYLIC_SKU]) {
                    
                    if ([arr[i][@"cart_order_info"][@"stand_back"] boolValue]) {
                        temDic[@"name_to"]=@"Stand Back";
                    }
                    else{
                        temDic[@"name_to"]=@"Wall Mount";
                    }
                    
                }
                else if ([sku isEqualToString:BLOCK_MOUNT_SKU]){
                    if ([arr[i][@"cart_order_info"][@"hanger"] isEqualToString:HANGER_SAWTOOTH]) {
                        temDic[@"name_to"]=@"Sawtooth";
                    }
                    else{
                        temDic[@"name_to"]=@"Support Frame";
                    }
                }
                else if ([sku isEqualToString:CANVAS_PRINTING_SKU]){
                    if ([arr[i][@"cart_order_info"][@"edge"] isEqualToString:@"WHITE"]) {
                        temDic[@"name_to"]=@"Gallery Wrap";
                    }
                    else if ([arr[i][@"cart_order_info"][@"edge"] isEqualToString:@"BLACK"]){
                        temDic[@"name_to"]=@"Colour Wrap";
                    }
                    else{
                        temDic[@"name_to"]=@"Mirror Wrap";
                    }
                }
                
                
                
                
            }
            
            
            if ([Global isString:temDic[@"media_url"] contains:@"https"]) {
                _selectedCartItems[_selectedCartItems.count]=temDic;
                dataArray[dataArray.count]=temDic;
                [db insertIntoTable:temDic table_name:CART_TABLE];
            }
            else{
                
                
                [self deleteCartItemFromServerWhereItemId:temDic[@"id"]];
                
            }
            
            
        }
        isLoaded=YES;
        dataArray=[[[dataArray reverseObjectEnumerator] allObjects] mutableCopy];
        
        
        
        [_tableView reloadData];
        if (dataArray.count==0) {
            [self requestSucceededButActionFailed:nil];
        }
        [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%lu",(unsigned long)dataArray.count] forKey:CART_COUNT];
        NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
        [nc postNotificationName:CART_REFRESH object:self userInfo:nil];
        
        _prcdBtn.backgroundColor=APP_BLUE_COLOR;
        _prcdBtn.enabled=YES;
        [self.delegate didLoadedCartItem:_selectedCartItems];
        if (dataArray == nil || dataArray.count == 0) {
            [dataArray removeAllObjects];
            isLoaded=NO;
            [subView.view removeFromSuperview];
            subView.view.frame=
            CGRectMake(self.tableView.frame.origin.x, self.tableView.frame.origin.y, self.tableView.frame.size.width, self.tableView.frame.size.height);
            [self.view addSubview:subView.view];
            subView.noCartIcon.image=[UIImage imageNamed:@"emptyCart.png"];
            _tableView.backgroundColor=[UIColor clearColor];
            
            _prcdBtn.enabled=false;
            _prcdBtn.backgroundColor=[UIColor lightGrayColor];
            
            [[NSUserDefaults standardUserDefaults] setValue:nil forKey:CART_COUNT];
        }
        
    }
    
    
    
    
    
}
-(void)requestSucceededButActionFailed:(NSDictionary *)dataPack{
    
    
    // if ([dataPack[@"url"] isEqualToString:GET_CART_ITEMS]) {
    
    [dataArray removeAllObjects];
    isLoaded=NO;
    [subView.view removeFromSuperview];
    subView.view.frame=
    _tableView.frame;
    [self.view addSubview:subView.view];
    subView.noCartIcon.image=[UIImage imageNamed:@"emptyCart.png"];
    _tableView.backgroundColor=[UIColor clearColor];
    
    _prcdBtn.enabled=false;
    _prcdBtn.backgroundColor=[UIColor lightGrayColor];
    
    [[NSUserDefaults standardUserDefaults] setValue:nil forKey:CART_COUNT];
    
    // }
    
    
}
-(void)uploadedData:(NSString *)pecentage andByte:(float)bytes{
    
}
-(void)requestSucceededButNoDataFound:(NSDictionary *)dataPack{
    
}
-(void)requestFailedDueToServerIsuueWithError:(NSString*)errorDes{
    
}
@end
