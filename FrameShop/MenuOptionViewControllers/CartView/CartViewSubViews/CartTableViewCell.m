//
//  CartTableViewCell.m
//  FrameShop
//
//  Created by Saurabh Srivastav on 04/03/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import "CartTableViewCell.h"

@implementation CartTableViewCell{
    DbManager *db;
    NSString *oldQuantity;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    db=[[DbManager alloc] init];
    // Initialization code
    [Global roundCorner:_qtyView roundValue:5.0 borderWidth:1.5 borderColor:APP_BLUE_COLOR];
    [Global roundCorner:_viewMoreBtn roundValue:5.0 borderWidth:1.5 borderColor:APP_BLUE_COLOR];
    _activityIndicator.hidden=true;
    _deleteActivity.hidden=true;
  //  [self.delegate didSelectItem:_indexPath];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)removeItemFromCart{
    self.userInteractionEnabled=false;
      _trashBtn.hidden=YES;
      _deleteActivity.hidden=false;
      [_deleteActivity startAnimating];

    
    PopApi *api=[[PopApi alloc] init];
    
    NSString *urlString=[NSString stringWithFormat:@"%@%@/%@",SERVER_ADDRESS,DEL_CART_ITEMS,_infoDic[@"id"]];
    
    NSMutableArray *arr=[[NSMutableArray alloc] init];
    arr=[db getDataForField:USERTABLE where:nil];
     NSLog(@"arr %@",arr);
    api.delegate=self;
    
    
    if (arr.count!=0) {
        
        [api sendDelRequst:urlString andToken:arr[0][@"token"]];
        
    }
    else
        [self logInUser];
    
}



-(void)changeQuantity{

    [_activityIndicator startAnimating];
             _activityIndicator.hidden=NO;
    _price.hidden=!_activityIndicator.isHidden;
    
    PopApi *api=[[PopApi alloc] init];
       api.delegate=self;
       NSString *urlString=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,CHANG_QUANTITY];
       NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
       dic[@"url"]=urlString;
       
       NSMutableArray *arr=[[NSMutableArray alloc] init];
       arr=[db getDataForField:USERTABLE where:nil];

       NSLog(@"arr %@",arr);

       if (arr.count!=0) {
     
           dic[@"token"]=arr[0][@"token"];
           dic[@"cart_order_id"]=_infoDic[@"cart_order_id"];
           dic[@"quantity"]=_qty.text;
           [api sendPostRequst:dic];
        }
}

-(void)signUpUser{
    
   
    
   
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
   dic[@"name"]=[[UIDevice currentDevice] name];
    dic[@"email"]=[NSString stringWithFormat:@"%@@frameshop.com.au",[Global getDeviceId]];
    dic[@"password"]=@"qwxcjeub32ne";
    dic[@"c_password"]=@"qwxcjeub32ne";
    dic[@"url"]=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,REGISTER_USER];
    [api sendPostRequst:dic];
    
}
-(void)logInUser{
   
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
   
   dic[@"email"]=[NSString stringWithFormat:@"%@@frameshop.com.au",[Global getDeviceId]];
    dic[@"password"]=@"qwxcjeub32ne";
   
    dic[@"url"]=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,LOGIN_USER];
    [api sendPostRequst:dic];
}

- (IBAction)trashBtnAction:(id)sender {
    

    
    
    
  
 //   [self removeItemFromCart];
   //
    
    [self.delegate didPressDeleteItemIndexPath:_indexPath];
    
}
- (IBAction)minusBtnAction:(id)sender {
    
    if (!_activityIndicator.isHidden) {
        return;
    }
    
   // [self.delegate didPressQuantityButton];
    
   
    
    int count=[_qty.text intValue];
    oldQuantity=_qty.text;
    count--;
    
  
    if (count>0) {
       
         _qty.text=[NSString stringWithFormat:@"%d",count];
         [self changeQuantity];
    }
    
   
  
}
- (IBAction)pluseBtnAction:(id)sender {
    if (!_activityIndicator.isHidden) {
        return;
    }
   // [self.delegate didPressQuantityButton];
    
  
    int count=[_qty.text intValue];
     oldQuantity=_qty.text;
    count++;
    if (count>0) {
         
            _qty.text=[NSString stringWithFormat:@"%d",count];
        [self changeQuantity];
       }
}


- (IBAction)selectAction:(id)sender {
    _selectBtn.selected=!_selectBtn.isSelected;
    
    if (_selectBtn.isSelected) {
         [self.delegate didSelectItem:_indexPath];
    }
    else{
        [self.delegate didDeSelectItem:_indexPath ];
    }
   
}

- (IBAction)viewMoreDetail:(id)sender {
    [self.delegate didPressMoreDetail:_indexPath];
}

-(void)stopLoader{
    
    self.userInteractionEnabled=YES;
    _trashBtn.hidden=false;
    _deleteActivity.hidden=YES;
    [_deleteActivity stopAnimating];
}
#pragma mark -Pop api Delegates

-(void)connectionError{
    
    [_activityIndicator stopAnimating];
    _activityIndicator.hidden=YES;
     _price.hidden=!_activityIndicator.isHidden;
    [self stopLoader];
     
}
-(void)requestFailedSessionExpired:(NSDictionary *)dataPack{
     [self stopLoader];
}
-(void)requestFailedServerError:(NSDictionary *)dataPack{
     [self stopLoader];
}
-(void)requestFailedServiceUnavailableDueToMaintenance:(NSDictionary *)dataPack{
      [_activityIndicator stopAnimating];
     _activityIndicator.hidden=YES;
          _price.hidden=!_activityIndicator.isHidden;
          [self stopLoader];
    
}
-(void)requestFailedMethodNotFound:(NSDictionary *)dataPack{
     [self stopLoader];
}
-(void)requestFailedNotFound:(NSDictionary *)dataPack{
     [self stopLoader];
}
-(void)requestFailedUnauthorised:(NSDictionary *)dataPack{
     [self logInUser];
     [self stopLoader];
}
-(void)requestFailedBadRequest:(NSDictionary *)dataPack{
     [self stopLoader];
}
-(void)requestSucceededWithData:(NSDictionary *)dataPack{
    
    if ([dataPack[@"url"] isEqualToString:LOGIN_USER]) {
           
           [db delTableWithTableName:USERTABLE];
           NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
          
           dic[@"id"]              =[NSString stringWithFormat:@"%@",dataPack[@"data"][@"id"]];
           dic[@"name"]            =dataPack[@"data"][@"name"];
           dic[@"email"]           =dataPack[@"data"][@"email"];
           dic[@"created_at"]      =dataPack[@"data"][@"created_at"];
           dic[@"updated_at"]      =dataPack[@"data"][@"updated_at"];
           dic[@"token"]           =dataPack[@"data"][@"access_token"];
           
           [db insertIntoTable:dic table_name:USERTABLE];
           [self changeQuantity];
        }
       
       else if ([dataPack[@"url"] isEqualToString:REGISTER_USER]){
         
           NSMutableDictionary *dic =[[NSMutableDictionary alloc] init];
           dic[@"token"]            =dataPack[@"data"][@"token"];
           dic[@"id"]               =[NSString stringWithFormat:@"%@",dic[@"id"]];
             
           [db insertIntoTable:dic table_name:USERTABLE];
           [self changeQuantity];
       }
       else  if ([dataPack[@"url"] isEqualToString:CHANG_QUANTITY]){
 
           float oldValue=[_infoDic[@"card_base_price"]floatValue];
           [_activityIndicator stopAnimating];
           _activityIndicator.hidden=YES;
           _price.hidden=!_activityIndicator.isHidden;
           _price.text=[NSString stringWithFormat:@"$%.2f",[dataPack[@"data"][@"payable_amount"] floatValue]];
           _qty.text=[NSString stringWithFormat:@"%@",dataPack[@"data"][@"quantity"]];
           _infoDic[@"quantity"]=_qty.text;
           
         
           
           _infoDic[@"card_base_price"]=[NSString stringWithFormat:@"%@",dataPack[@"data"][@"payable_amount"]];
          // _infoDic[@""]
           [db update:CART_TABLE :_infoDic :[NSString stringWithFormat:@"id='%@'",_infoDic[@"id"]]];
            [self stopLoader];
            NSLog(@"%@",_infoDic[@"card_base_price"]);
           [self.delegate didChangeQuantity:_infoDic andAddedAmount:[_infoDic[@"card_base_price"] floatValue]-oldValue andIndexPath:_indexPath];
    }
       else{
           
           int count=[[[NSUserDefaults standardUserDefaults] valueForKey:CART_COUNT] intValue];
           count--;
           
           
           [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%d",count] forKey:CART_COUNT];
           NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
           [nc postNotificationName:CART_REFRESH object:self userInfo:nil];
           
           self.userInteractionEnabled=YES;
           [self.delegate didDeleteItemIndexPath:_indexPath];
           [self stopLoader];
           
       }
          
           
    
       
    
}
-(void)requestSucceededButActionFailed:(NSDictionary *)dataPack{
   
 [_activityIndicator stopAnimating];
_activityIndicator.hidden=YES;
     _price.hidden=!_activityIndicator.isHidden;
     [self stopLoader];
}
-(void)uploadedData:(NSString *)pecentage andByte:(float)bytes{
    
}
-(void)requestSucceededButNoDataFound:(NSDictionary *)dataPack{
     [self stopLoader];
}
-(void)requestFailedDueToServerIsuueWithError:(NSString*)errorDes{
     [self stopLoader];
}
@end
