//
//  CartTableViewCell.h
//  FrameShop
//
//  Created by Saurabh Srivastav on 04/03/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Global.h"
#import "DBManager.h"
#import "PopApi.h"
NS_ASSUME_NONNULL_BEGIN
@class CartTableViewCell;
@protocol CartTableViewCellDelegate <NSObject>
@optional

-(void)didSelectItem:(NSIndexPath *)indexPath;
-(void)didDeleteItemIndexPath:(NSIndexPath *)indexPath;
-(void)didPressDeleteItemIndexPath:(NSIndexPath *)indexPath;
-(void)didDeSelectItem:(NSIndexPath *)indexPath;
-(void)didPressMoreDetail:(NSIndexPath *)indexPath;
-(void)didChangeQuantity:(NSMutableDictionary *)infoDic andAddedAmount:(float)amount andIndexPath:(NSIndexPath *)indexPath;
@end
@interface CartTableViewCell : UITableViewCell<PopApiDelegate>
@property(strong,nonatomic)id<CartTableViewCellDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UIImageView *previewImage;
@property (weak, nonatomic) IBOutlet UILabel *productName;
@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UILabel *subTitle;
- (IBAction)trashBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *qtyView;
- (IBAction)minusBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *trashBtn;
@property (weak, nonatomic) IBOutlet UILabel *qty;
@property float amount;
@property (weak, nonatomic) IBOutlet UIButton *selectBtn;
@property NSIndexPath *indexPath;
@property NSMutableDictionary *infoDic;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *deleteActivity;
- (IBAction)pluseBtnAction:(id)sender;
- (IBAction)selectAction:(id)sender;
-(void)removeItemFromCart;
@property (weak, nonatomic) IBOutlet UIButton *viewMoreBtn;
- (IBAction)viewMoreDetail:(id)sender;
-(void)stopLoader;
@end

NS_ASSUME_NONNULL_END
