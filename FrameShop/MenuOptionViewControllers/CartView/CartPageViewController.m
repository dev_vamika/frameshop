//
//  CartPageViewController.m
//  FrameShop
//
//  Created by Saurabh Srivastav on 03/03/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import "CartPageViewController.h"

@interface CartPageViewController (){
    UIPageViewController *pageViewController;
    NSMutableArray *viewControllers;
    NSInteger index,pageNumber;
    
}

@end

@implementation CartPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setUpPageViewController];
    
    for (UIView *view in pageViewController.view.subviews) {
        if ([view isKindOfClass:[UIScrollView class]]) {
             [(UIScrollView *)view setScrollEnabled:false];
        }
    }
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
     [self.navigationItem setBackBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil]];
    self.navigationController.navigationBar.tintColor=[UIColor blackColor];
}




#pragma mark - Pageview Controller

-(UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    
    NSInteger index;
    if ([viewController isKindOfClass:[CartViewController class]]) {
        index = ((CartViewController*) viewController).index;
    }
    else if ([viewController isKindOfClass:[ShippingViewController class]]) {
        index = ((ShippingViewController*) viewController).index;
    }
    else if ([viewController isKindOfClass:[PaymentViewController class]]) {
        index = ((PaymentViewController*) viewController).index;
    }
    else{
        index=0;
    }
    
    if (index == 0) {
        return nil;
    }
    
    index--;
    
    return viewControllers[index];
}

-(UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    
    NSInteger index;
    if ([viewController isKindOfClass:[CartViewController class]]) {
        index = ((CartViewController*) viewController).index;
    }
    else if ([viewController isKindOfClass:[ShippingViewController class]]) {
        index = ((ShippingViewController*) viewController).index;
    }
    else if ([viewController isKindOfClass:[PaymentViewController class]]) {
        index = ((PaymentViewController*) viewController).index;
    }
    else{
        index=0;
    }
    
    if (index == viewControllers.count - 1) {
        return nil;
    }
    
    index++;
    
    return viewControllers[index];
}

- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray<UIViewController *> *)previousViewControllers transitionCompleted:(BOOL)completed{
    
    if (completed) {
        
        NSInteger currentIndex;
        if ([pageViewController.viewControllers.firstObject isKindOfClass:[CartViewController class]]) {
            currentIndex = ((CartViewController*) pageViewController.viewControllers.firstObject).index;
        }
        else if ([pageViewController.viewControllers.firstObject isKindOfClass:[ShippingViewController class]]) {
            currentIndex = ((ShippingViewController*) pageViewController.viewControllers.firstObject).index;
        }
        else if ([pageViewController.viewControllers.firstObject isKindOfClass:[PaymentViewController class]]) {
            currentIndex = ((PaymentViewController*) pageViewController.viewControllers.firstObject).index;
        }
        else{
            currentIndex=0;
        }
        
        [self slideBarToIndex:(int)currentIndex+1];
        
        
        pageNumber=(long)currentIndex;
        
    }
    
    
}

- (void)pageViewController:(UIPageViewController *)pageViewController willTransitionToViewControllers:(NSArray<UIViewController *> *)pendingViewControllers API_AVAILABLE(ios(6.0)){
    //NSInteger index = ((CameraViewController*) pendingViewControllers[0]).index;
}


#pragma mark - Class Methods

-(void)setUpPageViewController{
    
    pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
    pageViewController.dataSource = self;
    pageViewController.delegate=self;
    
    viewControllers = [NSMutableArray array];
    
    
    {
        CartViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"CartViewController"];
        vc.index=0;
        vc.delegate=self;
        [viewControllers addObject:vc];
    }
    
    {
        ShippingViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ShippingViewController"];
        vc.index=1;
        vc.delegate=self;
        [viewControllers addObject:vc];
    }
    
    {
        PaymentViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PaymentViewController"];
        vc.index=2;
        vc.delegate=self;
        [viewControllers addObject:vc];
    }
    
    
    NSArray *defaultViewController = [NSArray arrayWithObject:viewControllers[0]];
    
    [pageViewController setViewControllers:defaultViewController direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
    
    [self addChildViewController:pageViewController];
    [self.view addSubview:pageViewController.view];
    [pageViewController didMoveToParentViewController:self];
    
    index=0;
    
    [pageViewController.view addSubview:_tabBar];
    
    _indicatorBar.frame=CGRectMake(0,_tabBar.frame.size.height*2, self.view.bounds.size.width/3, 3);
    [pageViewController.view addSubview:_indicatorBar];
    [Global addshadow:_tabBar radious:1.0];
    _cartBtn.selected=YES;
    
    
    
    
}


-(void)slidePageOfIndex:(int)index andDirection:(UIPageViewControllerNavigationDirection) direction{
    
    NSArray *defaultViewController = [NSArray arrayWithObject:viewControllers[index]];
    
    [pageViewController setViewControllers:defaultViewController direction:direction animated:YES completion:nil];
}


-(void)slideBarToIndex:(int)index{
    
    float x=(self.view.bounds.size.width/3)*(index-1);
    
    [UIView animateWithDuration:0.3f delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        CGRect temp=self->_indicatorBar.frame;
        temp.origin.x=x;
        
        self->_indicatorBar.frame=temp;
        
        
    } completion:^(BOOL finished) {
        switch (index) {
            case 1:
                self->_cartBtn.selected=YES;
                self->_shippingBtn.selected=NO;
                self->_paymentBtn.selected=NO;
                break;
            case 2:
                self->_cartBtn.selected=NO;
                self->_shippingBtn.selected=YES;
                self->_paymentBtn.selected=NO;
                break;
            case 3:
                self->_cartBtn.selected=NO;
                self->_shippingBtn.selected=NO;
                self->_paymentBtn.selected=YES;
                break;
                
            default:
                break;
        }
        
    }];
}

#pragma mark - Actions

- (IBAction)nextAction:(id)sender {
    
}
- (IBAction)closeAction:(id)sender {
    [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"gift_shipping"];
    [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"product_shipping"];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)cartAction:(id)sender {
    
    
    if (_cartBtn.isSelected) {
        return;
    }
    else if (_shippingBtn.isSelected || _paymentBtn.isSelected){
        [self slidePageOfIndex:0 andDirection:UIPageViewControllerNavigationDirectionReverse];
        [self slideBarToIndex:1];
    }
    
    
}

- (IBAction)shippingAction:(id)sender {

    CartViewController *vc =(CartViewController *)viewControllers[0];
    if (vc.selectedCartItems.count==0) {
        return;
    }
    
    
    
    if (_shippingBtn.isSelected) {
        return;
    }
    else if ( _paymentBtn.isSelected){
        [self slidePageOfIndex:1 andDirection:UIPageViewControllerNavigationDirectionReverse];
        [self slideBarToIndex:2];
        _shippingBtn.selected=YES;
    }
    else{
        //will use in future
        [self slidePageOfIndex:1 andDirection:UIPageViewControllerNavigationDirectionForward];
        [self slideBarToIndex:2];
    }
    
    
}

- (IBAction)paymentAction:(id)sender {
//    NSString *cartCount = [[NSUserDefaults standardUserDefaults] valueForKey:CART_COUNT];
//    if ([cartCount intValue] == 0) {
//        return;
//    }
    ShippingViewController *vc1 =(ShippingViewController *)viewControllers[1];
    NSLog(@"%@",vc1.addressDic);
   NSString *email = [[NSUserDefaults standardUserDefaults] valueForKey:EMAIL];
   if (email == nil || [email isEqualToString:@""]) {
       UIAlertController * alert=   [UIAlertController
                                        alertControllerWithTitle:FRAMESHOP
                                        message:@"Please enter your E-mail and Address to proceed for Payment."
                                        preferredStyle:UIAlertControllerStyleAlert];
          
          UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                handler:^(UIAlertAction * action) {
          }];
          [alert addAction:defaultAction];
          [self presentViewController:alert animated:YES completion:nil];
       return;
   }else if(vc1.addressDic == nil || vc1.addressDic.count == 0){
       UIAlertController * alert=   [UIAlertController
                                     alertControllerWithTitle:FRAMESHOP
                                     message:@"Please enter your E-mail and Address to proceed for Payment."
                                     preferredStyle:UIAlertControllerStyleAlert];
       
       UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * action) {
       }];
       [alert addAction:defaultAction];
       [self presentViewController:alert animated:YES completion:nil];
       return;
   }
    CartViewController *vc =(CartViewController *)viewControllers[0];
     PaymentViewController *vc2 =(PaymentViewController *)viewControllers[2];
    
//    NSLog(@"%@",vc2.addressInfoDic);
    vc2.cartAmount=vc.totalAmount;
    if ( vc2.addressInfoDic==nil) {
        return;
    }
    
    [self slidePageOfIndex:2 andDirection:UIPageViewControllerNavigationDirectionForward];
    [self slideBarToIndex:3];
}




#pragma mark - ProceedBtn Delegate

-(void)didSelectProceedAction:(int)pageIndex{
    
    CartViewController *vc =(CartViewController *)viewControllers[0];
    
    NSMutableArray *arr=[[NSMutableArray alloc] init];
    
    ShippingViewController *vc1 =(ShippingViewController *)viewControllers[1];
    PaymentViewController *vc2 =(PaymentViewController *)viewControllers[2];
    
    for (int i=0; i<vc.selectedCartItems.count; i++) {
        arr[arr.count]=vc.selectedCartItems[i][@"cart_order_id"];
    }
    vc2.orderIdArray=arr;
    
    vc2.cartAmount=vc.totalAmount;
    vc2.addressInfoDic = vc1.addressDic;
     NSLog(@"%@",vc2.addressInfoDic);
    [self slidePageOfIndex:pageIndex+1 andDirection:UIPageViewControllerNavigationDirectionForward];
    [self slideBarToIndex:pageIndex+2];
}

#pragma mark - Shipping  Delegates
-(void)didSelectAdress:(NSMutableDictionary *)addressDic{
    
    PaymentViewController *vc2 =(PaymentViewController *)viewControllers[2];
    vc2.addressInfoDic=addressDic;
   
    
}
#pragma mark - Cart Delegates
-(void)didLoadedCartItem:(NSMutableArray *)orderArray{
    PaymentViewController *vc2 =(PaymentViewController *)viewControllers[2];
    NSMutableArray *arr=[[NSMutableArray alloc] init];
    for (int i=0; i<orderArray.count; i++) {
        arr[arr.count]=orderArray[i][@"cart_order_id"];
    }
    vc2.orderIdArray=arr;
    
}
#pragma mark - Payment Delegates
-(void)didAskToEmail:(int)pageIndex{
    [self shippingAction:_shpngBtn];
}
-(void)paymentFailedOrCanceled:(int)pageIndex{
    [self closeAction:[UIButton new]];
}
@end
