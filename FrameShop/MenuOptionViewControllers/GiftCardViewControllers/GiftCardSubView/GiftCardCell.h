//
//  GiftCardCell.h
//  FrameShop
//
//  Created by Vamika on 04/03/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopApi.h"
#import "DBManager.h"
#import "Validation.h"

NS_ASSUME_NONNULL_BEGIN
@class MenuViewController;
@protocol GiftCardCellDelegate <NSObject>
@optional
-(void)didSelectGiftCard:(NSDictionary *)infoDic;
-(void)didLoadedGiftCard:(NSDictionary *)infoDic;

-(void)didGetNoConnection;
-(void)didFailToLoadData;

@end
@interface GiftCardCell : UITableViewCell<UICollectionViewDelegate,UICollectionViewDataSource,PopApiDelegate,UITextFieldDelegate>
@property(strong,nonatomic)id<GiftCardCellDelegate> delegate;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionCell;
@property (weak, nonatomic) IBOutlet UITextField *nameToTF;
@property (weak, nonatomic) IBOutlet UITextField *emailToTF;
@property (weak, nonatomic) IBOutlet UITextView *personalNoteTF;
@property (weak, nonatomic) IBOutlet UITextField *emailFromTF;
@property (weak, nonatomic) IBOutlet UITextField *nameFromTF;

-(void)refreshCollectionView;
@end

NS_ASSUME_NONNULL_END
