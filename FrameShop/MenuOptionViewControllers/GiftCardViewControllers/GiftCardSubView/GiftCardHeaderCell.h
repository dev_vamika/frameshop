//
//  GiftCardHeaderCell.h
//  FrameShop
//
//  Created by Vamika on 04/03/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GiftCardHeaderCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *cardImage;

@end

NS_ASSUME_NONNULL_END
