//
//  GiftCardCell.m
//  FrameShop
//
//  Created by Vamika on 04/03/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import "GiftCardCell.h"
#import "GiftAmountViewCell.h"

@implementation GiftCardCell{
    DbManager *db;
    NSMutableArray *dataArray;
    NSIndexPath *selectIndexPath;
    
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    dataArray=[[NSMutableArray alloc] init];
    
    [self.collectionCell registerNib:[UINib nibWithNibName:@"GiftAmountViewCell" bundle:nil] forCellWithReuseIdentifier:@"GiftAmountViewCell"];
    
    
    
    [Global roundCorner:self.nameToTF roundValue:5.0 borderWidth:0.5 borderColor:[UIColor lightGrayColor]];
    [Global roundCorner:self.emailToTF roundValue:5.0 borderWidth:0.5 borderColor:[UIColor lightGrayColor]];
    [Global roundCorner:self.personalNoteTF roundValue:5.0 borderWidth:0.5 borderColor:[UIColor lightGrayColor]];
    [Global roundCorner:self.nameFromTF roundValue:5.0 borderWidth:0.5 borderColor:[UIColor lightGrayColor]];
    [Global roundCorner:self.emailFromTF roundValue:5.0 borderWidth:0.5 borderColor:[UIColor lightGrayColor]];
    [Global Padding:_emailToTF];
    [Global Padding:_emailFromTF];
    [Global Padding:_nameToTF];
    [Global Padding:_nameFromTF];
    // [Global Padding:_personalNoteTF];
    db=[[DbManager alloc] init];
    _emailToTF.delegate=self;
    _nameToTF.delegate=self;
    _emailFromTF.delegate=self;
    _nameFromTF.delegate=self;
    
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    
}

-(void)loadProductData{
    
    dataArray=[[db getDataForField:GIFTCARD_TABLE where:nil] mutableCopy];
    if (dataArray.count!=0) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        selectIndexPath=indexPath;
        [_collectionCell reloadData];
        
    }
    
    
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    NSString *urlString=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,GET_GIFTCARDS];
    
    NSMutableArray *arr=[[NSMutableArray alloc] init];
    arr=[db getDataForField:USERTABLE where:nil];
     NSLog(@"arr %@",arr);
    if (arr.count!=0) {
        [api sendGetRequst:urlString andToken:arr[0][@"token"]];
    }
    
}


-(void)signUpUser{
    
    
    
    
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
    dic[@"name"]=[[UIDevice currentDevice] name];
    dic[@"email"]=[NSString stringWithFormat:@"%@@frameshop.com.au",[Global getDeviceId]];
    dic[@"password"]=@"qwxcjeub32ne";
    dic[@"c_password"]=@"qwxcjeub32ne";
    dic[@"url"]=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,REGISTER_USER];
    [api sendPostRequst:dic];
    
}
-(void)logInUser{
    
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
    
   dic[@"email"]=[NSString stringWithFormat:@"%@@frameshop.com.au",[Global getDeviceId]];
    dic[@"password"]=@"qwxcjeub32ne";
    
    dic[@"url"]=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,LOGIN_USER];
    [api sendPostRequst:dic];
}

-(void)refreshCollectionView{
    _collectionCell.delegate = self;
    _collectionCell.dataSource = self;
    [_collectionCell reloadData];
    
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
    dic[@"contr"]=self;
    
    NSMutableArray *userInfo=[db getDataForField:USERTABLE where:nil];
    
    if (userInfo.count!=0) {
        [self loadProductData];
    }
    else
    {
        [self signUpUser];
    }
    
}



#pragma mark -Text Field Delegates




- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
   
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    [self checkValidation:textField];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField isW:(BOOL)value{
   
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    [self checkValidation:textField];
    return YES;
    
}

-(void)checkValidation:(UITextField *)textField{
   
    if (textField==_nameFromTF || textField==_nameToTF) {
       if ([Validation isValidText:textField.text]){
          // [Global roundCorner:textField roundValue:ROUND_CORNER borderWidth:BORDER_WIDTH borderColor:[UIColor greenColor]];
           
           [Validation setValidationMarkToTextField:textField isCorrect:YES];
        
           
       }
       else{
            [Validation setValidationMarkToTextField:textField isCorrect:NO];
       }
        
    }
    
    else  if (textField == _emailToTF ||textField == _emailFromTF )  {
        
        if ([Validation isValidEmail:textField.text] && !([_emailFromTF.text isEqualToString:_emailToTF.text])) {
             [Validation setValidationMarkToTextField:textField isCorrect:YES];
        }
        else{
            [Validation setValidationMarkToTextField:textField isCorrect:NO];
        }
      
    }
   
}






#pragma mark - UICollectionViewDataSource and Delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return (dataArray.count!=0)?dataArray.count:3;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"GiftAmountViewCell";
    GiftAmountViewCell *cells =[_collectionCell dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    [Global roundCorner:cells.bgView roundValue:5.0 borderWidth:0.5 borderColor:[UIColor colorWithRed:249/255.0 green:197/255.0 blue:47/255.0 alpha:1.0]];
    cells.knotImage.image = [UIImage imageNamed:@"knot_gray_icon.png"];
    cells.bgView.backgroundColor=[UIColor whiteColor];
    cells.priceLbl.textColor=[UIColor lightGrayColor];
    cells.priceLbl.text = @"";
    [Global roundCorner:cells.bgView roundValue:5.0 borderWidth:0.5 borderColor:[UIColor lightGrayColor]];
    [Global startShimmeringToView:cells.contentView];
    if (dataArray.count!=0) {
        cells.knotImage.image = [UIImage imageNamed:@"knot_icon.png"];
        if (indexPath==selectIndexPath) {
            //cells.backgroundColor = APP_BLUE_COLOR;
            cells.priceLbl.textColor = UIColor.whiteColor;
            cells.bgView.backgroundColor=APP_BLUE_COLOR;
        }else{
            cells.priceLbl.textColor = APP_GRAY_COLOR;
            cells.backgroundColor = [UIColor whiteColor];
            cells.knotImage.image = [UIImage imageNamed:@"knot_gray_icon.png"];//knot_gray_icon.png
//            cells.bgView.backgroundColor=[Global colorFromHexString:dataArray[indexPath.row][@"card_color"]];
//            cells.priceLbl.textColor=[self getCheckMarkWhereBgColor:dataArray[indexPath.row][@"card_color"]];
        }
        cells.priceLbl.text = [NSString stringWithFormat:@"$%@",dataArray[indexPath.row][@"card_amount"]];
        [Global stopShimmeringToView:cells.contentView];
    }
    return cells;
}

-(UIColor *)getCheckMarkWhereBgColor:(NSString *)hexString{
    
    
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    int r=((rgbValue & 0xFF0000) >> 16);
    int g=((rgbValue & 0xFF00) >> 8);
    int b=(rgbValue & 0xFF);
    
    int lightness = ((r*299)+(g*587)+(b*114))/1000; //get lightness value
    
    if (lightness < 127) { //127 = middle of possible lightness value
        return [UIColor whiteColor];
    }
    else  return [UIColor blackColor];
    
    
}
- (CGSize)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake((self.collectionCell.frame.size.width/4)-5, self.collectionCell.frame.size.height*0.9);
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    GiftAmountViewCell *cells = (GiftAmountViewCell*)[self.collectionCell cellForItemAtIndexPath:indexPath];
    
    cells.bgView.backgroundColor=[Global colorFromHexString:dataArray[indexPath.row][@"card_color"]];
    cells.knotImage.image = [UIImage imageNamed:@"knot_icon.png"];//knot_gray_icon.png
    cells.priceLbl.textColor=[UIColor blackColor];
    selectIndexPath=indexPath;
    [self.delegate didSelectGiftCard:dataArray[indexPath.row]];
    [_collectionCell reloadData];
}




#pragma mark - POP API DELEGATES
-(void)connectionError{
    
    if (dataArray.count==0) {
        [self.delegate didGetNoConnection];
    }
    
    
}
-(void)requestFailedSessionExpired:(NSDictionary *)dataPack{
    
}
-(void)requestFailedServerError:(NSDictionary *)dataPack{
    
}
-(void)requestFailedServiceUnavailableDueToMaintenance:(NSDictionary *)dataPack{
    [Global showSimpleAlert:self andTitle:FRAMESHOP andMessage:dataPack[@"message"]];
}
-(void)requestFailedMethodNotFound:(NSDictionary *)dataPack{
    
}
-(void)requestFailedNotFound:(NSDictionary *)dataPack{
    if (dataArray.count==0) {
        [self.delegate didGetNoConnection];
    }
}
-(void)requestFailedUnauthorised:(NSDictionary *)dataPack{
     [self logInUser];
}
-(void)requestFailedBadRequest:(NSDictionary *)dataPack{
    if (dataArray.count==0) {
        [self.delegate didGetNoConnection];
    }
}
-(void)requestSucceededWithData:(NSDictionary *)dataPack{
    
    if ([dataPack[@"url"] isEqualToString:LOGIN_USER]) {
        
        [db delTableWithTableName:USERTABLE];
        NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
        
        dic[@"id"]              =[NSString stringWithFormat:@"%@",dataPack[@"data"][@"id"]];
        dic[@"name"]            =dataPack[@"data"][@"name"];
        dic[@"email"]           =dataPack[@"data"][@"email"];
        dic[@"created_at"]      =dataPack[@"data"][@"created_at"];
        dic[@"updated_at"]      =dataPack[@"data"][@"updated_at"];
        dic[@"token"]           =dataPack[@"data"][@"access_token"];
        
        [db insertIntoTable:dic table_name:USERTABLE];
        [self loadProductData];
    }
    
    else if ([dataPack[@"url"] isEqualToString:REGISTER_USER]){
        
        NSMutableDictionary *dic =[[NSMutableDictionary alloc] init];
        dic[@"token"]            =dataPack[@"data"][@"token"];
        dic[@"id"]               =[NSString stringWithFormat:@"%@",dic[@"id"]];
        
        [db insertIntoTable:dic table_name:USERTABLE];
        [self loadProductData];
    }
    else{
        // reload table with product data
        [dataArray removeAllObjects];
        NSArray *arr=[dataPack[@"data"][@"card_details"] mutableCopy];
        [db delTableWithTableName:GIFTCARD_TABLE];
        for (int i=0; i<arr.count; i++) {
            NSMutableDictionary *temDic=[[NSMutableDictionary alloc] init];
            temDic[@"id"]          =[NSString stringWithFormat:@"%@",arr[i][@"id"]];
            temDic[@"card_name"]        =arr[i][@"card_name"];
            temDic[@"card_pic_url"]         =arr[i][@"card_pic_url"];
            temDic[@"card_amount"]  =[NSString stringWithFormat:@"%@",arr[i][@"card_amount"]];
            temDic[@"card_color"]         =arr[i][@"card_color"];
            dataArray[dataArray.count]=temDic;
            [db insertIntoTable:temDic table_name:GIFTCARD_TABLE];
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
            selectIndexPath=indexPath;
            if (i==0) {
                [self.delegate didLoadedGiftCard:dataArray[0]];
            }
            
        }
        [_collectionCell reloadData];
    }
}
-(void)requestSucceededButActionFailed:(NSDictionary *)dataPack{
    
    if ([dataPack[@"url"] isEqualToString:REGISTER_USER] || [dataPack[@"message"] isEqualToString:@"Unauthenticated."]) {
        //go To login
        
        [self logInUser];
    }
    else if ([dataPack[@"url"] isEqualToString:LOGIN_USER]) {
        //something bad happned with server
        [self signUpUser];
        NSLog(@"something bad happned with server login signup both failed");
    }
    else{
        NSLog(@"there is some problme with product api");
        //there is some problme with product api
    }
    
    
}
-(void)uploadedData:(NSString *)pecentage andByte:(float)bytes{
    
}
-(void)requestSucceededButNoDataFound:(NSDictionary *)dataPack{
    if (dataArray.count==0) {
        [self.delegate didFailToLoadData];
    }
}
-(void)requestFailedDueToServerIsuueWithError:(NSString*)errorDes{
    if (dataArray.count==0) {
        [self.delegate didFailToLoadData];
    }
}




@end
