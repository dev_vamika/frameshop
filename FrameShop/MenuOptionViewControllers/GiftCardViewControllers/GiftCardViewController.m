//
//  GiftCardViewController.m
//  FrameShop
//
//  Created by Vamika on 04/03/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import "GiftCardViewController.h"
#import "DBManager.h"
#import "GiftCardHeaderCell.h"
#import "Global.h"
#import "Validation.h"
#define GIFT_CARD_ID                @"GIFT_CARD_ID"
@interface GiftCardViewController ()<PopApiDelegate>{
    DbManager *db;
    NSMutableDictionary *selectedInfoDic;
    UIColor *addBtnColor;
    WarningSubView *warningView;
    
    
}

@end

@implementation GiftCardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _loader.hidden=YES;
    selectedInfoDic = [[NSMutableDictionary alloc] init];
    self.title = @"Gift Card";
    db=[[DbManager alloc] init];
    [Global roundCorner:self.addToCartBtn roundValue:self.addToCartBtn.frame.size.height/2 borderWidth:2.0 borderColor:[UIColor whiteColor]];
    addBtnColor=_addToCartBtn.backgroundColor;
    
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    [warningView.view removeFromSuperview];
    [_tableView reloadData];
}

-(void)updateFrame{
    self.view.userInteractionEnabled = NO;
    _loader.hidden=NO;
    [_loader startAnimating];
    [_addToCartBtn setTitle:@"" forState:UIControlStateNormal];
    [UIView animateWithDuration:0.5 animations:^{
        self->_addCartWidth.constant = (self.addToCartBtn.frame.size.height*2)-self.view.frame.size.width;
    }];
}

-(void)removeLoader{
    self.view.userInteractionEnabled = YES;
    _loader.hidden=YES;
    [_loader stopAnimating];
    [_addToCartBtn setTitle:@"ADD TO CART" forState:UIControlStateNormal];
    [UIView animateWithDuration:0.5 animations:^{
        self->_addCartWidth.constant = 0.0;
    }];
}

#pragma mark - Class Methods

-(void)setImageToImageViewWhereImageView:(UIImageView *)imageView andImageId:(NSString *)imageId andImageUrl:(NSString *)url andCacheName:(NSString *)imageName{
    
    imageView.backgroundColor=[UIColor groupTableViewBackgroundColor];
    imageView.image=[Global getImageFromDoucumentWithName:imageName];
    if (imageView.image!=nil) {
        imageView.backgroundColor=[UIColor clearColor];
        return;
    }
    
    
    NSString *giftId=imageId;
    [[NSUserDefaults standardUserDefaults] setValue:giftId forKey:GIFT_CARD_ID];
    
    
    dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(q, ^{
        //[Global startShimmeringToView:cell.contentView];
        NSData *datas;
        
        datas =[NSData dataWithContentsOfURL:[NSURL URLWithString:url]] ;
        
        [Global saveImagesInDocumentDirectory:datas ImageId:imageName];
        
        
        UIImage *img = [[UIImage alloc] initWithData:datas];
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([[[NSUserDefaults standardUserDefaults] valueForKey:GIFT_CARD_ID] isEqualToString:giftId]) {
                imageView.image=img;
                imageView.backgroundColor=[UIColor clearColor];
            }
            
            
        });
    });
    
    
}



#pragma mark - API Methods

-(void)sendDataToServerData{
    
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    
    NSString *urlString=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,ADD_GIFTCARDS_CART];
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
    dic[@"url"]=urlString;
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:1];
    GiftCardCell *cell = (GiftCardCell *)[_tableView cellForRowAtIndexPath:indexPath];
    NSMutableArray *arr=[[NSMutableArray alloc] init];
    arr=[db getDataForField:USERTABLE where:nil];
    NSLog(@"arr %@",arr);

    
    if (![Validation isValidText:cell.nameToTF.text]) {
        [Global showSimpleAlert:self andTitle:FRAMESHOP andMessage:NAME_TO_INVALID];
        
        return;
        
        
    }
    if (![Validation isValidText:cell.nameFromTF.text]) {
        [Global showSimpleAlert:self andTitle:FRAMESHOP andMessage:NAME_FROM_INVALID];
        
        return;
        
    }
    if (![Validation isValidEmail:cell.emailToTF.text]) {
        [Global showSimpleAlert:self andTitle:FRAMESHOP andMessage:EMAIL_TO_INVALID];
        
        return;
        
    }
    if (![Validation isValidEmail:cell.emailFromTF.text]) {
        [Global showSimpleAlert:self andTitle:FRAMESHOP andMessage:EMAIL_FROM_INVALID];
        return;
    }
    if ([cell.emailFromTF.text isEqualToString:cell.emailToTF.text] ) {
        
        [Global showSimpleAlert:self andTitle:FRAMESHOP andMessage:EMAIL_MATCH_ERROR];
        return;
    }
    
    
    
    
    [self updateFrame];
    
    if (arr.count!=0) {
        
        dic[@"linked_id"] = selectedInfoDic[@"id"];
        dic[@"order_type"] = @"gift_card";
        dic[@"payable_amount"]=selectedInfoDic[@"card_amount"];
        dic[@"token"]=arr[0][@"token"];
        
        
        NSMutableDictionary *userDic=[[NSMutableDictionary alloc] init];
        
        
        
        userDic[@"name_to"]=cell.nameToTF.text;
        userDic[@"email_to"]=cell.emailToTF.text;
        userDic[@"personal_note"]=cell.personalNoteTF.text;
        userDic[@"name_from"]=cell.nameFromTF.text;
        userDic[@"email_from"]=cell.emailFromTF.text ;
        userDic[@"product_name"] = selectedInfoDic[@"card_name"];//[NSString stringWithFormat:@"$%@ Gift Card",selectedInfoDic[@"card_amount"]];//selectedInfoDic[@"card_name"];
        
        dic[@"token"]=arr[0][@"token"];
        
        dic[@"cart_order_info"]=userDic;
        
        [api sendPostRequst:dic];
    }
}


-(void)signUpUser{
    
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
    dic[@"name"]=[[UIDevice currentDevice] name];
    dic[@"email"]=[NSString stringWithFormat:@"%@@frameshop.com.au",[Global getDeviceId]];
    dic[@"password"]=@"qwxcjeub32ne";
    dic[@"c_password"]=@"qwxcjeub32ne";
    dic[@"url"]=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,REGISTER_USER];
    [api sendPostRequst:dic];
    
}
-(void)logInUser{
    
    PopApi *api=[[PopApi alloc] init];
    api.delegate=self;
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
    
    dic[@"email"]=[NSString stringWithFormat:@"%@@frameshop.com.au",[Global getDeviceId]];
    dic[@"password"]=@"qwxcjeub32ne";
    
    dic[@"url"]=[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,LOGIN_USER];
    [api sendPostRequst:dic];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        static NSString *simpleTableIdentifier = @"GiftCardHeaderCell";
        GiftCardHeaderCell *cell = (GiftCardHeaderCell *)[_tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"GiftCardHeaderCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        if (selectedInfoDic!=nil) {
            
            
            [self setImageToImageViewWhereImageView:cell.cardImage andImageId:selectedInfoDic[@"id"] andImageUrl:selectedInfoDic[@"card_pic_url"] andCacheName:[NSString stringWithFormat:@"gift_%@",selectedInfoDic[@"id"]]];
            
        }
        else{
            NSArray *arr=[[db getDataForField:GIFTCARD_TABLE where:nil] mutableCopy];
            
            if (arr.count!=0) {
                
               
                
                [self setImageToImageViewWhereImageView:cell.cardImage andImageId:arr[0][@"id"] andImageUrl:arr[0][@"card_pic_url"] andCacheName:[NSString stringWithFormat:@"gift_%@",arr[0][@"id"]]];
                
            }
        }
        
        
        
        
        
        return cell;
    }
    
    
    
    static NSString *simpleTableIdentifier = @"GiftCardCell";
    GiftCardCell *cell = (GiftCardCell *)[_tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"GiftCardCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.delegate=self;
    [cell refreshCollectionView];
    
    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        
        float cellWidth=self.tableView.bounds.size.width;
        float ratio=0.5;
        float cellHeight=cellWidth*ratio;
        return cellHeight;
        
        
        
        
        return cellHeight;
    }
    NSArray *nibViews = [[NSBundle mainBundle] loadNibNamed:@"GiftCardCell" owner:self options:nil];
    GiftCardCell *sqr = [nibViews objectAtIndex:0];
    return sqr.frame.size.height;
    
    
}

#pragma mark - Action Methods

- (IBAction)onClickAddToCart:(id)sender {
    
    
    [self sendDataToServerData];
    
    
}

- (IBAction)onCLosePressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - GiftCardDelegate
-(void)didSelectGiftCard:(NSDictionary *)infoDic{
    
    selectedInfoDic = [infoDic mutableCopy];
    
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    GiftCardHeaderCell *cell = (GiftCardHeaderCell *)[_tableView cellForRowAtIndexPath:indexPath];
    
    
    [self setImageToImageViewWhereImageView:cell.cardImage andImageId:infoDic[@"id"] andImageUrl:infoDic[@"card_pic_url"] andCacheName:[NSString stringWithFormat:@"gift_%@",infoDic[@"id"]]];
    
}

-(void)didLoadedGiftCard:(NSDictionary *)infoDic{
    
     selectedInfoDic=[infoDic mutableCopy];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    GiftCardHeaderCell *cell = (GiftCardHeaderCell *)[_tableView cellForRowAtIndexPath:indexPath];
    
    [self setImageToImageViewWhereImageView:cell.cardImage andImageId:infoDic[@"id"] andImageUrl:infoDic[@"card_pic_url"] andCacheName:[NSString stringWithFormat:@"gift_%@",infoDic[@"id"]]];
    
    
    
}



-(void)didGetNoConnection{
    [self connectionError];
}
-(void)didFailToLoadData{
    
}
#pragma mark -Pop api Delegates

-(void)connectionError{
    if (warningView ==nil) {
        warningView =[[WarningSubView alloc] init];
    }
    warningView.view.frame=self.view.frame;
    [self.view addSubview:warningView.view];
    
    warningView.titleText.text=NO_INTERNET_MSG;
    warningView.subTitleTxt.text=@"Tap to try again";
    warningView.noCartIcon.image=[UIImage imageNamed:@"noInternet.png"];
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [warningView.view addGestureRecognizer:singleFingerTap];
    
    
}
-(void)requestFailedSessionExpired:(NSDictionary *)dataPack{
    
}
-(void)requestFailedServerError:(NSDictionary *)dataPack{
    
}
-(void)requestFailedServiceUnavailableDueToMaintenance:(NSDictionary *)dataPack{
    [self removeLoader];
    [Global showSimpleAlert:self andTitle:FRAMESHOP andMessage:dataPack[@"message"]];
}
-(void)requestFailedMethodNotFound:(NSDictionary *)dataPack{
    
}
-(void)requestFailedNotFound:(NSDictionary *)dataPack{
    
}
-(void)requestFailedUnauthorised:(NSDictionary *)dataPack{
     [self logInUser];
}
-(void)requestFailedBadRequest:(NSDictionary *)dataPack{
    
}
-(void)requestSucceededWithData:(NSDictionary *)dataPack{
    
    if ([dataPack[@"url"] isEqualToString:LOGIN_USER]) {
        
        [db delTableWithTableName:USERTABLE];
        NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
        
        dic[@"id"]              =[NSString stringWithFormat:@"%@",dataPack[@"data"][@"id"]];
        dic[@"name"]            =dataPack[@"data"][@"name"];
        dic[@"email"]           =dataPack[@"data"][@"email"];
        dic[@"created_at"]      =dataPack[@"data"][@"created_at"];
        dic[@"updated_at"]      =dataPack[@"data"][@"updated_at"];
        dic[@"token"]           =dataPack[@"data"][@"access_token"];
        
        [db insertIntoTable:dic table_name:USERTABLE];
        [self sendDataToServerData];
    }
    
    else if ([dataPack[@"url"] isEqualToString:REGISTER_USER]){
        
        NSMutableDictionary *dic =[[NSMutableDictionary alloc] init];
        dic[@"token"]            =dataPack[@"data"][@"token"];
        dic[@"id"]               =[NSString stringWithFormat:@"%@",dic[@"id"]];
        
        [db insertIntoTable:dic table_name:USERTABLE];
        [self sendDataToServerData];
    }
    else{
        [self removeLoader];
        // reload table with product data
        //           currentIndex = nil;
        //           [self.tableView reloadData];
        
        int count=[[[NSUserDefaults standardUserDefaults] valueForKey:CART_COUNT] intValue]+1;
        [[NSUserDefaults standardUserDefaults ] setValue:[NSString stringWithFormat:@"%d",count] forKey:CART_COUNT];
        
        [self dismissViewControllerAnimated:YES completion:nil];
        NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
        [nc postNotificationName:FINISH_UPLOADING object:self userInfo:nil];
        
        
    }
    
    
    
    
    
}
-(void)requestSucceededButActionFailed:(NSDictionary *)dataPack{
    
}
-(void)uploadedData:(NSString *)pecentage andByte:(float)bytes{
    
}
-(void)requestSucceededButNoDataFound:(NSDictionary *)dataPack{
    
}
-(void)requestFailedDueToServerIsuueWithError:(NSString*)errorDes{
    
}



@end
