//
//  GiftCardViewController.h
//  FrameShop
//
//  Created by Vamika on 04/03/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Global.h"
#import "GiftCardCell.h"
#import "Validation.h"
#import "WarningSubView.h"
#define NAME_TO_INVALID @"Please enter Recipient's name!"
#define NAME_FROM_INVALID @"Please enter Your name!"
#define EMAIL_TO_INVALID @"Please enter Recipient's E-mail!"
#define EMAIL_FROM_INVALID @"Please enter Your E-mail!"
#define EMAIL_MATCH_ERROR @"Recipient's Email and Your Email can not be same!"
NS_ASSUME_NONNULL_BEGIN

@interface GiftCardViewController : UIViewController<GiftCardCellDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *addToCartBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *addCartWidth;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loader;
- (IBAction)onCLosePressed:(id)sender;
- (IBAction)onClickAddToCart:(id)sender;
@end

NS_ASSUME_NONNULL_END
