//
//  WebViewController.m
//  FrameShop
//
//  Created by Vamika on 18/05/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import "WebViewController.h"
#import "Global.h"
@interface WebViewController ()

@end

@implementation WebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _webView.UIDelegate = self;
    _webView.navigationDelegate = self;
    if ([_isFrom isEqualToString:@"payment"]) {
        [self.closeBtn setHidden:YES];
        [self.webView setHidden:NO];
    }else{
        [self.closeBtn setHidden:NO];
        [self.webView setHidden:YES];
    }
    if ([self.type isEqualToString: @"FAQ"]) {
        self.title = @"FAQ";
        [self loadWebViewWithUrl:self.url];
        [self.webView setHidden:NO];
    }else if ([self.type isEqualToString: @"Contact-Us"]) {
        self.title = @"Contact Us";
    }else if ([self.type isEqualToString: @"Terms & Conditions"]) {
        self.title = @"Terms & Conditions";
        [self loadWebViewWithUrl:self.url];
    }else if ([self.type isEqualToString: @"Privacy Policy"]) {
        self.title = @"Privacy Policy";
        [self loadWebViewWithUrl:self.url];
    }
    [Global roundCorner:self.callBtn roundValue:_callBtn.frame.size.height/2 borderWidth:0.8 borderColor:[UIColor colorWithRed:69/225.0 green:79/255.0 blue:162/255.0 alpha:1.0]];
    [Global roundCorner:self.emailBtn roundValue:_emailBtn.frame.size.height/2 borderWidth:0.0 borderColor:UIColor.whiteColor];
    // Do any additional setup after loading the view.
}
- (IBAction)onCancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)onCallUS:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",@"0297506055"]] options:@{} completionHandler:^(BOOL success) {
        //
    }];
}
- (IBAction)onEamilUS:(id)sender {
    NSString *toEmail=@"support@frameshop.com.au";
    NSString *subject=@"Query for Frameshop App";
    NSString *body = [NSString stringWithFormat:@"%@\n\n\n\n%@\n%@\n%@",@"",@"",@"",@""];
    
    //opens mail app with new email started
    NSString *email = [NSString stringWithFormat:@"mailto:%@?subject=%@&body=%@", toEmail,subject,body];
    email = [email stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:email] options:@{} completionHandler:^(BOOL success) {
    }];
}
-(void)loadWebViewWithUrl:(NSString*)url{
    NSURL *targetURL = [NSURL URLWithString:url];
    NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
    [_webView loadRequest:request];
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(null_unspecified WKNavigation *)navigation{
  
 }
 - (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler{
     decisionHandler(WKNavigationActionPolicyAllow);
 }

 - (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error{
     
 }
@end
