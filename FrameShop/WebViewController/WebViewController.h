//
//  WebViewController.h
//  FrameShop
//
//  Created by Vamika on 18/05/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>
NS_ASSUME_NONNULL_BEGIN

@interface WebViewController : UIViewController<WKNavigationDelegate,WKUIDelegate>
@property (weak, nonatomic) IBOutlet WKWebView *webView;
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;
@property (weak, nonatomic) IBOutlet UIButton *callBtn;
@property (weak, nonatomic) IBOutlet UIButton *emailBtn;
@property NSString *url, *type,*isFrom;
@end

NS_ASSUME_NONNULL_END
